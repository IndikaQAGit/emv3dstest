package com.modirum.ds.web.validators;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ValidationContext {

    public static final String STRICT_VALIDATION                 = "strictValidation";
    public static final String AREQ_BR_BROWSER_IP_REQUIRED       = "AReq.BR.browserIP.required";
    public static final String EXTRA_REQUESTOR_CHALLENGE_INDS    = "extraRequestorChallengeInds";
    public static final String CRITICAL_EXTENSIONS_PASS          = "criticalExtensionsPass";
    public static final String EXTRA_AUTHENTICATION_TYPES        = "extraAuthenticationTypes";
    public static final String ARES_NPA_AUTHENTICATION_REQUIRED  = "ARes.NPA.authenticationValue.required";
    public static final String ARES_3RI_AUTHENTICATION_REQUIRED  = "ARes.3RI.authenticationValue.required";
    public static final String ACCEPTED_ARES_STATUSES            = "acceptedAResStatuses";
    public static final String EXTRA_RESULTS_STATUSES            = "extraResultStatuses";
    public static final String RREQ_NPA_AUTHENTICATION_REQUIRED  = "RReq.NPA.authenticationValue.required";
    public static final String RREQ_3RI_AUTHENTICATION_REQUIRED  = "RReq.3RI.authenticationValue.required";
    public static final String RREQ_NPA_TRANS_STATUS_REASON      = "RReq.NPA.transStatusReason";
    public static final String RREQ_ECI_REQUIRED                 = "RReq.ECI.required";
    public static final String ACCEPTED_RREQ_STATUSES            = "acceptedRReqStatuses";
    public static final String ARES_ECI_REQUIRED                        = "ARes.ECI.required";
    public static final String ARES_TRANS_STATUS_A_ECI                  = "ARes.transStatusA.ECI";
    public static final String ARES_TRANS_STATUS_U_ECI                  = "ARes.transStatusU.ECI";
    public static final String ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE = "ARes.transStatusA.AuthenticationValue";

    private Map<String, String> params;

    /**
     * Constructor.
     */
    public ValidationContext() {
        params = new HashMap<>();
    }

    public String get(String key) {
        return params.get(key);
    }

    public boolean getBoolean(String key) {
        return Boolean.valueOf(params.get(key));
    }

    /**
     * If the value specified by the key is null/empty, it returns the defaultValue, otherwise it returns the
     * boolean value stored from the setting.
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        String value = params.get(key);
        return StringUtils.isEmpty(value) ? defaultValue : Boolean.valueOf(value);
    }

    public String put(String key, String value) {
        return params.put(key, value);
    }
}
