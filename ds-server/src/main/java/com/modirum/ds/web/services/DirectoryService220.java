/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 12. jaan 2016
 *
 */
package com.modirum.ds.web.services;

import com.modirum.ds.enums.FieldName;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.DeviceChannel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.model.TDSModel.MessageVersion;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService220;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XcardRangeData;
import com.modirum.ds.util.ErrorUtils;
import com.modirum.ds.util.ObjectUtils;
import com.modirum.ds.web.context.WebContext;
import com.modirum.ds.web.filters.FiltersProvider210;
import com.modirum.ds.web.filters.FiltersProvider220;
import com.modirum.ds.web.validators.ValidationContext;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.modirum.ds.model.TDSModel.XtransStatus.A;
import static com.modirum.ds.model.TDSModel.XtransStatus.N;
import static com.modirum.ds.model.TDSModel.XtransStatus.R;
import static com.modirum.ds.model.TDSModel.XtransStatus.U;
import static com.modirum.ds.model.TDSModel.XtransStatus.Y;

/**
 * Main service to process messages for 3DS2.2 specification.
 */
@Component
public class DirectoryService220 extends DirectoryService210 {

    private static final Set<String> validThreeDSReqAuthMethodIndValues = new HashSet<>(Arrays.asList("01", "02", "03"));
    private static final Set<String> browserColorDepthValidValues = new HashSet<>(Arrays.asList("1", "4", "8", "15", "16", "24", "30", "32", "48"));
    private static final Set<String> transStatusValues22ARes = new HashSet<>(Arrays.asList(
            TDSModel.XtransStatus.Y.value(),
            TDSModel.XtransStatus.N.value(),
            TDSModel.XtransStatus.U.value(),
            TDSModel.XtransStatus.A.value(),
            TDSModel.XtransStatus.C.value(),
            TDSModel.XtransStatus.R.value(),
            TDSModel.XtransStatus.D.value(),
            TDSModel.XtransStatus.I.value()
    ));

    protected static final Set<String> transStatusReasonList22  = new HashSet<>(Arrays.asList(
            TDSModel.XtransStatusReason.C01AuthFailed.value(),
            TDSModel.XtransStatusReason.C02UnknDevice.value(),
            TDSModel.XtransStatusReason.C03UnsupDevice.value(),
            TDSModel.XtransStatusReason.C04ExceedsAuthFreq.value(),
            TDSModel.XtransStatusReason.C05ExpiredCard.value(),
            TDSModel.XtransStatusReason.C06InvalidPan.value(),
            TDSModel.XtransStatusReason.C07InvalidTx.value(),
            TDSModel.XtransStatusReason.C08NoCardrecord.value(),
            TDSModel.XtransStatusReason.C09SecurityFailure.value(),
            TDSModel.XtransStatusReason.C10StolenCard.value(),
            TDSModel.XtransStatusReason.C11SupectFraud.value(),
            TDSModel.XtransStatusReason.C12TxNotPermitted.value(),
            TDSModel.XtransStatusReason.C13CardNotEnrolled.value(),
            TDSModel.XtransStatusReason.C14TxTimedOutACS.value(),
            TDSModel.XtransStatusReason.C15LowConfidence.value(),
            TDSModel.XtransStatusReason.C16MedConfidence.value(),
            TDSModel.XtransStatusReason.C17HighConfidence.value(),
            TDSModel.XtransStatusReason.C18VeryHighConfidence.value(),
            TDSModel.XtransStatusReason.C19ExceedsACSMaxChallenges.value(),
            TDSModel.XtransStatusReason.C20NonPaymentTransactionNotSupported.value(),
            TDSModel.XtransStatusReason.C213RITransactionNotSupported.value(),
            TDSModel.XtransStatusReason.C22ACSTechnicalIssue.value(),
            TDSModel.XtransStatusReason.C23DecoupledAuthenticationRequired.value(),
            TDSModel.XtransStatusReason.C24DecoupledAuthMaxExpiryTimeExceeded.value(),
            TDSModel.XtransStatusReason.C25DecoupledAuthWasProvidedInsufficientTime.value(),
            TDSModel.XtransStatusReason.C26AuthenticationAttemptedButNotPerformed.value()
    ));

    public DirectoryService220() {
        super();
        mse = MessageService220.getInstance();
        initFilters();
    }

    /**
     * For forwarded messsages.
     * key = MSGType + fieldname + Device + tdsMessageBase cat +
     * key2 = MSGType + fieldname + Device + tdsMessageBase cat +status
     * key3 = MSGType + fieldname  (for all)
     * <p>
     * Filter to pass through fields on condition.
     */
    void initFilters() {
        filters = new ArrayList<>();
        filters.addAll(FiltersProvider210.getFilters());
        filters.addAll(FiltersProvider220.getFilters());
    }

    public MessageService<?> getMessageService() {
        return mse;
    }

    protected Set<String> getBrowserColorDepthValidValues() {
        return browserColorDepthValidValues;
    }

    @Override
    protected List<Error> validateAReqEmv(TDSMessage aReq, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();
        mse.validateCommons(aReq, errors);
        if (errors.size() > 0) {
            return errors;
        }

        boolean isBrowser = DeviceChannel.cChannelBrow02.value().equals(aReq.getDeviceChannel());
        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.value().equals(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.value().equals(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.value().equals(aReq.getMessageCategory());

        if (Misc.isNotNullOrEmpty(aReq.getThreeDSRequestorDecMaxTime()) &&
            "10080".compareTo(aReq.getThreeDSRequestorDecMaxTime()) < 0) {
            errors.add(
                    new Error(ErrorCode.cInvalidFieldFormat203, "threeDSRequestorDecMaxTime", "exceeds maximum value"));
        }

        //for backward compatibility with the existing code validating missing recurringFrequency because of other reason.
        //also much better to separate the validation logic for each field in contrast of mixing different fields in nested IFs.
        if ((isPa || isNpa) &&
            (isAppDeviceChannel || isBrowser || is3RIDeviceChannel) &&
            ("02".equals(aReq.getThreeDSRequestorAuthenticationInd()) || "03".equals(aReq.getThreeDSRequestorAuthenticationInd())) &&
            Misc.isNullOrEmpty(aReq.getRecurringFrequency())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "recurringFrequency", "is missing"));
        } else if (is3RIDeviceChannel && ("02".equals(aReq.getThreeRIInd()) || "01".equals(aReq.getThreeRIInd())) &&
            Misc.isNullOrEmpty(aReq.getRecurringFrequency())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "recurringFrequency", "is missing"));
        }

        //for backward compatibility with the existing code validating missing recurringExpiry because of other reason.
        //also much better to separate the validation logic for each field in contrast of mixing different fields in nested IFs.
        if ((isPa || isNpa) &&
            (isAppDeviceChannel || isBrowser || is3RIDeviceChannel) &&
            ("02".equals(aReq.getThreeDSRequestorAuthenticationInd()) || "03".equals(aReq.getThreeDSRequestorAuthenticationInd())) &&
            Misc.isNullOrEmpty(aReq.getRecurringExpiry())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "recurringExpiry", "is missing"));
        } else if (is3RIDeviceChannel && ("02".equals(aReq.getThreeRIInd()) || "01".equals(aReq.getThreeRIInd())) &&
            Misc.isNullOrEmpty(aReq.getRecurringExpiry())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "recurringExpiry", "is missing"));
        }

        if (isBrowser && Misc.isNullOrEmpty(aReq.getBrowserLanguage())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "browserLanguage", "is missing"));
        }

        if (isNpa && Arrays.asList("01", "02", "06", "07", "08", "09", "11").contains(aReq.getThreeRIInd())) {
            if (Misc.isNullOrEmpty(aReq.getPurchaseAmount())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseAmount", "is missing"));
            }
            if (Misc.isNullOrEmpty(aReq.getPurchaseCurrency())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseCurrency", "is missing"));
            }
            if (Misc.isNullOrEmpty(aReq.getPurchaseExponent())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseExponent", "is missing"));
            }
            if (Misc.isNullOrEmpty(aReq.getPurchaseDate())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseDate", "is missing"));
            }
        }

        if (isBrowser && aReq.getBrowserJavascriptEnabled() != null && aReq.getBrowserJavascriptEnabled() && Misc.isNullOrEmpty(aReq.getBrowserColorDepth())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "browserColorDepth", "is missing"));
        } else {
            if (aReq.getIncomingJsonMessage().isEmptyOrNullField("browserColorDepth")) {
                errors.add(ErrorUtils.emptyOrNullFieldError("browserColorDepth"));
            }
        }

        if (Misc.isNotNullOrEmpty(aReq.getBrowserColorDepth()) && !getBrowserColorDepthValidValues().contains(aReq.getBrowserColorDepth())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "browserColorDepth", "is invalid. Value should be one of these: " + browserColorDepthValidValues));
        }

        Integer maxtime = Misc.parseIntBoxed(aReq.getThreeDSRequestorDecMaxTime());
        if (maxtime != null && (maxtime < 1 || maxtime > 10080)) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "threeDSRequestorDecMaxTime",
                                 "threeDSRequestorDecMaxTime invalid duration"));
        }

        if (StringUtils.isNotEmpty(aReq.getThreeDSReqAuthMethodInd())) {
            if (!validThreeDSReqAuthMethodIndValues.contains(aReq.getThreeDSReqAuthMethodInd())) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.threeDSReqAuthMethodInd, "is invalid"));
            }
        }
        //TC_DS_10468_011
        if (Misc.isNotNullOrEmpty(aReq.getWhiteListStatus()) && Misc.isNullOrEmpty(aReq.getWhiteListStatusSource())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "whiteListStatusSource", "is missing"));
        }

        if (isBrowser && (isPa || isNpa)) {
            if (aReq.getJsonObject() != null) {
                // XSD validation does not validate that boolean case seemingly need manual validation
                // only binded value remains null...
                JsonObject jsonObject = aReq.getJsonObject();
                JsonValue jv = jsonObject.get("browserJavascriptEnabled");
                if (jv == null || (jv.getValueType() == ValueType.STRING && Misc.isNullOrEmpty(jsonObject.getString("browserJavascriptEnabled")))) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "browserJavascriptEnabled", "is missing"));
                } else if (!(jv.getValueType() == ValueType.FALSE || jv.getValueType() == ValueType.TRUE)) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "browserJavascriptEnabled",
                                         "is invalid format shall be boolean true or false"));
                }
            }
        }
        if (TDSModel.XDSRequestorAuthenticationInd.C03Instalment.value.equals(
                aReq.getThreeDSRequestorAuthenticationInd())) {
            if (Misc.isNullOrEmpty(aReq.getPurchaseInstalData())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseInstalData", "is missing"));
            }
        }
        if (Misc.isNotNullOrEmpty(aReq.getThreeDSRequestorDecMaxTime()) && Misc.isNullOrEmpty(aReq.getThreeDSRequestorDecReqInd())) {
            aReq.setThreeDSRequestorDecReqInd("N");
        }
        if (Misc.isNullOrEmpty(aReq.getThreeDSRequestorDecMaxTime()) && "Y".equals(aReq.getThreeDSRequestorDecReqInd())) {
            errors.add(
                    new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "threeDSRequestorDecMaxTime", "is missing"));
        }
        if (!isValidWhiteListStatus(aReq)) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "whiteListStatus", "value is not allowed"));
        }
        if (aReq.getPayTokenInd() != null && aReq.getPayTokenInd()) {
            if (Misc.isNullOrEmpty(aReq.getPayTokenSource())) {
                errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "payTokenSource", "is missing"));
            }
        }
        if (isBrowser && Misc.isNullOrEmpty(aReq.getBrowserUserAgent())) {
            errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "browserUserAgent", "is missing"));
        }

        JsonMessage aReqJsonMessage = aReq.getIncomingJsonMessage();
        if (aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSRequestorDecMaxTime)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSRequestorDecMaxTime));
        }
        if (aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSRequestorDecReqInd)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSRequestorDecReqInd));
        }
        if (aReqJsonMessage.isEmptyOrNullField(FieldName.whiteListStatus)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.whiteListStatus));
        }

        errors.addAll(super.validateAReqEmv(aReq, validationContext));
        return errors;
    }

    protected void validateMissingPurchaseCurrency(TDSMessage aReq, List<Error> errors) {
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());
        if (isNpa && !(Arrays.asList("02", "03").contains(aReq.getThreeDSRequestorAuthenticationInd()) ||
                  Arrays.asList("01", "02", "06", "07", "08", "09", "11").contains(aReq.getThreeRIInd()))) {
            if (aReq.getIncomingJsonMessage().isEmptyOrNullField(FieldName.purchaseCurrency)) {
                errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.purchaseCurrency));
            }
        }
    }

    protected List<Error> postValidateAReqDSConditional(TDSMessage aReq) {
        List<Error> errors = new ArrayList<>();

        boolean isBrowser = DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());

        JsonMessage aReqJsonMessage = aReq.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowser) &&
            (isPa || isNpa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSReqAuthMethodInd)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSReqAuthMethodInd));
        }
        errors.addAll(super.postValidateAReqDSConditional(aReq));
        return errors;
    }

    @Override
    protected void validateAReqBrowserVersionSpecial(List<Error> errors, TDSMessage aReq) {
        if (Boolean.TRUE.equals(aReq.getBrowserJavascriptEnabled())) {
            super.validateAReqBrowserVersionSpecial(errors, aReq);
        }
    }

    @Override
    protected boolean validateThreeRIValue(String value) {
        return TDSModel.X3RIInd22.contains(value);
    }

    @Override
    protected String getMessageVersion() {
        return MessageVersion.V2_2_0.value();
    }

    @Override
    public void validate3RIDeviceChannel(boolean isPa, List<Error> errors) {
        // Do nothing for 2.2
    }

    @Override
    protected List<Error> validateAResEmv(TDSMessage aRes, TDSMessage aReq, ValidationContext validationContext) {
        List<Error> errors = super.validateAResEmv(aRes, aReq, validationContext);
        // decoupled authentication
        if ("Y".equals(aRes.getAcsDecConInd())) {
            if (!TDSModel.XtransStatus.D.isEqual(aRes.getTransStatus())) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "acsDecConInd, transStatus",
                                     "combination is invalid by 3DS2.2 spec"));
            }
            if (Misc.isNullOrEmpty(aRes.getCardholderInfo())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "cardholderInfo",
                                     "required as acsDecConInd is Y,  but is missing "));
            }
        }

        if (TDSModel.XtransStatus.D.isEqual(aRes.getTransStatus())) {
            if (Misc.isNullOrEmpty(aRes.getAcsChallengeMandated())) {
                errors.add(
                        new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "acsChallengeMandated", "is missing"));
            }
            if (!TDSModel.MessageVersion.V2_3_0.value().equals(aRes.getMessageVersion())) {
                if (Misc.isNullOrEmpty(aRes.getAuthenticationType())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "authenticationType", "is missing"));
                } else if (!isValidAuthenticationType(aRes.getAuthenticationType(), validationContext)) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "authenticationType",
                            "is invalid value by DS rules"));
                }
            }
        }

        if (Misc.isNotNullOrEmpty(aRes.getWhiteListStatus()) && Misc.isNullOrEmpty(aRes.getWhiteListStatusSource())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "whiteListStatusSource", "is missing"));
        }

        if (Misc.isNotNullOrEmpty(aRes.getWhiteListStatusSource()) && !Misc.in(aRes.getWhiteListStatusSource(),
                                                                    new String[]{"01", "02", "03"})) {//this validation is not precisely correct. it should validate for values between 04 and 79
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "whitelistStatusSource",
                                 "invalid value, reserved for future use"));
        }

        boolean app = DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean brow = DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean ri3 = DeviceChannel.cChannel3RI03.isEqual(aReq.getDeviceChannel());
        boolean pa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());
        boolean npa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());

        if (TDSModel.XtransStatus.I.isEqual(aRes.getTransStatus()) &&
            ObjectUtils.isNotIn(aReq.getTdsRequestorChallengeInd(), "05", "06", "07")) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "transStatus", "invalid value"));
        }

        JsonMessage rawJson = aRes.getIncomingJsonMessage();
        if (rawJson.isEmptyOrNullField("whiteListStatus")) {
            errors.add(ErrorUtils.emptyOrNullFieldError("whiteListStatus"));
        }

        if (TDSModel.XtransStatus.C.isEqual(aRes.getTransStatus())) {
            if ((app || brow) && (pa || npa) && "06".equals(aReq.getTdsRequestorChallengeInd())) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "transStatus",
                                     " invalid value 'C' for threeDSRequestorChallengeInd '06'"));
            }
            if (ri3 && pa) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.transStatus,
                        "is invalid value by DS rules"));
            }
        }

        return errors;
    }

    protected List<Error> postValidateAResDSConditional(TDSMessage aRes, TDSMessage aReq) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.isEqual(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());

        JsonMessage aResJsonMessage = aRes.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isPa &&
            TDSModel.XtransStatus.I.isEqual(aRes.getTransStatus()) &&
            aResJsonMessage.isEmptyOrNullField(FieldName.authenticationValue)) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.authenticationValue));
        }

        errors.addAll(super.postValidateAResDSConditional(aRes, aReq));

        return errors;
    }
    @Override
    public void createAndAddRangeData(CardRange cardRange, WebContext wctx) {
        XcardRangeData rangeData = createRangeData(cardRange, wctx,true);

        // Acs Info Indicator: comma separated values if is not blank
        if (Misc.isNotNullOrEmpty(cardRange.getAcsInfoInd())) {
            for (String part : cardRange.getAcsInfoInd().split(",")) {
                rangeData.getAcsInfoInd().add(part);
            }
        }
        TDSMessage pResMessage = pResMessageCache.get(cardRange.getPaymentSystemId());
        if (pResMessage == null) {
            pResMessage = mse.createNewMessage();
            pResMessageCache.put(cardRange.getPaymentSystemId(), pResMessage);
        }
        pResMessage.getCardRangeData().add(rangeData);
    }

    @Override
    protected List<Error> validateRReqEmv(TDSMessage rReq, TDSRecord record, ValidationContext validationContext) {
        List<Error> errors = super.validateRReqEmv(rReq, record, validationContext);
        if (!(TDSModel.XtransStatus.Y.value().equals(rReq.getTransStatus()) ||
              TDSModel.XtransStatus.N.value().equals(rReq.getTransStatus()))) {
            if (rReq.getAuthenticationType() != null && Misc.isNullOrEmpty(rReq.getAuthenticationType())) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "authenticationType",
                                     "is empty but invalid per Req 309"));
            }
        }
        if (Misc.isNotNullOrEmpty(rReq.getWhiteListStatus()) && !TDSModel.XwhiteListStatus.contains(rReq.getWhiteListStatus())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "whitelistStatus", "is invalid"));
        }

        if (Misc.isNotNullOrEmpty(rReq.getWhiteListStatus()) && Misc.isNullOrEmpty(rReq.getWhiteListStatusSource())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "whitelistStatusSource", "is missing"));
        }

        //I think this is just fine the way it is
        if (TDSModel.XtransStatusReason.C14TxTimedOutACS.value.equals(rReq.getTransStatusReason())) {
            if (Misc.isNullOrEmpty(rReq.getChallengeCancel())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "challengeCancel",
                                     "is missing but required for transStatusReason 14"));
            } else if (!Misc.in(rReq.getChallengeCancel(), new String[]{"04", "05"})) {
                errors.add(
                        new Error(ErrorCode.cInvalidFieldFormat203, "challengeCancel", "invalid value. Must be 04|05"));
            }
        }

        JsonMessage rawJson = rReq.getIncomingJsonMessage();
        if (rawJson.isEmptyOrNullField("whiteListStatus")) {
            errors.add(ErrorUtils.emptyOrNullFieldError("whiteListStatus"));
        }

        //Table A15 page 253
        if (!Misc.in(rReq.getTransStatus(), new String[]{N.value(), Y.value(), U.value(), A.value(), R.value()})) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "transStatus", "is invalid"));
        }

        return errors;
    }

    protected List<Error> postValidateRReqDSConditional(TDSMessage rReq, TDSRecord record) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(record.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.isEqual(record.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.isEqual(record.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(record.getMessageCategory());

        JsonMessage rReqJsonMessage = rReq.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isPa &&
            TDSModel.XtransStatus.I.isEqual(rReq.getTransStatus()) &&
            rReqJsonMessage.isEmptyOrNullField(FieldName.authenticationValue)) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.authenticationValue));
        }

        errors.addAll(super.postValidateRReqDSConditional(rReq, record));

        return errors;
    }
    private boolean isValidWhiteListStatus(TDSMessage aReq) {
        String whiteListStatus = Optional.ofNullable(aReq.getWhiteListStatus()).orElse("");
        return whiteListStatus.isEmpty() || TDSModel.XwhiteListStatus.Y.name().equals(whiteListStatus) ||
               TDSModel.XwhiteListStatus.N.name().equals(whiteListStatus);
    }

    @Override
    protected Set<String> getTransStatusValuesARes() {
        return transStatusValues22ARes;
    }

    @Override
    protected boolean isValidTransStatusReason(String transStatusReason) {
        return transStatusReasonList22.contains(transStatusReason);
    }
}
