package com.modirum.ds.web.validators;

import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XmerchantRiskIndicator;

import java.util.Optional;

public class MerchantRiskIndicatorValidator {

    private static final String NO_CARDS = "00";
    private static MerchantRiskIndicatorValidator instance;

    public static MerchantRiskIndicatorValidator getInstance() {
        if (instance == null) {
            instance = new MerchantRiskIndicatorValidator();
        }
        return instance;
    }

    /**
     * Constructor.
     */
    private MerchantRiskIndicatorValidator() {
    }

    public Optional<Error> isGiftCardCurrValid(TDSMessage aReq) {
        XmerchantRiskIndicator xmerchantRiskIndicator = Optional.ofNullable(aReq.getMerchantRiskIndicator()).orElse(
                emptyXmerchantRisk());
        Integer giftCardCount = Integer.valueOf(
                Optional.ofNullable(xmerchantRiskIndicator.getGiftCardCount()).orElse(NO_CARDS));
        String giftCardCurr = xmerchantRiskIndicator.getGiftCardCurr();
        if (giftCardCount > 0) {
            Integer giftCardCurrency;
            try {
                giftCardCurrency = Integer.valueOf(giftCardCurr);
            } catch (NumberFormatException e) {
                Error giftCardCurrInvalid = new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "giftCardCurr",
                                                      "Invalid or missing giftCardCurr value '" + giftCardCurr + "'");
                return Optional.of(giftCardCurrInvalid);
            }

            Integer rangeFrom = TDSModel.TableA6Currencies.C901.intValue();
            Integer rangeTo = TDSModel.TableA6Currencies.C999.intValue();
            if (giftCardCurrency >= rangeFrom && giftCardCurrency <= rangeTo) {
                Error giftCardCurrError = new Error(TDSModel.ErrorCode.cInvalidISOCode304, "giftCardCurr",
                                                    "Prohibited value '" + giftCardCurrency + "'");
                return Optional.of(giftCardCurrError);
            }
        }
        return Optional.empty();
    }

    private XmerchantRiskIndicator emptyXmerchantRisk() {
        return new XmerchantRiskIndicator();
    }
}
