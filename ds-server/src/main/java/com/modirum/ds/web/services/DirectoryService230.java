package com.modirum.ds.web.services;

import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.FieldName;
import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.model.TDSModel.XmessageType;
import com.modirum.ds.model.TDSModel.XtrustListStatus;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.MessageService230;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XacsProtocolVersion;
import com.modirum.ds.tds21msgs.XcardRangeData;
import com.modirum.ds.tds21msgs.XmerchantList;
import com.modirum.ds.tds21msgs.XmultiTransaction;
import com.modirum.ds.tds21msgs.Xranges;
import com.modirum.ds.util.ErrorUtils;
import com.modirum.ds.utils.ISO3166;
import com.modirum.ds.utils.ISO4217;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.context.WebContext;
import com.modirum.ds.web.filters.ExcludeFiltersProvider230;
import com.modirum.ds.web.filters.FiltersProvider210;
import com.modirum.ds.web.filters.FiltersProvider220;
import com.modirum.ds.web.filters.FiltersProvider230;
import com.modirum.ds.web.validators.ValidationContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class DirectoryService230 extends DirectoryService220 {

    private static final Logger log = LoggerFactory.getLogger(DirectoryService230.class);

    private static final Set<String> browserColorDepthValidValues = new HashSet<>(
            IntStream.range(1, 100).mapToObj(Integer::toString).collect(Collectors.toList()));

    protected static final Set<String> transStatusReasonList23 = new HashSet<>(Arrays.asList(
            TDSModel.XtransStatusReason.C01AuthFailed.value(),
            TDSModel.XtransStatusReason.C02UnknDevice.value(),
            TDSModel.XtransStatusReason.C03UnsupDevice.value(),
            TDSModel.XtransStatusReason.C04ExceedsAuthFreq.value(),
            TDSModel.XtransStatusReason.C05ExpiredCard.value(),
            TDSModel.XtransStatusReason.C06InvalidPan.value(),
            TDSModel.XtransStatusReason.C07InvalidTx.value(),
            TDSModel.XtransStatusReason.C08NoCardrecord.value(),
            TDSModel.XtransStatusReason.C09SecurityFailure.value(),
            TDSModel.XtransStatusReason.C10StolenCard.value(),
            TDSModel.XtransStatusReason.C11SupectFraud.value(),
            TDSModel.XtransStatusReason.C12TxNotPermitted.value(),
            TDSModel.XtransStatusReason.C13CardNotEnrolled.value(),
            TDSModel.XtransStatusReason.C14TxTimedOutACS.value(),
            TDSModel.XtransStatusReason.C15LowConfidence.value(),
            TDSModel.XtransStatusReason.C16MedConfidence.value(),
            TDSModel.XtransStatusReason.C17HighConfidence.value(),
            TDSModel.XtransStatusReason.C18VeryHighConfidence.value(),
            TDSModel.XtransStatusReason.C19ExceedsACSMaxChallenges.value(),
            TDSModel.XtransStatusReason.C20NonPaymentTransactionNotSupported.value(),
            TDSModel.XtransStatusReason.C213RITransactionNotSupported.value(),
            TDSModel.XtransStatusReason.C22ACSTechnicalIssue.value(),
            TDSModel.XtransStatusReason.C23DecoupledAuthenticationRequired.value(),
            TDSModel.XtransStatusReason.C24DecoupledAuthMaxExpiryTimeExceeded.value(),
            TDSModel.XtransStatusReason.C25DecoupledAuthWasProvidedInsufficientTime.value(),
            TDSModel.XtransStatusReason.C26AuthenticationAttemptedButNotPerformed.value(),
            TDSModel.XtransStatusReason.C27PreferredAuthenticationMethodNotSupported.value(),
            TDSModel.XtransStatusReason.C28ValidationOfContentSecurityPolicyFailed.value()
    ));

    public DirectoryService230() {
        super();
        mse = MessageService230.getInstance();
        initFilters();
    }

    @Override
    void initFilters() {
        filters = new ArrayList<>();
        filters.addAll(FiltersProvider210.getFilters());
        filters.addAll(FiltersProvider220.getFilters());
        filters.addAll(FiltersProvider230.getFilters());
        filters.removeAll(ExcludeFiltersProvider230.getFilters());
    }

    @Override
    protected String getMessageVersion() {
        return TDSModel.MessageVersion.V2_3_0.value();
    }

    @Override
    protected Set<String> getBrowserColorDepthValidValues() {
        return browserColorDepthValidValues;
    }

    protected List<String> getDSProtocolVersions(WebContext webContext) {
        String versionEnabled = webContext.getStringSetting(DsSetting.DS_VERSIONS_ENABLED.getKey());
        return Arrays.asList(versionEnabled.split(" "));
    }

    @Override
    protected void setDSProtocolVersions(TDSMessage pRes, WebContext webContext) {
        pRes.getDsProtocolVersions().addAll(getDSProtocolVersions(webContext));
    }

    @Override
    protected void setReadOrder(TDSMessage pRes) {
        pRes.setReadOrder(TDSModel.XReadOrder.C01FIFO.value());
    }

    private XacsProtocolVersion generateXacsProtocolVersion(CardRange cardRange, List<String> cardRangeAcsInfoIndList,
                                                            String version) {
        XacsProtocolVersion acsProtocolVersion = new XacsProtocolVersion();
        acsProtocolVersion.setVersion(version);
        if (CollectionUtils.isNotEmpty(cardRangeAcsInfoIndList)) {
            acsProtocolVersion.getAcsInfoInd().addAll(cardRangeAcsInfoIndList);
        }
        if (cardRange.isIncludeThreeDSMethodURL()) {
            acsProtocolVersion.setThreeDSMethodURL(cardRange.getMethodURL());
        }
        return acsProtocolVersion;
    }

    private List<String> getAcsInfoInd(XacsProtocolVersion rangeData) {
        // this is needed because getAcsInfoInd() will assign a new array to acsInfoInd if it is currently null
        List<String> acsInfoInd = null;
        try {
            Field acsInfoIndField = XacsProtocolVersion.class.getDeclaredField("acsInfoInd");
            acsInfoIndField.setAccessible(true);
            acsInfoInd = (List<String>) acsInfoIndField.get(rangeData);
        } catch (Exception e) {
            log.error("Failed to get acsInfoInd of acsProtocolVersion", e);
        }
        return acsInfoInd != null ? acsInfoInd : Collections.emptyList();
    }

    @Override
    public void createAndAddRangeData(CardRange cardRange, WebContext wctx) {
        XcardRangeData rangeData = createRangeData(cardRange, wctx,true);

        TDSMessage pResMessage = pResMessageCache.get(cardRange.getPaymentSystemId());
        if (pResMessage == null) {
            pResMessage = mse.createNewMessage();
            pResMessageCache.put(cardRange.getPaymentSystemId(), pResMessage);
        }

        boolean matchFound = false;
        for (XcardRangeData existingCardRangeData: pResMessage.getCardRangeData()) {

            boolean acsVersionsMatch = true;
            if (rangeData.getAcsProtocolVersions().size() != existingCardRangeData.getAcsProtocolVersions().size()) {
                acsVersionsMatch = false;
            } else {
                for (int i = 0, size = rangeData.getAcsProtocolVersions().size(); i < size; i++) {
                    XacsProtocolVersion xacsProtocolVersion = rangeData.getAcsProtocolVersions().get(i);
                    XacsProtocolVersion existingXacsProtocolVersion = existingCardRangeData.getAcsProtocolVersions().get(i);
                    if (!StringUtils.equals(xacsProtocolVersion.getVersion(),
                            existingXacsProtocolVersion.getVersion()) ||
                        !StringUtils.equals(xacsProtocolVersion.getThreeDSMethodURL(),
                                existingXacsProtocolVersion.getThreeDSMethodURL()) ||
                        !CollectionUtils.isEqualCollection(getAcsInfoInd(xacsProtocolVersion),
                                getAcsInfoInd(existingXacsProtocolVersion))) {
                        acsVersionsMatch = false;
                        break;
                    }
                }
            }

            if (acsVersionsMatch &&
                StringUtils.equals(rangeData.getActionInd(), existingCardRangeData.getActionInd()) &&
                CollectionUtils.isEqualCollection(rangeData.getDsProtocolVersions(),
                        existingCardRangeData.getDsProtocolVersions())) {
                matchFound = true;
                existingCardRangeData.getRanges().addAll(rangeData.getRanges());
            }
        }
        if (!matchFound) {
            pResMessage.getCardRangeData().add(rangeData);
        }
    }

    @Override
    protected XcardRangeData createRangeData(CardRange cardRange, WebContext webContext,  boolean add) {
        XcardRangeData rangeData = new XcardRangeData();

        Xranges range = new Xranges();

        if (issuerService.isBINRangeMode() || cardRange.isRangeMode()) {
            cardRange.setRangeMode();
            range.setStartRange(cardRange.getStart());
            range.setEndRange(cardRange.getEnd());
        } else {
            if (cardRange.isRangeMode()) {
                cardRange.setEnd(null);
            }
            range.setStartRange(Misc.padCutStringRight(cardRange.getBin(), '0', 16));
            range.setEndRange(Misc.padCutStringRight(cardRange.getBin(), '9', 16));
        }
        rangeData.getRanges().add(range);

        // DS protocol version
        rangeData.getDsProtocolVersions().addAll(getDSProtocolVersions(webContext));

        // ACS protocol version
        List<String> cardRangeAcsInfoIndList = Misc.isNotNullOrEmpty(cardRange.getAcsInfoInd2_3_0()) ?
                Arrays.asList(cardRange.getAcsInfoInd2_3_0().split(",")) : null;
        for (TDSModel.MessageVersion version: TDSModel.MessageVersion.getAllVersionsBetween(
                cardRange.getStartProtocolVersion(), cardRange.getEndProtocolVersion())) {
            rangeData.getAcsProtocolVersions().add(generateXacsProtocolVersion(cardRange, cardRangeAcsInfoIndList,
                    version.value()));
        }

        rangeData.setActionInd(add ? ACTION_IND_ADD : ACTION_IND_DELETE);
        return rangeData;
    }

    @Override
    protected void validateCardHolderName(TDSMessage req, List<Error> errors) {
        // no additional validation for 2.3.0
    }

    protected void validateDeviceBindingStatusSource(TDSMessage req, List<Error> errors) {
        String deviceBindingStatus = req.getDeviceBindingStatus();
        String deviceBindingStatusSource = req.getDeviceBindingStatusSource();

        if (req.getIncomingJsonMessage().isEmptyOrNullField(FieldName.deviceBindingStatus)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.deviceBindingStatus));
        }

        if (Misc.isNotNullOrEmpty(deviceBindingStatus) && Misc.isNullOrEmpty(deviceBindingStatusSource)) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.deviceBindingStatusSource));
        } else if (Misc.isNullOrEmpty(deviceBindingStatus) && req.getIncomingJsonMessage().isEmptyOrNullField(FieldName.deviceBindingStatusSource)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.deviceBindingStatusSource));
        }

        if (Misc.isNotNullOrEmpty(deviceBindingStatusSource) && !(Misc.in(deviceBindingStatusSource, new String[]{"01", "02", "03"}) ||
                isStringBetween80to99(deviceBindingStatusSource))) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.deviceBindingStatusSource,
                    "invalid or reserved value"));
        }
    }

    protected void validateTrustList(TDSMessage req, List<Error> errors) {

        String trustListStatus = req.getTrustListStatus();
        String trustListStatusSource = req.getTrustListStatusSource();

        JsonMessage reqJsonMessage = req.getIncomingJsonMessage();

        if (reqJsonMessage.isEmptyOrNullField(FieldName.trustListStatus)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.trustListStatus));
        }

        if (Misc.isNotNullOrEmpty(trustListStatus) && Misc.isNullOrEmpty(trustListStatusSource)) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.trustListStatusSource));
        } else if (req.getIncomingJsonMessage().isEmptyOrNullField(FieldName.trustListStatusSource)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.trustListStatusSource));
        }

        if (Misc.isNotNullOrEmpty(trustListStatusSource) && !(Misc.in(trustListStatusSource, new String[]{"01", "02", "03"}) ||
                isStringBetween80to99(trustListStatusSource))) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "trustListStatusSource",
                    "invalid or reserved value"));
        }

    }

    @Override
    protected List<Error> postValidateAReqDSConditional(TDSMessage aReq) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean isBrowserDeviceChannel = TDSModel.DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.isEqual(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());

        JsonMessage aReqJsonMessage = aReq.getIncomingJsonMessage();

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) && (isPa || isNpa)) {
            if(aReqJsonMessage.isEmptyOrNullField(FieldName.taxId)) {
                errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.taxId));
            }
            String taxId = aReq.getTaxId();
            if (taxId != null && taxId.length() > 45) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "FieldName.acceptLanguage",
                        "length " + taxId.length() + " exceeds max allowed 45"));
            }
        }

        errors.addAll(super.postValidateAReqDSConditional(aReq));
        return errors;
    }

    @Override
    protected List<Error> validateAReqEmv(TDSMessage aReq, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();
        mse.validateCommons(aReq, errors);
        if (errors.size() > 0) {
            return errors;
        }

        boolean isBrowser = TDSModel.DeviceChannel.cChannelBrow02.value().equals(aReq.getDeviceChannel());
        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.value().equals(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.value().equals(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.value().equals(aReq.getMessageCategory());

        JsonMessage aReqJsonMessage = aReq.getIncomingJsonMessage();

        if (isBrowser && (isNpa || isPa) && aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSMethodId)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSMethodId));
        }

        if (is3RIDeviceChannel && (isNpa || isPa) && aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSRequestorChallengeInd)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSRequestorChallengeInd));
        }

        if (isBrowser && (isPa || isNpa) && aReqJsonMessage.isEmptyOrNullField(FieldName.acceptLanguage)) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.acceptLanguage));
        }
        else if (isBrowser && (isPa || isNpa) && !aReqJsonMessage.isArray(FieldName.acceptLanguage)) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.acceptLanguage, "invalid value"));
        } else if (isBrowser && (isPa || isNpa)) {
            List<String> acceptLanguage = aReq.getAcceptLanguage();
            int size = acceptLanguage.size();
            for (int i = 0; i < size; i++) {
                String language = acceptLanguage.get(i);
                if (Misc.isNullOrEmpty(language) || !Misc.isBCP47LanguageTag(language)) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.acceptLanguage,
                            "Index " + i + " contains an invalid value"));
                } else if (language.length() > 100) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "FieldName.acceptLanguage",
                            "Index " + i + " length " + language.length() + " exceeds max allowed 99"));
                }
            }
            if (size > 99) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.acceptLanguage,
                        "Invalid size. Cannot exceed 99 elements"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String acquirerCountryCode = aReq.getAcquirerCountryCode();
            if (Misc.isNullOrEmpty(acquirerCountryCode)) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.acquirerCountryCode));
            } else if (acquirerCountryCode.length() != 3 || ISO3166.findFromNumeric(Short.parseShort(acquirerCountryCode)) == null) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.acquirerCountryCode, "invalid value"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String acquirerCountryCodeSource = aReq.getAcquirerCountryCodeSource();
            if (Misc.isNullOrEmpty(acquirerCountryCodeSource)) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.acquirerCountryCodeSource));
            } else if (!(Misc.in(acquirerCountryCodeSource, new String[]{"01", "02"}) ||
                    isStringBetween80to99(acquirerCountryCodeSource))) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.acquirerCountryCodeSource,
                        "invalid or reserved value"));
            }
        }

        if (isAppDeviceChannel && (isPa || isNpa)) {
            String appIp = aReq.getAppIp();
            if (Misc.isNullOrEmpty(appIp)) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.appIp));
            } else if (!(Misc.isIpV4Address(appIp) || Misc.isIpV6Address(appIp))) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.appIp,
                        "invalid value"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String deviceBindingStatus = aReq.getDeviceBindingStatus();
            if (Misc.isNotNullOrEmpty(deviceBindingStatus) && !TDSModel.XYesNo.contains(deviceBindingStatus)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceBindingStatus", "invalid value"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            validateDeviceBindingStatusSource(aReq, errors);
        }

        if (isAppDeviceChannel && (isPa || isNpa) && aReq.getDeviceRenderOptions() != null) {
            if (aReqJsonMessage.isEmptyOrNullField("deviceRenderOptions.sdkAuthenticationType")) {
                errors.add(ErrorUtils.fieldMissingError("deviceRenderOptions.sdkAuthenticationType"));
            } else if (!aReqJsonMessage.isArray("deviceRenderOptions.sdkAuthenticationType")) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceRenderOptions.sdkAuthenticationType",
                        "invalid value"));
            } else {
                List<String> sdkAuthenticationType = aReq.getDeviceRenderOptions().getSdkAuthenticationType();
                if (CollectionUtils.isEmpty(sdkAuthenticationType)) {
                    errors.add(ErrorUtils.fieldMissingError("deviceRenderOptions.sdkAuthenticationType"));
                } else if (sdkAuthenticationType.size() > 99) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceRenderOptions.sdkAuthenticationType",
                            "Invalid size. Cannot exceed 99 elements"));
                } else {
                    List<String> validValues = Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08",
                            "09", "10", "11", "12", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89",
                            "90", "91", "92", "93", "94", "95", "96", "97", "98", "99");
                    if (!validValues.containsAll(sdkAuthenticationType)) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceRenderOptions.sdkAuthenticationType",
                                "invalid or reserved value"));
                    }
                }
            }

            if (aReqJsonMessage.isArray("deviceRenderOptions.sdkUiType")) {
                List<String> sdkUiTypes = aReq.getDeviceRenderOptions().getSdkUiType();
                if (CollectionUtils.isNotEmpty(sdkUiTypes) && "01".equals(aReq.getDeviceRenderOptions().getSdkInterface()) &&
                        sdkUiTypes.contains("05") || sdkUiTypes.contains("06")) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceRenderOptions.sdkUiType",
                            "invalid combination of sdkInterface and sdkUiType"));
                }
            }
        }

        if ((isAppDeviceChannel || isBrowser) && (isPa || isNpa) &&
                aReqJsonMessage.isEmptyOrNullField(FieldName.multiTransaction)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.multiTransaction));
        } else if ((isAppDeviceChannel || isBrowser) && (isPa || isNpa)) {
            XmultiTransaction multiTransaction = aReq.getMultiTransaction();
            if (multiTransaction != null) {
                if (!aReqJsonMessage.isObject(FieldName.multiTransaction)) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.multiTransaction, "invalid value"));
                } else {
                    List<XmerchantList> merchantList = multiTransaction.getMerchantList();
                    if (CollectionUtils.isEmpty(merchantList)) {
                        errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, "multiTransaction.merchantList",
                                "merchantList is missing"));
                    } else if (!aReqJsonMessage.isArray("multiTransaction.merchantList")) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "multiTransaction.merchantList",
                                "merchantList contains an invalid value"));
                    } else {
                        if (merchantList.size() > 50) {
                            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "multiTransaction.merchantList",
                                    "Invalid size. Cannot exceed 50 elements"));
                        } else {
                            int merchantSize = merchantList.size();
                            for (int i = 0; i < merchantSize; i++) {
                                XmerchantList merchant = merchantList.get(i);
                                String merchantNameListed = merchant.getMerchantNameListed();
                                if (Misc.isNullOrEmpty(merchantNameListed)) {
                                    errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201,
                                            "multiTransaction.merchantList.merchantNameListed",
                                            "merchantList index " + i + " merchantNameListed is missing"));
                                } else if (merchantNameListed.length() > 40) {
                                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                            "multiTransaction.merchantList.merchantNameListed",
                                            "merchantList index " + i + " merchantNameListed length " +
                                                    merchantNameListed.length() + " exceeds max allowed 40"));
                                }

                                String acquirerMerchantIdListed = merchant.getAcquirerMerchantIdListed();
                                if (aReqJsonMessage.isEmptyOrNullField(String.format("multiTransaction.merchantList.%s.acquirerMerchantIdListed", i))) {
                                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                            "multiTransaction.merchantList.acquirerMerchantIdListed",
                                            "merchantList index " + i +
                                                    " acquirerMerchantIdListed can not be empty or null if exists in json"));
                                } else if (StringUtils.length(acquirerMerchantIdListed) > 15) {
                                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                            "multiTransaction.merchantList.acquirerMerchantIdListed",
                                            "merchantList index " + i + " acquirerMerchantIdListed length " +
                                                    acquirerMerchantIdListed.length() + " exceeds max allowed 15"));
                                }

                                String merchantAmount = merchant.getMerchantAmount();
                                if (aReqJsonMessage.isEmptyOrNullField(String.format("multiTransaction.merchantList.%s.merchantAmount", i))) {
                                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                            "multiTransaction.merchantList.merchantAmount",
                                            "merchantList index " + i + " merchantAmount can not be empty or null if exists in json"));
                                } else if (StringUtils.length(merchantAmount) > 48) {
                                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                            "multiTransaction.merchantList.merchantAmount",
                                            "merchantList index " + i +
                                                    " merchantAmount length " + merchantAmount.length() + " exceeds max allowed 48"));
                                }

                                if (Misc.isNotNullOrEmpty(merchantAmount)) {
                                    String merchantCurrency = merchant.getMerchantCurrency();
                                    ISO4217 isoCurrency = null;
                                    if (Misc.isInt(merchantCurrency)) {
                                        isoCurrency = ISO4217.findFromNumeric((short) Misc.parseInt(merchantCurrency));
                                    }
                                    if (Misc.isNullOrEmpty(merchantCurrency)) {
                                        errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201,
                                                "multiTransaction.merchantList.merchantCurrency",
                                                "merchantList index " + i + " merchantCurrency is missing"));
                                    } else if (merchantCurrency.length() != 3 || !StringUtils.isNumeric(merchantCurrency) ||
                                            isoCurrency == null) {
                                        errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304,
                                                "multiTransaction.merchantList.merchantCurrency",
                                                "merchantList index " + i + " merchantCurrency invalid value"));
                                    }

                                    String merchantExponent = merchant.getMerchantExponent();
                                    if (Misc.isNullOrEmpty(merchantExponent)) {
                                        errors.add(new Error(TDSModel.ErrorCode.cRequiredFieldMissing201,
                                                "multiTransaction.merchantList.merchantExponent",
                                                "merchantList index " + i + " merchantExponent is missing"));
                                    } else if (merchantExponent.length() != 1 || !StringUtils.isNumeric(merchantExponent) ||
                                            (isoCurrency != null && isoCurrency.getCurrencyExponent() != Integer.parseInt(merchantExponent))) {
                                        errors.add(new Error(TDSModel.ErrorCode.cInvalidISOCode304,
                                                "multiTransaction.merchantList.merchantExponent",
                                                "merchantList index " + i + " merchantExponent invalid value"));
                                    }
                                } else {
                                    String merchantCurrency = merchant.getMerchantCurrency();
                                    ISO4217 isoCurrency = null;
                                    if (Misc.isInt(merchantCurrency)) {
                                        isoCurrency = ISO4217.findFromNumeric((short) Misc.parseInt(merchantCurrency));
                                    }
                                    if (aReqJsonMessage.isEmptyOrNullField(String.format("multiTransaction.merchantList.%s.merchantCurrency", i))) {
                                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                                "multiTransaction.merchantList.merchantCurrency",
                                                "merchantList index " + i + " merchantCurrency can not be empty or null if exists in json"));
                                    } else if (merchantCurrency != null && (merchantCurrency.length() != 3 ||
                                            !StringUtils.isNumeric(merchantCurrency) || isoCurrency == null)) {
                                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                                "multiTransaction.merchantList.merchantCurrency",
                                                "merchantList index " + i + " merchantCurrency invalid value"));
                                    }

                                    String merchantExponent = merchant.getMerchantExponent();
                                    if (aReqJsonMessage.isEmptyOrNullField(String.format("multiTransaction.merchantList.%s.merchantExponent", i))) {
                                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                                "multiTransaction.merchantList.merchantExponent",
                                                "merchantList index " + i + " merchantExponent can not be empty or null if exists in json"));
                                    } else if (merchantExponent != null && (merchantExponent.length() != 1 ||
                                                !StringUtils.isNumeric(merchantExponent) || (isoCurrency != null &&
                                                    isoCurrency.getCurrencyExponent() != Integer.parseInt(merchantExponent)))) {
                                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203,
                                                "multiTransaction.merchantList.merchantExponent",
                                                "merchantList index " + i + " merchantExponent invalid value"));
                                    }
                                }
                            }
                        }
                    }

                    String avValidityTime = multiTransaction.getAvValidityTime();
                    if (aReqJsonMessage.isEmptyOrNullField("multiTransaction.avValidityTime")) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "multiTransaction.avValidityTime",
                                "avValidityTime can not be empty or null if exists in json"));
                    } else if (Misc.isNotNullOrEmpty(avValidityTime) && (avValidityTime.length() > 3 || !Misc.isInt(avValidityTime))) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "multiTransaction.avValidityTime",
                                "avValidityTime invalid value"));
                    }

                    String avNumberUse = multiTransaction.getAvNumberUse();
                    if (aReqJsonMessage.isEmptyOrNullField("multiTransaction.avNumberUse")) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "multiTransaction.avNumberUse",
                                "avNumberUse can not be empty or null if exists in json"));
                    } else if (Misc.isNotNullOrEmpty(avNumberUse) && (avNumberUse.length() > 2 || !Misc.isInt(avNumberUse))) {
                        errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "multiTransaction.avNumberUse",
                                "avNumberUse invalid value"));
                    }
                }
            }
        }

        String sdkType = aReq.getSdkType();
        if (isAppDeviceChannel && (isPa || isNpa) && (TDSModel.XSdkType.C02SplitSdk.value().equals(sdkType) ||
                TDSModel.XSdkType.C03SplitSdk.value().equals(sdkType)) && Misc.isNullOrEmpty(aReq.getSdkServerSignedContent())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.sdkServerSignedContent));
        }

        if (isAppDeviceChannel && (isPa || isNpa) && !(TDSModel.XSdkType.C02SplitSdk.value().equals(sdkType) ||
                TDSModel.XSdkType.C03SplitSdk.value().equals(sdkType)) &&
                aReqJsonMessage.isEmptyOrNullField(FieldName.sdkServerSignedContent)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.sdkServerSignedContent));
        }

        if (isAppDeviceChannel && (isPa || isNpa) && Misc.isNullOrEmpty(sdkType)) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.sdkType));
        }
        if (isAppDeviceChannel && (isPa || isNpa) && Misc.isNotNullOrEmpty(sdkType) &&
                !(TDSModel.XSdkType.contains(sdkType) || isStringBetween80to99(sdkType))) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "sdkType", "invalid or reserved value"));
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String trustListStatus = aReq.getTrustListStatus();
            if (Misc.isNotNullOrEmpty(trustListStatus) && !TDSModel.XYesNo.contains(trustListStatus)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "trustListStatus",
                        "value is not allowed for " + aReq.getMessageType()));
            }
            validateTrustList(aReq, errors);
        }

        errors.addAll(super.validateAReqEmv(aReq, validationContext));
        return errors;
    }

    @Override
    protected List<Error> validateAResEmv(TDSMessage aRes, TDSMessage aReq, ValidationContext validationContext) {
        List<Error> errors = super.validateAResEmv(aRes, aReq, validationContext);

        boolean isBrowser = TDSModel.DeviceChannel.cChannelBrow02.value().equals(aReq.getDeviceChannel());
        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.value().equals(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.value().equals(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.value().equals(aReq.getMessageCategory());

        JsonMessage aResJsonMessage = aRes.getIncomingJsonMessage();

        if (isAppDeviceChannel && (isPa || isNpa) && TDSModel.XtransStatus.C.isEqual(aRes.getTransStatus()) &&
                aRes.getAcsRenderingType() != null) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.acsRenderingType));
        }
        if (isAppDeviceChannel && (isPa || isNpa) && !TDSModel.XtransStatus.C.isEqual(aRes.getTransStatus()) &&
                aResJsonMessage.isEmptyOrNullField(FieldName.acsRenderingType)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.acsRenderingType));
        }
        if (isAppDeviceChannel && (isPa || isNpa) && aRes.getAcsRenderingType() != null) {
            String acsUiTemplate = aRes.getAcsRenderingType().getAcsUiTemplate();
            if (TDSModel.XAcsInterface.C01NativeUI.value().equals(aRes.getAcsRenderingType().getAcsInterface()) &&
                    (TDSModel.ACSUIType2_3_0.C05_HTMLOther.value().equals(acsUiTemplate) ||
                    TDSModel.ACSUIType2_3_0.C06_HTMLOOB.value().equals(acsUiTemplate))) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "acsRenderingType.acsInterface",
                        "invalid combination of acsInterface and acsUiTemplate"));
            }

            if (isAppDeviceChannel && (isPa || isNpa) && StringUtils.isEmpty(aRes.getAcsRenderingType().getDeviceUserInterfaceMode())) {
                errors.add(ErrorUtils.fieldMissingError("acsRenderingType.deviceUserInterfaceMode"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String authMethod = aRes.getAuthenticationMethod();
            if (Misc.isNullOrEmpty(authMethod)) {
                if (TDSModel.XtransStatus.C.name().equals(aRes.getTransStatus()) || TDSModel.XtransStatus.D.name().equals(aRes.getTransStatus())) {
                    errors.add(ErrorUtils.fieldMissingError(FieldName.authenticationMethod));
                }
            } else if (!TDSModel.XauthenticationMethod.contains(authMethod)) {
                    errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.authenticationMethod, "invalid or reserved value"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String deviceBindingStatus = aRes.getDeviceBindingStatus();
            if (Misc.isNotNullOrEmpty(deviceBindingStatus) && !TDSModel.XDeviceBindingStatus.contains(
                    deviceBindingStatus)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceBindingStatus", "invalid value"));
            }
            validateDeviceBindingStatusSource(aRes, errors);
        }

        if (isAppDeviceChannel && (isPa || isNpa)) {
            String deviceInfoRecognisedVersion = aRes.getDeviceInfoRecognisedVersion();
            if (Misc.isNullOrEmpty(deviceInfoRecognisedVersion)) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.deviceInfoRecognisedVersion));
            } else if (deviceInfoRecognisedVersion.length() < 3) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, FieldName.deviceInfoRecognisedVersion, "invalid value"));
            }
        }

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String trustListStatus = aRes.getTrustListStatus();
            if (Misc.isNotNullOrEmpty(trustListStatus) && !TDSModel.XtrustListStatus.contains(trustListStatus)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "trustListStatus", "invalid value"));
            }
            validateTrustList(aRes, errors);
        }

        return errors;
    }

    protected List<Error> validateRReqEmv(TDSMessage rReq, TDSRecord record, ValidationContext validationContext) {
        List<Error> errors = super.validateRReqEmv(rReq, record, validationContext);

        boolean isBrowser = TDSModel.DeviceChannel.cChannelBrow02.value().equals(record.getDeviceChannel());
        boolean isAppDeviceChannel = TDSModel.DeviceChannel.cChannelApp01.value().equals(record.getDeviceChannel());
        boolean is3RIDeviceChannel = TDSModel.DeviceChannel.cChannel3RI03.value().equals(record.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.value().equals(rReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.value().equals(rReq.getMessageCategory());

        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String deviceBindingStatus = rReq.getDeviceBindingStatus();
            if (Misc.isNotNullOrEmpty(deviceBindingStatus) && !TDSModel.XDeviceBindingStatus.contains(
                    deviceBindingStatus)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "deviceBindingStatus", "invalid value"));
            }
            validateDeviceBindingStatusSource(rReq, errors);
        }
        if ((isAppDeviceChannel || isBrowser || is3RIDeviceChannel) && (isPa || isNpa)) {
            String trustListStatus = rReq.getTrustListStatus();
            if (Misc.isNotNullOrEmpty(trustListStatus) && !TDSModel.XtrustListStatus.contains(trustListStatus)) {
                errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "trustListStatus", "invalid value"));
            }
            validateTrustList(rReq, errors);
        }
        return errors;
    }

    @Override
    protected boolean isValidTransStatusReason(String transStatusReason) {
        return transStatusReasonList23.contains(transStatusReason);
    }

}
