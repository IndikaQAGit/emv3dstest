package com.modirum.ds.web.job;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class CleanUpJob implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(CleanUpJob.class);

    private final PersistenceService persistenceService;
    Context ctx;
    int months;
    boolean all;
    int cleanUpDsCr = 45;

    private File tempDir;

    /**
     * Constructor.
     */
    public CleanUpJob(PersistenceService persistenceService, int count, boolean all, Context ctx) {
        this.persistenceService = persistenceService;
        this.ctx = ctx;
        this.months = count;
        this.all = all;
        int cleanUpDsCrl = ctx.getIntSetting(DsSetting.CLEANUP_DSCR.getKey());
        if (cleanUpDsCrl > 0) {
            cleanUpDsCr = cleanUpDsCrl;
        }
    }

    @Override
    public void run() {
        try {
            cleanupRun(months, all, cleanUpDsCr);
        } catch (Exception e) {
            log.warn("Error during cleanup: " + e.getMessage());
        }
    }

    void cleanupRun(int months, boolean all, int dscr) throws Exception {
        log.info("Starting cleanup " + (all ? "all" : "chd") + " " + months + " months back");
        long s = System.currentTimeMillis();
        int total = 0;
        if (all) {
            try {
                log.info("Cleanup about to delete TDSrecords and Msgs " + months + " months back..");
                total = persistenceService.deleteTDSrecordsAndMsgs(months);
            } catch (Exception e) {
                log.error("Cleanup error ", e);
            }
        } else {
            int hours = 1;
            int cnt = 0;
            //int lpcnt=0;
            do {
                try {
                    log.info("Cleanup about to null out chd data " + months + " months and " + hours + " hours back..");
                    long sx = System.currentTimeMillis();
                    cnt = persistenceService.setChdNull(months, hours);
                    total += cnt;
                    long ex = System.currentTimeMillis();
                    if (ex - sx > 1000) // slow dd
                    {
                        hours += 2;
                    } else if (ex - sx > 700) // med db
                    {
                        hours += 4;
                    } else {
                        hours += 8;
                    }

                    if (cnt < 1 && hours > 2400) {
                        break;
                    }

                } catch (Exception e) {
                    log.error("Cleanup error ", e);
                    break;
                }

            } while (true);
        }
        long e = System.currentTimeMillis();
        log.info("Completed cleanup " + (all ? "all (deleted)" : "chd (null out)") + " " + months + " months back in " +
                 (e - s) + "ms, records affected " + total);

        boolean useDiffUpdates = "true".equals(persistenceService.getStringSetting(DsSetting.DS_SUPPORT_RANGE_DIFF_UPDATES.getKey()));
        if (useDiffUpdates && dscr > 0) {
            long back = System.currentTimeMillis() - DateUtil.DT24H * dscr;
            log.info("Cleanup about to delete CR files " + dscr + " days or more back, before " +
                     DateUtil.formatDate(new java.util.Date(back), "yyMMdd HH:mm"));
            try {
                int fail = 0;
                int del = 0;
                if (tempDir == null) {
                    File rf = File.createTempFile("DSCR-", ".ser");
                    tempDir = rf.getParentFile();
                }

                String today = DateUtil.formatDate(new java.util.Date(), "yyyyMMdd");
                String yesterday = DateUtil.formatDate(new java.util.Date(System.currentTimeMillis() - DateUtil.DT24H),
                                                       "yyyyMMdd");

                File[] tempFiles = tempDir.listFiles();
                for (File fx : tempFiles) {
                    if (fx.exists() && fx.isFile() && fx.getName().startsWith("DSCR-") &&
                        fx.getName().endsWith(".ser") && !fx.getName().contains(today) &&
                        !fx.getName().contains(yesterday) && fx.lastModified() < back) {
                        try {
                            if (fx.delete()) {
                                del++;
                            } else {
                                fail++;
                                log.warn("Failed to delete CR file " + fx.getAbsolutePath());
                            }
                        } catch (Exception de) {
                            fail++;
                            log.error("Failed to delete CR file " + fx.getAbsolutePath(), de);
                        }
                    }

                }
                log.info("Cleanup deleted ok " + del + " CR files created before " +
                         DateUtil.formatDate(new java.util.Date(back), "yyMMdd HH:mm") +
                         (fail > 0 ? ", but " + fail + " files failed to delete" : ""));
            } catch (Exception ex) {
                log.error("Cleanup error deleting old CR files", ex);
            }
        }
    }
}
