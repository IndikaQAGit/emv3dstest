package com.modirum.ds.web.services;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.FieldMaxLength;
import com.modirum.ds.enums.FieldName;
import com.modirum.ds.enums.PaymentSystemType;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.enums.TransactionAttribute;
import com.modirum.ds.exceptions.KeyDataIdNotFoundException;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.messageprocessor.PaymentSystemMessageProcessor;
import com.modirum.ds.messageprocessor.PaymentSystemMsgProcessorRegistry;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.CertificateData;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.DSModel.TSDRecord;
import com.modirum.ds.model.Error;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.DeviceChannel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.model.TDSModel.ErrorComponent;
import com.modirum.ds.model.TDSModel.MessageVersion;
import com.modirum.ds.model.TDSModel.XDSRequestorAuthenticationInd;
import com.modirum.ds.model.TDSModel.XauthenticationMethod;
import com.modirum.ds.model.TDSModel.XauthenticationType;
import com.modirum.ds.model.TDSModel.XmessageType;
import com.modirum.ds.model.TDSModel.XtransStatusReason;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.model.Timings;
import com.modirum.ds.model.threeds.ThreeDSRequestorAuthenticationIndicator;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.MessageService.JsonAndDucplicates;
import com.modirum.ds.services.MessageService.PublicBOS;
import com.modirum.ds.services.MessageService210;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.elo.papi.EloPapiDetokenizationService;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XcardRangeData;
import com.modirum.ds.tds21msgs.XmessageExtension;
import com.modirum.ds.tds21msgs.XrenderObj2;
import com.modirum.ds.util.BooleanUtils;
import com.modirum.ds.util.DateFormatUtils;
import com.modirum.ds.util.DsStringUtils;
import com.modirum.ds.util.ErrorUtils;
import com.modirum.ds.util.ObjectUtils;
import com.modirum.ds.util.RegexUtil;
import com.modirum.ds.utils.ISO4217;
import com.modirum.ds.web.context.WebContext;
import com.modirum.ds.web.core.ByteArrayOutputStreamUnreadable;
import com.modirum.ds.web.core.PresCardRangeCache;
import com.modirum.ds.web.filters.FiltersProvider210;
import com.modirum.ds.services.server.PaymentSystemSettingsCacheService;
import com.modirum.ds.web.validators.MerchantRiskIndicatorValidator;
import com.modirum.ds.web.validators.ValidationContext;
import com.modirum.ds.utils.http.HttpsJSSEClient;
import com.modirum.ds.utils.http.SSLSocketHelper;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.ISO3166;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.net.ssl.KeyManagerFactory;
import javax.xml.bind.JAXBException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.modirum.ds.util.DSUtils.createUUID;
import static com.modirum.ds.util.DSUtils.isUUID;
import static com.modirum.ds.util.ErrorUtils.fieldMissingError;
import static com.modirum.ds.web.util.Utils.getFirstError;
import static com.modirum.ds.web.util.Utils.isArraysEqualsLength;
import static com.modirum.ds.web.validators.ValidationContext.ARES_ECI_REQUIRED;
import static com.modirum.ds.web.validators.ValidationContext.ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE;
import static com.modirum.ds.web.validators.ValidationContext.ARES_TRANS_STATUS_A_ECI;
import static com.modirum.ds.web.validators.ValidationContext.ARES_TRANS_STATUS_U_ECI;
import static java.lang.String.format;


/**
 * Main service to process and route messages after initial acceptance by DServer servlet.
 */
@Component
public class DirectoryService210 extends AbstractDirectoryService {

    private static final Logger log = LoggerFactory.getLogger(DirectoryService210.class);

    private final static String[] NUR = new String[]{TDSModel.XtransStatus.N.value(), TDSModel.XtransStatus.U.value(), TDSModel.XtransStatus.R.value()};
    private final static String[] YCA = new String[]{TDSModel.XtransStatus.Y.value(), TDSModel.XtransStatus.C.value(), TDSModel.XtransStatus.A.value()};
    private static final Set<String> transStatusValues21ARes = new HashSet<>(Arrays.asList(
            TDSModel.XtransStatus.Y.value(),
            TDSModel.XtransStatus.N.value(),
            TDSModel.XtransStatus.U.value(),
            TDSModel.XtransStatus.A.value(),
            TDSModel.XtransStatus.C.value(),
            TDSModel.XtransStatus.R.value()
    ));

    private static final Set<String> transStatusValues21_22RReq = new HashSet<>(Arrays.asList(
            TDSModel.XtransStatus.Y.value(),
            TDSModel.XtransStatus.N.value(),
            TDSModel.XtransStatus.U.value(),
            TDSModel.XtransStatus.A.value(),
            TDSModel.XtransStatus.R.value()
    ));

    protected static final Set<String> transStatusReasonList21  = new HashSet<>(Arrays.asList(
            TDSModel.XtransStatusReason.C01AuthFailed.value(),
            TDSModel.XtransStatusReason.C02UnknDevice.value(),
            TDSModel.XtransStatusReason.C03UnsupDevice.value(),
            TDSModel.XtransStatusReason.C04ExceedsAuthFreq.value(),
            TDSModel.XtransStatusReason.C05ExpiredCard.value(),
            TDSModel.XtransStatusReason.C06InvalidPan.value(),
            TDSModel.XtransStatusReason.C07InvalidTx.value(),
            TDSModel.XtransStatusReason.C08NoCardrecord.value(),
            TDSModel.XtransStatusReason.C09SecurityFailure.value(),
            TDSModel.XtransStatusReason.C10StolenCard.value(),
            TDSModel.XtransStatusReason.C11SupectFraud.value(),
            TDSModel.XtransStatusReason.C12TxNotPermitted.value(),
            TDSModel.XtransStatusReason.C13CardNotEnrolled.value(),
            TDSModel.XtransStatusReason.C14TxTimedOutACS.value(),
            TDSModel.XtransStatusReason.C15LowConfidence.value(),
            TDSModel.XtransStatusReason.C16MedConfidence.value(),
            TDSModel.XtransStatusReason.C17HighConfidence.value(),
            TDSModel.XtransStatusReason.C18VeryHighConfidence.value(),
            TDSModel.XtransStatusReason.C19ExceedsACSMaxChallenges.value(),
            TDSModel.XtransStatusReason.C20NonPaymentTransactionNotSupported.value(),
            TDSModel.XtransStatusReason.C213RITransactionNotSupported.value()
    ));

    private static final String emvBook4CommonCharacterSet  = " ='!\"#$%&()*+,-./0123456789:;<>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

    protected static final String ACTION_IND_ADD = "A";
    protected static final String ACTION_IND_DELETE = "D";

    public final static int  cDefaultConnectTimeout  = 3000;
    public final static int  cDefaultResponseTimeout = 3000;
    public final static long max3DSSessionDuration   = 24L * 3600L * 1000L;

    protected MessageService210<TDSMessage> mse;

    protected List<String> filters;

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private PaymentSystemSettingsCacheService paymentSystemSettingsCacheService;

    public DirectoryService210() {
        super();
        mse = MessageService210.getInstance();
        initFilters();
    }

    /**
     * For forwarded messages.
     * key = MSGType + field name + Device + tdsMessageBase cat +
     * key2 = MSGType + field name + Device + tdsMessageBase cat +status
     * key3 = MSGType + field name  (for all)
     * <p>
     * Filter to pass through fields on condition.
     */
    void initFilters() {
        filters = new ArrayList<>();
        filters.addAll(FiltersProvider210.getFilters());
    }

    public MessageService<?> getMessageService() {
        return mse;
    }

    /**
     * Create error message.
     */
    private TDSMessage createErrorMessage(String messageVersion, ErrorCode errorCode, String description, CharSequence detail, TDSMessage req) {
        String messageType = (req != null) ? req.getMessageType() : null;
        return createErrorMessage(messageVersion, errorCode, description, messageType, detail, req);
    }

    /**
     * Create error message.
     */
    private TDSMessage createErrorMessage(String messageVersion, ErrorCode errorCode, String description, String errorMessageType, CharSequence detail, TDSMessage req) {
        String[] IDs = (req != null) ? new String[]{req.getSdkTransID(), req.getTdsServerTransID(), req.getDsTransID(), req.getAcsTransID()} : null;
        return createErrorMessage(errorCode, messageVersion, errorMessageType, description, detail, IDs);
    }

    /**
     * Create error message.
     */
    public TDSMessage createErrorMessage(ErrorCode errorCode, String messageVersion, String errorMessageType, String description, CharSequence detail, String[] sdkIdTDSIdDSIdSACSId) {
        TDSMessage tdsMessage = new TDSMessage();
        tdsMessage.setErrorCode(errorCode.value);
        if (description == null && errorCode != null) {
            description = errorCode.desc;
        }

        tdsMessage.setMessageVersion(messageVersion);
        tdsMessage.setMessageType(XmessageType.ERRO.value);

        tdsMessage.setErrorDescription(description);
        tdsMessage.setErrorDetail(detail != null ? detail.toString() : null);
        tdsMessage.setErrorMessageType(errorMessageType != null && errorMessageType.length() == 4 ? errorMessageType : null);
        tdsMessage.setErrorComponent(ErrorComponent.cDS.value);

        // include specific id if present and is valid
        tdsMessage.setSdkTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 0 &&
                                 isUUID(sdkIdTDSIdDSIdSACSId[0]) ? sdkIdTDSIdDSIdSACSId[0] : null);
        tdsMessage.setTdsServerTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 1 &&
                                       isUUID(sdkIdTDSIdDSIdSACSId[1]) ? sdkIdTDSIdDSIdSACSId[1] : null);
        tdsMessage.setDsTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 2 &&
                                isUUID(sdkIdTDSIdDSIdSACSId[2]) ? sdkIdTDSIdDSIdSACSId[2] : null);
        tdsMessage.setAcsTransID(sdkIdTDSIdDSIdSACSId != null && sdkIdTDSIdDSIdSACSId.length > 3 &&
                                 isUUID(sdkIdTDSIdDSIdSACSId[3]) ? sdkIdTDSIdDSIdSACSId[3] : null);

        return tdsMessage;
    }

    @Override
    public Results createErrorResults(String v, ErrorCode ecode, String mtsrc, String descr, CharSequence detail, String[] sdkIdTDSIdDSIdSACSId) throws JAXBException {
        TDSMessage tdsMessage = createErrorMessage(ecode, v, mtsrc, descr, detail, sdkIdTDSIdDSIdSACSId);
        Results results = new Results();
        results.tdsMessageBase = tdsMessage;
        results.jsonRaw = mse.toJSONBos(tdsMessage);
        return results;
    }

    @Override
    public Results processRequest(TDSMessageBase reqMsgbase, byte[] msgRaw1, String[] msgRaw2, Date now, Timings timings, WebContext wctx) throws Exception {
        directory.checkLicensing();

        TDSMessage reqMsg = (TDSMessage) reqMsgbase;
        boolean isRReq = XmessageType.R_REQ.value().equals(reqMsg.getMessageType());

        if (!isRReq) { // for RReq the paymentSystemId is retrieved from existing tds_record.
            int serverPort = wctx.getRequestServerPort();
            Optional<Integer> paymentSystemId = getPaymentSystemIdByPort(serverPort);
            if (!paymentSystemId.isPresent()) {
                log.error("Unknown payment system. Request at port: {}", serverPort);
                return processUnknownPaymentSystem(now, wctx);

            }
            wctx.setPaymentSystemId(paymentSystemId.get());
        }

        if (XmessageType.A_REQ.value().equals(reqMsg.getMessageType())) {
            return processAReq(reqMsg, msgRaw2, now, timings, wctx);
        } else if (isRReq) {
            return processRReq(reqMsg, msgRaw1, msgRaw2, now, timings, wctx);
        } else if (XmessageType.P_REQ.value().equals(reqMsg.getMessageType())) {
            return processPReq(reqMsg, msgRaw2, now, timings, wctx);
        } else {
            Results results = new Results();
            String messageVersion = getMessageVersion();
            results.tdsRecord = createNewTDSRecord(reqMsg, now, wctx);
            MDC.getMDCAdapter().put("tx", "tx" + results.tdsRecord.getId());
            results.tdsRecord.setDSTransID(createUUID(getUUIDPrefix(wctx), results.tdsRecord.getId()));
            String errId = WebContext.getUnique();
            createNewTDSMessageData(reqMsg, null, msgRaw2, now, wctx.getRequest().getRemoteAddr(),
                                    "DS:" + getDSID(), wctx);
            TDSMessage emsg = createErrorMessage(messageVersion, ErrorCode.cInvalidMessageType101, null,
                                                 "Directory does not process messages of type '" +
                                                 reqMsg.getMessageType() + "' err id=" + errId, reqMsg);
            results.tdsMessageBase = emsg;
            results.jsonRaw = mse.toJSONBos(emsg);
            createNewTDSMessageData(emsg, results.tdsRecord, results.jsonRaw, new Date(),
                                    "DS:" + getDSID(), wctx.getRequest().getRemoteAddr(), wctx);
            results.tdsRecord.setLocalStatus(DSModel.TSDRecord.Status.ERROR);
            results.tdsRecord.setLocalDateEnd(new Date());
            results.tdsRecord.setErrorCode(Misc.cut(results.tdsMessageBase.getErrorCode(), 3));
            results.tdsRecord.setErrorDetail(Misc.cut(results.tdsMessageBase.getErrorDetail(), 2048));
            safeUpdate(results.tdsRecord, -1, wctx);
            log.error("Invalid message type received '" + reqMsg.getMessageType() + "' errId = " + errId);
            return results;
        }
    }

    public boolean isValidAuthenticationType(String at, ValidationContext validationContext) {
        if (XauthenticationType.contains(at)) {
            return true;
        }
        return at != null && ("," + validationContext.get(ValidationContext.EXTRA_AUTHENTICATION_TYPES) + ",").contains("," + at + ",");
    }

    public boolean isValidRequestorChallengeInd(String at, ValidationContext validationContext) {
        if (Misc.in(at, TDSModel.threeDSRequestorChallengeInds21) || Misc.parseInt(at) >= 80) {
            return true;
        }
        String extraRequestorChallengeInds = validationContext.get(ValidationContext.EXTRA_REQUESTOR_CHALLENGE_INDS);
        return at != null && extraRequestorChallengeInds != null && extraRequestorChallengeInds.contains(at);
    }

    protected boolean isValidResultsStatus(String at, ValidationContext validationContext) {
        if (TDSModel.XResultsStatus.contains(at)) {
            return true;
        }
        return at != null &&
               ("," + validationContext.get(ValidationContext.EXTRA_RESULTS_STATUSES) + ",").contains("," + at + ",");
    }

    public void validate3RIDeviceChannel(boolean isPa, List<Error> errors) {
        if (isPa) {
            // EMV 2.1.0 - 3.4 3RI-based Requirements
            // "3RI-based implementations shall support only Non-Payment Authentication (NPA) transactions."
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageCategory.deviceChannel", "invalid combination of messageCategory and deviceChannel"));
        }
    }

    public boolean isMessageCategory80to99(TDSMessage aReq) {
        //if in the future it MessageCategory will have an instance of not an integer
        if (!NumberUtils.isParsable(aReq.getMessageCategory( ))) {
            return false;
        }
        return (Integer.parseInt(aReq.getMessageCategory( )) >= 80 && Integer.parseInt(aReq
                .getMessageCategory( )) <= 99);
    }

    protected void validateCardHolderName(TDSMessage req, List<Error> errors) {
        if (Misc.isNotNullOrEmpty(req.getCardholderName()) &&
                !RegexUtil.validAllowedCharacters(req.getCardholderName(), emvBook4CommonCharacterSet)) {
            errors.add(new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, "cardholderName", "contains invalid character/s"));
        }
    }

    protected List<Error> validateAReqEmv(TDSMessage aReq, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();
        mse.validateCommons(aReq, errors);
        if (errors.size() > 0) {
            return errors;
        }
        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.value().equals(aReq.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.value().equals(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.value().equals(aReq.getDeviceChannel());

        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory()); // Non-Payment Authentication
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory()); // Payment Authentication
        boolean strictValidation = validationContext.getBoolean(ValidationContext.STRICT_VALIDATION);

        if (isBrowserDeviceChannel) {
            Object test;
            if (Misc.isNullOrEmpty(test = aReq.getBrowserAcceptHeader())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "browserAcceptHeader", (
                        test == null ? "is missing" : "is empty")));
            }
            if (Misc.isNullOrEmpty(test = aReq.getBrowserIP())) {
                if (validationContext.getBoolean(ValidationContext.AREQ_BR_BROWSER_IP_REQUIRED) || test != null) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "browserIP", (
                            test == null ? "is missing" : "is empty")));
                }
            }

            validateAReqBrowserVersionSpecial(errors, aReq);

            // 2.1 in Creq in 2.0
            if (Misc.isNullOrEmpty(test = aReq.getNotificationURL())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "notificationURL", (test == null ? "is missing" : "is empty")));
            }

            if (Misc.isNullOrEmpty(test = aReq.getThreeDSCompInd())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSCompInd", (test == null ? "is missing" : "is empty")));
            }
        }

        JsonMessage aReqJsonMessage = aReq.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isPa && Misc.isNullOrEmpty(aReq.getAcquirerBIN())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.acquirerBIN));
        } else if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
                   isNpa && aReqJsonMessage.isEmptyOrNullField(FieldName.acquirerBIN)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.acquirerBIN));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isPa && Misc.isNullOrEmpty(aReq.getMcc())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.mcc));
        } else if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
                   isNpa && aReqJsonMessage.isEmptyOrNullField(FieldName.mcc)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.mcc));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isPa && Misc.isNullOrEmpty(aReq.getMerchantName())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.merchantName));
        } else if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
                   isNpa && aReqJsonMessage.isEmptyOrNullField(FieldName.merchantName)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.merchantName));
        }

        if (Misc.isNotNullOrEmpty(aReq.getTdsRequestorURL())) {
            try {
                new URL(aReq.getTdsRequestorURL());
            } catch (MalformedURLException e) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "threeDsRequestorURL", "invalid value"));
            }
        }

        if (Misc.isNotNullOrEmpty(aReq.getMessageExtension())) {
            for (XmessageExtension extension : aReq.getMessageExtension()) {
                if (Misc.isNotNullOrEmpty(extension.getName()) && extension.getName().length() > 64) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageExtension.name", " message extension name exceeds maximum value"));

                }
            }
        }

        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) && (isNpa || isPa) 
            && Misc.isNotNullOrEmpty(aReq.getBroadInfo())) {
            if (aReqJsonMessage.getRootNode().get(FieldName.broadInfo).toString().length() > 4096) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "broadInfo", "Maximum number of characters is 4096"));
            }
        }

        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
            (isNpa || isPa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.acctID)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.acctID));
        }

        //much better to separate the validation logic for each field in contrast of mixing different fields in nested IFs
        if ((isPa || isNpa) &&
            (isAppDeviceChannel || isBrowserDeviceChannel) &&
            ("02".equals(aReq.getThreeDSRequestorAuthenticationInd()) || "03".equals(aReq.getThreeDSRequestorAuthenticationInd())) &&
            Misc.isNullOrEmpty(aReq.getRecurringFrequency())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "recurringFrequency", "is missing"));
        }

        //much better to separate the validation logic for each field in contrast of mixing different fields in nested IFs
        if ((isPa || isNpa) &&
            (isAppDeviceChannel || isBrowserDeviceChannel) &&
            ("02".equals(aReq.getThreeDSRequestorAuthenticationInd()) || "03".equals(aReq.getThreeDSRequestorAuthenticationInd())) &&
            Misc.isNullOrEmpty(aReq.getRecurringExpiry())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "recurringExpiry", "is missing"));
        }

        if (isAppDeviceChannel || isBrowserDeviceChannel) {
            if (Misc.isNullOrEmpty(aReq.getThreeDSRequestorAuthenticationInd())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSRequestorAuthenticationInd", "is missing"));
            } else if (XDSRequestorAuthenticationInd.C03Instalment.value.equals(aReq.getThreeDSRequestorAuthenticationInd())) {
                if (Misc.isNullOrEmpty(aReq.getPurchaseInstalData())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseInstalData", "is missing"));
                }
            }
        } else if (is3RIDeviceChannel) {
            validate3RIDeviceChannel(isPa, errors);
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            StringUtils.isNotEmpty(aReqJsonMessage.getCardExpiryDate())) {
            String cardExpiryDateFormat = "yyMM";
            if (aReqJsonMessage.getCardExpiryDate().length() != cardExpiryDateFormat.length() ||
                !DateFormatUtils.isDateFormatted(aReqJsonMessage.getCardExpiryDate(), cardExpiryDateFormat)) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.cardExpiryDate, "has invalid format"));
            }
        }

        if (Misc.isNullOrEmpty(aReq.getAcctNumber())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acctNumber", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getDeviceChannel())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "deviceChannel", "is missing"));
        }
        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
            isPa && Misc.isNullOrEmpty(aReq.getMerchantCountryCode())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.merchantCountryCode));
        } else if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
                   isNpa && aReqJsonMessage.isEmptyOrNullField(FieldName.merchantCountryCode)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.merchantCountryCode));
        }
        if (Misc.isNullOrEmpty(aReq.getTdsRequestorName())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSRequestorName", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getTdsRequestorID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSRequestorID", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getMessageCategory())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageCategory", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getTdsServerRefNumber())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerRefNumber", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getTdsServerTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerTransID", "is missing"));
        }
        if (!is3RIDeviceChannel && Misc.isNullOrEmpty(aReq.getTdsServerURL())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerURL", "is missing"));
        }
        if (Misc.isNullOrEmpty(aReq.getTdsRequestorURL())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSRequestorURL", "is missing"));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel) &&
            (isNpa || isPa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSRequestorChallengeInd)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSRequestorChallengeInd));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel) &&
            (isNpa || isPa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.addrMatch)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.addrMatch));
        }

        if (aReq.getDeviceRenderOptions() != null) {
            // case TC_DS_10182_001
            boolean isObject = true;
            if (aReq.getJsonObject() != null) {
                JsonValue jv = aReq.getJsonObject().get("deviceRenderOptions");
                if (jv != null && ValueType.OBJECT != jv.getValueType()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "deviceRenderOptions",
                                         "is not json " + ValueType.OBJECT.name() + " but " +
                                         jv.getValueType().name()));
                    isObject = false;
                }
            }

            if (isObject) {
                if ((aReq.getDeviceRenderOptions().getSdkUiType() == null ||
                     aReq.getDeviceRenderOptions().getSdkUiType().size() < 1) &&
                    Misc.isNullOrEmpty(aReq.getDeviceRenderOptions().getSdkInterface())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "deviceRenderOptions", "has no attributes"));
                } else if (aReq.getDeviceRenderOptions().getSdkUiType() == null ||
                           aReq.getDeviceRenderOptions().getSdkUiType().size() < 1) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "deviceRenderOptions.sdkUiType", "is missing"));
                } else if (Misc.isNullOrEmpty(aReq.getDeviceRenderOptions().getSdkInterface())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "deviceRenderOptions.sdkInterface", "is missing"));
                }
            }
        }

        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
            isPa && Misc.isNullOrEmpty(aReq.getAcquirerMerchantID())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.acquirerMerchantID));
        } else if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
                   isNpa && aReqJsonMessage.isEmptyOrNullField(FieldName.acquirerMerchantID)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.acquirerMerchantID));
        }

        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
            (isNpa || isPa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.acctInfo)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.acctInfo));
        }

        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
            (isNpa || isPa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.merchantRiskIndicator)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.merchantRiskIndicator));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel) &&
            (isNpa || isPa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSRequestorAuthenticationInfo)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSRequestorAuthenticationInfo));
        }

        if (isPa || isNpa && Misc.in(aReq.getThreeDSRequestorAuthenticationInd(), new String[]{"02", "03"})) {
            if (Misc.isNullOrEmpty(aReq.getPurchaseAmount())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseAmount", "is missing"));
            }
            if (Misc.isNullOrEmpty(aReq.getPurchaseCurrency())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseCurrency", "is missing"));
            }
            if (Misc.isNullOrEmpty(aReq.getPurchaseExponent())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseExponent", "is missing"));
            }
            if (Misc.isNullOrEmpty(aReq.getPurchaseDate())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "purchaseDate", "is missing"));
            }
        }

        validateMissingPurchaseCurrency(aReq, errors);

        // app cases
        if (isAppDeviceChannel) {
            if (Misc.isNullOrEmpty(aReq.getDeviceRenderOptions())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "deviceRenderOptions", "is missing"));
            }

            if (Misc.isNullOrEmpty(aReq.getSdkAppID())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkAppID", "is missing"));
            }

            if (aReq.getSdkEphemPubKey() == null) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkEphemPubKey", "is missing"));
            } else if (Misc.isNullOrEmpty(aReq.getSdkEphemPubKey().getX())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkEphemPubKey.x", "is missing"));
            } else if (Misc.isNullOrEmpty(aReq.getSdkEphemPubKey().getY())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkEphemPubKey.y", "is missing"));
            } else if (Misc.isNullOrEmpty(aReq.getSdkEphemPubKey().getKty())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkEphemPubKey.kty", "is missing"));
            } else if (Misc.isNullOrEmpty(aReq.getSdkEphemPubKey().getCrv())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkEphemPubKey.crv", "is missing"));
            }

            // validate key contents TC.DS.00587.001
            else if (strictValidation) {
                //"RSA" or "EC"
                if (!Misc.in(aReq.getSdkEphemPubKey().getKty(), new String[]{"RSA", "EC"})) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "sdkEphemPubKey.kty", " unspecified value"));
                }
                if (aReq.getSdkEphemPubKey().getUse() != null &&
                    !Misc.in(aReq.getSdkEphemPubKey().getUse(), new String[]{"sig", "enc"})) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "sdkEphemPubKey.use", " unspecified value"));
                }
                if (aReq.getSdkEphemPubKey().getCrv() != null &&
                    !Misc.in(aReq.getSdkEphemPubKey().getCrv(), new String[]{"P-256"})) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "sdkEphemPubKey.crv", " unspecified value"));
                }
                if (aReq.getSdkEphemPubKey().getX() != null) {

                    byte[] rawx = null;
                    try {
                        rawx = Base64.decodeURL(aReq.getSdkEphemPubKey().getX());
                    } catch (Exception e) {
                        log.warn("sdkEphemPubKey.x base64 deconding failed", e);
                    }
                    if (rawx == null || rawx.length != 32) {
                        errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "sdkEphemPubKey.x", "invalid value"));
                    }
                }
                if (aReq.getSdkEphemPubKey().getY() != null) {
                    byte[] rawy = null;
                    try {
                        rawy = Base64.decodeURL(aReq.getSdkEphemPubKey().getY());
                    } catch (Exception e) {
                        log.warn("sdkEphemPubKey.y base64 deconding failed", e);
                    }
                    if (rawy == null || rawy.length != 32) {
                        errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "sdkEphemPubKey.y", "invalid value"));
                    }
                }
            }

            if (Misc.isNullOrEmpty(aReq.getSdkReferenceNumber())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkReferenceNumber", "is missing"));
            }

            if (Misc.isNullOrEmpty(aReq.getSdkTransID())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkTransID", "is missing"));
            }

            // required in incoming
            if (Misc.isNullOrEmpty(aReq.getSdkEncData())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkEncData", "is missing"));
            }

            if (Misc.isNullOrEmpty(aReq.getSdkMaxTimeout())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkMaxTimeout", "is missing"));
            } else if (Misc.parseInt(aReq.getSdkMaxTimeout()) < 5) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "sdkMaxTimeout", "is less than 5"));
            }
        }

        //3RI cases
        if (is3RIDeviceChannel) {
            if (Misc.isNullOrEmpty(aReq.getThreeRIInd())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeRIInd", "is missing"));
            }
        }
        String threeRIIndFromReq = aReq.getThreeRIInd();
        if (threeRIIndFromReq != null) {
            if (strictValidation && errors.size() == 0 && !validateThreeRIValue(threeRIIndFromReq)) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "threeRIInd", "invalid or reserved value"));
            }
        }

        // addr conditionals
        if (Misc.isNotNullOrEmpty(aReq.getBillAddrState()) && Misc.isNullOrEmpty(aReq.getBillAddrCountry())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "billAddrCountry", "is missing"));
        }
        if (Misc.isNotNullOrEmpty(aReq.getShipAddrState()) && Misc.isNullOrEmpty(aReq.getShipAddrCountry())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "shipAddrCountry", "is missing"));
        }

        if (aReqJsonMessage.isEmptyOrNullField(FieldName.cardholderName)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.cardholderName));
        }
        validateCardHolderName(aReq, errors);

        if ((isAppDeviceChannel || is3RIDeviceChannel || isBrowserDeviceChannel) &&
            (isPa || isNpa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSRequestorPriorAuthenticationInfo)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSRequestorPriorAuthenticationInfo));
        }

        //critical extensions
        if (errors.size() == 0) {
            validateCriticalExtensions(aReq, errors, validationContext);
        }
        // validates edit criteria pattern and lengths as per Seq 5.21 this shall be after present validation
        if (errors.size() == 0) {
            mse.validateFieldSyntax(aReq, errors);
            if (errors.size() > 0) {
                return errors;
            }
        }
        return errors;
    }

    protected void validateMissingPurchaseCurrency(TDSMessage aReq, List<Error> errors) {
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());
        if (isNpa && !(Arrays.asList("02", "03").contains(aReq.getThreeDSRequestorAuthenticationInd()))) {
            if (aReq.getIncomingJsonMessage().isEmptyOrNullField(FieldName.purchaseCurrency)) {
                errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.purchaseCurrency));
            }
        }
    }

    /**
     * All default validation for AReq DS Conditional fields are grouped in this method.
     * This is executed after Core DS-AReq validation and PS-AReq validation.
     * By default, DS Conditional fields are treated as Optional when validated.
     *
     * @param aReq
     * @return
     */
    protected List<Error> postValidateAReqDSConditional(TDSMessage aReq) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.isEqual(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());

        JsonMessage aReqJsonMessage = aReq.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.threeDSServerOperatorID)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.threeDSServerOperatorID));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.broadInfo)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.broadInfo));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.cardExpiryDate)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.cardExpiryDate));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.messageExtension)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.messageExtension));
        }

        boolean isRecurringTxn = ThreeDSRequestorAuthenticationIndicator.RECURRING_TRANSACTION.isEqual(aReq.getThreeDSRequestorAuthenticationInd());
        boolean isInstalmentTxn = ThreeDSRequestorAuthenticationIndicator.INSTALMENT_TRANSACTION.isEqual(aReq.getThreeDSRequestorAuthenticationInd());

        if ((isPa || isNpa) &&
            (isAppDeviceChannel || isBrowserDeviceChannel) &&
            (!isRecurringTxn && !isInstalmentTxn) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.recurringExpiry)) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.recurringExpiry, "has invalid value"));
        }

        if ((isPa || isNpa) &&
            (isAppDeviceChannel || isBrowserDeviceChannel) &&
            (!isRecurringTxn && !isInstalmentTxn) &&
            aReqJsonMessage.isEmptyOrNullField(FieldName.recurringFrequency)) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.recurringFrequency, "has invalid value"));
        }

        return errors;
    }

    protected void validateAReqBrowserVersionSpecial(List<Error> errors, TDSMessage aReq) {
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory()); // Non-Payment Authentication
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory()); // Payment Authentication

        if (Misc.isNullOrEmpty(aReq.getBrowserLanguage())) {
            errors.add(fieldMissingError("browserLanguage"));
        }
        if (Misc.isNullOrEmpty(aReq.getBrowserColorDepth())) {
            errors.add(fieldMissingError("browserColorDepth"));
        }
        if (Misc.isNullOrEmpty(aReq.getBrowserScreenHeight())) {
            errors.add(fieldMissingError("browserScreenHeight"));
        } else if (Misc.parseInt(aReq.getBrowserScreenHeight()) < 1) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "browserScreenHeight", "is invalid value (lt 1)"));
        }
        if (Misc.isNullOrEmpty(aReq.getBrowserScreenWidth())) {
            errors.add(fieldMissingError("browserScreenWidth"));
        }
        if (Misc.isNullOrEmpty(aReq.getBrowserUserAgent())) {
            errors.add(fieldMissingError("browserUserAgent"));
        }
        if ((isPa || isNpa) &&
            Misc.isNullOrEmpty(aReq.getBrowserTZ())) {
            errors.add(fieldMissingError("browserTZ"));
        }

        String browserJavaEnabled = aReq.getIncomingJsonMessage().getString(FieldName.browserJavaEnabled);
        if ((isPa || isNpa) &&
            StringUtils.isEmpty(browserJavaEnabled)) {
            errors.add(fieldMissingError(FieldName.browserJavaEnabled));
        } else if (StringUtils.isNotEmpty(browserJavaEnabled) &&
                   !BooleanUtils.isBoolean(browserJavaEnabled)) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.browserJavaEnabled, "is an invalid value"));
        } else {
            if (aReq.getIncomingJsonMessage().isEmptyOrNullField(FieldName.browserJavaEnabled)) {
                errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.browserJavaEnabled));
            }
        }
    }

    protected boolean validateThreeRIValue(String value) {
        return TDSModel.X3RIInd.contains(value);
    }

    protected void validateCriticalExtensions(TDSMessage aReq, List<Error> errors, ValidationContext validationContext) {
        if (aReq.getMessageExtension() != null) {
            if (aReq.getMessageExtension().size() > 10) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageExtension",
                                     "too many extensions (" + aReq.getMessageExtension().size() +
                                     " is more than 10)"));
            } else {
                String knownExtensions = validationContext.get(ValidationContext.CRITICAL_EXTENSIONS_PASS);
                for (XmessageExtension xx : aReq.getMessageExtension()) {
                    if ("true".equals(xx.getCriticalityIndicator()) &&
                        (knownExtensions == null || !knownExtensions.contains(xx.getId()))) {
                        errors.add(new Error(ErrorCode.cCriticalElemNotRecognized202,
                                             "messageExtension " + xx.getId(), "critical extension not recognized"));
                    }
                }

                // CASE TC_DS_10089_001..005
                if (aReq.getJsonObject() != null &&
                    aReq.getJsonObject().containsKey("messageExtension")) {
                    try {
                        int datamaxlen = 8059;
                        JsonArray exta = aReq.getJsonObject().getJsonArray("messageExtension");
                        for (int i = 0; exta != null && i < exta.size(); i++) {
                            JsonObject xx = exta.getJsonObject(i);
                            JsonObject xxd = xx.getJsonObject("data");
                            if (xxd != null) {
                                StringWriter sw = mse.toJSONStrWriter(xxd);
                                if (sw.getBuffer().length() > datamaxlen) {
                                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageExtension.data",
                                                         "Message extension " + xx.getString("id") + " data length " +
                                                         sw.getBuffer().length() + " exceeds " + datamaxlen));
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.error("Error validating extension data len with recreation method", e);
                    }
                }
            }
        }
    }

    protected List<Error> validateAResEmv(TDSMessage aRes, TDSMessage aReq, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();
        mse.validateCommons(aRes, errors);
        if (errors.size() > 0) {
            return errors;
        }

        if (!XmessageType.A_RES.value().equals(aRes.getMessageType())) {
            errors.add(new Error(ErrorCode.cInvalidMessageType101, "messageType",
                                 "messageType is not expected: " + aRes.getMessageType()));
        }

        if (Misc.isNotNullOrEmpty(aReq.getMessageVersion()) && !aReq.getMessageVersion().equals(aRes.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageVersion", "AReq/AREs messageVersion mismatch"));
        }

        // can probably be expressed more succinctly...
        //the below seems to be wrong. the specs apparently say that all values are allowed for HTML and only 01-04 for native UI
        //2.2 specs page 250
        //        if (aRes.getAcsRenderingType() != null && ("02".equals(aRes.getAcsRenderingType().getAcsInterface()) && !"05".equals(
        //                aRes.getAcsRenderingType().getAcsUiTemplate()) || "05".equals(aRes.getAcsRenderingType().getAcsUiTemplate()) && !"02".equals(
        //                aRes.getAcsRenderingType().getAcsInterface()))) {
        //            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "acsRenderingType.acsInterface",
        //                    "invalid combination of acsInterface and acsRenderingType"));
        //        }

        if (aRes.getAcsRenderingType() != null) {

            if (TDSModel.ACSUIType2_1_0.C05_HTML.value().equals(aRes.getAcsRenderingType().getAcsUiTemplate()) &&
                "01".equals(aRes.getAcsRenderingType().getAcsInterface())) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "acsRenderingType.acsInterface", "invalid combination of acsInterface and acsUiTemplate"));
            }
        }

        if (errors.size() > 0) {
            return errors;
        }

        boolean app = DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean brow = DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean ri3 = DeviceChannel.cChannel3RI03.isEqual(aReq.getDeviceChannel());
        boolean pa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());
        boolean npa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());
        boolean strictValidation = validationContext.getBoolean(ValidationContext.STRICT_VALIDATION);

        if (Misc.isNullOrEmpty(aRes.getAcsTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsTransID", "is missing"));
        }

        if ((app || brow || ri3) && pa) {
            if (Strings.isEmpty(aRes.getTransStatus())) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.transStatus));
            } else if (!getTransStatusValuesARes().contains(aRes.getTransStatus())) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.transStatus, "is invalid value by DS rules"));
            }
        }

        // challenge
        if (TDSModel.XtransStatus.C.isEqual(aRes.getTransStatus())) {
            if ((app || brow) && (pa || npa) && Misc.isNullOrEmpty(aRes.getAcsChallengeMandated())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsChallengeMandated", "is missing"));
            }
            if (app && (pa || npa) && aRes.getAcsRenderingType() == null) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsRenderingType", "is missing"));
            } else if (app && (pa || npa) && aRes.getAcsRenderingType() != null) {
                String acsInterface = aRes.getAcsRenderingType().getAcsInterface();
                if (StringUtils.isEmpty(acsInterface)) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsRenderingType.acsInterface",
                        acsInterface == null ? "is missing" : "is empty"));
                }
                String acsUiTemplate = aRes.getAcsRenderingType().getAcsUiTemplate();
                if (StringUtils.isEmpty(acsUiTemplate)) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsRenderingType.acsUiTemplate",
                        acsUiTemplate == null ? "is missing" : "is empty"));
                }
            }

            if (app && Misc.isNullOrEmpty(aRes.getAcsSignedContent())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsSignedContent", "is missing"));
            }

            if (brow && Misc.isNullOrEmpty(aRes.getAcsURL())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsURL", "is missing"));
            }

            if (!TDSModel.MessageVersion.V2_3_0.isEqual(aRes.getMessageVersion())) {
                if (Misc.isNullOrEmpty(aRes.getAuthenticationType())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "authenticationType", "is missing"));
                } else if (!isValidAuthenticationType(aRes.getAuthenticationType(), validationContext)) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "authenticationType", "is invalid value by DS rules"));
                }
            }

            if (ri3 && npa) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.transStatus,
                        "is invalid value by DS rules"));
            }
        }

        //For backward compatibility, retain these setting-based flags added on GH ticket DS#150,
        //If needed to be removed, to be deferred on refactoring phase for further analysis
        boolean npaAvRequired = npa && validationContext.getBoolean(ValidationContext.ARES_NPA_AUTHENTICATION_REQUIRED);
        boolean ri3AvRequired = ri3 && validationContext.getBoolean(ValidationContext.ARES_3RI_AUTHENTICATION_REQUIRED);
        if ((app || brow || ri3) &&
            (pa || npaAvRequired || ri3AvRequired) &&
            (TDSModel.XtransStatus.A.isEqual(aRes.getTransStatus()) || TDSModel.XtransStatus.Y.isEqual(aRes.getTransStatus())) &&
            StringUtils.isEmpty(aRes.getAuthenticationValue())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.authenticationValue));
        }

        if ((pa && ObjectUtils.isNotIn(aRes.getTransStatus(), NUR))) {
            if (aRes.getIncomingJsonMessage().isEmptyOrNullField(FieldName.transStatusReason)) {
                errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.transStatusReason));
            }
        } else if (strictValidation) {
            if (Misc.isNullOrEmpty(aRes.getTransStatusReason())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "transStatusReason", "is missing"));
            }
        }
        //backward compatible with settings-based customization `strictValidation`
        //do not want to touch the other field validation logic depending on this variable `strictValidation`
        if (validationContext.getBoolean(ValidationContext.STRICT_VALIDATION, true) &&
            Misc.isNotNullOrEmpty(aRes.getTransStatusReason()) &&
            !isDsReservedTransStatusReason(aRes.getTransStatusReason()) &&
            !isValidTransStatusReason(aRes.getTransStatusReason())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "transStatusReason", "invalid or reserved value"));
        }

        if (pa || npa) {
            if (aRes.getCardholderInfo() != null && !"Y".equals(aRes.getAcsDecConInd()) &&
                aRes.getCardholderInfo().length() == 0) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "cardholderInfo", "invalid value"));
            }

            if (!"C".equals(aRes.getTransStatus()) && !"D".equals(aRes.getTransStatus())) {
                if (aRes.getAcsChallengeMandated() != null && aRes.getAcsChallengeMandated().length() == 0) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "acsChallengeMandated", "invalid value"));
                }
            }
        }

        if (Misc.isNullOrEmpty(aRes.getAcsReferenceNumber())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsReferenceNumber", "is missing"));
        }

        // any case
        if (Misc.isNullOrEmpty(aRes.getDsReferenceNumber())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "dsReferenceNumber", "is missing"));
        }

        if (Misc.isNullOrEmpty(aRes.getDsTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "dsTransID", "is missing"));
        }

        if (Misc.isNullOrEmpty(aRes.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion", "is missing"));
        }

        if ((app || brow || ri3) && (npa || pa) && Misc.isNotNullOrEmpty(aRes.getBroadInfo())) {
            if (aRes.getIncomingJsonMessage().getRootNode().get(FieldName.broadInfo).toString().length() > 4096) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "broadInfo",
                        "Maximum number of characters is 4096"));
            }
        }

        if (Misc.isNullOrEmpty(aRes.getTdsServerTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerTransID", "is missing"));
        }

        if (app && Misc.isNullOrEmpty(aRes.getSdkTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "sdkTransID", "is missing"));
        }

        if (errors.size() == 0) {
            validateCriticalExtensions(aRes, errors, validationContext);
        }
        // validates edit criteria pattern and lengths as per Seq 5.21 this shall be after present validation
        if (errors.size() == 0) {
            mse.validateFieldSyntax(aRes, errors);
            if (errors.size() > 0) {
                return errors;
            }
            mse.validateCommons(aRes, errors);
            if (errors.size() > 0) {
                return errors;
            }
        }

        if (errors.size() < 1) {
            String acceptedRReqStatuses = validationContext.get(ValidationContext.ACCEPTED_ARES_STATUSES);
            if (Misc.isNotNullOrEmpty(acceptedRReqStatuses)) {
                String as = (";" + acceptedRReqStatuses + ";");
                String statusMix = ";" + aRes.getTransStatus() + "," +
                                   (Misc.isNullOrEmpty(aRes.getTransStatusReason()) ? "-" : aRes.getTransStatusReason()) + ";";
                String statusWld = ";" + aRes.getTransStatus() + ",*" + ";";

                if (!(as.contains(statusMix) || as.contains(statusWld))) {
                    errors.add(new Error(ErrorCode.cInvalidTxData305, "transStatus, transStatusReason", "combination is invalid by DS rules"));
                }
            }
        }
        log.info("Has errors: " + errors.size()); // Extra log
        return errors;
    }

    protected boolean isStringBetween80to99(String inputString) {
        boolean isBetween80to99 = false;
        if (NumberUtils.isParsable(inputString)) {
            Integer intFromStringConverted = Integer.parseInt(inputString);
            if (intFromStringConverted >= 80 && intFromStringConverted <= 99) {
                isBetween80to99 = true;
            }
        }
        return isBetween80to99;
    }

    private boolean isDsReservedTransStatusReason(String transStatusReason) {
        return isStringBetween80to99(transStatusReason);
    }

    protected boolean isValidTransStatusReason(String transStatusReason) {
        return transStatusReasonList21.contains(transStatusReason);
    }

    /**
     * Default validation for DS Conditional fields after core DS ARes validation and PS ARes validation.
     * By default, DS Conditional fields are treated as Optional when validated.
     *
     * @param aRes
     * @param aReq
     * @return
     */
    protected List<Error> postValidateAResDSConditional(TDSMessage aRes, TDSMessage aReq) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(aReq.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.isEqual(aReq.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.isEqual(aReq.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(aReq.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(aReq.getMessageCategory());

        JsonMessage aResJsonMessage = aRes.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aResJsonMessage.isEmptyOrNullField(FieldName.acsOperatorID)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.acsOperatorID));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            aResJsonMessage.isEmptyOrNullField(FieldName.authenticationValue)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.authenticationValue));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aResJsonMessage.isEmptyOrNullField(FieldName.broadInfo)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.broadInfo));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aResJsonMessage.isEmptyOrNullField(FieldName.eci)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.eci));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            aResJsonMessage.isEmptyOrNullField(FieldName.messageExtension)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.messageExtension));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            aResJsonMessage.isEmptyOrNullField(FieldName.transStatus)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.transStatus));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            aResJsonMessage.isEmptyOrNullField(FieldName.transStatusReason)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.transStatusReason));
        }

        return errors;
    }

    protected List<Error> validateRResEmv(TDSMessage rRes, TDSRecord record, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();
        mse.validateCommons(rRes, errors);
        if (errors.size() > 0) {
            return errors;
        }

        if (Misc.isNotNullOrEmpty(record.getProtocol()) && !record.getProtocol().equals(rRes.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageVersion", "AReq/RRes message version mismatch"));
        }

        if (!XmessageType.R_RES.value().equals(rRes.getMessageType())) {
            errors.add(new Error(ErrorCode.cInvalidMessageType101, "messageType",
                                 "messageType is not as expected " + XmessageType.R_RES.value()));
        }

        if (errors.size() > 0) {
            return errors;
        }

        if (Misc.isNullOrEmpty(rRes.getAcsTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsTransID", "is missing"));
        }

        if (Misc.isNullOrEmpty(rRes.getDsTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "dsTransID", "is missing"));
        }

        if (Misc.isNullOrEmpty(rRes.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion", "is missing"));
        }

        if (Misc.isNullOrEmpty(rRes.getTdsServerTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerTransID", "is missing"));
        }

        if (Misc.isNullOrEmpty(rRes.getResultsStatus())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "resultsStatus", "is missing"));
        } else {
            if (!isValidResultsStatus(rRes.getResultsStatus(), validationContext)) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "resultsStatus", "is invalid value by DS rules"));
            }
        }

        //crititcal extensions
        if (errors.size() == 0) {
            validateCriticalExtensions(rRes, errors, validationContext);
        }

        // validates edit criteria pattern and lengths as per Seq 5.21 this shall be after present validation
        if (errors.size() == 0) {
            mse.validateFieldSyntax(rRes, errors);
            if (errors.size() > 0) {
                return errors;
            }
        }

        return errors;
    }

    /**
     * Default validation for DS Conditional fields after core DS RRes validation and PS RRes validation.
     * By default, DS Conditional fields are treated as Optional when validated.
     * @param rRes
     * @param record
     * @return
     */
    protected List<Error> postValidateRResDSConditional(TDSMessage rRes, TDSRecord record) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(record.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.isEqual(record.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.isEqual(record.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.isEqual(record.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(record.getMessageCategory());

        JsonMessage rResJsonMessage = rRes.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            rResJsonMessage.isEmptyOrNullField(FieldName.messageExtension)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.messageExtension));
        }

        return errors;
    }

    protected List<Error> validateRReqEmv(TDSMessage rReq, TDSRecord record, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();
        mse.validateCommons(rReq, errors);
        if (errors.size() > 0) {
            return errors;
        }

        //TC_DS_10451_001, TC_DS_10452_001 not sure what this is about exactly. i just added the validation needed to pass these specific cases
        //stuff like that can easily break other cases. this needs to be reviewed
        if ("06".equals(rReq.getChallengeCancel()) && "14".equals(rReq.getTransStatusReason())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "challengeCancel", "RReq message contains invalid value of Challenge Cancelation Indicator for Transaction Status Reason '14'"));
        }

        if (Misc.isNotNullOrEmpty(record.getProtocol()) && !record.getProtocol().equals(rReq.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "messageVersion", "AReq/RReq message version mismatch"));
        }

        if (!record.getMessageCategory().equals(rReq.getMessageCategory())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.messageCategory, "AReq/RReq message category mismatch"));
        }

        if (record != null && record.getProtocol() != null && !record.getProtocol().equals(rReq.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cMessageVersionNotSupported102, "messageVersion", "mismatching value"));
            return errors;
        }

        // format priority TC_DS_10384_001..008
        if (Misc.isNullOrEmpty(rReq.getTdsServerTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerTransID", "is missing"));
            return errors;
        }

        if (record.getTdsTransID() != null && !record.getTdsTransID().equals(rReq.getTdsServerTransID())) {
            errors.add(new Error(ErrorCode.cInvalidTransactionID301, "threeDSServerTransID", "mismatching value"));
            return errors;
        }

        if (rReq.getAuthenticationType() != null && !"Y".equals(rReq.getTransStatus()) &&
            !"N".equals(rReq.getTransStatus()) && rReq.getAuthenticationType().length() == 0) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "authenticationType", "invalid value"));

        }

        if (rReq.getWhiteListStatusSource() != null && Misc.isNullOrEmpty(rReq.getWhiteListStatus()) &&
            rReq.getWhiteListStatusSource().length() == 0) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "whiteListStatusSource", "invalid value"));

        }

        String devCh = record.getDeviceChannel();
        boolean browser = DeviceChannel.cChannelBrow02.value().equals(devCh);
        boolean app = DeviceChannel.cChannelApp01.value().equals(devCh);
        boolean ri3 = DeviceChannel.cChannel3RI03.value().equals(devCh);
        boolean pa = TDSModel.XMessageCategory.C01.isEqual(rReq.getMessageCategory());
        boolean npa = TDSModel.XMessageCategory.C02.isEqual(rReq.getMessageCategory());
        boolean isDecoupled = TDSModel.XtransStatus.D.isEqual(record.getTransStatus());//should only be used when current protocol is 2.2 but i used it in general here. D is invalid for 2.1
        boolean strictValidation = validationContext.getBoolean(ValidationContext.STRICT_VALIDATION);

        if ((app || browser || ri3) && pa) {
            if (Strings.isEmpty(rReq.getTransStatus())) {
                errors.add(ErrorUtils.fieldMissingError(FieldName.transStatus));
            } else if (!transStatusValues21_22RReq.contains(rReq.getTransStatus())) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.transStatus, "is invalid value by DS rules"));
            }
        }
        //		String test = null;
        if (Misc.isNullOrEmpty(rReq.getAcsTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsTransID", "is missing"));
        }

        if ((app || browser) && (pa ||
                                 npa)) {//I coudln't find this detail about decoupled authentication in the protocol but there are several cases that depend on this and it makes alot of sense
            if (Misc.isNullOrEmpty(rReq.getInteractionCounter()) && !isDecoupled) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "interactionCounter", "is missing"));
            }
        }

        if (app && (pa || npa) && rReq.getAcsRenderingType() == null && !isDecoupled) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsRenderingType", "is missing"));
        }
        if (app && (pa || npa) && rReq.getAcsRenderingType() != null && !isDecoupled) {
            // TC_DS_10188_001
            boolean isObject = true;
            if (rReq.getJsonObject() != null) {
                JsonValue jv = rReq.getJsonObject().get("acsRenderingType");
                if (jv != null && ValueType.OBJECT != jv.getValueType()) {
                    errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "acsRenderingType",
                                         "is not json " + ValueType.OBJECT.name() + " but " +
                                         jv.getValueType().name()));
                    isObject = false;
                }
            }

            if (isObject) {
                if (Misc.isNullOrEmpty(rReq.getAcsRenderingType().getAcsInterface())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsRenderingType.acsInterface", "is missing"));
                }
                if (Misc.isNullOrEmpty(rReq.getAcsRenderingType().getAcsUiTemplate())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "acsRenderingType.acsUiTemplate", "is missing"));
                }
            }
        }

        if (Misc.isNullOrEmpty(rReq.getAuthenticationMethod())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "authenticationMethod", "is missing"));
        } else if (strictValidation && !XauthenticationMethod.contains(rReq.getAuthenticationMethod())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "authenticationMethod", "invalid or reserved value"));
        }

        if (TDSModel.XtransStatus.Y.isEqual(rReq.getTransStatus()) ||
            TDSModel.XtransStatus.A.isEqual(rReq.getTransStatus())) {
            boolean npaAvRequired =
                    npa && validationContext.getBoolean(ValidationContext.RREQ_NPA_AUTHENTICATION_REQUIRED);
            boolean ri3AvRequired =
                    ri3 && validationContext.getBoolean(ValidationContext.RREQ_3RI_AUTHENTICATION_REQUIRED);

            if (Misc.isNullOrEmpty(rReq.getAuthenticationValue()) && (pa || npaAvRequired || ri3AvRequired)) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "authenticationValue", "is missing"));
            }
        }
        if (TDSModel.XtransStatus.Y.isEqual(rReq.getTransStatus()) ||
            TDSModel.XtransStatus.N.isEqual(rReq.getTransStatus())) {
            if (Misc.isNullOrEmpty(rReq.getAuthenticationType())) {
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "authenticationType", "is missing"));
            } else if (!isValidAuthenticationType(rReq.getAuthenticationType(), validationContext)) {
                errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "authenticationType", "is invalid value by DS rules"));
            }
        }
        boolean transStatusReasonNpaRequired = validationContext.getBoolean(ValidationContext.RREQ_NPA_TRANS_STATUS_REASON);
        //backward compatible to settings-based customization but need to handle NPA
        if (ObjectUtils.isNotIn(rReq.getTransStatus(), NUR) || npa && !transStatusReasonNpaRequired) {
            if (rReq.getIncomingJsonMessage().isEmptyOrNullField(FieldName.transStatusReason)) {
                errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.transStatusReason));
            }
        } else {
            if (pa ||
                npa && transStatusReasonNpaRequired ||
                strictValidation) {
                if (Misc.isNullOrEmpty(rReq.getTransStatusReason())) {
                    errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "transStatusReason", "is missing"));
                }
            }
        }
        //backward compatible with settings-based customization `strictValidation`
        //do not want to touch the other field validation logic depending on this variable `strictValidation`
        if (validationContext.getBoolean(ValidationContext.STRICT_VALIDATION, true) &&
            Misc.isNotNullOrEmpty(rReq.getTransStatusReason()) &&
            !isDsReservedTransStatusReason(rReq.getTransStatusReason()) &&
            !isValidTransStatusReason(rReq.getTransStatusReason())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "transStatusReason", "invalid or reserved value"));
        }

        if (validationContext.getBoolean(ValidationContext.RREQ_ECI_REQUIRED) && Misc.isNullOrEmpty(rReq.getEci())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "eci", "is missing"));
        }

        if (Misc.isNullOrEmpty(rReq.getDsTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "DSTransID", "is missing"));
        }

        if (Misc.isNullOrEmpty(rReq.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion", "is missing"));
        }

        if (rReq.getTdsServerTransID() == null) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerTransID", "is missing"));
        }

        if (errors.size() == 0) {
            validateCriticalExtensions(rReq, errors, validationContext);
        }
        // validates edit criteria pattern and lengths as per Seq 5.21 this shall be after present validation
        if (errors.size() == 0) {
            mse.validateFieldSyntax(rReq, errors);
            if (errors.size() > 0) {
                return errors;
            }
        }

        // #106 NSPK extra validations
        if (errors.size() < 1) {
            String acceptedRReqStatuses = validationContext.get(ValidationContext.ACCEPTED_RREQ_STATUSES);
            if (Misc.isNotNullOrEmpty(acceptedRReqStatuses)) {
                String as = (";" + acceptedRReqStatuses + ";");
                String statusMix = ";" + rReq.getTransStatus() + "," +
                                   (Misc.isNullOrEmpty(rReq.getTransStatusReason()) ? "-" : rReq.getTransStatusReason()) + ";";
                String statusWld = ";" + rReq.getTransStatus() + ",*;";
                if (!(as.contains(statusMix) || as.contains(statusWld))) {
                    errors.add(new Error(ErrorCode.cInvalidTxData305, "transStatus, transStatusReason", "combination is invalid by DS rules"));
                }
            }
        }
        log.info("Has errors: " + errors.size()); // Extra log
        return errors;
    }

    protected List<Error> validateAReqAResMatch(TDSMessage aReq, TDSMessage aRes, ACSProfile acp, List<Error> errors) {
        StringBuilder idata = new StringBuilder();
        String fname = null;
        if (!aReq.getDsTransID().equals(aRes.getDsTransID())) {
            idata.append("dsTransID mismatch");
            fname = "dsTransID";
        }
        if (!aReq.getTdsServerTransID().equals(aRes.getTdsServerTransID())) {
            fname = "threeDServerTransID";
            if (idata.length() > 0) {
                idata.append(",");
            }

            idata.append("threeDSServerTransID mismatch");
        }
        if (Misc.isNotNullOrEmpty(aReq.getSdkTransID()) && !aReq.getSdkTransID().equals(aRes.getSdkTransID())) {
            fname = "sdkTransID";
            if (idata.length() > 0) {
                idata.append(",");
            }
            idata.append("sdkTransID mismatch");
        }

        if (idata.length() > 0) // EAP TC.DS.00230.001 301 instead of 305
        {
            errors.add(new Error(ErrorCode.cInvalidTransactionID301, fname, idata.toString()));
            idata.setLength(0);
        }

        if (Misc.isNotNullOrEmpty(acp.getRefNo()) && !DsStringUtils.splitToList(acp.getRefNo(), ",").contains(aRes.getAcsReferenceNumber())) {
            if (idata.length() > 0) {
                idata.append(",");
            }

            idata.append("mismatch");
            errors.add(new Error(ErrorCode.cAccessDenied303, "acsReferenceNumber", idata.toString()));
            idata.setLength(0);
        }

        if (Misc.isNotNullOrEmpty(aRes.getAcsOperatorID()) && Misc.isNotNullOrEmpty(acp.getOperatorID()) &&
            !aRes.getAcsOperatorID().equals(acp.getOperatorID())) {
            if (idata.length() > 0) {
                idata.append(",");
            }

            idata.append("acsOperatorID mismatch");
            errors.add(new Error(ErrorCode.cAccessDenied303, "acsOperatorID", idata.toString()));
        }
        return errors;
    }

    protected List<Error> validateRReqRResMatch(TDSMessage rReq, TDSMessage rRes, List<Error> errors) {
        log.info("Validate RReq RRes match"); // Extra log
        StringBuilder idata = new StringBuilder();
        if (!rReq.getDsTransID().equals(rRes.getDsTransID())) {
            idata.append("dsTransID mismatch");
        }
        if (!rReq.getTdsServerTransID().equals(rRes.getTdsServerTransID())) {
            if (idata.length() > 0) {
                idata.append(",");
            }
            idata.append("threeDSServerTransID mismatch");
        }
        if (rReq.getAcsTransID() != null && !rReq.getAcsTransID().equals(rRes.getAcsTransID())) {
            if (idata.length() > 0) {
                idata.append(",");
            }
            idata.append("acsTransID mismatch");
        }

        if (idata.length() > 0) {
            errors.add(new Error(ErrorCode.cInvalidTransactionID301, null, idata.toString()));
        }
        return errors;
    }

    /**
     * Default validation for DS Conditional fields after core DS RReq validation and PS RReq validation.
     * By default, DS Conditional fields are treated as Optional when validated.
     *
     * @param rReq
     * @param record
     * @return
     */
    protected List<Error> postValidateRReqDSConditional(TDSMessage rReq, TDSRecord record) {
        List<Error> errors = new ArrayList<>();

        boolean isAppDeviceChannel = DeviceChannel.cChannelApp01.isEqual(record.getDeviceChannel());
        boolean isBrowserDeviceChannel = DeviceChannel.cChannelBrow02.isEqual(record.getDeviceChannel());
        boolean is3RIDeviceChannel = DeviceChannel.cChannel3RI03.isEqual(record.getDeviceChannel());
        boolean isPa = TDSModel.XMessageCategory.C01.value().equals(record.getMessageCategory());
        boolean isNpa = TDSModel.XMessageCategory.C02.isEqual(record.getMessageCategory());

        JsonMessage rReqJsonMessage = rReq.getIncomingJsonMessage();
        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            rReqJsonMessage.isEmptyOrNullField(FieldName.authenticationValue)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.authenticationValue));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            rReqJsonMessage.isEmptyOrNullField(FieldName.eci)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.eci));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            (isPa || isNpa) &&
            rReqJsonMessage.isEmptyOrNullField(FieldName.messageExtension)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.messageExtension));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            rReqJsonMessage.isEmptyOrNullField(FieldName.transStatus)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.transStatus));
        }

        if ((isAppDeviceChannel || isBrowserDeviceChannel || is3RIDeviceChannel) &&
            isNpa &&
            rReqJsonMessage.isEmptyOrNullField(FieldName.transStatusReason)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.transStatusReason));
        }

        return errors;
    }

    private List<Error> validatePReq(TDSMessage pReq, ValidationContext validationContext) {
        List<Error> errors = new ArrayList<>();

        mse.validateCommons(pReq, errors);
        if (errors.size() > 0) {
            return errors;
        }

        if (Misc.isNullOrEmpty(pReq.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion", "is missing"));
        }

        if (Misc.isNullOrEmpty(pReq.getTdsServerTransID())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerTransID", "is missing"));
        }

        if (Misc.isNullOrEmpty(pReq.getTdsServerRefNumber())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "threeDSServerRefNumber", "is missing"));
        }

        if (pReq.getSerialNum() != null && pReq.getSerialNum().length() == 0) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, "serialNum", "invalid value"));
        }

        if (errors.size() == 0) {
            validateCriticalExtensions(pReq, errors, validationContext);
        }

        if (pReq.getIncomingJsonMessage().isEmptyOrNullField(FieldName.serialNum)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.serialNum));
        }

        // validates edit criteria pattern and lengths as per Seq 5.21 this shall be after present validation
        if (errors.isEmpty()) {
            mse.validateFieldSyntax(pReq, errors);
        }

        return errors;
    }

    protected List<Error> postValidatePReqDSConditional(TDSMessage pReq) {
        List<Error> errors = new ArrayList<>();

        JsonMessage pReqJsonMessage = pReq.getIncomingJsonMessage();
        if (pReqJsonMessage.isEmptyOrNullField(FieldName.messageExtension)) {
            errors.add(ErrorUtils.emptyOrNullFieldError(FieldName.messageExtension));
        }

        return errors;
    }

    /**
     * Payment specific validation can be performed by using 3DS Reference Number to identify Payment System that sends a PReq.
     */
    private void validatePReqPaymentSystemSpecific(Integer paymentSystemId, final TDSMessage pReq, final List<Error> errors) {
        try {
            PaymentSystemMessageProcessor paymentSystemMessageProcessor = PaymentSystemMsgProcessorRegistry.DEFAULT;
            PaymentSystem paymentSystem = persistenceService.getPersistableById(paymentSystemId, PaymentSystem.class);
            if (paymentSystem.getType() != null) {
                paymentSystemMessageProcessor = PaymentSystemMsgProcessorRegistry.get(paymentSystem.getType());
            }
            errors.addAll(paymentSystemMessageProcessor.validatePReq(pReq.getIncomingJsonMessage()));
        } catch (Exception e) {
            log.error("Exception during payment system specific PReq validation.", e);
            throw new RuntimeException(e);
        }
    }

    public TDSMessage createAResMessage(TDSMessage aReq, String uuid, TDSModel.XtransStatus txStatus, String statusReason, WebContext wctx, ValidationContext validationContext) {
        TDSMessage message = new TDSMessage();
        message.setTransStatus(txStatus != null ? txStatus.value() : null);
        message.setMessageVersion(aReq.getMessageVersion());
        message.setMessageType(XmessageType.A_RES.value);
        message.setDsReferenceNumber(getDSReferenceNumber(wctx));
        message.setDsTransID(uuid);

        // from areq
        message.setSdkTransID(aReq.getSdkTransID());
        message.setTdsServerTransID(aReq.getTdsServerTransID());
        message.setTransStatusReason(statusReason);

        // not available
        // https://www.emvco.com/wp-content/uploads/2018/04/EMVCo-Website-Content-2.6-3DS-Portal_FAQ_v2-2_PHASE2.pdf
        //1. What ACS Reference Number and ACS Transaction ID should be used when the
        //Directory Server (DS) creates the ARes message?
        //When the DS creates the ARes message, (including when the DS submits a
        //Transaction Status = A, indicating Attempts Processing Performed), the ACS
        //Reference Number provided in the ARes message will be the DS Reference
        //Number, and the ACS Transaction ID will be the DS Transaction ID
        message.setAcsTransID(message.getDsTransID());
        message.setAcsReferenceNumber(message.getDsReferenceNumber());

        if (validationContext.getBoolean(ARES_ECI_REQUIRED)) {
            if (TDSModel.XtransStatus.A.equals(txStatus)) {
                String transStatusAECI = validationContext.get(ARES_TRANS_STATUS_A_ECI);
                message.setEci(transStatusAECI == null ? "06" : transStatusAECI);
                String authenticationValue = validationContext.get(ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE);
                message.setAuthenticationValue(authenticationValue);
            } else if (TDSModel.XtransStatus.U.equals(txStatus)) {
                String transStatusUECI = validationContext.get(ARES_TRANS_STATUS_U_ECI);
                message.setEci(transStatusUECI == null ? "07" : transStatusUECI);
            }
        }

        return message;
    }

    public TDSMessage createRReqMessage(TDSMessage rReqErr, TDSRecord match, TDSModel.XtransStatus txst, String transStatusReason, TDSModel.ChallengeCancelInd2_0_1 cind, WebContext wctx) {
        TDSMessage message = new TDSMessage();
        message.setChallengeCancel(cind.value);

        if (rReqErr.getInteractionCounter() != null && rReqErr.getInteractionCounter().length() == 2) {
            message.setInteractionCounter(rReqErr.getInteractionCounter());
        } else {
            message.setInteractionCounter("01");
        }

        message.setMessageVersion(TDSModel.MessageVersion.V2_1_0.value());
        message.setMessageType(XmessageType.R_REQ.value);
        message.setMessageCategory(match.getMessageCategory());
        XrenderObj2 value = rReqErr.getAcsRenderingType();

        if (value == null) {
            value = new XrenderObj2();
            value.setAcsInterface("01");
            value.setAcsUiTemplate("04");
        }
        message.setAcsRenderingType(value);

        message.setSdkTransID(match.getSDKTransID());
        message.setDsReferenceNumber(getDSReferenceNumber(wctx));
        message.setDsTransID(match.getDSTransID());
        message.setTdsServerTransID(match.getTdsTransID());
        message.setTransStatus(txst.value());
        message.setTransStatusReason(transStatusReason);

        // not available
        String acsTransId = rReqErr.getAcsTransID();
        if (Misc.isNullOrEmpty(acsTransId) && match != null) {
            acsTransId = match.getACSTransID();
        }

        message.setAcsTransID(isUUID(acsTransId) ? acsTransId : createUUID(0, 0));

        String acsRef = rReqErr.getAcsReferenceNumber();
        if (Misc.isNullOrEmpty(acsRef) && match != null) {
            acsRef = match.getACSRefNo();
        }

        message.setAcsReferenceNumber(Misc.isNullOrEmpty(acsRef) ? "Not Available" : acsRef);

        return message;
    }

    static class TDSMessageExt extends TDSMessage {

        public void setXcardRangeData(List<XcardRangeData> list) {
            super.cardRangeData = list;
        }
    }

    protected void setDSProtocolVersions(TDSMessage pRes, WebContext webContext) {
        TDSModel.MessageVersion min = TDSModel.MessageVersion.minSupported();
        pRes.setDsStartProtocolVersion(min != null ? min.value() : getMessageService().getSupportedVersion());
        TDSModel.MessageVersion max = TDSModel.MessageVersion.maxSupported();
        pRes.setDsEndProtocolVersion(max != null ? max.value() : getMessageService().getSupportedVersion());
    }

    protected void setReadOrder(TDSMessage pRes) {
        // do nothing here. readOrder only exists for v2.3.0+
    }

    public TDSMessage createPResMessage(TDSMessage pReq, TDSRecord record, WebContext wctx) throws Exception {
        log.info("Create PRes message"); // Extra log
        if (Misc.isNotNullOrEmpty(pReq.getSerialNum())) {
            if (!directory.useDiffUpdates) {
                String errId = Context.getUnique();
                log.warn("Diff mode cardrange updates by serial not supported err id " + errId);
                return createErrorMessage(pReq.getMessageVersion(), ErrorCode.cInvalidSerialNum307, null,
                                          "Ranges partial updates not supported, retry without serial number error, ref " +
                                          errId, pReq);
            } else {
                // no updates input is same as latest in ds memory return empty ranges
                if (directory.cachedRangeDataSerial != null &&
                    directory.cachedRangeDataSerial.equals(pReq.getSerialNum())) {
                    TDSMessageExt pRes = new TDSMessageExt();
                    pRes.setMessageVersion(pReq.getMessageVersion());
                    pRes.setMessageType(XmessageType.P_RES.value());
                    pRes.setTdsServerTransID(pReq.getTdsServerTransID());
                    pRes.setDsTransID(record.getDSTransID());
                    pRes.setSerialNum(directory.cachedRangeDataSerial);

                    setDSProtocolVersions(pRes, wctx);
                    setReadOrder(pRes);

                    return pRes;
                }

                long s = System.currentTimeMillis();
                Map<Integer, PresCardRangeCache> history = directory.getRangesFromHistory(pReq.getSerialNum());
                if (history != null) {
                    TDSMessageExt pRes = new TDSMessageExt();
                    List<XcardRangeData> l = pRes.getCardRangeData();

                    PresCardRangeCache latestPresCardRangeCache = Optional
                            .ofNullable(directory.cache.getPresCardRangeCacheMap().get(wctx.getPaymentSystemId()))
                            .orElse(new PresCardRangeCache());

                    // removals no longer exist
                    PresCardRangeCache oldPresCardRangeCache = Optional
                            .ofNullable(history.get(wctx.getPaymentSystemId()))
                            .orElse(new PresCardRangeCache());
                    for (CardRange crx : oldPresCardRangeCache.values()) {
                        if (latestPresCardRangeCache.get(crx.getBinSt() + "-" + crx.getEnd()) == null) {
                            l.add(createRangeData(crx, wctx,false));
                        }
                    }
                    // new addons
                    for (CardRange crx : latestPresCardRangeCache.values()) {
                        if (oldPresCardRangeCache.get(crx.getBinSt() + "-" + crx.getEnd()) == null) {
                            l.add(createRangeData(crx, wctx,true));
                        }
                    }

                    // zero len array is error 203
                    if (l.size() == 0) {
                        pRes.setXcardRangeData(null);
                    }

                    pRes.setMessageVersion(pReq.getMessageVersion());
                    pRes.setMessageType(XmessageType.P_RES.value());
                    pRes.setTdsServerTransID(pReq.getTdsServerTransID());
                    pRes.setDsTransID(record.getDSTransID());
                    pRes.setSerialNum(directory.cachedRangeDataSerial);

                    setDSProtocolVersions(pRes, wctx);
                    setReadOrder(pRes);

                    long e = System.currentTimeMillis();
                    log.info("Created diff  PRes from " + pReq.getSerialNum() + " to " + pRes.getSerialNum() + " in " +
                             (e - s) + " ms (h=" + oldPresCardRangeCache.size() + "/c=" + latestPresCardRangeCache.size() + ")");

                    return pRes;
                } else {
                    String errId = Context.getUnique();
                    log.warn("Diff mode cardrange updates by serial " + pReq.getSerialNum() +
                             " can not be calculated ref not found, err id " + errId);
                    return createErrorMessage(pReq.getMessageVersion(), ErrorCode.cInvalidSerialNum307, null,
                                              "Ranges serial number provided no longer exists or incorrect, retry without serial number error, ref " +
                                              errId, pReq);
                }
            }
        } else if (cachedRangeDataRawMap.size() == 0 || (cachedRangeDataRawMap.get(wctx.getPaymentSystemId()) != null && cachedRangeDataRawMap.get(wctx.getPaymentSystemId()).getCount() > 0)) {
            TDSMessageExt pRes = new TDSMessageExt();
            pRes.setMessageVersion(pReq.getMessageVersion());
            pRes.setMessageType(XmessageType.P_RES.value());
            pRes.setTdsServerTransID(pReq.getTdsServerTransID());
            pRes.setDsTransID(record.getDSTransID());

            setDSProtocolVersions(pRes, wctx);
            setReadOrder(pRes);

            if (cachedRangeDataRawMap.size() != 0) {
                wctx.setRequestAttribute("pResUseRaw", Boolean.TRUE);
            }
            if (directory.useDiffUpdates) {
                pRes.setSerialNum(directory.cachedRangeDataSerial);
            }

            return pRes;
        } else {
            String errId = Context.getUnique();
            log.warn("Failed to create cardrangeData err id " + errId + " " +
                     (cachedRangeDataRawMap.get(wctx.getPaymentSystemId()) != null ? " empty " : " null "));
            return createErrorMessage(pReq.getMessageVersion(), ErrorCode.cTransientSysFailure403, null,
                                      "Ranges temporarily not available, retry error, ref " + errId, pReq);
        }
    }


    public void forwardAReqToACS(TDSRecord record, TDSMessage aReq, JsonObject aReqOutJ, Merchant m, List<ACSProfile> acsList, boolean attemptsACSAvailable, Results results, Timings timings, WebContext webContext, PaymentSystemMessageProcessor paymentSystemMessageProcessor) throws Exception {
        ValidationContext validationContext = new ValidationContext();
        validationContext.put(ValidationContext.STRICT_VALIDATION, webContext.getStringSetting(DsSetting.STRICT_VALIDATION.getKey()));
        validationContext.put(ValidationContext.EXTRA_AUTHENTICATION_TYPES, webContext.getStringSetting(DsSetting.EXTRA_AUTHENTICATION_TYPES.getKey()));
        validationContext.put(ValidationContext.ARES_NPA_AUTHENTICATION_REQUIRED, webContext.getStringSetting(DsSetting.ARES_NPA_AUTHENTICATION_VALUE_REQUIRED.getKey()));
        validationContext.put(ValidationContext.ARES_3RI_AUTHENTICATION_REQUIRED, webContext.getStringSetting(DsSetting.ARES_3RI_AUTHENTICATION_VALUE_REQUIRED.getKey()));
        validationContext.put(ValidationContext.ACCEPTED_ARES_STATUSES, webContext.getStringSetting(DsSetting.ACCEPTED_ARES_STATUSES.getKey()));
        validationContext.put(ARES_ECI_REQUIRED, webContext.getStringSetting(DsSetting.ARES_ECI_REQUIRED.getKey()));
        validationContext.put(ARES_TRANS_STATUS_A_ECI, webContext.getStringSetting(DsSetting.ARES_TRANS_STATUS_A_ECI.getKey()));
        validationContext.put(ARES_TRANS_STATUS_U_ECI, webContext.getStringSetting(DsSetting.ARES_TRANS_STATUS_U_ECI.getKey()));
        validationContext.put(ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE, webContext.getStringSetting(DsSetting.ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE.getKey()));

        JsonMessage aReqOutJsonMessage = jsonMessageService.parse(aReqOutJ.toString());
        paymentSystemMessageProcessor.processAReqOutgoing(aReqOutJsonMessage);
        PublicBOS aReqOut = jsonMessageService.writeOutputStream(new PublicBOS(512), aReqOutJsonMessage);
        TDSMessage acsResp;
        JsonAndDucplicates acsRespjsd;
        byte[] resp = null;
        timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);
        long start = System.currentTimeMillis();
        int timeoutsSum = 100;

        Map<String, String> resph = new HashMap<>(5);
        String host = "";
        URL acsUrl = null;

        Results originalAcsResult = null;

        for (int acsIndex = 0; acsIndex < acsList.size(); acsIndex++) {
            boolean respContentException = false;
            boolean connectionFailed;
            resph.clear();

            ACSProfile acp = acsList.get(acsIndex);
            HttpsJSSEClient client = null;
            try {
                // record outgoing version
                acsUrl = new URL(acp.getURL());
                host = acsUrl.getHost();
                this.createNewTDSMessageData(aReq, record, aReqOut, new java.util.Date(),
                                             "DS:" + getDSID(), "ACS:" + host, webContext);
                String kf = webContext.getStringSetting(DsSetting.SSL_CLIENT_KEYFACTORY.getKey());

                record.setACSURL(acp.getURL());
                // embed acs support
                if (acp.getURL().startsWith("embed")) {
                    log.info("Sending " + aReq.getMessageType() + " " + aReq.getTdsServerTransID() + "/" +
                             aReq.getDsTransID() + " to " +
                             (acp.getIssuerId() != null ? "Issuer (" + acp.getIssuerId() + ")" : "Attempts") +
                             " ACS (" + acp.getId() + ") " + acp.getURL());

                    if (virtualAcsService == null) {
                        log.warn("Virtual embed attempts ACS service not configured, skipping");
                        continue;
                    }
                    resp = virtualAcsService.processAReq(aReq, webContext);
                } else {
                    Long finalId = null;
                    String defaultACSAuthCertId = webContext.getStringSetting(DsSetting.ACS_SSL_CLIENT_CERTID.getKey());
                    Long defaultACSAuthCertIdL = null;
                    if (Misc.isLong(defaultACSAuthCertId)) {
                        defaultACSAuthCertIdL = Misc.parseLongBoxed(defaultACSAuthCertId);
                    }

                    if (acp.getOutClientCert() != null || defaultACSAuthCertIdL != null) {
                        finalId = acp.getOutClientCert() != null ? acp.getOutClientCert() : defaultACSAuthCertIdL;
                    }

                    final String ckey =
                            acp.getId() + " kid: " + finalId + " url:" + acp.getURL() + " (" + acp.getSSLProto() + ")";
                    client = directory.getHttpsJSSEClient(ckey);
                    boolean newcl = false;
                    if (client == null) {
                        String sslContext = acp.getSSLProto();
                        if (Misc.isNullOrEmpty(sslContext)) {
                            sslContext = webContext.getStringSetting(DsSetting.ACS_SSL_CLIENT_PROTO.getKey());
                        }
                        if (Misc.isNullOrEmpty(sslContext)) {
                            sslContext = SSLSocketHelper.SSLProto.TLSv12;
                        }
                        // for testing only!!!
                        String sslTrustAny = webContext.getStringSetting(DsSetting.ACS_SSL_CLIENT_TRUSTANY.getKey());
                        String sslVerifyHostName = webContext.getStringSetting(DsSetting.ACS_SSL_CLIENT_VERIFY_HOSTNAME.getKey());

                        PrivateKey pk = null;
                        X509Certificate[] chain = null;
                        KeyStore trustStore = null;

                        if (acp.getURL().toLowerCase().startsWith("https://")) {
                            trustStore = keyService.getAllTrustedRootsAsKeyStore();
                            KeyData clientKeyAndCert = keyService.getPrivateKeyAndCertChainById(finalId);
                            if (clientKeyAndCert != null) {
                                byte[] privateKeyBytes  = cryptoService.unwrapPrivateKey(clientKeyAndCert);
                                HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(clientKeyAndCert.getHsmDeviceId());
                                pk = hsmDevice.toRSAprivateKey(privateKeyBytes);

                                String certChainPem = KeyService.parsePEMChain(clientKeyAndCert.getKeyData());
                                chain = KeyService.parseX509Certificates(certChainPem.getBytes(StandardCharsets.ISO_8859_1));
                            } else {
                                log.warn("Auth to ACS Client cert/key not found by id " + finalId);
                            }
                        }

                        int connectTimeout = webContext.getIntSetting(DsSetting.CONNECT_TIMEOUT.getKey());
                        if (connectTimeout < cDefaultConnectTimeout / 2) {
                            connectTimeout = cDefaultConnectTimeout;
                        }

                        int acsTimeout = webContext.getIntSetting(DsSetting.ACS_TIMEOUT.getKey());
                        if (acsTimeout < cDefaultResponseTimeout / 2) {
                            acsTimeout = cDefaultResponseTimeout;
                        }
                        // 3.5.9 if acstimeout - spent on ext fss > default then safely adjust acs timeout
                        acsTimeout = acsTimeout - timings.extSum > cDefaultResponseTimeout ?
                                     acsTimeout - timings.extSum : cDefaultResponseTimeout;

                        timeoutsSum += connectTimeout;
                        timeoutsSum += acsTimeout;

                        client = new HttpsJSSEClient();
                        String[] sslCiphers = Misc.split(webContext.getStringSetting(DsSetting.ACS_SSL_CLIENT_CIPHERS.getKey()), ",");
                        if (Misc.isNotNullOrEmpty(sslCiphers)) {
                            client.setCiphers(sslCiphers);
                        }
                        client.init(null, Misc.isNullOrEmpty(kf) ? KeyManagerFactory.getDefaultAlgorithm() : kf, sslContext, "true".equals(sslTrustAny) ? "true" : "false", pk, chain, trustStore, connectTimeout, acsTimeout, !"false".equals(sslVerifyHostName));
                        configureHttpClient(client, webContext);
                        newcl = true;
                    } // cached client

                    timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);

                    log.info("Sending " + aReq.getMessageType() + " " + aReq.getTdsServerTransID() + "/" +
                             aReq.getDsTransID() + " to " +
                             (acp.getIssuerId() != null ? "Issuer (" + acp.getIssuerId() + ")" : "Attempts") + " ACS " +
                             ckey + ", using client key " + (client.isClientKeySet() ? "yes " + finalId : "no"));

                    for (int a = 0; a < 2; a++) {
                        try {
                            resp = client.post(acp.getURL(), MessageService.CT_JSON_UTF8, aReqOut.getBuf(), 0, aReqOut.getCount(), null, resph);
                            break;
                        } catch (Exception cfe) {
                            //immediate connect/ssql failed retry Seq 5.50 [Req 233]
                            connectionFailed = isConnectionFailedException(cfe);
                            if (connectionFailed && a < 1) {
                                log.warn("First connect attempt to " + acp.getURL() + " failed " + cfe +
                                         ", making next auto attempt [Req 233]");
                                continue;
                            } else {
                                throw cfe;
                            }
                        }
                    }

                    if (newcl) {
                        directory.putHttpsJSSEClient(ckey, client);
                    }
                }

                timings.resStart = (int) (System.currentTimeMillis() - timings.reqStart);
                String rspCode = resph.get("HTTP_RESPONSE");
                String rspMsg = resph.get("HTTP_RESPONSE_MSG");
                String rspCt = resph.get("Content-Type");
                try {
                    if (resp == null || resp.length < 1) {
                        throw new RuntimeException(
                                "No response content from ACS" + (rspCode != null ? ", HTTP Code " + rspCode : ""));
                    } else {
                        acsRespjsd = MessageService.parseJsonAndDups(resp);
                        acsResp = mse.fromJSON(acsRespjsd.json);
                        acsResp.setJsonObject(acsRespjsd.json);
                        acsResp.setDuplicateCounts(acsRespjsd.dups);
                        acsResp.setIncomingJsonMessage(jsonMessageService.parse(acsRespjsd.json.toString()));
                    }
                } catch (Exception e) {
                    log.info("No response content from ACS:" + e.getMessage()); // Extra log
                    respContentException = true;
                    String detailErr = resph.get("errorData");
                    log.error("Error with ACS " + acp.getId() + " " + acp.getURL() +
                              (rspCode != null ? ", HTTP Code " + rspCode : "") +
                              (rspMsg != null ? ", HTTP Msg " + rspMsg : "") +
                              (rspCt != null ? ", HTTP content-type " + rspCt : "") +
                              (detailErr != null ? ", Err content: " + detailErr : "") + ", unable to parse message " +
                              e, e);
                    String msgStr = null;
                    if (resp != null) {
                        msgStr = new String(resp, StandardCharsets.UTF_8);
                        if (msgStr.length() > 5) {
                            String mask = Misc.mask(aReq.getAcctNumber(), 3);
                            msgStr = Misc.replace(msgStr, aReq.getAcctNumber(), mask);
                        }
                    }

                    log.info("Invalid tdsMessageBase content='" + msgStr + "'"); // Extra log
                    log.error("Invalid tdsMessageBase content='" + msgStr + "'");
                    record.setLocalStatus(DSModel.TSDRecord.Status.ACSERROR);
                    if ((resp == null || resp.length < 1) && rspCode != null) {
                        resp = ("No content, HTTP code: " + rspCode + (rspMsg != null ? ", HTTP Msg: " + rspMsg : "") +
                                (rspCt != null ? ", HTTP content-type " + rspCt : "") + (detailErr != null ?
                                                                                         ", Err content: " +
                                                                                         detailErr : "")).getBytes(StandardCharsets.UTF_8);
                    }
                    if (resp != null && resp.length > 0) {
                        TDSMessage info = new TDSMessage();
                        createNewTDSMessageData(info, record, resp, new java.util.Date(),
                                                "ACS:" + host, "DS:" + getDSID(), webContext);
                        info.setMessageType("RAW");
                    }

                    log.info("Response equal to null"); // Extra log
                    throw e;
                }
                log.info("Got resp from ACS");
                TDSMessageData aResMsgData = createNewTDSMessageData(acsResp, record, resp, new java.util.Date(),
                                                                     "ACS:" + host, "DS:" + getDSID(), webContext);
                log.info("Message Version: " + aResMsgData.getMessageVersion());
                JsonObjectBuilder aResOut = null;
                // validate ares
                if (!mse.isErrorMessage(acsResp)) {
                    List<Error> errors = this.validateAResEmv(acsResp, aReq, validationContext);

                    if (errors.isEmpty()) {
                        validateAReqAResMatch(aReq, acsResp, acp, errors);
                    }

                    if (errors.isEmpty()) {
                        errors.addAll(paymentSystemMessageProcessor.validateAResIncoming(acsResp.getIncomingJsonMessage(), aReq.getIncomingJsonMessage()));
                    }

                    if (errors.isEmpty()) {
                        errors.addAll(postValidateAResDSConditional(acsResp, aReq));
                    }

                    if (errors.isEmpty()) {
                        if (extensionService != null) {
                            long timeLeft = timeoutsSum - (System.currentTimeMillis() - start);
                            long extStart = System.currentTimeMillis();
                            extensionService.createAResExtensions(record, results, m, acsResp, timeLeft, this, webContext);
                            timings.extSum += (int) (System.currentTimeMillis() - extStart);
                        }
                        aResOut = createFilteredJson(acsResp, acsRespjsd.json, results.aresExtensions, record, !mse.isTreatEmptyValueAsMissing() ? errors : null);
                        if (Misc.isNotNullOrEmpty(acsResp.getAcsOperatorID())) {
                            aResOut.add("acsOperatorID", acsResp.getAcsOperatorID());
                        }
                    }

                    boolean attemptACSOnAResValidationError = "true".equals(webContext.getStringSetting(DsSetting.ATTEMPT_ACS_ON_ARES_VALIDATION_ERROR.getKey()));
                    if (!errors.isEmpty()) {
                        Error e1 = errors.get(0);
                        log.warn("ACS ARes to 3DS had errors, first error " + e1.getCode() + " field name '" +
                                 e1.getFieldName() + "' with description '" + e1.getDescription() + "'");
                        // NSPK #106
                        String statusACSError = null;
                        boolean returnRs = false;
                        TDSMessage errorMsg = null;
                        TDSMessage respMsg = null;
                        if (TDSModel.ErrorCode.cInvalidTxData305 == e1.getCode() &&
                            "transStatus, transStatusReason".equals(e1.getFieldName()) &&
                            Misc.isNotNullOrEmpty(statusACSError = webContext.getStringSetting(DsSetting.STATUS_ACS_ERROR.getKey())) ||
                            ("acsReferenceNumber".equals(e1.getFieldName()) ||
                             "acsOperatorID".equals(e1.getFieldName())) &&
                            Misc.isNotNullOrEmpty(statusACSError = webContext.getStringSetting(DsSetting.STATUS_ACS_IDS_ERROR.getKey()))) {
                            TDSModel.XtransStatus xtstaus = TDSModel.XtransStatus.U;
                            try {
                                xtstaus = TDSModel.XtransStatus.valueOf(statusACSError);
                            } catch (Exception de) {
                                log.warn("statusACSError " + statusACSError + " unknown opting to default " +
                                         xtstaus.value());
                            }
                            String statusReasonValue = XtransStatusReason.C01AuthFailed.value;
                            String statusReasonACSError = webContext.getStringSetting(DsSetting.STATUS_REASON_ACS_ERROR.getKey());
                            if (Misc.isNotNullOrEmpty(statusReasonACSError) && statusReasonACSError.length() == 2) {
                                statusReasonValue = statusReasonACSError;
                            }

                            if (Misc.isNullOrEmpty(statusReasonValue) ||
                                Misc.in(xtstaus, new Object[]{TDSModel.XtransStatus.N, TDSModel.XtransStatus.U, TDSModel.XtransStatus.R})) {
                                statusReasonValue = TDSModel.XtransStatusReason.C01AuthFailed.value;
                            }

                            respMsg = createAResMessage(aReq, record.getDSTransID(), xtstaus, statusReasonValue, webContext, validationContext);
                            if (isUUID(acsResp.getAcsTransID())) {
                                respMsg.setAcsTransID(acsResp.getAcsTransID());
                            }

                            log.warn("Created " + respMsg.getMessageType() + " to 3DS with transStatus " +
                                     respMsg.getTransStatus() + " " + respMsg.getTransStatusReason());
                            returnRs = true;
                        } else if (!attemptACSOnAResValidationError) {
                            //String v, ErrorCode ecode, String mtsrc, String descr, CharSequence detail,	String[] sdkIdTDSIdDSIdSACSId
                            errorMsg = createErrorMessage(e1.getCode(), getMessageVersion(), acsResp.getMessageType(),
                                                          Misc.merge(e1.getFieldName(), e1.getDescription()) +
                                                          " (in ARes from ACS)", this.createErrorDetails(errors), new String[]{record.getSDKTransID(), record.getTdsTransID(), record.getDSTransID(), acsResp.getAcsTransID()});//
                            //acsResp);
                            //errorMsg=acsResp;
                            log.warn(
                                    "Created " + errorMsg.getMessageType() + " to 3DS with " + errorMsg.getErrorCode() +
                                    " " + errorMsg.getErrorDetail());
                            returnRs = true;
                        }

                        if (!returnRs && acsIndex == acsList.size() - 1) {
                            returnRs = true;
                        }

                        // copy error to ACS (part of ends processing defintion)
                        String onErr = webContext.getStringSetting(DsSetting.ARES_IN_ERROR_SEND_ERRO_ACS.getKey());
                        String toACS = "";
                        PublicBOS errorBos = null;
                        if (!"false".equals(onErr) && client != null) {
                            if (errorMsg == null) {
                                //errorMsg=createErrorMessage(mversion, e1.getCode(),
                                //	Misc.merge(e1.getFieldName(), e1.getDescription()) + " (in ARes from ACS)", this.createErrorDetails(errs), acsResp);
                                errorMsg = createErrorMessage(e1.getCode(), getMessageVersion(), acsResp.getMessageType(),
                                                              Misc.merge(e1.getFieldName(), e1.getDescription()) +
                                                              " (in ARes from ACS)", this.createErrorDetails(errors), new String[]{record.getSDKTransID(), record.getTdsTransID(), record.getDSTransID(), acsResp.getAcsTransID()});
                            } else if (returnRs) {
                                toACS = "ACS:" + host + ";";
                            }

                            errorBos = mse.toJSONBos(errorMsg);
                            if (errorMsg != null) // && !returnRs)
                            {
                                createNewTDSMessageData(errorMsg, record, errorBos, new java.util.Date(),
                                                        "DS:" + getDSID(), "ACS:" + host, webContext);
                            }

                            sendForkedErrorMessage(acsUrl, "ACS", client, errorBos, record, webContext);
                        }

                        if (respMsg != null) {
                            results.jsonRaw = mse.toJSONBos(respMsg);
                            results.tdsMessageBase = respMsg;
                            createNewTDSMessageData(respMsg, record, results.jsonRaw, new java.util.Date(),
                                                    "DS:" + getDSID(), toACS + "3DS:" +
                                                                       webContext.getRequest().getRemoteAddr(), webContext);
                        } else if (errorMsg != null && (!attemptsACSAvailable || !attemptACSOnAResValidationError)) {
                            //There is an Error in ARes received from this ACS
                            //and attemptsACS is not available (this covers when message is NPA)
                            //or attemptACSOnAResValidationError=false
                            //then return this Error as result to 3DSS
                            results.jsonRaw = errorBos != null ? errorBos : mse.toJSONBos(errorMsg);
                            results.tdsMessageBase = errorMsg;
                            createNewTDSMessageData(errorMsg, record, results.jsonRaw, new java.util.Date(),
                                                    "DS:" + getDSID(), toACS + "3DS:" +
                                                                       webContext.getRequest().getRemoteAddr(), webContext);
                        } else if (errorMsg != null && attemptsACSAvailable
                                   && (acsIndex == (acsList.size() - 1))) {
                            //There is an Error in ARes received from this ACS
                            //and an AACS is registered under the issuer
                            //and if this current ACS is the AACS already
                            //then return this Error to 3DSS
                            results.jsonRaw = errorBos != null ? errorBos : mse.toJSONBos(errorMsg);
                            results.tdsMessageBase = errorMsg;
                            createNewTDSMessageData(errorMsg, record, results.jsonRaw, new java.util.Date(),
                                                    "DS:" + getDSID(), toACS + "3DS:" +
                                                                       webContext.getRequest().getRemoteAddr(), webContext);
                        }

                        //#1182, temporarily store original ACS ARes with Error while waiting for the last ACS connection
                        if (errorMsg != null && (acsIndex == 0)) {
                            originalAcsResult = new Results();
                            originalAcsResult.jsonRaw = errorBos != null ? errorBos : mse.toJSONBos(errorMsg);
                            originalAcsResult.tdsMessageBase = errorMsg;
                        }

                        if (returnRs) {
                            record.setLocalStatus(DSModel.TSDRecord.Status.ACSERROR);
                            record.setLocalDateEnd(new java.util.Date());
                            if (errorMsg != null) {
                                record.setErrorCode(Misc.cut(errorMsg.getErrorCode(), 3));
                                record.setErrorDetail(Misc.cut(errorMsg.getErrorDetail(), 2048));
                            }
                            safeUpdate(record, -1, webContext);
                            timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
                            return;
                        }
                    } // errs > 0

                    // from acs
                    log.info("ACS resp get Message version" + acsResp.getMessageVersion()); // Extra log
                    record.setACSTransID(acsResp.getAcsTransID());
                    record.setACSRefNo(acsResp.getAcsReferenceNumber());
                    record.setACSOperatorID(acsResp.getAcsOperatorID());
                    // because of optional nature
                    if (Misc.isNullOrEmpty(record.getACSOperatorID()) && Misc.isNotNullOrEmpty(acp.getOperatorID())) {
                        record.setACSOperatorID(acp.getOperatorID());
                    }
                    record.setTransStatus(acsResp.getTransStatus());
                    record.setTransStatusReason(acsResp.getTransStatusReason());
                    record.setAuthenticationMethod(acsResp.getAuthenticationMethod());
                    record.setAuthenticationType(acsResp.getAuthenticationType());
                    boolean isStoreAuthValue = webContext.isTrueSetting(DsSetting.STORE_AUTHENTICATION_VALUE.getKey());
                    if (isStoreAuthValue) {
                        try {
                            if (StringUtils.isNotEmpty(acsResp.getAuthenticationValue())) {
                                String encryptedAuthValue = cryptoService.encryptData(acsResp.getAuthenticationValue());
                                tdsRecordAttributeService.saveAuthenticationValue(encryptedAuthValue, record);
                            }
                        } catch (Exception e) {
                            log.error("Error on saving record attribute.", e);
                        }
                    }

                    record.setECI(acsResp.getEci());
                    if (TDSModel.XtransStatus.C.isEqual(acsResp.getTransStatus()) ||
                        TDSModel.XtransStatus.D.isEqual(acsResp.getTransStatus()))// transStatus D means the tx is not done, its another form of challenge so there will be rreq/rres. maybe we need to add a different local status for D
                    {
                        record.setLocalStatus(DSModel.TSDRecord.Status.CHANLLENGE_INPROCESS);
                    } else {
                        record.setLocalStatus(DSModel.TSDRecord.Status.COMPLETED);
                    }

                    // other attempts rules
                    if (attemptsACSAvailable && acsIndex < acsList.size() - 1) {
                        String message = null;
                        if (!errors.isEmpty() && attemptACSOnAResValidationError) {
                            message = "Internal trigger to attempts ACS due to ARes validation errors";
                            log.info(message);
                        } else if (!Misc.in(acsResp.getTransStatus(), YCA) && "true".equals(webContext.getStringSetting(
                                "attemptStatusCondition." + acsResp.getTransStatus() + "." +
                                acsResp.getTransStatusReason()))) {
                            log.info("Trigger " + record.getId() + "/" + record.getDSTransID() +
                                     " to attempts ACS, condition status=" + acsResp.getTransStatus() +
                                     ", status reason=" + acsResp.getTransStatusReason());
                            message = "Internal trigger to attempts ACS due to condition status=" +
                                      acsResp.getTransStatus() + " status reason=" + acsResp.getTransStatusReason();
                        }

                        if (message != null) {
                            record.setLocalStatus(DSModel.TSDRecord.Status.INPROCESS);
                            TDSMessage info = new TDSMessage();
                            info.setMessageType("DSINFO");
                            byte[] msgRawBuf = message.getBytes(StandardCharsets.UTF_8);
                            createNewTDSMessageData(info, record, msgRawBuf, new java.util.Date(), "-", "-", webContext);
                            acsIndex = acsList.size() - 2; // +1 will add on ending this cycle. by continue
                            continue;
                        }
                    }
                } // ARes
                else // Erro
                {
                    record.setLocalStatus(DSModel.TSDRecord.Status.ACSERROR);
                    record.setErrorCode(Misc.cut(acsResp.getErrorCode(), 3));
                    record.setErrorDetail(Misc.cut(acsResp.getErrorDetail(), 2048));

                    boolean failoverError = "true".equals(webContext.getStringSetting(
                            "failoverErrorCondition." + acsResp.getErrorCode()));
                    if (failoverError && acsIndex < acsList.size() - 1) {
                        boolean at = attemptsACSAvailable && acsIndex == acsList.size() - 2;
                        record.setLocalStatus(DSModel.TSDRecord.Status.INPROCESS);
                        String infoStr = "DS Failover " + record.getId() + "/" + record.getDSTransID() + " to " +
                                         (at ? "attempts" : "next") + " ACS, due error(set up to failover) " +
                                         acsResp.getErrorCode() + ", descr " + acsResp.getErrorDescription();
                        log.info(infoStr);
                        TDSMessage info = new TDSMessage();
                        info.setMessageType("DSINFO");
                        byte[] msgRawBuf = (infoStr.getBytes(StandardCharsets.UTF_8));
                        log.info("calling createNewTDSMessageData");// Extra log
                        createNewTDSMessageData(info, record, msgRawBuf, new java.util.Date(), "-", "-", webContext);
                        continue;
                    }

                    if (extensionService != null) {
                        long timeLeft = timeoutsSum - (System.currentTimeMillis() - start);
                        long extStart = System.currentTimeMillis();
                        extensionService.createAResExtensions(record, results, m, acsResp, timeLeft, this, webContext);
                        timings.extSum += (int) (System.currentTimeMillis() - extStart);
                    }
                    List<Error> errors = mse.isTreatEmptyValueAsMissing() ? null : validateErroIncoming(acsResp, aReq);
                    if (CollectionUtils.isEmpty(errors)) {
                        aResOut = createFilteredJson(acsResp, acsRespjsd.json, results.aresExtensions, record, null);
                    } else {
                        prepareNewErroResult(record, aReq, results, webContext, acsResp, errors);
                        return;
                    }
                }
                if (aResOut != null) {
                    //custom filtering of ARes Out
                    JsonMessage aResOutJsonMessage = this.jsonMessageService.parse(aResOut.build().toString());
                    paymentSystemMessageProcessor.processAResOutgoing(aResOutJsonMessage, aReq.getIncomingJsonMessage());
                    PublicBOS arescopy = jsonMessageService.writeOutputStream(new PublicBOS(512), aResOutJsonMessage);
                    results.jsonRaw = arescopy;
                    if (isArraysEqualsLength(resp, arescopy.getBuf(), arescopy.getCount())) {
                        aResMsgData.setDestIP("DS:" + getDSID() + "; 3DS:" + webContext.getRequest().getRemoteAddr());
                        this.safeUpdate(aResMsgData, -2, webContext);
                    } else {
                        // if indifferent store new copy
                        this.createNewTDSMessageData(acsResp, record, arescopy, new java.util.Date(),
                                                     "DS:" + getDSID(),
                                                     "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);
                    }
                }

                record.setLocalDateEnd(new java.util.Date());
                safeUpdate(record, -1, webContext);
                timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
                results.tdsMessageBase = acsResp;
                return;
            } catch (Exception e) {
                String errId = Context.getUnique();
                String em = "Error id " + errId + " in sending AReq To ACS " + acp.getId();
                //String detailResp = resph.get("errorData");
                connectionFailed = this.isConnectionFailedException(e);
                //log.error(em + (detailResp != null ? ", errors received: " + detailResp : ""), e);
                log.error(em, e);

                // last one
                if (acsIndex >= acsList.size() - 1) {
                    //Seq 5.50 [Req 233]
                    String detail = "ACS Connection failed";
                    if (this.isReadTimeoutException(e)) {
                        detail = "ACS Connection read timeout";
                        log.info("ACS returned invalid content, timeout"); // Extra log
                        log.info(""); // Extra log
                    }
                    if (respContentException) {
                        log.info("ACS returned invalid content, not timeout"); // Extra log
                        detail = "ACS returned invalid content";
                    }
                    em = Misc.merge(detail, em);
                    record.setLocalStatus(DSModel.TSDRecord.Status.ACSCOMERROR);

                    String statusACSError = webContext.getStringSetting(DsSetting.STATUS_ACS_ERROR.getKey());
                    String toACS = "";
                    if (Misc.isNullOrEmpty(statusACSError)) {
                        ErrorCode ec = connectionFailed ? ErrorCode.cSysConnectionFailure405 : ErrorCode.cTransTimedOut402;
                        if (respContentException) {
                            ec = ErrorCode.cInvalidMessageType101;
                            acsResp = createErrorMessage(getMessageVersion(), ec, null, null, detail, aReq);
                        } else {
                            acsResp = createErrorMessage(getMessageVersion(), ec, null, detail, aReq);
                        }
                        record.setErrorCode(ec.value);
                    } else {
                        TDSModel.XtransStatus xtstaus = TDSModel.XtransStatus.U;
                        try {
                            xtstaus = TDSModel.XtransStatus.valueOf(statusACSError);
                        } catch (Exception de) {
                            log.warn("statusACSError " + statusACSError + " unknown opting to default " +
                                     xtstaus.value());
                        }
                        String statusReasonValue = XtransStatusReason.C01AuthFailed.value;
                        String statusReasonACSError = webContext.getStringSetting(DsSetting.STATUS_REASON_ACS_ERROR.getKey());
                        if (Misc.isNotNullOrEmpty(statusReasonACSError) && statusReasonACSError.length() == 2) {
                            statusReasonValue = statusReasonACSError;
                        }

                        acsResp = createAResMessage(aReq, record.getDSTransID(), xtstaus, statusReasonValue, webContext, validationContext);
                    }


                    if (extensionService != null) {
                        long timeLeft = timeoutsSum - (System.currentTimeMillis() - start);
                        long extStart = System.currentTimeMillis();
                        extensionService.createAResExtensions(record, results, m, acsResp, timeLeft, this, webContext);
                        timings.extSum += (int) (System.currentTimeMillis() - extStart);
                        if (results.aresExtensions != null) {
                            JsonObjectBuilder acsRespJob = mse.toJSONOB(acsResp);
                            MessageService.addExtensions(acsRespJob, results.aresExtensions);
                            results.jsonRaw = mse.toJSONBos(acsRespJob.build());
                        }
                    }

                    if (results.jsonRaw == null) {
                        results.jsonRaw = mse.toJSONBos(acsResp);
                    }
                    results.tdsMessageBase = acsResp;
                    String onErr = webContext.getStringSetting(DsSetting.ARES_IN_ERROR_SEND_ERRO_ACS.getKey());

                    if (!"false".equals(onErr) && client != null && "Erro".equals(acsResp.getMessageType())) {
                        toACS = "ACS:" + host + ";";
                        final PublicBOS errorBos = results.jsonRaw;
                        sendForkedErrorMessage(acsUrl, "ACS", client, errorBos, record, webContext);
                    }
                    //#1182, send the original ACS Response Message to 3DSS if last ACS fails with connection error
                    if (connectionFailed && originalAcsResult != null) {
                        results.tdsMessageBase = originalAcsResult.tdsMessageBase;
                        results.jsonRaw = originalAcsResult.jsonRaw;
                        toACS = "ACS:" + new URL(acsList.get(0).getURL()).getHost() + ";";
                    }

                    createNewTDSMessageData(results.tdsMessageBase, record, results.jsonRaw, new java.util.Date(),
                                            "DS:" + getDSID(),
                                            toACS + "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);


                    if (record.getErrorCode() == null) {
                        record.setErrorCode(ErrorCode.cSysConnectionFailure405.value);
                    }
                    record.setErrorDetail(Misc.cut(em + " " + e, 2048));
                    record.setLocalDateEnd(new java.util.Date());
                    safeUpdate(record, -1, webContext);
                    timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
                    diagnoseAReqTransactionException(e, record, webContext.getLocale());
                    return;
                } else {
                    boolean at = attemptsACSAvailable && acsIndex == acsList.size() - 2;
                    TDSMessage info = new TDSMessage();
                    info.setMessageType("DSINFO");
                    byte[] msgRawBuf = ("Fallover to " + (at ? "attempts" : "next") + " ACS, due error " +
                                        Misc.cut(em + " " + e, 2048)).getBytes(StandardCharsets.UTF_8);
                    createNewTDSMessageData(info, record, msgRawBuf, new java.util.Date(), "-", "-", webContext);
                }
            }
        } // for
    }

    // When an Erro message received by DS contains invalid values (missing fields, wrong format). DS creates
    // a new Erro message that contains the issue in the `original` Erro
    private void prepareNewErroResult(TDSRecord record, TDSMessage aReq, Results results, WebContext webContext, TDSMessage acsResp, List<Error> errors) throws Exception {
        Error e1 = getFirstError(errors);
        String messageVersion = aReq.getMessageVersion();
        TDSMessage erroOut = createErrorMessage(messageVersion, e1.getCode(), null, this.createErrorDetails(errors),
                                                acsResp);
        results.jsonRaw = mse.toJSONBos(erroOut);
        createNewTDSMessageData(erroOut, record, results.jsonRaw, new Date(), "DS:" + getDSID(),
                                "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);
        record.setLocalStatus(TSDRecord.Status.ACSERROR);
        record.setLocalDateEnd(new Date());
        record.setErrorCode(Misc.cut(erroOut.getErrorCode(), FieldMaxLength.ERROR_CODE));
        record.setErrorDetail(Misc.cut(erroOut.getErrorDetail(), FieldMaxLength.ERROR_DETAIL));
        safeUpdate(record, -1, webContext);
        results.tdsMessageBase = erroOut;
    }

    private void diagnoseAReqTransactionException(Exception transactionException, TDSRecord record, Locale locale) {
        LocalizationService localizationService = ServiceLocator.getInstance().getLocalizationService();
        if (transactionException instanceof KeyDataIdNotFoundException) {
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_DESCRIPTION, record.getId(), localizationService.getText(
                    "text.err.desc.invalidClientCertId", new Object[]{DsSetting.ACS_SSL_CLIENT_CERTID.getKey()}, locale));
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_RESOLUTION, record.getId(), localizationService.getText(
                    "text.err.resolution.invalidClientCertId", new Object[]{DsSetting.ACS_SSL_CLIENT_CERTID.getKey(), KeyService.KeyAlias.clientAuthCert}, locale));
        } else if (isReadTimeoutException(transactionException)) {
                tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_DESCRIPTION, record.getId(), localizationService.getText(
                        "text.err.desc.AReqReadTimeout", new Object[]{record.getACSURL()}, locale));
                tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_RESOLUTION, record.getId(), localizationService.getText(
                        "text.err.resolution.AReqReadTimeout", new Object[]{record.getACSURL()}, locale));
        } else if (isConnectionFailedException(transactionException)) {
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_DESCRIPTION, record.getId(), localizationService.getText(
                    "text.err.desc.AReqConnectTimeout", new Object[]{record.getACSURL()}, locale));
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_RESOLUTION, record.getId(), localizationService.getText(
                    "text.err.resolution.AReqConnectTimeout", new Object[]{record.getACSURL()}, locale));
        }
    }

    protected String getMessageVersion() {
        return MessageVersion.V2_1_0.value();
    }

    public Results forwardRReqToMI(TDSRecord record, TDSMessage rReq, byte[] origRReq, byte[] rReqRawNew, TDSMessageData rreqData, TDSServerProfile tdsServerProfile, boolean needResult, Results rs, Timings timings, final WebContext wctx, PaymentSystemMessageProcessor paymentSystemMessageProcessor) throws Exception {
        log.info("Forward RReq To MI"); // Extra log
        if (rReq.getAuthenticationMethod() != null) {
            rReq.setAuthenticationMethod(null);
        }
        // must reserialize to get clean shot

        byte[] miresp = null;
        JsonAndDucplicates rResjsd;
        TDSMessage rRes;

        URL tdsServerURL = new URL(record.getTdsServerURL());
        String tdsServerHost = tdsServerURL.getHost();
        boolean respContentException = false;
        String mversion = getMessageVersion();
        Map<String, String> resph = new java.util.HashMap<String, String>();
        Long finalId = null;
        try {
            if (origRReq != null && rreqData != null && Arrays.equals(origRReq, rReqRawNew)) {
                rreqData.setDestIP(rreqData.getDestIP() + "; 3DS:" + tdsServerHost);
                this.safeUpdate(rreqData, -2, wctx);
            } else { // store new copy
                createNewTDSMessageData(rReq, record, rReqRawNew, new java.util.Date(),
                                        "DS:" + getDSID(), "3DS:" + tdsServerHost, wctx);
            }

            String defaultMIAuthCertId = wctx.getStringSetting(DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey());
            Long defaultMIAuthCertIdL = null;
            if (Misc.isLong(defaultMIAuthCertId)) {
                defaultMIAuthCertIdL = Misc.parseLongBoxed(defaultMIAuthCertId);
            }

            finalId = tdsServerProfile.getOutClientCert() != null ? tdsServerProfile.getOutClientCert() : defaultMIAuthCertIdL;
            final String ckey = tdsServerProfile.getId() + " kid: " + finalId + " url: " + record.getTdsServerURL();

            HttpsJSSEClient client = directory.getHttpsJSSEClient(ckey);
            boolean newcl = false;
            if (client == null) {
                client = new HttpsJSSEClient();
                newcl = true;
                String sslContext = tdsServerProfile.getSSLProto();
                if (Misc.isNullOrEmpty(sslContext)) {
                    sslContext = wctx.getStringSetting(DsSetting.TDSSERVER_SSL_CLIENT_PROTO.getKey());
                }
                if (Misc.isNullOrEmpty(sslContext)) {
                    sslContext = SSLSocketHelper.SSLProto.TLSv12;
                }
                String[] sslCiphers = Misc.split(wctx.getStringSetting(DsSetting.TDSSERVER_SSL_CLIENT_CIPHERS.getKey()), ",");
                if (Misc.isNotNullOrEmpty(sslCiphers)) {
                    client.setCiphers(sslCiphers);
                }

                // for testing only!!!
                String sslTrustAny = wctx.getStringSetting(DsSetting.TDSSERVER_SSL_CLIENT_TRUSTANY.getKey());
                String sslVerifyHostName = wctx.getStringSetting(DsSetting.TDSSERVER_SSL_CLIENT_VERIFY_HOSTNAME.getKey());

                PrivateKey pk = null;
                X509Certificate[] chain = null;
                KeyStore trustStore = null;

                if (record.getTdsServerURL().toLowerCase().startsWith("https://")) {
                    log.info("Record TDSServerURL start with https://"); // Extra log
                    trustStore = keyService.getAllTrustedRootsAsKeyStore();

                    if (tdsServerProfile.getOutClientCert() != null || defaultMIAuthCertIdL != null) {

                        KeyData clientKeyAndCert = keyService.getPrivateKeyAndCertChainById(finalId);
                        if (clientKeyAndCert != null) {
                            byte[] privateKeyBytes  = cryptoService.unwrapPrivateKey(clientKeyAndCert);
                            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(clientKeyAndCert.getHsmDeviceId());
                            pk = hsmDevice.toRSAprivateKey(privateKeyBytes);

                            String certChainPem = KeyService.parsePEMChain(clientKeyAndCert.getKeyData());
                            chain = KeyService.parseX509Certificates(certChainPem.getBytes(StandardCharsets.ISO_8859_1));
                        } else {
                            log.warn("Auth to 3DS Client cert/key not found by id " + finalId);
                        }
                    }
                }

                int connectTimeout = wctx.getIntSetting(DsSetting.CONNECT_TIMEOUT.getKey());
                if (connectTimeout < cDefaultConnectTimeout / 2) {
                    connectTimeout = cDefaultConnectTimeout;
                }

                int tdssTimeout = wctx.getIntSetting(DsSetting.TDSSERVER_TIMEOUT.getKey());
                if (tdssTimeout < cDefaultResponseTimeout / 2) {
                    tdssTimeout = cDefaultResponseTimeout;
                }

                String kf = wctx.getStringSetting(DsSetting.SSL_CLIENT_KEYFACTORY.getKey());
                timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);
                client.init(null, Misc.isNullOrEmpty(kf) ? KeyManagerFactory.getDefaultAlgorithm() : kf, sslContext, "true".equals(sslTrustAny) ? "true" : "false", pk, chain, trustStore, connectTimeout, tdssTimeout, !"false".equals(sslVerifyHostName));
                configureHttpClient(client, wctx);
            }
            timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);

            log.info("Sending " + rReq.getMessageType() + " " + rReq.getTdsServerTransID() + "/" + rReq.getDsTransID() +
                     " to 3DS " + ckey + ", client key " + (client.isClientKeySet() ? "yes" : "no"));

            for (int a = 0; a < 2; a++) {
                try {
                    miresp = client.post(record.getTdsServerURL(), MessageService.CT_JSON_UTF8, rReqRawNew, null, resph);
                    break;
                } catch (Exception cfe) {
                    //immediate connect/ssl failed retry Seq 5.60 [Req 243]
                    if (isConnectionFailedException(cfe) && a < 1) {
                        log.warn("First connection attempt to 3DS " + record.getTdsServerURL() +
                                 " failed, making next auto attempt [Req 243]");
                        continue;
                    } else {
                        throw cfe;
                    }
                }
            }

            timings.resStart = (int) (System.currentTimeMillis() - timings.reqStart);
            if (newcl) {
                directory.putHttpsJSSEClient(ckey, client);
            }

            if (!needResult && rReq.getMessageType().equals("Erro") && (miresp == null || miresp.length < 1)) {
                log.info("No response from 3DS to Erro this may be normal");
                return rs;
            }

            String rspCode = resph.get("HTTP_RESPONSE");
            //String rspMsg=resph.get("HTTP_RESPONSE_MSG");
            try {
                if (miresp == null || miresp.length < 1) {
                    throw new RuntimeException(
                            "No response content from 3DS " + (rspCode != null ? ", HTTP Code " + rspCode : ""));
                } else {
                    rResjsd = MessageService.parseJsonAndDups(miresp);
                    rRes = mse.fromJSON(rResjsd.json);
                    rRes.setJsonObject(rResjsd.json);
                    rRes.setDuplicateCounts(rResjsd.dups);
                    rRes.setIncomingJsonMessage(jsonMessageService.parse(rResjsd.json.toString()));
                }
            } catch (Exception e) {
                log.info("No response content from 3DS " + e.getMessage()); // Extra log
                respContentException = true;
                throw e;
            }

            log.info("Got resp from 3DS " + tdsServerHost + ": " + rRes.getMessageType() + " " + rRes.getTdsServerTransID() +
                     "/" + rRes.getDsTransID() + "/" + rRes.getAcsTransID());
            TDSMessageData rresData = createNewTDSMessageData(rRes, record, miresp, new java.util.Date(),
                                                              "3DS:" + tdsServerHost, "DS:" + getDSID(), wctx);

            // no further procssing in case formwarding an self created errror
            rs.tdsMessageBase = rRes;
            if (!needResult) {
                return rs;
            }

            // validate rres
            JsonObjectBuilder rResOutBuilder = null;
            if (!mse.isErrorMessage(rRes)) {
                ValidationContext validationContext = new ValidationContext();
                validationContext.put(ValidationContext.STRICT_VALIDATION, wctx.getStringSetting(DsSetting.STRICT_VALIDATION.getKey()));
                validationContext.put(ValidationContext.EXTRA_RESULTS_STATUSES, wctx.getStringSetting(DsSetting.EXTRA_RESULT_STATUSES.getKey()));

                List<Error> errors = this.validateRResEmv(rRes, record, validationContext);
                if (errors.size() < 1) {
                    validateRReqRResMatch(rReq, rRes, errors);
                }

                if (errors.size() == 0) {
                    errors.addAll(paymentSystemMessageProcessor.validateRResIncoming(rRes.getIncomingJsonMessage(), record));
                }

                if (errors.size() == 0) {
                    errors.addAll(postValidateRResDSConditional(rRes, record));
                }

                if (errors.size() == 0) {
                    rResOutBuilder = createFilteredJson(rRes, rRes.getJsonObject(), null, record, mse.isTreatEmptyValueAsMissing() ? null : errors);
                }

                if (errors.size() > 0) {
                    Error firstError = errors.get(0);
                    rRes = createErrorMessage(mversion, firstError.getCode(),
                            String.format("%s %s (in RRes from 3DS)", firstError.getFieldName(), firstError.getDescription()), this.createErrorDetails(errors), rRes);
                    log.warn("RRes errors " + rRes.getErrorDetail());
                    rs.jsonRaw = mse.toJSONBos(rRes);

                    String onErr = wctx.getStringSetting(DsSetting.RRES_IN_ERROR_SEND_ERRO_3DS.getKey());
                    String to3DS = "";
                    if (!"false".equals(onErr) && client != null) {
                        final TDSRecord initial = persistenceService.getTDSRecordByDSTransId(rRes.getDsTransID());
                        if (initial != null) {
                            // invalid threeDSServerTransID should not be forwarded to 3ds
                            rRes.setTdsServerTransID(null);
                            if (Misc.isNotNullOrEmpty(rRes.getAcsTransID()) && !rRes.getAcsTransID().equals(initial.getACSTransID())) {
                                rRes.setAcsTransID(initial.getACSTransID());
                            }
                        } else {
                            if (!rReq.getDsTransID().equals(rRes.getDsTransID())) {
                                log.info("DsTransIDs do not match");
                                rRes.setDsTransID(rReq.getDsTransID());
                            }
                        }

                        rs.jsonRaw = mse.toJSONBos(rRes);
                        to3DS = "3DS:" + tdsServerHost + ";";
                        sendForkedErrorMessage(tdsServerURL, "3DS", client, rs.jsonRaw, record, wctx);
                    } // copy 3DS error to 3DS

                    createNewTDSMessageData(rRes, record, rs.jsonRaw, new java.util.Date(),
                                            "DS:" + getDSID(),
                                            to3DS + "ACS:" + wctx.getRequest().getRemoteAddr(), wctx);
                    record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
                    record.setLocalDateEnd(new java.util.Date());
                    safeUpdate(record, -1, wctx);

                    timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
                    rs.tdsMessageBase = rRes;
                    return rs;
                }

                if (TDSModel.XtransStatus.C.isEqual(rs.tdsMessageBase.getTransStatus())) {
                    record.setLocalStatus(DSModel.TSDRecord.Status.CHANLLENGE_INPROCESS);
                } else {
                    record.setLocalStatus(DSModel.TSDRecord.Status.COMPLETED);
                }

                rs.jsonRaw = mse.toJSONBos(rResOutBuilder.build());
                rs.tdsMessageBase = rRes;
                if (isArraysEqualsLength(miresp, rs.jsonRaw.getBuf(), rs.jsonRaw.getCount())) {
                    rresData.setDestIP("DS:" + getDSID() + "; ACS:" + wctx.getRequest().getRemoteAddr());
                    this.safeUpdate(rresData, -2, wctx);
                } else { // store different copy
                    this.createNewTDSMessageData(rs.tdsMessageBase, record, rs.jsonRaw, new java.util.Date(),
                                                 "DS:" + getDSID(), "ACS:" + wctx.getRequest().getRemoteAddr(), wctx);
                }
            } else {
                log.warn("Unexpected response " + rRes.getMessageType() + " to RReq");
                record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
                record.setErrorCode(Misc.cut(rRes.getErrorCode(), 3));
                record.setErrorDetail(Misc.cut(rRes.getErrorDetail(), 2048));

                ErrorCode ec = ErrorCode.fromValue(rRes.getErrorCode());

                if (ec == null) {
                    log.warn("Tx " + record.getId() + " Unknown error code from 3DSServer '" + rRes.getErrorCode() +
                             "'");
                    record.setErrorDetail(Misc.merge(record.getErrorDetail(),
                                                     "DS: Unknown error code: '" + rRes.getErrorCode() + "'"));
                    ec = ErrorCode.cInvalidTxData305;
                }
                rRes = createErrorMessage(mversion, ec, rRes.getErrorDescription() +
                                                        " (Erro response from 3DS Server)", rRes.getErrorDetail(), rRes);
                rRes.setErrorMessageType("RReq");
                rs.jsonRaw = mse.toJSONBos(rRes);
                createNewTDSMessageData(rRes, record, rs.jsonRaw, new java.util.Date(),
                                        "DS:" + getDSID(), "ACS:" + wctx.getRequest().getRemoteAddr(), wctx);
            }

            record.setLocalDateEnd(new java.util.Date());
            safeUpdate(record, -1, wctx);
            timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
            return rs;
        } catch (Exception e) {
            String errId = Context.getUnique();
            String em = "Error id " + errId + " in sending RReq To 3DS " + tdsServerProfile.getId() + "kid " + finalId;
            String rspCode = resph.get("HTTP_RESPONSE");
            String rspMsg = resph.get("HTTP_RESPONSE_MSG");
            String detailErr = resph.get("errorData");
            String logdetail = ("detail: " + record.getTdsServerURL() + (rspCode != null ? ", HTTP Code " + rspCode : "") +
                                (rspMsg != null ? ", HTTP Msg " + rspMsg : "") +
                                (detailErr != null ? ", Err content: '" + detailErr + "'" : "") + (miresp != null ?
                                                                                                   ", Content: '" +
                                                                                                   new String(miresp, StandardCharsets.UTF_8) +
                                                                                                   "'" : "") + " ex: " +
                                e);

            log.error(em + " " + logdetail, e);
            TDSMessage info = new TDSMessage();
            info.setMessageType("INFO");
            createNewTDSMessageData(info, record, logdetail.getBytes(StandardCharsets.UTF_8), new java.util.Date(),
                                    "3DS:" + record.getTdsServerURL(), "DS:" + getDSID(), wctx);

            record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
            // error forwarding no record state needs updated or errors generated..
            if (!needResult) {
                return rs;
            }

            if (respContentException) {
                rRes = createErrorMessage(mversion, ErrorCode.cInvalidMessageType101, ErrorCode.cInvalidMessageType101.desc, null,
                    "Invalid response from 3DS Server on RRes", rReq);
                record.setErrorDetail(Misc.cut(Misc.merge("Invalid response from 3DS Server on RRes",
                                                          em + " " + e.toString()), 2048));
            } else {
                String detail = "3DS Server connection failed";
                ErrorCode ec = ErrorCode.cSysConnectionFailure405;
                if (this.isReadTimeoutException(e)) {
                    detail = "3DS Server connection read timeout on RReq/RRes";
                    ec = ErrorCode.cTransTimedOut402;
                }
                if (this.isConnectionFailedException(e)) {
                    detail = "3DS Server connection failed on RReq/RRes";
                }

                record.setLocalStatus(DSModel.TSDRecord.Status.MICOMERROR);
                rRes = createErrorMessage(mversion, ec, null, detail, rReq);
                record.setErrorDetail(Misc.cut(Misc.merge(detail, em), 2048));
                diagnoseRReqTransactionException(e, record, wctx.getLocale());
            }

            rs.jsonRaw = mse.toJSONBos(rRes);
            createNewTDSMessageData(rRes, record, rs.jsonRaw, new java.util.Date(),
                                    "DS:" + getDSID(), "ACS:" + wctx.getRequest().getRemoteAddr(), wctx);
            record.setLocalDateEnd(new java.util.Date());
            safeUpdate(record, -1, wctx);
            rs.tdsMessageBase = rRes;
            return rs;
        }
    }

    private void diagnoseRReqTransactionException(Exception transactionException, TDSRecord record, Locale locale) {
        LocalizationService localizationService = ServiceLocator.getInstance().getLocalizationService();
        if(transactionException instanceof KeyDataIdNotFoundException){
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_DESCRIPTION, record.getId(), localizationService.getText(
                    "text.err.desc.invalidClientCertId", new Object[]{DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey()}, locale));
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_RESOLUTION, record.getId(), localizationService.getText(
                    "text.err.resolution.invalidClientCertId", new Object[]{DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey(), KeyService.KeyAlias.clientAuthCert}, locale));
        } else if (isReadTimeoutException(transactionException)) {
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_DESCRIPTION, record.getId(), localizationService.getText(
                    "text.err.desc.RReqReadTimeout", new Object[]{record.getTdsServerURL()}, locale));
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_RESOLUTION, record.getId(), localizationService.getText(
                    "text.err.resolution.RReqReadTimeout", new Object[]{record.getTdsServerURL()}, locale));
        } else if (isConnectionFailedException(transactionException)) {
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_DESCRIPTION, record.getId(), localizationService.getText(
                    "text.err.desc.RReqConnectTimeout", new Object[]{record.getTdsServerURL()}, locale));
            tdsRecordAttributeService.saveAttribute(TransactionAttribute.ERROR_RESOLUTION, record.getId(), localizationService.getText(
                    "text.err.resolution.RReqConnectTimeout", new Object[]{record.getTdsServerURL()}, locale));
        }
    }

    public boolean decryptSDKData(TDSMessage aReq, WebContext wctx) {
        log.info("Decrypt SDK data"); // Extra log
        if (Misc.isNotNullOrEmpty(aReq.getSdkEncData())) {
            try {
                boolean isNimbusDecryption = "true".equals(wctx.getStringSetting(DsSetting.DECRYPT_DEVICE_DATA_ALL_NIMBUS.getKey()));
                KeyData sdkRSAPrivateKey = keyService.getSdkRsaPrivateKey();
                KeyData sdkECPrivateKey = keyService.getSdkEcPrivateKey();

                byte[] plainData = cryptoService.decryptSdkEncData(sdkRSAPrivateKey, sdkECPrivateKey,
                        aReq.getSdkEncData(), isNimbusDecryption);

                // #173 Note: base64url ins 2.1 sepc
                aReq.setDeviceInfo(Base64.encodeURL(plainData, plainData.length * 2));
                aReq.setSdkEncData(null);
            } catch (Exception e) {
                log.error("Error decrypting SDK data for SDKTXID=" + aReq.getSdkTransID() + " SDK=" + aReq.getSdkAppID()
                        + ", please fix", e);
                return false;
            }

        }
        return true;
    }

    protected Results processAReq(TDSMessage reqMsg, String[] msgRaw2, Date now, Timings timings, WebContext webContext) throws Exception {
        Results results = new Results();
        TDSMessage aRes;
        CertificateData cd = (CertificateData) webContext.getRequestAttribute("localCertificateData");
        String messageVersion = mse.getSupportedVersion();
        directory.getRequestsTiming().addAReqTiming(timings);
        TDSRecord record = results.tdsRecord = createNewTDSRecord(reqMsg, now, webContext);
        MDC.getMDCAdapter().put("tx", "tx" + record.getId());
        record.setDSTransID(createUUID(getUUIDPrefix(webContext), record.getId()));

        ValidationContext validationContext = new ValidationContext();
        validationContext.put(ValidationContext.STRICT_VALIDATION, webContext.getStringSetting(DsSetting.STRICT_VALIDATION.getKey()));
        validationContext.put(ValidationContext.AREQ_BR_BROWSER_IP_REQUIRED, webContext.getStringSetting(DsSetting.AREQ_BR_BROWSER_IP_REQUIRED.getKey()));
        validationContext.put(ValidationContext.EXTRA_REQUESTOR_CHALLENGE_INDS, webContext.getStringSetting(DsSetting.EXTRA_REQUESTOR_CHALLENGE_INDS.getKey()));
        validationContext.put(ValidationContext.CRITICAL_EXTENSIONS_PASS, webContext.getStringSetting(
                reqMsg.getMessageType() + ".criticalExtensionsPass"));
        validationContext.put(ARES_ECI_REQUIRED, webContext.getStringSetting(DsSetting.ARES_ECI_REQUIRED.getKey()));
        validationContext.put(ARES_TRANS_STATUS_A_ECI, webContext.getStringSetting(DsSetting.ARES_TRANS_STATUS_A_ECI.getKey()));
        validationContext.put(ARES_TRANS_STATUS_U_ECI, webContext.getStringSetting(DsSetting.ARES_TRANS_STATUS_U_ECI.getKey()));
        validationContext.put(ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE, webContext.getStringSetting(DsSetting.ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE.getKey()));

        List<Error> errors = validateAReqEmv(reqMsg, validationContext);
        errors.addAll(validateISOCodes(reqMsg, webContext));

        createNewTDSMessageData(reqMsg, record, msgRaw2, now,
                                "3DS:" + webContext.getRequest().getRemoteAddr(), "DS:" + getDSID(), webContext);

        if (!errors.isEmpty()) {
            Error e1 = getFirstError(errors);
            aRes = createErrorMessage(messageVersion, e1.getCode(), null, this.createErrorDetails(errors), reqMsg);
            results.jsonRaw = mse.toJSONBos(aRes);
            createNewTDSMessageData(aRes, record, results.jsonRaw, new Date(),
                                    "DS:" + getDSID(), "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);
            record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
            record.setLocalDateEnd(new Date());
            record.setErrorCode(Misc.cut(aRes.getErrorCode(), 3));
            record.setErrorDetail(Misc.cut(aRes.getErrorDetail(), 2048));
            safeUpdate(record, -1, webContext);
            results.tdsMessageBase = aRes;
            return results;
        }

        // ELO-specific configuration workaround GH-1169
        String jointPaymentSystemIds = webContext.getStringSetting(DsSetting.JOINT_PAYMENT_SYSTEMS.getKey());
        if (StringUtils.isNotEmpty(jointPaymentSystemIds)) {
            Integer paymentSystemIdFrom3DSS = getPaymentSystemIdFromJoint3DSS(jointPaymentSystemIds,
                    webContext.getPaymentSystemId(), reqMsg.getThreeDSServerOperatorID());
            webContext.setPaymentSystemId(paymentSystemIdFrom3DSS);
        }

        PaymentSystemMessageProcessor paymentSystemMessageProcessor = PaymentSystemMsgProcessorRegistry.DEFAULT;
        PaymentSystem paymentSystem = persistenceService.getPersistableById(webContext.getPaymentSystemId(), PaymentSystem.class);
        record.setPaymentSystemId(paymentSystem.getId());
        if (paymentSystem.getType() != null) {
            paymentSystemMessageProcessor = PaymentSystemMsgProcessorRegistry.get(paymentSystem.getType());
        }

        errors.addAll(paymentSystemMessageProcessor.validateAReqIncoming(reqMsg.getIncomingJsonMessage()));
        if (!errors.isEmpty()) {
            Error e1 = getFirstError(errors);
            aRes = createErrorMessage(messageVersion, e1.getCode(), null, this.createErrorDetails(errors), reqMsg);
            results.jsonRaw = mse.toJSONBos(aRes);
            createNewTDSMessageData(aRes, record, results.jsonRaw, new Date(),
                                    "DS:" + getDSID(), "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);
            record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
            record.setLocalDateEnd(new Date());
            record.setErrorCode(Misc.cut(aRes.getErrorCode(), 3));
            record.setErrorDetail(Misc.cut(aRes.getErrorDetail(), 2048));
            safeUpdate(record, -1, webContext);
            results.tdsMessageBase = aRes;
            return results;
        }
        errors.addAll(postValidateAReqDSConditional(reqMsg));
        if (!errors.isEmpty()) {
            Error e1 = getFirstError(errors);
            aRes = createErrorMessage(messageVersion, e1.getCode(), null, this.createErrorDetails(errors), reqMsg);
            results.jsonRaw = mse.toJSONBos(aRes);
            createNewTDSMessageData(aRes, record, results.jsonRaw, new Date(),
                                    "DS:" + getDSID(), "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);
            record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
            record.setLocalDateEnd(new Date());
            record.setErrorCode(Misc.cut(aRes.getErrorCode(), 3));
            record.setErrorDetail(Misc.cut(aRes.getErrorDetail(), 2048));
            safeUpdate(record, -1, webContext);
            results.tdsMessageBase = aRes;
            return results;
        }

        ErrorCode errorCode = ErrorCode.cAccessDenied303;
        String reason = null;

        // SDK list validation
        if (reason == null && !dsSettings.isAcceptedSDKListCheckSkipped() && DeviceChannel.cChannelApp01.value().equals(reqMsg.getDeviceChannel())) {
            String acceptedSDKList = paymentSystemSettingsCacheService.getPaymentSystemSetting(webContext.getPaymentSystemId(),
                                                                                               PsSetting.ACCEPTED_SDK_LIST);
            if (acceptedSDKList == null || !acceptedSDKList.contains(reqMsg.getSdkReferenceNumber())) {
                reason = "SDK '" + reqMsg.getSdkReferenceNumber() + "' not approved";
                log.warn("SDK ref '" + reqMsg.getSdkReferenceNumber() +
                         "' not in approved list (setting acceptedSDKList)");
            }
        }

        // mcc validation
        if (reason == null && reqMsg.getMcc() != null) {
            String acceptedMCCList = webContext.getStringSetting(DsSetting.ACCEPTED_MCC_LIST.getKey());
            if (!(acceptedMCCList == null || acceptedMCCList.length() < 1 ||
                  acceptedMCCList.contains(reqMsg.getMcc()))) {
                reason = "MCC '" + reqMsg.getMcc() + "' not accepted for scheme";
                errorCode = ErrorCode.cInvalidMCC306;
                log.warn("MCC '" + reqMsg.getMcc() + "' not in approved list (setting acceptedMCCList)");
            }
        }

        // other business validation
        if (reason == null) {
            Optional<Error> giftCardCurrError = MerchantRiskIndicatorValidator.getInstance().isGiftCardCurrValid(reqMsg);
            giftCardCurrError.ifPresent(errors::add);
            if (errors.size() > 0) {
                errorCode = ErrorCode.cInvalidISOCode304;
                reason = createErrorDetails(errors).toString();
            }
        }

        boolean npa = "02".equals(reqMsg.getMessageCategory());

        boolean requestorIdWithMerchant = "true".equals(webContext.getStringSetting(DsSetting.REQUESTOR_ID_WITH_MERCHANT.getKey()));
        boolean singleMerchantPer3DSS = "true".equals(webContext.getStringSetting(DsSetting.SINGLE_MERCHANT_PER_3DSS.getKey()));

        if (singleMerchantPer3DSS) {
            requestorIdWithMerchant = true;
        }

        boolean enableTrustAllMerchants = webContext.isTrueSetting(DsSetting.ENABLE_TRUST_ALL_MERCHANTS.getKey());

        TDSServerProfile tdsServerProfile = null;
        Merchant merchant = null;
        Acquirer acquirer = null;

        if (enableTrustAllMerchants) {
            if (reason == null) {
                if (Misc.isNullOrEmpty(reqMsg.getThreeDSServerOperatorID())) {
                    reason = "Unknown 3DS Server, empty threeDSServerOperatorID";
                    log.warn("Unknown 3DS Server/3DSS Operator Id: threeDSServerOperatorID is empty");
                } else {
                    tdsServerProfile = acquirerService.getActiveTdsServerProfileByOperatorId(webContext.getPaymentSystemId(), reqMsg.getThreeDSServerOperatorID());
                    if (tdsServerProfile == null) {
                        reason = "Unknown 3DS Server, threeDSServerOperatorID not found";
                        log.warn("Unknown 3DS Server/3DSS Operator Id: threeDSServerOperatorID={} not found", reqMsg.getThreeDSServerOperatorID());
                    } else {
                        record.setMIProfileId(tdsServerProfile.getId());
                    }
                }
            }
        } else {
            if (reason == null) {
                if (!(reqMsg.getAcquirerBIN()==null && npa)) {
                    acquirer = directory.getAcquirerByBIN(webContext.getPaymentSystemId(), reqMsg.getAcquirerBIN());
                }
                if (acquirer == null) {
                    if(!npa) {
                        reason = "Unknown Acquirer";
                        log.warn("Acquirer by BIN '" + reqMsg.getAcquirerBIN() + "' not found");
                    }
                } else if (!DSModel.Acquirer.Status.ACTIVE.equals(acquirer.getStatus())) {
                    reason = "Acquirer has been switched off";
                    log.warn("Acquirer by BIN '" + reqMsg.getAcquirerBIN() + "' status=" + acquirer.getStatus());
                    record.setLocalAcquirerId(acquirer.getId());
                } else {
                    record.setLocalAcquirerId(acquirer.getId());
                }
            }

            if (reason == null) {
                if (npa && acquirer == null || npa &&
                                          Misc.isNullOrEmpty(reqMsg.getAcquirerMerchantID())) {
                    //I don't fully understand the merchant resolution logic but this is required by UL cases and it does make some sense
                    // no merchant required  unless requestorIdWithMerchant
                    if (requestorIdWithMerchant) {
                        merchant = directory.getRequestorMerchant(webContext.getPaymentSystemId(), reqMsg.getTdsRequestorID());

                        if (merchant == null) {
                            reason = "Unknown Requestor/Merchant";
                            log.warn("Requestor/Merchant requestorID: '" + reqMsg.getTdsRequestorID() + "' not found");
                        } else if (!DSModel.Merchant.Status.ACTIVE.equals(merchant.getStatus())) {
                            reason = "Requestor/Merchant has been disabled";
                            log.warn("Requestor/Merchant ': " + reqMsg.getTdsRequestorID() + "' not active, status=" +
                                     merchant.getStatus());
                        }
                    }
                } else if (singleMerchantPer3DSS) { //
                    merchant = directory.getRequestorMerchant(webContext.getPaymentSystemId(), reqMsg.getTdsRequestorID());

                    if (merchant == null) {
                        reason = "Unknown Requestor/Merchant";
                        log.warn("Requestor/Merchant requestorID: '" + reqMsg.getTdsRequestorID() + "' not found");
                    } else if (!DSModel.Merchant.Status.ACTIVE.equals(merchant.getStatus())) {
                        reason = "Requestor/Merchant has been disabled";
                        log.warn("Requestor/Merchant ': " + reqMsg.getTdsRequestorID() + "' not active, status=" + merchant.getStatus());
                    }
                } else {
                    merchant = directory.getAquirerMerchant(webContext.getPaymentSystemId(), acquirer.getId(), reqMsg.getAcquirerMerchantID());
                    if (merchant == null) {
                        reason = "Unknown Merchant";
                        log.warn("Merchant acq:acqmid '" + acquirer.getId() + ":" + reqMsg.getAcquirerMerchantID() +
                                 "' not found");
                    } else if (!DSModel.Merchant.Status.ACTIVE.equals(merchant.getStatus())) {
                        reason = "Merchant has been disabled";
                        log.warn("Merchant acq:acqmid '" + acquirer.getId() + ":" + reqMsg.getAcquirerMerchantID() +
                                 "' status=" + merchant.getStatus());
                    }
                }

                if (reason == null && !singleMerchantPer3DSS) {
                    boolean mcm = "true".equals(webContext.getStringSetting(DsSetting.VALIDATE_MERCHANT_COUNTRY_CODE_MATCH.getKey()));
                    if (merchant != null && mcm && Misc.isInt(reqMsg.getMerchantCountryCode()) && Misc.isNotNullOrEmpty(merchant.getCountry())) {
                        short mmc = (short) Misc.parseInt(reqMsg.getMerchantCountryCode());
                        ISO3166 icx = ISO3166.findFromNumeric(mmc);
                        if (icx != null && !merchant.getCountry().equals(icx.getA2code())) {
                            errorCode = ErrorCode.cInvalidTxData305;
                            reason = "merchantCountryCode mismatch";
                            log.warn("Merchant acq:acqmid '" + acquirer.getId() + ":" + reqMsg.getAcquirerMerchantID() +
                                     "' contry mismatch: " + "tdsMessageBase=" + mmc + " in profile " +
                                     icx.getNumeric() + "(" + merchant.getCountry() + ")");
                        } else if (icx == null) {
                            log.warn("Merchant acq:acqmid '" + acquirer.getId() + ":" + reqMsg.getAcquirerMerchantID() +
                                     " AReq merchantCountryCode " + mmc +
                                     " not defined in internal Iso3166 country table, can not validate match");
                        }
                    }
                }

                if (reason == null && merchant != null && requestorIdWithMerchant && Misc.isNotNullOrEmpty(merchant.getRequestorID()) &&
                    !merchant.getRequestorID().equals(reqMsg.getTdsRequestorID())) {
                    reason = "3DS Server threeDSRequestorID mismatch";
                    log.warn("3DS Server threeDSRequestorID id mismatches: supplied=" + reqMsg.getTdsRequestorID() +
                             " in file=" + merchant.getRequestorID() + " with Merchant " + merchant.getId());
                }
            }

            if (reason == null) {
                if (merchant != null && merchant.getTdsServerProfileId() != null) {
                    tdsServerProfile = directory.getTdsServerProfile(webContext.getPaymentSystemId(), merchant.getTdsServerProfileId());
                } else if (!requestorIdWithMerchant && reqMsg.getTdsRequestorID() != null) {
                    tdsServerProfile = directory.getTdsServerProfileByRequestorID(webContext.getPaymentSystemId(), reqMsg.getTdsRequestorID());
                }

                if (tdsServerProfile != null) {
                    record.setMIProfileId(tdsServerProfile.getId());
                }

                if (tdsServerProfile == null) {
                    reason = "Unknown 3DS Server/Requestor";
                    Long proId = null;
                    if (merchant != null && merchant.getTdsServerProfileId() != null) {
                        proId = merchant.getTdsServerProfileId();
                    } else if (reqMsg.getTdsRequestorID() != null) {
                        log.warn("3DS(MI) by requestor id " + reqMsg.getTdsRequestorID() + " not found");
                    }

                    if (proId != null) {
                        log.warn("3DS(MI) with by " + proId + " not found");
                    }

                } else if (!requestorIdWithMerchant && Misc.isNotNullOrEmpty(tdsServerProfile.getRequestorID()) &&
                           !tdsServerProfile.getRequestorID().equals(reqMsg.getTdsRequestorID())) {
                    reason = "3DS Server threeDSRequestorID mismatch";
                    log.warn("3DS Server threeDSRequestorID id mismatches: supplied=" + reqMsg.getTdsRequestorID() +
                             " in file=" + tdsServerProfile.getRequestorID() + " with 3DServer(MI) " + tdsServerProfile.getId());
                } else if (Misc.isNotNullOrEmpty(tdsServerProfile.getOperatorID()) &&
                           Misc.isNotNullOrEmpty(reqMsg.getThreeDSServerOperatorID()) &&
                           !tdsServerProfile.getOperatorID().equals(reqMsg.getThreeDSServerOperatorID())) {
                    reason = "3DS Server threeDSServerOperatorID mismatch";
                    log.warn("3DS Server threeDSServerOperatorID id mismatches: supplied=" +
                             reqMsg.getThreeDSServerOperatorID() + " in file=" + tdsServerProfile.getOperatorID() +
                             " with 3DServer(MI) " + tdsServerProfile.getId() + "");
                } else if (cd != null && (tdsServerProfile.getInClientCert() == null || !tdsServerProfile.getInClientCert().equals(cd.getId()))) {
                    reason = "3DS Server certificate configuration issue";
                    log.warn("3DS Server certificate id mismatches: clientcertId=" + tdsServerProfile.getInClientCert() +
                             " auth cert id=" + cd.getId());
                }
            }

        } // non superRequestor (normal)

        if (reason == null) {
            if (tdsServerProfile != null && !DSModel.TDSServerProfile.Status.ACTIVE.equals(tdsServerProfile.getStatus())) {
                reason = "3DS Server has been disabled";
                log.warn("3DS Server 3DS(MI) {} status {} not active", tdsServerProfile.getId(), tdsServerProfile.getStatus());
            }
        }

        if (reason == null) {
            String acceptedTDSServerList = ObjectUtils.safeNull(webContext.getStringSetting(DsSetting.ACCEPTED_TDSSERVER_LIST.getKey()));
            if (tdsServerProfile != null && Misc.isNotNullOrEmpty(tdsServerProfile.getTdsReferenceNumberList())) {
                List<String> tdsReferenceNumberList = DsStringUtils.splitToList(tdsServerProfile.getTdsReferenceNumberList(), ",");
                if (!tdsReferenceNumberList.contains(reqMsg.getTdsServerRefNumber())) {
                    reason = "3DS Server '" + reqMsg.getTdsServerRefNumber() + "' not approved";
                    log.warn("3DS Server ref '{}' not in approved list (3DS Profile = '{}')", reqMsg.getTdsServerRefNumber(), tdsServerProfile.getName());
                }
            } else if (tdsServerProfile != null && !acceptedTDSServerList.contains(reqMsg.getTdsServerRefNumber())) {
                reason = "3DS Server '" + reqMsg.getTdsServerRefNumber() + "' not approved";
                log.warn("3DS Server ref '{}' not in approved list (setting acceptedTDSServerList)", reqMsg.getTdsServerRefNumber());
            }
        }

        EloPapiDetokenizationService.DetokenizedData detokenizedData = null;
        if (PaymentSystemType.elo.isEqual(paymentSystem.getType())) {
            if (paymentSystemSettingsCacheService.isTrueSetting(paymentSystem.getId(), PsSetting.ELO_PAPI_ENABLED)) {
                EloPapiDetokenizationService eloPapiService = applicationContext.getBean(EloPapiDetokenizationService.class);
                detokenizedData = eloPapiService.sendAreq(record.getId(), getDSID(), paymentSystem.getId(),
                        reqMsg.getIncomingJsonMessage());
            }
        }

        String acctNumber = reqMsg.getAcctNumber();
        if (detokenizedData != null && detokenizedData.getAcctNumber() != null) {
            acctNumber = detokenizedData.getAcctNumber();
            log.info("Using detokenized acctNumber.");
        }

        TDSModel.XtransStatus xts = null;
        String statusReason = XtransStatusReason.C01AuthFailed.value();
        CardRange cardRange = null;
        Issuer issuer = null;
        if (reason == null) {
            String statusReasonBINNotFound = webContext.getStringSetting(DsSetting.STATUS_REASON_BIN_NOT_FOUND.getKey());
            String statusBINNotFound = webContext.getStringSetting(DsSetting.STATUS_BIN_NOT_FOUND.getKey());

            String issuerBIN = Misc.trunc(acctNumber, issuerService.getMaxBINLength());
            if (issuerService.isBINRangeMode()) {
                cardRange = issuerService.getBestMatchingIssuerCardBinByRange(acctNumber, webContext.getPaymentSystemId());
            } else {
                cardRange = issuerService.getBestMatchingIssuerCardBin(issuerBIN, webContext.getPaymentSystemId());
            }

            if (cardRange != null) {
                issuer = directory.getIssuerById(webContext.getPaymentSystemId(), cardRange.getIssuerId());
            }

            if (issuer != null) {
                record.setIssuerBIN(issuer.getBIN());
            }

            if (cardRange != null && DSModel.CardRange.Status.PARTICIPATING.equals(cardRange.getStatus())) {
                if (issuer != null && DSModel.Issuer.Status.ACTIVE.equals(issuer.getStatus())) {
                    record.setLocalIssuerId(issuer.getId());
                }

                // check versions supported for range
                boolean smaller = false;
                boolean higher = false;
                int msgValue = Misc.parseInt(Misc.replace(reqMsg.getMessageVersion(), ".", ""));

                if (Misc.isNotNullOrEmpty(cardRange.getStartProtocolVersion()) && msgValue > 0) {
                    int minValue = Misc.parseInt(Misc.replace(cardRange.getStartProtocolVersion(), ".", ""));
                    if (minValue > 0 && msgValue < minValue) {
                        smaller = true;
                    }
                }

                if (Misc.isNotNullOrEmpty(cardRange.getEndProtocolVersion()) && msgValue > 0) {
                    int maxValue = Misc.parseInt(Misc.replace(cardRange.getEndProtocolVersion(), ".", ""));
                    if (maxValue > 0 && msgValue > maxValue) {
                        higher = true;
                    }
                }
                if (smaller || higher) {
                    errorCode = ErrorCode.cMessageVersionNotSupported102;
                    reason = "messageVersion not supported for this range/BIN";
                    log.warn("Issuer BIN id=" + cardRange.getId() + " '" + Misc.maskEnd(cardRange.getBin(), 7) +
                             "' does not support protcol " + reqMsg.getMessageVersion());
                }
            } else if (cardRange == null) {
                log.warn("Issuer BIN '" + Misc.cut(issuerBIN, 6) + "' not found");
                xts = Misc.isNullOrEmpty(statusBINNotFound) ? TDSModel.XtransStatus.U : TDSModel.XtransStatus.valueOf(statusBINNotFound);
                statusReason = Misc.isNullOrEmpty(statusReasonBINNotFound) ? XtransStatusReason.C08NoCardrecord.value : statusReasonBINNotFound;
                reason = "Unknown BIN";
            } else if (!DSModel.CardRange.Status.PARTICIPATING.equals(cardRange.getStatus())) {
                log.warn("Issuer BIN id=" + cardRange.getId() + " '" + Misc.maskEnd(cardRange.getBin(), 7) +
                         "' not participating, status=" + cardRange.getStatus());
                String statusReasonBINPart = webContext.getStringSetting(DsSetting.STATUS_REASON_BIN_NOT_PARTICIPATING.getKey());
                String statusBINPart = webContext.getStringSetting(DsSetting.STATUS_BIN_NOT_PARTICIPATING.getKey());
                xts = Misc.isNullOrEmpty(statusBINPart) ? TDSModel.XtransStatus.U : TDSModel.XtransStatus.valueOf(statusBINPart);
                statusReason = Misc.isNullOrEmpty(statusReasonBINPart) ? XtransStatusReason.C13CardNotEnrolled.value : statusReasonBINPart;
                reason = "BIN not participating";
            }
        }
        if (cardRange != null) {
            tdsRecordAttributeService.saveCardRangeAttribute(record, cardRange);
        }
        tdsRecordAttributeService.saveTDSRecordAttributesAreq(record, webContext.getRequestServerPort());
        List<ACSProfile> acsList = null;

        if (reason == null) {
            boolean attempts = false;

            if (issuer != null && DSModel.Issuer.Status.ACTIVE.equals(issuer.getStatus())) {
                acsList = issuerService.getIssuerACSList(issuer.getId(), cardRange.getSet(), DSModel.ACSProfile.Status.ACTIVE, true);
                //#21 Attempts acs

                if (issuer.getAttemptsACSProId() != null
                    && TDSModel.XMessageCategory.C01.isEqual(reqMsg.getMessageCategory())) {
                    ACSProfile attemptsACS = persistenceService.getPersistableById(issuer.getAttemptsACSProId(), ACSProfile.class);
                    if (attemptsACS != null && DSModel.ACSProfile.Status.ACTIVE.equals(attemptsACS.getStatus())) {
                        // 133 instance based attempt acs url..
                        String acpHostInst = webContext.getStringSetting(
                                "attemptsACSHost." + webContext.getServerIntanceId());
                        if (Misc.isNotNullOrEmpty(acpHostInst)) {
                            attemptsACS.setURL(Misc.replace(attemptsACS.getURL(), "{attemptsACSHost}", acpHostInst));
                        }

                        acsList.add(attemptsACS);
                        attempts = true;
                    }
                }
            } else {
                if (issuer == null) {
                    errorCode = ErrorCode.cTransientSysFailure403;
                    log.warn("Issuer by BIN " + " '" + Misc.maskEnd(cardRange.getBin(), 7) + "'" + " or id " +
                            cardRange.getIssuerId() + " not found (broken mapping)");
                    reason = "Unknown Issuer (broken mapping)";
                } else if (!DSModel.Issuer.Status.ACTIVE.equals(issuer.getStatus())) {
                    log.warn("Issuer " + issuer.getId() + " is not active");
                    reason = "Issuer not praticipating (off)";
                    String statusReasonIssuerA = webContext.getStringSetting(DsSetting.STATUS_REASON_ISSUER_NOT_ACTIVE.getKey());
                    String statusIssuerA = webContext.getStringSetting(DsSetting.STATUS_ISSUER_NOT_ACTIVE.getKey());
                    xts = Misc.isNullOrEmpty(statusIssuerA) ? TDSModel.XtransStatus.U : TDSModel.XtransStatus.valueOf(statusIssuerA);
                    statusReason = Misc.isNullOrEmpty(statusReasonIssuerA) ? XtransStatusReason.C13CardNotEnrolled.value : statusReasonIssuerA;
                }
            }
            if (reason == null && (acsList == null || acsList.size() < 1)) {
                reason = "Issuer misconfiguration (no active ACS)";
                log.error("No ACS Profiles found for Issuer " + cardRange.getIssuerId());
            } else if (reason == null) {
                // decyopte edf
                boolean ok = decryptSDKData(reqMsg, webContext);
                if (!ok) {
                    reason = "Failed to decrypt SdkEncData";
                    errorCode = ErrorCode.cDateDecryptionFailed302;
                }

                if (reason == null) {
                    // prepare service specific areq extensions if any..in time (like fss)
                    String tdsmCmpInd = reqMsg.getThreeDSCompInd();
                    if (extensionService != null) {
                        long extStart = System.currentTimeMillis();
                        extensionService.createAReqExtensions(record, results, merchant, acquirer, cardRange, issuer, acsList, reqMsg, this, webContext);
                        timings.extSum += (int) (System.currentTimeMillis() - extStart);
                    }
                    JsonObjectBuilder aReqOutJob = createFilteredJson(reqMsg, reqMsg.getJsonObject(), results.areqExtensions, record, errors);

                    // replaces the original acctNumber in AReq with possibly detokenized one before sending to ACS.
                    if (detokenizedData != null) {
                        if (detokenizedData.getAcctNumber() != null) {
                            aReqOutJob.add(FieldName.acctNumber, detokenizedData.getAcctNumber());
                        }
                        if (detokenizedData.getCardExpiryDate() != null) {
                            aReqOutJob.add(FieldName.cardExpiryDate, detokenizedData.getCardExpiryDate());
                        }
                        if (detokenizedData.getMessageExtension() != null) {
                            aReqOutJob.add(FieldName.messageExtension, detokenizedData.getMessageExtension());
                        }
                    }

                    if (!mse.isTreatEmptyValueAsMissing() && errors.size() > 0) {
                        Error e1 = getFirstError(errors);
                        errorCode = e1.getCode();
                        reason = this.createErrorDetails(errors).toString();
                    } else {
                        reqMsg.setDsReferenceNumber(this.getDSReferenceNumber(webContext));
                        aReqOutJob.add("dsReferenceNumber", reqMsg.getDsReferenceNumber());
                        reqMsg.setDsTransID(record.getDSTransID());
                        aReqOutJob.add("dsTransID", reqMsg.getDsTransID());

                        String dsUrl = directory.getDSURL(webContext);

                        if (dsUrl != null &&
                            !TDSModel.DeviceChannel.cChannel3RI03.value().equals(reqMsg.getDeviceChannel()) ||
                            MessageVersion.V2_2_0.value().equals(reqMsg.getMessageVersion())) {
                            reqMsg.setDsURL(dsUrl);
                            aReqOutJob.add("dsURL", dsUrl);
                        }
                        if (reqMsg.getTransType() != null && !npa) {
                            aReqOutJob.add("transType", reqMsg.getTransType());
                        }

                        if (reqMsg.getDeviceInfo() != null) {
                            aReqOutJob.add("deviceInfo", reqMsg.getDeviceInfo());
                        }
                        // in case extensionService changes challenge completion inidicator becuse of 3dsmethor relay for non 3ds m
                        if (tdsmCmpInd != null && !tdsmCmpInd.equals(reqMsg.getThreeDSCompInd())) {
                            aReqOutJob.add("threeDSCompInd", reqMsg.getThreeDSCompInd());
                        }
                        if (reqMsg.getTdsServerURL() != null) {
                            aReqOutJob.add("threeDSServerURL", reqMsg.getTdsServerURL());
                        }
                        if (reqMsg.getPurchaseCurrency() != null) {
                            aReqOutJob.add("purchaseCurrency", reqMsg.getPurchaseCurrency());
                        }
                        if (reqMsg.getPurchaseDate() != null) {
                            aReqOutJob.add("purchaseDate", reqMsg.getPurchaseDate());
                        }
                        if (reqMsg.getPurchaseAmount() != null) {
                            aReqOutJob.add("purchaseAmount", reqMsg.getPurchaseAmount());
                        }
                        if (reqMsg.getPurchaseExponent() != null) {
                            aReqOutJob.add("purchaseExponent", reqMsg.getPurchaseExponent());
                        }
                        if (TDSModel.DeviceChannel.cChannel3RI03.value().equals(reqMsg.getDeviceChannel()) &&
                            reqMsg.getThreeRIInd() != null) {
                            aReqOutJob.add("threeRIInd", reqMsg.getThreeRIInd());
                        }
                        if (reqMsg.getRecurringExpiry() != null) {
                            aReqOutJob.add("recurringExpiry", reqMsg.getRecurringExpiry());
                        }
                        if (reqMsg.getRecurringFrequency() != null) {
                            aReqOutJob.add("recurringFrequency", reqMsg.getRecurringFrequency());
                        }
                        if (reqMsg.getPurchaseInstalData() != null) {
                            aReqOutJob.add("purchaseInstalData", reqMsg.getPurchaseInstalData());
                        }
                        if (MessageVersion.V2_2_0.value().equals(messageVersion) && "30".equals(reqMsg.getBrowserColorDepth())) {
                            aReqOutJob.add("browserColorDepth", "24");
                        }
                        JsonObject aReqOutJ = aReqOutJob.build();
                        timings.reqMid = (int) (System.currentTimeMillis() - timings.reqStart);
                        forwardAReqToACS(record, reqMsg, aReqOutJ, merchant, acsList, attempts, results, timings, webContext, paymentSystemMessageProcessor);
                        return results;
                    }
                }
            }
        }

        // return ares with ireq and reason or ares not enrolled
        timings.reqMid = timings.reqEnd = timings.resStart = (int) (System.currentTimeMillis() - timings.reqStart);
        //reply.tdsMessageBase = createAResMessage(tdsMessageBase, record.getDSTransID(), ireqCode, reason, xts, statusReason, webContext);
        if (xts != null && statusReason != null) {
            aRes = createAResMessage(reqMsg, record.getDSTransID(), xts, statusReason, webContext, validationContext);
        } else {
            aRes = createErrorMessage(errorCode, messageVersion, reqMsg.getMessageType(), null, reason, new String[]{reqMsg.getSdkTransID(), reqMsg.getTdsServerTransID(), record.getDSTransID()});
        }
        JsonMessage aResOutJsonMessage = jsonMessageService.parse(mse.toJSONOB(aRes).build().toString());
        paymentSystemMessageProcessor.processAResOutgoing(aResOutJsonMessage, reqMsg.getIncomingJsonMessage());
        results.jsonRaw = jsonMessageService.writeOutputStream(new PublicBOS(512), aResOutJsonMessage);
        createNewTDSMessageData(aRes, record, results.jsonRaw, new java.util.Date(),
                                "DS:" + getDSID(), "3DS:" + webContext.getRequest().getRemoteAddr(), webContext);
        record.setTransStatus(aRes.getTransStatus());
        record.setTransStatusReason(aRes.getTransStatusReason());
        if (errorCode != null) {
            record.setErrorCode(errorCode.value);
            record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
        } else {
            record.setLocalStatus(DSModel.TSDRecord.Status.ERROR);
        }
        record.setErrorDetail(reason);

        record.setLocalDateEnd(new java.util.Date());
        safeUpdate(record, -1, webContext);
        timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
        results.tdsMessageBase = aRes;

        return results;
    }

    protected Results processRReq(TDSMessage reqMsg, byte[] rReqRawBuf, String[] rReqRawStr, Date now, Timings timings, WebContext wctx) throws Exception {
        Results results = new Results();
        TDSRecord record;
        TDSMessage resMsg;
        CertificateData cd = (CertificateData) wctx.getRequestAttribute("localCertificateData");
        String messageVersion = mse.getSupportedVersion();
        directory.getRequestsTiming().addRReqTiming(timings);

        String reason = null;
        ErrorCode errorCode = null;
        results.tdsRecord = record = Misc.isNotNullOrEmpty(reqMsg.getDsTransID()) ? persistenceService.getTDSRecordByDSTransId(reqMsg.getDsTransID()) : null;
        tdsRecordAttributeService.setTDSRecordAttributes(record);
        if (Misc.isNullOrEmpty(reqMsg.getMessageVersion()) && record != null && Misc.isNotNullOrEmpty(record.getProtocol())) {
            messageVersion = record.getProtocol();
        }

        if (record != null) {
            MDC.getMDCAdapter().put("tx", "tx" + record.getId());
            wctx.setPaymentSystemId(record.getPaymentSystemId());
        }

        if (record == null) {
            if (Misc.isNullOrEmpty(reqMsg.getDsTransID())) {
                errorCode = ErrorCode.cRequiredFieldMissing201;
                List<Error> errors = new ArrayList<>(1);
                errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "dsTransID", "is missing"));
                reason = this.createErrorDetails(errors).toString();
            } else {
                errorCode = ErrorCode.cTransientSysFailure403;
                reason = "Original matching DS record does not exist";
            }
            log.warn(
                    "Unable to match message DSID=" + reqMsg.getDsTransID() + " 3DSID=" + reqMsg.getTdsServerTransID() +
                    " to any existing records " +
                    (record != null ? " (record " + record.getId() + " macthes only by DSID)" : ""));
            if (record == null) {
                record = createNewTDSRecord(reqMsg, now, wctx);
                org.slf4j.MDC.getMDCAdapter().put("tx", "tx" + record.getId());
            }
        }

        JsonObjectBuilder rReqOutBuilder = null;
        PaymentSystemMessageProcessor paymentSystemMessageProcessor = PaymentSystemMsgProcessorRegistry.DEFAULT;
        if (reason == null) {
            ValidationContext validationContext = new ValidationContext();
            validationContext.put(ValidationContext.STRICT_VALIDATION, wctx.getStringSetting(DsSetting.STRICT_VALIDATION.getKey()));
            validationContext.put(ValidationContext.EXTRA_AUTHENTICATION_TYPES, wctx.getStringSetting(DsSetting.EXTRA_AUTHENTICATION_TYPES.getKey()));
            validationContext.put(ValidationContext.RREQ_NPA_AUTHENTICATION_REQUIRED, wctx.getStringSetting(DsSetting.RREQ_NPA_AUTHENTICATION_VALUE_REQUIRED.getKey()));
            validationContext.put(ValidationContext.RREQ_3RI_AUTHENTICATION_REQUIRED, wctx.getStringSetting(DsSetting.RREQ_3RI_AUTHENTICATION_VALUE_REQUIRED.getKey()));
            validationContext.put(ValidationContext.RREQ_NPA_TRANS_STATUS_REASON, wctx.getStringSetting(DsSetting.RREQ_NPA_TRANS_STATUS_REASON.getKey()));
            validationContext.put(ValidationContext.RREQ_ECI_REQUIRED, wctx.getStringSetting(DsSetting.RREQ_ECI_REQUIRED.getKey()));
            validationContext.put(ValidationContext.ACCEPTED_RREQ_STATUSES, wctx.getStringSetting(DsSetting.ACCEPTED_RREQ_STATUSES.getKey()));

            List<Error> errors = validateRReqEmv(reqMsg, record, validationContext);

            if (record.getPaymentSystemId() != null) {
                PaymentSystem paymentSystem = persistenceService.getPersistableById(record.getPaymentSystemId(), PaymentSystem.class);
                if (paymentSystem != null && Misc.isNotNullOrEmpty(paymentSystem.getType())) {
                    paymentSystemMessageProcessor =  PaymentSystemMsgProcessorRegistry.get(paymentSystem.getType());
                }
            }

            if (errors.size() == 0) {
                errors.addAll(paymentSystemMessageProcessor.validateRReqIncoming(reqMsg.getIncomingJsonMessage(), record));
            }

            if (errors.size() == 0) {
                errors.addAll(postValidateRReqDSConditional(reqMsg, record));
            }

            timings.reqMid = (int) (System.currentTimeMillis() - timings.reqStart);

            if (errors.size() == 0) {
                if (this.extensionService != null) {
                    Merchant mer = directory.getAquirerMerchant(wctx.getPaymentSystemId(), record.getLocalAcquirerId(),
                            record.getAcquirerMerchantID());

                    this.extensionService.createRReqExtensions(record, results, mer, reqMsg, 0L, this, wctx);
                }
                rReqOutBuilder = createFilteredJson(reqMsg, reqMsg.getJsonObject(), results.rreqExtensions, record, mse.isTreatEmptyValueAsMissing() ? null : errors);
            }

            if (errors.size() > 0) {
                Error e1 = getFirstError(errors);
                errorCode = e1.getCode();
                reason = this.createErrorDetails(errors).toString();
            }
        }

        TDSMessageData rreqData = null;
        boolean wrongState = false;
        if (record != null) {
            MDC.getMDCAdapter().put("tx", "tx" + record.getId());
            rreqData = createNewTDSMessageData(reqMsg, record, rReqRawStr, now,
                                               "ACS:" + wctx.getRequest().getRemoteAddr(), "DS:" + getDSID(), wctx);
        }

        // check if already in error then dont forward and return errror
        if (reason == null) {
            if (!TSDRecord.Status.CHANLLENGE_INPROCESS.equals(record.getLocalStatus())) {
                log.warn("Tx " + record.getId() + "/" + record.getDSTransID() + " status " + record.getLocalStatus() +
                         " not challenge in progress(" + TSDRecord.Status.CHANLLENGE_INPROCESS +
                         ") forwarding RReq is inappropriate");
                wrongState = true;
                // #772 for UL certification cases TC_DS_10017_001,2, TC_DS_10018_001,2
                // and EMVco specs says so
                errorCode = ErrorCode.cInvalidTransactionID301;
                reason = "Transaction state invalid forwarding RReq not appropriate";
            }
        }

        if (reason == null) {
            // 1st validate that sending acs if eligible to send this..
            List<ACSProfile> acsList = issuerService.getIssuerACSList(record.getLocalIssuerId(), null, DSModel.ACSProfile.Status.ACTIVE, true);

            if (acsList.size() < 0) {
                reason = "Unknown ACS";
                log.warn("Issuer with bin = " + record.getIssuerBIN() + " id = " + record.getLocalIssuerId() +
                         ") has not active ACS");
            }
            if (reason == null && cd != null) {
                boolean acsAndCertMatch = false;
                for (ACSProfile acp : acsList) {
                    if (acp.getInClientCert() != null && acp.getInClientCert().equals(cd.getId())) {
                        acsAndCertMatch = true;
                        break;
                    }
                }
                if (!acsAndCertMatch) {
                    errorCode = ErrorCode.cAccessDenied303;
                    reason = "Mismatching client certifcate";
                    log.warn("Certificate " + cd.getId() + " is not assigned as in auth with any of issuer " +
                             record.getLocalIssuerId() + " ACS profiles");
                }
            }
        }

        TDSServerProfile tdsServerProfile = null;
        if (record != null && record.getMIProfileId() != null) {
            tdsServerProfile = directory.getTdsServerProfile(wctx.getPaymentSystemId(), record.getMIProfileId());
        }

        if (tdsServerProfile == null) {
            if (reason == null) {
                reason = "Unknown 3DSServer(MI)";
            }
            log.warn("TDSServer with id " + record.getMIProfileId() + " not found");
        } else if (!DSModel.TDSServerProfile.Status.ACTIVE.equals(tdsServerProfile.getStatus())) {
            if (reason == null) {
                reason = "3DSServer(MI) has been disabled";
            }
            record.setMIProfileId(tdsServerProfile.getId());
            log.warn("TDSServer " + tdsServerProfile.getId() + " status " + tdsServerProfile.getStatus() + " not active");
            tdsServerProfile = null;
        }

        if (reason == null) {
            long diff = now.getTime() - record.getLocalDateStart().getTime();
            if (diff > max3DSSessionDuration) {
                errorCode = ErrorCode.cTransTimedOut402;
                reason = "Original matching DS record too old";
                log.warn("ACS returned RReq too late for " + record.getDSTransID() + " timediff " +
                         DateUtil.formatDate(new Date(diff), "D'd' HH:mm:ss", true));
            }
        }

        if (reason != null) {
            timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);
            timings.resStart = (int) (System.currentTimeMillis() - timings.reqStart);
            resMsg = createErrorMessage(messageVersion, errorCode !=
                                                        null ? errorCode : ErrorCode.cTransientSysFailure403, null, reason, reqMsg);

            resMsg.setTdsServerTransID(record.getTdsTransID());
            resMsg.setSdkTransID(record.getSDKTransID());
            results.tdsMessageBase = resMsg;
            results.jsonRaw = mse.toJSONBos(resMsg);

            TDSMessageData errorStore = createNewTDSMessageData(resMsg, record, results.jsonRaw, new Date(),
                                                                "DS:" + getDSID(),
                                                                "ACS:" + wctx.getRequest().getRemoteAddr(), wctx);

            // update only if correct status else ignore (duplicate for example)
            if (!wrongState) {
                record.setLocalStatus(DSModel.TSDRecord.Status.ACSERROR);
                record.setLocalDateEnd(new java.util.Date());
                record.setErrorCode(Misc.cut(resMsg.getErrorCode(), 3));
                record.setErrorDetail(Misc.cut(resMsg.getErrorDetail(), 2048));
                safeUpdate(record, -1, wctx);
            }
            timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);

            // NSPK #106
            String statusACSError;
            TDSModel.XtransStatus xtstaus = TDSModel.XtransStatus.U;
            String statusReasonValue = XtransStatusReason.C01AuthFailed.value;
            if (TDSModel.ErrorCode.cInvalidTxData305 == errorCode &&
                reason.contains("transStatus, transStatusReason") &&
                Misc.isNotNullOrEmpty(statusACSError = wctx.getStringSetting(DsSetting.STATUS_ACS_ERROR.getKey()))) {
                try {
                    xtstaus = TDSModel.XtransStatus.valueOf(statusACSError);
                } catch (Exception de) {
                    log.warn("statusACSError " + statusACSError + " unknown opting to default " + xtstaus.value());
                }
                String statusReasonACSError = wctx.getStringSetting(DsSetting.STATUS_REASON_ACS_ERROR.getKey());
                if (Misc.isNotNullOrEmpty(statusReasonACSError) && statusReasonACSError.length() == 2) {
                    statusReasonValue = statusReasonACSError;
                }
            }

            String onErr = wctx.getStringSetting(DsSetting.RREQ_IN_ERROR_SEND_3DS.getKey());
            // create and send an independent error to MI
            if (tdsServerProfile != null && !wrongState &&
                ("RReq".equals(onErr) || Misc.isNullOrEmpty(onErr) || "Erro".equals(onErr))) {
                log.info("Invoked line 2760");
                TDSMessage rreqError;
                byte[] rreqErrorOrig;
                TDSMessageData errorStore1;

                final byte[] rReqErrRawNew;
                if (Misc.isNullOrEmpty(onErr) || "Erro".equals(onErr)) {
                    rreqError = resMsg;
                    rReqErrRawNew = rreqErrorOrig = results.jsonRaw.toByteArray();
                    errorStore1 = errorStore;
                    log.warn("5.9.8 Copy error on ACS RReq " + rreqError.getMessageType() + " to 3DS with " +
                             rreqError.getErrorCode() + "/" + rreqError.getErrorDetail());
                } else {
                    rreqError = createRReqMessage(reqMsg, record, xtstaus, statusReasonValue, TDSModel.ChallengeCancelInd2_0_1.CO5_TxError, wctx);
                    rreqErrorOrig = null;
                    rReqErrRawNew = mse.toJSON(rreqError);
                    errorStore1 = null;
                    log.warn("Selfgen " + rreqError.getMessageType() + " to 3DS with transStatus/transStatusReason " +
                             rreqError.getTransStatus() + "/" + rreqError.getTransStatusReason());
                }

                TDSRecord finalRecord = record;
                TDSServerProfile finalTDSServer = tdsServerProfile;
                PaymentSystemMessageProcessor finalPsmp = paymentSystemMessageProcessor;

                // run this error posting in stanalone thread
                Runnable cmd = () -> {
                    try {
                        forwardRReqToMI(finalRecord, rreqError, rreqErrorOrig, rReqErrRawNew, errorStore1, finalTDSServer, false, results, timings, wctx, finalPsmp);
                    } catch (Exception fe) {
                        log.error("Failed to send selfgen error RReq " + finalRecord.getId() + "/" +
                                  finalRecord.getDSTransID() + " to 3DServer " + finalRecord.getTdsServerURL());
                    }
                };
                getScheduledExecutorService().execute(cmd);
            } // copy acs error to MI
            timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
            return results;
        }

        record.setLocalStatus(DSModel.TSDRecord.Status.CHANLLENGE_COMPLETED);
        record.setLocalDateEnd(new java.util.Date());
        safeUpdate(record, -1, wctx);

        // from acs
        record.setAuthenticationMethod(reqMsg.getAuthenticationMethod());
        record.setAuthenticationType(reqMsg.getAuthenticationType());

        boolean isStoreAuthValue = wctx.isTrueSetting(DsSetting.STORE_AUTHENTICATION_VALUE.getKey());
        if (isStoreAuthValue) {
            try {
                if (StringUtils.isNotEmpty(reqMsg.getAuthenticationValue())) {
                    String encryptedAuthValue = cryptoService.encryptData(reqMsg.getAuthenticationValue());
                    tdsRecordAttributeService.saveAuthenticationValue(encryptedAuthValue, record);
                }
            } catch (Exception e) {
                log.error("Error on saving record attribute.", e);
            }
        }

        record.setECI(reqMsg.getEci());
        record.setTransStatus(reqMsg.getTransStatus());
        record.setResultsStatus(Misc.cut(reqMsg.getResultsStatus(), 2));

        timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);
        //custom filtering of RReq Out
        JsonMessage rReqOutJsonMessage = this.jsonMessageService.parse(rReqOutBuilder.build().toString());
        paymentSystemMessageProcessor.processRReqOutgoing(rReqOutJsonMessage);
        byte[] rReqRawNew = rReqOutJsonMessage.toString().getBytes(StandardCharsets.UTF_8);
        return forwardRReqToMI(record, reqMsg, rReqRawBuf, rReqRawNew, rreqData, tdsServerProfile, true, results, timings, wctx, paymentSystemMessageProcessor);
    }

    protected Results processPReq(TDSMessage reqMsg, String[] msgRawStr, Date now, Timings timings, WebContext wctx) throws Exception {
        Results results = new Results();
        directory.getRequestsTiming().addPReqTiming(timings);
        TDSMessage resMsg;
        timings.reqMid = (int) (System.currentTimeMillis() - timings.reqStart);

        ValidationContext validationContext = new ValidationContext();
        validationContext.put(ValidationContext.STRICT_VALIDATION, wctx.getStringSetting(DsSetting.STRICT_VALIDATION.getKey()));

        List<Error> errors = new ArrayList<>();
        String threeDSServerOperatorID = reqMsg.getThreeDSServerOperatorID();
        if (Misc.isNotNullOrEmpty(threeDSServerOperatorID)) {
            // ELO-specific configuration workaround GH-1169
            String jointPaymentSystemIds = wctx.getStringSetting(DsSetting.JOINT_PAYMENT_SYSTEMS.getKey());
            if (StringUtils.isNotEmpty(jointPaymentSystemIds)) {
                Integer paymentSystemIdFrom3DSS = getPaymentSystemIdFromJoint3DSS(jointPaymentSystemIds,
                        wctx.getPaymentSystemId(), reqMsg.getThreeDSServerOperatorID());
                wctx.setPaymentSystemId(paymentSystemIdFrom3DSS);
            }

            TDSServerProfile activeTDSServerProfileByOperatorId =
                    acquirerService.getActiveTdsServerProfileByOperatorId(wctx.getPaymentSystemId(), threeDSServerOperatorID);

            if (activeTDSServerProfileByOperatorId == null) {
                errors.add(new Error(ErrorCode.cAccessDenied303, "threeDSServerOperatorID", "invalid value"));
            }
        }

        if (errors.isEmpty()) {
            errors = validatePReq(reqMsg, validationContext);
        }

        if (errors.isEmpty()) {
            validatePReqPaymentSystemSpecific(wctx.getPaymentSystemId(), reqMsg, errors);
        }

        if (errors.isEmpty()) {
            errors.addAll(postValidatePReqDSConditional(reqMsg));
        }

        String mversion = mse.getSupportedVersion();
        TDSRecord record = results.tdsRecord = createNewTDSRecord(reqMsg, now, wctx);
        MDC.getMDCAdapter().put("tx", "tx" + record.getId());
        record.setDSTransID(createUUID(getUUIDPrefix(wctx), record.getId()));
        createNewTDSMessageData(reqMsg, record, msgRawStr, now,
                                "3DS:" + wctx.getRequest().getRemoteAddr(), "DS:" + getDSID(), wctx);
        timings.reqEnd = (int) (System.currentTimeMillis() - timings.reqStart);
        if (errors.size() > 0) {
            timings.resStart = (int) (System.currentTimeMillis() - timings.reqStart);
            Error e1 = getFirstError(errors);
            resMsg = createErrorMessage(mversion, e1.getCode(), null, this.createErrorDetails(errors), reqMsg);
            results.jsonRaw = mse.toJSONBos(resMsg);
            createNewTDSMessageData(resMsg, record, results.jsonRaw, new Date(),
                                    "DS:" + getDSID(), "3DS:" + wctx.getRequest().getRemoteAddr(), wctx);
            record.setLocalStatus(DSModel.TSDRecord.Status.MIERROR);
            record.setLocalDateEnd(new Date());
            record.setErrorCode(resMsg.getErrorCode());
            record.setErrorDetail(resMsg.getErrorDetail());
            safeUpdate(record, -1, wctx);
            timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
            results.tdsMessageBase = resMsg;
            return results;
        }
        timings.resStart = (int) (System.currentTimeMillis() - timings.reqStart);
        // caching..
        resMsg = createPResMessage(reqMsg, record, wctx);

        if (Boolean.TRUE.equals(wctx.getRequestAttribute("pResUseRaw"))) {
            PublicBOS bos = new PublicBOS(2048);
            results.jsonRaw = this.cachedRangeDataRawMap.get(wctx.getPaymentSystemId());
            results.jsonRaw2 = pResSpecial(mse.toJSONBos(resMsg));

            if (results.jsonRaw.getCount() > 2048) {
                bos.write(results.jsonRaw.getBuf(), 0, 960);
                byte[] contentStub = "-.....-".getBytes(); // replaces middle part of long message with dots
                bos.write(contentStub);
                bos.write(results.jsonRaw.getBuf(), results.jsonRaw.getCount() - 960, 960);
            } else {
                bos.write(results.jsonRaw.getBuf(), 0, results.jsonRaw.getCount());
            }
            bos.write(results.jsonRaw2.getBuf(), 0, results.jsonRaw2.getCount());
            //	partial placebo message with 100 start and 100 end samples
            String pRes = bos.toString(StandardCharsets.UTF_8);
            savePResTDSMessage(resMsg, record, pRes, "DS:" + getDSID(), "3DS:" + wctx.getRequest().getRemoteAddr(), wctx);
        } else {
            results.jsonRaw = mse.toJSONBos(resMsg);
            String pRes = results.jsonRaw.toString(StandardCharsets.UTF_8);
            savePResTDSMessage(resMsg, record, pRes, "DS:" + getDSID(), "3DS:" + wctx.getRequest().getRemoteAddr(), wctx);
        }

        if (resMsg.getErrorCode() != null) {
            record.setLocalStatus(DSModel.TSDRecord.Status.ERROR);
        } else {
            record.setLocalStatus(DSModel.TSDRecord.Status.COMPLETED);
        }
        record.setLocalDateEnd(new Date());
        record.setErrorCode(Misc.cut(resMsg.getErrorCode(), 3));
        record.setErrorDetail(Misc.cut(resMsg.getErrorDetail(), 2048));
        safeUpdate(record, -1, wctx);
        tdsRecordAttributeService.saveTDSRecordAttributesPreq(record, wctx.getRequestServerPort());
        timings.resMid = (int) (System.currentTimeMillis() - timings.reqStart);
        results.tdsMessageBase = resMsg;
        logFullPResIfEnabled(wctx, results);
        return results;
    }

    private void logFullPResIfEnabled(WebContext wctx, Results results) {
        if (wctx.isTrueSetting(DsSetting.ENABLE_LOGGING_FULL_PRES.getKey())) {
            log.info("Sending full PRes for threeDSServerTransID = {}", results.tdsMessageBase.getTdsServerTransID());
            log.info("{}", results.jsonRaw.toString(StandardCharsets.UTF_8));
        }
    }

    protected Map<Integer, TDSMessage> pResMessageCache;

    public void initPRes() {
        if (pResMessageCache == null) {
            pResMessageCache = new HashMap<>();
        }
        pResMessageCache.clear();
    }

    protected XcardRangeData createRangeData(CardRange cardRange, WebContext wctx, boolean add) {
        XcardRangeData rangeData = new XcardRangeData();
        if (issuerService.isBINRangeMode() || cardRange.isRangeMode()) {
            cardRange.setRangeMode();
            rangeData.setStartRange(cardRange.getStart());
            rangeData.setEndRange(cardRange.getEnd());
        } else {
            if (cardRange.isRangeMode()) {
                cardRange.setEnd(null);
            }
            rangeData.setStartRange(Misc.padCutStringRight(cardRange.getBin(), '0', 16));
            rangeData.setEndRange(Misc.padCutStringRight(cardRange.getBin(), '9', 16));
        }

        if (cardRange.isIncludeThreeDSMethodURL()) {
            rangeData.setThreeDSMethodURL(cardRange.getMethodURL());
        }

        // DS protocol version
        String minSupportedDS = MessageVersion.minSupported().value();
        if (minSupportedDS == null) {
            minSupportedDS = TDSModel.MessageVersion.V2_1_0.value();
        }
        rangeData.setDsStartProtocolVersion(minSupportedDS);

        String maxSupportedDS = MessageVersion.maxSupported().value();

        if (maxSupportedDS == null) {
            maxSupportedDS = TDSModel.MessageVersion.V2_3_0.value();
        }

        rangeData.setDsEndProtocolVersion(maxSupportedDS);

        // ACS protocol version
        rangeData.setAcsStartProtocolVersion(cardRange.getStartProtocolVersion());
        rangeData.setAcsEndProtocolVersion(cardRange.getEndProtocolVersion());

        rangeData.setActionInd(add ? ACTION_IND_ADD : ACTION_IND_DELETE);
        return rangeData;
    }

    @Override
    public void createAndAddRangeData(CardRange cardRange, WebContext webContext) {
        XcardRangeData rangeData = createRangeData(cardRange, webContext, true);
        TDSMessage pResMessage = pResMessageCache.get(cardRange.getPaymentSystemId());
        if (pResMessage == null) {
            pResMessage = mse.createNewMessage();
            pResMessageCache.put(cardRange.getPaymentSystemId(), pResMessage);
        }
        pResMessage.getCardRangeData().add(rangeData);
    }

    public void createRawCache() throws Exception {
        for (Map.Entry<Integer, TDSMessage> paymentSystemPresEntry : pResMessageCache.entrySet()) {
            TDSMessage tdsMessage = paymentSystemPresEntry.getValue();
            ByteArrayOutputStreamUnreadable bos =
                    new ByteArrayOutputStreamUnreadable(tdsMessage.getCardRangeData().size() * 128);
            mse.toJSON(tdsMessage, bos);

            char curr = 0;
            int unread = 0;
            do {
                int c = bos.unread();
                unread++;
                if (c < 0) {
                    break;
                }
                curr = (char) c;
                if (curr == ']') {
                    bos.recount();
                    break;
                }
            } while (true);
            cachedRangeDataRawMap.put(paymentSystemPresEntry.getKey(), bos);
            log.info("Intialized new pRes ranges raw " + bos.getCount() + " bytes");
        }
    }

    /**
     * @param req
     * @param jsonSrc
     * @param record
     * @param errors  - null or a list if deep copy emty array/object is validated
     * @return
     */
    public JsonObjectBuilder createFilteredJson(TDSMessage req, JsonObject jsonSrc, JsonArray addExtensions, TDSRecord record, List<Error> errors) {
        String ch = req.getDeviceChannel();
        if (ch == null && record != null) {
            ch = record.getDeviceChannel();
        }
        String cat = req.getMessageCategory();
        if (cat == null) {
            cat = record.getMessageCategory();
        }
        return MessageService.createFilteredJson(jsonSrc, addExtensions, req.getMessageType(), ch, cat, req.getTransStatus(), filters, errors);
    }

    // send forked error to error returned party during "ends processing" because of error
    void sendForkedErrorMessage(final java.net.URL url, final String acsOrMi, final HttpsJSSEClient fclient, final PublicBOS errorBos, final TDSRecord frecord, final WebContext wctx) {
        log.info("Send Forked error message"); // Extra log
        Runnable cmd = new Runnable() {
            public void run() {
                Map<String, String> resph = new java.util.HashMap<String, String>();
                byte[] resp = null;
                try {
                    resp = fclient.post(url.toString(), MessageService.CT_JSON_UTF8, errorBos.getBuf(), 0, errorBos.getCount(), null, resph);
                    if (resp != null && resp.length > 0) {
                        TDSMessage info = new TDSMessage();
                        info.setMessageType("RAW");
                        createNewTDSMessageData(info, frecord, resp, new java.util.Date(),
                                                acsOrMi + ":" + url.getHost(), "DS:" + getDSID(), wctx);
                    }
                } catch (Exception cfe) {
                    log.warn("Posting an error to " + acsOrMi + " to " + url + " failed " + cfe +
                             " but must live with it");
                    String rspCode = resph.get("HTTP_RESPONSE");
                    String rspMsg = resph.get("HTTP_RESPONSE_MSG");
                    String rspCt = resph.get("Content-Type");
                    String detailErr = resph.get("errorData");
                    String infoStr = ("No content, HTTP code: " + rspCode +
                                      (rspMsg != null ? ", HTTP Msg: " + rspMsg : "") +
                                      (rspCt != null ? ", HTTP content-type " + rspCt : "") +
                                      (detailErr != null ? ", Err content: " + detailErr : ""));

                    TDSMessage info = new TDSMessage();
                    info.setMessageType("INFO");
                    try {
                        createNewTDSMessageData(info, frecord, new String[]{infoStr}, new java.util.Date(),
                                                acsOrMi + ":" + url.getHost(), "DS:" + getDSID(), wctx);
                    } catch (Exception ee) {
                        log.error("Failed to created INFO data for" + frecord.getDSTransID() + ": " + infoStr, ee);
                    }
                }
            } // run
        }; // command
        getScheduledExecutorService().execute(cmd);
    }

    protected Set<String> getTransStatusValuesARes() {
        return transStatusValues21ARes;
    }

    private Results processUnknownPaymentSystem(Date now,
                                                WebContext webContext) throws Exception {

        TDSRecord tdsRecord = new TDSRecord();
        tdsRecord.setLocalDateStart(now);
        tdsRecord.setPurchaseDate(now);

        try {
            persistenceService.save(tdsRecord, AbstractDirectoryService.C_MAX_DB_STORE_TX_TIME);
            MDC.getMDCAdapter().put("tx", "tx" + tdsRecord.getId());

            String messageVersion = TDSModel.MessageVersion.V2_1_0.value();
            Results responseResults = createErrorResults(messageVersion, ErrorCode.cAccessDenied303,
                    null, null, "Unassigned port number", null);

            String source = "DS:" + getDSID();
            createNewTDSMessageData(responseResults.tdsMessageBase, tdsRecord,
                    responseResults.jsonRaw, new Date(), source, webContext.getRequest().getRemoteAddr(), webContext);

            tdsRecord.setLocalStatus(DSModel.TSDRecord.Status.ERROR);
            tdsRecord.setLocalDateEnd(new Date());
            tdsRecord.setErrorCode(Misc.cut(responseResults.tdsMessageBase.getErrorCode(), 3));
            tdsRecord.setErrorDetail(Misc.cut(responseResults.tdsMessageBase.getErrorDetail(), 2048));
            safeUpdate(tdsRecord, -1, webContext);
            tdsRecordAttributeService.saveIncomingPortNumber(tdsRecord, webContext.getRequestServerPort());
            return responseResults;
        } catch (Exception e) {
            log.error("Unable to process transaction response for an unknown payment system/port", e);
            throw e;
        }
    }

    /*
     * Workaround for ELO sharing a DS port number with another payment system. In this scenario ELO payment system id
     * is resolved by looking up a 3DSS profile from both payment systems based on operator id.
     */
    private Integer getPaymentSystemIdFromJoint3DSS(String jointPaymentSystemIds,
                                                    Integer paymentSystemIdByPort,
                                                    String threeDSServerOperatorID) {

        // limit to only 2 PS to avoid restrict further deprecated functionality misuse
        String[] paymentSystemIds = jointPaymentSystemIds.split(",", 2);

        // If port-based payment system is not part of "joint" payment systems, then leave PS id unchanged.
        if (!ArrayUtils.contains(paymentSystemIds, String.valueOf(paymentSystemIdByPort))) {
            return paymentSystemIdByPort;
        }
        log.info("Processing request for joint payment system ID-s: {}. Looking up 3DSS by operatorID={}",
                Arrays.toString(paymentSystemIds), threeDSServerOperatorID);

        TDSServerProfile tdsServerProfile =
                acquirerService.getActiveTdsServerProfileByOperatorId(Integer.parseInt(paymentSystemIds[0]), threeDSServerOperatorID);

        if (tdsServerProfile == null) {
            tdsServerProfile = acquirerService.getActiveTdsServerProfileByOperatorId(Integer.parseInt(paymentSystemIds[1]), threeDSServerOperatorID);
        }

        if (tdsServerProfile == null) {
            log.info("3DS Server profile (operatorID={}) not found under joint payment systems." +
                    " Using port-based payment system id={}", threeDSServerOperatorID, paymentSystemIdByPort);
            return paymentSystemIdByPort;
        }

        Integer tdsServerPSid = tdsServerProfile.getPaymentSystemId();
        log.info("Using Payment System ID={} from 3DSS profile={}", tdsServerPSid, tdsServerProfile.getId());
        return tdsServerPSid;
    }

    @Override
    public void periodic(WebContext wctx) {
        if (this.extensionService != null) {
            this.extensionService.periodic(wctx);
        }
    }

    private List<Error> validateErroIncoming(TDSMessage erroMessage, TDSMessage aReq){
        assert mse.isErrorMessage(erroMessage);

        List<Error> errors = validateErroRequiredFields(erroMessage);
        if(errors.isEmpty()){
            errors = validateErroFormatFields(erroMessage, aReq);
        }
        return errors;
    }

    private List<Error> validateErroRequiredFields(TDSMessage erroMessage){
        List<Error> errors = new ArrayList<>();
        if (Misc.isNullOrEmpty(erroMessage.getMessageVersion())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.messageVersion));
        }

        if (Misc.isNullOrEmpty(erroMessage.getErrorCode())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.errorCode));
        }

        if (Misc.isNullOrEmpty(erroMessage.getErrorComponent())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.errorComponent));
        }

        if (Misc.isNullOrEmpty(erroMessage.getErrorDescription())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.errorDescription));
        }

        if (Misc.isNullOrEmpty(erroMessage.getErrorDetail())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.errorDetail));
        }

        if (Misc.isNullOrEmpty(erroMessage.getErrorMessageType())) {
            errors.add(ErrorUtils.fieldMissingError(FieldName.errorMessageType));
        }

        return errors;
    }

    private List<Error> validateErroFormatFields(TDSMessage erroMessage, TDSMessage aReq){
        List<Error> errors = new ArrayList<>();
        if (!aReq.getMessageVersion().equals(erroMessage.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.messageVersion, "AReq/AREs messageVersion mismatch"));
        }

        if (!ErrorCode.contains(erroMessage.getErrorCode())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.errorCode, "invalid value"));
        }

        if (!ErrorComponent.contains(erroMessage.getErrorComponent() )) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.errorComponent, "invalid value"));
        }

        if (erroMessage.getErrorDescription().length() > FieldMaxLength.ERROR_DESCRIPTION) {
            String errorDetail = "Maximum number of characters is " + FieldMaxLength.ERROR_DESCRIPTION;
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.errorDescription, errorDetail));
        }

       if (erroMessage.getErrorDetail().length() > FieldMaxLength.ERROR_DETAIL) {
            String errorDetail = "Maximum number of characters is " + FieldMaxLength.ERROR_DETAIL;
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.errorDetail, errorDetail));
        }

        if (!XmessageType.contains(erroMessage.getErrorMessageType())) {
            errors.add(new Error(ErrorCode.cInvalidFieldFormat203, FieldName.errorMessageType, "invalid value"));
        }

        return errors;
    }
}
