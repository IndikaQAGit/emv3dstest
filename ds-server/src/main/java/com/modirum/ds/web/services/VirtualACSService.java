/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 8. sept 2016
 *
 */
package com.modirum.ds.web.services;

import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.web.context.WebContext;

/**
 * Placeholder to implement any embed attempts ACS or anything.
 */
public class VirtualACSService {

    public byte[] processAReq(TDSMessageBase aReq, WebContext wctx) {
        throw new RuntimeException("VirtualACSService Not yet implemented");
    }
}
