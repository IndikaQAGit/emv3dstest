package com.modirum.ds.web.util;

import com.modirum.ds.model.Error;
import com.modirum.ds.utils.DateUtil;

import java.util.Date;
import java.util.List;

public class Utils {

    private static final Date MAX_DB_SAFE_MAX_DATE = DateUtil.parseDate("99991231 23:59:59", "yyyyMMdd HH:mm:ss", true);
    private static final Date MIN_DB_SAFE_MAX_DATE = DateUtil.parseDate("00010101 00:00:00", "yyyyMMdd HH:mm:ss", true);

    /**
     * Get first error with non null errorCode.
     */
    public static Error getFirstError(List<Error> errors) {
        for (Error error : errors) {
            if (error.getCode() != null) {
                return error;
            }
        }
        return null;
    }

    public static boolean isArraysEqualsLength(byte[] a1, byte[] a2, int len2) {
        if (a1 == a2) {
            return true;
        }
        if (a1 == null || a2 == null) {
            return false;
        }

        int length = a1.length;
        if (len2 != length) {
            return false;
        }

        for (int i = 0; i < length; i++) {
            if (a1[i] != a2[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if a date is within the range.
     */
    public static boolean isDbSafeDate(Date date) {
        return date == null || !date.before(MIN_DB_SAFE_MAX_DATE) && !date.after(MAX_DB_SAFE_MAX_DATE);
    }
}
