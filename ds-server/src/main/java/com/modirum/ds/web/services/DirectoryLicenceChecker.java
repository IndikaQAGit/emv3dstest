package com.modirum.ds.web.services;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ProductInfoService;
import com.modirum.ds.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class DirectoryLicenceChecker {

    private static final Logger log = LoggerFactory.getLogger(DirectoryLicenceChecker.class);

    private static final long LICENCE_DAYS_REMINDER = 30;
    private static final long ONE_HOUR_IN_MILISECONDS = 3600L * 1000L;
    private static final long LICENSE_CHECK_INTERVAL = 24L * ONE_HOUR_IN_MILISECONDS;

    private final PersistenceService persistenceService;
    private final ProductInfoService productInfoService;

    private long lastLicenseCheck;
    private ProductInfo prodInfo;

    /**
     * Constructor.
     */
    public DirectoryLicenceChecker(PersistenceService persistenceService, ProductInfoService productInfoService) {
        this.persistenceService = persistenceService;
        this.productInfoService = productInfoService;
    }

    /**
     * Check licencing.
     */
    public void checkLicensing() {
        // once started check once per day only
        if (lastLicenseCheck > 0 && lastLicenseCheck > System.currentTimeMillis() - LICENSE_CHECK_INTERVAL) {
            return;
        }

        try {
            // reload license info if not in startup
            prodInfo = productInfoService.loadProductInfo();

            log.info("Product: " + ProductInfo.cProductName + " " + ProductInfo.cProductVersion);

            String buildVersion = ProductInfo.getBuildVersion() + " (" +
                                  DateUtil.formatDate(new Date(ProductInfo.getBuildDate()), "yyyyMMdd HH:mm:ss", true) +
                                  ")";
            Setting bvs = persistenceService.getSettingById(DsSetting.DS_BUILD_VERSION.getKey());
            if (bvs == null || !buildVersion.equals(bvs.getValue())) {
                if (bvs == null) {
                    bvs = new Setting();
                    bvs.setKey(DsSetting.DS_BUILD_VERSION.getKey());
                    bvs.setProcessorId((short) 0);
                    bvs.setComment("Running DS version");
                }
                bvs.setValue(buildVersion);
                persistenceService.saveOrUpdate(bvs);
            }

            String lif = prodInfo.getProduct() + " " + prodInfo.getVersion() + " (" + buildVersion + ")" +
                         " License info - Licensee: " + prodInfo.getLicensee() + " Max merchants: " +
                         prodInfo.getMaxMerchants() + " Expiration " + prodInfo.getExpiration();
            log.info("License: " + lif);

            int merchants = persistenceService.countActiveMerchants();
            log.info("Current active merchants " + merchants);

            if (prodInfo.isAllValidAndCorrect(merchants)) {
                log.info("License info validated and correct.");
            } else {
                log.error("License info invalid or expired, application cannot continue");
                throw new RuntimeException("License info invalid or expired, application cannot continue");
            }

            if (prodInfo.expiresInDays() > -1 && prodInfo.expiresInDays() < LICENCE_DAYS_REMINDER) {
                log.warn("WARNING: Your " + prodInfo.getProduct() + " license is about to expire in " +
                         prodInfo.expiresInDays() + " days!");
            }

            if (prodInfo.getMerchantsLimit() > 0 && merchants > 0 &&
                (prodInfo.getMerchantsLimit() - merchants < 3 || prodInfo.getMerchantsLimit() * 100 / merchants < 5)) {
                String mlw =
                        "WARNING: Your " + prodInfo.getProduct() + " is licensed for " + prodInfo.getMerchantsLimit() +
                        " merchants, " + " you have currntly " + merchants + " active merchants, so only " +
                        (prodInfo.getMerchantsLimit() - merchants) + " new merchants can be added";
                log.warn(mlw);
            }

        } catch (Exception e) {
            log.error("Error validating licensing information, application cannot continue " + e, e);
            throw new RuntimeException(e);
        }

        lastLicenseCheck = System.currentTimeMillis();
    }

    /**
     * Check extension licensing.
     */
    public boolean checkExtensionLicensing(String ext) throws RuntimeException {
        if (prodInfo == null) {
            try {
                prodInfo = productInfoService.loadProductInfo();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return prodInfo.checkExtensionLicense(ext);
    }
}
