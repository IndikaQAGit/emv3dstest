<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isErrorPage = "true"%>
<html>
<head>
    <title>Page not found</title>
</head>
<body>
<div align="center">
    Sorry, page not found.
    <br/><br/>
<%
try {
	org.slf4j.Logger log=org.slf4j.LoggerFactory.getLogger("com.modirum.mdpay.ds.web.Errors");
	log.error("DS page not found "+ pageContext.getErrorData().getRequestURI());
	
} catch (Exception e) {}

%>  
</div>
</body>
</html>
