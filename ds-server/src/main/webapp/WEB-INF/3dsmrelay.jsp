<%@page	import="com.modirum.ds.utils.Misc,com.modirum.ds.utils.DateUtil,java.util.Date"%><%--
 this implements 3dsmethod relay scripts and html --%><%
	String acs3dsmURL = (String) request.getAttribute("acs3dsmURL");
	String mpi3dsmData = (String) request.getAttribute("3DSMethodData");
	String notiUrl = (String) request.getAttribute("3dsmNotiURL");
	String tdsId = (String) request.getAttribute("tdsId");
	String xid = (String) request.getAttribute("xid");
	String fpScriptUrl = (String) request.getAttribute("fpScriptUrl");
	String everCookieValue = (String) request.getAttribute("everCookieValue");
	String everCookieName = (String) request.getAttribute("everCookieName");
	String everCookieURI=(String)request.getAttribute("ecURI");
	Boolean singleCollect=(Boolean)request.getAttribute("singleCollect");
	String collectURI=(String)request.getAttribute("collectURI");
%><!DOCTYPE html>
<html>
<head>
<title>3DS Method Relay/DD Collection </title>
<script type="text/javascript" src="<%=fpScriptUrl%>"> <!-- script script  --> </script>
<% if (everCookieName != null && everCookieValue != null) 
{ %>
	<script type="text/javascript" src="<%=everCookieURI %>swfobject-2.2.min.js"> <!-- script script  -->  </script>
	<script type="text/javascript" src="<%=everCookieURI %>dtjava.js"> <!-- script script  -->  </script>
	<script type="text/javascript" src="<%=everCookieURI %>evercookie.js"> <!-- script script  --> </script>
<% } %>
<script type="text/javascript">
	var singleCollect = <%=Boolean.TRUE.equals(singleCollect) %>;
	var notified = false;
	var notified1 = false;
	var notified2 = false;
	var eco=null;
	var details = null;
	function del(eln) { return document.getElementById(eln); }
	function sv(eln, val) {
		var e=del(eln);
		if (e!=null)
		{
			e.value=val;
		}
	}
	function notify3DS() {
		if (!notified) {
			var f3m=del('3dsmethodrecall')
			if (f3m!=null)
			{	
				f3m.submit();
				notified = true;
			}
		}
	}
	
	function notifyCollectOnUnload()
	{
		singleCollect=false;
		notifyCollect();
	}
	
	
	function notifyCollect() {
		if (eco!=null && details!=null)
		{	
			if (!singleCollect && notified1 && !notified2)
			{
				notified2 = true;
				del('local3dsm2').submit();
			}	
			else if (!singleCollect && !notified1 && notified2)
			{
				notified1 = true;
				del('local3dsm1').submit();
			}	
			else
			{	
				notified1 = true;
				notified2 = true;
				del('local3dsm1').submit();
			}
		}
		else if (singleCollect)
		{
			
		}	
		else if (eco!=null)
		{	
			notified2 = true;
			del('local3dsm2').submit();
		}
		else if (details!=null)
		{	
			notified1 = true;
			del('local3dsm1').submit();
		}
	}
</script>
</head>
<body onunload="notifyCollectOnUnload(); notify3DS();">
	Server time: <%=DateUtil.formatDate(new Date(), "yyyyMMdd HH:mm:ss", true)%> GMT<br/>
	<%
	if (acs3dsmURL != null && acs3dsmURL.length() > 0) {
%>
	<iframe style="height: 1px;" name="tdsmethodrelayframe"	id="tdsmethodrelayframe"> <!-- doc --> </iframe>
	<form id="tdsmethodrelay" name="tdsmethodrelay" action="<%=Misc.encodeHTMLForForm(acs3dsmURL)%>" method="post" target="tdsmethodrelayframe">
		<input type="hidden" name="threeDSMethodData" value="<%=mpi3dsmData%>" /> 
		<input type="hidden" name="3DSMethodData" value="<%=mpi3dsmData%>" />
	</form>
	<script>
		var form = del("tdsmethodrelay");
		if (form != null) 
		{
			form.submit();
			notified = true;
		}
	</script>
	<%
	} // send notification on your own if acs has no 3ds method support
	else if (notiUrl != null && notiUrl.length() > 0) 
	{
	%><br/><iframe style="height: 1px;" name="tdsmethodnotiframe" id="tdsmethodnotiframe"> <!-- doc --> </iframe>
	<form id="3dsmethodrecall" name="3dsmethodrecall" action="<%=Misc.encodeHTMLForForm(notiUrl)%>" method="post" target="tdsmethodnotiframe">
		<input type="hidden" name="From3DSMRelay"	value="<%=Misc.encodeHTMLForForm(tdsId)%>" />
	</form>
	<script type="text/javascript">
		var timer = setTimeout("notify3DS();", 2000);
	</script>
	<%
	}
	%>
	<br/><iframe style="height: 1px;" name="localpost1" id="localpost1"> <!-- doc --> </iframe>
	<br/><iframe style="height: 1px;" name="localpost2" id="localpost2"> <!-- doc --> </iframe>
	<form id="local3dsm1" name="local3dsm1" action="<%=collectURI %>" method="post" target="localpost1">
		<input type="hidden" id="devicedata" name="deviceData" value="" /> 
		<input type="hidden" id="tdsTransId1" name="tdsTransId" value="<%=Misc.encodeHTMLForForm(tdsId)%>" />
		<input type="hidden" id="xid1" name="xid" value="<%=Misc.encodeHTMLForForm(xid)%>" />
		<input type="hidden" id="fpversion" name="version" value="" /> 
		<input type="hidden" id="TDS2_Navigator_language" name="TDS2_Navigator_language" value="" />
		<input type="hidden" id="TDS2_Navigator_jsEnabled" name="TDS2_Navigator_jsEnabled" value="" />
		<input type="hidden" id="TDS2_Navigator_javaEnabled" name="TDS2_Navigator_javaEnabled" value="" /> 
		<input type="hidden" id="TDS2_Screen_colorDepth" name="TDS2_Screen_colorDepth" value="" />
		<input type="hidden" id="TDS2_Screen_height" name="TDS2_Screen_height" value="" /> 
		<input type="hidden" id="TDS2_Screen_width" name="TDS2_Screen_width" value="" /> 
		<input type="hidden" id="TDS2_Screen_pixelDepth" name="TDS2_Screen_pixelDepth" value="" /> 
		<input type="hidden" id="TDS2_TimezoneOffset" name="TDS2_TimezoneOffset" value="" /> 
		<input type="hidden" id="TDS2_Navigator_UA" name="TDS2_Navigator_UA" value="" />
		<input type="hidden" id="EC1" name="EC" value="" />
		<input type="hidden" id="TM1" name="TM" value="" />
	</form>
	<form id="local3dsm2" name="local3dsm2" action="<%=collectURI %>" method="post" target="localpost2">
		<input type="hidden" id="tdsTransId2" name="tdsTransId" value="<%=Misc.encodeHTMLForForm(tdsId)%>" />
		<input type="hidden" id="xid1" name="xid" value="<%=Misc.encodeHTMLForForm(xid)%>" />
		<input type="hidden" id="EC2" name="EC" value="" />
		<input type="hidden" id="TM2" name="TM" value="" />
	</form>

	<script type="text/javascript">
	var st = new Date();
		function populateBrwData() {
			 var elx=null;
				if ((elx=del("TDS2_Navigator_language")) != null && typeof(navigator.language)!=undefined) {
					elx.value = (navigator.language || '');
				}
				if ((elx=del("TDS2_Navigator_jsEnabled")) != null) {
					elx.value = "true";
				}
				if ((elx=del("TDS2_Navigator_javaEnabled")) != null && typeof(navigator.javaEnabled)!=undefined) {
					elx.value = navigator.javaEnabled();
				}
				if ((elx=del("TDS2_Screen_colorDepth")) != null && typeof(screen.colorDepth)!=undefined) {
					elx.value = screen.colorDepth;
				}
				if ((elx=del("TDS2_Screen_height")) != null && typeof(screen.height)!=undefined) {
					elx.value = screen.height;
				}
				if ((elx=del("TDS2_Screen_width")) != null && typeof(screen.width)!=undefined) {
					elx.value = screen.width;
				}
				if ((elx=del("TDS2_Screen_pixelDepth")) != null && typeof(screen.pixelDepth)!=undefined) {
					elx.value = screen.pixelDepth;
				}
				if ((elx=del("TDS2_Navigator_UA")) != null && typeof(navigator.userAgent)!=undefined ) {
					elx.value = navigator.userAgent;
				}
				var tzoF = del("TDS2_TimezoneOffset");
				if (tzoF != null) {
					tzoF.value = new Date().getTimezoneOffset();
				}
		}

		function localpost(components) {
			var details1="";
			for (var index in components) 
			{
				var obj = components[index]
				details1 += obj.key + "=" + String(obj.value).substr(0, 512)+ "\n";
			}
			sv('fpversion', Fingerprint2.VERSION);
			sv('devicedata',details1);
			details=details1;
			var fpe = new Date();
			var fpt=(fpe.getTime()-st.getTime());
			console.log("fp: "+fpt);
			var elxt=del('TM1');
			if (elxt!=null) elxt.value=elxt.value+";fp:"+fpt;
			elxt=del('TM2');
			if (elxt!=null) elxt.value=elxt.value+";fp:"+fpt;
			notifyCollect();
			notify3DS();
		}

		var fpo = {	fonts : { extendedJsFonts : true}, audio : { timeout : 199 }};
		setTimeout( function() { Fingerprint2.get(fpo, function(components) { localpost(components);})}, 60 );
		var stb = new Date();
		populateBrwData();
		var brde = new Date();
		var brdt=(brde.getTime()-stb.getTime())+"/"+(brde.getTime()-st.getTime());
		console.log("brd: "+brdt);
		var elxt=del('TM1');
		if (elxt!=null) elxt.value=elxt.value+"brd:"+brdt;
		elxt=del('TM2');
		if (elxt!=null) elxt.value=elxt.value+"brd:"+brdt;

<% if (everCookieName != null && everCookieValue != null) 
	{
		String everCookieURINS=everCookieURI;
		if (everCookieURI!=null && everCookieURI.endsWith("/"))
		{
			everCookieURINS=everCookieURI.substring(0,everCookieURI.length()-1);
		}
	%><%-- evercookie last as everything else can potentially be intact --%>
		var evest = new Date();
		var o={ phpuri: '<%=everCookieURINS %>', asseturi : '<%=everCookieURINS %>', history: false, java: false, silverlight: false, lso: false, hsts: true, pngCookieName: false, cacheCookieName: false, tests: 2,  hsts: false, hsts_domains: []};
		var ec = new evercookie(o);
		var cnt=0;
		ec.get("<%=everCookieName %>", function(value) { setEC(value); }); 
		
		function setEC(ecValue)
		{
			cnt++;
			if (ecValue==null || ecValue===undefined || ecValue=='undefined' || ecValue.length%3!=0)
			{	
				if (cnt<2)
				{
				ec.set("<%=everCookieName %>", "<%=everCookieValue %>");
				ec.get("<%=everCookieName %>", function(value) { setEC(value); }); 
				}
			}
			var eve = new Date();
			var evt=(eve.getTime()-evest.getTime())+"/"+(eve.getTime()-st.getTime());
			console.log("ec: "+evt);
			if (!(ecValue==null || ecValue===undefined || ecValue=='undefined'))
			{	
				var elxt=del('TM2');
				if (elxt!=null) elxt.value=elxt.value+";ec:"+evt+";";
				elxt=del('TM1');
				if (elxt!=null) elxt.value=elxt.value+";ec:"+evt+";";
				del('EC1').value="<%=everCookieName %>="+ecValue;
				del('EC2').value="<%=everCookieName %>="+ecValue;
				eco=ecValue;
				notifyCollect(); // late posting may reach in time occasionally
				notify3DS();
			}
		}
<%	} %>
	</script>
</body>
</html>