<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isErrorPage = "true"%>
<html>
<head>
    <title>Application general error</title>
</head>
<body>
<div align="center">
    Sorry, application encountered system failure, if this repeats contact administrator<br/>
    <%
    String errId="E"+System.currentTimeMillis();
    %>
    Error reference <%=errId %>
    <br/><br/>
<%
try {
	if (pageContext != null && pageContext.getErrorData()!=null)
	{
		org.slf4j.Logger log=org.slf4j.LoggerFactory.getLogger("com.modirum.ds.web.Errors");
		log.error("DS error id "+errId+" uri "+pageContext.getErrorData().getRequestURI(), pageContext.getErrorData().getThrowable());
	}
	

%> 

<%  

} catch (Exception e) {}

%>  
</div>
</body>
</html>
