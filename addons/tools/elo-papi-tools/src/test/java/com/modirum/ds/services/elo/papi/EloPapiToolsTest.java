package com.modirum.ds.services.elo.papi;

import com.modirum.ds.services.elo.auth.EloAuthenticationService;
import com.modirum.ds.services.elo.papi.test.support.RunWithDB;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.Console;

@RunWithDB
public class EloPapiToolsTest {
    @Mocked
    private Console consoleMock;

    private MockUp<System> mockSystem;

    @BeforeEach
    public void beforeEach() {
        if (mockSystem == null) {
            mockSystem = new MockUp<System>() {
                @Mock
                public Console console() {
                    return consoleMock;
                }
            };
        }
    }

    @Test
    public void testCreateUser_Success(
            @Injectable EloAuthenticationService eloAuthenticationService,
            @Tested EloPapiTools eloPapiTools) {

        Integer eloPsId = 3;
        String username = "elo-ds-user";
        String password = "password";
        String cpfId = "253.298.192-02";
        new Expectations() {{

            consoleMock.readLine("Please enter Elo paymentSystemId: ");
            result = eloPsId.toString();

            consoleMock.readLine("Please enter Elo username: ");
            result = username;

            consoleMock.readPassword("Please enter Elo password: ");
            result = password.toCharArray();

            consoleMock.readLine("Please enter Elo CPF id (###.###.###-##): ");
            result = cpfId;

            eloAuthenticationService.createUser(withEqual(eloPsId),
                                                withEqual(username),
                                                withEqual(password),
                                                withEqual(cpfId));
            result = true;
        }};

        eloPapiTools.start();

        new Verifications() {{

            eloAuthenticationService.createUser(anyInt, anyString, anyString, anyString);

            consoleMock.readLine("Do you want to try again (Y)?");
            times = 0;
        }};
    }

    @Test
    public void testCreateUser_FailAndRetryOnce(
            @Injectable EloAuthenticationService eloAuthenticationService,
            @Tested EloPapiTools eloPapiTools) {

        Integer eloPsId = 3;
        String username = "elo-ds-user";
        String password = "password";
        String cpfId = "253.298.192-02";
        new Expectations() {{

            consoleMock.readLine("Please enter Elo paymentSystemId: ");
            result = eloPsId.toString();

            consoleMock.readLine("Please enter Elo username: ");
            result = username;

            consoleMock.readPassword("Please enter Elo password: ");
            result = password.toCharArray();

            consoleMock.readLine("Please enter Elo CPF id (###.###.###-##): ");
            result = cpfId;

            eloAuthenticationService.createUser(withEqual(eloPsId),
                                                withEqual(username),
                                                withEqual(password),
                                                withEqual(cpfId));
            returns(false, true);

            consoleMock.readLine("Do you want to try again (Y)?");
            result = "y";
        }};

        eloPapiTools.start();

        new Verifications() {{

            eloAuthenticationService.createUser(anyInt, anyString, anyString, anyString);
            times = 2;

            consoleMock.readLine("Do you want to try again (Y)?");
        }};
    }

    @Test
    public void testCreateUser_InvalidPaymentSystemId(
            @Injectable EloAuthenticationService eloAuthenticationService,
            @Tested EloPapiTools eloPapiTools) {

        Integer eloPsId = 3;
        String username = "elo-ds-user";
        String password = "password";
        String cpfId = "253.298.192-02";
        new Expectations() {{
            consoleMock.readLine("Please enter Elo paymentSystemId: ");
            returns("A", "1.2", eloPsId.toString());

            consoleMock.readLine("Please enter Elo username: ");
            result = username;

            consoleMock.readPassword("Please enter Elo password: ");
            result = password.toCharArray();

            consoleMock.readLine("Please enter Elo CPF id (###.###.###-##): ");
            result = cpfId;

            eloAuthenticationService.createUser(withEqual(eloPsId),
                                                withEqual(username),
                                                withEqual(password),
                                                withEqual(cpfId));
            result = true;
        }};

        eloPapiTools.start();

        new Verifications() {{
            consoleMock.readLine("Please enter Elo paymentSystemId: "); times = 3;
            eloAuthenticationService.createUser(anyInt, anyString, anyString, anyString);
        }};
    }

    @Test
    public void testCreateUser_InvalidCpfIdAndRetryOnce(
            @Injectable EloAuthenticationService eloAuthenticationService,
            @Tested EloPapiTools eloPapiTools) {

        Integer eloPsId = 3;
        String username = "elo-ds-user";
        String password = "password";
        String cpfId = "253.298.192-02";
        new Expectations() {{
            consoleMock.readLine("Please enter Elo paymentSystemId: ");
            result = eloPsId.toString();

            consoleMock.readLine("Please enter Elo username: ");
            result = username;

            consoleMock.readPassword("Please enter Elo password: ");
            result = password.toCharArray();

            consoleMock.readLine("Please enter Elo CPF id (###.###.###-##): ");
            returns("cpfId", cpfId);

            eloAuthenticationService.createUser(withEqual(eloPsId),
                                                withEqual(username),
                                                withEqual(password),
                                                withEqual(cpfId));
            result = true;
        }};

        eloPapiTools.start();

        new Verifications() {{
            consoleMock.readLine("Please enter Elo CPF id (###.###.###-##): ");
            times = 2;
        }};
    }
}
