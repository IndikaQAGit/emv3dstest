package com.modirum.ds.services.elo.auth;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.modirum.ds.db.model.User;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;
import com.modirum.ds.services.PaymentSystemConfigService;
import com.modirum.ds.services.elo.EloHttpService;
import com.modirum.ds.services.server.PaymentSystemSettingsCacheService;
import elo.api.bcrypter.EloBcrypter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

/**
 * This service is responsible in providing access to ELO API. It integrates with ELO by creating an API user to be used
 * in authentication and getting access token from the ELO Authentication Server. Securing access token involves the
 * login and refreshAccessToken flows.
 */
@Lazy
@Service
public class EloAuthenticationService {
    //since the actual expiration of accessToken is 1 hour
    //use 55 minutes as limit with 5 minute buffer to be safe
    //and prevents the last minute issue by refreshing earlier
    static final int accessTokenMaxLifeMinutes = 55;
    private static final Logger log = LoggerFactory.getLogger(EloAuthenticationService.class);
    private static final ObjectMapper jsonObjectMapper = new ObjectMapper();
    @Autowired
    private PaymentSystemSettingsCacheService psSettingsCacheService;

    @Autowired
    private EloHttpService eloHttpService;

    @Autowired
    private PaymentSystemConfigService paymentSystemConfigService;

    private EloAuthToken eloAuthToken;

    /**
     * Calls createUser API of ELO Authentication Server.
     *
     * @param eloPsId
     */
    public boolean createUser(Integer eloPsId, String username, String password, String cpfId) {
        String bcryptPassword = EloBcrypter.encryptPassword(username, password);
        String queryRequest = readGraphQLTemplate("createUser.graphql");
        queryRequest = queryRequest
                .replace("${username}", username)
                .replace("${bcryptPassword}", bcryptPassword)
                .replace("${cpf}", cpfId);

        String authEndpointUrl = getSetting(eloPsId, PsSetting.ELO_PAPI_AUTH_URL);
        try {
            HttpURLConnection connection = eloHttpService.getPostConnection(eloPsId, authEndpointUrl);

            String clientId = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            connection.setRequestProperty("client_id", clientId);

            String clientSecret = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            connection.setRequestProperty("Authorization", buildAuthorizationHeader(clientId, clientSecret));
            connection.setRequestProperty("acceptType", "application/json");

            EloHttpService.Response response = eloHttpService.sendHttpRequest(queryRequest, connection);
            log.info("Received this ELO createUser response={}", response.getBody());

            if (!response.isSuccessfulResponse()) {
                log.info("Detected status error in Elo response!");
                return false;
            }

            JsonNode responseJson = jsonObjectMapper.readTree(response.getBody());
            if (responseJson.has("errors")) {
                log.info("Detected response body error in Elo response!");
                return false;
            }

            storeEloLoginCredentials(eloPsId, username, password);

            return true;
        } catch (Exception e) {
            log.error("Error creating ELO API User.", e);
            return false;
        }
    }

    /**
     * Stores the login credentials to DS database.
     *
     * @param eloPsId
     * @param username
     */
    private void storeEloLoginCredentials(Integer eloPsId, String username, String password) {
        log.info("Storing ELO API user credentials in DS Database...");
        User user = new User();
        user.setLoginname("system");
        user.setId(0L);
        PaymentSystemConfig usernamePsConfig = PaymentSystemConfig.builder()
                                                                  .dsId(0)
                                                                  .paymentSystemId(eloPsId)
                                                                  .key(PsSetting.ELO_PAPI_USERNAME.getKey())
                                                                  .value(username)
                                                                  .build();
        paymentSystemConfigService.saveOrUpdatePaymentSystemConfig(usernamePsConfig, user);

        PaymentSystemConfig passwordPsConfig = PaymentSystemConfig.builder()
                                                                  .dsId(0)
                                                                  .paymentSystemId(eloPsId)
                                                                  .key(PsSetting.ELO_PAPI_PASSWORD.getKey())
                                                                  .value(password)
                                                                  .build();
        paymentSystemConfigService.saveOrUpdatePaymentSystemConfig(passwordPsConfig, user);
        log.info("Storing credentials successful!");
    }

    /**
     * Returns the accessToken for any authorized access to ELO API Services
     *
     * @param eloPsId
     * @return
     */
    public String getAccessToken(Integer eloPsId) throws EloAuthenticationFailureException {
        if (eloAuthToken != null &&
            eloAuthToken.getTimestamp().plus(accessTokenMaxLifeMinutes, ChronoUnit.MINUTES).isAfter(LocalDateTime.now())) {
            return eloAuthToken.getAccessToken();
        }
        return getNewAccessToken(eloPsId);
    }

    /**
     * Synchronized call in getting new accessToken from ELO API to prevent
     * multiple threads doing the same thing at the same time.
     *
     * @param eloPsId
     * @return
     * @throws EloAuthenticationFailureException
     */
    private synchronized String getNewAccessToken(Integer eloPsId) throws EloAuthenticationFailureException {
        if (eloAuthToken != null &&
            eloAuthToken.getTimestamp().plus(accessTokenMaxLifeMinutes, ChronoUnit.MINUTES).isAfter(LocalDateTime.now())) {
            //another thread has just logged in before this thread, immediately return
            return eloAuthToken.getAccessToken();
        }
        if (eloAuthToken != null && refreshAccessToken(eloPsId)) {
            return eloAuthToken.getAccessToken();
        }
        //either no valid accessToken is available or refreshAccessToken failed, relogin
        if (login(eloPsId)) {
            return eloAuthToken.getAccessToken();
        }
        //login failed, no accessToken available
        return null;
    }

    private boolean refreshAccessToken(Integer eloPsId) {
        String queryRequest = readGraphQLTemplate("refreshToken.graphql");
        queryRequest = queryRequest
                .replace("${refreshToken}", eloAuthToken.getRefreshToken());

        String authEndpointUrl = getSetting(eloPsId, PsSetting.ELO_PAPI_AUTH_URL);

        try {
            HttpURLConnection connection = eloHttpService.getPostConnection(eloPsId, authEndpointUrl);

            String clientId = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            connection.setRequestProperty("client_id", clientId);

            String clientSecret = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            connection.setRequestProperty("Authorization", buildAuthorizationHeader(clientId, clientSecret));
            connection.setRequestProperty("acceptType", "application/json");

            EloHttpService.Response response = eloHttpService.sendHttpRequest(queryRequest, connection);
            log.info("Received this ELO refreshAccessToken response={}", response.getBody());
            if (!response.isSuccessfulResponse()) {
                //TODO handle refreshAccessToken failure
                return false;
            }

            JsonNode responseJson = jsonObjectMapper.readTree(response.getBody());
            if (responseJson.has("errors")) {
                //TODO handle refreshAccessToken failure
                return false;
            }
            EloAuthToken eloAuthToken = jsonObjectMapper.readValue(responseJson.at("/data/refreshAccessToken/oauthToken").toPrettyString(), EloAuthToken.class);
            eloAuthToken.setTimestamp(LocalDateTime.now());
            this.eloAuthToken = eloAuthToken;
            return true;
        } catch (Exception e) {
            log.error("Error calling refreshAccessToken API of Elo Authentication Server.", e);
            return false;
        }
    }

    /**
     * Logins to ELO Authentication Server to get accessToken and refreshToken
     *
     * @param eloPsId
     */
    private boolean login(Integer eloPsId) throws EloAuthenticationFailureException {
        String loginSalt = getLoginSalt(eloPsId);
        if (loginSalt == null) {
            //TODO handle loginSalt failure
            return false;
        }

        String username = getSetting(eloPsId, PsSetting.ELO_PAPI_USERNAME);
        String bcryptPassword = EloBcrypter.encryptPassword(username, getSetting(eloPsId, PsSetting.ELO_PAPI_PASSWORD));
        String challenge = EloBcrypter.createLoginChallenge(bcryptPassword, loginSalt);

        String queryRequest = readGraphQLTemplate("login.graphql");
        queryRequest = queryRequest
                .replace("${username}", username)
                .replace("${challenge}", challenge);

        String authEndpointUrl = getSetting(eloPsId, PsSetting.ELO_PAPI_AUTH_URL);

        try {
            HttpURLConnection connection = eloHttpService.getPostConnection(eloPsId, authEndpointUrl);

            String clientId = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            connection.setRequestProperty("client_id", clientId);

            String clientSecret = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            connection.setRequestProperty("Authorization", buildAuthorizationHeader(clientId, clientSecret));
            connection.setRequestProperty("acceptType", "application/json");

            EloHttpService.Response response = eloHttpService.sendHttpRequest(queryRequest, connection);
            log.info("Received this ELO login response={}", response.getBody());
            if (!response.isSuccessfulResponse()) {
                //TODO handle login failure
                return false;
            }

            JsonNode responseJson = jsonObjectMapper.readTree(response.getBody());
            if (responseJson.has("errors")) {
                //TODO handle login failure
                return false;
            }
            EloAuthToken eloAuthToken = jsonObjectMapper.readValue(responseJson.at("/data/login/oauthToken").toPrettyString(), EloAuthToken.class);
            eloAuthToken.setTimestamp(LocalDateTime.now());
            this.eloAuthToken = eloAuthToken;
            return true;
        } catch (Exception e) {
            log.error("Error calling login API of Elo Authentication Server.", e);
            throw new EloAuthenticationFailureException("Encounter error in ELO login API.", e);
        }
    }

    /**
     * Gets one-time loginSalt from ELO Authentication Server and used in login challenge.
     *
     * @param eloPsId
     * @return
     * @throws EloAuthenticationFailureException
     */
    private String getLoginSalt(Integer eloPsId) throws EloAuthenticationFailureException {
        String username = getSetting(eloPsId, PsSetting.ELO_PAPI_USERNAME);

        String queryRequest = readGraphQLTemplate("loginSalt.graphql");
        queryRequest = queryRequest
                .replace("${username}", username);

        String authEndpointUrl = getSetting(eloPsId, PsSetting.ELO_PAPI_AUTH_URL);

        try {
            HttpURLConnection connection = eloHttpService.getPostConnection(eloPsId, authEndpointUrl);

            String clientId = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
            connection.setRequestProperty("client_id", clientId);

            String clientSecret = getSetting(eloPsId, PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET);
            connection.setRequestProperty("Authorization", buildAuthorizationHeader(clientId, clientSecret));
            connection.setRequestProperty("acceptType", "application/json");

            EloHttpService.Response response = eloHttpService.sendHttpRequest(queryRequest, connection);
            log.info(" Received this ELO loginSalt response={}", response.getBody());
            if (!response.isSuccessfulResponse()) {
                //TODO handle loginSalt failure
                return null;
            }

            JsonNode loginSaltNode = jsonObjectMapper.readTree(response.getBody());
            if (loginSaltNode.has("errors")) {
                //TODO handle loginSalt failure
                return null;
            }
            return loginSaltNode.at("/data/createLoginSalt/salt").asText();

        } catch (Exception e) {
            log.error("Error calling loginSalt API of Elo Authentication Server.", e);
            throw new EloAuthenticationFailureException("Encounter error in ELO loginSalt API.", e);
        }
    }

    /**
     * Builds the Authorization header needed for API that doesn't require accessToken
     *
     * @param clientId     Modirum DS clientId registered in Elo Authentication Server
     * @param clientSecret Modirum DS clientSecret registered in Elo Authentication Server
     * @return
     */
    private String buildAuthorizationHeader(String clientId, String clientSecret) {
        return "Basic " +
               Base64.getEncoder()
                     .encodeToString((clientId + ":" + clientSecret).getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Shorter convenient retrieval of ELO specific setting.
     *
     * @param psId
     * @param psSetting
     * @return
     */
    private String getSetting(Integer psId, PsSetting psSetting) {
        return psSettingsCacheService.getPaymentSystemSetting(psId, psSetting);
    }

    /**
     * Reads the specific graphql template relative to the classpath within the jar.
     *
     * @param graphQLFile
     * @return
     */
    private String readGraphQLTemplate(String graphQLFile) {
        try {
            String graphqlTemplate = IOUtils.toString(Thread.currentThread()
                                                            .getContextClassLoader()
                                                            .getResourceAsStream(graphQLFile), StandardCharsets.UTF_8);
            return "{ \"query\": \"" + StringEscapeUtils.escapeJson(graphqlTemplate) + "\" }";
        } catch (IOException e) {
            log.error("Error reading graphql template={} within the elo-papi jar!", graphQLFile);
            log.error("Details: ", e);
            throw new RuntimeException(e);
        }
    }

}
