package com.modirum.ds.services.elo;

import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceService;
import com.modirum.ds.services.server.PaymentSystemSettingsCacheService;
import com.modirum.ds.utils.http.SSLSocketHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.NoSuchAlgorithmException;
import java.security.KeyManagementException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.util.Optional;

/**
 * This provides the same http connection to ELO API endpoint with apparently same basic http parameters
 * for all PAPI integration.
 */
@Lazy
@Service
public class EloHttpService {
    private final static Logger log = LoggerFactory.getLogger(EloHttpService.class);
    private static final int DEFAULT_CONNECT_TIMEOUT = 5000;
    private static final int DEFAULT_READ_TIMEOUT = 10000;
    @Autowired
    private PaymentSystemSettingsCacheService psSettingsCacheService;
    @Autowired
    private HsmDeviceService hsmDeviceService;
    @Autowired
    private KeyService keyService;
    @Autowired
    private CryptoService cryptoService;

    SSLSocketFactory socketFactory;

    /**
     * Common Http Post connection to ELO API
     *
     * @param paymentSystemId
     * @param endpoint
     * @return
     * @throws Exception
     */
    public HttpURLConnection getPostConnection(Integer paymentSystemId,
            String endpoint) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        Optional<Integer> connectTimeout =
                psSettingsCacheService.getIntegerSetting(paymentSystemId, PsSetting.ELO_PAPI_CONNECT_TIMEOUT_MILLIS);
        con.setConnectTimeout(connectTimeout.orElse(DEFAULT_CONNECT_TIMEOUT));

        Optional<Integer> readTimeout =
                psSettingsCacheService.getIntegerSetting(paymentSystemId, PsSetting.ELO_PAPI_READ_TIMEOUT_MILLIS);
        con.setReadTimeout(readTimeout.orElse(DEFAULT_READ_TIMEOUT));

        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        String client_id = psSettingsCacheService.getPaymentSystemSetting(paymentSystemId,
                                                                          PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID);
        con.setRequestProperty("client_id", client_id);

        con.setDoOutput(true);
        con.setDoInput(true);
        con.setUseCaches(false);

        // handle HTTPS connection
        if (con instanceof HttpsURLConnection) {
            HttpsURLConnection httpsConn = (HttpsURLConnection) con;

            // get key data ID from payment system setting
            Optional<Long> clientCertId = psSettingsCacheService.getLongSetting(paymentSystemId,
                    PsSetting.ELO_PAPI_TLS_CLIENT_CERT_ID);
            KeyData clientKeyData = null;
            if (clientCertId.isPresent()) {
                clientKeyData = keyService.getPrivateKeyAndCertChainById(clientCertId.get());
            }

            // if client key data are defined, set up for TLS authentication
            if (clientKeyData != null) {
                setupTLS(clientKeyData);
                httpsConn.setSSLSocketFactory(socketFactory);
            } else {
                log.debug("Setup for TLS authentication skipped");
                httpsConn.setSSLSocketFactory(getEmptySslSocketFactoryStub());
            }

            boolean isSkipServerCertHostnameVerify =
                    psSettingsCacheService.isTrueSetting(paymentSystemId, PsSetting.ELO_PAPI_SKIP_TLS_HOSTNAME_VERIFY);
            if (isSkipServerCertHostnameVerify) {
                httpsConn.setHostnameVerifier((hostname, session) -> true);
            }
        }

        return con;
    }

    private void setupTLS(KeyData clientKeyData) {
        log.debug("Setting up for TLS authentication..");

        try {
            // extract information from client key data
            byte[] privateKeyBytes  = cryptoService.unwrapPrivateKey(clientKeyData);
            HSMDevice hsmDevice = hsmDeviceService.getInitializedHSMDevice(clientKeyData.getHsmDeviceId());
            PrivateKey clientKey = hsmDevice.toRSAprivateKey(privateKeyBytes);
            String certChainPem = KeyService.parsePEMChain(clientKeyData.getKeyData());
            X509Certificate[] clientCert = KeyService.parseX509Certificates(certChainPem.getBytes(StandardCharsets.ISO_8859_1));

            // load key manager for authentication as a client
            // proceed only if client key and certificate are defined
            // otherwise, leave client key and cert out of context
            KeyManager[] keyManagers = null;
            String keyManagerFactoryType = KeyManagerFactory.getDefaultAlgorithm();
            if (clientKey != null && clientCert != null) {
                KeyStore clientKeyStore = KeyStore.getInstance("JKS");
                clientKeyStore.load(null);
                clientKeyStore.setKeyEntry(clientKeyData.getKeyAlias(), clientKey, "password".toCharArray(), clientCert);
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(keyManagerFactoryType);
                kmf.init(clientKeyStore, "password".toCharArray());

                KeyManager[] defaultKeyManagers = kmf.getKeyManagers();
                if (defaultKeyManagers.length <= 0) {
                    throw new RuntimeException("No default manager");
                }

                keyManagers = new KeyManager[]{new SSLSocketHelper.EnchX509KeyManager(defaultKeyManagers[0])};
                log.debug("Client key/certificate specified, key manager loaded");
            } else {
                log.debug("Client key/certificate not specified and not to be used for DS connection");
            }

            // load trust manager for authentication of server
            KeyStore trustStore = keyService.getAllTrustedRootsAsKeyStore();
            TrustManager[] trustManagers = null;
            boolean allowUntrustedCertificates = false;
            if (trustStore != null) {
                log.debug("Not allowing untrusted certificates, using defined trust store");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(trustStore);
                trustManagers = tmf.getTrustManagers();
            } else if (allowUntrustedCertificates) {
                log.debug("Allowing untrusted certificates");
                trustManagers = new TrustManager[]{new SSLSocketHelper.TrustingX509TrustManager()};
            }

            // initialize SSL context and create socket factory
            // use TLSv1.2 as default protocol, may be changed if required
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(keyManagers, trustManagers, null);
            socketFactory = context.getSocketFactory();
            log.debug("Setup done for TLS authentication");
        } catch (KeyStoreException ex) {
            throw new RuntimeException("Invalid keystore - ", ex);
        } catch (IOException ex) {
            throw new RuntimeException("IO: invalid keystore - ", ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("No algorithm - ", ex);
        } catch (CertificateException ex) {
            throw new RuntimeException("Invalid cert- ", ex);
        } catch (KeyManagementException ex) {
            throw new RuntimeException("KeyManagement: invalid cert - ", ex);
        } catch (UnrecoverableKeyException ex) {
            throw new RuntimeException("Unrecoverable key / invalid keystore - ", ex);
        } catch (Exception ex) {
            throw new RuntimeException("Unexpected exception initialization ", ex);
        }
    }

    public Response sendHttpRequest(String request, HttpURLConnection con) throws Exception {
        try (OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream())) {
            out.write(request);
            out.flush();
        }

        int httpStatusCode = con.getResponseCode();
        log.debug("Elo PAPI service HTTP response status code: {}", httpStatusCode);

        String responseBody;
        if (httpStatusCode == HttpURLConnection.HTTP_OK || httpStatusCode == HttpURLConnection.HTTP_CREATED) {
            try (InputStream is = con.getInputStream()) {
                responseBody = IOUtils.toString(is, StandardCharsets.UTF_8);
            }
        } else {
            try (InputStream es = con.getErrorStream()) {
                responseBody = IOUtils.toString(es, StandardCharsets.UTF_8);
            }
        }

        return new Response(httpStatusCode, responseBody);
    }

    // temporary stub. Enables the use of https:// URL, but has no truststore or client certs.
    private static SSLSocketFactory getEmptySslSocketFactoryStub() {
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, null, null);
            return ctx.getSocketFactory();
        } catch (GeneralSecurityException e) {
            log.error("Error instantiating stub SSL Socket Factory.", e);
            return null;
        }
    }

    /**
     * Convenience class to encapsulate Elo server's HTTP response.
     */
    @Data
    @AllArgsConstructor
    @Builder
    public static class Response {
        private final int statusCode;
        private final String body;

        public boolean isSuccessfulResponse() {
            return statusCode == HttpURLConnection.HTTP_OK || statusCode == HttpURLConnection.HTTP_CREATED;
        }
    }
}
