package com.modirum.ds.services.elo.papi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.enums.FieldName;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.services.JsonMessage;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.elo.EloHttpService;
import com.modirum.ds.services.elo.auth.EloAuthenticationService;
import com.modirum.ds.services.server.PaymentSystemSettingsCacheService;
import com.modirum.ds.utils.Misc;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.json.JsonArrayBuilder;
import java.net.HttpURLConnection;
import java.util.Date;

/**
 * Elo PAPI Detokenization service. Used during AReq phase. AReq is received by DS, sent to Elo service for
 * acctNumber/expiry date detokenization. Then detokenized data is substituted in the original AReq and sent to ACS.
 * <p>
 * Service is initialized on demand based on Payment System settings.
 */
@Lazy
@Service
public class EloPapiDetokenizationService {

    private static final Logger log = LoggerFactory.getLogger(EloPapiDetokenizationService.class);
    private static final ObjectMapper jsonObjectMapper = new ObjectMapper();

    @Autowired
    private PaymentSystemSettingsCacheService psSettingsCacheService;

    @Autowired
    private EloHttpService eloHttpService;

    @Autowired
    private EloAuthenticationService eloAuthenticationService;

    /**
     * Sends AReq to Elo PAPI service. Returns detokenized data to be injected into original AReq.
     *
     * @param tdsRecordId     TDS record id to attach logs to.
     * @param dsHostUrl       DS host IP/URL address for logging.
     * @param eloPsId Payment System id of the EMV 3DS transaction being processed.
     * @param aReqJsonObj     AReq object incoming from 3DS Server.
     * @return Detokenized data from Elo PAPI
     */
    public DetokenizedData sendAreq(Long tdsRecordId,
            String dsHostUrl,
            Integer eloPsId,
            JsonMessage aReqJsonObj) {

        String areqMessages_EndpointUrl = psSettingsCacheService.getPaymentSystemSetting(eloPsId,
                                                                                         PsSetting.ELO_PAPI_DETOKEN_URL);

        try {
            HttpURLConnection con = eloHttpService.getPostConnection(eloPsId, areqMessages_EndpointUrl);
            String accessToken = eloAuthenticationService.getAccessToken(eloPsId);
            if (StringUtils.isEmpty(accessToken)) {
                //TODO how to handle if accessToken is not available because of login failure or ELO server timeout
                return null;
            }
            con.setRequestProperty("access_token", accessToken);

            JsonNode aReqJsonObject = aReqJsonObj.getRootNode();
            String requestBody = aReqJsonObject.toString();
            logEloPapiRequest(tdsRecordId, requestBody, dsHostUrl, con.getURL().toString());

            log.info("Sending AReq to Elo PAPI Detokenization service: {}", areqMessages_EndpointUrl);
            EloHttpService.Response response = eloHttpService.sendHttpRequest(requestBody, con);

            logEloPapiResponse(tdsRecordId, response.getBody(), con.getURL().toString(), dsHostUrl);

            if (!response.isSuccessfulResponse()) {
                return null;
            }

            JsonNode areqFromElo = jsonObjectMapper.readTree(response.getBody());
            //invalid accessToken encountered here
            if (areqFromElo.has("errors") && "401.001".equals(areqFromElo.at("/errors/0/code").asText())) {
                //TODO handle invalid accessToken
                log.error("Invalid accessToken error after sending AReq to Elo PAPI Detokenization service.");
                return null;
            }
            DetokenizedData data = new DetokenizedData();
            data.setAcctNumber(areqFromElo.get(FieldName.acctNumber).asText());
            data.setCardExpiryDate(areqFromElo.get(FieldName.cardExpiryDate).asText());
            // it is not clear from elo specs which format the messageExtension will use. To be checked from integration
            //            data.setMessageExtension(areqFromElo.get(FieldName.messageExtension).asText());
            return data;
        } catch (Exception e) {
            log.error("Error sending AReq to Elo PAPI Detokenization service.", e);
            return null;
        }

    }

    private void logEloPapiRequest(Long tdsRecordId, String requestBody, String requestSrcUrl, String requestDestUrl) throws Exception {
        TDSMessageData messageData = prepareTdsMessage(tdsRecordId, "EloPapiReq", requestSrcUrl, requestDestUrl);
        messageData.setContents(requestBody);
        ServiceLocator.getInstance().getPersistenceService().saveOrUpdate(messageData);
    }

    private void logEloPapiResponse(Long tdsRecordId, String responseBody, String requestSrcUrl, String requestDestUrl) throws Exception {
        TDSMessageData messageData = prepareTdsMessage(tdsRecordId, "EloPapiRes", requestSrcUrl, requestDestUrl);
        messageData.setContents(responseBody);
        ServiceLocator.getInstance().getPersistenceService().saveOrUpdate(messageData);
    }

    private TDSMessageData prepareTdsMessage(Long tdsRecordId, String messageType, String requestSrcUrl, String requestDestUrl) {
        TDSMessageData messageData = new TDSMessageData();
        messageData.setTdsrId(tdsRecordId);
        messageData.setMessageType(messageType);
        messageData.setMessageVersion("1"); // a stub. ds_tdsmessage doesn't yet allow null values

        messageData.setSourceIP(Misc.cut(requestSrcUrl, 128));
        messageData.setDestIP(Misc.cut(requestDestUrl, 128));
        messageData.setMessageDate(new Date());
        return messageData;
    }

    @Data
    public static class DetokenizedData {
        private String acctNumber;
        private String cardExpiryDate;
        private JsonArrayBuilder messageExtension;
    }
}
