// Specification Values which will be used for converting UI values
const SPEC_VALUE = {
    "deviceChannel" : {
        "APP": "01",
        "BRW": "02",
        "3RI": "03"
    },
    "messageCategory" : {
        "PA":   "01",
        "NPA":  "02"
    }
};

// Areq Fields with default values
const AREQ_FIELDS = {};
AREQ_FIELDS['acceptLanguage'] = "[\"en\"]";
AREQ_FIELDS['acctInfo'] = "{\"chAccAgeInd\" : \"05\", \"chAccChange\" : \"20180808\", \"chAccChangeInd\" : \"04\", " +
    "\"chAccDate\" : \"20180808\", \"chAccPwChange\" : \"20180808\", \"chAccPwChangeInd\" : \"05\", " +
    "\"nbPurchaseAccount\" : \"24\", \"provisionAttemptsDay\" : \"1\", \"txnActivityDay\" : \"2\", " +
    "\"txnActivityYear\" : \"100\", \"paymentAccAge\" : \"20180808\", \"paymentAccInd\" : \"05\", " +
    "\"shipAddressUsage\" : \"20180808\", \"shipAddressUsageInd\" : \"04\", \"shipNameIndicator\" : \"01\", " +
    "\"suspiciousAccActivity\" : \"01\"}";
AREQ_FIELDS['acctNumber'] = "\"4000000000000000\"";
AREQ_FIELDS['acctType'] = "\"02\"";
AREQ_FIELDS['acquirerBIN'] = "\"444444\"";
AREQ_FIELDS['acquirerCountryCode'] = "\"840\"";
AREQ_FIELDS['acquirerCountryCodeSource'] = "\"01\"";
AREQ_FIELDS['acquirerMerchantID'] = "\"000001\"";
AREQ_FIELDS['addrMatch'] = "\"Y\"";
AREQ_FIELDS['appIp'] = "\"216.58.197.110\"";
AREQ_FIELDS['browserAcceptHeader'] = "\"text/html, application/xhtml+xml, */*\"";
AREQ_FIELDS['browserIP'] = "\"216.58.197.110\"";
AREQ_FIELDS['browserJavaEnabled'] = true;
AREQ_FIELDS['browserLanguage'] = "\"en-GB\"";
AREQ_FIELDS['browserColorDepth'] = "\"24\"";
AREQ_FIELDS['browserScreenHeight'] = "\"1137\"";
AREQ_FIELDS['browserScreenWidth'] = "\"2021\"";
AREQ_FIELDS['browserTZ'] = "\"-180\"";
AREQ_FIELDS['browserUserAgent'] = "\"Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko\"";
AREQ_FIELDS['cardExpiryDate'] = "\"2404\"";
AREQ_FIELDS['deviceInfo'] = "\"eyJEViI6IjEuMCIsIkREIjp7IkMwMDEiOiJBbmRyb2lkIiwiQzAwMiI6IkhUQyBPbmVfTTgiLCJDM" +
    "DA0IjoiNS4wLjEiLCJDMDA1IjoiZW5fVVMiLCJDMDA2IjoiRWFzdGVybiBTdGFuZGFyZCBUaW1lIiwiQzAwNyI6IjA2Nzk3OTAzLWZ" +
    "iNjEtNDFlZC05NGMyLTRkMmI3NGUyN2QxOCIsIkMwMDkiOiJKb2huJ3MgQW5kcm9pZCBEZXZpY2UiLCJDMDEwIjoiMjE2LjU4LjE5N" +
    "y4xMTAifSwiRFBOQSI6eyJDMDExIjoiUkUwMyJ9LCJTVyI6WyJTVzAxIiwiU1cwNCJdfQ\"";
AREQ_FIELDS['deviceRenderOptions'] = "{\n" +
    "\"sdkAuthenticationType\": [\"01\"],\n" +
    "\"sdkInterface\": \"03\",\n" +
    "\"sdkUiType\": [\n" +
    "\"01\",\n" +
    "\"02\",\n" +
    "\"03\",\n" +
    "\"04\",\n" +
    "\"05\"] \n }";
AREQ_FIELDS['mcc'] = "\"5499\"";
AREQ_FIELDS['merchantCountryCode'] = "\"246\"";
AREQ_FIELDS['merchantName'] = "\"Modirum DS-3DS-Emulator\"";
AREQ_FIELDS['merchantRiskIndicator'] = "{ \"shipIndicator\" : \"07\" }";
AREQ_FIELDS['messageType'] = "\"AReq\"";
AREQ_FIELDS['purchaseAmount'] = "\"10600\"";
AREQ_FIELDS['purchaseCurrency'] = "\"840\"";
AREQ_FIELDS['purchaseExponent'] = "\"2\"";
AREQ_FIELDS['purchaseDate'] = "\"20170127075424\"";
AREQ_FIELDS['purchaseInstalData'] = "\"123\"";
AREQ_FIELDS['recurringExpiry'] = "\"20170127\"";
AREQ_FIELDS['recurringFrequency'] = "\"1234\"";
AREQ_FIELDS['sdkAppID'] = "\"087d1d12-15b2-11e6-a148-3e1d05defe78\"";
AREQ_FIELDS['sdkEphemPubKey'] = "{" +
    "\"kty\":\"EC\"," +
    "\"crv\":\"P-256\"," +
    "\"x\":\"D8nJZ3ao2siZNlsPi4M4wWvJCdGSbEEmksh_xneTd_I\"," +
    "\"y\":\"6l5UA7-a-ydYVTS9pnZXZDUSqlt22-zpAesJQm-X89U\"}";
AREQ_FIELDS['sdkMaxTimeout'] = "\"05\"";
AREQ_FIELDS['sdkReferenceNumber'] = "\"abc123ABC\"";
AREQ_FIELDS['threeDSCompInd'] = "\"U\"";
AREQ_FIELDS['threeDSRequestorAuthenticationInd'] = "\"03\"";
AREQ_FIELDS['threeDSRequestorChallengeInd'] = "\"01\"";
AREQ_FIELDS['threeDSRequestorID'] = "\"7979791\"";
AREQ_FIELDS['threeDSRequestorName'] = "\"Modirum DS-3DS-Emulator\"";
AREQ_FIELDS['threeDSRequestorURL'] = "\"http://localhost\"";
AREQ_FIELDS['threeDSServerRefNumber'] = "\"DS-3DS-EMULATOR-REF-NUMBER\"";
AREQ_FIELDS['threeDSServerOperatorID'] = "\"DS-3DS-EMULATOR-OPER-ID\"";
AREQ_FIELDS['threeDSServerTransID'] = "\"20000000000\"";
AREQ_FIELDS['threeDSServerURL'] = "\"http://localhost:8080/ds-3ds-emulator/3dsServerRreqStub.jsp\"";
AREQ_FIELDS['threeRIInd'] = "\"01\"";
AREQ_FIELDS['transType'] = "\"28\"";
// AREQ_FIELDS['messageExtension'] = "[{\n" +
//     "\"name\": \"testExtensionField\",\n" +
//     "\"id\": \"ID1\",\n" +
//     "\"criticalityIndicator\": \"true\",\n" +
//     "\"data\": {\"key\": \"value\"} }," +
//     "{\n" +
//     "\"name\": \"testExtensionNonCriticalField\",\n" +
//     "\"id\": \"ID3\",\n" +
//     "\"criticalityIndicator\": \"false\",\n" +
//     "\"data\": {\"key\": \"value\"} }]";

// added on message version = 2.2.0
AREQ_FIELDS['threeDSRequestorDecMaxTime'] = "\"10000\"";
AREQ_FIELDS['threeDSRequestorDecReqInd'] = "\"Y\"";
AREQ_FIELDS['whiteListStatus'] = "\"Y\"";
AREQ_FIELDS['whiteListStatusSource'] = "\"01\"";
AREQ_FIELDS['browserJavascriptEnabled'] = true;
AREQ_FIELDS['sdkType'] = "\"01\"";


//added on message version = 2.3.0
AREQ_FIELDS['deviceBindingStatus'] = "\"Y\"";
AREQ_FIELDS['deviceBindingStatusSource'] = "\"01\"";
AREQ_FIELDS['trustListStatus'] = "\"Y\"";
AREQ_FIELDS['trustListStatusSource'] = "\"01\"";

//added on message version =2.3.0

// JSON Fields to be populated for PA App flow 2.1.0
const AREQ_PA_APP_210 = [
    "sdkMaxTimeout",
    "threeDSRequestorAuthenticationInd",
    "acctNumber",
    "cardExpiryDate",
    "acquirerBIN",
    "acquirerMerchantID",
    "deviceInfo",
    "deviceRenderOptions",
    "sdkEncData",
    "sdkAppID",
    "sdkEphemPubKey",
    "sdkReferenceNumber",
    "sdkTransID",
    "deviceChannel",
    "mcc",
    "merchantCountryCode",
    "merchantName",
    "messageCategory",
    "messageType",
    "messageVersion",
    "purchaseAmount",
    "purchaseCurrency",
    "purchaseExponent",
    "purchaseDate",
    "purchaseInstalData",
    "recurringExpiry",
    "recurringFrequency",
    "threeDSRequestorID",
    "threeDSRequestorName",
    "threeDSRequestorURL",
    "threeDSServerRefNumber",
    "threeDSServerOperatorID",
    "threeDSServerTransID",
    "threeDSServerURL"
];

// JSON Fields to be populated for PA Browser flow 2.1.0
const AREQ_PA_BRW_210 = [
    "acctInfo",
    "acctNumber",
    "cardExpiryDate",
    "acctType",
    "acquirerBIN",
    "acquirerMerchantID",
    "addrMatch",
    "browserAcceptHeader",
    "browserIP",
    "browserJavaEnabled",
    "browserLanguage",
    "browserColorDepth",
    "browserScreenHeight",
    "browserScreenWidth",
    "browserTZ",
    "browserUserAgent",
    "deviceChannel",
    "mcc",
    "merchantCountryCode",
    "merchantName",
    "merchantRiskIndicator",
    "messageCategory",
    "messageType",
    "messageVersion",
    "notificationURL",
    "purchaseAmount",
    "purchaseCurrency",
    "purchaseExponent",
    "purchaseDate",
    "purchaseInstalData",
    "recurringExpiry",
    "recurringFrequency",
    "threeDSCompInd",
    "threeDSRequestorAuthenticationInd",
    "threeDSRequestorChallengeInd",
    "threeDSRequestorID",
    "threeDSRequestorName",
    "threeDSRequestorURL",
    "threeDSServerRefNumber",
    "threeDSServerOperatorID",
    "threeDSServerTransID",
    "threeDSServerURL",
    "transType"
];

// JSON Fields to be populated for PA 3ri flow 2.1.0
const AREQ_PA_3RI_210 = [
    "acctNumber",
    "cardExpiryDate",
    "acquirerBIN",
    "acquirerMerchantID",
    "deviceChannel",
    "mcc",
    "merchantCountryCode",
    "merchantName",
    "messageCategory",
    "messageType",
    "messageVersion",
    "threeDSRequestorID",
    "threeDSRequestorName",
    "threeDSRequestorURL",
    "threeDSServerRefNumber",
    "threeDSServerOperatorID",
    "threeDSServerTransID",
    "purchaseAmount",
    "purchaseCurrency",
    "purchaseExponent",
    "purchaseDate",
    "threeRIInd"
];

// JSON Fields to be populated for NPA App flow 2.1.0
const AREQ_NPA_APP_210 = [
    "sdkMaxTimeout",
    "threeDSRequestorAuthenticationInd",
    "acctNumber",
    "cardExpiryDate",
    "deviceInfo",
    "deviceRenderOptions",
    "sdkEncData",
    "sdkAppID",
    "sdkEphemPubKey",
    "sdkReferenceNumber",
    "sdkTransID",
    "deviceChannel",
    "mcc",
    "merchantCountryCode",
    "merchantName",
    "messageCategory",
    "messageType",
    "messageVersion",
    "purchaseAmount",           // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseCurrency",         // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseExponent",         // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseDate",             // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseInstalData",       // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "recurringExpiry",          // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "recurringFrequency",       // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "threeDSRequestorID",
    "threeDSRequestorName",
    "threeDSRequestorURL",
    "threeDSServerRefNumber",
    "threeDSServerOperatorID",
    "threeDSServerTransID",
    "threeDSServerURL"
];

// JSON Fields to be populated for NPA Browser flow 2.1.0
const AREQ_NPA_BRW_210 = [
    "acctInfo",
    "acctNumber",
    "cardExpiryDate",
    "acctType",
    "acquirerBIN",
    "acquirerMerchantID",
    "addrMatch",
    "browserAcceptHeader",
    "browserIP",
    "browserJavaEnabled",
    "browserLanguage",
    "browserColorDepth",
    "browserScreenHeight",
    "browserScreenWidth",
    "browserTZ",
    "browserUserAgent",
    "deviceChannel",
    "mcc",
    "merchantCountryCode",
    "merchantName",
    "merchantRiskIndicator",
    "messageCategory",
    "messageType",
    "messageVersion",
    "notificationURL",
    "purchaseAmount",       // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseCurrency",     // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseExponent",     // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseDate",         // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "purchaseInstalData",   // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "recurringExpiry",      // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "recurringFrequency",   // conditional but added to field list because default value for threeDSRequestorAuthenticationInd = 03
    "threeDSCompInd",
    "threeDSRequestorAuthenticationInd",
    "threeDSRequestorChallengeInd",
    "threeDSRequestorID",
    "threeDSRequestorName",
    "threeDSRequestorURL",
    "threeDSServerRefNumber",
    "threeDSServerOperatorID",
    "threeDSServerTransID",
    "threeDSServerURL"
];

// JSON Fields to be populated for NPA 3RI flow 2.1.0
const AREQ_NPA_3RI_210 = [
    "acctNumber",
    "cardExpiryDate",
    "acquirerBIN",
    "acquirerMerchantID",
    "deviceChannel",
    "mcc",
    "merchantCountryCode",
    "merchantName",
    "messageCategory",
    "messageType",
    "messageVersion",
    "threeDSRequestorID",
    "threeDSRequestorName",
    "threeDSRequestorURL",
    "threeDSServerRefNumber",
    "threeDSServerOperatorID",
    "threeDSServerTransID",
    "threeRIInd"
];

// JSON Fields to be populated for PA App flow 2.2.0
const AREQ_PA_APP_220 = AREQ_PA_APP_210.concat([
    "threeDSRequestorDecMaxTime",
    "threeDSRequestorDecReqInd",
    "whiteListStatus",
    "whiteListStatusSource"
]);

// JSON Fields to be populated for PA Browser flow 2.2.0
const AREQ_PA_BRW_220 = AREQ_PA_BRW_210.concat([
    "threeDSRequestorDecMaxTime",
    "threeDSRequestorDecReqInd",
    "whiteListStatus",
    "whiteListStatusSource",
    "browserJavascriptEnabled"
]);

// JSON Fields to be populated for PA 3ri flow 2.2.0
const AREQ_PA_3RI_220 = AREQ_PA_3RI_210.concat([
    "purchaseInstalData",
    "recurringExpiry",
    "recurringFrequency",
    "threeDSServerURL",
    "threeDSRequestorDecMaxTime",
    "threeDSRequestorDecReqInd",
    "whiteListStatus",
    "whiteListStatusSource"
]);

// JSON Fields to be populated for NPA App flow 2.2.0
const AREQ_NPA_APP_220 = AREQ_NPA_APP_210.concat([
    "threeDSRequestorDecMaxTime",
    "threeDSRequestorDecReqInd",
    "whiteListStatus",
    "whiteListStatusSource"
]);

// JSON Fields to be populated for NPA Browser flow 2.2.0
const AREQ_NPA_BRW_220 = AREQ_NPA_BRW_210.concat([
    "threeDSRequestorDecMaxTime",
    "threeDSRequestorDecReqInd",
    "whiteListStatus",
    "whiteListStatusSource",
    "browserJavascriptEnabled"
]);

// JSON Fields to be populated for NPA 3ri flow 2.2.0
const AREQ_NPA_3RI_220 = AREQ_NPA_3RI_210.concat([
    "purchaseAmount",
    "purchaseCurrency",
    "purchaseExponent",
    "purchaseDate",
    "purchaseInstalData",
    "recurringExpiry",
    "recurringFrequency",
    "threeDSServerURL",
    "threeDSRequestorDecMaxTime",
    "threeDSRequestorDecReqInd",
    "whiteListStatus",
    "whiteListStatusSource"
]);

// JSON Fields to be populated for PA App flow 2.3.0
const AREQ_PA_APP_230 = generateMap(AREQ_PA_APP_220,
     ["appIp", "acceptLanguage", "acquirerCountryCode", "acquirerCountryCodeSource", "sdkType","deviceBindingStatus","deviceBindingStatusSource", "trustListStatus", "trustListStatusSource"],
     ["whiteListStatus", "whiteListStatusSource"]);

// JSON Fields to be populated for PA Browser flow 2.3.0
const AREQ_PA_BRW_230 = generateMap(AREQ_PA_BRW_220,
     ["acceptLanguage", "acquirerCountryCode", "acquirerCountryCodeSource","deviceBindingStatus","deviceBindingStatusSource", "trustListStatus", "trustListStatusSource"],
     ["whiteListStatus", "whiteListStatusSource",]);

// JSON Fields to be populated for PA 3ri flow 2.3.0
const AREQ_PA_3RI_230 = generateMap(AREQ_PA_3RI_220,
     ["acceptLanguage", "acquirerCountryCode", "acquirerCountryCodeSource","deviceBindingStatus","deviceBindingStatusSource", "trustListStatus", "trustListStatusSource"],
     ["whiteListStatus", "whiteListStatusSource"]);

// JSON Fields to be populated for NPA App flow 2.3.0
const AREQ_NPA_APP_230 = generateMap(AREQ_NPA_APP_220,
     ["appIp", "acceptLanguage", "acquirerCountryCode", "acquirerCountryCodeSource", "sdkType","deviceBindingStatus","deviceBindingStatusSource", "trustListStatus", "trustListStatusSource"],
     ["whiteListStatus", "whiteListStatusSource"]);

// JSON Fields to be populated for NPA Browser flow 2.3.0
const AREQ_NPA_BRW_230 = generateMap(AREQ_NPA_BRW_220,
     ["acceptLanguage", "acquirerCountryCode", "acquirerCountryCodeSource","deviceBindingStatus","deviceBindingStatusSource", "trustListStatus", "trustListStatusSource"],
     ["whiteListStatus", "whiteListStatusSource"]);

// JSON Fields to be populated for NPA 3ri flow 2.3.0
const AREQ_NPA_3RI_230 = generateMap(AREQ_NPA_3RI_220,
    ["acceptLanguage", "acquirerCountryCode", "acquirerCountryCodeSource","deviceBindingStatus","deviceBindingStatusSource",  "trustListStatus", "trustListStatusSource"],
    ["whiteListStatus", "whiteListStatusSource"]);

function generateMap(map, additionalFields, lessFields) {
    return map.concat(additionalFields).filter(field => !lessFields.includes(field));
}
