
// Areq Fields with default values
const PREQ_FIELDS = {};
PREQ_FIELDS['messageType'] = "PReq";
PREQ_FIELDS['messageVersion'] = "2.2.0";
PREQ_FIELDS['threeDSServerRefNumber'] = "DS-3DS-EMULATOR-REF-NUMBER";
PREQ_FIELDS['threeDSServerOperatorID'] = "DS-3DS-EMULATOR-OPER-ID";
PREQ_FIELDS['threeDSServerTransID'] = "20000000000";