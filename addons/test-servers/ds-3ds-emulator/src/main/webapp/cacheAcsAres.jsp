<%@ page import="java.io.BufferedReader, java.util.Map, java.util.HashMap"%><%
    // Save ARes string in application servlet context. Map value to threeDSServerTransID key, which has been a part of queryString.

    StringBuffer sb = new StringBuffer();
    BufferedReader br = request.getReader();
    String line;
    while ((line = br.readLine()) != null) {
        sb.append(line);
        sb.append("\n");
    }

    String aresString = sb.toString();
    String threeDSServerTransID = request.getParameter("threeDSServerTransID");

    if (application.getAttribute("aresCache") == null) {
        application.setAttribute("aresCache", new HashMap<String, String>());
    }
    Map<String, String> aresCache = (Map<String, String>) application.getAttribute("aresCache");

    aresCache.put(threeDSServerTransID, aresString);

    out.print("ARes cached successfully: " + threeDSServerTransID);
%>