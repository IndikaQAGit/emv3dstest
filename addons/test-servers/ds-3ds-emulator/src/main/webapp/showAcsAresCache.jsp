<%@ page import="java.util.Map" %><%

    if (request.getParameter("clear") != null) {
        application.removeAttribute("aresCache");
    }

    StringBuilder sb = new StringBuilder();
    sb.append("Cached items: ");

    if (application.getAttribute("aresCache") != null) {
        Map<String, String> aresCache = (Map<String, String>) application.getAttribute("aresCache");

        if (aresCache.isEmpty()) {
            sb.append("empty cache<br/>");
        } else {
            sb.append(aresCache.size() + " <br/>");
        }

        for (Map.Entry entry : aresCache.entrySet()) {
            sb.append("key: " + entry.getKey() + " value: " + entry.getValue()).append("<br/>");
        }
    } else {
        sb.append("no cache");
    }

    out.print(sb.toString());
%>