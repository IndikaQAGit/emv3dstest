/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 5. dets 2017
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.utils.DaemonThreadFactory;
import com.modirum.ds.utils.DateUtil;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SchedulerTest {

    public static void main(String[] args) {
        ScheduledThreadPoolExecutor taskScheduler = new ScheduledThreadPoolExecutor(12,
                                                                                    new DaemonThreadFactory("EXEC"));
        taskScheduler.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
        taskScheduler.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        taskScheduler.setMaximumPoolSize(12);
        System.out.println(DateUtil.formatDate(new java.util.Date(), "yyyyMMdd HH:mm:ss.SSS") + " Max=" +
                           taskScheduler.getMaximumPoolSize());

        System.out.println(DateUtil.formatDate(new java.util.Date(), "yyyyMMdd HH:mm:ss.SSS") + " Start");
        for (int i = 0; i < 12; i++) {
            taskScheduler.submit(new TestTask(i));
        }
        try {
            taskScheduler.awaitTermination(100, TimeUnit.SECONDS);
        } catch (Exception ee) {
            ee.printStackTrace(System.out);
        }

        System.out.println(DateUtil.formatDate(new java.util.Date(), "yyyyMMdd HH:mm:ss.SSS") + " End");
    }


    static class TestTask implements Runnable {
        int n;

        public TestTask(int n) {
            this.n = n;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                System.out.println(DateUtil.formatDate(new java.util.Date(), "yyyyMMdd HH:mm:ss.SSS") + " " +
                                   Thread.currentThread().getName() + " Task " + n);
                try {
                    Thread.sleep(100);
                } catch (Exception ee) {
                    ee.printStackTrace(System.out);
                    return;
                }
            }
        }

    }


}
