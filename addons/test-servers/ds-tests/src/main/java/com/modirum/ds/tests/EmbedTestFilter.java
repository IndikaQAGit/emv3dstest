/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 4. apr 2017
 *
 */
package com.modirum.ds.tests;

import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import com.modirum.ds.utils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.nio.charset.StandardCharsets;
import java.security.Permission;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author andri<br />
 * <br/>
 * <b>EmbedTestFilter</b> for injecting test scenarios to ACS or ACS emulator generated messages. Injects cases to servlet response outputstream
 * and JSSE https outputstreams<br/>
 * <b>Usage:</b>
 * <pre>
 * {@code
 * <filter>
 * <filter-name>EmbedTestFilter</filter-name>
 * <filter-class>com.modirum.ds.tests.EmbedTestFilter</filter-class>
 * </filter>
 * <filter-mapping>
 * <filter-name>EmbedTestFilter</filter-name>
 * <url-pattern>/*</url-pattern>
 * </filter-mapping>
 * }
 * </pre>
 * <br/>
 * Testcase to be loaded at startup can be speficifed from file having set context init param in web.xml
 * <pre>
 * {@code
 * <init-param>
 * <param-name>EmbedTestFilter.casesFile</param-name>
 * <param-value>/tmp/cases1.xml</param-value>
 * </init-param>
 * }
 * </pre>
 * or context.xml<br/>
 * <pre> {@code <Parameter name="EmbedTestFilter.casesFile" value="${catalina.base}/conf/Catalina/localhost/cases.xml" override="false"/>
 * }
 * </pre>
 * <br/>
 * <b>Syntax of xml file:</b><br/>
 * <pre>
 * {@code
 * <Cases>
 * <Case>
 * <caseType>ReplaceMatch</caseType>
 * <matcher>555</matcher>
 * <newValue>666</newValue>
 * </Case>
 * <Case>
 * <caseMatchMsgType>RReq</caseMatchMsgType>
 * <caseMatchFree>4016000000002</caseMatchFree>
 * <caseType>AddField</caseType>
 * <matcher>newField</matcher>
 * <newValue>newValue</newValue>
 * </Case>
 * </Cases>
 * }
 * </pre>
 * <br/>
 * <b>Case types:</b><br/>
 * caseType can have values: AddField, RemoveField, ReplaceFieldValue, ReplaceMatch<br/>
 *         <ul>
 *         <b>AddField</b>: add field <b>matcher</b>=fieldName to add, <b>newValue</b>=newValue<br/>
 *         <b>RemoveField</b>: remove field <b>matcher</b>=fieldName to remonve<br/>
 *         <b>ReplaceFieldValue</b>: replace value of field, <b>matcher</b>=fieldName to replace, <b>newValue</b>= new field value as replacement<br/>
 *         <b>ReplaceMatch</b>: free text replace <b>matcher</b>=text to match replace, <b>newValue</b>= new text to put as rplacement<br/>
 *         </ul>
 *         <br/>
 *         <b>Case to message matches:</b><br/>
 * <p>
 *         caseMatchMsgType: is the value or messageType to match the case<br/>
 *         caseMatchFree: is free text matcher<br/>
 *         if both are set for case then case matches if both matchers are present in message
 *
 *         <pre>
 * {@code
 * <caseMatchMsgType>ARes</caseMatchMsgType>
 * <caseMatchFree>"Y"</caseMatchFree>
 * }
 * </pre>
 * Match only ARes with having "Y" present
 * <br/>
 * <br/>
 * <b>Possible integration with already deployed full ACS product</b><br/>
 * <span style="color: red;">Caution: Not to be attempteted in any production env!!!</span><br/>
 * Tomcat must run without security manager enabled<br/>
 * On ACS deployment location (tomcat/webapps/acs) find WEB-INF/web.xml<br/>
 * Before &lt;servlet&gt; Add lines:
 * <pre>
 * {@code
 * <filter>
 * <filter-name>EmbedTestFilter</filter-name>
 * <filter-class>com.modirum.ds.tests.EmbedTestFilter</filter-class>
 * </filter>
 * <filter-mapping>
 * <filter-name>EmbedTestFilter</filter-name>
 * <url-pattern>/*</url-pattern>
 * </filter-mapping>
 * }</pre>
 * <p>
 * You may need to redo this after every new ACS deplyment!<br/>
 * From file dstests.war WEB-INF/lib copy all jar files to to tomcat/lib <br/>
 * Optional for web based management copy
 * from file dstests.war dstest.jsp to ACS deployment location (tomcat/webapps/acs)
 * <br/>
 * Restart tomcat
 */
public class EmbedTestFilter implements javax.servlet.Filter {
    public static boolean factorySet = false;
    protected transient static Logger log = LoggerFactory.getLogger(EmbedTestFilter.class);

    public static XmlUtils.JaxbHelp<EmbedTestFilter.Cases> jaxbHelp;
    // SUPPORTING STRUCTURES

    static {
        try {
            //URL.setURLStreamHandlerFactory();
            //Utils.getStaticMethod(arg0, arg1)
            setHandlerFactory(URL.class, "factory", new MyURLStreamHandlerFactory());
            factorySet = true;
        } catch (Throwable t) {
            log.error("Failed to set URLStreamHandlerFactory JSSE injection not possible..", t);
        }

        try {
            jaxbHelp = new XmlUtils.JaxbHelp<EmbedTestFilter.Cases>(EmbedTestFilter.Cases.class);
            jaxbHelp.setFormatted(true);
        } catch (Exception e) {
            log.error("Error creating jaxb helper", e);
        }
    }

    public static void setHandlerFactory(Class<?> clazz, String fieldName, Object o) throws Exception {
        Field[] allf = clazz.getDeclaredFields();
        for (Field f : allf) {
            if (Modifier.isStatic(f.getModifiers()) && fieldName.equals(f.getName())) {
                f.setAccessible(true);
                f.set(null, o);
            }
            if (Modifier.isStatic(f.getModifiers()) && "handlers".equals(f.getName())) {
                f.setAccessible(true);
                @SuppressWarnings("rawtypes") Map h = (Map) f.get(null);
                if (h != null) {
                    h.remove("https");
                }
            }
        }
    }


    public static class MyURLStreamHandlerFactory implements URLStreamHandlerFactory {
        @Override
        public URLStreamHandler createURLStreamHandler(String protocol) {
            if ("https".equals(protocol)) {
                return new MyURLStreamHandler();
            }
            return null;
        }
    }

    public static class MyURLStreamHandler extends URLStreamHandler {

        public MyURLStreamHandler() {
            try {
                //		real=(URLStreamHandler)Class.forName("sun.net.www.protocol.https.Handler").newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
            return openConnection(u, null);
        }

        protected URLConnection openConnection(URL u, Proxy proxy) throws IOException {
            if ("https".equals(u.getProtocol())) {
                MyHttpsURLConnection uc = new MyHttpsURLConnection(u);
                try {
                    Class impl = Class.forName("sun.net.www.protocol.https.HttpsURLConnectionImpl");
                    URLStreamHandler hi = (URLStreamHandler) Class.forName(
                            "sun.net.www.protocol.https.Handler").newInstance();
                    if (proxy != null) {
                        Constructor<javax.net.ssl.HttpsURLConnection> c = impl.getDeclaredConstructor(URL.class,
                                                                                                      Proxy.class,
                                                                                                      hi.getClass());
                        c.setAccessible(true);
                        uc.real = c.newInstance(u, proxy, hi);
                        return uc;
                    } else {
                        Constructor<javax.net.ssl.HttpsURLConnection> c = impl.getDeclaredConstructor(URL.class,
                                                                                                      hi.getClass());
                        c.setAccessible(true);
                        uc.real = c.newInstance(u, hi);
                        return uc;
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            return null;
        }

        protected int getDefaultPort() {
            return 443;
        }

    }

    public static class MyHttpsURLConnection extends javax.net.ssl.HttpsURLConnection {
        BufferedOutputStream os;
        javax.net.ssl.HttpsURLConnection real;

        protected MyHttpsURLConnection(URL url) {
            super(url);
        }

        @Override
        public void disconnect() {
            real.disconnect();
        }

        public void setDoOutput(boolean b) {
            real.setDoOutput(b);
        }

        @Override
        public boolean usingProxy() {
            return real.usingProxy();
        }

        @Override
        public void connect() throws IOException {
            real.connect();
        }

        @Override
        public String getCipherSuite() {
            return real.getCipherSuite();
        }

        @Override
        public Certificate[] getLocalCertificates() {
            return real.getLocalCertificates();
        }

        @Override
        public Certificate[] getServerCertificates() throws SSLPeerUnverifiedException {
            return real.getServerCertificates();
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return new MyFilteredOutputStream(real.getOutputStream());
        }

        public String getHeaderFieldKey(int n) {
            return real.getHeaderFieldKey(n);
        }

        public boolean equals(Object obj) {
            return real.equals(obj);
        }

        public String getHeaderField(int n) {
            return real.getHeaderField(n);
        }

        public boolean getInstanceFollowRedirects() {
            return real.getInstanceFollowRedirects();
        }

        public int getConnectTimeout() {
            return real.getConnectTimeout();
        }

        public int getContentLength() {
            return real.getContentLength();
        }

        public long getContentLengthLong() {
            return real.getContentLengthLong();
        }

        public String getContentType() {
            return real.getContentType();
        }

        public long getHeaderFieldDate(String name, long Default) {
            return real.getHeaderFieldDate(name, Default);
        }

        public String getContentEncoding() {
            return real.getContentEncoding();
        }

        public long getExpiration() {
            return real.getExpiration();
        }

        public long getDate() {
            return real.getDate();
        }

        public InputStream getErrorStream() {
            return real.getErrorStream();
        }

        public long getLastModified() {
            return real.getLastModified();
        }

        public String getHeaderField(String name) {
            return real.getHeaderField(name);
        }

        public Map<String, List<String>> getHeaderFields() {
            return real.getHeaderFields();
        }

        public int getHeaderFieldInt(String name, int Default) {
            return real.getHeaderFieldInt(name, Default);
        }

        public long getHeaderFieldLong(String name, long Default) {
            return real.getHeaderFieldLong(name, Default);
        }

        public Object getContent() throws IOException {
            return real.getContent();
        }

        public Object getContent(Class[] classes) throws IOException {
            return real.getContent(classes);
        }

        public InputStream getInputStream() throws IOException {
            return real.getInputStream();
        }

        public boolean getDoInput() {
            return real.getDoInput();
        }

        public boolean getDoOutput() {
            return real.getDoOutput();
        }

        public boolean getAllowUserInteraction() {
            return real.getAllowUserInteraction();
        }

        public long getIfModifiedSince() {
            return real.getIfModifiedSince();
        }

        public boolean getDefaultUseCaches() {
            return real.getDefaultUseCaches();
        }

        public void addRequestProperty(String key, String value) {
            real.addRequestProperty(key, value);
        }

        public HostnameVerifier getHostnameVerifier() {
            return real.getHostnameVerifier();
        }

        public Principal getLocalPrincipal() {
            return real.getLocalPrincipal();
        }

        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
            return real.getPeerPrincipal();
        }

        public int hashCode() {
            return real.hashCode();
        }

        public void setFixedLengthStreamingMode(int contentLength) {
            real.setFixedLengthStreamingMode(contentLength);
        }

        public void setFixedLengthStreamingMode(long contentLength) {
            real.setFixedLengthStreamingMode(contentLength);
        }

        public void setChunkedStreamingMode(int chunklen) {
            real.setChunkedStreamingMode(chunklen);
        }

        public void setInstanceFollowRedirects(boolean followRedirects) {
            real.setInstanceFollowRedirects(followRedirects);
        }

        public void setRequestMethod(String method) throws ProtocolException {
            real.setRequestMethod(method);
        }

        public void setConnectTimeout(int timeout) {
            real.setConnectTimeout(timeout);
        }

        public String getRequestMethod() {
            return real.getRequestMethod();
        }

        public int getResponseCode() throws IOException {
            return real.getResponseCode();
        }

        public void setReadTimeout(int timeout) {
            real.setReadTimeout(timeout);
        }

        public int getReadTimeout() {
            return real.getReadTimeout();
        }

        public URL getURL() {
            return real.getURL();
        }

        public String getResponseMessage() throws IOException {
            return real.getResponseMessage();
        }

        public Permission getPermission() throws IOException {
            return real.getPermission();
        }

        public String toString() {
            return real.toString();
        }

        public void setDoInput(boolean doinput) {
            real.setDoInput(doinput);
        }

        public void setAllowUserInteraction(boolean allowuserinteraction) {
            real.setAllowUserInteraction(allowuserinteraction);
        }

        public void setUseCaches(boolean usecaches) {
            real.setUseCaches(usecaches);
        }

        public boolean getUseCaches() {
            return real.getUseCaches();
        }

        public void setIfModifiedSince(long ifmodifiedsince) {
            real.setIfModifiedSince(ifmodifiedsince);
        }

        public void setDefaultUseCaches(boolean defaultusecaches) {
            real.setDefaultUseCaches(defaultusecaches);
        }

        public void setRequestProperty(String key, String value) {
            real.setRequestProperty(key, value);
        }

        public String getRequestProperty(String key) {
            return real.getRequestProperty(key);
        }

        public Map<String, List<String>> getRequestProperties() {
            return real.getRequestProperties();
        }

        public SSLSocketFactory getSSLSocketFactory() {
            return real.getSSLSocketFactory();
        }

        public void setHostnameVerifier(HostnameVerifier v) {
            real.setHostnameVerifier(v);
        }

        public void setSSLSocketFactory(SSLSocketFactory sf) {
            real.setSSLSocketFactory(sf);
        }
    }

    public static class MyFilteredOutputStream extends BufferedOutputStream {
        public MyFilteredOutputStream(OutputStream out) {
            super(out, 32 * 1024);
        }

        public MyFilteredOutputStream(OutputStream out, int size) {
            super(out, size);
        }

        // manipulate ssl conncection outbound data
        public void flush() throws IOException {
            byte[] newBuf = applyTransformations(buf, 0, count);
            this.buf = newBuf;
            this.count = newBuf.length;
            super.flush();
        }

    }

    public static class MyServletOutputStream extends javax.servlet.ServletOutputStream {
        ByteArrayOutputStream bos = null;

        public MyServletOutputStream() {
            bos = new ByteArrayOutputStream(1024 * 32);
        }

        public MyServletOutputStream(int s) {
            bos = new ByteArrayOutputStream(s);
        }

        // manipulate ssl conncection outbound data
        public void flush() throws IOException {

        }

        public void close() throws IOException {

        }

        public void write(byte[] b) throws IOException {
            bos.write(b);
        }

        public void write(byte[] b, int off, int len) throws IOException {
            bos.write(b, off, len);
        }

        @Override
        public void write(int b) throws IOException {
            bos.write(b);
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {

        }
    }

    public static class MyServletResponse extends javax.servlet.http.HttpServletResponseWrapper {
        MyServletOutputStream sos;
        HttpServletResponse res;
        PrintWriter pw;

        public MyServletResponse(HttpServletResponse res) throws IOException {
            super(res);
            this.res = res;

        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (sos == null) {
                sos = new MyServletOutputStream(res.getBufferSize());
            }
            return sos;
        }

		/*
		@Override
		public PrintWriter getWriter() throws IOException
		{
			if (pw!=null)
			{
				pw=new PrintWriter(new OutputStreamWriter(sos, res.getCharacterEncoding()));
			}

		return pw;
		}
		*/

        @Override
        public void flushBuffer() throws IOException {

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name = "Cases")
    @XmlSeeAlso({Case.class})
    public static class Cases {
        @XmlElement(name = "Case")
        List<Case> cases = new java.util.ArrayList<>(20);

        public List<Case> getCases() {
            return cases;
        }

        public void setCases(List<Case> cases) {
            this.cases = cases;
        }
    }

    public static class Case {
        public interface Type {
            String cAddField = "AddField";
            String cRemoveField = "RemoveField";
            String cReplaceFieldValue = "ReplaceFieldValue";
            String cReplaceMatch = "ReplaceMatch";
        }

        // case match
        String caseMatchMsgType;
        String caseMatchFree;

        String caseType;
        String matcher;
        String newValue;

        public String getCaseMatchMsgType() {
            return caseMatchMsgType;
        }

        public void setCaseMatchMsgType(String caseMatchMsgType) {
            this.caseMatchMsgType = caseMatchMsgType;
        }

        public String getCaseMatchFree() {
            return caseMatchFree;
        }

        public void setCaseMatchFree(String caseMatchFree) {
            this.caseMatchFree = caseMatchFree;
        }

        public String getCaseType() {
            return caseType;
        }

        public void setCaseType(String cseType) {
            this.caseType = cseType;
        }

        public String getNewValue() {
            return newValue;
        }

        public String getMatcher() {
            return matcher;
        }

        public void setMatcher(String matcher) {
            this.matcher = matcher;
        }

        public void setNewValue(String newValue) {
            this.newValue = newValue;
        }
    }

    static Cases cases;

    // BUSINESS LOGIC
    // apply json modifications here
    static byte[] applyTransformations(byte[] buf, int offs, int count) {
        if (count < 1) {
            return buf;
        }
        byte[] newbuf = null;

        if (config != null) {
            cases = (Cases) config.getServletContext().getAttribute("EmbedTestFilter.cases");
        }

        if (cases != null) {
            try {
                if (cases != null && cases.getCases() != null && cases.getCases().size() > 0) {
                    StringBuilder sb = new StringBuilder(count * 2).append(
                            new String(buf, 0, count, StandardCharsets.UTF_8));
                    for (Case casex : cases.getCases()) {
                        applyCase(casex, sb);
                    }

                    String tf = sb.toString();
                    if (log.isDebugEnabled()) {
                        log.debug("Transformed:" + tf);
                    }
                    newbuf = tf.getBytes(StandardCharsets.UTF_8);
                }
            } catch (Exception e) {
                log.warn("Error in transformation ", e);
            }
        }
        if (newbuf == null) {
            newbuf = new byte[count];
            System.arraycopy(buf, 0, newbuf, 0, count);
        }

        return newbuf;
    }

    static void applyCase(Case casex, StringBuilder sb) {

        // "messageType" : "AReq",
        if (casex.getCaseMatchMsgType() != null && casex.getCaseMatchMsgType().length() > 0) {
            int pmmt = sb.indexOf("\"messageType\"");
            if (pmmt >= 0) {
                int pm = sb.indexOf(casex.getCaseMatchMsgType(), pmmt);
                if (pm < 0) {
                    return;
                }

                int colm = sb.indexOf(":", pmmt);
                if (colm < pm) {
                    // match
                } else {
                    return;
                }
            } else {
                return;
            }
        }

        if (casex.getCaseMatchFree() != null && casex.getCaseMatchFree().length() > 0) {
            int cmf = sb.indexOf(casex.getCaseMatchFree());
            if (cmf < 0) {
                return;
            }
        }

        if (Case.Type.cReplaceMatch.equals(casex.caseType)) {
            int start = sb.indexOf(casex.getMatcher());
            if (start > -1) {
                sb.delete(start, start + casex.getMatcher().length());
                sb.insert(start, casex.newValue);
            }
        } else if (Case.Type.cReplaceFieldValue.equals(casex.caseType)) {
            int fieldNameStart = sb.indexOf("\"" + casex.getMatcher() + "\"");
            if (fieldNameStart > -1) {
                int colon = sb.indexOf(":", fieldNameStart + casex.getMatcher().length());
                int comma = sb.indexOf(",", fieldNameStart + casex.getMatcher().length());
                int endobj = sb.indexOf("}", fieldNameStart + casex.getMatcher().length());
                if (colon < comma || colon < endobj) {
                    int quote = sb.indexOf("\"", colon);
                    int endquote = sb.indexOf("\"", quote + 1);

                    boolean qutoesvalue =
                            endquote > quote && quote > colon && (quote < comma || comma < 0) && quote < endobj;
                    if (qutoesvalue) {
                        sb.delete(quote + 1, endquote);
                        sb.insert(quote + 1, casex.newValue);
                    } else {
                        int valueend = min(comma, endobj);
                        sb.delete(colon + 1, valueend);
                        sb.insert(colon + 1, " " + casex.newValue);
                    }
                }
            }
        } else if (Case.Type.cRemoveField.equals(casex.caseType)) {
            int fieldNameStart = getFieldNameStart(sb, casex.matcher);
            if (fieldNameStart > -1) {
                int fieldEnd = getFieldEnd(sb, casex.matcher, fieldNameStart);
                int nc = nextcommabeforedata(sb, fieldEnd);
                sb.delete(fieldNameStart, nc >= fieldEnd ? nc + 1 : fieldEnd);
            }
        } else if (Case.Type.cAddField.equals(casex.caseType)) {
            int end = sb.indexOf("}");
            if (end < 0) {
                end = sb.length();
            }
            boolean hadfield = sb.indexOf(":") > -1;
            sb.insert(end > 0 ? end - 1 : 0,
                      (hadfield ? "," : "") + ("\"" + casex.getMatcher() + "\" : \"" + casex.getNewValue() + "\""));
        }

    }

    static int getFieldNameStart(StringBuilder sb, String fieldName) {
        int fieldNameStart = sb.indexOf("\"" + fieldName + "\"");
        if (fieldNameStart > -1) {
            int colon = sb.indexOf(":", fieldNameStart + fieldName.length() + 2);
            int comma = sb.indexOf(",", fieldNameStart + fieldName.length() + 2);
            int endobj = sb.indexOf("}", fieldNameStart + fieldName.length() + 2);
            if (colon < comma || colon < endobj) {
                return fieldNameStart;

            }
        }
        return -1;
    }

    static int getFieldEnd(StringBuilder sb, String fieldName, int start) {
        int fieldNameStart = sb.indexOf("\"" + fieldName + "\"", start);
        if (fieldNameStart > -1) {
            int colon = sb.indexOf(":", fieldNameStart + fieldName.length() + 2);
            int comma = sb.indexOf(",", fieldNameStart + fieldName.length() + 2);
            int endobj = sb.indexOf("}", fieldNameStart + fieldName.length() + 2);
            if (colon < comma || colon < endobj) {
                int end = min(comma, endobj);
                if (end < 0) {
                    end = sb.length();
                }
                return end;
            }
        }
        return -1;
    }

    static int getFieldValueStart(StringBuilder sb, String fieldName, int start) {
        int fieldNameStart = sb.indexOf("\"" + fieldName + "\"", start);
        if (fieldNameStart > -1) {
            int colon = sb.indexOf(":", fieldNameStart + fieldName.length() + 2);
            int comma = sb.indexOf(",", fieldNameStart + fieldName.length() + 2);
            int endobj = sb.indexOf("}", fieldNameStart + fieldName.length() + 2);
            if (colon < comma || colon < endobj) {
                int end = min(comma, endobj);
                if (end < 0) {
                    end = sb.length();
                }
                return end;
            }
        }
        return -1;
    }

    static int commabefore(StringBuilder sb, int start) {
        int cmb = -1;
        int cmbx = -1;
        while (cmbx < start) {
            cmbx = sb.indexOf(",", cmbx + 1);
            if (cmbx > -1) {
                cmb = cmbx;
            } else {
                break;
            }
        }

        return cmb;
    }

    static int nextcommabeforedata(StringBuilder sb, int start) {
        for (int i = start; i < sb.length(); i++) {
            char c = sb.charAt(i);
            if (c == ',') {
                return i;
            }

            if (!Character.isWhitespace(c)) {
                break;
            }
        }
        return -1;
    }

    static int min(int a, int b) {
        if (a > -1 && (a <= b || b < 0)) {
            return a;
        }
        if (b > -1 && (b <= a || a < 0)) {
            return b;
        }

        return -1;
    }

    static FilterConfig config;

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        res.setBufferSize(32 * 1024);
        MyServletResponse myRes = new MyServletResponse((HttpServletResponse) res);
        chain.doFilter(req, myRes);

        if (myRes.sos != null) {
            res.resetBuffer();
            byte[] sb = myRes.sos.bos.toByteArray();
            myRes.sos.bos.reset();
            if (myRes.getContentType() != null && myRes.getContentType().contains("json")) {
                sb = applyTransformations(sb, 0, sb.length);
            }
            res.setContentLength(sb.length);
            res.getOutputStream().write(sb);
        }
        res.flushBuffer();

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        config = arg0;
        Cases cases = new Cases();
        config.getServletContext().setAttribute("EmbedTestFilter.cases", cases);
        String preDefinedCasesFile = config.getServletContext().getInitParameter("EmbedTestFilter.casesFile");
        if (Misc.isNotNullOrEmpty(preDefinedCasesFile) && new java.io.File(preDefinedCasesFile).exists()) {
            String fn = new java.io.File(preDefinedCasesFile).getAbsolutePath();
            try {
                byte[] d = Utils.readFile(fn);
                Cases casesx = jaxbHelp.unmarshal(new java.io.ByteArrayInputStream(d));
                config.getServletContext().setAttribute("EmbedTestFilter.cases", casesx);

                log.info("Loaded startup case from file " + fn + " ok");
            } catch (Exception e) {
                log.error("Loading startup case from file " + fn + " failed", e);
            }

        }
    }

    public static void main(String[] a) {
        try {
            testlogconf();
            cases = new Cases();
            Case c1 = new Case();
            c1.setCaseType(Case.Type.cReplaceMatch);
            c1.setMatcher("c=");
            c1.setNewValue("c=y");
            cases.getCases().add(c1);


            String url = "https://vposadmin.modirum.com/jvm.jsp";
            log.info("Send to " + url);
            com.modirum.ds.utils.http.HttpsJSSEClient jssecl = new com.modirum.ds.utils.http.HttpsJSSEClient();
            jssecl.init(null, null, "TLSv1.1", "true", null, null, null, 10000, 30000, false);
            byte[] resp = jssecl.post(url, "application/x-www-form-urlencoded", "gc=".getBytes(StandardCharsets.UTF_8));
            //System.out.println("Resp=" + new String(resp, StandardCharsets.UTF_8));
            log.info("Resp: " + new String(resp, StandardCharsets.UTF_8));

            Case c2 = new Case();
            c2.setCaseType(Case.Type.cAddField);
            c2.setMatcher("newField");
            c2.setNewValue("newValue");
            cases.getCases().add(c2);

            Case c3 = new Case();
            c3.setCaseType(Case.Type.cRemoveField);
            c3.setMatcher("purchaseAmount");
            c3.setNewValue("");
            cases.getCases().add(c3);

            Case c4 = new Case();
            c4.setCaseType(Case.Type.cReplaceFieldValue);
            c4.setMatcher("purchaseCurrency");
            c4.setCaseMatchMsgType("AReq");
            c4.setNewValue("5557");
            cases.getCases().add(c4);

            Case c5 = new Case();
            c5.setCaseType(Case.Type.cReplaceFieldValue);
            c5.setMatcher("numberField");
            c5.setNewValue("555");
            cases.getCases().add(c5);

            Case c6 = new Case();
            c6.setCaseType(Case.Type.cReplaceMatch);
            c6.setCaseMatchMsgType("RReq");
            c6.setMatcher("\"messageCategory\" : \"01\"");
            c6.setNewValue("!!!\"messaKKategory\" : \"71\"");
            cases.getCases().add(c6);

            c6 = new Case();
            c6.setCaseType(Case.Type.cReplaceFieldValue);
            c6.setCaseMatchMsgType("AReq");
            c6.setMatcher("newField");
            c6.setNewValue("newValue2");
            cases.getCases().add(c6);


            byte[] buf = Utils.readFile(EmbedTestFilter.class.getResource("test.json").getFile());
            byte[] buf2 = EmbedTestFilter.applyTransformations(buf, 0, buf.length);

            byte[] xml = jaxbHelp.marshal(cases);
            log.info("XML:\n" + new String(xml, "UTF-8"));

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    static void testlogconf() throws Exception {
        Properties settings = new Properties();
        settings.put("log4j.rootLogger", "CONSOLE");
        settings.put("log4j.appender.CONSOLE", "org.apache.log4j.ConsoleAppender");
        settings.put("log4j.appender.CONSOLE.layout.ConversionPattern", "%d{MMd HH:mm:ss,SSS} %-5p %c{1} - %m%n");
        settings.put("log4j.appender.CONSOLE.layout", "org.apache.log4j.PatternLayout");
        settings.put("log4j.appender.CONSOLE.Threshold", "DEBUG");

        settings.put("log4j.logger.com.mchange.v2.c3p0.impl", "WARN");

        settings.put("log4j.logger.org", "WARN, CONSOLE");
        settings.put("log4j.additivity.org", "false");

        settings.put("log4j.logger.com", "WARN, CONSOLE");
        settings.put("log4j.additivity.com", "false");

        settings.put("log4j.logger.com.modirum", "DEBUG, CONSOLE");
        settings.put("log4j.additivity.com.modirum", "false");
        Utils.configureLog4j(settings);
    }

}
