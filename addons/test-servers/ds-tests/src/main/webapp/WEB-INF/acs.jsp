<%@page import="com.modirum.ds.utils.DateUtil"%><%@page import="com.modirum.ds.model.TDSModel.XmessageType"%>
<%@page import="java.security.PrivateKey"%><%@page import="com.modirum.ds.tests.Helper"%><%@page import="java.security.cert.X509Certificate"%>
<%@page session="true" import="com.modirum.ds.utils.Base64,com.modirum.ds.utils.Misc"%><%
request.setCharacterEncoding("UTF-8"); 
response.setCharacterEncoding("UTF-8");	
org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger("com.modirum.mdpay.ds.web.dstest");
// RRES
java.util.Date now =new java.util.Date();

String cRes=(String)request.getAttribute("cRes");
String termUrl=(String)request.getAttribute("termUrl");
String challenge =request.getParameter("challenge");
String cReq=(String)request.getAttribute("cReq");
String error=(String)request.getAttribute("error");
String counter=(String)request.getAttribute("counter");
String threeDSSessionData=(String)request.getAttribute("threeDSSessionData");

%><html>
<head>
<title>ACS test</title>
<script type="text/javascript">
function doRedirect() {
    var form = document.getElementById("redirectForm");
    if (form != null) {
      form.submit();
    }
  }
</script>
</head>
<body>
<h2>ACS 3DS20 Emulation</h2>
<%
if (termUrl!=null && cRes!=null)
{
%>
	<form id="redirectForm" name="redirectForm" action="<%=termUrl %>" method="post" >
	<input type="hidden" name="cres" value="<%=Misc.defaultString(cRes) %>"/>
	<input type="hidden" name="threeDSSessionData" value="<%=Misc.defaultString(threeDSSessionData) %>"/>
	<input type="submit" name="return" value="Return"/>
	</form>
	<script type="text/javascript">
		doRedirect();
	</script>
<%
}
else
{	
%>
<h3>Make test authentication</h3>
<% if (error!=null) { %><div style="color: red;"><%=error %></div><% } %>
<form id="authform" name="dummy" action="" method="post" >
<input type="hidden" name="CReq" value="<%=Misc.defaultString(cReq) %>"/>
<input type="hidden" name="threeDSSessionData" value="<%=Misc.defaultString(threeDSSessionData) %>"/>
<input type="hidden" name="counter" value="<%=Misc.defaultString(counter) %>"/>
Challenge: <input type="text" size="45" name="challenge" placeholder="correct|delayed|attempt|wrong/cancel/error/unable/reject" value="<%=Misc.defaultString(challenge) %>"/>
<br/>
<input type="submit" name="authenticate" value="Authenticate"/>
</form>
<%
}
%>

</body>
</html>
