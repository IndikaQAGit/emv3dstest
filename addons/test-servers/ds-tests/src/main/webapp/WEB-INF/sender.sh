#!/bin/sh
# Note you need to run this application with java that has
# installed the Java Cryptography Extension (JCE) Unlimited Strength
# jurisdiction Policy Files (local_policy.jar)
# for example this java has it..
java="java -Xmx512M"
#
umask 077
LIBS="./classes"
LIBS="$LIBS:./lib/*"
$java -cp $LIBS com.modirum.ds.tests.MessageBurster $*