package com.modirum.generic.utility;

import org.openqa.selenium.remote.server.SeleniumServer;

import java.io.IOException;
import java.net.ServerSocket;

public class SelServer {
    private static SeleniumServer server = null;
    private static int port;

    public static void setPort(String prt) {
        port = Integer.parseInt(prt);
    }

    public static void start() {
        try {
            ServerSocket socket = new ServerSocket(port);
            socket.close();
            server = new SeleniumServer(port);

            if (server != null) {
                server.boot();
            }
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public static void stop() {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                // do nothing
            } finally {
                server = null;
            }
        }
    }

    public static SeleniumServer getServer() {
        return server;
    }
}
