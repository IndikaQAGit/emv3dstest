package com.modirum.generic.utility;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Class name: SelDriver
 * Description: This class wraps and extends the capabilities of the Webdriver for the specified browser and selenium server node.
 * - Copied and merged some functions from Jai/Jace's BrowserConfig and SelDriver classes
 */
public class SelDriver {
    private WebDriver driver = null;

    /**
     * SelDriver Constructor
     * Pass browser and URL node as Arguments to this method
     *
     * @param browser
     * @param node
     * @throws IllegalArgumentException
     */
    public SelDriver(String browser, String node) {
        try {
            DesiredCapabilities cap = null;

            if (browser.equalsIgnoreCase("firefox")) {
                System.out.println(" Executing on Firefox");

                cap = DesiredCapabilities.firefox();
                cap.setBrowserName("firefox");
                driver = new RemoteWebDriver(new URL(node), cap);
            } else if (browser.equalsIgnoreCase("chrome")) {
                System.out.println(" Executing on Chrome");

                cap = DesiredCapabilities.chrome();
                cap.setBrowserName("chrome");
                driver = new RemoteWebDriver(new URL(node), cap);
            } else {
                throw new IllegalArgumentException("The Browser type is not supported!");
            }

            // Puts an Implicit wait, Will wait for 10 seconds before throwing exception
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("Something went wrong... - " + e.getMessage());
        }
    }

    /**
     * Get the native web m_driver instance
     * This method is to get the native WebDriver instance
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Open the given URL
     * This method is to open the given URL in the browser
     * Pass the URL path as Arguments to this method
     *
     * @param url
     */
    public void openUrl(String url) {
        driver.get(url);
        //m_driver.manage().window().maximize();
    }

    /**
     * Close the browser
     * This method is to close the browser
     */
    public void closeBrowser() {
        driver.get(driver.getCurrentUrl());
        driver.close();
    }

    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    /**
     * Set the input field
     * This method is to clear the input field and to set its value to the given string
     * Pass locator (By) and the value to set as Arguments to this method
     *
     * @param by
     * @param val
     * @throws IllegalArgumentException
     */
    public void setInputField(By by, String val) {
        WebElement el = driver.findElement(by);

        el.clear();
        el.sendKeys(val);
    }

    public void setInputField(WebElement el, String val) {
        el.clear();
        el.sendKeys(val);
    }

    /**
     * Click the button or link
     * This method is to click the button using the given locator
     * Pass the locator (By) as Argument to this method
     *
     * @param by
     */
    public void click(By by) {
        driver.findElement(by).click();
    }

    public void click(WebElement el) {
        el.click();
    }

    /**
     * Double-click the button
     * This method is to double-click the button using the given locator
     * Pass the locator (By) as Argument to this method
     *
     * @param by
     */
    public void doubleClick(By by) {
        Actions action = new Actions(driver);
        WebElement el = driver.findElement(by);

        action.doubleClick(el).perform();
    }

    public void doubleClick(WebElement el) {
        Actions action = new Actions(driver);
        action.doubleClick(el).perform();
    }

    /**
     * Get the selected option from the combo-box
     *
     * @param by
     * @return
     */
    public String getSelectedOption(By by) {
        return new Select(driver.findElement(by)).getFirstSelectedOption().getText();
    }

    public String getSelectedOption(WebElement el) {
        return new Select(el).getFirstSelectedOption().getText();
    }

    /**
     * Select the value in the combo-box
     * This method is to select a given value in the combobox using the given locator
     * Pass the locator (By) as Argument to this method
     *
     * @param by
     * @param val
     */
    public void selectSingleOption(By by, String val) {
        Select cBox = new Select(driver.findElement(by));

        cBox.selectByVisibleText(val);
    }

    public void selectSingleOption(WebElement el, String val) {
        Select cBox = new Select(el);

        cBox.selectByVisibleText(val);
    }

    /**
     * Select the value in the combo-box
     * This method is to select a given index in the combobox using the given locator
     * Pass the locator (By) as Argument to this method
     *
     * @param by
     * @param index
     */
    public void selectSingleOption(By by, int index) {
        Select cBox = new Select(driver.findElement(by));
        cBox.selectByIndex(index);
    }

    public void selectSingleOption(WebElement el, int index) {
        Select cBox = new Select(el);
        cBox.selectByIndex(index);
    }

    public void selectMultiOptions(By by, String[] opts) {
        Select sel = new Select(driver.findElement(by));
        sel.deselectAll();

        for (String arg : opts) {
            sel.selectByVisibleText(arg);
            driver.findElement(by).sendKeys(Keys.CONTROL);
        }
    }

    /**
     * Get the value
     * This method is to get the value of the given element
     * Pass the locator (By) as Argument to this method
     *
     * @param by
     * @throws IllegalArgumentException
     */
    public String getValue(By by) {
        WebElement elem = driver.findElement(by);
        String val = getValue(elem);
        return val;
    }

    public String getValue(WebElement elem) {
        String val = elem.getText();
        String val2;
        if (val.equals("")) {
            val2 = elem.getAttribute("value");
            if (val2 != null) {
                val = val2;
            }
        }
        return val;
    }

    /**
     * Is Element Present
     * Checks if element is present in the browser
     *
     * @param by
     * @return true or false
     */
    public boolean isElementPresent(By by) {
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        try {
            driver.findElement(by);
            return true;
        } catch (Throwable e) {
            return false;
        } finally {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }

    /**
     * Get Alert and Close Alert
     * This will get the alert text and at the same time close the alert text based on passed value
     *
     * @param acceptAlert
     * @return
     */
    public String getAlertCloseAlert(boolean acceptAlert) {
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        if (acceptAlert) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return alertText;
    }

    /**
     * Get List of TRs in a table
     * If you want to know the number of TRs available in a table, you can use this method.
     * This method will return an array of rows (TRs) that holds arrays of cell (TDs)
     *
     * @param tablePath pass the table you want to get the list of TR
     * @return
     */
    public List<WebElement> getListOfTRs(String tablePath) {
        if (tablePath.length() == 0) {
            tablePath = "//div[@class='content']/table";
        }

        // Grab the table
        WebElement table = driver.findElement(By.xpath(tablePath));

        // Now get all the TR elements from the table
        List<WebElement> allRows = table.findElements(By.tagName("tr"));

        return allRows;
    }

    public int getTDColumnIndexFromRow(By byTD, String columnToFind) {
        int iColumn = -1;

        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        List<WebElement> tableRow = driver.findElements(byTD);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (tableRow.size() > 0) {
            for (int i = 0; i < tableRow.size(); i++) {
                if (tableRow.get(i).getText().trim().equals(columnToFind)) {
                    iColumn = i;
                    break;
                }
            }
            if (iColumn == -1) {
                System.out.println("No " + columnToFind + " column in table!");
            }
        } else {
            System.out.println("Specified table row does not exist!");
        }

        return iColumn + 1;
    }

    /**
     * Get Number of Element in the page
     *
     * @param element
     * @return
     */
    public int getNumOfElements(String element) {
        int elements = driver.findElements(By.xpath(element)).size();

        return elements;
    }

    /**
     * Get All Options available in Select tag
     *
     * @param selectName
     * @return
     */
    public List<WebElement> getAllOptions(String selectName) {
        /*WebElement sel = m_driver.findElement(By.name(selectName));
        List<WebElement> lists = sel.findElements(By.tagName("option"));*/

        Select options = new Select(driver.findElement(By.name(selectName)));
        List<WebElement> lists = options.getOptions();//will get all options as List<WebElement>

        return lists;
    }

    public boolean pageSourceContains(String findContent) {
        return driver.getPageSource().contains(findContent);
    }
}
