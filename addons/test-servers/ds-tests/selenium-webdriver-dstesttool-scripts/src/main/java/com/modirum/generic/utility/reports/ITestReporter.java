package com.modirum.generic.utility.reports;

public interface ITestReporter {
    void startTest(String testName, String testDescription);

    void logInfo(String info);

    void logSkip(String info);

    void logPass(String info);

    void logFail(String info);

    void logWarning(String info);

    void endTest();
}
