package com.modirum.generic.utility;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TestSummary {
    private class TestCase {
        Map<String, String> columns;

        public TestCase() {
            columns = new HashMap<String, String>();
        }

        public void setTestCaseData(String testCase, String result) {
            columns.put(testCase, result);
        }

        public String getTestCaseData(String testCase) {
            if (columns.containsKey(testCase)) {
                return columns.get(testCase);
            }
            return "-";
        }
    }

    private List<String> testRows;
    private String[] testCases;
    Map<String, TestCase> testSummary;

    public TestSummary(String[] tcs) {
        testSummary = new HashMap<String, TestCase>();
        testCases = tcs;
        testRows = new LinkedList<String>();
    }

    public void mergeResult(String testField, String testCase, String resultString) {
        if (!testSummary.containsKey(testField)) {
            testSummary.put(testField, new TestCase());
            testRows.add(testField);
        }

        TestCase row = testSummary.get(testField);
        String curString = row.getTestCaseData(testCase);
        if (curString != "-") {
            curString = curString.concat("\n");
            curString = curString.concat(resultString);
        } else {
            curString = resultString;
        }
        row.setTestCaseData(testCase, curString);

        testSummary.put(testField, row);
    }

    public void setResult(String testField, String testCase, String resultString) {
        if (!testSummary.containsKey(testField)) {
            testSummary.put(testField, new TestCase());
            testRows.add(testField);
        }

        TestCase row = testSummary.get(testField);
        row.setTestCaseData(testCase, resultString);

        testSummary.put(testField, row);
    }

    public void displayResults(List<String> fields) {
        ListIterator<String> iter = fields.listIterator();

        while (iter.hasNext()) {
            String key = iter.next();
            TestCase row = testSummary.get(key);
            if (row == null) {
                continue;
            }

            StringBuilder str = new StringBuilder();

            str.append(key);

            for (String tc : testCases) {
                str.append(",").append(row.getTestCaseData(tc));
            }
            System.out.println(str);
        }
    }

    public boolean generateTestSummaryFile(String fullpath) {
        //displayResults(fields);
        boolean res = false;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fullpath));
            CSVPrinter printer = new CSVPrinter(writer, CSVFormat.EXCEL);
            // print headers
            printer.print("FIELD");
            for (int i = 0; i < testCases.length; i++) {
                printer.print(testCases[i]);
            }
            printer.println();

            // print rows
            ListIterator<String> iter = testRows.listIterator();

            while (iter.hasNext()) {
                String key = iter.next();
                TestCase row = testSummary.get(key);

                if (row == null) {
                    continue;
                }
                printer.print(key);

                for (String tc : testCases) {
                    printer.print(row.getTestCaseData(tc));
                }
                printer.println();
            }

            printer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            return res;
        }
    }
}
