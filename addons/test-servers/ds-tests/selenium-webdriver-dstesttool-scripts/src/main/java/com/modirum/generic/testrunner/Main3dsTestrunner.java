package com.modirum.generic.testrunner;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.collections.Lists;

import java.util.List;

public class Main3dsTestrunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();

        TestListenerAdapter testListenerAdapter = new TestListenerAdapter();
        testNG.addListener(testListenerAdapter);

        List<String> suites = Lists.newArrayList();
        suites.add("testng.xml");
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
