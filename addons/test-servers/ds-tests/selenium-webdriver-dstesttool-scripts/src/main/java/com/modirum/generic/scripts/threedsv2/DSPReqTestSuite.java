package com.modirum.generic.scripts.threedsv2;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.modirum.generic.scripts.common.threedsv2TestSuite;
import com.modirum.generic.utility.CSVTestParams;
import com.modirum.generic.utility.JsonData;
import com.modirum.generic.webdriver2.dstesttool.DSTestPage;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/*
 * DSPReqTestSuite - Parent class for DS PReq Test Suites
 */
public abstract class DSPReqTestSuite extends threedsv2TestSuite {
    /*
     * CSV Records data
     */
    protected Object[][] m_preqInclusionInfo;   // Stores inclusion requirements for PReq fields [test input]
    protected Object[][] m_presInclusionInfo;   // Stores inclusion requirements and default values for PRes fields [expected results]

    /*
     * PReq Json data
     */
    protected JsonData m_required = new JsonData();
    protected JsonData m_optional = new JsonData();
    protected JsonData m_conditional_opt = new JsonData();

    /*
     * Main
     */
    @Parameters({"browser", "ip_add", "port", "screenshots", "startURL", "alljson", "numFieldsPerTest", "report_type"})
    @BeforeClass(alwaysRun = true)
    public void setUp(String browser, String ip_add, String port, String screenshots, String startURL, String alljson, int numFieldsPerTest, int report_type) {
        super.setUp(browser, ip_add, port, screenshots, startURL, alljson, numFieldsPerTest, report_type);

        m_errorComponent = "D";

        // Initialize input info
        initPReqInclusionData();
        initDefaultPReqDataForTestClass();
    }

    protected void initTestClassFields() {
        m_messageType = "PReq";
    }


    protected void initExpectedData() {
        expectedData = new HashMap<String, String[]>();

        // Initialize common error codes from the errorCodesv2.csv file
        String path = m_testFilesPath + "expecteddata" + File.separator + "errorCodesv2.csv";

        CSVTestParams.getRecordsAsMapByColHeaders(expectedData, path, "ERROR CATEGORY",
                                                  new String[]{"errorCode", "errorDescription"});

        path = m_testFilesPath + "expecteddata" + File.separator + "DS_Erro.csv";
        m_erroInclusionInfo = CSVTestParams.getRecordsAsArrayByColHeaders(path,
                                                                          new String[]{"FIELD", "INCLUSION", "CONDITION", "EXPECTED VALUE"});


        initExpectedPResData();
    }

    protected void initPReqInclusionData() {
        Assert.assertTrue(m_preqInclusionInfo == null);

        String csvPath = m_testFilesPath + "inputs" + File.separator + "DS_PReq.csv";

        m_preqInclusionInfo = CSVTestParams.getRecordsAsArrayByColHeaders(csvPath,
                                                                          new String[]{"FIELD", "INCLUSION", "CONDITIONAL GROUP"});
    }

    protected void initExpectedPResData() {
        String path = m_testFilesPath + "expecteddata" + File.separator + "DS_PRes.csv";

        m_presInclusionInfo = CSVTestParams.getRecordsAsArrayByColHeaders(path,
                                                                          new String[]{"FIELD", "INCLUSION", "CONDITION", "EXPECTED VALUE"});
    }

    protected void initDefaultPReqDataForTestClass() {
        String jsonPath = m_testFilesPath + "defaultMessages" + File.separator + m_json;

        JsonData allJson = new JsonData(jsonPath);
        //-- Set Test Class specific fields
        // Set messageType to PReq
        allJson.setValue("messageType", m_messageType);

        initDefaultPReqData(allJson);
    }

    protected void initDefaultPReqData(JsonData allJson) {
        m_testGroupMap = new HashMap<String, String>();
        //-- Set default Json fields based on inclusion
        for (Object[] field : m_preqInclusionInfo) {
            String category = (String) field[1];
            String condition = (String) field[2];

            if (category.equals("-")) {
                continue;
            } else {
                String key = (String) field[0];
                JsonValue val = allJson.getJsonValue((String) field[0]);
                if (category.equals("R")) {
                    m_testGroupMap.put(key, "R");

                    // Required fields are needed for all default PReqs
                    m_required.addElement(key, val);
                    m_optional.addElement(key, val);
                    m_conditional_opt.addElement(key, val);
                } else if (category.equals("O")) {
                    m_testGroupMap.put(key, "O");
                    m_optional.addElement(key, val);
                } else if (category.equals("C")) {
                    if (condition.equals("optional")) {
                        m_testGroupMap.put(key, "C-optional");
                        m_conditional_opt.addElement(key, val);
                    } else {
                        System.out.println("[WARNING] Unknown conditional group [" + condition + "]");
                    }
                } else {
                    System.out.println("[WARNING] Unknown category [" + category + "]");
                }
            }
        }
    }

    /*
     * This function sets the column of the input CSV file that will be used for the current test suite
     */
    protected void setTestClassColumn() {
        m_testClassColumn = "INCLUSION";
    }

    protected JsonData getTestGroupJson(String field) {
        String group = m_testGroupMap.get(field);

        if (group.equals("R")) {
            return m_required;
        }
        if (group.equals("O")) {
            return m_optional;
        }
        if (group.equals("C-optional")) {
            return m_conditional_opt;
        }
        return null;
    }

    /*******************************
     * COMMON TEST METHODS
     ********************************/
    protected void _testDefaultJson(String testName, JsonData defaultJson) {
        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName, test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName, "");
        }
        m_reporter.logInfo("Starting test with no params");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] No default PReq Json info!");

        sendAndVerifyPReqNoError(softAssert, defaultJson);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertFalse(false);
    }

    protected void _testRequired_Absent(String testName, JsonData defaultJson, List<String> testFields) {
        StringBuilder paramLog = new StringBuilder();
        for (String field : testFields) {
            paramLog.append(field + ",");
        }
        paramLog.deleteCharAt(paramLog.length() - 1);

        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", "");
        }

        m_reporter.logInfo("Starting test with params [" + paramLog + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default PReq Json");
        JsonData inJson = new JsonData(defaultJson);

        for (String testField : testFields) {
            inJson.removeElement(testField);
        }

        sendAndVerifyPReqMissingElems(softAssert, inJson, testFields);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testEmpty(String testName, JsonData defaultJson, List<String> testFields) {
        StringBuilder paramLog = new StringBuilder();
        for (String field : testFields) {
            paramLog.append(field + ",");
        }
        paramLog.deleteCharAt(paramLog.length() - 1);

        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", "");
        }
        m_reporter.logInfo("Starting test with params [" + paramLog + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default PReq Json");
        JsonData inJson = new JsonData(defaultJson);

        List<String> removeFields = new LinkedList<String>();
        for (String testField : testFields) {
            if (testField.equals("messageType") || testField.equals("messageVersion")) {
                removeFields.add(testField);
                continue;
            }
            JsonValue val = inJson.getJsonValue(testField);
            if (val.isString()) {
                inJson.setValue(testField, "");
            } else if (val.isObject()) {
                inJson.setValue(testField, new JsonObject());
            } else if (val.isArray()) {
                inJson.setValue(testField, new JsonArray());
            } else {
                // shouldn't happen, but just in case
                inJson.setValue(testField, "");
            }
        }

        for (String removeField : removeFields) {
            testFields.remove(removeField);
        }

        if (testName.contains("Required")) {
            sendAndVerifyPReqMissingElems(softAssert, inJson, testFields);
        } else if (testName.contains("Optional")) {
            sendAndVerifyPReqNoError(softAssert, inJson);
        } else {
            // should not reach here
        }

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testDuplicate(String testName, JsonData defaultJson, List<String> testFields) {
        StringBuilder paramLog = new StringBuilder();
        for (String field : testFields) {
            paramLog.append(field + ",");
        }
        paramLog.deleteCharAt(paramLog.length() - 1);

        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + paramLog + "]", "");
        }
        m_reporter.logInfo("Starting test with params [" + paramLog + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default PReq Json");
        JsonData inJson = new JsonData(defaultJson);

        for (String testField : testFields) {
            JsonValue val = inJson.getJsonValue(testField);
            inJson.addElement(testField, val);
        }

        sendAndVerifyPReqDuplicateElems(softAssert, inJson, testFields);

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    protected void _testElementFormat(String testName, String testField, String testValue, String expCat) {
        try {
            Test test = this.getClass().getMethod(testName).getAnnotation(Test.class);
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + testValue + "," + expCat + "]",
                                 test.description());
        } catch (NoSuchMethodException e) {
            m_reporter.startTest("[" + m_testClassName + "] : " + testName + "[" + testValue + "," + expCat + "]", "");
        }
        m_reporter.logInfo("Starting test with params [" + testValue + "]");

        SoftAssert softAssert = new SoftAssert();

        // If no Json data was created, test should fail.
        JsonData defaultJson = getTestGroupJson(testField);
        Assert.assertNotEquals(defaultJson, null, "\n[FAILED TEST] Check Default PReq Json");
        JsonData inJson = new JsonData(defaultJson);

        inJson.setValue(testField, testValue);

        if (expCat.equals("validRes")) {
            sendAndVerifyPReqNoError(softAssert, inJson);
        } else { // error
            sendAndVerifyPReqErrorElem(softAssert, inJson, testField, expCat);
        }

        m_reporter.logInfo("End test - " + testName);

        // Assert all verifications at the end so that we don't need to restart if some fail,
        // and so that a failed verification result does not affect other verifications in the test
        softAssert.assertAll();
    }

    /*******************************
     * COMMON VERIFICATION METHODS
     ********************************/
    protected void sendAndVerifyPReqNoError(SoftAssert softAssert, JsonData inJson) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String preqString = inJson.convertToString();
        dsTestPage.setInputJsonData(preqString);

        m_reporter.logInfo("[PREQ]" + preqString);

        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        dsTestPage.getJsonResult();
        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();

        m_reporter.logInfo("[RESPONSE]" + response);

        if (!response.contains("{")) {
            softAssert.assertTrue(response.contains("{"),
                                  "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            //replyJson.showJsonString();

            String messageType = replyJson.getValueAsString("messageType");
            if (m_basicResCheckOnly) {
                softAssert.assertEquals(messageType, "PRes",
                                        "\n[FAILED TEST] Response is " + messageType + " not PRes!");
            } else {
                if (messageType.equals("PRes")) {

                    for (Object[] presField : m_presInclusionInfo) {
                        String field = (String) presField[0];
                        String inclusion = (String) presField[1];
                        String condition = (String) presField[2];
                        String[] parse = condition.split(":");
                        String val = (String) presField[3];

                        boolean isExist = replyJson.containsField(field);
                        boolean isRequired = false;

                        if (inclusion.equals("R")) {  // required field
                            isRequired = true;
                        } else if (inclusion.equals("O") ||
                                   (inclusion.equals("C") && parse[0].equals("optional"))) { // optional field
                            isRequired = false;
                        } else if (inclusion.equals("C")) { // conditional field
                            if (condition.equalsIgnoreCase("optional")) {
                                isRequired = false;
                            }
                        }
                        if (isRequired) {
                            softAssert.assertTrue(isExist, "\n[FAILED TEST] Missing required field [" + field + "]");
                        }
                        if (isExist) {
                            verifyResponseField(softAssert, val, field, inJson, replyJson);
                        }
                    }
                } else {
                    softAssert.assertEquals(messageType, "PRes",
                                            "\n[FAILED TEST] Response is " + messageType + " not PRes!");
                }
            }
        }
        dsTestPage.resetPage();
    }

    protected void sendAndVerifyPReqMissingElems(SoftAssert softAssert, JsonData inJson, List<String> testFields) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String preqString = inJson.convertToString();
        dsTestPage.setInputJsonData(preqString);

        m_reporter.logInfo("[PREQ]" + preqString);

        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();
        m_reporter.logInfo("[RESPONSE]" + response);
        if (!response.contains("{")) {
            softAssert.assertFalse(response.contains("{"),
                                   "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            String messageType = replyJson.getValueAsString("messageType");
            if (messageType.equals("Erro")) {
                // check error cases
                if (testFields.contains("messageType")) {
                    verifyErrorResponse(softAssert, inJson, replyJson, "error101", testFields);
                } else {
                    verifyErrorResponse(softAssert, inJson, replyJson, "error201", testFields);
                }
            } else {
                softAssert.assertEquals(messageType, "Erro",
                                        "\n[FAILED TEST] Response is " + messageType + " not Erro!");
            }
        }
        dsTestPage.resetPage();
    }

    protected void sendAndVerifyPReqDuplicateElems(SoftAssert softAssert, JsonData inJson, List<String> testFields) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String preqString = inJson.convertToString();
        dsTestPage.setInputJsonData(preqString);

        m_reporter.logInfo("[PREQ]" + preqString);
        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();
        m_reporter.logInfo("[RESPONSE]" + response);
        if (!response.contains("{")) {
            softAssert.assertFalse(response.contains("{"),
                                   "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            String messageType = replyJson.getValueAsString("messageType");
            if (messageType.equals("Erro")) {
                // check error cases
                verifyErrorResponse(softAssert, inJson, replyJson, "error204", testFields);
            } else {
                softAssert.assertEquals(messageType, "Erro",
                                        "\n[FAILED TEST] Response is " + messageType + " not Erro!");
            }
        }
        dsTestPage.resetPage();
    }

    protected void sendAndVerifyPReqErrorElem(SoftAssert softAssert, JsonData inJson, String testField, String expResult) {
        DSTestPage dsTestPage = new DSTestPage(m_driver);

        String preqString = inJson.convertToString();
        dsTestPage.setInputJsonData(preqString);

        m_reporter.logInfo("[PREQ]" + preqString);

        // Send Json to DS and check result
        dsTestPage.sendJsonToDS();

        JsonData replyJson = dsTestPage.getJsonResult();
        String response = replyJson.convertToString();
        m_reporter.logInfo("[RESPONSE]" + response.replaceAll("\n", "<br>"));
        if (!response.contains("{")) {
            softAssert.assertFalse(response.contains("{"),
                                   "\n[FAILED TEST] DS response is not a Json message! [" + response + "]");
        } else {
            String messageType = replyJson.getValueAsString("messageType");
            if (messageType.equals("Erro")) {
                // check error cases
                List<String> testFields = new LinkedList<String>();
                testFields.add(testField);
                verifyErrorResponse(softAssert, inJson, replyJson, expResult, testFields);
            } else {
                softAssert.assertEquals(messageType, "Erro",
                                        "\n[FAILED TEST] Response is " + messageType + " not Erro!");
            }
        }
        dsTestPage.resetPage();
    }

    /*******************************
     * COMMON DATA PROVIDER METHODS
     ********************************/
    protected Object[][] getPReqDataProvider(String methodName) {
        if (methodName.contains("testRequired")) {
            return CSVTestParams.createListDP(m_preqInclusionInfo, m_required.size(), m_numFieldsPerTest, "R", "");
        }
        if (methodName.contains("testOptional")) {
            return CSVTestParams.createListDP(m_preqInclusionInfo, m_optional.size(), m_numFieldsPerTest, "O",
                                              ""); // gets both "O-default" and "O-priorauth"
        }
        // The following are testConditional sub-test groups
        if (methodName.contains("Opt")) {
            return CSVTestParams.createListDP(m_preqInclusionInfo, m_conditional_opt.size(), m_numFieldsPerTest, "C",
                                              "optional");
        }
        return null;
    }
}
