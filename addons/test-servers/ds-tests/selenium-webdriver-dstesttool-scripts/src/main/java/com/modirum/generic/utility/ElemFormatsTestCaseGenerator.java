package com.modirum.generic.utility;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * ElemFormatsTestCaseGenerator - This class generates test cases for testing element formats
 */
public class ElemFormatsTestCaseGenerator {
    Map<String, String[]> m_elemFormatInfo;

    public void initElemFormatInfo() {
        Assert.assertTrue(m_elemFormatInfo == null);

        String csvPath = System.getProperty("user.dir") + File.separator + "test-files" + File.separator + "inputs" +
                         File.separator + "threeDS2_ElemFormats.csv";

        m_elemFormatInfo = CSVTestParams.getRecordsAsMapByColHeaders(m_elemFormatInfo, csvPath, "FIELD",
                                                                     new String[]{"TEST ACTION", "TYPE", "LENGTH", "VALID VALUES", "INVALID VALUES", "INVALID VALUES RESULT", "WHITESPACE RESULT", "NUMBERS RESULT", "ALPHA RESULT", "SYMBOL RESULT", "CYRILLIC RESULT", "CHINESE RESULT", "GREEK RESULT"});
    }

    public Collection<Object[]> getEmailLengthTestCases(String action, String length, String defaultValue) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        String[] range = length.split("-");
        if (range.length == 2) { // min-max
            int min = Integer.parseInt(range[0]);
            int max = Integer.parseInt(range[1]);

            // MIN
            StringBuilder minString = new StringBuilder();
            String[] email = defaultValue.split("@");

            if (email.length < 2) {
                // invalid default value
                return dp;
            }

            if (min > 3) {
                if (min < email[0].length()) {
                    minString.append(email[0].substring(0, min - 2)).append("@").append(email[1].charAt(0));
                }
                dp.add(new String[]{action, "[LENGTH = MIN]", minString.toString(), "validRes"});
            }

            // MAX
            StringBuilder maxString = new StringBuilder();
            maxString.append(email[0]);

            int defValLength = email[0].length();
            int maxNameLength = max - (email[1].length() + 1);
            for (int len = maxString.length(); (maxNameLength) - len >= defValLength; len += defValLength) {
                maxString.append(email[0]);
            }
            if (maxNameLength - maxString.length() > 0) {
                maxString.append(defaultValue.substring(0, maxNameLength - maxString.length()));
            }
            maxString.append("@").append(email[1]);

            dp.add(new String[]{action, "[LENGTH = MAX]", maxString.toString(), "validRes"});
            dp.add(new String[]{action, "[LENGTH > MAX]", maxString.append(
                    defaultValue.charAt(0)).toString(), "error203"});
        }

        return dp;
    }

    public Collection<Object[]> getURLLengthTestCases(String action, String length, String defaultValue) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        String[] range = length.split("-");
        if (range.length == 2) { // min-max
            int min = Integer.parseInt(range[0]);
            int max = Integer.parseInt(range[1]);

            // MIN
            StringBuilder minString = new StringBuilder();
            minString.append("http://");

            if (min > 1) {
                dp.add(new String[]{action, "[LENGTH < MIN]", minString.deleteCharAt(min).toString(), "error203"});
            }
            // MAX
            StringBuilder maxString = new StringBuilder();
            maxString.append("http://");

            int i = defaultValue.indexOf(":");
            defaultValue = defaultValue.substring(i + 3); // start after "http://"

            int defValLength = defaultValue.length();
            for (int len = maxString.length(); max - len >= defValLength; len += defValLength) {
                maxString.append(defaultValue);
            }
            if (max - maxString.length() > 0) {
                maxString.append(defaultValue.substring(0, max - maxString.length()));
            }

            dp.add(new String[]{action, "[LENGTH = MAX]", maxString.toString(), "validRes"});
            dp.add(new String[]{action, "[LENGTH > MAX]", maxString.append(
                    defaultValue.charAt(0)).toString(), "error203"});
        }

        return dp;
    }


    public Collection<Object[]> getLengthTestCases(String action, String length, String defaultValue) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        String[] range = length.split("-");
        if (range.length == 2) { // min-max
            int min = Integer.parseInt(range[0]);
            int max = Integer.parseInt(range[1]);

            // MIN
            StringBuilder minString = new StringBuilder();
            minString.append(defaultValue.substring(0, min));

            dp.add(new String[]{action, "[LENGTH = MIN]", minString.toString(), "validRes"});
            if (min > 1) {
                dp.add(new String[]{action, "[LENGTH < MIN]", minString.deleteCharAt(min - 1).toString(), "error203"});
            }
            // MAX
            StringBuilder maxString = new StringBuilder();

            int defValLength = defaultValue.length();
            for (int len = 0; max - len >= defValLength; len += defValLength) {
                maxString.append(defaultValue);
            }
            maxString.append(defaultValue.substring(0, max - maxString.length()));

            dp.add(new String[]{action, "[LENGTH = MAX]", maxString.toString(), "validRes"});
            dp.add(new String[]{action, "[LENGTH > MAX]", maxString.append(
                    defaultValue.charAt(0)).toString(), "error203"});
        } else {
            StringBuilder testValue = new StringBuilder();
            int len = Integer.parseInt(length);
            if (len > 1) {
                testValue.append(defaultValue.substring(0, Integer.parseInt(length) - 1));
                dp.add(new String[]{action, "[LENGTH < FIXED SIZE]", testValue.toString(), "error203"});
            }
            testValue = new StringBuilder();
            testValue.append(defaultValue).append(testValue.charAt(0));
            dp.add(new String[]{action, "[LENGTH > FIXED SIZE]", testValue.toString(), "error203"});
        }

        return dp;
    }

    public Collection<Object[]> getValidValuesTestCases(String action, String validValues) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        if (validValues.equals("ANY") || validValues.equals("URL") || validValues.contains("RFC") ||
            validValues.contains("DATE") || validValues.equals("BASE64") || validValues.equals("NUMBERS")) {
            return dp;
        }

        String[] parsedList = validValues.split("[|]");
        for (String val : parsedList) {
            dp.add(new String[]{action, "[VALID VALUE]", val, "validRes"});
        }

        return dp;
    }

    public Collection<Object[]> getInvalidValuesTestCases(String action, String invalidValues, String expRes) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        String[] parsedList = invalidValues.split("[|]");
        for (String val : parsedList) {
            if (val.contains("-") && val.indexOf("-") != 0) {
                String[] parsedValRange = val.split("-");
                if (StringUtils.isNumeric(parsedValRange[0]) && StringUtils.isNumeric(parsedValRange[1])) {
                    int minRange = Integer.parseInt(parsedValRange[0]);
                    int maxRange = Integer.parseInt(parsedValRange[1]);
                    int midRange = (minRange + ((maxRange - minRange) / 2));
                    String midRangeVal = Integer.toString(midRange);

                    dp.add(new String[]{action, "[INVALID VALUE]", parsedValRange[0], expRes});
                    dp.add(new String[]{action, "[INVALID VALUE]", midRangeVal, expRes});
                    dp.add(new String[]{action, "[INVALID VALUE]", parsedValRange[1], expRes});
                } else {
                    dp.add(new String[]{action, "[INVALID VALUE]", val, expRes});
                }
            } else {
                dp.add(new String[]{action, "[INVALID VALUE]", val, expRes});
            }


        }

        return dp;
    }

    public int getMaxFromRange(String length) {
        if (length.contains("-")) {
            String[] parseLen = length.split("-");
            return Integer.parseInt(parseLen[1]);
        } else {
            return Integer.parseInt(length);
        }
    }

    public Collection<Object[]> getWhitespaceValuesTestCases(String action, String length, String defaultValue, String expRes) {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        StringBuilder testValue;
        int defValLen = defaultValue.length();

        int max = getMaxFromRange(length);

        // start
        testValue = new StringBuilder();
        if (defValLen == max) {
            testValue.append(defaultValue);
            testValue.setCharAt(0, ' ');
        } else {
            testValue.append(" ").append(defaultValue);
        }
        dp.add(new String[]{action, "[SPACE AT START]", testValue.toString(), expRes});

        // mid
        if (max > 2) {
            int mid = defValLen / 2;
            testValue = new StringBuilder();
            if (defValLen == max) {
                testValue.append(defaultValue);
                testValue.setCharAt(mid, ' ');
            } else {
                testValue.append(defaultValue.substring(0, mid)).append(" ").append(
                        defaultValue.substring(mid, defValLen));
            }
            dp.add(new String[]{action, "[SPACE AT MID]", testValue.toString(), expRes});
        }

        // end
        testValue = new StringBuilder();
        testValue.append(defaultValue);
        if (defValLen == max) {
            testValue.setCharAt(testValue.length() - 1, ' ');
        } else {
            testValue.append(" ");
        }
        dp.add(new String[]{action, "[SPACE AT END]", testValue.toString(), expRes});

        return dp;
    }

    public Collection<Object[]> getCharacterCheckTestCases(String action, String length, String defaultValue, String expRes, String testChars, String testName) {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        StringBuilder testValue;
        int defValLen = defaultValue.length();

        int max = getMaxFromRange(length);
        testValue = new StringBuilder();

        if (testChars.length() > max) {
            testValue.append(testChars.substring(0, max));
        } else if ((defValLen + testChars.length()) > max) {
            testValue.append(testChars.substring(0, testChars.length()));
            int rem = max - testValue.length();
            testValue.append(defaultValue.substring(0, rem));
        } else {
            testValue.append(defaultValue).append(testChars);
        }

        dp.add(new String[]{action, testName, testValue.toString(), expRes});

        return dp;
    }

    public Collection<Object[]> getNumberValuesTestCases(String action, String length, String defaultValue, String expRes) {
        return getCharacterCheckTestCases(action, length, defaultValue, expRes, "1234567890", "[NUMBERS]");
    }

    public Collection<Object[]> getAlphaValuesTestCases(String action, String length, String defaultValue, String expRes) {
        return getCharacterCheckTestCases(action, length, defaultValue, expRes, "zyxwvutsrqpo", "[ALPHA]");
    }

    public Collection<Object[]> getSymbolValuesTestCases(String action, String length, String defaultValue, String expRes) {
        return getCharacterCheckTestCases(action, length, defaultValue, expRes, "@!#$%^&*()", "[SYMBOLS]");
    }

    public Collection<Object[]> getCyrillicValuesTestCases(String action, String length, String defaultValue, String expRes) {
        return getCharacterCheckTestCases(action, length, defaultValue, expRes, "символ", "[CYRILLIC]");
    }

    public Collection<Object[]> getChineseValuesTestCases(String action, String length, String defaultValue, String expRes) {
        return getCharacterCheckTestCases(action, length, defaultValue, expRes, "字符", "[CHINESE]");
    }

    public Collection<Object[]> getGreekValuesTestCases(String action, String length, String defaultValue, String expRes) {
        return getCharacterCheckTestCases(action, length, defaultValue, expRes, "γράμμα", "[GREEK]");
    }


    public Iterator<Object[]> getElemFormatDataProvider(String testField, JsonData defaultJson) {
        String[] formatTests = m_elemFormatInfo.get(testField);
        if (formatTests == null) {
            return null;
        }

        String defaultValue = defaultJson.getValueAsString(testField);

        String action = formatTests[0];
        String type = formatTests[1];
        String length = formatTests[2];
        String validValues = formatTests[3];
        String invalidValues = formatTests[4];
        String invalidValuesRes = formatTests[5];
        String whitespaceRes = formatTests[6];
        String numbersRes = formatTests[7];
        String alphaRes = formatTests[8];
        String symbolRes = formatTests[9];
        String cyrillicRes = formatTests[10];
        String chineseRes = formatTests[11];
        String greekRes = formatTests[12];

        if (type.equals("OBJECT") || type.equals("ARRAY")) {
            return null;
        }

        Collection<Object[]> dp = new ArrayList<Object[]>();

        // LENGTH TEST CASES : MIN , MAX , MIN-1 , MAX+1 (no need to test fixed length since that is default)
        // special cases : URL, RFC5322(EMAIL)
        if (validValues.equals("URL")) {
            dp.addAll(getURLLengthTestCases(action, length, defaultValue));
        } else if (validValues.equals("RFC5322")) {
            dp.addAll(getEmailLengthTestCases(action, length, defaultValue));
        } else if (!(testField.equals("browserIP") || testField.equals(
                "browserTZ"))) { // skip length check for browserIP and browserTZ since there is a specific format
            if (!(length.equals("-") || length.equals(""))) {
                dp.addAll(getLengthTestCases(action, length, defaultValue));
            }
        }

        // VALID VALUES TEST CASES : just get all values separated by "|"
        if (!(validValues.equals("-") || validValues.equals(""))) {
            if (!testField.equals("purchaseExponent")) {
                // purchaseExponent needs to be paired with different values of purchaseCurrency to be valid,
                // so skip automatic testing for now and test manually
                dp.addAll(getValidValuesTestCases(action, validValues));
            }
        }

        // INVALID VALUES TEST CASES : get all values separated by "|"
        if ((!(invalidValues.equals("-") || invalidValues.equals(""))) &&
            (!(invalidValuesRes.equals("-") || (invalidValuesRes.equals(""))))) {
            dp.addAll(getInvalidValuesTestCases(action, invalidValues, invalidValuesRes));
        }

        // WHITESPACE TEST CASES : at start, at end, in middle -- make sure length of test value is valid
        if (!(whitespaceRes.equals("-") || whitespaceRes.equals(""))) {
            dp.addAll(getWhitespaceValuesTestCases(action, length, defaultValue, whitespaceRes));
        }

        // NUMBERS TEST CASES : replace part of default value with numbers
        if (!(numbersRes.equals("-") || numbersRes.equals(""))) {
            dp.addAll(getNumberValuesTestCases(action, length, defaultValue, numbersRes));
        }

        // ALPHA TEST CASES : replace part of default value with alpha chars
        // special cases : RFC4122 (TRANSID) --> use values outside HEX
        if (!(alphaRes.equals("-") || alphaRes.equals(""))) {
            dp.addAll(getAlphaValuesTestCases(action, length, defaultValue, alphaRes));
        }

        // SYMBOL TEST CASES : replace part of default value with symbol chars
        if (!(symbolRes.equals("-") || symbolRes.equals(""))) {
            dp.addAll(getSymbolValuesTestCases(action, length, defaultValue, symbolRes));
        }

        // CYRILLIC TEST CASES : return cyrillic value within length
        if (!(cyrillicRes.equals("-") || cyrillicRes.equals(""))) {
            dp.addAll(getCyrillicValuesTestCases(action, length, defaultValue, cyrillicRes));
        }

        // CHINESE TEST CASES : return chinese value within length
        if (!(chineseRes.equals("-") || chineseRes.equals(""))) {
            dp.addAll(getChineseValuesTestCases(action, length, defaultValue, chineseRes));
        }

        // GREEK TEST CASES : return greek value within length
        if (!(greekRes.equals("-") || greekRes.equals(""))) {
            dp.addAll(getGreekValuesTestCases(action, length, defaultValue, greekRes));
        }

        // Add other special cases as needed

        return dp.iterator();
    }
}
