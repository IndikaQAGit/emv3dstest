CREATE TABLE ds_paymentsystems (
  id INTEGER NOT NULL,
  name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  type VARCHAR(32) NULL,
  port SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT ds_ps_port UNIQUE (port)
) ENGINE = InnoDB;

CREATE TABLE ds_acquirers (
  id INTEGER NOT NULL,
  paymentSystemId INTEGER NOT NULL,
  BIN VARCHAR(12),
  name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  phone VARCHAR(32),
  email VARCHAR(128),
  address VARCHAR(255) CHARSET utf8mb4,
  city VARCHAR(128) CHARSET utf8mb4,
  country VARCHAR(5),
  status CHAR(1),
  isid TINYINT,
  PRIMARY KEY (id)
) COMMENT ='Acquirer entity definitions table name ds_acquirers' ENGINE = InnoDB;
CREATE INDEX ds_acq_bin ON ds_acquirers (BIN);

CREATE TABLE ds_acsprofiles (
  id INTEGER NOT NULL,
  issuerId INTEGER,
  paymentSystemId INTEGER NOT NULL,
  name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  URL VARCHAR(1024),
  threeDSMethodURL VARCHAR(2048),
  acsInfoInd VARCHAR(20),
  inClientCert BIGINT,
  operatorID VARCHAR(40) CHARSET utf8mb4,
  outClientCert BIGINT,
  refNo VARCHAR(150) CHARSET utf8mb4,
  setv SMALLINT,
  status CHAR(1),
  SSLProto VARCHAR(10),
  startProtocolVersion VARCHAR(12),
  endProtocolVersion VARCHAR(12),
  lastMod DATETIME(6),
  isid TINYINT,
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_acp_issuerId ON ds_acsprofiles (issuerId);

CREATE TABLE ds_auditlog (
  id BIGINT NOT NULL,
  whendate DATETIME(6),
  byuser VARCHAR(32),
  byuserId BIGINT,
  action VARCHAR(16),
  objectId BIGINT NOT NULL,
  objectClass VARCHAR(255),
  details VARCHAR(512) CHARSET utf8mb4,
  paymentSystemId INTEGER,
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_al_whendate ON ds_auditlog (whendate);

CREATE TABLE ds_card_ranges (
  id BIGINT NOT NULL,
  bin VARCHAR(19) NOT NULL,
  end VARCHAR(19),
  name VARCHAR(32) CHARSET utf8mb4,
  cardtype SMALLINT NOT NULL,
  subtype SMALLINT,
  setv SMALLINT,
  status CHAR(1),
  lastMod DATETIME(6),
  issuerId INTEGER NOT NULL,
  isid TINYINT,
  acsInfoInd VARCHAR(20),
  includeThreeDSMethodURL BIT,
  options VARCHAR(40),
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_cr_bin_index ON ds_card_ranges (bin);
CREATE INDEX ds_cr_end ON ds_card_ranges (end);
CREATE INDEX ds_cr_issuerId ON ds_card_ranges (issuerId);

CREATE TABLE ds_certdata (
  id BIGINT NOT NULL,
  name VARCHAR(50) CHARSET utf8mb4 NOT NULL,
  issuerDN VARCHAR(255) NOT NULL,
  subjectDN VARCHAR(255) NOT NULL,
  x509data VARCHAR(20000),
  issued DATETIME(6),
  expires DATETIME(6),
  checkValue VARCHAR(128),
  status CHAR(1),
  pkId BIGINT,
  PRIMARY KEY (id),
  CONSTRAINT ds_cd_sdn UNIQUE (subjectDN)
) ENGINE = InnoDB;
CREATE INDEX ds_cd_name ON ds_certdata (name);

CREATE TABLE ds_ids (
  name VARCHAR(64) NOT NULL,
  seed BIGINT NOT NULL,
  lastUpdate BIGINT NOT NULL,
  PRIMARY KEY (name)
) ENGINE = InnoDB;

CREATE TABLE ds_issuers (
  id INTEGER NOT NULL,
  BIN VARCHAR(12),
  name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  phone VARCHAR(32),
  email VARCHAR(128),
  address VARCHAR(255) CHARSET utf8mb4,
  city VARCHAR(128) CHARSET utf8mb4,
  country VARCHAR(5),
  status CHAR(1),
  attemptsACSPro INTEGER,
  isid TINYINT,
  paymentSystemId INTEGER NOT NULL,
  createdDate DATETIME(6) NOT NULL,
  lastModifiedDate DATETIME(6) NOT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_iss_bin ON ds_issuers (BIN);

CREATE TABLE ds_keydata (
  id BIGINT NOT NULL AUTO_INCREMENT,
  keyAlias VARCHAR(64),
  status VARCHAR(16) NOT NULL,
  hsmdevice_id BIGINT NOT NULL,
  wrapkey_id BIGINT DEFAULT NULL,
  keyData VARCHAR(16000),
  keyCheckValue VARCHAR(32),
  keyDate DATETIME(6) NOT NULL,
  cryptoPeriodDays INTEGER DEFAULT NULL,
  signedby1 VARCHAR(128) DEFAULT NULL,
  signedby2 VARCHAR(128) DEFAULT NULL,
  signature1 VARCHAR(1000) DEFAULT NULL,
  signature2 VARCHAR(1000) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT kd_alias_status UNIQUE (keyAlias, status)
) ENGINE = InnoDB;

CREATE TABLE ds_merchants (
  id BIGINT NOT NULL,
  paymentSystemId INTEGER NOT NULL,
  tdsServerProfileId BIGINT,
  acquirerId INTEGER,
  identifier VARCHAR(35) CHARSET utf8mb4,
  name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  phone VARCHAR(32),
  email VARCHAR(128),
  country VARCHAR(5),
  URL VARCHAR(255),
  requestorID VARCHAR(40) CHARSET utf8mb4,
  status CHAR(1),
  isid TINYINT,
  options VARCHAR(40),
  PRIMARY KEY (id),
  CONSTRAINT ds_mer_acqid UNIQUE (acquirerId, identifier)
) ENGINE = InnoDB;

CREATE TABLE ds_tdsserver_profiles (
  id BIGINT NOT NULL,
  name VARCHAR(100) CHARSET utf8mb4 NOT NULL,
  URL VARCHAR(1024),
  outClientCert BIGINT,
  inClientCert BIGINT,
  status CHAR(1),
  SSLProto VARCHAR(10),
  requestorID VARCHAR(40) CHARSET utf8mb4,
  operatorID VARCHAR(40) CHARSET utf8mb4,
  isid TINYINT,
  tdsReferenceNumberList VARCHAR(255) CHARSET utf8mb4,
  paymentSystemId INTEGER NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT ds_tdsserver_reqpsid UNIQUE (paymentSystemId, requestorID)
) ENGINE = InnoDB;
CREATE INDEX ds_tdsserver_operid ON ds_tdsserver_profiles (operatorID);

CREATE TABLE ds_settings (
  skey VARCHAR(128) NOT NULL,
  svalue VARCHAR(20480),
  comments VARCHAR(255),
  lastModified DATETIME(6),
  lastModifiedBy VARCHAR(64),
  PRIMARY KEY (skey)
) ENGINE = InnoDB;

CREATE TABLE ds_tdsmessages (
  id BIGINT NOT NULL,
  tdsrId BIGINT NOT NULL,
  messageType VARCHAR(10) NOT NULL,
  messageVersion VARCHAR(12) NOT NULL,
  sourceIP VARCHAR(128),
  destIP VARCHAR(128),
  messageDate DATETIME(6) NOT NULL,
  contents LONGTEXT CHARSET utf8mb4,
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_tdsm_tdsrId ON ds_tdsmessages (tdsrId);

CREATE TABLE ds_tdsrecords (
  id BIGINT NOT NULL,
  acquirerBIN VARCHAR(12),
  acquirerMerchantID VARCHAR(35) CHARSET utf8mb4,
  ACSOperatorID VARCHAR(40) CHARSET utf8mb4,
  ACSTransID VARCHAR(40),
  ACSRef VARCHAR(32) CHARSET utf8mb4,
  ACSURL VARCHAR(2048),
  acctNumber VARCHAR(20),
  acctNumberEnc VARCHAR(64),
  acctNumberKid BIGINT,
  acctNumberHMAC VARCHAR(64),
  acctNumberHMKid BIGINT,
  authenticationMethod VARCHAR(32),
  authenticationType VARCHAR(5),
  authenticationValue CHAR(28),
  browserIP VARCHAR(64),
  browserJavaEnabled CHAR(1),
  browserLanguage VARCHAR(8),
  browserUserAgent VARCHAR(2048) CHARSET utf8mb4,
  cardExpiryDate CHAR(4),
  deviceChannel CHAR(2),
  dsRef VARCHAR(32) CHARSET utf8mb4,
  errorCode CHAR(3),
  DSTransID VARCHAR(40),
  errorDetail VARCHAR(2048) CHARSET utf8mb4,
  ECI CHAR(2),
  issuerBIN VARCHAR(12),
  localDateStart DATETIME(6) NOT NULL,
  localDateEnd DATETIME(6),
  localStatus SMALLINT,
  localIssuerId INTEGER,
  localAcquirerId INTEGER,
  requestorID VARCHAR(40) CHARSET utf8mb4,
  requestorName VARCHAR(40) CHARSET utf8mb4,
  MCC CHAR(4),
  merchantCountry SMALLINT,
  merchantName VARCHAR(40) CHARSET utf8mb4,
  merchantRiskIndicator VARCHAR(16),
  requestorURL VARCHAR(2048) CHARSET utf8mb4,
  messageCategory CHAR(2),
  MIReferenceNumber VARCHAR(32) CHARSET utf8mb4,
  MITransID VARCHAR(40),
  MIProfileId BIGINT,
  protocol VARCHAR(32),
  purchaseAmount BIGINT,
  purchaseCurrency SMALLINT,
  purchaseExponent SMALLINT,
  purchaseDate DATETIME(6) NOT NULL,
  purchaseInstalData SMALLINT,
  recurringExpiry VARCHAR(8),
  recurringFrequency SMALLINT,
  resultsStatus CHAR(2),
  SDKAppID VARCHAR(40),
  SDKReferenceNumber VARCHAR(32) CHARSET utf8mb4,
  SDKTransID VARCHAR(40),
  transType CHAR(2),
  transStatus CHAR(1),
  transStatusReason CHAR(2),
  acquirerOptions VARCHAR(32),
  issuerOptions VARCHAR(32),
  paymentSystemId INTEGER,
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_tdsr_acqbin ON ds_tdsrecords (acquirerBIN);
CREATE INDEX ds_tdsr_amid ON ds_tdsrecords (acquirerMerchantID);
CREATE INDEX ds_tdsr_acstrid ON ds_tdsrecords (ACSTransID);
CREATE INDEX ds_tdsr_an ON ds_tdsrecords (acctNumber);
CREATE INDEX ds_tdsr_anhm ON ds_tdsrecords (acctNumberHMAC);
CREATE INDEX ds_tdsr_dstrid ON ds_tdsrecords (DSTransID);
CREATE INDEX ds_tdsr_isbin ON ds_tdsrecords (issuerBIN);
CREATE INDEX ds_tdsr_localDateStart ON ds_tdsrecords (localDateStart);
CREATE INDEX ds_tdsr_lisid ON ds_tdsrecords (localIssuerId);
CREATE INDEX ds_tdsr_lacqid ON ds_tdsrecords (localAcquirerId);
CREATE INDEX ds_tdsr_reqid ON ds_tdsrecords (requestorID);
CREATE INDEX ds_tdsr_mname ON ds_tdsrecords (merchantName);
CREATE INDEX ds_tdsr_miref ON ds_tdsrecords (MIReferenceNumber);

CREATE TABLE ds_texts (
  id INTEGER NOT NULL AUTO_INCREMENT,
  tkey VARCHAR(96) NOT NULL,
  locale VARCHAR(5) NOT NULL,
  message VARCHAR(10000) CHARSET utf8mb4,
  PRIMARY KEY (id)
) ENGINE = InnoDB;
CREATE INDEX ds_tkey ON ds_texts (tkey);

CREATE TABLE ds_users (
  id BIGINT NOT NULL,
  loginname VARCHAR(32) CHARSET utf8mb4,
  password VARCHAR(64),
  oldpasswords VARCHAR(256),
  lastPassChangeDate DATETIME(6),
  loginAttempts INTEGER,
  lockedDate DATETIME(6),
  firstName VARCHAR(64) CHARSET utf8mb4,
  lastName VARCHAR(64) CHARSET utf8mb4,
  paymentSystemId INTEGER,
  status CHAR(1),
  phone VARCHAR(32),
  email VARCHAR(255),
  locale VARCHAR(5),
  factor2Authmethod VARCHAR(16),
  factor2AuthDeviceId VARCHAR(128),
  factor2Data1 VARCHAR(255),
  factor2Data2 VARCHAR(255),
  factor2Data3 VARCHAR(255),
  factor2Data4 VARCHAR(10000),
  PRIMARY KEY (id),
  CONSTRAINT ds_us_login UNIQUE (loginname)
) ENGINE = InnoDB;
CREATE INDEX ds_us_fn ON ds_users (firstName);
CREATE INDEX ds_us_ln ON ds_users (lastName);
CREATE INDEX ds_us_f2did ON ds_users (factor2AuthDeviceId);

CREATE TABLE ds_userroles (
  user_id BIGINT NOT NULL,
  role VARCHAR(32) NOT NULL,
  PRIMARY KEY (user_id, role),
  CONSTRAINT FKbp1blgqf0h9a4ox3osn4tmkib FOREIGN KEY (user_id) REFERENCES ds_users (id)
) ENGINE = InnoDB;

CREATE TABLE ds_hsmdevice (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL,
  className VARCHAR(254) NOT NULL,
  status CHAR(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (id)
);

CREATE TABLE ds_hsmdevice_conf (
  hsmdevice_id BIGINT NOT NULL,
  dsId TINYINT DEFAULT 0,
  config VARCHAR(254) DEFAULT NULL,
  PRIMARY KEY (hsmdevice_id, dsId)
);

CREATE TABLE ds_tdsrecord_attributes (
  id VARCHAR(36) NOT NULL,
  tdsRecordId BIGINT NOT NULL,
  attr VARCHAR(64) NOT NULL,
  value VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  asciiValue VARCHAR(256) CHARACTER SET ascii DEFAULT NULL,
  createdDate DATETIME(6) NOT NULL,
  createdUser VARCHAR(64) NOT NULL,
  PRIMARY KEY(id)
);
CREATE INDEX ds_tdsr_attr_tdsrid_attr ON ds_tdsrecord_attributes (tdsRecordId, attr);
CREATE INDEX ds_tdsr_attr_created ON ds_tdsrecord_attributes (createdDate);
CREATE INDEX ds_tdsr_attr_asciiValue ON ds_tdsrecord_attributes (asciiValue);
CREATE INDEX ds_tdsr_attr_attr_value ON ds_tdsrecord_attributes (attr, value(36));

CREATE TABLE ds_init (
  initKey VARCHAR(128) NOT NULL,
  initValue VARCHAR(128) NOT NULL,
  lastModifiedBy VARCHAR(64) NOT NULL DEFAULT 'admin (10000)',
  lastModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (initKey)
) ENGINE = InnoDB;

-- add flag for showing Create Initial Admin Account page upon first login in DS Admin
INSERT INTO ds_init (initKey, initValue) VALUES ('enableInitAdmin', 'true');

-- #1211 Add payment system configuration database table 
CREATE TABLE ds_paymentsystem_conf (
  paymentSystemId INT(11) NOT NULL,
  dsId TINYINT(4) NOT NULL DEFAULT 0,
  skey VARCHAR(64) NOT NULL,
  `value` VARCHAR(256) NULL,
  comment VARCHAR(256) NULL,
  createdDate DATETIME(6) NOT NULL,
  createdUser VARCHAR(64) NOT NULL,
  lastModifiedDate DATETIME(6) NOT NULL,
  lastModifiedBy VARCHAR(64) NOT NULL,
  PRIMARY KEY (`paymentSystemId`, `skey`, `dsId`));

-- #1286 Move DS license information from ds_settings table to a dedicated ds_license table
CREATE TABLE ds_license (
  extension VARCHAR(32) DEFAULT NULL,
  licensee VARCHAR(32) DEFAULT NULL,
  expiration VARCHAR(32) NOT NULL,
  issuedDate DATETIME NOT NULL,
  licenseKey VARCHAR(20480) DEFAULT NULL,
  maxMerchants VARCHAR(32) DEFAULT NULL,
  name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
  version VARCHAR(32) DEFAULT NULL,
  id bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id));

-- #1256 Implement Reporting
CREATE TABLE ds_reports (
  id BIGINT NOT NULL,
  paymentSystemId INT(11) NOT NULL,
  status VARCHAR(1) DEFAULT 'U' NOT NULL,
  secret VARCHAR(1024) DEFAULT NULL,
  filename VARCHAR(1024) DEFAULT NULL,
  name VARCHAR(254) DEFAULT 'Unknown' NOT NULL,
  dateFrom VARCHAR(23) DEFAULT '2003-09-09 00:00:00',
  dateTo VARCHAR(23) DEFAULT '2003-09-09 00:00:00',
  createdDate DATETIME(6) NOT NULL,
  createdUser VARCHAR(64) NOT NULL,
  lastModifiedDate DATETIME(6) NOT NULL,
  lastModifiedBy VARCHAR(64) NOT NULL,
  criteria VARCHAR(1024) DEFAULT NULL,
  PRIMARY KEY (id));

CREATE INDEX ds_mdar_rptstatus_modified ON ds_reports(status,lastModifiedDate);
CREATE INDEX ds_mdar_rptstatus_psid_modified ON ds_reports(status,paymentSystemId,lastModifiedDate);
