/*
 * Copyright (C) 2010 Modirum (Copyright (C) (AZS Services Ltd donated to Modirum.)
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.modirum.ds.web.xsl.XmlDateAdapter;

/**
 * @author Andri
 */
@XmlRootElement(name = "Page", namespace = "##default")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlSeeAlso({Option.class, Label.class, WebForm.class, Error.class, Locale.class, WebFormField.class,
        PaymentMethod.class})
public class Page implements java.io.Serializable {
    private static final long serialVersionUID = -6354202116952377651L;

    Object order = null;
    String header = null;
    String trailer = null;
    Date date = null;

    String message = null;
    Object actionObject = null;
    String userName = null;
    String styleSheet = null;
    String metatags = null;
    String view = null;
    boolean loggedIn = false;
    boolean isAdmin = false;
    String charSet;
    Locale locale = null;

    String payStep;
    Object merchant;
    String title;
    String css;

    Object issuer;
    Object cardHolder;
    Object card;
    Object address;
    Object[] authdata;
    String userAgent;
    String contentType;

    java.util.List<Option> expYears = null;
    java.util.List<?> shippings = null;
    java.util.List<?> fees = null;
    java.util.List<?> payMethods;
    java.util.List<?> issuers;
    java.util.List<?> cards;
    java.util.List<Option> countries;
    java.util.List<Label> labels = null;
    java.util.List<Error> errors = null;


    protected void finalize() throws Throwable {
        if (labels != null) {
            labels.clear();
            // labels=null;
        }

        if (errors != null) {
            errors.clear();
            // errors=null;
        }

        // actionObject=null;

        super.finalize();
    }

    /**
     * Method getOrder
     *
     * @return Returns the order.
     */
    @XmlElement(required = false)
    public Object getOrder() {
        return order;
    }

    /**
     * Method setOrder
     *
     * @param order The order to set.
     */
    public void setOrder(Object order) {
        this.order = order;
    }

    /**
     * Method getDate
     *
     * @return Returns the date.
     */
    @XmlElement(required = false)
    @XmlJavaTypeAdapter(XmlDateAdapter.class)
    public java.util.Date getDate() {
        return date;
    }

    /**
     * Method setDate
     *
     * @param date The date to set.
     */
    public void setDate(java.util.Date date) {
        this.date = date;
    }

    /**
     * Method getHeader
     *
     * @return Returns the header.
     */
    public final void addLabel(Label l) {
        if (labels == null) {
            labels = new java.util.LinkedList<Label>();
        }
        labels.add(l);

    }

    /**
     * Method getHeader
     *
     * @return Returns the header.
     */
    @XmlElement(required = false)
    public final String getHeader() {
        return header;
    }

    /**
     * Method setHeader
     *
     * @param header The header to set.
     */
    public final void setHeader(String header) {
        this.header = header;
    }

    /**
     * Method getTrailer
     *
     * @return Returns the trailer.
     */
    public final String getTrailer() {
        return trailer;
    }

    /**
     * Method setTrailer
     *
     * @param trailer The trailer to set.
     */
    public final void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    @XmlElementWrapper(name = "labels", required = false)
    @XmlElement(name = "label")
    public Label[] getLabels() {
        return (labels != null ? (Label[]) labels.toArray(new Label[labels.size()]) : null);
    }

    @XmlElementWrapper(name = "errors", required = false)
    @XmlElement(name = "Error")
    public java.util.List<Error> getErrors() {
        return errors;
    }

    /**
     * Method setErrors
     *
     * @param errors The errors to set.
     */
    public void setErrors(java.util.List<Error> errors) {
        this.errors = errors;
    }

    public void addError(Error errormsg) {
        if (this.errors == null) {
            this.errors = new LinkedList<Error>();
        }
        this.errors.add(errormsg);
    }

    /**
     * Method getMessage
     *
     * @return Returns the message.
     */
    @XmlElement(required = false)
    public String getMessage() {
        return message;
    }

    /**
     * Method setMessage
     *
     * @param message The message to set.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Method getActionObjects
     *
     * @return Returns the actionObjects.
     */
    @XmlElement(required = false)
    public Object getActionObject() {
        return actionObject;
    }

    /**
     * Method setActionObjects
     *
     * @param actionObjects The actionObjects to set.
     */
    public void setActionObject(Object actionObject) {
        this.actionObject = actionObject;
    }

    /**
     * Method isLoggedIn
     *
     * @return Returns the loggedIn.
     */
    public boolean isLoggedIn() {
        return loggedIn;
    }

    /**
     * Method setLoggedIn
     *
     * @param loggedIn The loggedIn to set.
     */
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    /**
     * Method getUserName
     *
     * @return Returns the userName.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Method setUserName
     *
     * @param userName The userName to set.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Method isAdmin
     *
     * @return Returns the isAdmin.
     */
    public boolean isAdmin() {
        return isAdmin;
    }

    /**
     * Method setAdmin
     *
     * @param isAdmin The isAdmin to set.
     */
    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * Method getStyleSheet
     *
     * @return Returns the styleSheet.
     */
    @XmlElement(required = false)
    public String getStyleSheet() {
        return styleSheet;
    }

    /**
     * Method setStyleSheet
     *
     * @param styleSheet The styleSheet to set.
     */
    public void setStyleSheet(String styleSheet) {
        this.styleSheet = styleSheet;
    }

    /**
     * Method getMetatags
     *
     * @return Returns the metatags.
     */
    @XmlElement(required = false)
    public String getMetatags() {
        return metatags;
    }

    /**
     * Method setMetatags
     *
     * @param metatags The metatags to set.
     */
    public void setMetatags(String metatags) {
        this.metatags = metatags;
    }

    public String getVersion() {
        return "";
    }

    @XmlElement(required = false)
    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    @XmlElement(required = false)
    public String getCharSet() {
        return charSet;
    }

    public void setCharSet(String charSet) {
        this.charSet = charSet;
    }

    @XmlElementWrapper(name = "shippings", required = false)
    @XmlElement(name = "shipping")
    public List<?> getShippings() {
        return shippings;
    }

    public void setShippings(List<?> shippings) {
        this.shippings = shippings;
    }

    @XmlElement(required = false)
    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @XmlElementWrapper(name = "expYears", required = false)
    @XmlElement(name = "Option")
    public java.util.List<Option> getExpYears() {
        return expYears;
    }

    public void setExpYears(java.util.List<Option> expYears) {
        this.expYears = expYears;
    }

    public String getPayStep() {
        return payStep;
    }

    public void setPayStep(String payStep) {
        this.payStep = payStep;
    }

    @XmlElement(required = false)
    public Object getMerchant() {
        return merchant;
    }

    public void setMerchant(Object merchant) {
        this.merchant = merchant;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    @XmlElementWrapper(name = "paymMethods", required = false)
    @XmlElement(name = "payMethod")
    public List<?> getPayMethods() {
        return payMethods;
    }

    public void setPayMethods(List<?> payMethods) {
        this.payMethods = payMethods;
    }

    @XmlElementWrapper(name = "issuers", required = false)
    @XmlElement(name = "issuer")
    public List<?> getIssuers() {
        return issuers;
    }

    public void setIssuers(List<?> issuers) {
        this.issuers = issuers;
    }

    @XmlElement(required = false)
    public Object getIssuer() {
        return issuer;
    }

    public void setIssuer(Object issuer) {
        this.issuer = issuer;
    }

    @XmlElementWrapper(name = "cards", required = false)
    @XmlElement(name = "card")
    public List<?> getCards() {
        return cards;
    }

    public void setCards(List<?> cards) {
        this.cards = cards;
    }

    public void setLabels(java.util.List<Label> labels) {
        this.labels = labels;
    }

    @XmlElement(required = false)
    public Object getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(Object cardHolder) {
        this.cardHolder = cardHolder;
    }

    @XmlElementWrapper(name = "countries", required = false)
    @XmlElement(name = "Option")
    public java.util.List<Option> getCountries() {
        return countries;
    }

    public void setCountries(java.util.List<Option> countries) {
        this.countries = countries;
    }

    @XmlElement(required = false)
    public Object getCard() {
        return card;
    }

    public void setCard(Object card) {
        this.card = card;
    }

    @XmlElement(required = false)
    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    @XmlElementWrapper(name = "authdata", required = false)
    @XmlElement(name = "AuthData")
    public Object[] getAuthdata() {
        return authdata;
    }

    public void setAuthdata(Object[] authdata) {
        this.authdata = authdata;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
