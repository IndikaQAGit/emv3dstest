package com.modirum.ds.web.context;

import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.ContextHolder;
import com.modirum.ds.utils.Setting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Properties;

public class WebContext extends Context implements Serializable {

    private static final long serialVersionUID = 1L;

    protected transient static Logger log = LoggerFactory.getLogger(Context.class);

    private WebContextHolder wctxHolder;

    public class WebContextHolder implements ContextHolder, java.io.Serializable {

        private static final long serialVersionUID = 1L;
        private transient Integer paymentSystemId;
        protected transient HttpSession session;
        protected transient HttpServletRequest request;
        protected transient HttpServletResponse response;
        protected transient HttpServlet servlet;
        protected transient ServletContext servletContext;

        WebContextHolder() {
        }

        public Object getApplicationAttribute(String key) {

            if (servlet != null && servlet.getServletContext() != null) {
                return servlet.getServletContext().getAttribute(key);
            }
            if (servletContext != null) {
                return servletContext.getAttribute(key);
            }

            return null;
        }

        @SuppressWarnings("rawtypes")
        public Enumeration getApplicationAttributeNames() {
            if (servlet != null && servlet.getServletContext() != null) {
                return servlet.getServletContext().getAttributeNames();
            }
            if (servletContext != null) {
                return servletContext.getAttributeNames();
            }

            return null;
        }

        public String getApplicationParameter(String key) {
            String value = null;
            if (servlet != null) {
                value = servlet.getInitParameter(key);
            }

            if (value == null && servlet != null && servlet.getServletContext() != null) {
                value = servlet.getServletContext().getInitParameter(key);
            }

            if (value == null && servletContext != null) {
                value = servletContext.getInitParameter(key);
            }

            return value;
        }

        public Object getRequestAttribute(String key) {
            if (request != null && key != null) {
                try {
                    return request.getAttribute(key);
                } catch (Exception e) {
                    log.error("Warning get request atttibute failed " + e);
                }
            }
            return null;
        }

        public Object getSessionAttribute(String key) {
            if (session != null) {
                return session.getAttribute(key);
            }

            return null;
        }

        public Object removeApplicationAttribute(String key) {
            if (servlet != null) {
                Object value = servlet.getServletContext().getAttribute(key);
                servlet.getServletContext().removeAttribute(key);
                return value;
            }
            if (servletContext != null) {
                Object value = servletContext.getAttribute(key);
                servletContext.removeAttribute(key);
                return value;
            }
            return null;
        }

        public Object removeRequestAttribute(String key) {
            if (request != null) {
                try {
                    Object value = request.getAttribute(key);
                    request.removeAttribute(key);
                    return value;
                } catch (Exception e) {
                    log.error("Warning removing request atttibute failed " + e);
                }
            }
            return null;
        }

        public Object removeSessionAttribute(String key) {
            if (session != null) {
                Object value = session.getAttribute(key);
                session.removeAttribute(key);
                return value;
            }
            return null;
        }

        public void setApplicationAttribute(String key, Object value) {
            if (servlet != null) {
                servlet.getServletContext().setAttribute(key, value);
            }
            if (servletContext != null) {
                servletContext.setAttribute(key, value);
            }
        }

        public void setRequestAttribute(String key, Object value) {
            if (request != null) {
                try {
                    request.setAttribute(key, value);
                } catch (Exception e) {
                    log.error("Warning set request atttibute failed " + e);
                }
            }
        }

        public void setSessionAttribute(String key, Object value) {
            if (session != null) {
                session.setAttribute(key, value);
            }
        }

        public HttpSession getSession() {
            return session;
        }

        public void setSession(HttpSession session) {
            this.session = session;
        }

        public HttpServletRequest getRequest() {
            return request;
        }

        public void setRequest(HttpServletRequest request) {
            this.request = request;
        }

        public HttpServletResponse getResponse() {
            return response;
        }

        public void setResponse(HttpServletResponse response) {
            this.response = response;
        }

        public ServletContext getServletContext() {
            return servlet != null ? servlet.getServletContext() : null;
        }

        public HttpServlet getServlet() {
            return servlet;
        }

        public void setServlet(HttpServlet servlet) {
            this.servlet = servlet;
            if (servlet != null) {
                this.servletContext = servlet.getServletContext();
            }
        }

        public void setServletContext(ServletContext servletContext) {
            this.servletContext = servletContext;
        }

        @Override
        public Properties getParams() {
            return null;
        }

        @Override
        public void setParams(Properties params) {
            throw new RuntimeException("Unimplemented method");
        }
    }

    public WebContext() {
        this.wctxHolder = new WebContextHolder();
        this.ctxHolder = this.wctxHolder;
    }

    public WebContext(ContextHolder ctxHolder) {
        super(ctxHolder);
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        wctxHolder.paymentSystemId = paymentSystemId;
    }

    public Integer getPaymentSystemId() {
        return wctxHolder.paymentSystemId;
    }

    public void setSession(HttpSession s) {
        this.wctxHolder.session = s;
    }

    public javax.servlet.http.HttpSession getSession() {
        return this.wctxHolder.session;
    }

    /**
     * reset dbservices
     */
    public static void reset() {

    }

    public final HttpServletRequest getRequest() {
        return this.wctxHolder.request;
    }

    /**
     * Returns DS service port number which accepted the request. The port number is taken either from
     * <code>X-Forwarded-Port</code> http header if exists, or from the Request object provided by the container. This
     * is for configurations where DS is deployed behind a proxy.
     *
     * @return DS server port number.
     */
    public int getRequestServerPort() {
        String forwardedServerPort = getRequest().getHeader("X-Forwarded-Port");
        if (StringUtils.isEmpty(forwardedServerPort)) {
            return getRequest().getServerPort();
        }

        return Integer.parseInt(forwardedServerPort);
    }

    /**
     * Method setRequest
     *
     * @param request The request to set.
     */
    public final void setRequest(javax.servlet.http.HttpServletRequest request) {
        this.wctxHolder.request = request;
        this.wctxHolder.setRequestAttribute("com.modirum.ds.web.Context", this);
    }

    /**
     * @return response
     */
    public HttpServletResponse getResponse() {
        return this.wctxHolder.response;
    }

    /**
     * @param response
     */
    public void setResponse(HttpServletResponse response) {
        this.wctxHolder.response = response;
    }

    public ServletContext getServletContext() {
        if (this.wctxHolder.servlet != null) {
            return this.wctxHolder.servlet.getServletContext();
        }

        return this.wctxHolder.servletContext;
    }

    public void setServletContext(ServletContext sctx) {
        wctxHolder.setServletContext(sctx);
    }

    public HttpServlet getServlet() {
        return this.wctxHolder.servlet;
    }

    public void setServlet(HttpServlet servlet) {
        this.wctxHolder.servlet = servlet;
    }

    /**
     * Looks up a setting value. Returns true, if the setting exists and its value is "true". Otherwise - false.
     *
     * @param key Setting key to use for lookup.
     * @return True, if setting exists and is true. Otherwise - false.
     */
    public boolean isTrueSetting(String key) {
        Setting setting = getSetting(key);
        if (setting == null) {
            return false;
        }
        return Boolean.parseBoolean(setting.getValue());
    }
}
