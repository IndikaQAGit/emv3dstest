/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 21.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.utils;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Map;

import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;

public class WebUtils {

    public static String getRequestUrl(HttpServletRequest request, String charset, boolean relative) {
        String url = null;
        try {
            url = getModifiedURL(request, "xxx", null, charset, relative);
        } catch (Exception e) {
            url = request.getRequestURI();
        }
        return url;
    }

    public static String getModifiedURL(javax.servlet.http.HttpServletRequest request,
                                        String mParam, String mValue, String enc, boolean relative) throws Exception {
        String[] mParams = new String[1];
        mParams[0] = mParam;
        String[] mValues = new String[1];
        mValues[0] = mValue;

        return getModifiedURL(request, mParams, mValues, enc, relative);
    }


    @SuppressWarnings("rawtypes")
    public static String getModifiedURL(javax.servlet.http.HttpServletRequest request,
                                        String[] mParams, String[] mValues, String enc, boolean relative) throws Exception {

        String scheme = request.getScheme();
        int port = request.getServerPort();
        String portStr = "" + port;
        if ("http".equals(scheme) && port == 80) portStr = null;
        else if ("https".equals(scheme) && port == 443) portStr = null;

        StringBuffer rurl = new StringBuffer(32);
        if (!relative) {
            rurl.append(scheme)
                    .append("://")
                    .append(request.getServerName())
                    .append(portStr != null ? ":" + portStr : "");
        }
        rurl.append(request.getRequestURI());


        StringBuffer queryStringBuffer = new StringBuffer();

        // go through exsisting request params
        for (Enumeration pna = request.getParameterNames(); pna.hasMoreElements(); ) {
            String cna = (String) pna.nextElement();
            String[] cva = request.getParameterValues(cna);
            //if (cva==null) cva=null;

            // look int supplied new params
            int nmods = 0;
            for (int i = 0; cva != null && mParams != null && i < mParams.length; i++) {
                // change peameter to new walue if found
                if (mParams[i].equals(cna) && cva != null && cva.length > nmods) {
                    cva[nmods] = mValues[i];
                    mValues[i] = null;
                    nmods++;
                } // null out mod param
            } // for inner

            // construct new queryString
            // filter out if new value is null
            for (int i = 0; cva != null && i < cva.length; i++)
                if (cva[i] != null) {
                    if (queryStringBuffer.length() > 0) queryStringBuffer.append("&");
                    //.append("&#38");
                    queryStringBuffer.append(encode(cna, enc))
                            .append("=")
                            .append(encode(cva[i], enc));
                } // for inner2

        } // for	outer

        for (int i = 0; mParams != null && i < mParams.length; i++) // add new params that still there
        {
            if (mParams[i] != null && mParams[i].length() > 0 && mValues[i] != null) {
                if (queryStringBuffer.length() > 0) queryStringBuffer.append("&");
                //.append("&#38");
                queryStringBuffer
                        .append(encode(mParams[i], enc))
                        .append("=")
                        .append(encode(mValues[i], enc));
            }
        } // for

        if (queryStringBuffer != null && queryStringBuffer.length() > 0) {
            rurl.append("?" + queryStringBuffer.toString());
        }

        return rurl.toString();
    } // getModifiedURL


    public static String encode(String s) throws Exception {
        return encode(s, "ISO-8859-1");
    }

    public static String encode(String s, String enc) throws Exception {

        String res = null;
        res = URLEncoder.encode(s, enc);
        return res;
    }

    public static String encodeHTMLForForm(String s) {
        if (s == null) {
            return "";
        }
        StringBuffer to = new StringBuffer();

        for (int i = 0; i < s.length(); i++) {
            char xc = s.charAt(i);
            if (xc == '&')
                to.append("&amp;");
            else if (xc == '<')
                to.append("&lt;");
            else if (xc == '>')
                to.append("&gt;");
            else if (xc == '"')
                to.append("&quot;");
            else to.append(xc);
        }

        return to.toString();
    }

    public static String toRequestParams(Map<String, String> nameValueMap) {
        StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (String name : nameValueMap.keySet()) {
            String value = nameValueMap.get(name);
            if (Misc.isNotNullOrEmpty(value)) {
                sb.append(delimiter).append(name).append("=").append(Utils.encode(value));
                delimiter = "&";
            }
        }
        return sb.toString();
    }
}
