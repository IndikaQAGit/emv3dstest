/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.modirum.ds.utils.Utils;

@XmlType(namespace = "##default")
public class WebForm implements java.io.Serializable {

    private static final long serialVersionUID = 3995659759802444691L;
    String id = null;
    String name = null;
    String action = null;
    String method = null;
    String accept = null;
    String accept_charset = null;
    String enctype = null;

    java.util.List<WebFormField> fields = null;

    public String toRequest(boolean withurl) {
        return toRequest(withurl, false);
    }

    public String toRequest(boolean withurl, boolean encode) {

        StringBuilder req = new StringBuilder(fields != null ? fields.size() * 32 : 512);
        if (withurl) {
            req.append(getAction());
            req.append("?");
        }

        java.util.List<WebFormField> flds = getFields();
        try {
            if (this.getAccept_charset() != null) {
                req.append("_charset_=" + Utils.encode(this.getAccept_charset(), this.getAccept_charset()) + "&");
            }

            String charSet = this.getAccept_charset() != null ? this.getAccept_charset() : "UTF-8";
            for (int i = 0; flds != null && i < flds.size(); i++) {

                WebFormField fx = flds.get(i);
                if (fx != null && fx.getValue() != null) {
                    if (i > 0) {
                        req.append("&");
                    }

                    if (encode) {
                        req.append(Utils.encode(fx.getName(), charSet) + "=" + Utils.encode(fx.getValue(), charSet));
                    } else {
                        req.append(fx.getName() + "=" + fx.getValue());
                    }
                }
            } // for
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return req.toString();
    }

    /**
     * Method getAction
     *
     * @return Returns the action.
     */
    public String getAction() {
        return action;
    }

    /**
     * Method setAction
     *
     * @param action The action to set.
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Method getFields
     *
     * @return Returns the fields.
     */
    @XmlElementWrapper(name = "fields", required = false)
    @XmlElement(name = "field")
    public java.util.List<WebFormField> getFields() {
        return fields;
    }

    /**
     * Method setFields
     *
     * @param fields The fields to set.
     */
    public void setFields(java.util.List<WebFormField> fields) {
        this.fields = fields;
    }

    public void addField(WebFormField f) {
        if (this.fields == null) {
            this.fields = new java.util.LinkedList<WebFormField>();
        }
        this.fields.add(f);
    }

    /**
     * Method getId
     *
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * Method setId
     *
     * @param id The id to set.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Method getMethod
     *
     * @return Returns the method.
     */
    public String getMethod() {
        return method;
    }

    /**
     * Method setMethod
     *
     * @param method The method to set.
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Method getName
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     *
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method getAccept
     *
     * @return Returns the accept.
     */
    public String getAccept() {
        return accept;
    }

    /**
     * Method setAccept
     *
     * @param accept The accept to set.
     */
    public void setAccept(String accept) {
        this.accept = accept;
    }

    /**
     * Method getAccept_charset
     *
     * @return Returns the accept_charset.
     */
    public String getAccept_charset() {
        return accept_charset;
    }

    /**
     * Method setAccept_charset
     *
     * @param accept_charset The accept_charset to set.
     */
    public void setAccept_charset(String accept_charset) {
        this.accept_charset = accept_charset;
    }

    /**
     * Method getEnctype
     *
     * @return Returns the enctype.
     */
    public String getEnctype() {
        return enctype;
    }

    /**
     * Method setEnctype
     *
     * @param enctype The enctype to set.
     */
    public void setEnctype(String enctype) {
        this.enctype = enctype;
    }

}
