package com.modirum.ds.web.xsl;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class XmlDateAdapter extends XmlAdapter<String, Date> {

    private static final long serialVersionUID = 1L;

    final static String xmlDateFmtPattern = "yyyy-MM-dd'T'HH:mm:ss";

    public String marshal(Date object) {
        if (object != null) {
            return formatDateTime(object);
        }

        return null;
    }

    public Date unmarshal(String object) {
        if (object != null) {
            return parseDateTime(object);
        }

        return null;
    }

    private final Date parseDateTime(String date) {
        Date val = null;
        java.text.SimpleDateFormat df = poolDateFormat(null);
        try {
            val = df.parse(date);
        } catch (Exception e) {

        } finally {
            poolDateFormat(df);
        }

        return val;
    }


    private final String formatDateTime(java.util.Date date) {
        if (date == null)
            return null;

        java.text.SimpleDateFormat df = poolDateFormat(null);
        String val = df.format(date);
        poolDateFormat(df);

        return val;
    }

    private java.util.List<java.text.SimpleDateFormat> dfPool = new java.util.LinkedList<java.text.SimpleDateFormat>();

    private java.text.SimpleDateFormat poolDateFormat(java.text.SimpleDateFormat df) {
        synchronized (dfPool) {
            if (df != null) {
                if (dfPool.size() < 10)
                    dfPool.add(df);
                return null;
            }

            if (dfPool.size() > 0)
                return dfPool.get(0);
        }

        return new java.text.SimpleDateFormat(xmlDateFmtPattern);
    }
}
