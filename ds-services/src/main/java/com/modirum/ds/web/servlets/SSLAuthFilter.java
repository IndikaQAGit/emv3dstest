/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on Mar 2, 2017
 *
 */
package com.modirum.ds.web.servlets;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A filter for servlet 3 api if matched to trigger container/tomcat SSL authentication
 * so browser will start ask user to supply certifcate if any available.
 * sets tomcat7 sslsocket to want state to avoid ugly browser errors
 * accepts option init parameter loginUri where to redirect if ssl certficate was not found and authentication did not succeed
 * defaults to login/login.html.
 */
public class SSLAuthFilter implements Filter {

    static Logger log = LoggerFactory.getLogger(SSLAuthFilter.class);
    String loginUri = "login/login.html";

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        try {
            log.info("Call req.authenticate..");
            setSSLSocketToWant(resp); // it would be disirable if tomcat ispatched instead
            SkipErrorResponse fr = new SkipErrorResponse(((HttpServletResponse) resp));
            ((HttpServletRequest) req).authenticate(fr);
            log.info("After req.authenticate..");

            Object o = req.getAttribute("javax.servlet.request.X509Certificate");
            if (o == null) {
                log.info("No certificate supplied");
                resp.reset();
                ((HttpServletResponse) resp).sendRedirect(loginUri + "?error=sslcm");

                Exception bce = new java.security.AccessControlException("No certicate supplied (try restart browser if smartcard)");
                req.setAttribute("SPRING_SECURITY_LAST_EXCEPTION", bce);
                if (((HttpServletRequest) req).getSession(false) != null) {
                    ((HttpServletRequest) req).getSession(false).setAttribute("SPRING_SECURITY_LAST_EXCEPTION", bce);
                }
                return;
            }
        } catch (Throwable t) {
            log.error("Auth failed ", t);
            resp.reset();
            ((HttpServletResponse) resp).sendRedirect(loginUri + "?error=ssle");

            Exception bce = new java.security.AccessControlException("SSL authentication failed, check your certificates");
            req.setAttribute("SPRING_SECURITY_LAST_EXCEPTION", bce);
            if (((HttpServletRequest) req).getSession(false) != null) {
                ((HttpServletRequest) req).getSession(false).setAttribute("SPRING_SECURITY_LAST_EXCEPTION", bce);
            }

            return;
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void init(FilterConfig fc) {
        if (fc.getInitParameter("loginUri") != null) {
            loginUri = fc.getInitParameter("loginUri");
        }
    }

    void setSSLSocketToWant(ServletResponse resp) {
        if (resp.getClass().getName().equals("org.apache.catalina.connector.ResponseFacade")) {
            try {
                Field responseField = resp.getClass().getDeclaredField("response");
                responseField.setAccessible(true);
                Object conResponse = responseField.get(resp);
                if (conResponse != null && conResponse.getClass().getName().equals("org.apache.catalina.connector.Response")) {
                    Field coyResponseField = conResponse.getClass().getDeclaredField("coyoteResponse");
                    coyResponseField.setAccessible(true);
                    Object coyResponse = coyResponseField.get(conResponse);
                    if (coyResponse != null && coyResponse.getClass().getName().equals("org.apache.coyote.Response")) {
                        Field hookField = coyResponse.getClass().getDeclaredField("hook");
                        hookField.setAccessible(true);
                        Object hook = hookField.get(coyResponse);
                        if (hook != null && hook.getClass().getName().equals("org.apache.coyote.http11.Http11Processor")) {
                            Field sslSupportField = hook.getClass().getDeclaredField("sslSupport");
                            sslSupportField.setAccessible(true);
                            Object sslSupport = sslSupportField.get(hook);
                            if (sslSupport != null && sslSupport.getClass().getName().equals("org.apache.tomcat.util.net.jsse.JSSESupport")) {
                                Field sslSockField = sslSupport.getClass().getDeclaredField("ssl");
                                sslSockField.setAccessible(true);
                                Object sslSock = sslSockField.get(sslSupport);
                                if (sslSock != null && sslSock instanceof javax.net.ssl.SSLSocket) {
                                    ((javax.net.ssl.SSLSocket) sslSock).setWantClientAuth(true);
                                }
                            }
                        }
                    }
                }

            } catch (Throwable e) {
                if (log.isDebugEnabled()) {
                    log.warn("Tomcat SSL socket fix to want failed " + e, e);
                } else {
                    log.warn("Tomcat SSL socket fix to want failed " + e);
                }
            }
        }
    }

    static class SkipErrorResponse extends HttpServletResponseWrapper {

        public SkipErrorResponse(HttpServletResponse real) {
            super(real);
        }

        public void sendError(int sc) {
            log.info("Senderor " + sc + " skipped");
        }

        public void sendError(int sc, java.lang.String msg) {
            log.info("Senderor " + sc + " " + msg + " skipped");
        }
    }
}
