/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 07.12.2010
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.blocks;

import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "##default")
public class PaymentMethod implements java.io.Serializable {

    private static final long serialVersionUID = 1L;


    private String id;
    private String name;
    private String logoUrl;
    private String type; // card/ext

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
