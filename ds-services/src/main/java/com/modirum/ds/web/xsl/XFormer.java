/*
 * Copyright (C) 2010 Modirum  (AZS Services Ltd donated to Modirum.)
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.web.xsl;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.Utils;

public class XFormer {

    protected transient static Logger log = LoggerFactory.getLogger(XFormer.class);

    byte[] xsl = null;
    String xslSourceSid = null;
    String xmlSourceSid = null;
    TransformerFactory ctFactory = null;
    long xslLastMod = 0;
    long xslLastSize = 0;
    long created = 0;
    long lastUsed = 0;
    java.util.List<Transformer> pool;
    int bufSize = 32 * 1024;

    final static Object synch = new Object();
    static long lastClean = 0;

    public XFormer() {
        ctFactory = TransformerFactory.newInstance();
        created = System.currentTimeMillis();
        pool = new ArrayList<>(10);
    }

    Transformer getTransFormer() throws Exception {

        lastUsed = System.currentTimeMillis();
        // transformer not thread safe should be pooled..
        if (pool.size() > 0) {
            synchronized (pool) {
                if (pool.size() > 0) {
                    return pool.remove(pool.size() - 1);
                }
            }
        }
        // seems this shall be syncronized as well ans source isolated
        StreamSource cxslSource = new StreamSource(new java.io.ByteArrayInputStream(xsl));
        cxslSource.setSystemId(xslSourceSid);

        return ctFactory.newTransformer(cxslSource);
    }

    void returnTransFormer(Transformer transformer) {
        if (pool.size() < 10) {
            synchronized (pool) {
                pool.add(transformer);
            }
        }
    }

    public static Map<String, XFormer> getRendererMap(Context ctx) {
        Map<String, XFormer> map = (Map<String, XFormer>) ctx.getCtxHolder().getApplicationAttribute("rendererMap");
        if (map == null) {
            synchronized (synch) {
                map = (Map<String, XFormer>) ctx.getCtxHolder().getApplicationAttribute("rendererMap");
                if (map == null) {
                    map = new java.util.TreeMap<String, XFormer>();
                    ctx.getCtxHolder().setApplicationAttribute("rendererMap", map);
                }
            }
        }

        return map;
    }

    // clean from cache renderers not used for specified period of time
    public static int cleanNotUsed(Context ctx, int seconds) {

        long now = System.currentTimeMillis();

        if (lastClean > now + seconds * 1000 / 4) {
            return 0;
        }

        Map<String, XFormer> map = getRendererMap(ctx);
        String[] keys = null;
        synchronized (map) {
            keys = map.keySet().toArray(new String[map.size()]);
        }

        int removed = 0;
        for (String key : keys) {
            XFormer xx = map.get(key);
            if (xx != null && xx.lastUsed < now - seconds * 1000L) {
                synchronized (map) {
                    map.remove(key);
                    removed++;
                }
            }

        }
        if (removed > 0) {
            log.info("Removed " + removed + " cached renderers as not used since "
                    + new java.util.Date(now - seconds * 1000L) + " in " + (System.currentTimeMillis() - now) + "ms");
        }

        lastClean = System.currentTimeMillis();
        return removed;
    }

    public static XFormer getRenderer(String xsld, Context ctx) throws Exception {
        Map<String, XFormer> map = getRendererMap(ctx);
        XFormer rndr = map.get(xsld);

        long fmd = -1;
        long fsize = -1;
        java.io.File f = null;
        if (rndr == null || rndr.getCreated() < System.currentTimeMillis() - 30000) {
            f = new java.io.File(xsld);
            if (log.isDebugEnabled()) {
                log.debug("getrenderer Filepath=" + f.getAbsolutePath());
            }

            fmd = f.lastModified();
            fsize = f.length();
        }

        if (rndr == null || fmd > -1 && rndr.getXslLastMod() < fmd || fsize > -1 && rndr.getXslLastSize() != fsize) // ||
        {
            if (log.isInfoEnabled()) {
                log.info("Creating new renderer " + f.getAbsolutePath()
                        + " as was not cache or xsl was mod or older than current file.. ");
            }
            rndr = null;
            rndr = new XFormer();
            byte[] xsl = Utils.readFile(f);
            rndr.setXslSource(xsl, xsld);
            rndr.setXslLastMod(fmd);
            rndr.setXslLastSize(fsize);
            rndr.setXslSourceSid(xsld);

            String xslBase = f.getParentFile().getAbsolutePath();
            // org.apache.fop.configuration.Configuration.put("baseDir", xslBase);
            rndr.setXmlSourceSid(xslBase + "/");
            rndr.lastUsed = System.currentTimeMillis();
            // new org.apache.fop.apps.Options(new java.io.File(xslBase+"/userconfig.xml"));
            synchronized (map) {
                map.put(xsld, rndr);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("using cached renderer " + xsld + " filep=" + xsld);
            }
            rndr.lastUsed = System.currentTimeMillis();
        }

        return rndr;
    }

    /**
     * Method getXslLastMod
     *
     * @return Returns the xslLastMod.
     */
    public final long getXslLastMod() {
        return xslLastMod;
    }

    /**
     * Method setXslLastMod
     *
     * @param xslLastMod The xslLastMod to set.
     */
    public final void setXslLastMod(long xslLastMod) {
        this.xslLastMod = xslLastMod;
    }

    public void setXslSource(byte[] xsl, String sysid) throws Exception {
        // cxslSource = new StreamSource(new java.io.ByteArrayInputStream(xsl));
        // cxslSource.setSystemId(sysid);
        // ctransformer=ctFactory.newTransformer(cxslSource);
        synchronized (pool) {
            this.xsl = xsl;
            this.xslSourceSid = sysid;
            pool.clear();
        }
    }

    public void transformXML2HTML(byte[] xml, byte[] xsl, OutputStream out) throws Exception {
        // Construct driver
        Source xmlSource = new StreamSource(new java.io.ByteArrayInputStream(xml));
        Source xslSource = new StreamSource(new java.io.ByteArrayInputStream(xsl));
        Transformer transformer = null;
        transformer = ctFactory.newTransformer(xslSource);
        transformer.transform(xmlSource, new StreamResult(out));
        transformer = null;
    }

    public byte[] transformXML2HTML(byte[] xml) throws Exception {
        // Construct driver

        Source xmlSource = new StreamSource(new java.io.ByteArrayInputStream(xml));
        java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream(xml.length);
        Transformer t = this.getTransFormer();
        t.transform(xmlSource, new StreamResult(bos));
        this.returnTransFormer(t);
        byte[] html = bos.toByteArray();
        bos.reset();
        return html;
    }

    public void transformXML2HTMLOut(byte[] xml, OutputStream out) throws Exception {

        transformXML2HTMLOut(xml, null, out);
    }

    public void transformXML2HTMLOut(byte[] xml, String enc, OutputStream out) throws Exception {
        // Construct driver
        Source xmlSource = new StreamSource(new java.io.ByteArrayInputStream(xml));
        // bos.reset();
        Transformer t = this.getTransFormer();
        if (enc != null) {
            t.setOutputProperty(OutputKeys.ENCODING, enc);
        }
        t.transform(xmlSource, new StreamResult(out));
        this.returnTransFormer(t);
        // return bos.toByteArray();
    }

    public byte[] transformDOM2HTML(Document xmldoc) throws Exception {
        // Construct driver

        Source xmlSource = new javax.xml.transform.dom.DOMSource(xmldoc);
        java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream(bufSize);
        Transformer t = this.getTransFormer();
        t.transform(xmlSource, new StreamResult(bos));
        this.returnTransFormer(t);
        byte[] html = bos.toByteArray();
        return html;
    }

    public byte[] transformXMLSource2HTML(Source xmlSource) throws Exception {
        java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream(bufSize);
        Transformer t = this.getTransFormer();
        t.transform(xmlSource, new StreamResult(bos));
        this.returnTransFormer(t);
        byte[] html = bos.toByteArray();
        return html;
    }

    public void transformDOM2HTMLOut(Document xmldoc, String enc, OutputStream out) throws Exception {

        Transformer t = this.getTransFormer();
        if (enc != null) {
            t.setOutputProperty(OutputKeys.ENCODING, enc);
        }
        t.transform(new javax.xml.transform.dom.DOMSource(xmldoc), new StreamResult(out));

        this.returnTransFormer(t);
    }

    public void transformXMLSource2HTMLOut(Source xmlSource, OutputStream out) throws Exception {
        Transformer t = this.getTransFormer();
        t.transform(xmlSource, new StreamResult(out));
        this.returnTransFormer(t);
    }

    /**
     * Method getXslLastSize
     *
     * @return Returns the xslLastSize.
     */
    public long getXslLastSize() {
        return xslLastSize;
    }

    /**
     * Method setXslLastSize
     *
     * @param xslLastSize The xslLastSize to set.
     */
    public void setXslLastSize(long xslLastSize) {
        this.xslLastSize = xslLastSize;
    }

    /**
     * Method getCreated
     *
     * @return Returns the created.
     */
    public long getCreated() {
        return created;
    }

    /**
     * Method setCreated
     *
     * @param created The created to set.
     */
    public void setCreated(long created) {
        this.created = created;
    }

    /**
     * Method getXmlSourceSid
     *
     * @return Returns the xmlSourceSid.
     */
    public String getXslSourceSid() {
        return xslSourceSid;
    }

    /**
     * Method setXslSourceSid
     *
     * @param xslSourceSid The xmlSourceSid to set.
     */
    public void setXslSourceSid(String xslSourceSid) {
        this.xslSourceSid = xslSourceSid;
    }

    /**
     * Method getXmlSourceSid
     *
     * @return Returns the xmlSourceSid.
     */
    public String getXmlSourceSid() {
        return xmlSourceSid;
    }

    /**
     * Method setXmlSourceSid
     *
     * @param xmlSourceSid The xmlSourceSid to set.
     */
    public void setXmlSourceSid(String xmlSourceSid) {
        this.xmlSourceSid = xmlSourceSid;
    }

    public long getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(long lastUsed) {
        this.lastUsed = lastUsed;
    }

    public int getBufSize() {
        return bufSize;
    }

    public void setBufSize(int bufSize) {
        this.bufSize = bufSize;
    }

}
