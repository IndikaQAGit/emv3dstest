/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 04.04.2011
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.tools;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.SQLLoader;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Properties;

public class SqlFileLoader extends Process {

    protected transient static Logger log = LoggerFactory.getLogger(SqlFileLoader.class);

    public static void main(String[] args) {
        try {
            boolean help = false;
            String propertyFile = null;
            String sqlFile = null;
            String fileEncoding = "utf-8";

            for (int i = 0; args != null && i < args.length; i++) {
                if ("-help".equals(args[i])) {
                    help = true;
                    break;
                } else if ("-sqlfile".equals(args[i]) && args.length > i + 1) {
                    sqlFile = args[i + 1];
                } else if ("-p".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                    propertyFile = args[i + 1];
                } else if ("-e".equals(args[i]) && args.length > i + 1 && Misc.isNotNullOrEmpty(args[i + 1])) {
                    fileEncoding = args[i + 1];
                }
            }

            if (help || Misc.isNullOrEmpty(sqlFile)) {
                System.out.println("usage: sqlloader options");
                System.out.println("options:");
                System.out.println("	-sqlfile sqlfilename");
                System.out.println("	-p propertiesfilename");
                System.out.println("	-e file encoding (default utf-8)");
                return;
            }

            Properties settings = new Properties();
            settings.load(new FileInputStream(Misc.isNotNullOrEmpty(propertyFile) ? propertyFile : Importer.DEFAULT_CONF_PROPS));

            log.info("Start sql data processing...");
            PersistenceService pse = new PersistenceService();
            Process.configHibernate(settings, pse);
            ServiceLocator.getInstance(pse);
            int loaded = new SQLLoader().process(sqlFile, fileEncoding, settings);
            log.info("Sql loading complete, " + loaded + " statements executed");
            System.exit(0);
        } catch (Throwable e) {
            log.error("SQL Loader failed", e);
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
