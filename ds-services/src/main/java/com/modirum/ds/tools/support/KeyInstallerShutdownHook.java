package com.modirum.ds.tools.support;

import org.slf4j.Logger;

/**
 * A JVM shutdown hook used for KeyInstaller to perform final actions on shutdown.
 */
public class KeyInstallerShutdownHook extends Thread {

    private final Logger log;
    private boolean started;

    public KeyInstallerShutdownHook(Logger callerLog) {
        log = callerLog;
    }

    @Override
    public void run() {
        if (started) {
            log.info("Keysinstaller stopped, system user " + System.getProperty("user.name") + ", stopped logging");
        }
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
