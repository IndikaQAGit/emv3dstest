package com.modirum.ds.tools;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.model.DS;
import com.modirum.ds.model.Error;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.services.ImportService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Kostas Sakellaridis,
 * Athens Greece, http://www.modirum.com
 *
 * Created on 06-Dec-16
 *
 */
public class Importer extends Process {

    protected static Logger log = LoggerFactory.getLogger(Importer.class);

    public final static String DEFAULT_CONF_PROPS = "../conf/dstools.properties";

    private static class Hook extends Thread {

        boolean started;

        @Override
        public void run() {
            if (started) {
                log.info("DS Importer stopped, system user " + System.getProperty("user.name") + ", stopped logging");
            }
        }
    }

    public static void main(String[] args) {
        System.exit(new Importer().mainImpl(args));
    }

    private int mainImpl(String[] args) {
        long start = System.currentTimeMillis();

        String propertyFile = null;
        String importFilePath = "";
        String mode = null;
        boolean delete = true;
        boolean help = false;
        boolean validate = false;
        boolean withEnded = false;

        //Import test entity counts
        int merchantCount = 1;
        int acquirerCount = 1;
        int cardRangeCount = 1;
        int acsProfileCount = 1;
        int tdsServerProfileCount = 1;
        int issuerCount = 1;

        for (int i = 0; i < args.length; i++) {
            if ("-help".equals(args[i])) {
                help = true;
                break;
            } else if ("-props".equals(args[i]) || "-p".equals(args[i])) {
                if (i + 1 < args.length) {
                    propertyFile = args[++i];
                }
            } else if ("-file".equals(args[i])) {
                if (i + 1 < args.length) {
                    importFilePath = args[++i];

                }
            } else if ("-mode".equals(args[i])) {
                if (i + 1 < args.length) {
                    mode = args[++i];
                }
            }
            //else if ("-delete".equals(args[i]))
            //{
            //	delete = true;
            //}
            else if ("-validate".equals(args[i])) {
                validate = true;
            } else if ("-ended".equals(args[i])) {
                withEnded = true;
            } else if ("-tis".equals(args[i])) {
                if (i + 1 < args.length) {
                    issuerCount = Misc.parseInt(args[++i]);
                }
            } else if ("-tac".equals(args[i])) {
                if (i + 1 < args.length) {
                    acquirerCount = Misc.parseInt(args[++i]);
                }
            } else if ("-tmi".equals(args[i])) {
                if (i + 1 < args.length) {
                    tdsServerProfileCount = Misc.parseInt(args[++i]);
                }
            } else if ("-tm".equals(args[i])) {
                if (i + 1 < args.length) {
                    merchantCount = Misc.parseInt(args[++i]);
                }
            } else if ("-tacsp".equals(args[i])) {
                if (i + 1 < args.length) {
                    acsProfileCount = Misc.parseInt(args[++i]);
                }
            } else if ("-tcr".equals(args[i])) {
                if (i + 1 < args.length) {
                    cardRangeCount = Misc.parseInt(args[++i]);
                }
            } else {
                log.warn("Invalid parameter: " + args[i]);
            }

        }

        if ((help || Misc.isNullOrEmpty(importFilePath)) &&
            !(ImportService.Mode.EXPORT.equals(mode) || ImportService.Mode.EXPORT_TEST.equals(mode))) {
            System.out.println("-file: path to the xml file to be imported or exported (required)");

            System.out.println("-mode: update|full|export|exporttest");
            System.out.println("  update - will update existing entities and import new ones (default)");
            System.out.println("  full - updates matching and removes existing that are not included in the file");
            System.out.println("  export - export data, exporttest genereate test data");
            System.out.println("-validate to validate imput with xsd");
            System.out.println("-ended import also ended status objects");
            System.out.println("-p: path to configuration(connection) properties file.");
            return 0;
        } else if (args.length < 1) {
            System.out.println("\nDS Importer\nUse -help for command line argument description\n");
            return -1;
        }
        //Properties settingProps = new Properties();

        java.util.Properties settings = new java.util.Properties();
        // shall be before log init
        if (Misc.isNotNullOrEmpty(propertyFile)) {
            File propsFile = new File(propertyFile);
            if (!propsFile.exists()) {
                System.out.println("Properties file: " + propsFile.getAbsolutePath() + " was not found, quitting...");
                log.error("Properties file: " + propsFile.getAbsolutePath() + " was not found, quitting...");
                System.exit(-1);
            }
            try {
                settings.load(new FileInputStream(propsFile));
            } catch (IOException e) {
                log.error("Error loading properties ", e);
                System.out.println("Error loading properties " + e);
                return -5;
            }
        }
        // discard bundled settings if supplied
        else if (new File(DEFAULT_CONF_PROPS).exists()) {
            settings.clear();
            try {
                settings.load(new FileInputStream(DEFAULT_CONF_PROPS));
            } catch (IOException e) {
                log.warn("Could not locate dstools.properties", e);
                System.out.println("Could not locate dstools.properties" + e);
                return -6;
            }
        }

        String user = System.getProperty("user.name");
        Hook s = new Hook();
        s.started = true;
        Runtime.getRuntime().addShutdownHook(s);
        log.info("DS Data Importer Version " + ProductInfo.getBuildVersion() + " " +
                 DateUtil.formatDate(new java.util.Date(ProductInfo.getBuildDate()), "yyyy-MM-dd HH:mm") +
                 " started, system user " + user + ", started logging");

        if (Misc.isInt(settings.getProperty("modirum.dbId"))) {
            System.setProperty("modirum.dbId", settings.getProperty("modirum.dbId"));
        }

        if (System.getProperty("modirum.dbId") == null) {
            System.setProperty("modirum.dbId", "0");
        }

        log.info("Options: validate=" + validate + " ended=" + withEnded + " mode=" + Misc.defaultString(mode) + " file=" +
                 importFilePath + " dbid=" + System.getProperty("modirum.dbId"));

        Context ctx = new Context();
        ctx.setServerIntanceId(Utils.getLocalNonLoIp());
        ctx.getCtxHolder().setApplicationAttribute("properties", settings);

        PersistenceService pse = new PersistenceService();
        Process.configHibernate(settings, pse);
        ServiceLocator.getInstance(pse);
        ImportService importService = new ImportService();

        if (ImportService.Mode.EXPORT.equals(mode)) {
            int exportedCount = 0;
            String fdf = ("dd_MM_yy_HH_mm");
            File outFile = new File(importFilePath.isEmpty() ? ("DSExport_" + DateUtil.formatDate(new Date(), fdf) +
                                                                ".xml") : importFilePath);
            try {
                exportedCount = importService.exportEntities(outFile, validate);
                if (exportedCount > 0) {
                    log.info("Exported " + exportedCount + " entities to file " + outFile.getAbsolutePath());
                } else {
                    log.warn("No entities were exported");
                }
            } catch (Exception e) {
                log.error("Failed to export entities", e);
                return -11;
            }
            return 11;
        } else if (ImportService.Mode.EXPORT_TEST.equals(mode)) {
            DS generatedEntities = importService.generateTestEntities(issuerCount, acquirerCount, merchantCount,
                                                                      acsProfileCount, tdsServerProfileCount, cardRangeCount);
            File file = new File(importFilePath.isEmpty() ? ("testFile.xml") : importFilePath);
            OutputStream os = null;
            try {
                boolean gzip = file.getName().toLowerCase().endsWith(".gz");

                os = gzip ? new java.util.zip.GZIPOutputStream(new java.io.FileOutputStream(file), 32 * 1024,
                                                               false) : new java.io.FileOutputStream(file);
                importService.marshall(generatedEntities, os, validate);
            } catch (Exception e) {
                log.error("DE export test file failed", e);
                return -77;
            } finally {
                if (os != null) {
                    try {
                        os.flush();
                        os.close();
                    } catch (IOException e) {

                    }
                }
            }
            return 77;
        }

        File file = new File(importFilePath);
        boolean gzip = file.getName().toLowerCase().endsWith(".gz");
        if (!file.exists()) {
            log.error("File: " + file.getAbsolutePath() + " was not found, quitting...");
            return -3;
        }

        List<Error> errors = new ArrayList<>();
        InputStream fis = null;
        String runtime = null;
        boolean error = false;
        try {
            fis = new FileInputStream(file);
            if (gzip) {
                fis = new GZIPInputStream(fis);
            }
            DS toImport = importService.unmarshal(fis, errors, validate);
            for (Error err : errors) {
                log.warn(err.getDescription());
            }
            if (errors.size() > 0) {
                log.error("XSD Validation detected " + errors.size() + " errors, import will not continue..");
                error = true;

            } else {
                errors = null;
                boolean resultReportFile = "true".equals(pse.getStringSetting(DsSetting.IMPORT_GEN_RESULT_FILE.getKey()));
                Map<String, Integer> results = importService.doImport(toImport, mode, delete, withEnded, user);
                if (resultReportFile) {
                    FileOutputStream fos = null;
                    try {
                        log.info("Generating summary file: " + file.getAbsolutePath() + ".res.txt");
                        fos = new FileOutputStream(file.getAbsolutePath() + ".res.txt");
                        fos.write(("Summary of import: " + file.getAbsolutePath() + " at " +
                                   DateUtil.formatDate(new java.util.Date(start), "yyyyMMdd HH:mm:ss") + "\n").
                                                                                                                      getBytes(
                                                                                                                              StandardCharsets.UTF_8));
                        fos.write(importService.resultsToString(results).getBytes(StandardCharsets.UTF_8));

                        long ms = System.currentTimeMillis() - start;
                        long h = ms / 3600000;
                        runtime = "" + h + DateUtil.formatDate(new java.util.Date(ms), ":mm:ss,SSS", true);
                        fos.write(("Total exec time: " + runtime + " ms ").getBytes(StandardCharsets.UTF_8));
                    } catch (Exception re) {
                        log.error("Failed to save resutls file ", re);
                    } finally {
                        if (fos != null) {
                            try {
                                fos.close();
                            } catch (Exception dc) {
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            error = true;
            log.error("Error during import", e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception dc) {
                }
            }
        }
        if (runtime == null) {
            long ms = System.currentTimeMillis() - start;
            long h = ms / 3600000;
            runtime = "" + h + DateUtil.formatDate(new java.util.Date(ms), ":mm:ss,SSS", true);
        }
        log.info("Total exec time: " + runtime + " ms");
        return error ? -10 : 1;
    }
}
