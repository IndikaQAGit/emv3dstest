package com.modirum.ds.tools.support;

import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.hsm.SoftwareJCEService;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceConfService;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceService;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

public class KeyInstallerHelper {

    private static final Logger LOG = LoggerFactory.getLogger(KeyInstallerHelper.class);

    public KeyInstallerHelper() {
        // util
    }

    /**
     * Adds SoftwareJCEService to DS if it doesn't exists.
     * @param hsmDeviceService
     */
    public static void addIfAbsentSoftwareEngine(HsmDeviceService hsmDeviceService) throws Exception {
        List<HsmDevice> dbHsmDevices = hsmDeviceService.getActiveHSMDevices();
        if (dbHsmDevices == null || dbHsmDevices.isEmpty()) {
            LOG.info("Adding default Software JCE engine.");
            HsmDevice dbHsmDevice = new HsmDevice();
            dbHsmDevice.setName("Software engine");
            dbHsmDevice.setClassName(SoftwareJCEService.class.getName());
            dbHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
            hsmDeviceService.saveOrUpdate(dbHsmDevice);
            return;
        }

        boolean softwareEngineExists = false;
        for (HsmDevice dbHsm : dbHsmDevices) {
            if (SoftwareJCEService.class.getName().equals(dbHsm.getClassName())) {
                softwareEngineExists = true;
            }
        }

        if (!softwareEngineExists) {
            LOG.info("Adding default Software JCE engine.");
            HsmDevice dbHsmDevice = new HsmDevice();
            dbHsmDevice.setName("Software engine");
            dbHsmDevice.setClassName(SoftwareJCEService.class.getName());
            dbHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
            hsmDeviceService.saveOrUpdate(dbHsmDevice);
            return;
        }
    }

    public static HsmDevice promptForHsmDevice(HsmDeviceService hsmDeviceService, HsmDeviceConfService hsmDeviceConfService) throws Exception {
        List<HsmDevice> dbHsmDevices = hsmDeviceService.getActiveHSMDevices();
        System.out.println("Available HSM devices:");
        for (HsmDevice dbHsm : dbHsmDevices) {
            System.out.println(dbHsm.getId() + " = " + dbHsm.getClassName());
        }

        String hsmId = getPrompt("Enter the ID of the HSM device you want to use: ");
        HsmDevice hsmDeviceSelected = hsmDeviceService.get(Long.valueOf(hsmId));

        if (!SoftwareJCEService.class.getName().equals(hsmDeviceSelected.getClassName())) {
            HsmDeviceConf hsmDeviceConfSelected = hsmDeviceConfService.get(hsmDeviceSelected.getId());
            String isChangeHsmConfig = getPrompt("Your current HSM config: " + hsmDeviceConfSelected.getConfig() + " Do you want to change y/n?");
            if ("y".equalsIgnoreCase(isChangeHsmConfig)) {
                String hsmConfig = getPrompt("Enter HSM config string: ");

                HsmDeviceConf dbHsmDeviceConf = new HsmDeviceConf();
                dbHsmDeviceConf.setHsmDeviceId(hsmDeviceSelected.getId());
                dbHsmDeviceConf.setDsId(0);
                dbHsmDeviceConf.setConfig(hsmConfig);
                hsmDeviceConfService.saveOrUpdate(dbHsmDeviceConf);
            }
        }

        return hsmDeviceSelected;
    }

    public static void generateSdkRsaCsr(CryptoService cryptoService, PersistenceService persistenceService,
                                         Pair<Long, HSMDevice> hsmDevicePair, KeyPair sdkKeypair, String subjectDN,
                                         String signatureAlgorithm, Integer cryptoPeriod) throws Exception {
        PKCS10CertificationRequest csr = cryptoService.generateCSR(hsmDevicePair.getValue(), sdkKeypair,
                subjectDN, null, signatureAlgorithm);

        String keyStatus = KeyData.KeyStatus.pending;

        int counter = 1;
        String keyAlias = KeyService.KeyAlias.sdkRSACSR;
        while (persistenceService.getKeyDataByAlias(keyAlias, keyStatus) != null) {
            keyAlias = KeyService.KeyAlias.sdkRSACSR + "-" + counter;
            counter++;
        }

        byte[] newPrivateKeyData = sdkKeypair.getPrivate().getEncoded();
        String generatedKeyData = KeyService.combinePrivateKeyAndCSR(newPrivateKeyData, csr.getEncoded());

        StringBuilder buf = new StringBuilder(4096);
        buf.append("-----BEGIN CERTIFICATE REQUEST-----\r\n");
        String b64 = Base64.encode(csr.getEncoded(), 76);
        buf.append(b64);
        buf.append("\r\n-----END CERTIFICATE REQUEST-----");
        System.out.println("Certificate request for external CA:");
        System.out.println(buf.toString());

        KeyData sdkCsrEntry = new KeyData();
        sdkCsrEntry.setKeyAlias(keyAlias);
        sdkCsrEntry.setStatus(keyStatus);
        sdkCsrEntry.setHsmDeviceId(hsmDevicePair.getKey());
        sdkCsrEntry.setCryptoPeriodDays(cryptoPeriod);
        sdkCsrEntry.setKeyDate(new Date());
        sdkCsrEntry.setKeyData(generatedKeyData);
        persistenceService.saveOrUpdate(sdkCsrEntry);
    }

    /**
     * Lookup CA root and intermediate certificates from database and prompt user to select one of them.
     * @param persistenceService DB access service.
     * @return User selected certificate authority keydata.
     */
    public static KeyData selectCertAuthority(PersistenceService persistenceService) throws Exception {
        List<KeyData> certAuthorities = persistenceService.getKeyDatasByAlias(KeyService.KeyAlias.schemeRootCert + "%",
                KeyData.KeyStatus.working, "keyAlias", null, null, null, null);
        List<KeyData> intermediates = persistenceService.getKeyDatasByAlias(KeyService.KeyAlias.schemeIntermedCert + "%",
                KeyData.KeyStatus.working, "keyAlias", null, null, null, null);
        certAuthorities.addAll(intermediates);

        KeyData selectedCA = null;
        while (certAuthorities.size() > 1) {
            System.out.println("Available CA keys: ");
            for (int i = 0; i < certAuthorities.size(); i++) {
                KeyData kdx = certAuthorities.get(i);
                if (KeyService.parsePemPrivateKey(kdx.getKeyData()) != null) {
                    byte[] certificateBytes = Base64.decode(KeyService.parsePEMCert(kdx.getKeyData()));
                    X509Certificate[] c = KeyService.parseX509Certificates(certificateBytes, null);
                    System.out.println((i + 1) + " - " + kdx.getKeyAlias() +
                            (c != null && c.length > 0 ? " - " + c[0].getSubjectDN() : ""));
                }
            }
            String selected = getPrompt("Please enter number 1.." + certAuthorities.size() + " of which CA key to use for signing:");
            if (!Misc.isInt(selected) || Misc.parseInt(selected) < 1 || Misc.parseInt(selected) > certAuthorities.size()) {
                System.out.println("Invalid value");
            } else {
                selectedCA = certAuthorities.get(Misc.parseInt(selected) - 1);
                break;
            }
        }

        if (certAuthorities.size() == 1) {
            selectedCA = certAuthorities.get(0);
        } else if (certAuthorities.size() < 0) {
            System.out.println("No root CA keys available, install them first.");
        }

        return selectedCA;
    }

    /**
     * Prompts user to input key length.
     */
    public static int promptKeyLength(String algorithm, int defaultLength, int min, int max) {
        int keyBits = defaultLength;
        while (true) {
            String kb = getPrompt("Enter " + algorithm + " key length bits (default " + defaultLength + "): ");
            if (Misc.isNullOrEmpty(kb)) {
                break;
            }
            if (Misc.isInt(kb)) {
                if (Misc.isInRange(kb, min, max)) {
                    keyBits = Misc.parseInt(kb);
                    break;
                } else {
                    System.out.println("Enter values " + min + ".." + max);
                }
            }
        }
        return keyBits;
    }

    /**
     * Prompts user to select ROOT CA private key to use.
     */
    public static KeyData promptRootCertPrivateKey(PersistenceService persistenceService) throws Exception {
        while (true) {
            LOG.info("Enter private key id signing");
            List<KeyData> availableSchemeRoots = persistenceService.getKeyDatasByAlias(KeyService.KeyAlias.schemeRootCert,
                    KeyData.KeyStatus.working, "id", null, 0, null, null);
            for (KeyData root : availableSchemeRoots) {
                if (KeyService.parsePemPrivateKey(root.getKeyData()) != null) {
                    System.out.println(root.getId() + " - " + root.getKeyAlias() + " " +
                            DateUtil.formatDate(root.getKeyDate(), "yyyy-MM-dd HH:mm"));
                }
            }

            String signingKeyDataId = getPrompt("Enter private key id signing: ");
            try {
                return persistenceService.getKeyDataById(Misc.parseLongBoxed(signingKeyDataId));
            } catch (Exception ee) {
                LOG.error("Failed to load key " + signingKeyDataId, ee);
                System.out.println("Failed to load key " + signingKeyDataId + " exception: " + ee);
            }
        }
    }

    /**
     * Prints <code>prompt</code> to stdout. Reads line input.
     * @param prompt prompt to print
     * @return Value entered by user.
     */
    public static String getPrompt(String prompt) {
        System.out.print(prompt);

        BufferedReader br = new LineNumberReader(new InputStreamReader(System.in));
        try {
            return br.readLine();
        } catch (Exception ee) {
            throw new RuntimeException(ee);
        }
    }
}
