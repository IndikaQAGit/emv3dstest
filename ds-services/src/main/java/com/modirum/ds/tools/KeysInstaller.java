/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 03.02.2016
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.tools;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.hsm.ACOS5Service;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.hsm.KeyServiceBase;
import com.modirum.ds.db.model.CertificateData;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.KeyData.KeyStatus;
import com.modirum.ds.db.model.Persistable;
import com.modirum.ds.model.ProductInfo;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.KeyService.KeyAlias;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceConfService;
import com.modirum.ds.services.db.jdbc.core.HsmDeviceService;
import com.modirum.ds.tools.support.KeyInstallerHelper;
import com.modirum.ds.tools.support.KeyInstallerShutdownHook;
import com.modirum.ds.util.CertUtil;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.bc.BcPKCS10CertificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static com.modirum.ds.tools.support.KeyInstallerHelper.getPrompt;

/**
 * A console command processor for key management (DS Tools). Facilitates encryption keys and certificates management for DS.
 */
@Configuration
public class KeysInstaller extends Process {

    private static final Logger log = LoggerFactory.getLogger(KeysInstaller.class);

    private static final int CERT_VALIDITY_DAYS_DEFAULT = 3650;
    private static final int CRYPTO_PERIOD_INITIAL = 365;
    public static Properties settings;

    private PersistenceService persistenceService;
    private KeyService keyService;
    private CryptoService cryptoService;
    private Pair<Long, HSMDevice> hsmDevicePair; // selected HSM ID and HSM device to use.

    @Autowired
    private HsmDeviceService hsmDeviceService;

    @Autowired
    private HsmDeviceConfService hsmDeviceConfService;

    public static void main(String[] args) {
        System.out.println("DS Security model keys installer");

        // validate for root/admin user
        String user = System.getProperty("user.name");
        if ("root".equalsIgnoreCase(user) || "administrator".equalsIgnoreCase(user)) {
            System.out.println("DS Security model keys installer should not be run under " + user + " user account");
            return;
        }

        // process injection based on settings
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        KeysInstaller keysInstaller = context.getBean(KeysInstaller.class);

        // start installer
        keysInstaller.startInstaller(args);
    }

    private void startInstaller(String[] args) {
        String user = System.getProperty("user.name");
        log.info("Keysinstaller {} {} started, system user {}, started logging", ProductInfo.getBuildVersion(),
                DateUtil.formatDate(new Date(ProductInfo.getBuildDate()), "yyyy-MM-dd HH:mm"), user);

        KeyInstallerShutdownHook shutdownHook = new KeyInstallerShutdownHook(log);
        shutdownHook.setStarted(true);
        Runtime.getRuntime().addShutdownHook(shutdownHook);

        Context ctx = new Context();
        ctx.setServerIntanceId(Utils.getLocalNonLoIp());
        ctx.getCtxHolder().setApplicationAttribute("properties", settings);
        persistenceService = new PersistenceService();
        Process.configHibernate(settings, persistenceService);

        persistenceService.getSettingById("test");
        ctx.setSettingService(persistenceService);
        keyService = ServiceLocator.getInstance(persistenceService).getKeyService();

        cryptoService = new CryptoService();

        // Backwards compatibility with acso5, probably unused legacy
        if (Arrays.binarySearch(args, "-acos5") > -1) {
            doACOS5NoDb(ctx);
            return;
        }

        try {
            hsmDevicePair = promptSelectHsmDevice();
            startHSMkeyInstallation(ctx);
        } catch (Exception e) {
            System.out.println("Error occured.");
            e.printStackTrace(System.out);
        }
    }

    public void doACOS5NoDb(Context ctx) {
        try {
            System.out.println("ACOS5 load DS masterkey only procedure.");
            char[] pin = getPassword("Enter PIN for keyfile:");

            com.modirum.ds.hsm.ACOS5Service hsms = new com.modirum.ds.hsm.ACOS5Service();
            hsms.setPin(pin);
            ctx.getCtxHolder().setApplicationAttribute(HSMDevice.class.getName(), hsms);
            installACOS5AESKey(false, true);
            System.out.println("ACOS5 load DS mastekey only procedure completed.");
        } catch (Exception e) {
            System.out.println("Error loading ACOS5 master key.");
            e.printStackTrace(System.out);
        }
    }

    private void startHSMkeyInstallation(Context ctx) throws Exception {
        List<KeyData> allKeysList = persistenceService.getKeyDataByStatus(null);
        if (allKeysList == null || allKeysList.size() < 1) {
            String migi = getPrompt("No keys installed yet, continue to initial installation flow y/n?");
            if ("y".equals(migi)) {
                doIntial(ctx);
            }
        }

        while (doHSMMenu(ctx)) {
        }

    }

    /**
     * Creates initial (minimal) setup of keys/certificates for a clean DS installation.
     */
    protected void doIntial(Context ctx) throws Exception {
        String cnt = getPrompt("Step 1: do you want to install CA root certficate? (y/n): ");
        if ("y".equals(cnt)) {
            installNewSchemeRoot(ctx);
        }

        cnt = getPrompt("Step 2: do you want to install CA intermediate certificate? (y/n): ");
        if ("y".equals(cnt)) {
            installNewSchemeIntermed(ctx);
        }

        cnt = getPrompt("Step 3: do you want to install SDK data decryption key (RSA) and certificate? (y/n): ");
        if ("y".equals(cnt)) {
            installNewSDKKeyPair(ctx);
        }
    }

    protected String generateHsmMenu() {
        HSMDevice hsms = hsmDevicePair.getValue();
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("DS model keysinstaller menu\n");

        if (hsms instanceof ACOS5Service) {
            sb.append("MI install DS master key info using existing master key in ACOS5-64\n");
            sb.append("W To wipe out DS master key in ACOS5-64\n");
            sb.append("CUP Change ACOS5-64 user/local PIN\n");
            sb.append("RUP Reset ACOS5-64 user/local PIN using PUK\n");
            sb.append("CGP Change ACOS5-64 global PIN/PUK\n");
            sb.append("SER Display ACOS5-64 inserted card serial number\n");
            sb.append("MCV Display ACOS5-64 inserted card DS master key check value\n");
        }

        sb.append("D To install new data encryption key\n");
        sb.append("R To install new root CA\n");
        sb.append("ICA To install new intermediate CA\n");
        sb.append("SDK-RSA To install SDK RSA keypair\n");
        sb.append("SLA To install (default) SSL Client auth certificate (ACS/MI)\n");
        sb.append("SLT To install additonal trusted root certicate for SSL server verification\n");
        sb.append("SCR Sign a CSR with DS CA\n");
        sb.append("SLS Generate and sign server sertficate with DS CA\n");
        sb.append("ADD To insert data directly from command line into ds_keydata table.\n");
        sb.append("========================================\n");
        sb.append("RMC Re HMAC certifcates\n");
        sb.append("m To install keys for crypto migration\n");
        sb.append("p To encrypt a config/database password\n");
        sb.append("pls Install a 256 bit secret key (for plugins)\n");
        sb.append("i display current key info\n");
        sb.append("ret set a key as retired\n");
        sb.append("obs set a key as obsolete\n");
        sb.append("dest destroy obsolete key data\n");
        sb.append("rm remove certificate\n");
        sb.append("admin create initial admin account for manager app\n");
        sb.append("sipm manage settings ip access list for manager app\n");
        sb.append("pkcv calculate a key check value of private key\n");
        sb.append("x Exit\n");

        return sb.toString();
    }

    protected boolean doHSMMenu(Context ctx) throws Exception {
        ctx.resetSettingCache();
        HSMDevice hsms = hsmDevicePair.getValue();

        System.out.print(generateHsmMenu());
        String migi = getPrompt("Your choice: ");

        if ("x".equals(migi)) {
            return false;
        } else if ("admin".equals(migi)) {
            createInitialAdminAccount(ctx);
            return true;
        } else if ("sipm".equals(migi)) {
            setManagerIpAccessList();
            return true;
        } else if ("W".equals(migi) && hsms instanceof ACOS5Service) {
            wipeOutAcos5MasterKey(hsms);
            return true;
        } else if ("CUP".equals(migi) && hsms instanceof ACOS5Service) {
            setAcos5LocalPin(hsms);
            return true;
        } else if ("RUP".equals(migi) && hsms instanceof ACOS5Service) {
            resetAcos5PinUsingPuk(hsms);
            return true;
        } else if ("CGP".equals(migi) && hsms instanceof ACOS5Service) {
            setAcos5GlobalPinOrPuk(hsms);
            return true;
        } else if ("SER".equals(migi) && hsms instanceof ACOS5Service) {
            showAcos5SerialNumber(hsms);
            return true;
        } else if ("MCV".equals(migi) && hsms instanceof ACOS5Service) {
            showAcos5MasterKeyCheckValue(hsms);
            return true;
        } else if ("i".equals(migi)) {
            displayKeyInfo(ctx);
            return true;
        } else if ("dest".equals(migi)) {
            destroyObsoleteKeyData(ctx);
            return true;
        } else if ("p".equals(migi)) {
            encryptDatabasePassword(hsms);
            return true;
        } else if ("pls".equals(migi)) {
            installPluginKey(hsms, ctx);
            return true;
        } // z
        else if ("z".equals(migi)) {
            return installZipFilePassword(hsms);
        } // z
        else if ("SCR".equals(migi)) {
            return signCSR(ctx);
        } else if ("D".equals(migi)) {
            installNewDataKey(ctx);
            return true;
        } else if ("R".equals(migi)) {
            return installNewSchemeRoot(ctx);
        } else if ("ICA".equals(migi)) {
            return installNewSchemeIntermed(ctx);
        } else if ("SDK-RSA".equals(migi)) {
            return installNewSDKKeyPair(ctx);
        } else if ("SLA".equals(migi)) {
            return installSSLAuthPair(hsms, ctx);
        } else if ("SLT".equals(migi)) {
            return installeNewTrustedRoot();
        } else if ("SLS".equals(migi)) {
            return generateAndExportSSLServerCert(ctx);
        } else if ("ADD".equals(migi)) {
            return addKeyDataFromPrompt();
        } else if ("RMC".equals(migi)) {
            return hmacCertificates();
        } else if ("rm".equals(migi)) {
            return removeCertifcateData();
        } else if ("obs".equals(migi) || "ret".equals(migi)) {
            setKeyStatus(ctx, "obs".equals(migi) ? KeyData.KeyStatus.obsolete : KeyData.KeyStatus.retired);
            return true;
        } else if ("m".equals(migi)) {
            doMigMenu(ctx);
            return true;
        } // migration keys
        else if ("pkcv".equals(migi)) {
            calculatePrivateKeyCheckValue();
            return true;
        } // pkcv

        return true;
    } // method

    public static char[] getPassword(String prompt) throws Exception {
        System.out.print(prompt);
        return System.console().readPassword();

    }

    /**
     * keyskey migration. A procedure to change DS 'keys key' (HSM DEK key). Decrypts all software keys/certificate
     * private keys using old 'keys key', encrypts the keys using new 'keys key'.
     */
    protected void migHSMKey(Context ctx, KeyData hsmKeyd, HSMDevice oldHsm, HSMDevice.Key newHsmKey, HSMDevice newHsm, int cryptoPeriod) throws Exception {

        java.util.List<KeyData> l = persistenceService.getKeyDataByStatus(null);
        java.util.List<KeyData> newl = new java.util.LinkedList<KeyData>();
        KeyData mki = null;

        byte[] hsmKey = Base64.decode(hsmKeyd.getKeyData());
        java.util.Date now = new java.util.Date();

        for (KeyData kx : l) {
            if (kx.getKeyAlias().equals(KeyAlias.keysKey) || kx.getKeyAlias().equals(KeyAlias.masterkeyInfo) ||
                kx.getKeyAlias().equals(KeyAlias.signerKeystore) ||
                kx.getKeyAlias().startsWith(KeyAlias.trustedRootCert)) {
                if (kx.getKeyAlias().startsWith("obs")) {
                    continue;
                }

                if (kx.getKeyAlias().equals(KeyAlias.masterkeyInfo) && KeyStatus.working.equals(kx.getStatus())) {
                    mki = kx;
                }

                if (kx.getKeyAlias().equals(KeyAlias.keysKey) && KeyStatus.retired.equals(kx.getStatus())) {
                    kx.setKeyAlias("obs-" + kx.getKeyAlias() + "-" + DateUtil.formatDate(now, "yyMMddHHmm"));
                    newl.add(kx);
                }
                continue;
            }

            log.info("Migrating " + kx.getId() + ":" + kx.getKeyAlias() + "..");

            byte[] keyDataAesHSMNew = null;
            String newData = null;
            if (kx.getKeyAlias().startsWith(KeyAlias.schemeRootCert) ||
                kx.getKeyAlias().startsWith(KeyAlias.sdkECKey) ||
                kx.getKeyAlias().startsWith(KeyAlias.sdkRSAKey) ||
                kx.getKeyAlias().startsWith(KeyAlias.clientAuthCert) ||
                kx.getKeyAlias().startsWith(KeyAlias.schemeIntermedCert)) {

                String b64pkDataEnc = KeyService.parsePemPrivateKey(kx.getKeyData());
                String b64cert = KeyService.parsePEMCert(kx.getKeyData());
                if (b64pkDataEnc != null) {
                    byte[] keyDataAes = oldHsm.decryptData(hsmKey, Base64.decode(b64pkDataEnc));
                    keyDataAesHSMNew = newHsm.encryptData(newHsmKey.getValue(), keyDataAes);
                    keyDataAesHSMNew = KeyService.combineEncryptedPrivateKeyAndCert(keyDataAesHSMNew, b64cert != null ? Base64.decode(
                            b64cert.getBytes(StandardCharsets.ISO_8859_1)) : null).getBytes(
                            StandardCharsets.ISO_8859_1);
                    newData = new String(keyDataAesHSMNew, StandardCharsets.ISO_8859_1);
                } else {
                    continue;
                }
            } else {
                byte[] keyDataAes = oldHsm.decryptData(hsmKey, Base64.decode(kx.getKeyData()));
                keyDataAesHSMNew = newHsm.encryptData(newHsmKey.getValue(), keyDataAes);
                newData = Base64.encode(keyDataAesHSMNew);
            }

            kx.setKeyData(newData);
            newl.add(kx);
            log.info("OK " + kx.getId() + ":" + kx.getKeyAlias() + " migrated..");
        }

        hsmKeyd.setStatus(KeyData.KeyStatus.retired);
        newl.add(hsmKeyd);

        KeyData newHsmKeyd = new KeyData();
        newHsmKeyd.setStatus(KeyData.KeyStatus.working);
        newHsmKeyd.setKeyAlias(KeyService.KeyAlias.keysKey);
        newHsmKeyd.setHsmDeviceId(hsmDevicePair.getKey());
        newHsmKeyd.setKeyCheckValue(HexUtils.toString(newHsmKey.getCheckValue()));
        newHsmKeyd.setKeyData(Base64.encode(newHsmKey.getValue()));
        newHsmKeyd.setKeyDate(new java.util.Date());
        newHsmKeyd.setStatus(KeyData.KeyStatus.working);
        newHsmKeyd.setCryptoPeriodDays(cryptoPeriod);

        newl.add(newHsmKeyd);

        if (oldHsm != newHsm) {
            if (mki != null) {
                mki.setStatus(KeyData.KeyStatus.retired);
                newl.add(mki);
            }
        }

        persistenceService.saveOrUpdate(newl, 0, newl.size(), true); // commit all in once
        ctx.resetSettingCache();
    }

    private void printAvailableHsmDevices() {
        List<HsmDevice> dbHsmDevices = hsmDeviceService.getActiveHSMDevices();
        System.out.println("List of registered HSM devices:");
        for (HsmDevice dbHsm : dbHsmDevices) {
            System.out.println("HSM device ID: " + dbHsm.getId() + " - " + dbHsm.getClassName());
        }
    }

    /**
     * Prompts the user for which HSM to use. Returns selected HSM ID - HSM instance pair.
     */
    private Pair<Long, HSMDevice> promptSelectHsmDevice() throws Exception {
        // Need software engine anyway for client/server TLS certificates
        KeyInstallerHelper.addIfAbsentSoftwareEngine(hsmDeviceService);

        printAvailableHsmDevices();

        String isAddHsm = getPrompt("Do you want to add another HSM Device class (y/n)? ");
        if ("y".equalsIgnoreCase(isAddHsm)) {
            String hsmDeviceClass = getPrompt("Enter HSM Devices impl. class (default: "
                    + com.modirum.ds.hsm.HSMR9000Service.class.getName() + "):");
            // save HSM
            HsmDevice dbHsmDevice = new HsmDevice();
            dbHsmDevice.setName("Thales HSM");
            dbHsmDevice.setClassName(Misc.isNotNullOrEmpty(hsmDeviceClass)
                    ? hsmDeviceClass : com.modirum.ds.hsm.HSMR9000Service.class.getName());
            dbHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
            hsmDeviceService.saveOrUpdate(dbHsmDevice);

            // save HSM conf
            String example = "host=127.0.0.1;port=1500;tcp=true;lmkkcv=2A56FA";
            String hsmConfig = getPrompt("Enter HSM config string (example '" + example + "'):");

            HsmDeviceConf dbHsmDeviceConf = new HsmDeviceConf();
            dbHsmDeviceConf.setHsmDeviceId(dbHsmDevice.getId());
            dbHsmDeviceConf.setDsId(DSModel.HsmDeviceConf.dsId.DEFAULT_DS);
            dbHsmDeviceConf.setConfig(hsmConfig);
            hsmDeviceConfService.saveOrUpdate(dbHsmDeviceConf);
        }

        // Pick hsm to use
        HsmDevice hsmDeviceSelected = KeyInstallerHelper.promptForHsmDevice(hsmDeviceService, hsmDeviceConfService);

        System.out.println("Intializing HSM device...");
        HSMDevice hsms = hsmDeviceService.getInitializedHSMDevice(hsmDeviceSelected.getId());
        if (Misc.isNullOrEmpty(hsms)) {
            throw new RuntimeException("Error initializing HSM device, HSM security model cant be established.");
        }

        System.out.println("Using HSM device ID: " + hsmDeviceSelected.getId() + " - " + hsms.getClass().getName());
        return new ImmutablePair<>(hsmDeviceSelected.getId(), hsms);
    }

    protected String genMigMenu(HSMDevice hsms) {
        StringBuilder sb = new StringBuilder();
        sb.append("Migration menu:\n");
        sb.append("1) Migrate Keys Key (HSM Data) key\n");
        sb.append("2) Install new HMAC key\n");
        sb.append("3) Install new Data encryption key\n");
        if ("ACOS5Service".equals(hsms.getClass().getSimpleName())) {
            sb.append("4) Migrate Master key in HSM");
        }
        sb.append("5) Migrate Keys Key to new Key and new HSM\n");
        sb.append("6) Install new HMACExt key for plugins\n");

        sb.append("9) Exit");
        sb.append("\n");

        return sb.toString();
    }

    protected boolean doMigMenu(Context ctx) throws Exception {
        HsmDevice hsmDeviceSelected = KeyInstallerHelper.promptForHsmDevice(hsmDeviceService, hsmDeviceConfService);
        HSMDevice hsmDeviceInstance = hsmDeviceService.getInitializedHSMDevice(hsmDeviceSelected.getId());

        while (true) {
            System.out.println(genMigMenu(hsmDeviceInstance));
            String migc = getPrompt("Your choice?");

            if ("9".equals(migc)) {
                System.out.println("Exit");
                return true;
            }

            if ("4".equals(migc) && hsmDeviceInstance instanceof ACOS5Service) {
                installACOS5AESKey(true, false);
            } else if ("1".equals(migc)) {
                migKeysKey(hsmDeviceInstance, ctx);
            } else if ("3".equals(migc)) {
                installNewDataKey(ctx);
            } // command
            else if ("2".equals(migc)) {
                installNewHMACKey(hsmDeviceInstance, KeyAlias.hmacKey, ctx);
            } // command
            else if ("6".equals(migc)) {
                installNewHMACKey(hsmDeviceInstance, KeyAlias.hmacKeyExt, ctx);
            } // command
            else if ("5".equals(migc)) {
                migKeysKeyAndHSM(hsmDeviceInstance, ctx);
            }
        }
    } // migmenu

    public static String getFirs4Last4(String s) {
        if (s == null) {
            return null;
        }
        String rs = "";
        if (s.length() > 4) {
            rs = s.substring(0, 4) + "..";
        }
        if (s.length() > 8) {
            rs = rs + s.substring(s.length() - 4);
        }

        return rs;
    }

    protected boolean installACOS5AESKey(boolean mig, boolean nodb) throws Exception {
        ACOS5Service acos5Service = (ACOS5Service) hsmDevicePair.getValue();
        KeyData keysKey = null;
        byte[] keysKeyDataPlain = null;
        if (!mig || nodb) {
            System.out.println("Start installation of AES master key into ACOS5 device");
        } else {
            System.out.println("Start migation of AES master key in ACOS5 device and recryption of keyskey");
            keysKey = persistenceService.getKeyDataByAlias(KeyAlias.keysKey, KeyData.KeyStatus.working);
            keysKeyDataPlain = acos5Service.encryptDataInHSMWithMaster(Base64.decode(keysKey.getKeyData()), false);
        }

        String part1 = null;
        String part2 = null;
        byte[] newKey = null;
        String genOrEnter = null;

        if (mig) {
            genOrEnter = "n";
        } else {
            while (true) {
                genOrEnter = getPrompt(
                        "Doy want to install existing key, exsiting key on chip or generate new key e/o/n?");
                if (("e".equals(genOrEnter) || "o".equals(genOrEnter)) || "n".equals(genOrEnter)) {
                    break;
                }
            }
        }

        if ("o".equals(genOrEnter)) {
            throw new RuntimeException("Not using master key concept any more.");
        }

        for (int i = 0; i < 2; i++) {
            getPrompt("Attention Key custodian " + (i + 1) + " ask for privacy, press enter when privacy established");

            while ("e".equals(genOrEnter)) {
                String part = getPrompt("Key custodian " + (i + 1) + " enter your key part 32 hex chars: ");
                part = part.trim();
                try {
                    if (part.length() == 32 && HexUtils.fromString(part).length == 16) {
                        if (i == 0) {
                            part1 = part;
                        }
                        if (i == 1) {
                            part2 = part;
                        }

                        getPrompt("Value ok, press enter to continue..");
                        break;
                    }
                } catch (Exception e) {
                    System.out.println("Invalid data entered");
                    e.printStackTrace();
                }
            }

            while ("n".equals(genOrEnter)) {
                if (newKey == null) {
                    newKey = KeyService.genKey(32);
                    part1 = HexUtils.toString(newKey).substring(0, 32);
                    part2 = HexUtils.toString(newKey).substring(32);
                }

                System.out.println("Keypart " + (i + 1) + ": " + (i == 0 ? part1 : part2));
                getPrompt("Key custodian " + (i + 1) + " write down your key part, press enter when done");
                break;
            }

            /*
             * if (System.getProperty("os.name").toLowerCase().indexOf("windows")>-1) Runtime.getRuntime().exec("cmd /c cls"); else
             * Runtime.getRuntime().exec("clear");
             */
            for (int ccc = 0; ccc < 10000; ccc++) {
                System.out.println();
            }

        }

        String keyOk = null;
        if ("e".equals(genOrEnter)) {
            System.out.println("Attention Key custodians 1 and 2 verify, the key check value!");
            System.out.println("Key check value of key formed from entered parts is " +
                               KeyService.calculateAESKeyCheckValue(HexUtils.fromString(part1 + part2)));

            keyOk = getPrompt("Is this correct key check value y/n?");
            if (!"y".equals(keyOk)) {
                return false;
            }
        }
        if ("n".equals(genOrEnter)) {
            System.out.println("Attention Key custodians 1 and 2 write down, the key check value!");
            System.out.println("Key check value of key formed from generated parts is " +
                               KeyService.calculateAESKeyCheckValue(HexUtils.fromString(part1 + part2)));
        }

        if (part1 != null && part2 != null) {
            byte[] keyData = HexUtils.fromString(part1 + part2);
            String kcv = KeyService.calculateAESKeyCheckValue(keyData);
            String kcv2 = null;
            int cryptoPeriod = 365;
            while (true && !nodb) {
                String crp = getPrompt(
                        "Enter keys crypto period in days (recommended 365..730 min 60 max 1460) for master key:");
                if (Misc.parseInt(crp) > 59 && Misc.parseInt(crp) <= 1460) {
                    cryptoPeriod = Misc.parseInt(crp);
                    break;
                } else {
                    System.out.println("Invalid value or out of range");
                }
            }

            StringBuffer serials = new StringBuffer();
            while (true) {
                String cardSerial = HexUtils.toString((acos5Service).getCardSerial());
                getPrompt("Enter the ACOS5 card to cardreader and when ready press enter");
                (acos5Service).installAESKey(keyData);
                log.info("New master key with checkvalue " + kcv + " to ACOS5 card with id " + cardSerial +
                         " was installed ");

                kcv2 = (acos5Service).getMasterKeyCheckValue();
                if (!kcv2.equals(kcv)) {
                    throw new RuntimeException(
                            "Device failed to produce correct key check value, expected " + kcv + " got " + kcv2);
                }

                serials.append(" " + cardSerial);
                String y = getPrompt(
                        "Do You want to install same key to another card, if yes remove current card and insert new card and enter y/n, press enter: ");
                if (!"y".equals(y)) {
                    break;
                }

            }

            if (nodb) {
                return true;
            }

            KeyData masterKeyInfo = persistenceService.getKeyDataByAlias(KeyAlias.masterkeyInfo,
                                                                         KeyData.KeyStatus.working);
            java.util.Date keyDateBackup = null;
            String keyDataBackup = null;
            String kcvBackup = null;
            if (masterKeyInfo == null) {
                masterKeyInfo = new KeyData();
            } else {
                keyDateBackup = masterKeyInfo.getKeyDate();
                keyDataBackup = masterKeyInfo.getKeyData();
                kcvBackup = masterKeyInfo.getKeyCheckValue();

            }

            masterKeyInfo.setKeyAlias(KeyService.KeyAlias.masterkeyInfo);
            masterKeyInfo.setHsmDeviceId(hsmDevicePair.getKey());
            masterKeyInfo.setKeyCheckValue(kcv);

            if (kcvBackup != null && kcvBackup.equals(kcv) && keyDataBackup != null) {
                masterKeyInfo.setKeyData(keyDataBackup + " " + serials);
            } else {
                masterKeyInfo.setKeyData("ACOS5" + serials);
            }

            // reloading same master
            if (kcvBackup != null && kcvBackup.equals(kcv) && keyDateBackup != null) {
                masterKeyInfo.setKeyDate(keyDateBackup);
            } else {
                masterKeyInfo.setKeyDate(new java.util.Date());
            }

            masterKeyInfo.setStatus(KeyData.KeyStatus.working);
            masterKeyInfo.setCryptoPeriodDays(cryptoPeriod);
            persistenceService.saveOrUpdate(masterKeyInfo);
            log.info(masterKeyInfo.getKeyAlias() + " key created, installed");

            if (mig) {
                byte[] keysKeyDataNewMaster = (acos5Service).encryptDataInHSMWithMaster(
                        keysKeyDataPlain, true);
                keysKey.setKeyData(Base64.encode(keysKeyDataNewMaster));
                persistenceService.saveOrUpdate(keysKey);
                log.info(keysKey.getKeyAlias() + " key recrypted, updated");
            }

            return true;
        }

        return false;
    }

    protected boolean signCSR(Context ctx) throws Exception {
        log.info("Sign a certificate request with CA");

        String csr = getPrompt("Enter CSR file name: ");
        File csrFile = new File(csr);
        if (!csrFile.exists()) {
            log.error("Unable to find file " + csrFile.getAbsolutePath());
            return true;
        }

        byte[] csrData = Utils.readFile(csrFile.getAbsolutePath());
        String csrDataStr = new String(csrData, StandardCharsets.UTF_8);
        if (csrDataStr.indexOf("-----BEGIN") >= 0 && csrDataStr.indexOf("CERTIFICATE REQUEST-----") > 0) {
            String[] lines = Misc.split(csrDataStr, "\n");

            StringBuilder b64Data = new StringBuilder();
            for (String line : lines) {
                if (line.indexOf("-----BEGIN ") >= 0) {
                    continue;
                }
                if (line.indexOf("-----END") >= 0) {
                    break;
                }

                b64Data.append(line + "\n");

            }

            csrData = Base64.decode(b64Data.toString());
        }

        KeyData selectedCA = KeyInstallerHelper.selectCertAuthority(persistenceService);
        if (selectedCA == null) {
            log.error("No CA Key found");
            return false;
        }

        log.info("Signing a certificate request with CA " + selectedCA.getKeyAlias());

        KeyService.getBouncyCastleProvider();
        PKCS10CertificationRequest originalCertReq = new PKCS10CertificationRequest(csrData);

        Date now = new Date();
        Date until = new Date(now.getTime() + 3600L * 24L * 365 * 1000L);

        String valid = getPrompt("Certificate will be valid until " + DateUtil.formatDate(until, "yyyy-MM-dd") + ":");
        if (valid.length() > 0) {
            until = DateUtil.parseDate(valid, "yyyy-MM-dd");
        }
        until = DateUtil.normalizeDateDayEnd(until);

        String signingKeyBase64 = KeyService.parsePemPrivateKey(selectedCA.getKeyData());
        byte[] signingPrivateKey = Base64.decode(signingKeyBase64);

        String certDataPEM = KeyService.parsePEMCert(selectedCA.getKeyData());
        byte[] cerPlain = Base64.decode(certDataPEM);
        X509Certificate[] certs = KeyService.parseX509Certificates(cerPlain);
        X509Certificate caCert = certs[0];

        String crlUrl = ctx.getStringSetting(DsSetting.DSCA_CRL_URL.getKey());

        X509v3CertificateBuilder updatedCSRbuilder =
                CertUtil.prepareUpdatedCSRbuilder(originalCertReq, until, caCert, crlUrl);

        // HSM device used for signing must correspond to _issuer private key_ HSM device
        HSMDevice signingKeyHsmDevice = hsmDeviceService.getInitializedHSMDevice(selectedCA.getHsmDeviceId());
        X509Certificate signedRequestedCertificate = cryptoService.signX509certificate(updatedCSRbuilder,
                signingKeyHsmDevice, signingPrivateKey, caCert.getPublicKey(), KeyService.sigAlgRSASHA256);

        byte[] issuerCertSubject = caCert.getSubjectX500Principal().getEncoded();
        byte[] subjectCertIssuer = signedRequestedCertificate.getIssuerX500Principal().getEncoded();
        if (!Arrays.equals(issuerCertSubject, subjectCertIssuer)) {
            throw new RuntimeException("Generated certifcate Issuer DN " + HexUtils.toString(subjectCertIssuer) +
                    " mismatch with supplied sig cert subject DN " + HexUtils.toString(issuerCertSubject));
        }

        StringBuilder buf = new StringBuilder();
        buf.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
        buf.append(Base64.encode(signedRequestedCertificate.getEncoded(), 76));
        buf.append("\r\n" + KeyService.END_CERTIFICATE);

        log.info("Signed certificate issuer " + signedRequestedCertificate.getIssuerDN() + " subject " + signedRequestedCertificate.getSubjectDN());
        log.info("\n" + buf);

        String installStore = getPrompt("Install certificate to store y/n:");
        if ("y".equals(installStore)) {
            CertificateData cd = persistenceService.getCertificateDataBySDN(
                    signedRequestedCertificate.getSubjectX500Principal().getName(X500Principal.RFC2253));
            if (cd != null) {
                String conf = getPrompt(
                        "There in certificate store already exists certificate with SDN: " + cd.getSubjectDN() +
                        " \noverwrite y/n:");
                if (!"y".equals(conf)) {
                    log.warn("Duplicate cert with SDN " + cd.getSubjectDN() + " overwrite aborted");
                    return false;
                }
            }
            if (cd == null) {
                cd = new CertificateData();
            }

            cd.setExpires(signedRequestedCertificate.getNotAfter());
            cd.setIssued(signedRequestedCertificate.getNotBefore());
            cd.setIssuerDN(signedRequestedCertificate.getIssuerX500Principal().getName(X500Principal.RFC2253));
            cd.setSubjectDN(signedRequestedCertificate.getSubjectX500Principal().getName(X500Principal.RFC2253));
            cd.setStatus(CertificateData.Status.cValid);
            cd.setX509data(buf.toString());
            cd.setName(DateUtil.formatDate(now, "yyyyMMddHHmm") + ":" +
                       Misc.cutLeft(signedRequestedCertificate.getSubjectDN().getName(), 32));

            persistenceService.save(cd);
            log.info("Certificate saved to store with id " + cd.getId() + " and name " + cd.getName());
        }

        buf.append("\r\n");
        buf.append(KeyService.BEGIN_CERTIFICATE + "\r\n");
        buf.append(Base64.encode(caCert.getEncoded(), 76));
        buf.append("\r\n" + KeyService.END_CERTIFICATE);

        String crtFile = csrFile.getAbsolutePath() + ".cer";
        Utils.saveFile(crtFile, buf.toString().getBytes(StandardCharsets.UTF_8));
        log.info("Certificate Also saved to file " + crtFile + " as chain ");

        return true;
    }

    protected void encryptDatabasePassword(final HSMDevice hsms) throws Exception {
        String password = getPrompt("Enter the database password to be encrypted: ");

        String encPassword = CryptoService.encryptConfigPassword(password, hsms);

        System.out.println("Encrypted password is:");
        System.out.println("vep://" + encPassword);
        String decryptedPassword = CryptoService.decryptConfigPassword(encPassword, hsms);
        if (decryptedPassword.equals(password)) {
            System.out.println("Password encryption successful");
            System.out.println("Decrypted password is: '" + decryptedPassword + "'");
        } else {
            System.out.println("Password encryption failed, do not use this value");
            System.out.println("Decrypted password is not equal to original:");
            System.out.println(decryptedPassword);
        }
    }

    protected void createInitialAdminAccount(final Context ctx) throws Exception {
        int au = 0; //persistenceService.countActiveUsers();
        if (au > 0) {
            System.out.println("There are already " + au +
                               " active users in system, operation not allowed if existing valid users");
            return;
        }

        String login = getPrompt("Enter login name: ");
        String email = getPrompt("Enter email: ");

        char[] pass1 = null;
        char[] pass2 = null;
        while (true) {
            pass1 = getPassword("Enter password: ");
            pass2 = getPassword("Repeat password: ");
            if (!java.util.Arrays.equals(pass1, pass2)) {
                System.out.println("Password and repeat mismatch, re enter");
            } else {
                break;
            }
        }

        User user = new User();
        user.setEmail(email);
        user.setLoginname(login);
        user.setStatus("A");
        user.setPassword(CryptoService.sha256WithUniquePredictable(pass2));
        user.setLastPassChangeDate(new Date());

        // assign all available privileges to initial admin account
        Set<String> roles = new HashSet<>();
        user.setRoles(Roles.getAllRoles());

        persistenceService.save(user);
        log.info("Initial user account " + login + " created");
    }

    protected void setManagerIpAccessList() throws Exception {
        Setting sipList = persistenceService.getSettingById(DsSetting.SETTINGS_ACCESS_LIST.getKey());
        if (sipList != null) {
            System.out.println("Current ip's allowed for settings edit:");
            System.out.println(Misc.isNullOrEmpty(sipList.getValue()) ? "none" : sipList.getValue());
        } else {
            sipList = new Setting();
            sipList.setKey(DsSetting.SETTINGS_ACCESS_LIST.getKey());
            sipList.setProcessorId((short) 0);
            sipList.setComment("Space seprated list of ip adresses eligible for settings edit");
        }

        String ar = getPrompt("Do You want to add or remove a ip to/from list: a/r");
        if ("a".equals(ar)) {
            String ipr = getPrompt("Enter ip to add: ");
            if (Misc.isNotNullOrEmpty(ipr)) {
                if (Misc.isNullOrEmpty(sipList.getValue())) {
                    sipList.setValue(ipr);
                } else {
                    sipList.setValue(sipList.getValue() + " " + ipr);
                }

                persistenceService.saveOrUpdate(sipList);
                log.info("Settings edit access list updated added new ip " + ipr);
            }

        }
        if ("r".equals(ar)) {
            String ipr = getPrompt("Enter ip to remove: ");
            if (Misc.isNotNullOrEmpty(ipr) && sipList.getValue() != null && sipList.getValue().indexOf(ipr.trim()) > -1) {
                if (Misc.isNotNullOrEmpty(sipList.getValue())) {
                    String sipLNew = Misc.replace(sipList.getValue(), ipr.trim(), "");
                    if (!sipLNew.equals(sipList.getValue())) {
                        sipList.setValue(sipLNew);
                        persistenceService.saveOrUpdate(sipList);
                        log.info("Settings edit access list updated added ip " + ipr + " removed");
                    } else {
                        log.info("Settings edit access no changers");
                    }
                } else {
                    log.info("Settings edit access list no changes, nothing to remove");
                }
            }
        }

    }

    protected void wipeOutAcos5MasterKey(final HSMDevice hsms) throws Exception {
        String sure = getPrompt("Are You sure to Wipe out DS master key y/n, all dependent data becomes useless!");
        if ("y".equals(sure)) {
            String serial = HexUtils.toString(((com.modirum.ds.hsm.ACOS5Service) hsms).getCardSerial());

            String kcv = null;
            try {
                kcv = ((com.modirum.ds.hsm.ACOS5Service) hsms).getMasterKeyCheckValue();
            } catch (Exception e) {
                log.warn("Seems no valid master key in that card, but will continue..");
            }

            ((com.modirum.ds.hsm.ACOS5Service) hsms).wipeoutAESKey();
            log.info("DS master key " + (kcv != null ? "with checkvalue " + kcv : " location ") + " in card " + serial +
                     " wiped out..");

        }
    }

    protected void setAcos5LocalPin(final HSMDevice hsms) throws Exception {
        if (!"ACOS5Service".equals(hsms.getClass().getSimpleName())) {
            return;
        }

        System.out.println("Change user PIN");
        String curPin = getPrompt("Enter current PIN:");
        String newPin = getPrompt("Enter new PIN:");

        javax.smartcardio.Card card = null;
        try {
            card = ((com.modirum.ds.hsm.ACOS5Service) hsms).initCardConnection();
            ((com.modirum.ds.hsm.ACOS5Service) hsms).selectMF(card);
            ((com.modirum.ds.hsm.ACOS5Service) hsms).selectFile(HexUtils.fromString("4100"), card);
            ((com.modirum.ds.hsm.ACOS5Service) hsms).verify((byte) 0x81, curPin.getBytes(StandardCharsets.UTF_8), card);
            ((com.modirum.ds.hsm.ACOS5Service) hsms).changePin((byte) 0x81, newPin.getBytes(StandardCharsets.UTF_8),
                                                               card);
            System.out.println("Change user PIN success");
        } catch (Exception e) {
            System.out.println("Change user PIN failed " + e);
        } finally {
            ((com.modirum.ds.hsm.ACOS5Service) hsms).disposeCardConnection(card);
        }
    }

    protected void resetAcos5PinUsingPuk(final HSMDevice hsms) throws Exception {
        while ("ACOS5Service".equals(hsms.getClass().getSimpleName())) {
            System.out.println("Reset user PIN using PUK");
            String puk = getPrompt("Enter PUK:");
            if (puk.length() < 4 || puk.length() > 8) {
                System.out.println("Invalid PUK length, must be 4..8");
                continue;
            }

            String newUsPin = getPrompt("Enter new user PIN:");
            if (newUsPin.length() < 4 || newUsPin.length() > 8) {
                System.out.println("Invalid PIN length, must be 4..8");
                continue;
            }

            javax.smartcardio.Card card = null;
            try {
                card = ((com.modirum.ds.hsm.ACOS5Service) hsms).initCardConnection();
                ((com.modirum.ds.hsm.ACOS5Service) hsms).selectMF(card);
                ((com.modirum.ds.hsm.ACOS5Service) hsms).selectFile(HexUtils.fromString("4100"), card);
                ((com.modirum.ds.hsm.ACOS5Service) hsms).selectFile(HexUtils.fromString("4101"), card); // EF1
                ((com.modirum.ds.hsm.ACOS5Service) hsms).verify((byte) 0x01, puk.getBytes(StandardCharsets.UTF_8),
                                                                card);
                // ((com.modirum.ds.hsm.ACOS5Service)hsms).resetPin((byte)0x81, puk.getBytes("UTF-8"), newUsPin.getBytes("UTF-8"));
                // ((com.modirum.ds.hsm.ACOS5Service)hsms).changePin((byte)0x81, newUsPin.getBytes("UTF-8"));
                ((com.modirum.ds.hsm.ACOS5Service) hsms).updateRecord(HexUtils.fromString("81" + // pin id
                                                                                     "88" + // counters
                                                                                     ("0" + newUsPin.length()) +
                                                                                     // pin len
                                                                                     HexUtils.toString(newUsPin.getBytes(
                                                                                             StandardCharsets.UTF_8)) +
                                                                                     // pin
                                                                                     "88" + // reset counter
                                                                                     ("0" + puk.length()) + // puk len
                                                                                     HexUtils.toString(puk.getBytes(
                                                                                             StandardCharsets.UTF_8))
                                                                                     // puk
                ), true, card);
                System.out.println("Reset user PIN success");
            } catch (Exception e) {
                System.out.println("Reset user PIN failed " + e);
            } finally {
                ((com.modirum.ds.hsm.ACOS5Service) hsms).disposeCardConnection(card);
            }

            return;
        }
    }

    protected void setAcos5GlobalPinOrPuk(final HSMDevice hsms) throws Exception {
        if (!"ACOS5Service".equals(hsms.getClass().getSimpleName())) {
            return;
        }

        System.out.println("Change global PIN/PUK");
        String curPin = getPrompt("Enter current PUK:");
        String newPin = getPrompt("Enter new PUK:");

        javax.smartcardio.Card card = null;
        try {
            card = ((com.modirum.ds.hsm.ACOS5Service) hsms).initCardConnection();
            ((com.modirum.ds.hsm.ACOS5Service) hsms).selectMF(card);
            ((com.modirum.ds.hsm.ACOS5Service) hsms).selectFile(HexUtils.fromString("4100"), card);
            ((com.modirum.ds.hsm.ACOS5Service) hsms).verify((byte) 0x01, curPin.getBytes(StandardCharsets.UTF_8), card);
            ((com.modirum.ds.hsm.ACOS5Service) hsms).changePin((byte) 0x01, newPin.getBytes(StandardCharsets.UTF_8),
                                                               card);
            System.out.println("Change global PIN/PUK success");
        } catch (Exception e) {
            System.out.println("Change global PIN/PUK failed " + e);
        } finally {
            ((com.modirum.ds.hsm.ACOS5Service) hsms).disposeCardConnection(card);
        }
    }

    protected void showAcos5SerialNumber(final HSMDevice hsms) {
        if (!"ACOS5Service".equals(hsms.getClass().getSimpleName())) {
            return;
        }
        try {
            byte[] ser = ((com.modirum.ds.hsm.ACOS5Service) hsms).getCardSerial();
            System.out.println("Inserted Card serial is: " + HexUtils.toString(ser));
        } catch (Exception e) {
            System.out.println("Get ACOS5-64 serial failed " + e);
        }
    }

    protected void showAcos5MasterKeyCheckValue(final HSMDevice hsms) {
        if (!"ACOS5Service".equals(hsms.getClass().getSimpleName())) {
            return;
        }
        try {
            String cv = ((com.modirum.ds.hsm.ACOS5Service) hsms).getMasterKeyCheckValue();
            System.out.println("Inserted Card DS masterkey check value is: " + cv);
        } catch (Exception e) {
            System.out.println("Key check value calculation failed (may be card without key loaded) " + e);
        }
    }

    protected void displayKeyInfo(final Context ctx) throws Exception {
        java.util.List<KeyData> kdl = persistenceService.getKeyDataByStatus(null);
        System.out.println("Keyinfo: ");
        for (int i = 0; i < kdl.size(); i++) {
            KeyData kdx = kdl.get(i);
            System.out.println("Alias: " + kdx.getKeyAlias());
            System.out.println("Id: " + kdx.getId());

            String data = kdx.getKeyData();
            if (KeyAlias.masterkeyInfo.equals(kdx.getKeyAlias())) {

            } else {
                data = getFirs4Last4(data);
            }

            String c = KeyService.parsePEMCert(kdx.getKeyData(), true);
            X509Certificate x509 = null;
            if (Misc.isNotNullOrEmpty(c)) {
                try {
                    X509Certificate[] x509s = KeyService.parseX509PemHeadersSafeCertificates(
                            c.getBytes(StandardCharsets.ISO_8859_1), "X509");
                    if (x509s.length > 0) {
                        x509 = x509s[0];
                    }

                } catch (Exception dc) {
                    log.warn("Failed to parse X509 " + dc);
                }
            }

            if (x509 != null) {
                System.out.println("X509 subject: \t " + x509.getSubjectDN().getName());
                System.out.println("X509 issuer: \t" + x509.getIssuerDN().getName());
                System.out.println("X509 validity: \t " +
                                   DateUtil.formatDate(x509.getNotBefore(), KeyService.signDateFormat, true) + " - " +
                                   DateUtil.formatDate(x509.getNotAfter(), KeyService.signDateFormat, true) + " GMT");

                int bits = KeyServiceBase.getPublicKeyBitLength(x509.getPublicKey());
                System.out.println("X509 " + x509.getPublicKey().getAlgorithm() + " public key" +
                                   (bits > -1 ? " bits: " + bits : ""));
            }
            System.out.println("Data: " + data);
            System.out.println("Checkvalue: " + kdx.getKeyCheckValue());
            System.out.println(
                    "Date: " + DateUtil.formatDate(kdx.getKeyDate(), KeyService.signDateFormat, true) + " GMT");
            System.out.println("Crypto period: " + kdx.getCryptoPeriodDays() + " days (" + DateUtil.formatDate(
                    new java.util.Date(kdx.getKeyDate().getTime() + 24L * 3600 * 1000L * kdx.getCryptoPeriodDays()),
                    KeyService.signDateFormat, true) + " GMT" + ")");
            System.out.println("Signer 1: " + kdx.getSignedBy1() + " sig: " + getFirs4Last4(kdx.getSignature1()));
            System.out.println("Signer 2: " + kdx.getSignedBy2() + " sig: " + getFirs4Last4(kdx.getSignature2()));
            System.out.println("Status: " + kdx.getStatus());
            System.out.println("============================================");

        }
        log.info("Key info displayed");
    }

    protected void destroyObsoleteKeyData(final Context ctx) throws Exception {
        log.info("Destroying obsolete key data..");
        java.util.List<KeyData> kdl = persistenceService.getKeyDataByStatus(KeyData.KeyStatus.obsolete);

        for (int i = 0; i < kdl.size(); i++) {
            KeyData kdx = kdl.get(i);
            System.out.println("Alias: " + kdx.getKeyAlias() + ", status: " + kdx.getStatus());
            System.out.println("Id: " + kdx.getId());
            String data = kdx.getKeyData();

            kdx.setKeyData(Misc.padCutStringRight(" ", ' ', data != null ? data.length() : 4096));
            log.info("Overwrting keydata id " + kdx.getId() + " alias " + kdx.getKeyAlias() + " record");
            for (int c = 0; c < 10; c++) {
                persistenceService.saveOrUpdate(kdx);
            }
            log.info("Deleting keydata id " + kdx.getId() + " alias " + kdx.getKeyAlias() + " record");
            persistenceService.delete(kdx);

        }
        log.info("Destroying retired key data completed");
    }

    protected boolean removeCertifcateData() throws Exception {
        log.info("About to remove certificate data..");
        X509Certificate toRemove = null;
        Long keyId = null;
        while (true) {
            log.info("Enter certficate key id to remove");
            //String alias, String status, String order, String od, Integer start, Integer limit, int[] count
            List<KeyData> pkl = persistenceService.getKeyDatasByAlias(KeyAlias.clientAuthCert, null, "id", null, 0,
                                                                      null, null);
            List<KeyData> pkl2 = persistenceService.getKeyDatasByAlias(KeyAlias.schemeIntermedCert, null, "id", null, 0,
                                                                       null, null);
            List<KeyData> pkl3 = persistenceService.getKeyDatasByAlias(KeyAlias.schemeRootCert, null, "id", null, 0,
                                                                       null, null);
            pkl.addAll(pkl2);
            pkl.addAll(pkl3);

            for (KeyData kdx : pkl) {
                if (KeyService.parsePemPrivateKey(kdx.getKeyData()) != null) {
                    System.out.println(kdx.getId() + " - " + kdx.getKeyAlias() + " " +
                                       DateUtil.formatDate(kdx.getKeyDate(), "yyyy-MM-dd HH:mm"));
                }
            }
            String keyIdStr = getPrompt("Enter entry id to remove: ");
            try {
                keyId = Misc.parseLongBoxed(keyIdStr);
                toRemove = keyService.loadCertificate(keyId, "X509");
                if (toRemove != null) {
                    break;
                } else {
                    log.info("Certficate by key id " + keyIdStr + " does not exist");
                }
            } catch (Exception ee) {
                log.error("Failed to load certificate " + keyId, ee);
                System.out.println("Failed to load key " + keyId + " exception: " + ee);
            }
        }

        if (toRemove != null) {
            KeyData kdx = persistenceService.getKeyDataById(keyId);
            log.info("WARNING: You are about to remove Alias: " + kdx.getKeyAlias() + " Id: " + kdx.getId());
            System.out.println("WARNING: You are about to remove Alias: " + kdx.getKeyAlias() + " Id: " + kdx.getId());
            System.out.println("X509 Subject: " + toRemove.getSubjectDN());
            String confirm = getPrompt("Confirm remove y/n: ");
            if ("y".equals(confirm)) {
                String data = kdx.getKeyData();
                kdx.setKeyData(Misc.padCutStringRight(" ", ' ', data.length()));
                log.info("Overwrting keydata id " + kdx.getId() + " alias " + kdx.getKeyAlias() + " record");
                for (int c = 0; c < 10; c++) {
                    persistenceService.saveOrUpdate(kdx);
                }
                log.info("Deleting keydata id " + kdx.getId() + " alias " + kdx.getKeyAlias() + " " +
                         toRemove.getSubjectDN() + " record");
                persistenceService.delete(kdx);
                log.info("Deleting certificate completed");
            } else {
                log.info("Deleting certificate cancelled by user");
            }
        }
        return true;
    }

    protected boolean installPluginKey(final HSMDevice hsms, final Context ctx) throws Exception {

        String keyAlias = getPrompt("Enter key alias for this key:");
        if (KeyAlias.hmacKeyExt.equals(keyAlias)) {
            installNewHMACKey(hsms, keyAlias, ctx);
            return true;
        }

        int cryptoPeriod = 365;
        while (true) {
            String crp = getPrompt("Enter keys crypto period in days (recommended 365..730 max 1460):");
            if (Misc.parseInt(crp) > 0 && Misc.parseInt(crp) <= 1460) {
                cryptoPeriod = Misc.parseInt(crp);
                break;
            } else {
                System.out.println("Invalid value or out of range");
            }
        }
        short procId = (short) 0;

        char[] password1 = null;
        char[] password2 = null;
        while (true) {
            password1 = getPassword("Custodian 1 Enter 256 bit key part one 32 hex chars: ");
            char[] password1r = getPassword("Re Enter part one: ");
            if (password1.length < 32) {
                System.out.println("Component too short, retry");
            } else if (Arrays.equals(password1, password1r)) {
                System.out.println("Component and confirmation matched");
                break;
            } else {
                System.out.println("Component and confirmation not matched, retry");
            }

        }

        while (true) {
            password2 = getPassword("Custodian 2 Enter 256 bit key part two 32 hex chars: ");
            char[] password2r = getPassword("Re Enter part two: ");
            if (password1.length < 32) {
                System.out.println("Component too short, retry");
            } else if (Arrays.equals(password2, password2r)) {
                System.out.println("Component and confirmation matched");
                break;
            } else {
                System.out.println("Component and confirmation not matched, retry");
            }

        }

        StringBuffer keyInHex = new StringBuffer();
        keyInHex.append(password1);
        keyInHex.append(password2);

        KeyData secKeydEx = persistenceService.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working);

        if (secKeydEx == null || "y".equals(getPrompt(
                "There is already existing " + keyAlias + " key from " + secKeydEx.getKeyDate() +
                ", replace this key y/n?"))) {

            KeyData hsmKey = persistenceService.getKeyDataByAlias(KeyAlias.keysKey, KeyData.KeyStatus.working);
            log.info(keyAlias + " for " + procId + " installation..");

            System.out.println(keyAlias + " encrypting..");
            byte[] passAesHSM = hsms.encryptData(Base64.decode(hsmKey.getKeyData()), HexUtils.fromString(keyInHex.toString()));

            KeyData secKeyd = new KeyData();
            if (secKeydEx != null) {
                secKeyd.setId(secKeydEx.getId());
            }

            secKeyd.setKeyAlias(keyAlias);
            secKeyd.setHsmDeviceId(hsmDevicePair.getKey());
            secKeyd.setKeyCheckValue("SHA-256 " + KeyService.getKeyHashCheckValue(HexUtils.fromString(keyInHex.toString())));
            secKeyd.setKeyData(Base64.encode(passAesHSM));
            secKeyd.setKeyDate(new java.util.Date());
            secKeyd.setStatus(KeyData.KeyStatus.working);
            secKeyd.setCryptoPeriodDays(cryptoPeriod);
            persistenceService.saveOrUpdate(secKeyd);
            log.info(keyAlias + " key created, installed");
        }
        return true;
    }

    protected boolean installZipFilePassword(HSMDevice hsmDevice) throws Exception {
        int cryptoPeriod = 365;
        while (true) {
            String crp = getPrompt("Enter keys crypto period in days (recommended 365..730 max 1460):");
            if (Misc.parseInt(crp) > 0 && Misc.parseInt(crp) <= 1460) {
                cryptoPeriod = Misc.parseInt(crp);
                break;
            } else {
                System.out.println("Invalid value or out of range");
            }
        }

        char[] password1 = null;
        char[] password2 = null;
        while (true) {
            password1 = getPassword("Custodian 1 Enter AES256 zip password part one 20+ chars: ");
            char[] password1r = getPassword("Re Enter zip password part one: ");
            if (password1.length < 20) {
                System.out.println("Password component too short, retry");
            } else if (Arrays.equals(password1, password1r)) {
                System.out.println("Password and confirmation matched");
                break;
            } else {
                System.out.println("Password and confirmation not matched, retry");
            }

        }

        while (true) {
            password2 = getPassword("Custodian 2 Enter AES256 zip password part two 20+ chars: ");
            char[] password2r = getPassword("Re Enter zip password part two: ");
            if (password1.length < 20) {
                System.out.println("Password component too short, retry");
            } else if (Arrays.equals(password2, password2r)) {
                System.out.println("Password and confirmation matched");
                break;
            } else {
                System.out.println("Password and confirmation not matched, retry");
            }

        }

        StringBuffer password = new StringBuffer();
        password.append(password1);
        password.append(password2);

        KeyData zipKeydEx = persistenceService.getKeyDataByAlias("zip", KeyData.KeyStatus.working);

        if (zipKeydEx == null || "y".equals(getPrompt(
                "There is already existing zip key  from " + zipKeydEx.getKeyDate() + ", replace this key y/n?"))) {

            KeyData hsmKey = persistenceService.getKeyDataByAlias("zip", KeyData.KeyStatus.working);
            log.info("zip installation..");

            System.out.println("zip" + " encrypting..");
            byte[] passAesHSM = hsmDevice.encryptData(Base64.decode(hsmKey.getKeyData()),
                    password.toString().getBytes(StandardCharsets.UTF_8));

            KeyData zipKeyd = new KeyData();
            if (zipKeydEx != null) {
                zipKeyd.setId(zipKeydEx.getId());
            }

            zipKeyd.setKeyAlias("zip");
            zipKeyd.setHsmDeviceId(hsmDevicePair.getKey());
            zipKeyd.setKeyCheckValue(
                    "SHA-256 " + KeyService.getKeyHashCheckValue(password.toString().getBytes(StandardCharsets.UTF_8)));
            zipKeyd.setKeyData(Base64.encode(passAesHSM));
            zipKeyd.setKeyDate(new java.util.Date());

            zipKeyd.setStatus(KeyData.KeyStatus.working);
            zipKeyd.setCryptoPeriodDays(cryptoPeriod);
            persistenceService.saveOrUpdate(zipKeyd);
            log.info("zip key created, installed");
        }
        return true;
    }

    protected boolean calculatePrivateKeyCheckValue() throws Exception {
        String ksn = getPrompt("Enter keystore file name:");
        java.io.File ksFile = new java.io.File(ksn);
        if (!ksFile.exists()) {
            log.error("Unable to find file " + ksFile.getAbsolutePath());
            return true;
        }

        String kst = getPrompt("Enter keystore type JKS/JCEKS/PKCS12:");
        String alias = null;

        if ("JKS".equals(kst) || "JCEKS".equals(kst)) {
            alias = getPrompt("Enter key alias in keystore:");
        }

        String kspass = getPrompt("Enter keystore password:");

        String kepass = getPrompt("Enter key entry password (if different):");
        if (kepass.length() < 1) {
            kepass = kspass;
        }

        PrivateKey pk = null;
        X509Certificate cert = null;
        if ("PKCS12".equals(kst)) {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            java.io.FileInputStream fis = new java.io.FileInputStream(ksFile);
            keyStore.load(fis, kspass.toCharArray());
            Enumeration<String> enumeration = keyStore.aliases();
            // uses the default alias
            alias = enumeration.nextElement();
            pk = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
            cert = (X509Certificate) keyStore.getCertificate(alias);
            fis.close();
        } else {
            KeyStore keyStore = KeyStore.getInstance(kst);
            java.io.FileInputStream fis = new java.io.FileInputStream(ksFile);
            keyStore.load(fis, kspass.toCharArray());
            pk = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
            cert = (X509Certificate) keyStore.getCertificate(alias);
            fis.close();
        }

        String kcv = KeyService.calculateRSAPrivateKeyCheckValue(pk);

        System.out.println("Private key " + alias + " keys check value is " + kcv);
        if (cert != null) {
            System.out.println("Certificate subject of " + alias + " is " + cert.getSubjectDN());
        }

        return true;
    }

    protected void migKeysKey(final HSMDevice hsms, final Context ctx) throws Exception {
        KeyData hsmKey = persistenceService.getKeyDataByAlias(KeyAlias.keysKey, KeyData.KeyStatus.working);
        if (hsmKey == null || Misc.isNullOrEmpty(hsmKey.getKeyData())) {
            System.out.println("Existing DS Keyskey (HSM key) not installed not possible to migrate..");
        } else {
            String mighsmkey = getPrompt("Do You want to migrate Keyskey Key y/n?");
            if ("y".equals(mighsmkey)) {
                int cryptoPeriod = CRYPTO_PERIOD_INITIAL;
                while (true) {
                    String crp = getPrompt(
                            "Enter keys crypto period in days (recommended 365..730 min 60 max 1460) for key:");
                    if (Misc.parseInt(crp) > 59 && Misc.parseInt(crp) <= 1460) {
                        cryptoPeriod = Misc.parseInt(crp);
                        break;
                    } else {
                        System.out.println("Invalid value or out of range");
                    }
                }

                System.out.println("DS KeysKey key migration.. ");
                // creates keysKey
                HSMDevice.Key newHsmKey = hsms.generateHSMDataEncryptionKey();
                migHSMKey(ctx, hsmKey, hsms, newHsmKey, hsms, cryptoPeriod);
                log.info("DS Keyskey (HSM key) migrated, new key created, installed");
            }
        }
    }

    protected void migKeysKeyAndHSM(final HSMDevice hsms, final Context ctx) throws Exception {
        KeyData hsmKey = persistenceService.getKeyDataByAlias(KeyAlias.keysKey, KeyData.KeyStatus.working);
        if (hsmKey == null || Misc.isNullOrEmpty(hsmKey.getKeyData())) {
            System.out.println("Existing DS Keyskey (HSM key) not installed not possible to migrate..");
            return;
        }

        System.out.println("WARNING: Something may fail, backup Your database and key table before proceeding!");
        String mighsmkey = getPrompt("Do You want to migrate to new Keyskey Key to work with new HSM y/n?");
        if ("y".equals(mighsmkey)) {
            int cryptoPeriod = CRYPTO_PERIOD_INITIAL;
            while (true) {
                String crp = getPrompt(
                        "Enter keys crypto period in days (recommended 365..730 min 60 max 1460) for key:");
                if (Misc.parseInt(crp) > 59 && Misc.parseInt(crp) <= 1460) {
                    cryptoPeriod = Misc.parseInt(crp);
                    break;
                } else {
                    System.out.println("Invalid value or out of range");
                }
            }

            HSMDevice hsmDeviceNew = null;
            String hsmDeviceClass = null;
            String newHsmConf = null;
            HSMDevice.Key newHsmKey = null;
            String hsmName = getPrompt("Enter new hsm device display name (e.g. Thales HSM):");
            do {
                try {
                    hsmDeviceClass = getPrompt("Enter new hsm device full class name:");
                    newHsmConf = getPrompt("Enter new hsm config string:");
                    hsmDeviceNew = (HSMDevice) Class.forName(hsmDeviceClass).newInstance();
                    hsmDeviceNew.initializeHSM(newHsmConf);
                    newHsmKey = hsmDeviceNew.generateHSMDataEncryptionKey();
                    break;
                } catch (Exception e) {
                    System.out.println("Failed to initialize new HSM or generte new HSM data key " + e);
                    log.error("Failed to initialize new HSM or generte new HSM data key", e);
                    String rd = getPrompt("Retry with new settings y/n:");
                    if ("y".equals(rd)) {
                        continue;
                    } else {
                        return;
                    }
                }
            } while (true);

            System.out.println("DS KeysKey key migration to new HSM.. ");
            log.info("DS Keyskey (HSM key) migration to new HSM " + hsmDeviceClass + " " + newHsmConf);
            migHSMKey(ctx, hsmKey, hsms, newHsmKey, hsmDeviceNew, cryptoPeriod);
            log.info("DS Keyskey (HSM key) migrated to new HSM, new KeysKey key created, installed");

            log.info("Updating HSM settings..");
            HsmDevice dbHsmDevice = new HsmDevice();
            dbHsmDevice.setName(hsmName);
            dbHsmDevice.setClassName(hsmDeviceClass);
            hsmDeviceService.saveOrUpdate(dbHsmDevice);

            HsmDeviceConf dbHsmDeviceConf = new HsmDeviceConf();
            dbHsmDeviceConf.setHsmDeviceId(dbHsmDevice.getId());
            dbHsmDeviceConf.setDsId(0);
            dbHsmDeviceConf.setConfig(newHsmConf);
            hsmDeviceConfService.saveOrUpdate(dbHsmDeviceConf);

            ctx.resetSettingCache();

            log.info("Please restart now applications to validate migration functional");
        }

    }

    protected void installNewDataKey(final Context ctx) throws Exception {
        String migdb = getPrompt("Do you want to install new key for " + KeyAlias.dataKey + " key y/n?");
        if ("y".equals(migdb)) {
            log.info(KeyAlias.dataKey + " new working data encryption key installation...");

            // check if old key exists and retire it
            KeyData currentDataKey = persistenceService.getKeyDataByAlias(KeyAlias.dataKey, KeyData.KeyStatus.working);
            if (currentDataKey != null) {
                currentDataKey.setStatus(KeyStatus.retired);

                // find if exist already
                KeyData oldret = persistenceService.getKeyDataByAlias(currentDataKey.getKeyAlias(), KeyData.KeyStatus.retired);
                if (oldret != null) {
                    oldret.setKeyAlias("obs-" + oldret.getKeyAlias() + "-" +
                                       DateUtil.formatDate(new Date(), "yyMMddHHmm"));
                    persistenceService.saveOrUpdate(oldret);
                    log.info(oldret.getKeyAlias() + " old retired key " + oldret.getId() + " set to obsolete");
                }

                persistenceService.saveOrUpdate(currentDataKey);
                log.info(currentDataKey.getKeyAlias() + " old working key " + currentDataKey.getId() +
                         " set to retired");
            }

            HSMDevice hsms = hsmDevicePair.getValue();
            HSMDevice.Key generatedDataKey = hsms.generateHSMDataEncryptionKey();

            KeyData dbkData = new KeyData();
            dbkData.setKeyData(Base64.encode(generatedDataKey.getValue()));
            dbkData.setKeyAlias(KeyAlias.dataKey);
            dbkData.setHsmDeviceId(hsmDevicePair.getKey());
            dbkData.setStatus(KeyData.KeyStatus.working);
            dbkData.setKeyDate(new Date());
            dbkData.setKeyCheckValue(HexUtils.toString(generatedDataKey.getCheckValue()));

            persistenceService.saveOrUpdate(dbkData);
            ctx.resetSettingCache();

            log.info(dbkData.getKeyAlias() + " new working key " + dbkData.getId() + " created, installed");
            KeyData reloadedFromDbKey = persistenceService.getKeyDataByAlias(KeyAlias.dataKey, KeyStatus.working);
            byte[] reloadedDataKeyBytes = Base64.decode(reloadedFromDbKey.getKeyData());

            String testdata = "1234567890abcdeefghijklmnop";
            if (Arrays.equals(generatedDataKey.getValue(), reloadedDataKeyBytes)) {
                System.out.println("DS DB Key loading/encryption/decryption validated match");
                System.out.println("Data encrypt selftest..");

                byte[] encDataDb = hsms.encryptData(reloadedDataKeyBytes, testdata.getBytes());
                System.out.println("Data encrypt of " + testdata + "=" + Base64.encode(encDataDb));

                String decDataDb = new String(hsms.decryptData(reloadedDataKeyBytes, encDataDb));

                System.out.println("Data decrypt of " + Base64.encode(encDataDb) + "=" + decDataDb);
                if (testdata.equals(decDataDb)) {
                    System.out.println("Data encrypt/decrypt successful");
                } else {
                    System.out.println("ERROR Data encrypt/dencrypt data mismatch");
                }
            } else {
                System.out.println("ERROR DS Data original and decrypted keys mismatch!!!");
            }

            log.info("For immediate effect restart application server");
        }
    }

    protected void installNewHMACKey(final HSMDevice hsms, final String alias, final Context ctx) throws Exception {
        int cryptoPeriod = CRYPTO_PERIOD_INITIAL;
        System.out.println("Application server restart is highly recommended at the end of this key installation");
        String migdb = getPrompt("Do You want to install new key for " + alias + " key y/n?");
        if ("y".equals(migdb)) {
            KeyData encryptedHMBKey = persistenceService.getKeyDataByAlias(alias, KeyData.KeyStatus.working);

            log.info(alias + " new working key installation..");
            byte[] hmacks = null;
            if (KeyAlias.hmacKeyExt.equals(alias) &&
                "y".equals(getPrompt("Do You want to enter plain key components for " + alias + " key y/n?"))) {
                String part1 = null, part2 = null;
                for (int i = 0; i < 2; i++) {
                    getPrompt("Attention Key custodian " + (i + 1) +
                              " ask for privacy, press enter when privacy established");
                    while (true) {
                        String part = getPrompt("Key custodian " + (i + 1) + " enter your key part 32 hex chars: ");
                        part = part.trim();
                        try {
                            if (part.length() == 32 && HexUtils.fromString(part).length == 16) {
                                if (i == 0) {
                                    part1 = part;
                                }
                                if (i == 1) {
                                    part2 = part;
                                }

                                getPrompt("Value ok, press enter to continue..");
                                for (int j = 0; j < 100; j++) {
                                    System.out.println();
                                }
                                break;
                            }
                        } catch (Exception e) {
                            System.out.println("Invalid data entered");
                            e.printStackTrace();
                        }
                    }
                }

                hmacks = HexUtils.fromString(part1 + part2);
            } else {
                hmacks = KeyService.genKey(32);
            }

            System.out.println("DS " + alias + " HSM encrypting..");

            while (true) {
                String crp = getPrompt(
                        "Enter keys crypto period in days (recommended 365..730 min 60 max 1460) for data key:");
                if (Misc.parseInt(crp) > 59 && Misc.parseInt(crp) <= 1460) {
                    cryptoPeriod = Misc.parseInt(crp);
                    break;
                } else {
                    System.out.println("Invalid value or out of range");
                }
            }

            if (encryptedHMBKey != null) {
                // find if exist already
                KeyData oldret = persistenceService.getKeyDataByAlias(encryptedHMBKey.getKeyAlias(),
                                                                      KeyData.KeyStatus.retired);
                if (oldret != null) {
                    oldret.setKeyAlias("obs-" + oldret.getKeyAlias() + "-" +
                                       DateUtil.formatDate(new java.util.Date(), "yyMMddHHmm"));
                    persistenceService.saveOrUpdate(oldret);
                    log.info(oldret.getKeyAlias() + " old retired key " + oldret.getId() + " set to obsolete");
                }

                encryptedHMBKey.setStatus(KeyStatus.retired);
                persistenceService.saveOrUpdate(encryptedHMBKey);
                log.info(encryptedHMBKey.getKeyAlias() + " olkd working key " + encryptedHMBKey.getId() +
                         " set to retired");
            }

            KeyData dbkData = new KeyData();
            dbkData.setKeyData(Base64.encode(hmacks));
            dbkData.setKeyAlias(alias);
            dbkData.setHsmDeviceId(hsmDevicePair.getKey());
            dbkData.setStatus(KeyData.KeyStatus.working);
            dbkData.setKeyDate(new java.util.Date());
            dbkData.setCryptoPeriodDays(cryptoPeriod);
            dbkData.setKeyCheckValue(cryptoService.calculateHMACKeyCheckValue(hmacks));
            persistenceService.saveOrUpdate(dbkData);
            log.info("New key " + encryptedHMBKey.getKeyAlias() + " " + dbkData.getStatus() + " hmac checkvalue: " +
                     dbkData.getKeyCheckValue() + " installed");
            ctx.resetSettingCache();

            if (KeyAlias.hmacKey.equals(alias)) {
                log.info("RE HMAC client certificates");
                hmacCertificates();
            }

            System.out.println("Application server restart is now recommended");
            log.info("Application server restart is now recommended");
        } // y
    }

    /**
     * Installs (adds) new CA Root certificate to DS. Allows either certificate import from existing
     * JKS/JCEKS/PKCS12/X509 file, or to generate a new self-signed certificate.
     */
    private boolean installNewSchemeRoot(Context ctx) throws Exception {
        log.info("Initialize and install new scheme root - CA");
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        HSMDevice hsms = hsmDevicePair.getValue();
        String keyAlias = KeyService.KeyAlias.schemeRootCert;
        int cryptoPeriod = 3650;

        String exi = getPrompt("Install existing from keystore/X509 file y or generate new n:");
        if ("y".equals(exi)) {
            // Importing only suppored by Software HSM
            String ksn = getPrompt("Enter keystore file name:");
            java.io.File ksFile = new java.io.File(ksn);
            if (!ksFile.exists()) {
                log.error("Unable to find file " + ksFile.getAbsolutePath());
                return true;
            }
            String kst = getPrompt("Enter keystore type JKS/JCEKS/PKCS12/X509 (certficate only):");
            String alias = null;

            if ("JKS".equals(kst) || "JCEKS".equals(kst)) {
                alias = getPrompt("Enter key alias in keystore:");
            }

            String kspass = !"X509".equals(kst) ? getPrompt("Enter keystore password:") : null;

            String kepass = !"X509".equals(kst) ? getPrompt("Enter key entry password (if different):") : null;
            if (kepass != null && kepass.length() < 1) {
                kepass = kspass;
            }

            PrivateKey pk = null;
            X509Certificate cert;
            if ("X509".equals(kst)) {
                X509Certificate[] certs = KeyService.parseX509Certificates(
                        Utils.readFile(ksFile.getAbsolutePath()), null);
                cert = certs[0];
            } else if ("PKCS12".equals(kst)) {
                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                java.io.FileInputStream fis = new java.io.FileInputStream(ksFile);
                keyStore.load(fis, kspass.toCharArray());
                Enumeration<String> enumeration = keyStore.aliases();
                // uses the default alias
                alias = enumeration.nextElement();
                pk = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
                cert = (X509Certificate) keyStore.getCertificate(alias);
                fis.close();
            } else {
                KeyStore keyStore = KeyStore.getInstance(kst);
                java.io.FileInputStream fis = new java.io.FileInputStream(ksFile);
                keyStore.load(fis, kspass.toCharArray());
                pk = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
                cert = (X509Certificate) keyStore.getCertificate(alias);
                fis.close();
            }

            KeyData schemeRootCertEntry;
            int counter = 1;
            while ((schemeRootCertEntry = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working)) != null) {
                keyAlias = KeyService.KeyAlias.schemeRootCert + "-" + counter;
                counter++;
            }

            schemeRootCertEntry = new KeyData();
            schemeRootCertEntry.setKeyAlias(keyAlias);
            schemeRootCertEntry.setStatus(KeyData.KeyStatus.working);
            schemeRootCertEntry.setHsmDeviceId(hsmDevicePair.getKey());

            if (cert != null) {
                cryptoPeriod = (int) ((cert.getNotAfter().getTime() - System.currentTimeMillis()) / DateUtil.DT24H);

                if (cryptoPeriod < 1) {
                    log.error("Key certificate is already expired on " + cert.getNotAfter() +
                              " not installing this certificate");
                    return true;
                }
            }

            schemeRootCertEntry.setCryptoPeriodDays(cryptoPeriod);
            schemeRootCertEntry.setKeyDate(new Date());


            byte[] newCertData = cert.getEncoded();
            byte[] newPrivateKeyData = null;
            if (pk != null) {
                newPrivateKeyData = pk.getEncoded();
            }

            schemeRootCertEntry.setKeyData(KeyService.combinePrivateKeyAndCertificate(newPrivateKeyData, newCertData));
            pse.saveOrUpdate(schemeRootCertEntry);
            log.info("New scheme root certificate " +
                     (newPrivateKeyData != null ? " with private key" : " (without private key)") + " " + schemeRootCertEntry.getId() +
                     " " + schemeRootCertEntry.getKeyAlias() + " " + cert.getSubjectDN().getName() + " has been installed");
            log.info("For immediate effect restart application server");
            return true;
        } else {
            // Generating new keypair
            int keyLength = KeyInstallerHelper.promptKeyLength("RSA", KeyService.RRSAKeyMin, KeyService.RRSAKeyMin, KeyService.RRSAKeyMax);
            String vd = getPrompt("Enter validity days (default " + cryptoPeriod + "):");
            if (Misc.isInt(vd)) {
                cryptoPeriod = Misc.parseInt(vd);
            }

            log.info("Enter Certificate details");
            String country = getPrompt("Enter two letter country code: ");
            String state = getPrompt("Enter state: ");
            String locality = getPrompt("Enter locality: ");
            String org = getPrompt("Enter org name: ");
            String ou = getPrompt("Enter org unit: ");
            String cn = getPrompt("Enter common name: ");
            String signatureAlgorithm = KeyService.sigAlgRSASHA256;

            String crlUrl = ctx.getStringSetting(DsSetting.DSCA_CRL_URL.getKey());

            KeyPair rootCAkeypair = hsms.generateRSAdecryptAndSign(keyLength);
            String subjectDN = CertUtil.createX509SubjectV3BC(country, state, locality, org, ou, cn).toString();
            X509v3CertificateBuilder rootCertBuilder =
                    CertUtil.prepareRootCAcertBuilder(rootCAkeypair.getPublic(), subjectDN, cryptoPeriod);

            X509Certificate selfSignedCert = cryptoService.signX509certificate(rootCertBuilder, hsms,
                    rootCAkeypair.getPrivate().getEncoded(), rootCAkeypair.getPublic(), signatureAlgorithm);

            KeyData exiKey = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working);

            int counter = 1;
            while ((exiKey = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working)) != null) {
                keyAlias = KeyService.KeyAlias.schemeRootCert + "-" + counter;
                counter++;

            }

            exiKey = new KeyData();
            exiKey.setKeyAlias(keyAlias);
            exiKey.setStatus(KeyData.KeyStatus.working);
            exiKey.setHsmDeviceId(hsmDevicePair.getKey());
            exiKey.setCryptoPeriodDays(cryptoPeriod);
            exiKey.setKeyDate(new java.util.Date());

            byte[] newPrivateKeyData = rootCAkeypair.getPrivate().getEncoded();
            exiKey.setKeyData(KeyService.combinePrivateKeyAndCertificate(newPrivateKeyData, selfSignedCert.getEncoded()));
            pse.saveOrUpdate(exiKey);
            log.info("New scheme root key  " + exiKey.getId() + " " + exiKey.getKeyAlias() +
                     " has been installed as working");
            log.info("Restart application server to have immediate effect");

            return true;
        }

    }

    /**
     * Installs (adds) new Intermediate CA certificate to DS. Allows either importing from a JKS/JCEKS/PKCS12/X509 file,
     * or generating a new certificate (signed by existing Root, or as CSR).
     */
    private boolean installNewSchemeIntermed(Context ctx) throws Exception {
        log.info("Initialize and install new intermediate - CA");
        int cryptoPeriod = 3650;

        String exi = getPrompt("Install from keystore/X509 file (y) or generate new (n): ");
        if ("y".equals(exi)) {
            // Importing only suppored by Software HSM
            String ksn = getPrompt("Enter keystore file name: ");
            File ksFile = new File(ksn);
            if (!ksFile.exists()) {
                log.error("Unable to find file " + ksFile.getAbsolutePath());
                return true;
            }
            String kst = getPrompt("Enter keystore type JKS/JCEKS/PKCS12/X509 (certficate only): ");
            String alias = null;

            if ("JKS".equals(kst) || "JCEKS".equals(kst)) {
                alias = getPrompt("Enter key alias in keystore:");
            }

            String kspass = !"X509".equals(kst) ? getPrompt("Enter keystore password: ") : null;

            String kepass = !"X509".equals(kst) ? getPrompt("Enter key entry password (if different): ") : null;
            if (kepass != null && kepass.length() < 1) {
                kepass = kspass;
            }

            byte[] privateKeyBytes = null;
            X509Certificate cert;
            if ("X509".equals(kst)) {
                X509Certificate[] certs = KeyService.parseX509Certificates(Utils.readFile(ksFile.getAbsolutePath()), null);
                cert = certs[0];

                System.out.println("Importing certificate with SDN: " + cert.getSubjectDN().getName());

                List<KeyData> intermediateCSRs = persistenceService.getKeyDatasByAlias(KeyAlias.schemeIntermedCSR,
                        KeyStatus.pending, null, null, null, null, null);
                if (intermediateCSRs != null && intermediateCSRs.size() > 0) {
                    System.out.println("Pending CSR list:");
                    for (KeyData intermedCSR : intermediateCSRs) {
                        String csrBase64 = KeyService.parsePEMCSR(intermedCSR.getKeyData());
                        byte[] csrBytes = Base64.decode(csrBase64);
                        BcPKCS10CertificationRequest certReq = new BcPKCS10CertificationRequest(csrBytes);
                        X500Principal subject = new X500Principal(certReq.getSubject().getEncoded());
                        String subjectDN = subject.getName(X500Principal.RFC2253);
                        System.out.println(intermedCSR.getId() + " - SDN: " + subjectDN);
                    }

                    String isUsePrivateKey;
                    while (true) {
                        isUsePrivateKey = getPrompt("Do you want to use pending CSR private key for the imported certificate? (y/n): ");
                        if ("y".equalsIgnoreCase(isUsePrivateKey) || "n".equalsIgnoreCase(isUsePrivateKey)) {
                            break;
                        }
                    }

                    if ("y".equalsIgnoreCase(isUsePrivateKey)) {
                        String csrID = getPrompt("Select the CSR id to extract the private key from: ");
                        Long id = Misc.parseLongBoxed(csrID);
                        KeyData csrFound = intermediateCSRs.stream().filter(e -> e.getId().equals(id)).findFirst().orElse(null);
                        String privateKeyBase64 = KeyService.parsePemPrivateKey(csrFound.getKeyData());
                        privateKeyBytes = Base64.decode(privateKeyBase64);
                    }
                }

            } else if ("PKCS12".equals(kst)) {
                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                FileInputStream fis = new FileInputStream(ksFile);
                keyStore.load(fis, kspass.toCharArray());
                Enumeration<String> enumeration = keyStore.aliases();
                // uses the default alias
                alias = enumeration.nextElement();
                PrivateKey privateKey = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
                privateKeyBytes = privateKey.getEncoded();
                cert = (X509Certificate) keyStore.getCertificate(alias);
                fis.close();
            } else {
                KeyStore keyStore = KeyStore.getInstance(kst);
                FileInputStream fis = new FileInputStream(ksFile);
                keyStore.load(fis, kspass.toCharArray());
                PrivateKey privateKey = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
                privateKeyBytes = privateKey.getEncoded();
                cert = (X509Certificate) keyStore.getCertificate(alias);
                fis.close();
            }

            if (cert != null) {
                cryptoPeriod = (int) ((cert.getNotAfter().getTime() - System.currentTimeMillis()) / DateUtil.DT24H);

                if (cryptoPeriod < 1) {
                    log.error("Key certificate is already expired on " + cert.getNotAfter() +
                              " not installing this certificate");
                    return true;
                }

                KeyService ks = ServiceLocator.getInstance().getKeyService();
                List<X509Certificate> roots = ks.loadSchemeRoots(KeyData.KeyStatus.working);

                boolean rootfound = false;
                for (X509Certificate scrx : roots) {
                    if (scrx.getSubjectX500Principal().getName(X500Principal.RFC2253).equals(
                            cert.getIssuerX500Principal().getName(X500Principal.RFC2253))) {
                        rootfound = true;
                        try {
                            cert.verify(scrx.getPublicKey());
                            break;
                        } catch (SignatureException se) {
                            log.error("Failed to validate certifcate with schemeroot " + scrx.getSubjectDN() +
                                      " invalid signature", se);
                            return true;

                        } catch (Exception e) {
                            log.error("Failed to validate certifcate with schemeroot " + scrx.getSubjectDN() +
                                      " invalid signature", e);
                            return true;
                        }
                    }
                } // for

                if (!rootfound) {
                    log.error("No root found matching certifcate issuer " + cert.getIssuerDN() +
                              " not installing this certificate");
                    return true;
                }
            }

            int counter = 1;
            String keyAlias = KeyService.KeyAlias.schemeIntermedCert;
            while (persistenceService.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working) != null) {
                keyAlias = KeyService.KeyAlias.schemeIntermedCert + "-" + counter;
                counter++;
            }

            KeyData exiKey = new KeyData();
            exiKey.setKeyAlias(keyAlias);
            exiKey.setStatus(KeyData.KeyStatus.working);
            exiKey.setHsmDeviceId(hsmDevicePair.getKey());
            exiKey.setCryptoPeriodDays(cryptoPeriod);
            exiKey.setKeyDate(new Date());

            byte[] newCertData = cert.getEncoded();
            exiKey.setKeyData(KeyService.combinePrivateKeyAndCertificate(privateKeyBytes, newCertData));
            persistenceService.saveOrUpdate(exiKey);
            log.info("New scheme intermediate certificate ({} private key) has been installed: {} {} {}",
                    (privateKeyBytes != null ? "with" : "without"),
                    exiKey.getId(),
                    exiKey.getKeyAlias(),
                    cert.getSubjectDN().getName());

            log.info("For immediate effect restart application server");
            return true;
        } else {
            // Generate new certificate
            log.info("Enter Certificate subject details");

            // get key length and generate keypair using _currently selected_ hsm device
            int privateKeyBitLength = KeyInstallerHelper.promptKeyLength("RSA", KeyService.RRSAKeyMin,
                    KeyService.RRSAKeyMin, KeyService.RRSAKeyMax);
            HSMDevice intermediateCAhsm = hsmDevicePair.getValue();
            KeyPair intermediateCAkeypair = intermediateCAhsm.generateRSAdecryptAndSign(privateKeyBitLength);

            // ask for subject DN details
            String country = getPrompt("Enter two letter country code: ");
            String state = getPrompt("Enter state: ");
            String locality = getPrompt("Enter locality: ");
            String org = getPrompt("Enter org name: ");
            String ou = getPrompt("Enter org unit: ");
            String cn = getPrompt("Enter common name: ");
            String subjectDN = CertUtil.createX509SubjectV3BC(country, state, locality, org, ou, cn).toString();
            String signatureAlgorithm = KeyService.sigAlgRSASHA256;

            String keyAlias;
            String keyStatus;
            String generatedKeyData;

            String isSignWithRootCA = getPrompt("Sign with root CA (y) or create CSR (n): ");
            if ("y".equalsIgnoreCase(isSignWithRootCA)) {
                // signt with ROOT CA
                String validityPeriodStr = getPrompt("Enter validity days (default 3650):");
                if (Misc.isInt(validityPeriodStr)) {
                    cryptoPeriod = Misc.parseInt(validityPeriodStr);
                }
                KeyData signingKeyCertData = KeyInstallerHelper.promptRootCertPrivateKey(persistenceService);
                String signingKeyBase64 = KeyService.parsePemPrivateKey(signingKeyCertData.getKeyData());
                byte[] signingPrivateKey = Base64.decode(signingKeyBase64);

                String signingCertBase64 = KeyService.parsePEMCert(signingKeyCertData.getKeyData());
                X509Certificate[] certs = KeyService.parseX509Certificates(Base64.decode(signingCertBase64));
                X509Certificate signingRootCert = certs[0];

                String crlUrl = ctx.getStringSetting(DsSetting.DSCA_CRL_URL + "." + signingKeyCertData.getId());

                X509v3CertificateBuilder intermediateCertBuilder = CertUtil.prepareIntermediateCAcertBuilder(
                        intermediateCAkeypair.getPublic(), subjectDN, cryptoPeriod, signingRootCert, crlUrl);

                // HSM device used for signing must correspond to _issuer private key_ HSM device
                HSMDevice signingKeyHsmDevice = hsmDeviceService.getInitializedHSMDevice(signingKeyCertData.getHsmDeviceId());
                X509Certificate intermediateCert = cryptoService.signX509certificate(intermediateCertBuilder,
                        signingKeyHsmDevice, signingPrivateKey, signingRootCert.getPublicKey(), signatureAlgorithm);

                byte[] issuerCertSubject = signingRootCert.getSubjectX500Principal().getEncoded();
                byte[] subjectCertIssuer = intermediateCert.getIssuerX500Principal().getEncoded();
                if (!Arrays.equals(issuerCertSubject, subjectCertIssuer)) {
                    throw new RuntimeException("Generated certifcate Issuer DN " + HexUtils.toString(subjectCertIssuer) +
                            " mismatch with supplied sig cert subject DN " + HexUtils.toString(issuerCertSubject));
                }

                int counter = 1;
                keyAlias = KeyService.KeyAlias.schemeIntermedCert;
                while (persistenceService.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working) != null) {
                    keyAlias = KeyService.KeyAlias.schemeIntermedCert + "-" + counter;
                    counter++;
                }
                keyStatus = KeyData.KeyStatus.working;
                byte[] newPrivateKeyData = intermediateCAkeypair.getPrivate().getEncoded();
                generatedKeyData = KeyService.combinePrivateKeyAndCertificate(newPrivateKeyData, intermediateCert.getEncoded());

            } else {
                // Generate (using _current hsm_)and save CSR
                PKCS10CertificationRequest csr = cryptoService.generateCSR(intermediateCAhsm, intermediateCAkeypair,
                        subjectDN, null, signatureAlgorithm);

                keyStatus = KeyData.KeyStatus.pending;

                int counter = 1;
                keyAlias = KeyService.KeyAlias.schemeIntermedCSR;
                while (persistenceService.getKeyDataByAlias(keyAlias, keyStatus) != null) {
                    keyAlias = KeyService.KeyAlias.schemeIntermedCSR + "-" + counter;
                    counter++;
                }

                byte[] newPrivateKeyData = intermediateCAkeypair.getPrivate().getEncoded();
                generatedKeyData = KeyService.combinePrivateKeyAndCSR(newPrivateKeyData, csr.getEncoded());

                StringBuilder buf = new StringBuilder(4096);
                buf.append("-----BEGIN CERTIFICATE REQUEST-----\r\n");
                String b64 = Base64.encode(csr.getEncoded(), 76);
                buf.append(b64);
                buf.append("\r\n-----END CERTIFICATE REQUEST-----");
                System.out.println("Certificate request for external CA:");
                System.out.println(buf.toString());
            }

            KeyData exiKey = new KeyData();
            exiKey.setKeyAlias(keyAlias);
            exiKey.setStatus(keyStatus);
            exiKey.setHsmDeviceId(hsmDevicePair.getKey());
            exiKey.setCryptoPeriodDays(cryptoPeriod);
            exiKey.setKeyDate(new Date());
            exiKey.setKeyData(generatedKeyData);
            persistenceService.saveOrUpdate(exiKey);

            log.info("New scheme intermed ca key " + exiKey.getId() + " " + exiKey.getKeyAlias() +
                     " has been installed as " + keyAlias);
            log.info("Restart application server to have immediate effect");

            return true;
        }

    }

    protected boolean installSSLAuthPair(HSMDevice hsms, Context ctx) throws Exception {
        log.info("Install new SSL Client auth key pair");
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        String keyAlias = KeyService.KeyAlias.clientAuthCert;
        String vd = getPrompt("Enter validity days (default 365):");

        int cryptoPeriod = 365;
        if (Misc.isInt(vd)) {
            cryptoPeriod = Misc.parseInt(vd);
        }

        String exi = getPrompt("Install existing from keystore y or generate new n:");
        if ("y".equals(exi)) {
            String ksn = getPrompt("Enter keystore file name:");
            File ksFile = new File(ksn);
            if (!ksFile.exists()) {
                log.error("Unable to find file " + ksFile.getAbsolutePath());
                return true;
            }
            String kst = getPrompt("Enter keystore type JKS/JCEKS/PKCS12:");
            String alias = null;

            if ("JKS".equals(kst) || "JCEKS".equals(kst)) {
                alias = getPrompt("Enter key alias in keystore:");
            }

            String kspass = getPrompt("Enter keystore password:");

            String kepass = getPrompt("Enter key entry password (if different):");
            if (kepass.length() < 1) {
                kepass = kspass;
            }

            KeyData exiKey = null;
            PrivateKey pk = null;
            Certificate[] cert = null;
            if ("PKCS12".equals(kst)) {

                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                java.io.FileInputStream fis = new java.io.FileInputStream(ksFile);
                keyStore.load(fis, kspass.toCharArray());
                Enumeration<String> enumeration = keyStore.aliases();
                // uses the default alias
                alias = enumeration.nextElement();
                pk = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
                cert = keyStore.getCertificateChain(alias);

                fis.close();
            } else {
                KeyStore keyStore = KeyStore.getInstance(kst);
                java.io.FileInputStream fis = new java.io.FileInputStream(ksFile);
                keyStore.load(fis, kspass.toCharArray());
                pk = (PrivateKey) (keyStore.getKey(alias, kepass.toCharArray()));
                cert = keyStore.getCertificateChain(alias);

                fis.close();
            }

            int counter = 1;
            while ((exiKey = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working)) != null) {
                keyAlias = KeyService.KeyAlias.clientAuthCert + "-" + counter;
                counter++;

            }

            if (!pk.getAlgorithm().contains("RSA")) {
                log.error("Invalid SSL auth key algorithm " + pk.getAlgorithm() + " must be RSA");
                return true;
            }

            exiKey = new KeyData();
            exiKey.setKeyAlias(keyAlias);
            exiKey.setStatus(KeyData.KeyStatus.working);
            exiKey.setHsmDeviceId(hsmDevicePair.getKey());

            if (cert != null) {
                X509Certificate cx509 = (X509Certificate) cert[0];
                cryptoPeriod = (int) ((cx509.getNotAfter().getTime() - System.currentTimeMillis()) / DateUtil.DT24H);

                if (cryptoPeriod < 1) {
                    log.error("Key certificate is already expired on " + cx509.getNotAfter() +
                              " not installing this key");
                    return true;
                }
            }

            exiKey.setCryptoPeriodDays(cryptoPeriod);
            exiKey.setKeyDate(new Date());

            byte[] newPkData = pk.getEncoded();
            exiKey.setKeyData(KeyService.combinePrivateKeyAndCertChain(newPkData, cert));
            pse.saveOrUpdate(exiKey);

            String sett = getPrompt("Update/set settings acs.ssl.client.certId, mi.ssl.client.certId y/n:");
            if ("y".equals(sett)) {
                Setting s1 = pse.getSettingById(DsSetting.ACS_SSL_CLIENT_CERTID.getKey());
                if (s1 == null) {
                    s1 = new Setting();
                    s1.setKey(DsSetting.ACS_SSL_CLIENT_CERTID.getKey());
                    s1.setComment("Default ACS SSL auth keypair/certificate id");
                }

                s1.setValue("" + exiKey.getId());

                Setting s2 = pse.getSettingById(DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey());
                if (s2 == null) {
                    s2 = new Setting();
                    s2.setKey(DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey());
                    s2.setComment("Default MI SSL auth keypair/certificate id");
                }
                s2.setValue("" + exiKey.getId());
                pse.saveOrUpdate(s1);
                pse.saveOrUpdate(s2);
                log.info("Settings " + s1.getKey() + ", " + s2.getKey() + " updated");
            }

            log.info("New SSL Auth keypair " + exiKey.getId() + " " + exiKey.getKeyAlias() + " has been installed");
            log.info("For immediate effect restart application server");
            return true;
        } else {
            int privateKeyLength = KeyInstallerHelper.promptKeyLength("RSA", KeyService.RRSAKeyMin,
                    KeyService.RRSAKeyMin, KeyService.RRSAKeyMax);

            log.info("Enter Certificate details");
            String country = getPrompt("Enter two letter country code: ");
            String state = getPrompt("Enter state: ");
            String locality = getPrompt("Enter locality: ");
            String org = getPrompt("Enter org name: ");
            String ou = getPrompt("Enter org unit: ");
            String cn = getPrompt("Enter common name: ");

            KeyData selectedCA = KeyInstallerHelper.selectCertAuthority(pse);
            if (selectedCA == null) {
                log.error("No CA Key found");
                return false;
            }

            String signingKeyBase64 = KeyService.parsePemPrivateKey(selectedCA.getKeyData());
            byte[] signingPrivateKey = Base64.decode(signingKeyBase64);

            String signingCertBase64 = KeyService.parsePEMCert(selectedCA.getKeyData());
            X509Certificate[] signingCerts = KeyService.parseX509Certificates(Base64.decode(signingCertBase64));
            X509Certificate signingCert = signingCerts[0];

            KeyPair clientTLScertKeypair = hsms.generateRSAdecryptAndSign(privateKeyLength);

            String subjectDN = CertUtil.createX509SubjectV3BC(country, state, locality, org, ou, cn).toString();
            String crlUrl = ctx.getStringSetting(DsSetting.DSCA_CRL_URL.getKey());
            List<KeyPurposeId> extendedKeyUsageList = new ArrayList<>(1);
            extendedKeyUsageList.add(KeyPurposeId.id_kp_clientAuth);
            X509v3CertificateBuilder clientTLScertBuilder = CertUtil.prepareTLSCertBuilder(
                    clientTLScertKeypair.getPublic(), subjectDN, cryptoPeriod, signingCert, crlUrl, extendedKeyUsageList);

            // HSM device used for signing must correspond to _issuer (CA) private key_ HSM device
            HSMDevice signingKeyHsmDevice = hsmDeviceService.getInitializedHSMDevice(selectedCA.getHsmDeviceId());
            X509Certificate clientTLScertificate = cryptoService.signX509certificate(clientTLScertBuilder,
                    signingKeyHsmDevice, signingPrivateKey, signingCert.getPublicKey(), KeyService.sigAlgRSASHA256);

            byte[] issuerCertSubject = signingCert.getSubjectX500Principal().getEncoded();
            byte[] subjectCertIssuer = clientTLScertificate.getIssuerX500Principal().getEncoded();
            if (!Arrays.equals(issuerCertSubject, subjectCertIssuer)) {
                throw new RuntimeException("Generated certifcate Issuer DN " + HexUtils.toString(subjectCertIssuer) +
                        " mismatch with supplied sig cert subject DN " + HexUtils.toString(issuerCertSubject));
            }

            boolean intermed = selectedCA.getKeyAlias().contains(KeyService.KeyAlias.schemeIntermedCert);
            // find root
            Certificate root = null;
            if (intermed) {
                List<X509Certificate> working = keyService.loadSchemeRoots(KeyData.KeyStatus.working);
                List<X509Certificate> retired = keyService.loadSchemeRoots(KeyData.KeyStatus.retired);
                List<X509Certificate> allRoots = new LinkedList<>();
                allRoots.addAll(working);
                allRoots.addAll(retired);

                for (X509Certificate dbRoot : allRoots) {
                    if (dbRoot.getSubjectDN().getName().equals(signingCert.getIssuerDN().getName())) {
                        root = dbRoot;
                        break;
                    }
                }
            }

            Certificate[] certs = new Certificate[root != null ? 3 : 2];
            certs[0] = clientTLScertificate;
            certs[1] = signingCert;
            if (root != null) {
                certs[2] = root;
            }

            KeyData exiKey = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working);

            int counter = 1;
            while ((exiKey = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working)) != null) {
                keyAlias = KeyService.KeyAlias.clientAuthCert + "-" + counter;
                counter++;
            }

            exiKey = new KeyData();
            exiKey.setKeyAlias(keyAlias);
            exiKey.setStatus(KeyData.KeyStatus.working);
            exiKey.setHsmDeviceId(hsmDevicePair.getKey());
            exiKey.setCryptoPeriodDays(cryptoPeriod);
            exiKey.setKeyDate(new Date());

            byte[] clientTLScertPrivateKeyBytes = clientTLScertKeypair.getPrivate().getEncoded();
            exiKey.setKeyData(KeyService.combinePrivateKeyAndCertChain(clientTLScertPrivateKeyBytes, certs));
            pse.saveOrUpdate(exiKey);

            String sett = getPrompt("Update/set settings acs.ssl.client.certId, mi.ssl.client.certId y/n:");
            if ("y".equals(sett)) {
                Setting s1 = pse.getSettingById(DsSetting.ACS_SSL_CLIENT_CERTID.getKey());
                if (s1 == null) {
                    s1 = new Setting();
                    s1.setKey(DsSetting.ACS_SSL_CLIENT_CERTID.getKey());
                    s1.setComment("Default ACS SSL auth keypair/certificate id");
                }

                s1.setValue("" + exiKey.getId());

                Setting s2 = pse.getSettingById(DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey());
                if (s2 == null) {
                    s2 = new Setting();
                    s2.setKey(DsSetting.TDSSERVER_SSL_CLIENT_CERTID.getKey());
                    s2.setComment("Default MI SSL auth keypair/certificate id");
                }
                s2.setValue("" + exiKey.getId());
                pse.saveOrUpdate(s1);
                pse.saveOrUpdate(s2);
                log.info("Settings " + s1.getKey() + ", " + s2.getKey() + " updated");
            }

            log.info("New SSL Auth key pair  " + exiKey.getId() + " " + exiKey.getKeyAlias() +
                     " has been installed as working");
            log.info("Restart application server to have immediate effect");

            return true;
        }
    }

    protected boolean addKeyDataFromPrompt() throws Exception {
        printAvailableHsmDevices();
        System.out.println("Adding keyData from command line.");

        String hsmDeviceId = getPrompt("Enter HSMDevice id: ");
        String keyAlias = getPrompt("Enter keyAlias (name) for the key: ");
        String status = getPrompt("Enter status (example: working): ");
        String keyDataFileName = getPrompt("Enter filename with the keyData: ");


        File keyDataFile = new File(keyDataFileName);
        if (!keyDataFile.exists()) {
            log.error("Unable to find file " + keyDataFile.getAbsolutePath());
            return true;
        }
        byte[] keyData = Utils.readFile(keyDataFile.getAbsolutePath());
        String keyDataString = new String(keyData, StandardCharsets.UTF_8);


        KeyData newKey = new KeyData();
        newKey.setKeyAlias(keyAlias);
        newKey.setStatus(status);
        newKey.setHsmDeviceId(Long.valueOf(hsmDeviceId));
        newKey.setKeyDate(new Date());
        newKey.setKeyData(keyDataString);

        persistenceService.saveOrUpdate(newKey);

        log.info("New key id: {}, keyAlias: {} has been installed", newKey.getId(), newKey.getKeyAlias());
        return true;
    }

    protected boolean generateAndExportSSLServerCert(Context ctx) throws Exception {
        log.info("Generate and export a CA signed server certficate");
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        String vd = getPrompt("Enter validity days (default 365):");
        int cryptoPeriod = 365;
        if (Misc.isInt(vd)) {
            cryptoPeriod = Misc.parseInt(vd);
        }

        int privateKeyLength = KeyInstallerHelper.promptKeyLength("RSA", KeyService.RRSAKeyMin,
                KeyService.RRSAKeyMin, KeyService.RRSAKeyMax);
        log.info("Enter Certificate details");
        String country = getPrompt("Enter two letter country code: ");
        String state = getPrompt("Enter state: ");
        String locality = getPrompt("Enter locality: ");
        String org = getPrompt("Enter org name: ");
        String ou = getPrompt("Enter org unit: ");
        String cn = getPrompt("Enter common name: ");

        KeyData selectedCA = KeyInstallerHelper.selectCertAuthority(pse);
        if (selectedCA == null) {
            log.error("No CA Key found");
            return false;
        }

        String signingKeyBase64 = KeyService.parsePemPrivateKey(selectedCA.getKeyData());
        byte[] signingPrivateKey = Base64.decode(signingKeyBase64);

        String certDataPEM = KeyService.parsePEMCert(selectedCA.getKeyData());
        byte[] cerPlain = Base64.decode(certDataPEM);
        X509Certificate[] caCerts = KeyService.parseX509Certificates(cerPlain);
        X509Certificate caCert = caCerts[0];


        KeyPair serverTLScertKeypair = hsmDevicePair.getValue().generateRSAdecryptAndSign(privateKeyLength);

        String subjectDN = CertUtil.createX509SubjectV3BC(country, state, locality, org, ou, cn).toString();
        String crlUrl = ctx.getStringSetting(DsSetting.DSCA_CRL_URL.getKey());
        List<KeyPurposeId> extendedKeyUsageList = new ArrayList<>(1);
        extendedKeyUsageList.add(KeyPurposeId.id_kp_serverAuth);
        X509v3CertificateBuilder clientTLScertBuilder = CertUtil.prepareTLSCertBuilder(
                serverTLScertKeypair.getPublic(), subjectDN, cryptoPeriod, caCert, crlUrl, extendedKeyUsageList);

        // HSM device used for signing must correspond to _issuer (CA) private key_ HSM device
        HSMDevice signingKeyHsmDevice = hsmDeviceService.getInitializedHSMDevice(selectedCA.getHsmDeviceId());
        X509Certificate serverTLScertificate = cryptoService.signX509certificate(clientTLScertBuilder,
                signingKeyHsmDevice, signingPrivateKey, caCert.getPublicKey(), KeyService.sigAlgRSASHA256);

        byte[] issuerCertSubject = caCert.getSubjectX500Principal().getEncoded();
        byte[] subjectCertIssuer = serverTLScertificate.getIssuerX500Principal().getEncoded();
        if (!Arrays.equals(issuerCertSubject, subjectCertIssuer)) {
            throw new RuntimeException("Generated certifcate Issuer DN " + HexUtils.toString(subjectCertIssuer) +
                    " mismatch with supplied sig cert subject DN " + HexUtils.toString(issuerCertSubject));
        }



        boolean intermed = selectedCA.getKeyAlias().contains(KeyService.KeyAlias.schemeIntermedCert);
        // find root
        Certificate root = null;
        if (intermed) {
            List<X509Certificate> working = keyService.loadSchemeRoots(KeyData.KeyStatus.working);
            List<X509Certificate> retired = keyService.loadSchemeRoots(KeyData.KeyStatus.retired);
            List<X509Certificate> allRoots = new LinkedList<>();
            allRoots.addAll(working);
            allRoots.addAll(retired);
            for (X509Certificate xc : allRoots) {
                if (xc.getSubjectDN().getName().equals(caCert.getIssuerDN().getName())) {
                    root = xc;
                    break;
                }
            }
        }

        Certificate[] certs = new Certificate[root != null ? 3 : 2];
        certs[0] = serverTLScertificate;
        certs[1] = caCert;
        if (root != null) {
            certs[2] = root;
        }

        String ksp = getPrompt("Enter keytore password: ");
        String ksFileName = "server-cert-" + DateUtil.formatDate(new java.util.Date(), "yyyyMMddHHmm") + ".p12";
        String alias = "server" + DateUtil.formatDate(new java.util.Date(), "yyyyMMddHHmm");

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(null, ksp.toCharArray());
        File f = new File(ksFileName);
        keyStore.setKeyEntry(alias, serverTLScertKeypair.getPrivate(), ksp.toCharArray(), certs);
        FileOutputStream fos = new FileOutputStream(f);

        keyStore.store(fos, ksp.toCharArray());
        fos.close();

        log.info("New SSL Certificate exported to p12 keystore: " + f.getAbsolutePath() + " alias " + alias);

        return true;
    }

    /**
     * Installs SDK keypair.
     */
    private boolean installNewSDKKeyPair(Context ctx) throws Exception {
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();

        String isInstall = getPrompt("Generate new (n) or install from X509 file (i): ");
        if ("i".equalsIgnoreCase(isInstall)) {
            String x509filename = getPrompt("Enter X509 file name: ");
            File x509file = new File(x509filename);
            if (!x509file.exists()) {
                log.error("Unable to find file " + x509file.getAbsolutePath());
                return true;
            }

            X509Certificate[] certs = KeyService.parseX509Certificates(Utils.readFile(x509file.getAbsolutePath()), null);
            X509Certificate cert = certs[0];
            System.out.println("Importing certificate with SDN: " + cert.getSubjectDN().getName());

            int cryptoPeriod = (int) ((cert.getNotAfter().getTime() - System.currentTimeMillis()) / DateUtil.DT24H);
            if (cryptoPeriod < 1) {
                log.error("Key certificate is already expired on " + cert.getNotAfter() +
                        " not installing this certificate");
                return true;
            }

            List<KeyData> sdkRsaCSRs = persistenceService.getKeyDatasByAlias(KeyAlias.sdkRSACSR,
                    KeyStatus.pending, null, null, null, null, null);

            byte[] privateKeyBytes = null;
            if (sdkRsaCSRs != null && sdkRsaCSRs.size() > 0) {
                System.out.println("Pending CSR list:");
                for (KeyData sdkRsaCSR : sdkRsaCSRs) {
                    String csrBase64 = KeyService.parsePEMCSR(sdkRsaCSR.getKeyData());
                    byte[] csrBytes = Base64.decode(csrBase64);
                    BcPKCS10CertificationRequest certReq = new BcPKCS10CertificationRequest(csrBytes);
                    X500Principal subject = new X500Principal(certReq.getSubject().getEncoded());
                    String subjectDN = subject.getName(X500Principal.RFC2253);
                    System.out.println(sdkRsaCSR.getId() + " - SDN: " + subjectDN);
                }

                String isUsePrivateKey;
                while (true) {
                    isUsePrivateKey = getPrompt("Do you want to use pending CSR private key for the imported certificate? (y/n): ");
                    if ("y".equalsIgnoreCase(isUsePrivateKey) || "n".equalsIgnoreCase(isUsePrivateKey)) {
                        break;
                    }
                }

                if ("y".equalsIgnoreCase(isUsePrivateKey)) {
                    String csrID = getPrompt("Select the CSR id to extract the private key from: ");
                    Long id = Misc.parseLongBoxed(csrID);
                    KeyData csrFound = sdkRsaCSRs.stream().filter(e -> e.getId().equals(id)).findFirst().orElse(null);
                    String privateKeyBase64 = KeyService.parsePemPrivateKey(csrFound.getKeyData());
                    privateKeyBytes = Base64.decode(privateKeyBase64);
                }
            }

            String keyAlias = KeyAlias.sdkRSAKey;
            int counter = 1;
            while (pse.getKeyDataByAlias(keyAlias, KeyStatus.working) != null) {
                keyAlias = KeyService.KeyAlias.sdkRSAKey + "-" + counter;
                counter++;
            }

            // save and sign key entry
            KeyData sdkRsaKey = new KeyData();
            sdkRsaKey.setKeyAlias(keyAlias);
            sdkRsaKey.setStatus(KeyData.KeyStatus.working);
            sdkRsaKey.setHsmDeviceId(hsmDevicePair.getKey());
            sdkRsaKey.setCryptoPeriodDays(cryptoPeriod);
            sdkRsaKey.setKeyDate(new Date());

            byte[] newCertData = cert.getEncoded();
            sdkRsaKey.setKeyData(KeyService.combinePrivateKeyAndCertificate(privateKeyBytes, newCertData));

            pse.saveOrUpdate(sdkRsaKey);

            log.info("New RSA SDK key {} {} has been installed as working", sdkRsaKey.getId(), sdkRsaKey.getKeyAlias());
            log.info("Restart application server to have immediate effect");
            return true;
        }

        // At the moment of implementation Thales (9k/10k) only supports RSA (but not EC).
        log.info("Install SDK RSA keypair");
        HSMDevice hsms = hsmDevicePair.getValue();

        // Collecting certificate details
        String validityDaysPrompt = getPrompt("Enter validity days (default 3650): ");
        int validityDays = CERT_VALIDITY_DAYS_DEFAULT;
        if (Misc.isInt(validityDaysPrompt)) {
            validityDays = Misc.parseInt(validityDaysPrompt);
        }

        log.info("Enter Certificate/CSR details");
        String certCountry = getPrompt("Enter two letter country code: ");
        String certState = getPrompt("Enter state: ");
        String certLocality = getPrompt("Enter locality: ");
        String certOrg = getPrompt("Enter org name: ");
        String certOu = getPrompt("Enter org unit: ");
        String certCn = getPrompt("Enter common name: ");

        String subjectDN = CertUtil.createX509SubjectV3BC(certCountry, certState, certLocality, certOrg, certOu, certCn).toString();
        String signatureAlgorithm = KeyService.sigAlgRSASHA256;

        int privateKeyLength = KeyInstallerHelper.promptKeyLength("RSA", KeyService.RRSAKeyMin, KeyService.RRSAKeyMin, KeyService.RRSAKeyMax);
        KeyPair sdkRsaKeypair = hsms.generateRSAdecryptAndSign(privateKeyLength);

        String isGenerateCSR = getPrompt("Generate CSR (c) or signed certificate (s): ");
        if ("c".equalsIgnoreCase(isGenerateCSR)) {
            KeyInstallerHelper.generateSdkRsaCsr(cryptoService, persistenceService, hsmDevicePair, sdkRsaKeypair,
                    subjectDN, signatureAlgorithm, validityDays);

            return true;
        }

        String isCaSigned = getPrompt("CA-signed (c) or self-signed certificate (s): ");
        String dbKeyData;
        if ("c".equalsIgnoreCase(isCaSigned)) {
            // Retrieve DS root certificate
            System.out.println("Installing CA-signed SDK RSA key...");
            KeyData certCA = KeyInstallerHelper.selectCertAuthority(pse);
            if (certCA == null) {
                log.error("No CA Key found");
                return false;
            }

            String signingKeyBase64 = KeyService.parsePemPrivateKey(certCA.getKeyData());
            byte[] signingPrivateKey = Base64.decode(signingKeyBase64);

            String signingCertBase64 = KeyService.parsePEMCert(certCA.getKeyData());
            X509Certificate[] certs = KeyService.parseX509Certificates(Base64.decode(signingCertBase64));
            X509Certificate signingCert = certs[0];

            // Generate HSM-based RSA keypair and create a certificate
            String crlUrl = ctx.getStringSetting(DsSetting.DSCA_CRL_URL.getKey());

            PrivateKey sdkPrivateKey = sdkRsaKeypair.getPrivate();
            PublicKey sdkPublicKey = sdkRsaKeypair.getPublic();

            X509v3CertificateBuilder intermediateCertBuilder = CertUtil.prepareSdkRsaDecryptionKeyCertBuilder(
                    sdkPublicKey, subjectDN, validityDays, signingCert, crlUrl);

            // HSM device used for signing must correspond to _issuer (CA) private key_ HSM device
            HSMDevice signingKeyHsmDevice = hsmDeviceService.getInitializedHSMDevice(certCA.getHsmDeviceId());
            X509Certificate sdkCertificate = cryptoService.signX509certificate(intermediateCertBuilder,
                    signingKeyHsmDevice, signingPrivateKey, signingCert.getPublicKey(), signatureAlgorithm);

            byte[] issuerCertSubject = signingCert.getSubjectX500Principal().getEncoded();
            byte[] subjectCertIssuer = sdkCertificate.getIssuerX500Principal().getEncoded();
            if (!Arrays.equals(issuerCertSubject, subjectCertIssuer)) {
                throw new RuntimeException("Generated certifcate Issuer DN " + HexUtils.toString(subjectCertIssuer) +
                        " mismatch with supplied sig cert subject DN " + HexUtils.toString(issuerCertSubject));
            }

            dbKeyData = KeyService.combinePrivateKeyAndCertificate(sdkPrivateKey.getEncoded(), sdkCertificate.getEncoded());

        } else {
            X509v3CertificateBuilder selfSignedCertBuilder = CertUtil.prepareRootCAcertBuilder(sdkRsaKeypair.getPublic(), subjectDN, validityDays);
            X509Certificate selfSignedCert = cryptoService.signX509certificate(selfSignedCertBuilder, hsms,
                    sdkRsaKeypair.getPrivate().getEncoded(), sdkRsaKeypair.getPublic(), signatureAlgorithm);

            byte[] newPrivateKeyData = sdkRsaKeypair.getPrivate().getEncoded();
            dbKeyData = KeyService.combinePrivateKeyAndCertificate(newPrivateKeyData, selfSignedCert.getEncoded());
        }

        String keyAlias = KeyAlias.sdkRSAKey;
        int counter = 1;
        while (pse.getKeyDataByAlias(keyAlias, KeyStatus.working) != null) {
            keyAlias = KeyService.KeyAlias.sdkRSAKey + "-" + counter;
            counter++;
        }

        // save and sign key entry
        KeyData sdkRsaKey = new KeyData();
        sdkRsaKey.setKeyAlias(keyAlias);
        sdkRsaKey.setStatus(KeyData.KeyStatus.working);
        sdkRsaKey.setHsmDeviceId(hsmDevicePair.getKey());
        sdkRsaKey.setCryptoPeriodDays(validityDays);
        sdkRsaKey.setKeyDate(new Date());
        sdkRsaKey.setKeyData(dbKeyData);

        pse.saveOrUpdate(sdkRsaKey);

        log.info("New RSA SDK key {} {} has been installed as working", sdkRsaKey.getId(), sdkRsaKey.getKeyAlias());
        log.info("Restart application server to have immediate effect");
        return true;
    }

    protected boolean installeNewTrustedRoot() throws Exception {
        log.info("Initialize and install new SSL trusted root");
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        String keyAlias = KeyService.KeyAlias.trustedRootCert;

        String ksn = getPrompt("Enter certicate file name:");
        File ksFile = new File(ksn);
        if (!ksFile.exists()) {
            log.error("Unable to find file " + ksFile.getAbsolutePath());
            return true;
        }

        KeyData exiKey;
        X509Certificate[] cert = KeyService.parseX509Certificates(Utils.readFile(ksFile.getAbsolutePath()), null);

        int counter = 1;
        while ((exiKey = pse.getKeyDataByAlias(keyAlias, KeyData.KeyStatus.working)) != null) {
            keyAlias = KeyAlias.trustedRootCert + "-" + counter;
            counter++;

        }

        exiKey = new KeyData();
        exiKey.setKeyAlias(keyAlias);
        exiKey.setStatus(KeyData.KeyStatus.working);
        exiKey.setHsmDeviceId(hsmDevicePair.getKey());

        int cryptoPeriod = 1;
        if (cert != null) {
            cryptoPeriod = (int) ((cert[0].getNotAfter().getTime() - System.currentTimeMillis()) / DateUtil.DT24H);
            if (cryptoPeriod < 1) {
                log.error("Certificate is already expired on " + cert[0].getNotAfter() + " not installing this key");
                return true;
            }
        }
        exiKey.setCryptoPeriodDays(cryptoPeriod);
        exiKey.setKeyDate(new Date());
        exiKey.setKeyData(KeyService.combinePrivateKeyAndCertChain(null, cert));
        pse.saveOrUpdate(exiKey);

        log.info("New trusted root certiciate only " + exiKey.getId() + " " + exiKey.getKeyAlias() +
                 " has been installed");
        log.info("For immediate effect restart application server");
        return true;

    }

    protected boolean hmacCertificates() throws Exception {
        log.info("HMAC existing certificates...");
        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        List<CertificateData> cdList = pse.getPersitableList(CertificateData.class, null, null);
        List<Persistable> cdList2 = new java.util.ArrayList<Persistable>(cdList.size());
        int cnt = 0;

        for (CertificateData cdx : cdList) {
            cdList2.add(cdx);
            cnt++;
            if (cnt % 100 == 0) {
                log.info("Checkvalues recalculated " + cnt + " of " + cdList.size());
            }

        }
        log.info("All Checkvalues recalculated " + cnt + " of " + cdList.size());
        pse.saveOrUpdate(cdList2);
        log.info("Data updated..");

        return true;
    }

    protected void setKeyStatus(Context ctx, String status) throws Exception {
        log.info("Enter key id to set as " + status);
        String keyIdStr = getPrompt("Enter key id to set as " + status);
        Long keyId = Misc.parseLongBoxed(keyIdStr);
        KeyData mki = persistenceService.getKeyDataById(keyId);
        if (mki != null) {
            String y = getPrompt("Are You sure to change key " + mki.getId() + " " + mki.getKeyAlias() + "status y/n?");
            if (!"y".equals(y)) {
                log.info("Key id " + keyId + " action canceled");
                return;
            }

            java.util.Date now = new java.util.Date();
            if (KeyStatus.obsolete.equals(status)) {
                mki.setKeyAlias("obs-" + mki.getKeyAlias() + "-" + DateUtil.formatDate(now, "yyMMddHHmm"));
            }
            mki.setStatus(status);
            persistenceService.saveOrUpdate(mki);
            log.info("Key by id " + keyId + " was set to " + mki.getStatus() + " status");
        } else {
            log.info("Key by id " + keyId + " was not found");
        }
    }
}
