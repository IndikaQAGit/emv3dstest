package com.modirum.ds.imports;

import com.modirum.ds.db.dao.PersistenceService;

public abstract class AbstractBatchHandlerFacade<M extends BaseBatchModel> {

    protected final PersistenceService persistenceService;
    protected final DataBatchHandlerConfiguration configuration;

    /**
     * Interface that provides a list of supported actions and operations.
     */
    protected abstract AbstractOperationProcessorProvider createProcessorsProvider();

    /**
     *
     */
    protected abstract AbstractCsvParser<M> createParser();

    /**
     *
     */
    protected abstract AbstractBatchDataValidator<M> createDataValidator();

    public AbstractBatchHandlerFacade(final PersistenceService persistenceService, final DataBatchHandlerConfiguration configuration) {
        this.persistenceService = persistenceService;
        this.configuration = configuration;
    }

    /**
     * Validate content of a file with merchants.
     */
    public DataBatchResultStatus validate(final String fileContent) {
        return createBatchDataHandler().validateData(fileContent);
    }

    /**
     * Validate and import content of a file with merchants.
     */
    public DataBatchResultStatus validateAndProcess(final String fileContent) {
        return createBatchDataHandler().validateAndImportData(fileContent);
    }

    private DataBatchHandler<M> createBatchDataHandler() {
        final AbstractCsvParser<M> parser = createParser();
        final AbstractBatchDataValidator<M> validator = createDataValidator();
        final AbstractOperationProcessorProvider provider = createProcessorsProvider();
        return new DataBatchHandler<>(parser, validator, provider);
    }
}
