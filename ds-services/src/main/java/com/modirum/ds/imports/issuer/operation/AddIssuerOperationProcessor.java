package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.dao.PersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.List;

public class AddIssuerOperationProcessor implements IDataOperationProcessor<BatchIssuerModel> {

    private static final Logger LOG = LoggerFactory.getLogger(AddIssuerOperationProcessor.class);

    private final PersistenceService persistenceService;

    private int processedCount;

    public AddIssuerOperationProcessor(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public void process(final List<BatchIssuerModel> issuers) {
        processedCount = 0;
        issuers.stream()
                .map(this::mapToIssuer)
                .forEach(this::saveIssuer);
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }

    private Issuer mapToIssuer(final BatchIssuerModel importModel) {
        Issuer issuer = new Issuer();
        issuer.setBIN(importModel.getBin());
        issuer.setName(importModel.getName());
        issuer.setStatus(mapStatus(importModel.getStatus()));
        issuer.setPaymentSystemId(Integer.parseInt(importModel.getPaymentSystemId()));
        issuer.setPhone(importModel.getPhone());
        issuer.setEmail(importModel.getEmail());
        issuer.setCountry(importModel.getCountry());
        issuer.setCity(importModel.getCity());
        issuer.setAddress(importModel.getAddress());
        issuer.setCreatedDate(LocalDateTime.now());
        issuer.setLastModifiedDate(LocalDateTime.now());
        return issuer;
    }

    private String mapStatus(final String status) {
        switch (status) {
            case "active":
                return DSModel.Issuer.Status.ACTIVE;
            case "disabled":
                return DSModel.Issuer.Status.DISABLED;
        }
        return DSModel.Issuer.Status.DISABLED;
    }

    private void saveIssuer(final Issuer issuer) {
        try {
            persistenceService.save(issuer);
            processedCount++;
        } catch (Exception e) {
            LOG.error("Error while saving issuer in database:", e);
        }
    }
}
