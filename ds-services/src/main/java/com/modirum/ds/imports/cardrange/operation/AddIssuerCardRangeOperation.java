package com.modirum.ds.imports.cardrange.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.imports.merchant.operation.AddMerchantOperationProcessor;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.db.dao.PersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddIssuerCardRangeOperation implements IDataOperationProcessor<BatchCardRangeModel> {

    private static final Logger LOG = LoggerFactory.getLogger(AddMerchantOperationProcessor.class);

    private final PersistenceService persistenceService;
    private final IssuerService issuerService;

    private final Map<String, Issuer> cache = new HashMap<>();

    private int processedCount;

    public AddIssuerCardRangeOperation(final PersistenceService persistenceService,
                                       final IssuerService issuerService) {
        this.persistenceService = persistenceService;
        this.issuerService = issuerService;
    }

    @Override
    public void process(final List<BatchCardRangeModel> dataList) {
        processedCount = 0;
        dataList.stream()
                .map(this::mapToCardRange)
                .forEach(this::saveCardRange);
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }

    private CardRange mapToCardRange(final BatchCardRangeModel importModel) {
        String issuerName = importModel.getIssuerName();
        String paymentSystemId = importModel.getPaymentSystemId();
        String issuerPaymentSystemKey = issuerName.concat(",").concat(paymentSystemId);
        Issuer issuer = cache.get(issuerPaymentSystemKey);
        if (issuer == null) {
            issuer = persistenceService.getIssuerByNameAndPaymentSystem(issuerName, Integer.parseInt(paymentSystemId));
            cache.put(issuerPaymentSystemKey, issuer);
        }
        CardRange cardRange = createCardRange(importModel, issuer);
        if (importModel.getAcsUrl() != null && !importModel.getAcsUrl().isEmpty()) {
            createAndSaveAcsProfile(importModel, issuer);
        }
        return cardRange;
    }

    private CardRange createCardRange(final BatchCardRangeModel importModel, final Issuer issuer) {
        CardRange cardRange = new CardRange();
        cardRange.setIssuerId(issuer.getId());
        cardRange.setBin(importModel.getBinStart());
        cardRange.setEnd(importModel.getBinEnd());
        cardRange.setName(importModel.getAlias() == null ? generateAlias(importModel) : importModel.getAlias());
        cardRange.setStatus(mapCardRangeStatus(importModel.getStatus()));
        cardRange.setPaymentSystemId(Integer.parseInt(importModel.getPaymentSystemId()));
        cardRange.setLastMod(new Date());
        cardRange.setType(Short.parseShort(importModel.getCardType()));
        if (importModel.getSet() == null) {
            cardRange.setSet((short)1); // Default card ranges set is 1
        } else {
            cardRange.setSet(Short.parseShort(importModel.getSet()));
        }
        return cardRange;
    }

    private void createAndSaveAcsProfile(final BatchCardRangeModel importModel, final Issuer issuer) {
        String acsUrl = importModel.getAcsUrl();
        // If this ACS URL is defined for this issuer we will not be adding it
        if (this.issuerService.getACSProfileByUrlAndIssuerId(acsUrl, issuer.getId()) != null) {
            return;
        }
        ACSProfile acsProfile = new ACSProfile();
        acsProfile.setStatus(mapACSProfileStatus(importModel.getAcsProfileStatus()));
        acsProfile.setIssuerId(issuer.getId());
        acsProfile.setPaymentSystemId(Integer.parseInt(importModel.getPaymentSystemId()));
        acsProfile.setURL(acsUrl);
        acsProfile.setRefNo(importModel.getAcsRefNumber());
        acsProfile.setLastMod(new Date());
        acsProfile.setOperatorID(importModel.getAcsOperatorId());
        acsProfile.setThreeDSMethodURL(importModel.getAcs3DSMethodUrl());
        acsProfile.setName(importModel.getAcsName() != null ? importModel.getAcsName() : generateAcsName(importModel.getAcsUrl()));
        if (importModel.getAcsProtocolStart() != null) {
            acsProfile.setStartProtocolVersion(importModel.getAcsProtocolStart());
        } else {
            acsProfile.setStartProtocolVersion(TDSModel.MessageVersion.V2_1_0.value());
        }
        if (importModel.getAcsProtocolEnd() != null) {
            acsProfile.setEndProtocolVersion(importModel.getAcsProtocolEnd());
        } else {
            acsProfile.setEndProtocolVersion(TDSModel.MessageVersion.V2_2_0.value());
        }
        if (importModel.getAcsSet() == null) {
            acsProfile.setSet((short)1); // Default card ranges set is 1
        } else {
            acsProfile.setSet(Short.parseShort(importModel.getAcsSet()));
        }
        saveAcsProfile(acsProfile);
    }

    private String generateAcsName(final String acsUrl) {
        try {
            URL url = new URL(acsUrl);
            return url.getHost();
        } catch (MalformedURLException e) {
            LOG.error("Error generation name from URL: " + e.getMessage());
        }
        return "DefaultAcs";
    }

    private String generateAlias(final BatchCardRangeModel importModel) {
        return importModel.getBinStart().substring(0, 6) + '-' + importModel.getBinEnd().substring(0, 6) + importModel.getIssuerName().substring(0, 2).trim();
    }

    private String mapCardRangeStatus(final String status) {
        switch (status) {
            case "participating":
                return DSModel.CardRange.Status.PARTICIPATING;
            case "notparticipating":
                return DSModel.CardRange.Status.NOTPARTICIPATING;
        }
        return DSModel.Merchant.Status.DISABLED;
    }

    private String mapACSProfileStatus(final String status) {
        switch (status) {
            case "active":
                return DSModel.ACSProfile.Status.ACTIVE;
            case "disabled":
                return DSModel.ACSProfile.Status.DISABLED;
            case "ended":
                return DSModel.ACSProfile.Status.ENDED;
        }
        return DSModel.ACSProfile.Status.DISABLED;
    }

    private void saveAcsProfile(final ACSProfile acsProfile) {
        try {
            persistenceService.save(acsProfile);
        } catch (Exception e) {
            LOG.error("Error while Acs profile: " + e.getMessage());
        }
    }

    private void saveCardRange(final CardRange cardRange) {
        try {
            persistenceService.save(cardRange);
            processedCount++;
        } catch (Exception e) {
            LOG.error("Error while saving card range in database: " + e.getMessage());
        }
    }
}
