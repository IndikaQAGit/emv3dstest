package com.modirum.ds.imports;

import com.modirum.ds.services.PaymentSystemsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public abstract class AbstractBatchDataValidator<D extends BaseBatchModel> {

    protected final DataBatchHandlerConfiguration configuration;
    protected Map<CSVHeaderFormat, ValidationsList<D>> validations;
    private final PaymentSystemsService paymentSystemsService;
    protected List<String> errors;

    /**
     * Initialize validators.
     */
    protected abstract Map<CSVHeaderFormat, ValidationsList<D>> initValidators();

    protected AbstractBatchDataValidator(final DataBatchHandlerConfiguration configuration, PaymentSystemsService paymentSystemsService) {
        this.configuration = configuration;
        this.paymentSystemsService = paymentSystemsService;
        this.validations = initValidators();
    }

    public Set<CSVHeaderFormat> getSupportedHeaders() {
        return validations.keySet();
    }

    /**
     *
     */
    public boolean isValid(final CSVHeaderFormat headerFormat, final List<D> importModels) {
        this.errors = new ArrayList<>();
        boolean result = true;
        // If this parameter is equal true, models will be validates until all errors are discovered.
        // Otherwise validation will stop after first error.
        boolean continueOnError = this.configuration.isContinueOnError();

        ValidationsList<D> validations = this.validations.get(headerFormat);

        for (Function<FilterParams<D>, Boolean> validator : validations.getValidationsList()) {
            if (!validator.apply(new FilterParams<D>(headerFormat, importModels, continueOnError))) {
                result = false;
                if (!continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    protected boolean isValidPaymentSystemsIDs(final boolean continueOnError, final List<String> paymentSystemIds) {
        boolean result = true;
        for (String paymentSystemIdStr : paymentSystemIds) {
            Integer paymentSystemId = null;
            if (StringUtils.isEmpty(paymentSystemIdStr)) {
                addError(emptyPaymentSystemIdError());
                result = false;
                if (!continueOnError) {
                    return result;
                }
            } else if (!NumberUtils.isDigits(paymentSystemIdStr)) {
                result = false;
                addError(invalidPaymentSystemIdError(paymentSystemIdStr));
                if (!continueOnError) {
                    return result;
                }
            } else {
                paymentSystemId = Integer.parseInt(paymentSystemIdStr);
            }

            if (paymentSystemId != null) {
                if (!paymentSystemsService.isPaymentSystemExists(paymentSystemId)) {
                    addError(nonExistentPaymentSystemIdError(paymentSystemIdStr));
                    result = false;
                    if (!continueOnError) {
                        return result;
                    }
                }
            }

            if (!configuration.isSuperuser() && !paymentSystemId.equals(configuration.getUserPaymentSystemId())) {
                addError(noAccessPaymentSystemId(paymentSystemIdStr));
                result = false;
                if (!continueOnError) {
                    return result;
                }
            }
        }
        return result;
    }

    private String nonExistentPaymentSystemIdError(final String paymentSystemId) {
        return String.format("Payment system ID = %s doesn't exist", paymentSystemId);
    }

    private String emptyPaymentSystemIdError() {
        return "Payment system ID cannot be empty";
    }

    private String invalidPaymentSystemIdError(final String paymentSystemId) {
        return String.format("Invalid numeric payment system ID: %s", paymentSystemId);
    }

    protected String noAccessPaymentSystemId(final String paymentSystemId) {
        return String.format("No sufficient permissions to import data for Payment System id = %s.", paymentSystemId);
    }

    public void setPaymentSystemIds(final List<D> importModels) {
        for (D importModel : importModels) {
            importModel.setPaymentSystemId(getPaymentSystemId(importModel.getPaymentSystemId()));
        }
    }

    private String getPaymentSystemId(String paymentSystemId) {
        if (StringUtils.isEmpty(paymentSystemId)) {
            return configuration.getUserPaymentSystemId().toString();
        }

        return paymentSystemId;
    }

    public Collection<String> getErrors() {
        return this.errors;
    }

    public void addError(final String error) {
        if (!this.errors.contains(error)) {
            this.errors.add(error);
        }
    }

    public static class FilterParams<D> {

        public final CSVHeaderFormat headerFormat;
        public final List<D> importModels;
        public final boolean continueOnError;

        public FilterParams(final CSVHeaderFormat headerFormat, final List<D> importModels, final boolean continueOnError) {
            this.headerFormat = headerFormat;
            this.importModels = importModels;
            this.continueOnError = continueOnError;
        }
    }
}
