package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.db.dao.PersistenceService;

import java.util.List;

public class DeleteIssuerOperationProcessor implements IDataOperationProcessor<BatchIssuerModel> {

    private final PersistenceService persistenceService;

    private int processedCount;

    public DeleteIssuerOperationProcessor(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public void process(final List<BatchIssuerModel> issuers) {
        issuers.stream()
               .peek(e -> processedCount++)
               .forEach(model -> persistenceService.deleteIssuerByNameAndPaymentSystemId(model.getName(), Integer.parseInt(model.getPaymentSystemId())));
    }

    @Override
    public int processedCount() {
        return this.processedCount;
    }
}
