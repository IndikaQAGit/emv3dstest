package com.modirum.ds.imports.merchant.parser;

import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.imports.AbstractCsvParser;
import com.modirum.ds.imports.CSVParserException;
import com.modirum.ds.imports.DataBatchBundle;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.services.AcquirerService;
import com.modirum.ds.services.ThreeDSProfileService;
import org.apache.commons.csv.CSVRecord;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.modirum.ds.util.DSUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class MerchantCSVParser extends AbstractCsvParser<BatchMerchantModel> {

     private static final String CSV_HEADER_NAME             = "name";
     private static final String CSV_HEADER_3DS_REQUESTOR_ID = "3dsRequestorId";
     private static final String CSV_HEADER_REGISTRATION_ID  = "registrationId";
     private static final String CSV_HEADER_ACQUIRER_BIN     = "acquirerBin";
     private static final String CSV_HEADER_3DS_OPERATOR_ID  = "3dssOperatorId";
     private static final String CSV_HEADER_EMAIL            = "email";
     private static final String CSV_HEADER_PHONE            = "phone";
     private static final String CSV_HEADER_URL              = "url";
     private static final String CSV_HEADER_COUNTRY          = "country";
     private static final String CSV_HEADER_EXTERNAL_OPTIONS = "externalOptions";
     private static final String CSV_HEADER_ACTION           = "action";
     private static final String CSV_HEADER_STATUS           = "status";
     private static final String CSV_PAYMENT_SYSTEM_ID       = "paymentSystemId";

     @Override
     protected Map<CSVHeaderFormat, HeaderFormat> getHeaderFormats() {
          Map<CSVHeaderFormat, HeaderFormat> headerFormats = new HashMap<>();
          headerFormats.put(CSVHeaderFormat.CHANGE_MERCHANT_STATE, new HeaderFormat(Arrays.asList(CSV_HEADER_NAME, CSV_HEADER_ACTION), true));
          headerFormats.put(CSVHeaderFormat.ADD_MERCHANT, new HeaderFormat( Arrays.asList(CSV_HEADER_NAME, CSV_HEADER_3DS_REQUESTOR_ID, CSV_HEADER_ACQUIRER_BIN, CSV_HEADER_3DS_OPERATOR_ID, CSV_HEADER_STATUS, CSV_HEADER_REGISTRATION_ID), true));
          return headerFormats;
     }

     @Override
     protected DataBatchBundle<BatchMerchantModel> handleHeaderFormat(final CSVHeaderFormat headerDataFormat, final List<String> headerNames, final List<CSVRecord> records) throws CSVParserException {
          List<BatchMerchantModel> importModels;
          switch (headerDataFormat) {
               case ADD_MERCHANT:
                    importModels = DSUtils.toList(records.stream().map(record -> mapRecordToMerchantBatchModel(headerNames, record)));
                    break;
               case CHANGE_MERCHANT_STATE:
                    importModels = DSUtils.toList(records.stream().map(this::mapRecordToMerchantChangeStateModel));
                    break;
               default:
                    throw new CSVParserException("Unsupported header data format: " + headerDataFormat);
          }
          return new DataBatchBundle<>(headerDataFormat, importModels);
     }

     private BatchMerchantModel mapRecordToMerchantChangeStateModel(final CSVRecord record) {
          final BatchMerchantModel.BatchMerchantModelBuilder builder = BatchMerchantModel.builder();
          builder.name(record.get(CSV_HEADER_NAME));
          if (record.isMapped(CSV_PAYMENT_SYSTEM_ID)) {
               builder.paymentSystemId(record.get(CSV_PAYMENT_SYSTEM_ID));
          }
          DataChangeStateOperation operation = DataChangeStateOperation.getValue(record.get(CSV_HEADER_ACTION).toUpperCase());
          if (DataChangeStateOperation.ADD.equals(operation)) {
               throw new IllegalStateException("'Add' is not valid action for this CSV file format");
          }
          builder.operation(operation);
          return builder.build();
     }

     private BatchMerchantModel mapRecordToMerchantBatchModel(final List<String> headerNames, final CSVRecord record) {
          final BatchMerchantModel.BatchMerchantModelBuilder builder = BatchMerchantModel.builder();
          builder.operation(DataChangeStateOperation.ADD);

          // Required fields
          setField(headerNames, CSV_HEADER_ACQUIRER_BIN, record, builder::acquirerBin);
          setField(headerNames, CSV_HEADER_3DS_OPERATOR_ID, record, builder::threeDSOperatorId);
          setField(headerNames, CSV_HEADER_REGISTRATION_ID, record, builder::registrationId);
          setField(headerNames, CSV_HEADER_REGISTRATION_ID, record, builder::registrationId);
          setField(headerNames, CSV_HEADER_NAME, record, builder::name);
          setField(headerNames, CSV_HEADER_3DS_REQUESTOR_ID, record, builder::requestorID);
          setField(headerNames, CSV_HEADER_STATUS, record, builder::status);

          // Optional fields
          setField(headerNames, CSV_PAYMENT_SYSTEM_ID, record, builder::paymentSystemId);
          setField(headerNames, CSV_HEADER_PHONE, record, builder::phone);
          setField(headerNames, CSV_HEADER_COUNTRY, record, builder::country);
          setField(headerNames, CSV_HEADER_EMAIL, record, builder::email);
          setField(headerNames, CSV_HEADER_URL, record, builder::URL);
          setField(headerNames, CSV_HEADER_EXTERNAL_OPTIONS, record, builder::externalOptions);
          return builder.build();
     }
}
