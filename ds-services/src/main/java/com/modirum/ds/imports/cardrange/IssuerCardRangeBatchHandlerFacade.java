package com.modirum.ds.imports.cardrange;

import com.modirum.ds.imports.AbstractCsvParser;
import com.modirum.ds.imports.AbstractBatchDataValidator;
import com.modirum.ds.imports.AbstractBatchHandlerFacade;
import com.modirum.ds.imports.AbstractOperationProcessorProvider;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.cardrange.operation.IssuerCardRangeOperationProcessorProvider;
import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.imports.cardrange.parser.IssuerCardRangeCSVParser;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;

public class IssuerCardRangeBatchHandlerFacade extends AbstractBatchHandlerFacade<BatchCardRangeModel> {

    public IssuerCardRangeBatchHandlerFacade(final PersistenceService persistenceService, final DataBatchHandlerConfiguration configuration) {
        super(persistenceService, configuration);
    }

    @Override
    protected AbstractOperationProcessorProvider createProcessorsProvider() {
        return new IssuerCardRangeOperationProcessorProvider(this.persistenceService);
    }

    @Override
    protected AbstractCsvParser<BatchCardRangeModel> createParser() {
        return new IssuerCardRangeCSVParser();
    }

    @Override
    protected AbstractBatchDataValidator<BatchCardRangeModel> createDataValidator() {
        return new IssuerCardRangeBatchModelValidator(this.configuration,
            ServiceLocator.getInstance().getIssuerService(),
            ServiceLocator.getInstance().getPaymentSystemService());
    }
}
