package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.AbstractOperationProcessorProvider;
import com.modirum.ds.imports.BaseBatchModel;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.imports.IDataOperationProcessor;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.db.dao.PersistenceService;

import java.util.HashMap;
import java.util.Map;

public class IssuerOperationProcessorProvider extends AbstractOperationProcessorProvider {

    private IssuerService issuerService;

    public IssuerOperationProcessorProvider(final PersistenceService persistenceService, final IssuerService issuerService) {
        super(persistenceService);
        this.issuerService = issuerService;
    }

    @Override
    public Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> initProviders() {
        Map<DataChangeStateOperation, IDataOperationProcessor<? extends BaseBatchModel>> processors = new HashMap<>();
        processors.put(DataChangeStateOperation.ADD, new AddIssuerOperationProcessor(this.persistenceService));
        processors.put(DataChangeStateOperation.DELETE, new DeleteIssuerOperationProcessor(this.persistenceService));
        processors.put(DataChangeStateOperation.DISABLE, new DisableIssuerOperationProcessor(this.issuerService));
        processors.put(DataChangeStateOperation.ACTIVATE, new ActivateIssuerOperationProcessor(this.issuerService));
        return processors;
    }
}
