package com.modirum.ds.imports;

public enum DataChangeStateOperation {
    ADD,
    DELETE,
    DISABLE,
    ACTIVATE;

    public static DataChangeStateOperation getValue(String val) {
        try {
            return valueOf(val);
        } catch (IllegalArgumentException exception) {
            throw new IllegalArgumentException(String.format("%s is not a valid action", val));
        }
    }
}
