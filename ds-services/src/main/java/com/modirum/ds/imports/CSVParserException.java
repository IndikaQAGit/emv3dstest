package com.modirum.ds.imports;

public class CSVParserException extends Exception {

    public CSVParserException(final String message) {
        super(message);
    }

    public CSVParserException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
