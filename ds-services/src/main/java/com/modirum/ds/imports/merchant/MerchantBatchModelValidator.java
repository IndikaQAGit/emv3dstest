package com.modirum.ds.imports.merchant;

import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.imports.AbstractBatchDataValidator;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.ValidationsList;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.imports.CSVHeaderFormat;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.providers.CountriesProvider;
import com.modirum.ds.services.AcquirerService;
import com.modirum.ds.services.PaymentSystemsService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ThreeDSProfileService;
import com.modirum.ds.util.DSUtils;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.modirum.ds.util.DSUtils.toSet;
import static java.util.stream.Collectors.groupingBy;

public class MerchantBatchModelValidator extends AbstractBatchDataValidator<BatchMerchantModel> {

    private final AcquirerService acquirerService;
    private final ThreeDSProfileService threeDSProfileService;
    private final PersistenceService persistenceService;

    public MerchantBatchModelValidator(final DataBatchHandlerConfiguration configuration,
                                       final AcquirerService acquirerService,
                                       final ThreeDSProfileService threeDSProfileService,
                                       final PersistenceService persistenceService,
                                       final PaymentSystemsService paymentSystemsService) {
        super(configuration, paymentSystemsService);
        this.acquirerService = acquirerService;
        this.persistenceService = persistenceService;
        this.threeDSProfileService = threeDSProfileService;
    }

    @Override
    protected Map<CSVHeaderFormat, ValidationsList<BatchMerchantModel>> initValidators() {
        final Map<CSVHeaderFormat, ValidationsList<BatchMerchantModel>> map = new HashMap<>();

        ValidationsList<BatchMerchantModel> addValidations = new ValidationsList<>();
        addValidations.add(this::isValidPaymentSystemId);
        addValidations.add(this::isValidCountries);
        addValidations.add(this::isValidAcquirerBins);
        addValidations.add(this::isValidEmails);
        addValidations.add(this::isValidMerchantNames);
        addValidations.add(this::isValidThreeDSProfileOperatorIDs);
        map.put(CSVHeaderFormat.ADD_MERCHANT, addValidations);

        ValidationsList<BatchMerchantModel> changeValidations = new ValidationsList<>();
        changeValidations.add(this::isValidPaymentSystemId);
        changeValidations.add(this::isValidMerchantNames);
        map.put(CSVHeaderFormat.CHANGE_MERCHANT_STATE, changeValidations);
        return map;
    }

    private boolean isValidPaymentSystemId(final FilterParams<BatchMerchantModel> params) {
        List<String> paymentSystemIds = params.importModels.stream().map(
                BatchMerchantModel::getPaymentSystemId).collect(Collectors.toList());

        return isValidPaymentSystemsIDs(params.continueOnError, paymentSystemIds);
    }

    private boolean isValidThreeDSProfileOperatorIDs(final FilterParams<BatchMerchantModel> params) {
        if (CSVHeaderFormat.CHANGE_MERCHANT_STATE.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        for (BatchMerchantModel merchantModel : params.importModels) {
            String threeDSProfileId = getTDSProfileId(merchantModel.getThreeDSOperatorId(), merchantModel.getPaymentSystemId());
            merchantModel.setThreeDSProfileId(threeDSProfileId);
            if (StringUtils.isEmpty(threeDSProfileId)) {
                addError(threeDsProfileNotExistError(merchantModel.getThreeDSOperatorId()));
                result = false;
                if (!params.continueOnError) {
                    return false;
                }
            }
        }

        return result;
    }

    private boolean isValidMerchantNames(final FilterParams<BatchMerchantModel> params) {
        List<BatchMerchantModel> batchMerchantModels =  params.importModels;
        boolean result = true;

        // skip test when there is error with payment system id.
        if (!isValidPaymentSystemId(params)) {
            return true;
        }

        Map<String, List<BatchMerchantModel>> paymentSystemIdToNames = batchMerchantModels.stream()
            .collect(groupingBy(BatchMerchantModel::getPaymentSystemId));

        for (String paymentSystemID : paymentSystemIdToNames.keySet()) {
            if (StringUtils.isEmpty(paymentSystemID)) {
                // Payment ID is not expected to be empty, but if it is empty here
                // the error already added in isValidPaymentSystemId method.
                continue;
            }
            List<String> names = paymentSystemIdToNames.get(paymentSystemID).stream()
                .map(BatchMerchantModel::getName)
                .collect(Collectors.toList());

            Set<String> duplicates = DSUtils.findDuplicates(names);
            if (!duplicates.isEmpty()) {
                result = false;
                addError(duplicatedNamesError(duplicates));
                if (!params.continueOnError) {
                    return result;
                }
            }

            for (String name : names) {
                Merchant merchant = persistenceService.getMerchantByNameAndPaymentSystemId(name, Integer.parseInt(paymentSystemID));
                if (CSVHeaderFormat.ADD_MERCHANT.equals(params.headerFormat)) {
                    if (merchant != null) {
                        result = false;
                        addError(merchantExistsError(name, paymentSystemID));
                        if (!params.continueOnError) {
                            return result;
                        }
                    }
                } else if (CSVHeaderFormat.CHANGE_MERCHANT_STATE.equals(params.headerFormat)) {
                    if (merchant == null) {
                        result = false;
                        addError(merchantDoesntExistsError(name, paymentSystemID));
                        if (!params.continueOnError) {
                            return result;
                        }
                    }
                }
            }
        }
        return result;
    }

    private boolean isValidEmails(final FilterParams<BatchMerchantModel> params) {
        if (CSVHeaderFormat.CHANGE_MERCHANT_STATE.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        EmailValidator validator = EmailValidator.getInstance();
        Set<String> emails = toSet(params.importModels.stream()
                                               .map(BatchMerchantModel::getEmail)
                                               .filter(Objects::nonNull));
        for (String email: emails) {
            if (!validator.isValid(email)) {
                result = false;
                addError(invalidEmailError(email));
                if (!params.continueOnError) {
                    return result;
                }
            }
        }

        return result;
    }

    private boolean isValidCountries(final FilterParams<BatchMerchantModel> params) {
        if (CSVHeaderFormat.CHANGE_MERCHANT_STATE.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        Set<String> actualCountryA2Codes = toSet(params.importModels.stream()
                                                                    .map(BatchMerchantModel::getCountry)
                                                                    .filter(Objects::nonNull)
                                                                    .filter(StringUtils::isNotBlank));
        List<String> existingA3CountriesCodes = CountriesProvider.getA2CountriesCodes();
        for (String a2CountryCode : actualCountryA2Codes) {
            if (!existingA3CountriesCodes.contains(a2CountryCode)) {
                addError(invalidA3CityCode(a2CountryCode));
                result = false;
                if (!params.continueOnError) {
                    return result;
                }
            }
        }
        return  result;
    }

    private boolean isValidAcquirerBins(final FilterParams<BatchMerchantModel> params) {
        if (CSVHeaderFormat.CHANGE_MERCHANT_STATE.equals(params.headerFormat)) {
            return true;
        }
        boolean result = true;
        Set<String> acquirerIds = new HashSet<>();
        for (BatchMerchantModel merchantModel : params.importModels) {
            String acquirerId = getAcquirerId(merchantModel.getAcquirerBin(), merchantModel.getPaymentSystemId());
            merchantModel.setAcquirerId(acquirerId);
            if (StringUtils.isEmpty(acquirerId)) {
                addError(acquirerNotExistError(merchantModel.getAcquirerBin()));
                result = false;
                if (!params.continueOnError) {
                    return false;
                }
            } else if (acquirerIds.contains(acquirerId)) {
                addError(acquirerDuplicateError(merchantModel.getAcquirerBin()));
                result = false;
                if (!params.continueOnError) {
                    return false;
                }
            }
            acquirerIds.add(acquirerId);
        }
        return result;
    }

    private String acquirerDuplicateError(final String acquirerBin) {
        return String.format("Duplicate acquirer bin. Bin = %s", acquirerBin);
    }

    private String acquirerNotExistError(final String acquirerBin) {
        return String.format("Acquirer with %s bin does not exist.", acquirerBin);
    }

    private String threeDsProfileNotExistError(final String threeDsOperatorId) {
        return String.format("3DS profile with %s operator id does not exist.", threeDsOperatorId);
    }


    private String invalidA3CityCode(final String countryA3Code) {
        return String.format("A2 Country code=%s is invalid. Expected 2 digit code in ISO 3166 format.", countryA3Code);
    }

    private String duplicatedNamesError(final Set<String> duplicates) {
        return String.format("Merchant's name should to be unique. Duplicates found: %s.", duplicates.toString().replace('[', ' ').replace(']', ' ').trim());
    }

    private String merchantExistsError(final String merchantName, final String paymentSystemId) {
        return String.format("Merchant with name = %s and Payment System ID = %s already exists", merchantName, paymentSystemId);
    }

    private String merchantDoesntExistsError(final String merchantName, final String paymentSystemId) {
        return String.format("Merchant with name = %s and Payment System ID = %s doesn't exists", merchantName, paymentSystemId);
    }

    private String invalidEmailError(final String email) {
        return String.format("Found invalid email: %s", email);
    }

    private String getAcquirerId(String acquirerBin, String paymentSystemId) {
        Acquirer acquirer = acquirerService.getAcquirer(acquirerBin, Misc.parseInt(paymentSystemId));
        return acquirer == null ? null : acquirer.getId().toString();
    }

    private String getTDSProfileId(String tdsOperatorId, String paymentSystemId) {
        TDSServerProfile tdsServerProfile = threeDSProfileService.getActiveTdsServerProfile(tdsOperatorId,
                                                                                            Misc.parseInt(paymentSystemId));
        return tdsServerProfile == null ? null : tdsServerProfile.getId().toString();

    }
}
