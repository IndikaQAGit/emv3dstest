package com.modirum.ds.imports.merchant;

import com.modirum.ds.imports.AbstractCsvParser;
import com.modirum.ds.imports.AbstractBatchHandlerFacade;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.AbstractOperationProcessorProvider;
import com.modirum.ds.imports.merchant.operation.MerchantOperationProcessorsProvider;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.imports.merchant.parser.MerchantCSVParser;
import com.modirum.ds.imports.AbstractBatchDataValidator;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;

/**
 * Holds the logic that creates DataImporter and its dependencies.
 */
public class MerchantBatchHandlerFacade extends AbstractBatchHandlerFacade<BatchMerchantModel> {

    public MerchantBatchHandlerFacade(final PersistenceService persistenceService, final DataBatchHandlerConfiguration configuration) {
        super(persistenceService, configuration);
    }

    @Override
    protected AbstractOperationProcessorProvider createProcessorsProvider() {
        return new MerchantOperationProcessorsProvider(this.persistenceService, ServiceLocator.getInstance().getMerchantService());
    }

    @Override
    protected AbstractCsvParser<BatchMerchantModel> createParser() {
        return new MerchantCSVParser();
    }

    @Override
    protected AbstractBatchDataValidator<BatchMerchantModel> createDataValidator() {
        return new MerchantBatchModelValidator(this.configuration,
                    ServiceLocator.getInstance().getAcquirerService(),
                    ServiceLocator.getInstance().getThreeDSProfileService(),
                    ServiceLocator.getInstance().getPersistenceService(),
                    ServiceLocator.getInstance().getPaymentSystemService());
    }
}
