package com.modirum.ds.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enumerated keys for DS Init
 */
@AllArgsConstructor
public enum DSInitKey {
    ENABLE_INIT_ADMIN("enableInitAdmin");

    @Getter
    private final String key;
}
