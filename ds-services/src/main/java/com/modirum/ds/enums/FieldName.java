package com.modirum.ds.enums;

/**
 * Enumerates EMV 3DS messaging protocol field names. Does not replicate Json structure (objects/sub-elements).
 */
public class FieldName {

    public static final String authenticationMethod = "authenticationMethod";
    public static final String acctNumber = "acctNumber";
    public static final String authenticationValue = "authenticationValue";
    public static final String threeDSServerOperatorID = "threeDSServerOperatorID";
    public static final String deviceInfo = "deviceInfo";
    public static final String sdkEncData = "sdkEncData";
    public static final String acsSignedContent = "acsSignedContent";
    public static final String acsEphemPubKey = "acsEphemPubKey";
    public static final String sdkEphemPubKey = "sdkEphemPubKey";
    public static final String messageExtension = "messageExtension";
    public static final String eci = "eci";
    public static final String transStatus = "transStatus";
    public static final String messageCategory = "messageCategory";
    public static final String transStatusReason = "transStatusReason";
    public static final String purchaseCurrency = "purchaseCurrency";
    public static final String acsOperatorID = "acsOperatorID";
    public static final String browserJavaEnabled = "browserJavaEnabled";
    public static final String threeDSRequestorPriorAuthenticationInfo = "threeDSRequestorPriorAuthenticationInfo";
    public static final String acquirerBIN = "acquirerBIN";
    public static final String acquirerMerchantID = "acquirerMerchantID";
    public static final String acctInfo = "acctInfo";
    public static final String acctType = "acctType";
    public static final String billAddrCity = "billAddrCity";
    public static final String billAddrCountry = "billAddrCountry";
    public static final String billAddrLine1 = "billAddrLine1";
    public static final String billAddrLine2 = "billAddrLine2";
    public static final String billAddrLine3 = "billAddrLine3";
    public static final String billAddrPostCode = "billAddrPostCode";
    public static final String billAddrState = "billAddrState";
    public static final String shipAddrCity = "shipAddrCity";
    public static final String shipAddrCountry = "shipAddrCountry";
    public static final String shipAddrLine1 = "shipAddrLine1";
    public static final String shipAddrLine2 = "shipAddrLine2";
    public static final String shipAddrLine3 = "shipAddrLine3";
    public static final String shipAddrPostCode = "shipAddrPostCode";
    public static final String shipAddrState = "shipAddrState";
    public static final String mobilePhone = "mobilePhone";
    public static final String mcc = "mcc";
    public static final String merchantName = "merchantName";
    public static final String merchantRiskIndicator = "merchantRiskIndicator";
    public static final String serialNum = "serialNum";
    public static final String cardholderName = "cardholderName";
    public static final String threeDSRequestorAuthenticationInfo = "threeDSRequestorAuthenticationInfo";
    public static final String threeDSRequestorAuthenticationInd = "threeDSRequestorAuthenticationInd";
    public static final String threeDSRequestorChallengeInd = "threeDSRequestorChallengeInd";
    public static final String threeRIInd = "threeRIInd";
    public static final String merchantCountryCode = "merchantCountryCode";
    public static final String addrMatch = "addrMatch";
    public static final String acctID = "acctID";
    public static final String deviceChannel = "deviceChannel";
    public static final String broadInfo = "broadInfo";
    public static final String cardExpiryDate = "cardExpiryDate";
    public static final String recurringExpiry = "recurringExpiry";
    public static final String recurringFrequency = "recurringFrequency";
    public static final String challengeCancel= "challengeCancel";
    public static final String dsReferenceNumber = "dsReferenceNumber";
    public static final String dsStartProtocolVersion = "dsStartProtocolVersion";
    public static final String dsEndProtocolVersion = "dsEndProtocolVersion";
    public static final String dsURL= "dsURL";
    public static final String interactionCounter= "interactionCounter";

    //2.2.0 Additions
    public static final String threeDSRequestorDecMaxTime = "threeDSRequestorDecMaxTime";
    public static final String threeDSRequestorDecReqInd = "threeDSRequestorDecReqInd";
    public static final String whiteListStatus = "whiteListStatus";
    public static final String whiteListStatusSource = "whiteListStatusSource";
    public static final String threeDSReqAuthMethodInd = "threeDSReqAuthMethodInd";

    //2.3.0 Additions
    public static final String threeDSMethodId = "threeDSMethodId";
    public static final String acceptLanguage = "acceptLanguage";
    public static final String acquirerCountryCode = "acquirerCountryCode";
    public static final String acquirerCountryCodeSource = "acquirerCountryCodeSource";
    public static final String acsRenderingType = "acsRenderingType";
    public static final String appIp = "appIp";
    public static final String deviceBindingStatus = "deviceBindingStatus";
    public static final String deviceBindingStatusSource = "deviceBindingStatusSource";
    public static final String deviceInfoRecognisedVersion = "deviceInfoRecognisedVersion";
    public static final String deviceRenderOptions = "deviceRenderOptions";
    public static final String multiTransaction = "multiTransaction";
    public static final String sdkServerSignedContent = "sdkServerSignedContent";
    public static final String sdkType = "sdkType";
    public static final String taxId = "taxId";
    public static final String trustListStatus = "trustListStatus";
    public static final String trustListStatusSource = "trustListStatusSource";
    public static final String payTokenInd = "payTokenInd";
    public static final String payTokenSource = "payTokenSource";


    // Added for erro message validation
    public static final String messageVersion = "messageVersion";
    public static final String errorCode = "errorCode";
    public static final String errorComponent = "errorComponent";
    public static final String errorDescription = "errorDescription";
    public static final String errorDetail = "errorDetail";
    public static final String errorMessageType = "errorMessageType";
}
