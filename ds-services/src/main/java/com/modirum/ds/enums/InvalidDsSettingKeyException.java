package com.modirum.ds.enums;

public class InvalidDsSettingKeyException extends Exception {
    public InvalidDsSettingKeyException(String message) {
        super(message);
    }
}
