package com.modirum.ds.enums;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum ReportStatus {
    QUEUED("Q", "text.report.status.queued"),
    PROCESSING("P", "text.report.status.processing"),
    COMPLETED("C", "text.report.status.completed"),
    ERROR("E", "text.report.status.error"),
    DELETED("D", "text.report.status.deleted"),
    UNKNOWN("U", "text.report.status.unknown");

    private String label;
    private String description;

    private ReportStatus(String label, String description) {
        this.label = label;
        this.description = description;
    }

    private static final Map<String, ReportStatus> REPORT_STATUS_MAP;

    static {
        HashMap<String, ReportStatus> reportStatusMap = new HashMap<>();
        for (ReportStatus reportStatus : ReportStatus.values()) {
            reportStatusMap.put(reportStatus.label, reportStatus);
        }
        REPORT_STATUS_MAP = Collections.unmodifiableMap(reportStatusMap);
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public static ReportStatus getByReportStatusLabel(String key) {
        return REPORT_STATUS_MAP.get(key);
    }

    public static Map<String, ReportStatus> getMap() {
        return REPORT_STATUS_MAP;
    }

}
