package com.modirum.ds.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enumerated type of PaymentSystems with custom EMV 3DS message handling.
 */
public enum PaymentSystemType {

    eftpos,
    elo;

    /**
     * Looks up Payment System type by name.
     * @param lookupName name to look up.
     * @return Payment System type optional value.
     */
    public static Optional<PaymentSystemType> getPaymentSystemType(String lookupName) {
        return Arrays.stream(PaymentSystemType.values()).filter(psType -> psType.name().equals(lookupName)).findFirst();
    }

    public boolean isEqual(String type){
        return name().equalsIgnoreCase(type);
    }
}
