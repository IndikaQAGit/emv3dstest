package com.modirum.ds.enums;

/**
 * Enumerated keys for Ds Settings
 */
public enum DsSetting {

    /****
     * DS Core Settings
     ****/

    /**
     * @Deprecated
     * A backwards compatibility configuration for a DS service port number shared between 2 payment systems.
     * 2 payment system IDs specified under this setting will share a single port number and have _both_ 3DSS namespaces
     * scanned using operatorID. The located 3DSS.paymentSystemId will be selected as transaction's payment system id.
     * Values accepted: {digit},{digit}
     * Example value: 1,2
     */
    JOINT_PAYMENT_SYSTEMS("jointPaymentSystemIds", DsSettingType.STRING, DsSettingSection.DS_CORE),
    /**
     * Trust Model Requirements - via registered 3DS Profile, any acquirer and merchant are trusted
     */
    ENABLE_TRUST_ALL_MERCHANTS("enableTrustAllMerchants", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    /**
     * Setting key for configuration that states whether to store encrypted pan in the database.
     */
    ENABLE_STORING_ENCRYPTED_PAN_KEY("enableStoringEncPan", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    /**
     * Setting key for configuration that states whether to store hashed pan in the database.
     */
    ENABLE_STORING_HASHED_PAN_KEY("enableStoringHashedPan", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    /** Enables storing Authentication Value, if exists and is "true". */
    STORE_AUTHENTICATION_VALUE("storeAuthenticationValue", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    /** Enables storing Device Info, if exists and is "true". */
    STORE_DEVICE_INFO("storeDeviceInfo", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    /** Enables storing SDK encrypted data, if exists and is "true". */
    STORE_SDK_ENC_DATA("storeSdkEncData", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    /** Enables storing ACS and SDK ephemeral public keys, if exists and is "true". */
    STORE_EPHEM_PUB_KEYS("storeEphemPubKeys", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    DS_BUILD_VERSION("DS.buildVersion", DsSettingType.STRING, DsSettingSection.DS_CORE),
    DS_RELOAD_RANGES("DS.reloadRanges", DsSettingType.INT, DsSettingSection.DS_CORE),
    DS_VERSIONS_ENABLED("DS.versionsEnabled", DsSettingType.STRING, DsSettingSection.DS_CORE),
    DS_VERSION_DEFAULT("DS.versionDefault", DsSettingType.STRING, DsSettingSection.DS_CORE),
    DS_MAX_POOL_THREADS("DS.maxPoolThreads", DsSettingType.INT, DsSettingSection.DS_CORE),
    DS_SUPPORT_RANGE_DIFF_UPDATES("DS.supportRangeDiffUpdates", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    DS_PUBLIC_URL("DSPublicURL", DsSettingType.STRING, DsSettingSection.DS_CORE),
    HTTP_USERAGENT("http.userAgent", DsSettingType.STRING, DsSettingSection.DS_CORE),
    HTTP_PROXY_HOST("http.proxy.host", DsSettingType.STRING, DsSettingSection.DS_CORE),
    HTTP_PROXY_PORT("http.proxy.port", DsSettingType.INT, DsSettingSection.DS_CORE),
    SMS_CLASS("com.modirum.authserver.services.SMSService", DsSettingType.STRING, DsSettingSection.DS_CORE),
    PROXY_TYPE("proxyType", DsSettingType.STRING, DsSettingSection.DS_CORE),
    VIA_PROXY_ENABLED("viaProxyEnabled", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    PATCH_PURCHASE_DATE_FORMAT("patchPurchaseDateFormat", DsSettingType.DATE_FORMAT, DsSettingSection.DS_CORE),
    SERVICE_URL("serviceUrl", DsSettingType.STRING, DsSettingSection.DS_CORE),
    CONNECT_TIMEOUT("connectTimeout", DsSettingType.INT, DsSettingSection.DS_CORE),
    DEBUG_THREAD_INFO("debugThreadInfo", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),
    EXTENSION_SERVICE("ExtensionService", DsSettingType.STRING, DsSettingSection.DS_CORE),
    /** dsReferenceNumber value that DS will using in EMV protocol messaging **/
    DS_REFERENCE_NUMBER("dsReferenceNumber", DsSettingType.STRING, DsSettingSection.DS_CORE),
    /** Enables logging of full PRes Message, if exists and is "true". */
    ENABLE_LOGGING_FULL_PRES("enableLoggingFullPres", DsSettingType.BOOLEAN, DsSettingSection.DS_CORE),

    /****
     * Importer Service Settings
     ****/

    IMPORT_GEN_RESULT_FILE("importGenResultFile", DsSettingType.BOOLEAN, DsSettingSection.IMPORTER_SERVICE),
    IMPORT_MAX_TX_SIZE("importMaxTxSize", DsSettingType.INT, DsSettingSection.IMPORTER_SERVICE),
    IMPORT_SESSION_ID("importSessionId", DsSettingType.INT, DsSettingSection.IMPORTER_SERVICE),

    /****
     * DS Manager Settings
     ****/

    MNGR_CSS("mngr.css", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_TIMEZONES("mngr.timeZones", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_SSL_AUTHPROXY_ENABLED("mngr.sslAuthProxyEnabled", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),
    MNGR_SSL_AUTHPROXY_TYPE("mngr.sslAuthProxyType", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_FACTOR2AUTHENTICATIONS("mngr.factor2Authentications", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_SSL_AUTH_ENABLED("mngr.sslAuthEnabled", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),
    MNGR_SSL_AUTHPROXY_URL("mngr.sslAuthProxyUrl", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_USER_NAME_MINLENGTH("mngr.user.name.minlength", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_MINLENGTH("mngr.user.password.minlength", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_DIFF("mngr.user.password.diff", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_SYMBOLS("mngr.user.password.symbols", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_COMPLEXITIES_REQUIRED("mngr.user.password.complexities.required", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_LOWERCASE_MIN_REQUIRED("mngr.user.password.lowercase.min.required", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_LOWERCASE_MAX_SEQUENCE("mngr.user.password.lowercase.max.sequence", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_UPPERCASE_MIN_REQUIRED("mngr.user.password.uppercase.min.required", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_UPPERCASE_MAX_SEQUENCE("mngr.user.password.uppercase.max.sequence", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_NUMBERS_MIN_REQUIRED("mngr.user.password.numbers.min.required", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_NUMBERS_MAX_SEQUENCE("mngr.user.password.numbers.max.sequence", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_SYMBOLS_MIN_REQUIRED("mngr.user.password.symbols.min.required", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_USER_PASSWORD_SYMBOLS_MAX_SEQUENCE("mngr.user.password.symbols.max.sequence", DsSettingType.INT, DsSettingSection.DS_MNGR),
    MNGR_LOCALE("mngr.locale", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    SETTINGS_ACCESS_LIST("settingsAccessList", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    MNGR_USE_STRONG_PASSWORD("mngr.useStrongPassword", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),
    MNGR_BIN_LENGTH("mngr.binLength", DsSettingType.INT, DsSettingSection.DS_MNGR),
    /**
     * This is the Setting key for the record limit of tdsRecord csv export.
     */
    TDS_RECORD_CSV_EXPORT_LIMIT("3dsRecordsCsvExportLimit", DsSettingType.LONG, DsSettingSection.DS_MNGR),
    /**
     * Setting key to disable the ip whitelisting in ds manager in accessing Keys/Settings page.
     */
    DISABLE_DS_MNGR_IP_WHITELISTING("disableDsMngrIpWhitelisting", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),
    AUDIT_LOG_SEARCHES("auditlog.searches", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),
    RESET_TEXT_CACHE("reset.text.cache", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),
    SCHEME_NAME("scheme.name", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    SCHEME_CONTACT("scheme.contact", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    SCHEME_LOGO("scheme.logo", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    SCHEME_WEBSITE("scheme.website", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    SETTINGS_ENUM_ENABLED("settingsEnumEnabled", DsSettingType.BOOLEAN, DsSettingSection.DS_MNGR),

    /****
     * Product License Settings
     ****/

    PRODUCT_NAME("product.name", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_VERSION("product.version", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_LICENSEE("product.licensee", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_MAX_MERCHANTS("product.maxmerchants", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_LICENSE_EXP("product.licenseexp", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_LICENSE_ISSUED("product.licenseissued", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_LICENSE_KEY("product.licensekey", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),
    PRODUCT_EXTENSIONS("product.extensions", DsSettingType.STRING, DsSettingSection.PRODUCT_LICENSE),

    /****
     * Issuer and ACS Settings
     ****/

    ISSUER_BIN_STORE_LENGTH("issuer.BINstoreLength", DsSettingType.INT, DsSettingSection.ISSUER_ACS),
    ISSUER_RANGE_MODE("issuer.rangeMode", DsSettingType.BOOLEAN, DsSettingSection.ISSUER_ACS),
    ISSUER_MIN_RANGE_SIZE("issuer.minRangeSize", DsSettingType.INT, DsSettingSection.ISSUER_ACS),
    ISSUER_MAX_BIN_LENGTH("issuer.maxBinLength", DsSettingType.INT, DsSettingSection.ISSUER_ACS),
    ATTEMPTS_ACS_ENABLED("attempts.acs.enabled", DsSettingType.BOOLEAN, DsSettingSection.ISSUER_ACS),
    ACS_SSL_CLIENT_CERTID("acs.ssl.client.certId", DsSettingType.LONG, DsSettingSection.ISSUER_ACS),
    ACS_SSL_CLIENT_PROTO("acs.ssl.client.proto", DsSettingType.STRING, DsSettingSection.ISSUER_ACS),
    ACS_SSL_CLIENT_TRUSTANY("acs.ssl.client.trustany", DsSettingType.BOOLEAN, DsSettingSection.ISSUER_ACS),
    ACS_SSL_CLIENT_VERIFY_HOSTNAME("acs.ssl.client.verifyhostname", DsSettingType.BOOLEAN, DsSettingSection.ISSUER_ACS),
    ACS_SSL_CLIENT_CIPHERS("acs.ssl.client.ciphers", DsSettingType.STRING, DsSettingSection.ISSUER_ACS),
    ATTEMPT_ACS_ON_ARES_VALIDATION_ERROR("attemptACSOnAResValidationError", DsSettingType.BOOLEAN, DsSettingSection.ISSUER_ACS),
    STATUS_ACS_ERROR("statusACSError", DsSettingType.STRING, DsSettingSection.ISSUER_ACS),
    STATUS_ACS_IDS_ERROR("statusACSIDSError", DsSettingType.STRING, DsSettingSection.ISSUER_ACS),
    STATUS_REASON_ACS_ERROR("statusReasonACSError", DsSettingType.STRING, DsSettingSection.ISSUER_ACS),
    ACS_TIMEOUT("acsTimeout", DsSettingType.INT, DsSettingSection.ISSUER_ACS),
    VIRTUAL_ACS_SERVICE("VirtualACSService", DsSettingType.BOOLEAN, DsSettingSection.ISSUER_ACS),

    /****
     * 3DS Server Settings
     ****/

    TDSSERVER_SSL_CLIENT_CERTID("tdsserver.ssl.client.certId", DsSettingType.LONG, DsSettingSection.TDS_SERVER),
    TDSSERVER_SSL_CLIENT_PROTO("tdsserver.ssl.client.proto", DsSettingType.STRING, DsSettingSection.TDS_SERVER),
    TDSSERVER_SSL_CLIENT_CIPHERS("tdsserver.ssl.client.ciphers", DsSettingType.STRING, DsSettingSection.TDS_SERVER),
    TDSSERVER_SSL_CLIENT_TRUSTANY("tdsserver.ssl.client.trustany", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),
    TDSSERVER_SSL_CLIENT_VERIFY_HOSTNAME("tdsserver.ssl.client.verifyhostname", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),
    ACCEPTED_TDSSERVER_LIST("acceptedTDSServerList", DsSettingType.STRING, DsSettingSection.TDS_SERVER),
    SKIP_ACCEPTED_SDK_LIST_CHECK("skipAcceptedSDKListCheck", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),
    ACCEPTED_MCC_LIST("acceptedMCCList", DsSettingType.STRING, DsSettingSection.TDS_SERVER),
    TDSSERVER_TIMEOUT("tdsServerTimeout", DsSettingType.INT, DsSettingSection.TDS_SERVER),
    REQUESTOR_ID_WITH_MERCHANT("requestorIDwithMerchant", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),
    SINGLE_MERCHANT_PER_3DSS("singleMerchantPer3DSS", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),
    VALIDATE_MERCHANT_COUNTRY_CODE_MATCH("validateMerchantCountryCodeMatch", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),
    SUPER_TDSSERVER_INCLIENT_CERTID("superTDSServerInClientCertId", DsSettingType.LONG, DsSettingSection.TDS_SERVER),
    SUPER_TDSSERVER_OUTCLIENT_CERTID("superTDSServerOutClientCertId", DsSettingType.LONG, DsSettingSection.TDS_SERVER),
    DECRYPT_DEVICE_DATA_ALL_NIMBUS("decryptDeviceDataAllNimbus", DsSettingType.BOOLEAN, DsSettingSection.TDS_SERVER),

    /****
     * Cleanup Job Settings
     ****/

    CLEANUP_DSCR("cleanup.dscr", DsSettingType.INT, DsSettingSection.CLEAN_UP_JOB),
    CLEANUP_ALL("cleanup.all", DsSettingType.INT, DsSettingSection.CLEAN_UP_JOB),
    CLEANUP_ALL_RUN("cleanup.all.run", DsSettingType.INT, DsSettingSection.CLEAN_UP_JOB),
    CLEANUP_CHD("cleanup.chd", DsSettingType.INT, DsSettingSection.CLEAN_UP_JOB),
    CLEANUP_CHD_RUN("cleanup.chd.run", DsSettingType.INT, DsSettingSection.CLEAN_UP_JOB),

    /****
     * Modirum ID Client Service Settings
     ****/

    MODIRUM_ID_AS_CLIENT_SERVICE_OTP_ONLY("ModirumIDASClientService.otponly", DsSettingType.BOOLEAN, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_AS_WS_URL("ModirumIDASClientService.aswsurl", DsSettingType.STRING, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_SSL_CLIENT_PROTO("ModirumIDASClientService.ssl.client.proto", DsSettingType.STRING, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_SSL_CLIENT_KEY_CERTID("ModirumIDASClientService.ssl.client.keyCertId", DsSettingType.LONG, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_SSL_TRUSTANY("ModirumIDASClientService.ssl.trustany", DsSettingType.BOOLEAN, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_SSL_HOST_VERIFY("ModirumIDASClientService.ssl.host.verify", DsSettingType.BOOLEAN, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_ISSUER_ID("ModirumIDASClientService.issuerId", DsSettingType.STRING, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_SSL_TRUSTED_CERTS("ModirumIDASClientService.sslTrustedCerts", DsSettingType.STRING, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_SSL_KEYMANAGERFACTORY_TYPE("ModirumIDASClientService.sslKeyManagerFactoryType", DsSettingType.STRING, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),
    MODIRUM_ID_AS_CLIENT_SERVICE_WS_TIMEOUT("ModirumIDASClientService.wstimeout", DsSettingType.INT, DsSettingSection.MODIRUM_ID_AS_CLIENT_SERVICE),

    /****
     * FSS Settings
     ****/

    FSS_ENABLED("fss.enabled", DsSettingType.BOOLEAN, DsSettingSection.FSS),
    FSS_DEFAULT_MER_OPTIONS("fss.defaultMerOptions", DsSettingType.STRING, DsSettingSection.FSS),
    FSS_PAN_HASH("fss.panhash", DsSettingType.BOOLEAN, DsSettingSection.FSS),
    FSS_SERVICE_URL("fss.serviceUrl", DsSettingType.STRING, DsSettingSection.FSS),
    FSS_CLIENT_CERTID("fss.client.certId", DsSettingType.LONG, DsSettingSection.FSS),
    FSS_SSL_CLIENT_PROTO("fss.ssl.client.proto", DsSettingType.STRING, DsSettingSection.FSS),
    FSS_SSL_CLIENT_CIPHERS("fss.ssl.client.ciphers", DsSettingType.STRING, DsSettingSection.FSS),
    FSS_SSL_CLIENT_TRUSTANY("fss.ssl.client.trustany", DsSettingType.BOOLEAN, DsSettingSection.FSS),
    FSS_SSL_CLIENT_VERIFY_HOSTNAME("fss.ssl.client.verifyhostname", DsSettingType.BOOLEAN, DsSettingSection.FSS),
    FSS_AREQ_MAXWAIT("fss.AReq.maxWait", DsSettingType.INT, DsSettingSection.FSS),
    FSS_RESPONSE_TIMEOUT("fss.responseTimeout", DsSettingType.INT, DsSettingSection.FSS),
    FSS_CONNECT_TIMEOUT("fss.connectTimeout", DsSettingType.INT, DsSettingSection.FSS),
    FSS_RETRY_WAIT("fss.retry.wait", DsSettingType.INT, DsSettingSection.FSS),

    /****
     * 3DS Method Relay Settings
     ****/

    TDSM_RELAY_COOKIE_NAME("3DSMRelay.cookieName", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_COOKIE_DOMAIN("3DSMRelay.cookieDomain", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_COOKIE_MAXAGEDAYS("3DSMRelay.cookieMaxAgeDays", DsSettingType.INT, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_SCRIPT_JSP("3DSMRelay.scriptjsp", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_URI("3DSMRelay.URI", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_FP_SCRIPT_URL("3DSMRelay.fpScriptURL", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_DD_VERSION("3DSMRelay.ddVersion", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_SKIP_CAPTURE_HEADERS("3DSMRelay.skipCaptureHeaders", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_EVERCOOKIE_URI("3DSMRelay.evercookieURI", DsSettingType.STRING, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_CAPTURE_IP("3DSMRelay.captureIP", DsSettingType.BOOLEAN, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_CAPTURE_HEADERS("3DSMRelay.captureHeaders", DsSettingType.BOOLEAN, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_CAPTURE_COOKIE("3DSMRelay.captureCookie", DsSettingType.BOOLEAN, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_ADD_COOKIE_IF_MISSING("3DSMRelay.addCookieIfMissing", DsSettingType.BOOLEAN, DsSettingSection.TDSM_RELAY),
    TDSM_RELAY_USE_EVERCOOKIE("3DSMRelay.useEverCookie", DsSettingType.BOOLEAN, DsSettingSection.TDSM_RELAY),
    DS_3DSM_RELAY_ENABLED("DS.3DSMRelayEnabled", DsSettingType.BOOLEAN, DsSettingSection.TDSM_RELAY),

    /****
     * Stage 2 Authentication Settings
     ****/

    STAGE2AUTHENTICATION_CHECK_SDN_IP_MATCH_ACQUIRER("stage2authentication.checksdnipmatchAcquirer", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_CN_DNS_MATCH_ACQUIRER("stage2authentication.checkSdnCnDNSMatchAcquirer", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_CN_DNS_MATCH_ISSUER("stage2authentication.checkSdnCnDNSMatchIssuer", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_BIN_MATCH("stage2authentication.checksdnbinmatch", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_MID_MATCH("stage2authentication.checksdnmidmatch", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_REQUESTOR_ID_MATCH("stage2authentication.checksdnrequestoridmatch", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_OPERATOR_ID_MATCH("stage2authentication.checksdnoperatoridmatch", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_IP_MATCH_ISSUER("stage2authentication.checksdnipmatchIssuer", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_ACS_REFNO("stage2authentication.checksdnACSRefNo", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),
    STAGE2AUTHENTICATION_CHECK_SDN_ACS_OPERATORID("stage2authentication.checksdnACSOperatorID", DsSettingType.BOOLEAN, DsSettingSection.STAGE2AUTHENTICATION),

    /****
     * EMV Fields Masking Settings
     ****/

    TDS_MASK_ACCT_ID("3ds.mask.acctID", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_BILLADDRLINE1("3ds.mask.billAddrLine1", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_BILLADDRLINE2("3ds.mask.billAddrLine2", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_BILLADDRLINE3("3ds.mask.billAddrLine3", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_CARDHOLDERNAME("3ds.mask.cardholderName", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_HOMEPHONE("3ds.mask.homePhone", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_MOBILEPHONE("3ds.mask.mobilePhone", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_SHIPADDRLINE1("3ds.mask.shipAddrLine1", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_SHIPADDRLINE2("3ds.mask.shipAddrLine2", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_SHIPADDRLINE3("3ds.mask.shipAddrLine3", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_WORKPHONE("3ds.mask.workPhone", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_EMAIL("3ds.mask.email", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),
    TDS_MASK_MERCHANT_RISK_INDICATOR_DELIVERY_EMAILADDRESS("3ds.mask.merchantRiskIndicator.deliveryEmailAddress", DsSettingType.REGEX_PATTERN, DsSettingSection.EMV_FIELDS_MASKING),

    /****
     * EMV Fields Validation Settings
     ****/
    SUPPORTED_PURCHASE_CURRENCIES("supportedPurchaseCurrencies", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    VALIDATE_CURRENCY_EXPONENT("validateCurrencyExponent", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    STRICT_VALIDATION("strictValidation", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    AREQ_BR_BROWSER_IP_REQUIRED("AReq.BR.browserIP.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    EXTRA_REQUESTOR_CHALLENGE_INDS("extraRequestorChallengeInds", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    EXTRA_AUTHENTICATION_TYPES("extraAuthenticationTypes", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    ARES_NPA_AUTHENTICATION_VALUE_REQUIRED("ARes.NPA.authenticationValue.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    ARES_3RI_AUTHENTICATION_VALUE_REQUIRED("ARes.3RI.authenticationValue.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    ACCEPTED_ARES_STATUSES("acceptedAResStatuses", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    EXTRA_RESULT_STATUSES("extraResultStatuses", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    RREQ_NPA_AUTHENTICATION_VALUE_REQUIRED("RReq.NPA.authenticationValue.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    RREQ_3RI_AUTHENTICATION_VALUE_REQUIRED("RReq.3RI.authenticationValue.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    RREQ_NPA_TRANS_STATUS_REASON("RReq.NPA.transStatusReason", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    RREQ_ECI_REQUIRED("RReq.ECI.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    ACCEPTED_RREQ_STATUSES("acceptedRReqStatuses", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    ARES_ECI_REQUIRED("ARes.ECI.required", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),
    ARES_TRANS_STATUS_A_ECI("ARes.transStatusA.ECI", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    ARES_TRANS_STATUS_U_ECI("ARes.transStatusU.ECI", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    ARES_TRANS_STATUS_A_AUTHENTICATION_VALUE("ARes.transStatusA.AuthenticationValue", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    EXTRA_COUNTRIES("extraCountries", DsSettingType.STRING, DsSettingSection.EMV_FIELDS_VALIDATION),
    TREAT_EMPTY_VALUE_AS_MISSING("treatEmptyValueAsMissing", DsSettingType.BOOLEAN, DsSettingSection.EMV_FIELDS_VALIDATION),

    /****
     * SSL Certificate Settings
     ****/
    MAX_SSL_CERT_DAYS("maxSSLCertDays", DsSettingType.INT, DsSettingSection.SSL_CERTIFICATE),
    SSL_ISSUER_SDNS("sslIssuerSDNs", DsSettingType.STRING, DsSettingSection.SSL_CERTIFICATE),
    SSL_ACQUIRER_SDNS("sslAcquirerSDNs", DsSettingType.STRING, DsSettingSection.SSL_CERTIFICATE),
    SSL_CERTS_MANAGED_EXTERNALLY("sslCertsManagedExternally", DsSettingType.BOOLEAN, DsSettingSection.SSL_CERTIFICATE),
    SSL_CLIENT_KEYFACTORY("ssl.client.keyfactory", DsSettingType.STRING, DsSettingSection.SSL_CERTIFICATE),
    DSCA_CRL_URL("DSCA.CRLURL", DsSettingType.STRING, DsSettingSection.SSL_CERTIFICATE),
    DSCA_AIA_URL("DSCA.AIAURL", DsSettingType.STRING, DsSettingSection.SSL_CERTIFICATE),
    ACCEPT_CONTAINER_CERT_AS_AUTH("acceptContainerCertAsAuth", DsSettingType.BOOLEAN, DsSettingSection.SSL_CERTIFICATE),

    /****
     * EMV Message Error Settings
     ****/
    ARES_IN_ERROR_SEND_ERRO_ACS("ARes.inErrorSendErroACS", DsSettingType.BOOLEAN, DsSettingSection.EMV_MESSAGE_ERROR),
    RRES_IN_ERROR_SEND_ERRO_3DS("RRes.inErrorSendErro3DS", DsSettingType.BOOLEAN, DsSettingSection.EMV_MESSAGE_ERROR),
    STATUS_REASON_BIN_NOT_FOUND("statusReasonBINNotFound", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    STATUS_BIN_NOT_FOUND("statusBINNotFound", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    STATUS_REASON_BIN_NOT_PARTICIPATING("statusReasonBINNotParticipating", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    STATUS_BIN_NOT_PARTICIPATING("statusBINNotParticipating", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    STATUS_REASON_ISSUER_NOT_ACTIVE("statusReasonIssuerNotActive", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    STATUS_ISSUER_NOT_ACTIVE("statusIssuerNotActive", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    RREQ_DUPLICATE_ERROR_CODE("RReq.duplicate.errorcode", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),
    RREQ_IN_ERROR_SEND_3DS("RReq.inErrorSend3DS", DsSettingType.STRING, DsSettingSection.EMV_MESSAGE_ERROR),

    /***
     * Report Settings
     */
    REPORTS_DOWNLOAD_URL_PREFIX("reports.download.url.prefix", DsSettingType.STRING, DsSettingSection.DS_MNGR),
    REPORTS_AUTO_REFRESH_SECONDS("reports.autorefresh.seconds", DsSettingType.INT, DsSettingSection.DS_MNGR);

    ;
    private String key;
    private DsSettingType type;
    private DsSettingSection section;

    DsSetting(String settingKey, DsSettingType type, DsSettingSection section) {
        this.key = settingKey;
        this.type = type;
        this.section = section;
    }

    public static DsSetting keyOf(String key) throws InvalidDsSettingKeyException {
        for (DsSetting dsSetting : DsSetting.values()) {
            if (dsSetting.key.equals(key)) {
                return dsSetting;
            }
        }

        throw new InvalidDsSettingKeyException("Invalid DsSetting key: " + key);
    }

    public String getKey() {
        return key;
    }

    public DsSettingType getType() {
        return type;
    }

    public DsSettingSection getSection() {
        return section;
    }

    public String toString() {
        return key;
    }
}
