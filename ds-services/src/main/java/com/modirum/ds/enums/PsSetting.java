package com.modirum.ds.enums;

/**
 * Enumerates ds_paymentsystem_conf.skey values.
 */
public enum PsSetting {
    /** DS server URL for accepting RReq messages. Value sent to ACS in AReq.dsURL **/
    DS_URL("DSURL", PaymentSystemSettingInputType.TEXTAREA),
    /** CSV list of SDK Reference Numbers that a given payment system would accept. **/
    ACCEPTED_SDK_LIST("acceptedSDKList", PaymentSystemSettingInputType.TEXTAREA),

    ///////////////////////////
    // Elo-specific settings //
    ///////////////////////////

    /** Toggles Elo PAPI Detokenization Service. Boolean. **/
    ELO_PAPI_ENABLED("eloPapiEnabled", PaymentSystemSettingInputType.CHECKBOX),
    /** Elo PAPI Detokenization Service endpoint URL. **/
    ELO_PAPI_DETOKEN_URL("eloPapiDetokenUrl", PaymentSystemSettingInputType.INPUT),
    /** Elo PAPI Detokenization Service client_id HTTP header value. **/
    ELO_PAPI_DETOKEN_CLIENT_ID("eloPapiDetokenClientId", PaymentSystemSettingInputType.INPUT),
    /** Elo PAPI Detokenization Service client secret value. Is used for secure token generation. **/
    ELO_PAPI_DETOKEN_CLIENT_SECRET("eloPapiDetokenClientSecret", PaymentSystemSettingInputType.INPUT),
    /** Elo PAPI Detokenization Service HTTP connect timeout in milliseconds **/
    ELO_PAPI_CONNECT_TIMEOUT_MILLIS("eloPapiConnectTimeoutMillis", PaymentSystemSettingInputType.INPUT),
    /** Elo PAPI Detokenization Service HTTP read timeout in milliseconds **/
    ELO_PAPI_READ_TIMEOUT_MILLIS("eloPapiReadTimeoutMillis", PaymentSystemSettingInputType.INPUT),
    /** FOR TESTING PURPOSES ONLY. Toggles Elo PAPI Detokenization Service TLS server cert CN/SAN check. **/
    ELO_PAPI_SKIP_TLS_HOSTNAME_VERIFY("eloPapiSkipTLSHostnameVerify", PaymentSystemSettingInputType.CHECKBOX),
    /** Elo Authentication Service endpoint URL. **/
    ELO_PAPI_AUTH_URL("eloPapiAuthUrl", PaymentSystemSettingInputType.INPUT),
    /** Elo PAPI username used in authenticating against ELO API **/
    ELO_PAPI_USERNAME("eloPapiUsername", PaymentSystemSettingInputType.DISPLAY_ONLY),
    /** Elo PAPI password used in authenticating against ELO API **/
    ELO_PAPI_PASSWORD("eloPapiPassword", PaymentSystemSettingInputType.DISPLAY_ONLY),
    /** Elo PAPI LEGAL ID CPF used in creating API User in ELO API **/
    ELO_PAPI_CPF("eloPapiCpf", PaymentSystemSettingInputType.DISPLAY_ONLY),
    /** Key Data ID of the client certificate needed for TLS connections with Elo Server **/
    ELO_PAPI_TLS_CLIENT_CERT_ID("eloPapiTLSClientCertId", PaymentSystemSettingInputType.DISPLAY_ONLY)
    ;

    private String key;
    private String inputType;

    PsSetting(String key, String inputType) {
        this.key = key;
        this.inputType = inputType;
    }
    public String getKey() {
        return key;
    }
    public String getInputType() {
        return inputType;
    }

    public boolean isEditable(){
        return !inputType.equals(PaymentSystemSettingInputType.DISPLAY_ONLY);
    }

    public String toString() {
        return key;
    }

    public static PsSetting fromKey(String key) {
        for (PsSetting psSetting : PsSetting.values()) {
            if (psSetting.key.equals(key)) {
                return psSetting;
            }
        }
        return null;
    }
}
