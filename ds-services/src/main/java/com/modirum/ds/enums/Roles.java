package com.modirum.ds.enums;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Enumerate all User Roles
 */
public class Roles {

    public static final String adminSetup = "adminSetup";
    public static final String paymentsystemView = "paymentsystemView";
    public static final String paymentsystemEdit = "paymentsystemEdit";
    public static final String merchantsView = "merchantsView";
    public static final String merchantsEdit = "merchantsEdit";
    public static final String userView = "userView";
    public static final String userEdit = "userEdit";
    public static final String issuerView = "issuerView";
    public static final String issuerEdit = "issuerEdit";
    public static final String acquirerView = "acquirerView";
    public static final String acquirerEdit = "acquirerEdit";
    public static final String recordsView = "recordsView";
    public static final String panView = "panView";
    public static final String textView = "textView";
    public static final String textEdit = "textEdit";
    public static final String caView = "caView";
    public static final String caEdit = "caEdit";
    public static final String auditLogView = "auditLogView";
    public static final String keyManage = "keyManage";
    public static final String reports = "reports";

    /**
     * Returns a set of all roles applicable for a user.
     * @return  Set of user roles
     */
    public static final Set<String> getAllRoles() {
        return Arrays.stream(Roles.class.getDeclaredFields())
           .filter(field -> Modifier.isStatic(field.getModifiers()) && Modifier.isPublic(field.getModifiers())
                   && Modifier.isFinal(field.getModifiers()) && field.getType().equals(String.class))
           .map(field -> field.getName())
           .collect(Collectors.toSet());
    }

}
