package com.modirum.ds.enums;

public enum DsSettingType {
    LONG("Please enter a number"),
    INT("Please enter an integer"),
    STRING("Please enter the value"),
    BOOLEAN("Please enter true or false"),
    DATE_FORMAT("Please enter a date format"),
    REGEX_PATTERN("Please enter a regex pattern")
    ;
    private String placeholder;
    DsSettingType(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getPlaceholder() {
        return placeholder;
    }
}
