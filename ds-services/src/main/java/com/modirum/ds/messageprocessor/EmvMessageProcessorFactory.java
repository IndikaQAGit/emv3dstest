package com.modirum.ds.messageprocessor;

/**
 * Interface to allow Payment System specific EMV message processor instantiation.
 */
public interface EmvMessageProcessorFactory {

    /**
     * Builds and initializes a Payment System's EMV protocol message processor.
     *
     * @return Implementation of a custom <code>PaymentSystemMessageProcessor</code>.
     */
    PaymentSystemMessageProcessor create();

}
