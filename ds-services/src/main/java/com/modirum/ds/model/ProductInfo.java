/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 10.03.2016
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;
import java.util.Properties;


public class ProductInfo {
    static Logger log = LoggerFactory.getLogger(ProductInfo.class);

    public static final String cProductName = "Modirum DS";
    public static final String cVendor = "Modirum Ltd.";
    public static final String cProductVersion = "1.0";
    public static final String cProductVersionPrev = "1.0";

    private static final String sigAlg = "SHA256withRSA";
    private static final String sigKeyType = "RSA";

    /* license validation public key X509EncodedKeySpec */
    private static final String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtmYvV4F+Sf4lTEr6kFeu" +
                                            "DxrhCcTPwgjf3Dk+bXsjSXZiWtfefZRngpKAePlwXsSpU8VuOMjDRsO7XWrEwD6L" +
                                            "rOFnGCVr3dLGDLIxj68dhiQNbQROnBnPfOnTO+/ZHFZWfLz0AkvNdH1lvmNZN4wa" +
                                            "Lzpz6y+4arECReRnsYgHMMiOslXEt5Y7IfyNHbr+dqdEh7jjMvofZthTDUjnPgJP" +
                                            "Y7RCUA0Kazl1MtztyeKzOLFwTBvwoGBRZetjCJvBXbjS10XDnUvLVs4Ty6g3SJqr" +
                                            "Zqe6eRrqhUm45rNu9D76F/mV9txN5MKl/1dcUV7fEYs85w2uobj0UdE2xjkoWVUP" +
                                            "RwIDAQAB";

    public static final String dateFormat = "yyyy-MM-dd";

    static private long buildDate = 0;
    static private final long OneYear = 365L * 24L * 3600L * 1000L;
    static private final long ThreeYears = 3 * OneYear;
    static private final long OneHour = 3600L * 1000L;
    static private final long OneDay = 24L * OneHour;
    private static final int licenseUpdateIntervalInDays = 2 * 365;
    private static final String DEFAULT_ISSUED_DATE = "2016-03-31";

    static java.util.jar.Manifest mf = null;
    static private String buildVersion = ProductInfo.cProductVersion;
    static private String commitId;
    static private final long lastCheck = 0;

    private String product;
    private String version;
    private String release;
    private String licensee;
    private String maxMerchants;
    private String extensions;
    private String expiration;
    private String issued;
    private String licenseKey;
    private boolean dbCorrect;

    private static final Map<String, String> allSchemesAndExtensions = new java.util.LinkedHashMap<String, String>();

    public enum Extensions {
        fss, tdsmrelay
    }

    static {
        allSchemesAndExtensions.put("", "");
        allSchemesAndExtensions.put(Extensions.fss.name(), "FSS");
        allSchemesAndExtensions.put(Extensions.tdsmrelay.name(), "3DSM Relay");
    }

    public static Map<String, String> getAllSchemesAndExtensions() {
        return allSchemesAndExtensions;
    }


    public boolean isLicenseValid() throws Exception {

        if (Misc.isNullOrEmpty(product)) {
            return false;
        }
        if (Misc.isNullOrEmpty(version)) {
            return false;
        }
        if (Misc.isNullOrEmpty(maxMerchants)) {
            return false;
        }
        if (Misc.isNullOrEmpty(expiration)) {
            return false;
        }
        if (Misc.isNullOrEmpty(licenseKey)) {
            return false;
        }
        if (Misc.isNullOrEmpty(licensee)) {
            return false;
        }

        String signString =
                product + "|" + version + "|" + licensee + "|" + maxMerchants + "|" + expiration + "|" + issued +
                (extensions != null && extensions.length() > 0 ? "|" + extensions : "");

        Signature rsaSig = Signature.getInstance(sigAlg);
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(Base64.decode(publicKey));
        KeyFactory keyFactory = KeyFactory.getInstance(sigKeyType);
        PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);
        rsaSig.initVerify(pubKey);
        rsaSig.update(signString.getBytes(StandardCharsets.UTF_8));

        return rsaSig.verify(Base64.decode(licenseKey));
    }

    public boolean isLicenseExpired() {
        if ("never".equalsIgnoreCase(expiration)) {
            return false;
        }

        java.util.Date validTo = DateUtil.parseDate(expiration, dateFormat, false);
        if (validTo != null) {
            java.util.Date now = new java.util.Date();
            java.util.Date validToDayEnd = DateUtil.normalizeDateDayEnd(validTo);
            return (validToDayEnd.before(now));
        }

        return true;
    }

    public int expiresInDays() {
        if ("never".equalsIgnoreCase(expiration)) {
            return -1;
        }

        java.util.Date validTo = DateUtil.parseDate(expiration, dateFormat, false);
        if (validTo != null) {
            java.util.Date now = new java.util.Date();
            java.util.Date validToDayEnd = DateUtil.normalizeDateDayEnd(validTo);

            int days = (int) ((validToDayEnd.getTime() - now.getTime()) / (24L * 3600L * 1000L));
            return days;
        }

        return 0;
    }

    public boolean isCorrectVersion() {
        return cProductVersion.equals(this.version);
    }

    public boolean isCorrectProduct() {
        return cProductName.equals(this.product);
    }

    public boolean isMerchantsInLimit(int merchants) {
        if (Misc.isInt(maxMerchants)) {
            return Misc.parseInt(maxMerchants) >= merchants;

        }

        return true;
    }

    public int getMerchantsLimit() {
        if (Misc.isInt(maxMerchants)) {
            return Misc.parseInt(maxMerchants);

        }

        return -1;
    }

    public final int buildIssuedDiffDays() {

        java.util.Date issuedAt = DateUtil.parseDate(
                this.issued != null && this.issued.length() > 0 ? this.issued : DEFAULT_ISSUED_DATE, dateFormat, false);
        long builtAt = getBuildDate();

        int days = (int) ((builtAt - issuedAt.getTime()) / (24L * 3600L * 1000L));
        return days;
    }

    public final boolean isAllValidAndCorrect(int merchants) throws Exception {

        if (!isLicenseValid()) {
            throw new RuntimeException("Invalid license data, contact support@modirum.com for valid license");
        }

        if (isLicenseExpired()) {
            throw new RuntimeException(
                    "License has been expired on " + expiration + ", contact support@modirum.com for valid license");
        }

        if (!isCorrectVersion()) {
            throw new RuntimeException("License is not valid for version " + cProductVersion + " but " + this.version +
                                       ", contact support@modirum.com for valid license");
        }

        if (!isCorrectProduct()) {
            throw new RuntimeException("License is not valid for product " + this.product +
                                       ", contact support@modirum.com for valid license");
        }

        if (!isMerchantsInLimit(merchants)) {
            throw new RuntimeException(
                    "Number of active merchants " + merchants + " exceeds licensed merchants " + this.maxMerchants +
                    ", contact support@modirum.com for valid license");
        }

        int diffDays = buildIssuedDiffDays();
        if (diffDays > licenseUpdateIntervalInDays) {
            throw new RuntimeException("Application cannot continue, Your license issued on " + this.issued +
                                       "is not eligible to run this version " + getBuildVersion() + " from " +
                                       DateUtil.formatDate(new java.util.Date(getBuildDate()), dateFormat, false) +
                                       ". Switch back to earlier version or obtain updated license from Modirum, email support@modirum.com");
        }

        return true;
    }

    public static String createLicenseInfo(CharSequence privateKey, String product, String version, String licensee, String maxMerchants, String expiration, String extensions) throws Exception {

        if (!"never".equalsIgnoreCase(expiration) && DateUtil.parseDate(expiration, dateFormat, false) == null) {
            throw new RuntimeException("Expiration must be 'never' or valid date in format 'yyyy-MM-dd'");
        }
        if (!cProductName.equals(product)) {
            throw new RuntimeException("Product name must be " + cProductName);
        }
        if (!cProductVersion.equals(version) && !cProductVersionPrev.equals(version)) {
            throw new RuntimeException("Product version must be " + cProductVersion + " or " + cProductVersionPrev);
        }
        if (Misc.isNullOrEmpty(licensee) || licensee.length() > 64) {
            throw new RuntimeException("Licensee must not be empty and must not be longer than 64 chars");
        }
        if (!"Unlimited".equalsIgnoreCase(maxMerchants) && !Misc.isInt(maxMerchants)) {
            throw new RuntimeException("Max merchants must be 'Unlimited' or integer number");
        }

        if (Misc.isNullOrEmpty(privateKey)) {
            throw new RuntimeException("Private key must be base64 encoded RSA private key value");
        }

        String issueDate = DateUtil.formatDate(new java.util.Date(), dateFormat, false);
        String signString =
                product + "|" + version + "|" + licensee + "|" + maxMerchants + "|" + expiration + "|" + issueDate +
                (extensions != null && extensions.length() > 0 ? "|" + extensions : "");

        Signature rsaSig = Signature.getInstance(sigAlg);
        PKCS8EncodedKeySpec priKeySpec = new PKCS8EncodedKeySpec(Base64.decode(parsePEMKey(privateKey)));
        // X509EncodedKeySpec priKeySpec = new X509EncodedKeySpec();
        KeyFactory keyFactory = KeyFactory.getInstance(sigKeyType);
        PrivateKey priKey = keyFactory.generatePrivate(priKeySpec);
        rsaSig.initSign(priKey);
        rsaSig.update(signString.getBytes(StandardCharsets.UTF_8));
        byte[] signature = rsaSig.sign();
        String base64Signature = Base64.encode(signature);

        ProductInfo test = new ProductInfo();
        test.setExpiration(expiration);
        test.setMaxMerchants(maxMerchants);
        test.setLicensee(licensee);
        test.setProduct(product);
        test.setVersion(version);
        test.setIssued(issueDate);
        test.setExtensions(extensions);
        test.setLicenseKey(base64Signature);

        if (!test.isLicenseValid()) {
            throw new RuntimeException(
                    "Internal validity check failed, Invalid input data or most likely invalid private key");
        }

        StringBuilder ld = new StringBuilder(8192);
        ld.append("License info created successfully and internally validated!\n");
        ld.append("Please use following license info:\n");
        ld.append("Product name: '" + test.product + "'\n");
        ld.append("Product version: '" + test.version + "'\n");
        ld.append("Licensee: '" + test.licensee + "'\n");
        ld.append("Max merchants: '" + test.maxMerchants + "'\n");
        ld.append("Extensions: '" + test.extensions + "'\n");
        ld.append("Issued: '" + test.issued + "'\n");
        ld.append("Expiration: '" + test.expiration + "'\n");
        ld.append("License key: '" + test.licenseKey + "'\n");

        return ld.toString();
    }

    public final static String BEGIN_PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----";
    public final static String END_PRIVATE_KEY = "-----END PRIVATE KEY-----";

    static CharSequence parsePEMKey(CharSequence tagged) {
        int start = -1;
        String s = null;
        if (tagged != null && (start = (s = tagged.toString()).indexOf(BEGIN_PRIVATE_KEY)) > -1) {
            start = start + BEGIN_PRIVATE_KEY.length();
            int end = s.indexOf(END_PRIVATE_KEY, start);
            if (end > start) {
                return s.substring(start, end - 1);
            }
            throw new RuntimeException("End private key block not found");
        }

        return tagged;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getMaxMerchants() {
        return maxMerchants;
    }

    public void setMaxMerchants(String maxMerchants) {
        this.maxMerchants = maxMerchants;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    public boolean isDbCorrect() {
        return dbCorrect;
    }

    public void setDbCorrect(boolean dbCorrect) {
        this.dbCorrect = dbCorrect;
    }

    public String getLicensee() {
        return licensee;
    }

    public void setLicensee(String licensee) {
        this.licensee = licensee;
    }

    public boolean checkExtensionLicense(String extension) {
        return this.extensions != null && extension != null && extensions.contains(extension);
    }

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        if (args != null && args.length > 4 && "checkvalue".equals(args[0])) {
            System.out.println(ProductInfo.checkSum(args[1], args[2], args[3], args[4]));
            return;
        }

        // CheckValue generation case for maven build, runnable via "exec-maven-plugin". Writes value to a properties
        // file under maven-provided filepath
        if (args != null && args.length >= 7 && "checkvalue_maven".equals(args[0])) {
            String fileName = args[1];
            String propertyKey = args[2];
            String checkValue = ProductInfo.checkSum(args[3], args[4], args[5], args[6]);

            File file = new File(fileName);
            if (file.exists() && file.isFile()) {
                file.delete();
            }
            try (OutputStream output = new FileOutputStream(file)) {
                Properties prop = new Properties();
                prop.setProperty(propertyKey, checkValue);
                prop.store(output, null);
                System.out.println("Created checkvalue file: " + file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        try {
            System.out.println(cProductName + " license generation:");
            java.io.DataInputStream dis = new java.io.DataInputStream(System.in);
            System.out.print("Enter (paste) private key in PKCS8 format (and then press enter):");
            StringBuilder privateKey = new StringBuilder(4096);

            while (true) {
                String line = dis.readLine();
                if (line.trim().length() == 0) {
                    break;
                }

                privateKey.append('\n').append(line);
            }

            System.out.println("Private key read");
            System.out.print("Type product or enter for default (" + cProductName + "):");
            String product = dis.readLine().trim();
            if (product.length() < 1) {
                product = cProductName;
            }
            System.out.println("Product='" + product + "'");


            System.out.print("Type version or enter for default (" + cProductVersion + "):");
            String version = dis.readLine().trim();
            if (version.length() < 1) {
                version = cProductVersion;
            }
            System.out.println("Version='" + version + "'");

            System.out.print("Enter licensee name:");
            String licensee = dis.readLine().trim();
            System.out.println("Licensee='" + licensee + "'");

            System.out.print("Enter maxMerchants (unlimited or integer):");
            String maxMerchants = dis.readLine().trim();
            System.out.println("MaxMerchants='" + maxMerchants + "'");

            System.out.print("Enter expiration (never or yyyy-MM-dd):");
            String expiration = dis.readLine().trim();
            System.out.println("Expiration='" + expiration + "'");

            System.out.print("Enter extensions:");
            String extensions = dis.readLine().trim();
            System.out.println("Extensions='" + extensions + "'");
            System.out.println(
                    createLicenseInfo(privateKey, product, version, licensee, maxMerchants, expiration, extensions));

        } catch (Exception e) {
            System.out.println("Error occured " + e);
            e.printStackTrace();
        }

    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    // manifest checksum secret
    private final static StringBuffer checksumSecret = new StringBuffer("yZa").append("0LZ").append("5gq;-").append(
            "1#9").append("LGI").append("wAZ05").append("pqMDj");

    static String checkSum(String prod, String vend, String version, String buildDate) {
        try {
            MessageDigest mdigest = MessageDigest.getInstance("SHA-256");
            String concatString = prod + vend + version + buildDate + checksumSecret.toString();
            byte[] digestResult = mdigest.digest(concatString.getBytes(StandardCharsets.UTF_8));
            return Base64.encode(digestResult);
        } catch (Exception e) {
            log.error("Digest calculation error", e);

            return null;
        }
    }

    public static final long getBuildDate() {
        getBuildVersion();
        return buildDate;
    }

    public static final String getBuildVersion() {
        if (buildDate > 0) {
            // if build is younger than one year dont go to print..
            if (buildDate > System.currentTimeMillis() - OneYear) {
                return buildVersion;
            }

            // go to print every hour in case build is between 1 and 3 years old..
            if (buildDate > System.currentTimeMillis() - ThreeYears &&
                lastCheck > System.currentTimeMillis() - OneHour) {
                return buildVersion;
            }
        }

        if (mf == null) {
            try {
                java.util.Enumeration<java.net.URL> resources = ProductInfo.class.getClassLoader().getResources(
                        "META-INF/MANIFEST.MF");

                while (resources.hasMoreElements()) {
                    java.io.InputStream mfs = null;
                    try {
                        mfs = resources.nextElement().openStream();
                        java.util.jar.Manifest mfx = new java.util.jar.Manifest();
                        mfx.read(mfs);
                        mfs.close();
                        mfs = null;
                        String it = mfx.getMainAttributes().getValue("Implementation-Title");
                        if (it != null && (it.trim().equalsIgnoreCase(cProductName) ||
                                           it.trim().equalsIgnoreCase(cProductName + " Core"))) {
                            mf = mfx;
                            break;
                        }

                    } catch (Exception e) {
                        // dont print anything
                        e.printStackTrace();
                    } finally {
                        if (mfs != null) {
                            try {
                                mfs.close();
                            } catch (Exception dc) {
                            }
                            mfs = null;
                        }
                    }

                } // while

            } catch (Exception e) {
                // dont print anything
                e.printStackTrace();
            }
        } // if

        String bd = null;
        String checkSum = null;
        String pro = cProductName;
        String ven = cVendor;
        String bv = buildVersion;
        if (mf != null) {
            pro = mf.getMainAttributes().getValue("Implementation-Title");

            ven = mf.getMainAttributes().getValue("Implementation-Vendor");

            bv = mf.getMainAttributes().getValue("Implementation-Version");
            if (Misc.isNotNullOrEmpty(bv)) {
                buildVersion = bv;
            }

            bd = mf.getMainAttributes().getValue("Built-Date");

            checkSum = mf.getMainAttributes().getValue("CheckValue");
            commitId = Misc.cut(mf.getMainAttributes().getValue("Git-commit-id"), 7);
        }

        try {
            buildDate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(bd).getTime();
        } catch (Exception dc) {
            buildDate = 0;
        }

        if (log != null && log.isInfoEnabled()) {
            log.info("Product: " + pro + " build version " + bv + " build date: " + new java.util.Date(buildDate));
        } else {
            System.out.println(
                    "Product: " + pro + " build version " + bv + " build date: " + new java.util.Date(buildDate));
        }

        if (checkSum == null) {
            throw new RuntimeException("Manifest file altered, missing CheckValue, application cannot continue");
        }

        String calcChecksum = ProductInfo.checkSum(pro, ven, bv, bd);
        if (!calcChecksum.equals(checkSum)) {
            throw new RuntimeException("Manifest file altered, invalid checkvalue, application cannot continue");
        }

        if (buildDate > System.currentTimeMillis() - OneYear && buildDate < System.currentTimeMillis() - 60 * OneDay) {
            if (log != null && log.isInfoEnabled()) {
                log.info("Current " + pro +
                         " build version less than one year old, so may be relatively safe to use, but allways check for updates from " +
                         ven);
            } else {
                System.out.println("Current " + pro +
                                   " build version less than one year old, so may be relatively safe to use, but allways check for updates from " +
                                   ven);
            }

        } else if (buildDate > System.currentTimeMillis() - ThreeYears &&
                   buildDate < System.currentTimeMillis() - OneYear) {
            if (log != null && log.isWarnEnabled()) {
                log.warn("Current " + pro + " build " + bv + " from " + new java.util.Date(buildDate) +
                         " is older than one year, please contact, " + ven + " for updates!");
            } else {
                System.out.println("Current " + pro + " build " + bv + " from " + new java.util.Date(buildDate) +
                                   " is older than one year, please contact, " + ven + " for updates!");
            }
        } else if (buildDate <= System.currentTimeMillis() - ThreeYears) {
            if (log != null && log.isWarnEnabled()) {
                log.warn("WARNING: Current " + pro + " build " + buildVersion + " from " +
                         new java.util.Date(buildDate) +
                         " is outdated and should not be used to process production transactions. ");
                log.warn("Please contact " + ven +
                         " obtain updates and install newer version to make this installation PCI DSS compliant!");
            } else {
                System.out.println(
                        "WARNING: Current " + pro + " build " + bv + " from " + new java.util.Date(buildDate) +
                        " is outdated and should not be used to process production transactions. ");
                System.out.println("Please contact " + ven +
                                   " obtain updates and install newer version to make this installation PCI DSS compliant!");
            }
        }

        return bv;
    }

    public static String getCommitId() {
        return commitId;
    }

    public String getExtensions() {
        return extensions;
    }

    public void setExtensions(String extensions) {
        this.extensions = extensions;
    }
}
