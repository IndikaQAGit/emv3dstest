/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on Oct 12, 2016
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.TDSServerProfile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlRootElement(name = "DS", namespace = "##default")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlSeeAlso({Merchant.class, Issuer.class, CardRange.class, Acquirer.class, ACSProfile.class, TDSServerProfile.class})
@XmlType(name = "DS", propOrder = {"acquirers", "ACSProfiles", "cardRanges", "issuers", "merchants", "tdsServerProfiles"})
public class DS {

    private List<Merchant> merchants;
    private List<Issuer> issuers;
    private List<Acquirer> acquirers;
    private List<TDSServerProfile> tdsServerProfiles;
    private List<ACSProfile> ACSProfiles;

    private List<CardRange> cardRanges;

    public void setAcquirers(List<Acquirer> acquirers) {
        this.acquirers = acquirers;
    }

    @XmlElementWrapper(name = "Acquirers", required = false)
    @XmlElement(name = "Acquirer")
    public List<Acquirer> getAcquirers() {
        return acquirers;
    }

    @XmlElementWrapper(name = "Merchants", required = false)
    @XmlElement(name = "Merchant")
    public List<Merchant> getMerchants() {
        return merchants;
    }

    public void setMerchants(List<Merchant> merchants) {
        this.merchants = merchants;
    }

    @XmlElementWrapper(name = "Issuers", required = false)
    @XmlElement(name = "Issuer")
    public List<Issuer> getIssuers() {
        return issuers;
    }

    public void setIssuers(List<Issuer> issuers) {
        this.issuers = issuers;
    }

    @XmlElementWrapper(name = "CardRanges", required = false)
    @XmlElement(name = "CardRange")
    public List<CardRange> getCardRanges() {
        return cardRanges;
    }

    public void setCardRanges(List<CardRange> cardRanges) {
        this.cardRanges = cardRanges;
    }

    @XmlElementWrapper(name = "TDSServerProfiles", required = false)
    @XmlElement(name = "TDSServerProfile")
    public List<TDSServerProfile> getTdsServerProfiles() {
        return tdsServerProfiles;
    }

    public void setTdsServerProfiles(List<TDSServerProfile> tdsServerProfiles) {
        this.tdsServerProfiles = tdsServerProfiles;
    }

    @XmlElementWrapper(name = "ACSProfiles", required = false)
    @XmlElement(name = "ACSProfile")
    public List<ACSProfile> getACSProfiles() {
        return ACSProfiles;
    }

    public void setACSProfiles(List<ACSProfile> ACSProfiles) {
        this.ACSProfiles = ACSProfiles;
    }
}
