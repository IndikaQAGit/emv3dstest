/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 20. jaan 2017
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.services.JsonMessage;

import javax.json.JsonObject;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@XmlTransient
public class JSONJAXBBase {

    @XmlTransient
    private Map<String, AtomicInteger> duplicateCounts;

    @XmlTransient
    private JsonObject jsonObject;

    @XmlTransient
    private JsonMessage incomingJsonMessage;

    public Map<String, AtomicInteger> getDuplicateCounts() {
        return duplicateCounts;
    }

    public void setDuplicateCounts(Map<String, AtomicInteger> duplicateCounts) {
        this.duplicateCounts = duplicateCounts;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public JsonMessage getIncomingJsonMessage() {
        return incomingJsonMessage;
    }

    public void setIncomingJsonMessage(JsonMessage incomingJsonMessage) {
        this.incomingJsonMessage = incomingJsonMessage;
    }
}
