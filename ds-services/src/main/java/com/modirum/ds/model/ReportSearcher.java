package com.modirum.ds.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportSearcher implements PaymentSystemResource {

    private Integer paymentSystemId;
    private Long total;
    private Integer start;
    private Integer limit;

}
