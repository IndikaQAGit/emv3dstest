/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 27. jaan 2016
 *
 */
package com.modirum.ds.model;

import com.modirum.ds.model.TDSModel.ErrorCode;

import java.io.Serializable;

public class Error implements Serializable {

    private static final long serialVersionUID = 1L;

    protected ErrorCode code;
    protected String description;
    protected String fieldName;

    public Error() {
    }

    public Error(ErrorCode code, String field, String detail) {
        this.code = code;
        this.fieldName = field;
        this.description = detail;
    }

    public ErrorCode getCode() {
        return code;
    }

    public void setCode(ErrorCode code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String toString() {
        return "Error {Code: " + (code != null ? code.value : null) +/*";iReqCode "+iReqCode+*/"; fieldName: " +
               fieldName + "; descr: " + description + "}";
    }
}
