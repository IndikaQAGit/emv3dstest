package com.modirum.ds.util;

import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import org.hibernate.Transaction;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DSUtils {

    public static <T> Set<T> findDuplicates(final Collection<T> collection) {
        Set<T> duplicates = new LinkedHashSet<>();
        Set<T> uniques = new HashSet<>();

        for (T t : collection) {
            if (!uniques.add(t)) {
                duplicates.add(t);
            }
        }
        return duplicates;
    }

    /**
     * Is UUID. For example: 00000000-000f-9bd1-0000-0159d5f12316
     */
    public static boolean isUUID(String s) {
        if (s == null || s.length() < 36) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (i == 8 || i == 13 || i == 18 || i == 23) {
                if (c != '-') {
                    return false;
                }
            } else if (!(c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F' || c >= '0' || c <= '9')) {
                return false;
            }
        }
        return true;
    }

    public static <T> List<T> toList(Stream<T> stream) {
        return stream == null ? null : stream.collect(Collectors.toList());
    }

    public static <T> Set<T> toSet(Stream<T> stream) {
        return stream == null ? null :stream.collect(Collectors.toSet());
    }

    /**
     * Create sort of Name type based version 5 UUID (applies version and IETF variant).
     */
    public static String createUUID(byte[] sixteen) {
        StringBuilder uuid = new StringBuilder(36);
        sixteen[6] &= 0x0f; /* clear version        */
        sixteen[6] |= 0x50; /* set to version 5     */
        sixteen[8] &= 0x3f; /* clear variant        */
        sixteen[8] |= 0x80; /* set to IETF variant  */

        uuid.append(HexUtils.toStringLC(sixteen));
        uuid.insert(8, '-');
        uuid.insert(13, '-');
        uuid.insert(18, '-');
        uuid.insert(23, '-');

        return uuid.toString();
    }

    /**
     * Create sort of Name type based version 5 UUID (applies version and IETF variant)
     *
     * @param prefix
     * @param sequence
     * @return
     */
    public static String createUUID(long prefix, long sequence) {
        StringBuilder uuid = new StringBuilder(36);
        byte[] f8 = Misc.longToBytes(prefix);
        f8[6] &= 0x0f; /* clear version        */
        f8[6] |= 0x50; /* set to version 5     */

        byte[] f16 = Misc.longToBytes(sequence);
        f16[0] &= 0x3f; /* clear variant        */
        f16[0] |= 0x80; /* set to IETF variant  */

        uuid.append(HexUtils.toStringLC(f8));
        uuid.insert(8, '-');
        uuid.insert(13, '-');
        uuid.append('-');
        uuid.append(HexUtils.toStringLC(f16));
        uuid.insert(23, '-');

        return uuid.toString();
    }
}
