package com.modirum.ds.util;

import com.modirum.ds.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateFormatUtils {
    private final static Logger LOG = LoggerFactory.getLogger(DateFormatUtils.class);

    /**
     * Checks the String if it is a valid date format
     * @param dateFormat The pattern of the date format
     * @return
     */
    public static boolean isValidDateFormat(String dateFormat) {
        try {
            DateTimeFormatter.ofPattern(dateFormat);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    /**
     * Validate if the string is a date with the given expected format.
     * @param date
     * @param expectedFormat
     * @return
     */
    public static boolean isDateFormatted(String date, String expectedFormat) {
        try {
            return DateUtil.parseDate(date, expectedFormat) != null;
        } catch (Exception e) {
            LOG.warn("For date [{}] and format [{}] has encountered error [{}]", date, expectedFormat, e.getMessage());
        }
        return false;
    }
}
