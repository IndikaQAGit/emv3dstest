package com.modirum.ds.util;

public class BooleanUtils {
    /**
     * Checks if string value is equal to either literal true or false with case insensitive.
     * @param value
     * @return
     */
    public static boolean isBoolean(String value) {
        return "true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value);
    }
}
