package com.modirum.ds.util;

import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.Error;

/**
 * Helper methods in building standard DS Error Message Data
 */
public class ErrorUtils {

    /**
     * Builds the 201 Error object for this given fieldName.
     *
     * @param fieldName
     * @return
     */
    public static Error fieldMissingError(String fieldName) {
        return new Error(TDSModel.ErrorCode.cRequiredFieldMissing201, fieldName, "is missing");
    }

    /**
     * Builds the 203 Error object for empty or null field.
     *
     * @param fieldName
     * @return
     */
    public static Error emptyOrNullFieldError(String fieldName) {
        return new Error(TDSModel.ErrorCode.cInvalidFieldFormat203, fieldName, "can not be empty or null if exists in json");
    }
}