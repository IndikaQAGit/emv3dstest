package com.modirum.ds.util;

import java.util.Map;

public class XEntry<K, V> implements Map.Entry<K, V> {
    K k;
    V v;

    public XEntry(K k, V v) {
        this.k = k;
        this.v = v;
    }

    @Override
    public K getKey() {
        return k;
    }

    @Override
    public V getValue() {
        return v;
    }

    @Override
    public V setValue(V value) {
        this.v = value;
        return value;
    }
}
