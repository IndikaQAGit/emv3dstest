/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 22. aug 2017
 *
 */
package com.modirum.ds.json;

import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.stream.JsonLocation;
import javax.json.stream.JsonParser;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ResourceBundle;

// adjusted copy of javax.json impl with using our objectbuilder with duplicate field detection capabilities
public class JsonReaderImpl implements JsonReader {

    private final JsonParser parser;
    private boolean readDone;

    public JsonReaderImpl(Reader reader) {
        parser = com.modirum.ds.json.JsonProviderImpl.provider().createParser(reader);
    }

    public JsonReaderImpl(InputStream in) {
        parser = com.modirum.ds.json.JsonProviderImpl.provider().createParser(in);
    }


    @Override
    public JsonStructure read() {
        if (readDone) {
            throw new IllegalStateException(JsonMessages.READER_READ_ALREADY_CALLED());
        }
        readDone = true;
        if (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            if (e == JsonParser.Event.START_ARRAY) {
                return readArray(com.modirum.ds.json.JsonProviderImpl.provider().createArrayBuilder());
            } else if (e == JsonParser.Event.START_OBJECT) {
                return readObject(com.modirum.ds.json.JsonProviderImpl.provider().createObjectBuilder());
            }
        }
        throw new JsonException("Internal Error");
    }

    @Override
    public JsonObject readObject() {
        if (readDone) {
            throw new IllegalStateException(JsonMessages.READER_READ_ALREADY_CALLED());
        }
        readDone = true;
        if (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            if (e == JsonParser.Event.START_OBJECT) {
                return readObject(com.modirum.ds.json.JsonProviderImpl.provider().createObjectBuilder());
            } else if (e == JsonParser.Event.START_ARRAY) {
                throw new JsonException(JsonMessages.READER_EXPECTED_OBJECT_GOT_ARRAY());
            }
        }
        throw new JsonException("Internal Error");
    }

    @Override
    public JsonArray readArray() {
        if (readDone) {
            throw new IllegalStateException(JsonMessages.READER_READ_ALREADY_CALLED());
        }
        readDone = true;
        if (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            if (e == JsonParser.Event.START_ARRAY) {
                return readArray(com.modirum.ds.json.JsonProviderImpl.provider().createArrayBuilder());
            } else if (e == JsonParser.Event.START_OBJECT) {
                throw new JsonException(JsonMessages.READER_EXPECTED_ARRAY_GOT_OBJECT());
            }
        }
        throw new JsonException("Internal Error");
    }

    @Override
    public void close() {
        readDone = true;
        parser.close();
    }

    private JsonArray readArray(JsonArrayBuilder builder) {
        while (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            switch (e) {
                case START_ARRAY:
                    JsonArray array = readArray(com.modirum.ds.json.JsonProviderImpl.provider().createArrayBuilder());
                    builder.add(array);
                    break;
                case START_OBJECT:
                    JsonObject object = readObject(
                            com.modirum.ds.json.JsonProviderImpl.provider().createObjectBuilder());
                    builder.add(object);
                    break;
                case VALUE_STRING:
                    builder.add(parser.getString());
                    break;
                case VALUE_NUMBER:
                    BigDecimal bd = parser.getBigDecimal();
                    if (bd != null && bd.longValue() >= Integer.MIN_VALUE && bd.longValue() <= Integer.MAX_VALUE &&
                        bd.intValue() == bd.doubleValue()) {
                        builder.add(bd.intValue());
                    } else {
                        builder.add(bd);
                    }
                    break;
                case VALUE_TRUE:
                    builder.add(JsonValue.TRUE);
                    break;
                case VALUE_FALSE:
                    builder.add(JsonValue.FALSE);
                    break;
                case VALUE_NULL:
                    builder.addNull();
                    break;
                case END_ARRAY:
                    return builder.build();
                default:
                    throw new JsonException("Internal Error (invalid evt type)");
            }
        }
        throw new JsonException("Internal Error");
    }

    private JsonObject readObject(JsonObjectBuilder builder) {
        String key = null;
        while (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            switch (e) {
                case START_ARRAY:
                    JsonArray array = readArray(com.modirum.ds.json.JsonProviderImpl.provider().createArrayBuilder());
                    builder.add(key, array);
                    break;
                case START_OBJECT:
                    JsonObject object = readObject(
                            com.modirum.ds.json.JsonProviderImpl.provider().createObjectBuilder());
                    builder.add(key, object);
                    break;
                case KEY_NAME:
                    key = parser.getString();
                    break;
                case VALUE_STRING:
                    builder.add(key, parser.getString());
                    break;
                case VALUE_NUMBER:
                    BigDecimal bd = parser.getBigDecimal();
                    if (bd != null && bd.longValue() >= Integer.MIN_VALUE && bd.longValue() <= Integer.MAX_VALUE &&
                        bd.intValue() == bd.doubleValue()) {
                        builder.add(key, bd.intValue());
                    } else {
                        builder.add(key, bd);
                    }
                    break;
                case VALUE_TRUE:
                    builder.add(key, JsonValue.TRUE);
                    break;
                case VALUE_FALSE:
                    builder.add(key, JsonValue.FALSE);
                    break;
                case VALUE_NULL:
                    builder.addNull(key);
                    break;
                case END_OBJECT:
                    return builder.build();
                default:
                    throw new JsonException("Internal Error (invalid evt type)");
            }
        }
        throw new JsonException("Internal Error");
    }


    final static class JsonMessages {
        private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.glassfish.json.messages");

        // tokenizer messages
        static String TOKENIZER_UNEXPECTED_CHAR(int unexpected, JsonLocation location) {
            return localize("tokenizer.unexpected.char", unexpected, location);
        }

        static String TOKENIZER_EXPECTED_CHAR(int unexpected, JsonLocation location, char expected) {
            return localize("tokenizer.expected.char", unexpected, location, expected);
        }

        static String TOKENIZER_IO_ERR() {
            return localize("tokenizer.io.err");
        }


        // parser messages
        static String PARSER_GETSTRING_ERR(JsonParser.Event event) {
            return localize("parser.getString.err", event);
        }

        static String PARSER_ISINTEGRALNUMBER_ERR(JsonParser.Event event) {
            return localize("parser.isIntegralNumber.err", event);
        }

        static String PARSER_GETINT_ERR(JsonParser.Event event) {
            return localize("parser.getInt.err", event);
        }

        static String PARSER_GETLONG_ERR(JsonParser.Event event) {
            return localize("parser.getLong.err", event);
        }

        static String PARSER_GETBIGDECIMAL_ERR(JsonParser.Event event) {
            return localize("parser.getBigDecimal.err", event);
        }

        static String PARSER_EXPECTED_EOF(Object token) {
            return localize("parser.expected.eof", token);
        }

        static String PARSER_TOKENIZER_CLOSE_IO() {
            return localize("parser.tokenizer.close.io");
        }

        static String PARSER_INVALID_TOKEN(Object token, JsonLocation location, String expectedTokens) {
            return localize("parser.invalid.token", token, location, expectedTokens);
        }


        // generator messages
        static String GENERATOR_FLUSH_IO_ERR() {
            return localize("generator.flush.io.err");
        }

        static String GENERATOR_CLOSE_IO_ERR() {
            return localize("generator.close.io.err");
        }

        static String GENERATOR_WRITE_IO_ERR() {
            return localize("generator.write.io.err");
        }

        static String GENERATOR_ILLEGAL_METHOD(Object scope) {
            return localize("generator.illegal.method", scope);
        }

        static String GENERATOR_DOUBLE_INFINITE_NAN() {
            return localize("generator.double.infinite.nan");
        }

        static String GENERATOR_INCOMPLETE_JSON() {
            return localize("generator.incomplete.json");
        }

        static String GENERATOR_ILLEGAL_MULTIPLE_TEXT() {
            return localize("generator.illegal.multiple.text");
        }


        // writer messages
        static String WRITER_WRITE_ALREADY_CALLED() {
            return localize("writer.write.already.called");
        }


        // reader messages
        static String READER_READ_ALREADY_CALLED() {
            return localize("reader.read.already.called");
        }

        static String READER_EXPECTED_ARRAY_GOT_OBJECT() {
            return localize("reader.expected.array.got.object");
        }

        static String READER_EXPECTED_OBJECT_GOT_ARRAY() {
            return localize("reader.expected.object.got.array");
        }


        // obj builder messages
        static String OBJBUILDER_NAME_NULL() {
            return localize("objbuilder.name.null");
        }

        static String OBJBUILDER_VALUE_NULL() {
            return localize("objbuilder.value.null");
        }

        static String OBJBUILDER_OBJECT_BUILDER_NULL() {
            return localize("objbuilder.object.builder.null");
        }

        static String OBJBUILDER_ARRAY_BUILDER_NULL() {
            return localize("objbuilder.array.builder.null");
        }


        // array builder messages
        static String ARRBUILDER_VALUE_NULL() {
            return localize("arrbuilder.value.null");
        }

        static String ARRBUILDER_OBJECT_BUILDER_NULL() {
            return localize("arrbuilder.object.builder.null");
        }

        static String ARRBUILDER_ARRAY_BUILDER_NULL() {
            return localize("arrbuilder.array.builder.null");
        }


        private static String localize(String key, Object... args) {
            try {
                String msg = BUNDLE.getString(key);
                return MessageFormat.format(msg, args);
            } catch (Exception e) {
                return getDefaultMessage(key, args);
            }
        }

        private static String getDefaultMessage(String key, Object... args) {
            StringBuilder sb = new StringBuilder();
            sb.append("[failed to localize] ");
            sb.append(key);
            if (args != null) {
                sb.append('(');
                for (int i = 0; i < args.length; ++i) {
                    if (i != 0) {
                        sb.append(", ");
                    }
                    sb.append(args[i]);
                }
                sb.append(')');
            }
            return sb.toString();
        }
    }
}
