/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 22. aug 2017
 *
 */
package com.modirum.ds.json;

import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import java.util.Map;

public class JsonBuilderFactoryImpl implements javax.json.JsonBuilderFactory {

    javax.json.JsonBuilderFactory wrp;

    public JsonBuilderFactoryImpl(javax.json.JsonBuilderFactory i) {
        this.wrp = i;
    }

    @Override
    public JsonArrayBuilder createArrayBuilder() {
        return wrp.createArrayBuilder();
    }

    @Override
    public JsonObjectBuilder createObjectBuilder() {
        return new com.modirum.ds.json.JsonObjectBuilderImpl(wrp.createObjectBuilder());
    }

    @Override
    public Map<String, ?> getConfigInUse() {
        return wrp.getConfigInUse();
    }
}
