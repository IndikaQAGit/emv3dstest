/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 7. jaan 2016
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.model.Error;
import com.modirum.ds.model.TDSMessageBase;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.model.TDSModel.ErrorCode;
import com.modirum.ds.tds21msgs.TDSMessage;
import com.modirum.ds.tds21msgs.XJWE;
import com.modirum.ds.tds21msgs.XJWEprotected;
import com.modirum.ds.tds21msgs.XJWSCompact;
import com.modirum.ds.tds21msgs.XacsSignedContentPayload;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author andri Service for transforming ThereeDSecure v2 messages (JSON)
 */
public class MessageService220<T extends TDSMessageBase> extends MessageService210<T> {

    protected static Logger log = LoggerFactory.getLogger(MessageService220.class);

    private volatile static MessageService220<TDSMessage> instance;

    public static MessageService220<TDSMessage> getInstance() {
        if (instance == null) {
            instance = new MessageService220<>();
        }
        return instance;
    }

    protected MessageService220(String schemaPath) {
        super(schemaPath);
    }

    protected MessageService220() {
        super("/tds220.xsd");
        log.info("MessageService220 initialized");
    }

    public String getSupportedVersion() {
        return TDSModel.MessageVersion.V2_2_0.value();
    }

    public boolean isErrorMessage(TDSMessage tdsMsg) {
        return TDSModel.XmessageType.ERRO.value().equals(tdsMsg.getMessageType());
    }

    /**
     * Validate first duplicates and if passed next field syntax to match as set in XSD.
     */
    public void validateFieldSyntax(TDSMessage msg, List<Error> errors) {
        if (msg.getDuplicateCounts() != null && msg.getDuplicateCounts().size() > 0) {
            Error e = new Error();
            errors.add(e);
            e.setCode(ErrorCode.cDuplicateDataElem204);

            StringBuilder details = new StringBuilder();
            for (Map.Entry<String, AtomicInteger> en : msg.getDuplicateCounts().entrySet()) {
                if (details.length() > 0) {
                    details.append(',');
                }
                details.append(en.getKey() + "(" + en.getValue() + ")");
            }
            e.setFieldName(details.toString());
            return;
        }

        if (schema != null) {
            try {
                JAXBSource source = new JAXBSource(tdsJsonCtx2, msg);
                Validator validator = this.getValidator();
                FieldErrorHandler feh = new FieldErrorHandler(errors);
                feh.treatEmptyValueAsMissing = treatEmptyValueAsMissing;
                validator.setErrorHandler(feh);
                validator.validate(source);
                this.returnValidator(validator);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }

        if (msg.getAcsUiType() != null) {
            if (!TDSModel.ACSUIType2_1_0.contains(msg.getAcsUiType())) {
                Error e = new Error();
                errors.add(e);
                e.setCode(ErrorCode.cInvalidTxData305);
                e.setFieldName("acsUiType");
                e.setDescription("Invalid undefined value");
            }
        }
    }

    /**
     * Validate commons: messageVersion, messageCategory.
     */
    @Override
    public void validateCommons(TDSMessage aMsg, List<Error> errors) {
        if (Misc.isNullOrEmpty(aMsg.getMessageVersion())) {
            errors.add(new Error(ErrorCode.cRequiredFieldMissing201, "messageVersion",
                                 (aMsg.getMessageVersion() == null ? "is missing" : "is empty")));
        } else {
            boolean vok = false;
            StringBuilder supportedValues = new StringBuilder();
            for (TDSModel.MessageVersion xv : TDSModel.MessageVersion.values()) {
                if (xv.value().equals(aMsg.getMessageVersion()) && xv.isSupported()) {
                    vok = true;
                    break;
                }

                if (xv.isSupported()) {
                    if (supportedValues.length() > 0) {
                        supportedValues.append(",");
                    }
                    supportedValues.append(xv.value());
                }
            }
            if (!vok) {
                errors.add(0, new Error(ErrorCode.cMessageVersionNotSupported102, "messageVersion",
                                        "invalid value '" + aMsg.getMessageVersion() + "'" + " supported versions: " +
                                        supportedValues));
            }
        }

        StringBuilder ireq05Details = new StringBuilder();

        if (Misc.isNullOrEmpty(aMsg.getMessageType())) {
            errors.add(0, new Error(ErrorCode.cRequiredFieldMissing201, "messageType",
                                    (aMsg.getMessageType() == null ? "is missing" : "is empty")));
        } else if (TDSModel.XmessageType.A_REQ.value.equals(aMsg.getMessageType()) ||
                   TDSModel.XmessageType.R_REQ.value.equals(aMsg.getMessageType())) {
            if (Misc.isNullOrEmpty(aMsg.getMessageCategory())) {
                errors.add(0, new Error(ErrorCode.cRequiredFieldMissing201, "messageCategory",
                                        (aMsg.getMessageCategory() == null ? "is missing" : "is empty")));
            } else {
                if (!TDSModel.XMessageCategory.contains(aMsg.getMessageCategory())) {
                    ireq05Details.append("messageCategory invalid value '" + aMsg.getMessageCategory() + "'");
                }
            }
        } else if (aMsg.getMessageCategory() != null) {
            ireq05Details.append("messageCategory not expected in " + aMsg.getMessageType());
        }

        if (ireq05Details.length() > 0) {
            errors.add(0, new Error(ErrorCode.cInvalidFieldFormat203, null, ireq05Details.toString()));
        }
    }

    public XJWE fromCompactJWE(byte[] compactJWE) throws Exception {
        XJWE jwe = new XJWE();
        String compactJWES = new String(compactJWE, StandardCharsets.ISO_8859_1);
        int dot1 = compactJWES.indexOf('.');
        String protStr = new String(compactJWE, 0, dot1, StandardCharsets.ISO_8859_1);
        XJWEprotected prot = ProtectedfromJSON(Base64.decodeURL(protStr));
        int dot2 = compactJWES.indexOf('.', dot1 + 1);

        jwe.setProtected(prot);
        jwe.setEncryptedKey(new String(compactJWE, dot1 + 1, dot2 - dot1 - 1, StandardCharsets.ISO_8859_1));

        jwe.setAad(protStr);
        int dot3 = compactJWES.indexOf('.', dot2 + 1);
        if (dot3 - dot2 > 1) {
            jwe.setIv(new String(compactJWE, dot2 + 1, dot3 - dot2 - 1, StandardCharsets.ISO_8859_1));
        }
        int dot4 = compactJWES.indexOf('.', dot3 + 1);
        jwe.setCiphertext(new String(compactJWE, dot3 + 1, dot4 - dot3 - 1, StandardCharsets.ISO_8859_1));
        jwe.setTag(new String(compactJWE, dot4 + 1, compactJWE.length - dot4 - 1, StandardCharsets.ISO_8859_1));

        return jwe;
    }

    public XJWEprotected ProtectedfromJSON(byte[] json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = new java.util.HashMap<>(4);
        com.modirum.ds.json.JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);

        XJWEprotected o = u.unmarshal(new javax.xml.transform.stream.StreamSource(new ByteArrayInputStream(json)),
                                      XJWEprotected.class).getValue();
        returnUnmarshaller(u);
        o.setDuplicateCounts(dups);
        return o;
    }

    public XJWE JWEfromJSON(byte[] json) throws JAXBException {
        Unmarshaller u = getJSONUnmarshaller();
        Map<String, AtomicInteger> dups = new java.util.HashMap<>(4);
        com.modirum.ds.json.JsonObjectBuilderImpl.duplicateFieldCountsTL.set(dups);
        XJWE o = u.unmarshal(new javax.xml.transform.stream.StreamSource(new ByteArrayInputStream(json)),
                             XJWE.class).getValue();
        returnUnmarshaller(u);
        o.setDuplicateCounts(dups);
        return o;
    }

    @Override
    public T createNewMessage() {
        return (T) new TDSMessage();
    }

    @Override
    public List getCardRangeData(T m) {
        return ((TDSMessage) m).getCardRangeData();
    }

    public XacsSignedContentPayload getPayloadFromACSSignedContent(String acsSignedContent) throws Exception {
        XJWSCompact jwsc = fromCompactJWS(acsSignedContent);
        byte[] json = Base64.decodeURL(jwsc.getPayload());
        Unmarshaller u = getJSONUnmarshaller();
        XacsSignedContentPayload xpl = u.unmarshal(
                new javax.xml.transform.stream.StreamSource(new ByteArrayInputStream(json)),
                XacsSignedContentPayload.class).getValue();
        returnUnmarshaller(u);

        return xpl;
    }
}
