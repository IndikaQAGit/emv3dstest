package com.modirum.ds.services.db.jdbc.core;

import com.modirum.ds.jdbc.core.dao.DSInitDAO;
import com.modirum.ds.jdbc.core.model.DSInit;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service("dsInitService")
public class DSInitService {

    @Resource
    private DSInitDAO dsInitDAO;

    /**
     * Retrieves {@link DSInit} entry from the database.
     */
    public DSInit getByInitKey(String initKey) {
        return dsInitDAO.getByInitKey(initKey);
    }

    /**
     * Persists or updates the provided {@link DSInit} object to the database.
     */
    public void saveOrUpdate(DSInit dsInit) {
        dsInit.setLastModifiedDate(new Date());
        if (dsInitDAO.exists(dsInit.getInitKey())) {
            dsInitDAO.update(dsInit);
        } else {
            dsInitDAO.insert(dsInit);
        }
    }
}
