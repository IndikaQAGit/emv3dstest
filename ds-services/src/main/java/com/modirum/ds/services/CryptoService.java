/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 16.02.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.hsm.KeyServiceBase;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.tds21msgs.XJWE;
import com.modirum.ds.util.CryptoUtil;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.ECDHDecrypter;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.jwk.CurveBasedJWK;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Optional;
import java.util.StringTokenizer;

@Service
public class CryptoService {

    protected static Logger log = LoggerFactory.getLogger(CryptoService.class);
    java.security.GuardedObject cipherPoolsGuarded = null;
    java.security.GuardedObject macPoolsGuarded = null;
    final static long someNumber = 52134567891123456L; // dont change it
    protected int cipherPoolSize = 16;
    final static Charset utf8 = StandardCharsets.UTF_8;
    final static Charset US_ASCII = StandardCharsets.US_ASCII;
    public final static String HMACSHA256 = "HmacSHA256";
    public final static String HMACSHA384 = "HmacSHA384";
    public final static String HMACSHA512 = "HmacSHA512";
    final public static String DATAENCALG = "AES/CBC/PKCS5Padding";
    public static final String HMAC_ALG = HMACSHA256;
    private final static int DEFAULT_SALT_LENGTH = 8;
    private static final byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    java.security.SecureRandom sr;

    @Autowired
    private KeyService keyService;

    private static final String MDE04_ = "MDE04_"; // Encrypted string prefix with "encryption version"

    public interface JWE {

        interface enc {
            String A128CBC_HS256 = "A128CBC-HS256";
            String A256CBC_HS512 = "A256CBC-HS512";
            String A192CBC_HS384 = "A192CBC-HS384";
            String A128GCM = "A128GCM"; //??
            String A192GCM = "A192GCM"; //??
            String A256GCM = "A256GCM"; //??
        }

        enum alg {
            RSAES_OAEP("RSAES-OAEP", "RSA/None/OAEPWithSHA1AndMGF1Padding"),
            RSA_OAEP_256("RSA-OAEP-256", "RSA/None/OAEPWithSHA256AndMGF1Padding"),
            RSAES_OAEP_256("RSAES-OAEP-256", "RSA/None/OAEPWithSHA256AndMGF1Padding"),
            RSA_OAEP("RSA-OAEP", "RSA/ECB/OAEPWithSHA1AndMGF1Padding"),
            ECDH_ES("ECDH-ES", "ECIES");

            public final String algv;
            public final String javaAlg;

            alg(String alg, String javaal) {
                this.algv = alg;
                this.javaAlg = javaal;
            }

            public static alg fromValue(String v) {
                for (alg c : alg.values()) {
                    if (c.algv.equals(v)) {
                        return c;
                    }
                }
                return null;
            }

            public static alg fromJavaValue(String v) {
                for (alg c : alg.values()) {
                    if (c.javaAlg.equals(v)) {
                        return c;
                    }
                }
                return null;
            }

        }

        String AES_128_CBC_HMAC_SHA_256 = "AES_128_CBC_HMAC_SHA_256"; //auth tag
    }


    public CryptoService() {
        java.security.GuardedObject x = new java.security.GuardedObject(this, Permissions.DS_SecurityPermission);
        x.getObject();

        cipherPoolsGuarded = new java.security.GuardedObject(
                new java.util.HashMap<String, java.util.LinkedList<Cipher>>(), Permissions.DS_SecurityPermission);
        macPoolsGuarded = new java.security.GuardedObject(new java.util.HashMap<String, java.util.LinkedList<Mac>>(),
                                                          Permissions.DS_SecurityPermission);

        try {
            sr = java.security.SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(("sasQQP<--er4h" + new java.util.Date() + "s99WWQSGHWM" + Runtime.getRuntime().totalMemory() +
                        "ZCPWCBsl" + System.currentTimeMillis() + Math.random()).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    // for CBC salt needs to be added to first block only..
    protected final byte[] addSaltForCBC(Cipher cipher, byte[] data) {
        int saltLen = cipher.getBlockSize() > DEFAULT_SALT_LENGTH ? DEFAULT_SALT_LENGTH : cipher.getBlockSize();
        if (saltLen > 0) {
            byte[] salt = new byte[saltLen];
            sr.nextBytes(salt);

            byte[] saltedData = new byte[salt.length + data.length];
            System.arraycopy(salt, 0, saltedData, 0, salt.length);
            System.arraycopy(data, 0, saltedData, salt.length, data.length);
            return saltedData;
        }
        return data;
    }

    protected final byte[] removeSaltForCBC(Cipher cipher, byte[] saltedData) {
        int saltLen = cipher.getBlockSize() > DEFAULT_SALT_LENGTH ? DEFAULT_SALT_LENGTH : cipher.getBlockSize();
        if (saltLen > 0 && saltLen <= saltedData.length) {
            byte[] unsaltedData = new byte[saltedData.length - saltLen];
            System.arraycopy(saltedData, saltLen, unsaltedData, 0, unsaltedData.length);
            return unsaltedData;
        }
	/*	else if (saltLen > saltedData.length)
		{
			throw new RuntimeException("Invalid data decryption not possible ml<sl");
		}
	*/

        return saltedData;
    }

    final private Cipher poolCipher(String cipherKey, Cipher cipher) {
        if (cipherKey != null) {
            @SuppressWarnings("unchecked") java.util.HashMap<String, java.util.LinkedList<Cipher>> cipherPools = (java.util.HashMap<String, java.util.LinkedList<Cipher>>) cipherPoolsGuarded.getObject();
            java.util.LinkedList<Cipher> chipherPoolx = cipherPools.get(cipherKey);
            if (chipherPoolx == null) {
                chipherPoolx = new java.util.LinkedList<Cipher>();
                cipherPools.put(cipherKey, chipherPoolx);
            }

            if (cipher != null && chipherPoolx.size() < this.cipherPoolSize) {
                synchronized (chipherPoolx) // does not matter if size changed in the middle
                {
                    chipherPoolx.add(cipher);
                }
            } else if (chipherPoolx.size() > 0) {
                synchronized (chipherPoolx) {
                    if (chipherPoolx.size() > 0) // matters is size changed in the middle
                    {
                        cipher = chipherPoolx.removeLast();
                    }
                }

            } // synch
        }

        return cipher;
    }

    // soft pooling
    // if not in pool should create inline and put into pool
    // no pool waiting and notifying
    final private Mac poolMac(String macKey, Mac mac) {
        if (macKey != null) {
            @SuppressWarnings("unchecked") java.util.Map<String, java.util.LinkedList<Mac>> macPools = (java.util.Map<String, java.util.LinkedList<Mac>>) macPoolsGuarded.getObject();
            java.util.LinkedList<Mac> macPoolx = macPools.get(macKey);
            if (macPoolx == null) {
                macPoolx = new java.util.LinkedList<Mac>();
                macPools.put(macKey, macPoolx);
            }


            if (mac != null && macPoolx.size() < this.cipherPoolSize) {
                synchronized (macPoolx) //// does not matter if size changed in the middle
                {
                    macPoolx.add(mac);
                }
            } else if (macPoolx.size() > 0) {
                synchronized (macPoolx) {
                    // matters a lot if is size changed in the middle
                    if (macPoolx.size() > 0) {
                        mac = macPoolx.removeLast();
                    }
                }
            }
        }

        return mac;
    }

    /**
     * PAN HMACing
     *
     * @param pan
     * @param macKey
     * @return pahHMAC (HmacSHA1/sha256 hmac in base64 encoding)
     */
    public String createPanHMAC(String pan, java.security.Key macKey) {
        byte[] pd = null;
        try {
            pd = pan.getBytes(utf8);
            return Base64.encode(createHMAC(pd, macKey));
        } catch (Exception e) {
            throw new RuntimeException("HMAC calculation failed", e);
        }
    }

    public String createPanHMAC(CharSequence val, java.security.Key macKey) {
        byte[] pd = null;
        try {
            pd = val.toString().getBytes(utf8);
            return Base64.encode(createHMAC(pd, macKey));
        } catch (Exception e) {
            throw new RuntimeException("HMAC calculation failed", e);
        }
    }

    public byte[] createHMAC(byte[] data, java.security.Key macKey) throws Exception {
        if (data == null || macKey == null) {
            return null;
        }

        Mac mac = null;
        String macMapKey = "panHMAC." + macKey.hashCode();

        mac = this.poolMac(macMapKey, null);
        if (mac == null) {
            mac = Mac.getInstance(HMAC_ALG);
            mac.init(macKey);
        }

        byte[] result = mac.doFinal(data);
        this.poolMac(macMapKey, mac);
        return result;
    }

    // pass password one way hash with unique predicable
    public static String sha256WithUniquePredictable(String password) {
        return sha256WithUniquePredictable(password.toCharArray());
    }

    public static String sha256WithUniquePredictable(char[] password) {
        try {
            StringBuilder sb = new StringBuilder(password.length * 2).append(password);
            long unipred1 = 0;
            for (int i = 0; i < password.length; i++) {
                unipred1 += (i + 1) * password[i] + someNumber;
            }
            sb.append(Base64.encode(HexUtils.fromString(Long.toHexString(unipred1))));

            MessageDigest mdigest = MessageDigest.getInstance("SHA-256");
            byte[] digestResult = mdigest.digest(sb.toString().getBytes(utf8));
            return Base64.encode(digestResult, 128);
        } catch (Exception e) {
            throw new RuntimeException("sha256WithUniquePredictable error", e);
        }

    }

    public static String encryptConfigPassword(String password, HSMDevice hsms) throws Exception {
        StringBuilder sb = new StringBuilder(password);
        long uniPred1 = 0;
        for (int i = 0; i < password.length(); i++) {
            uniPred1 += (i + 1) * password.charAt(i) + someNumber;
        }
        sb.append(Base64.encode(HexUtils.fromString(Long.toHexString(uniPred1))));

        HSMDevice.Key passwordKey = hsms.generateHSMDataEncryptionKey();
        byte[] encPassword = hsms.encryptData(passwordKey.getValue(), sb.toString().getBytes(utf8));

        return Base64.encode(passwordKey.getValue()) + ";" + Base64.encode(encPassword);
    }

    public static String decryptConfigPassword(String encPassword, HSMDevice hsms) throws Exception {
        String[] parts = Misc.split(encPassword, ';');
        byte[] passwordKey = Base64.decode(parts[0]);
        byte[] passwordData = Base64.decode(parts[1]);
        byte[] password = hsms.decryptData(passwordKey, passwordData);

        String passWithPredictable = new String(password, utf8);
        return passWithPredictable.substring(0, passWithPredictable.length() - 12);
    }


    public String decrypt(String sensitiveData, java.security.Key key) throws Exception {
        if (Misc.isNotNullOrEmpty(sensitiveData)) {
            return new String(decrypt(Base64.decode(sensitiveData.getBytes(US_ASCII)), key), utf8);
        }

        return sensitiveData;
    }

    /**
     * DS generic (sensitive) data encryption method. Encrypts provided input data with symmetric 'dataKey'.
     * @param input Input to encrypt.
     * @return Encrypted input.
     * @throws Exception in case of encryption errors.
     */
    public String encryptData(final String input) throws Exception {
        if (input == null || input.length() == 0) {
            return "";
        }

        Optional<KeyData> dataKeyOptional = keyService.getDataEncryptionKey();
        if (!dataKeyOptional.isPresent()) {
            throw new Exception("Data key must be present for data encryption. Key not found.");
        }

        KeyData dataKey = dataKeyOptional.get();
        Long hsmDeviceId = dataKey.getHsmDeviceId();
        HSMDevice hsm = ServiceLocator.getInstance().getHSMDevice(hsmDeviceId);

        byte[] dataKeyBytes = unwrapRawKey(dataKey);
        byte[] gzippedData = CryptoUtil.gzip(input.getBytes(StandardCharsets.UTF_8));
        byte[] crypted = hsm.encryptData(dataKeyBytes, gzippedData);

        return MDE04_ + dataKey.getId() + "_" + Base64.encode(crypted, 0);
    }

    /**
     * DS generic (sensitive) data decryption method. Decrypts provided input data with symmetric 'dataKey'.
     *
     * @param input Input to decrypt.
     * @return Decrypted input.
     * @throws Exception in case of decryption errors.
     */
    public String decryptData(final String input) throws Exception {
        if (input == null || !input.startsWith(MDE04_)) {
            return input;
        }

        StringTokenizer st = new StringTokenizer(input, "_");
        st.nextToken(); // remove MDExx_ prefix
        String keyId = st.nextToken();
        String encodedData = st.nextToken();

        KeyData dataKey = keyService.getKey(Long.valueOf(keyId));
        Long hsmDeviceId = dataKey.getHsmDeviceId();
        HSMDevice hsm = ServiceLocator.getInstance().getHSMDevice(hsmDeviceId);

        byte[] dataKeyBytes = unwrapRawKey(dataKey);
        byte[] encryptedData = Base64.decode(encodedData);
        byte[] decrypted = hsm.decryptData(dataKeyBytes, encryptedData);

        byte[] decompressed = CryptoUtil.gunzip(decrypted);
        return new String(decompressed, StandardCharsets.UTF_8);
    }

    public byte[] encrypt(byte[] sensitiveData, java.security.Key key) throws Exception {
        if (sensitiveData != null && sensitiveData.length > 0) {
            String cipherKey = "aesEncKey." + key.hashCode();
            Cipher cipher = this.poolCipher(cipherKey, null);
            if (cipher == null) {
                cipher = Cipher.getInstance(DATAENCALG);
                cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
            }

            sensitiveData = cipher.doFinal(addSaltForCBC(cipher, sensitiveData));
            // restore only good one
            this.poolCipher(cipherKey, cipher);
        }
        return sensitiveData;
    }

    public String encrypt(String sensitiveData, KeyData key) throws Exception {
        if (sensitiveData != null && sensitiveData.length() > 0) {
            byte[] rawKeyBytes = unwrapRawKey(key);
            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(key.getHsmDeviceId());
            return Base64.encode(hsmDevice.encryptData(rawKeyBytes, sensitiveData.getBytes(utf8)));
        }
        return sensitiveData;
    }

    public String encrypt(String sensitiveData, java.security.Key key) throws Exception {
        if (sensitiveData != null && sensitiveData.length() > 0) {
            sensitiveData = Base64.encode(encrypt(sensitiveData.getBytes(utf8), key));
        }

        return sensitiveData;
    }

    public String decrypt(String sensitiveData,  KeyData key) throws Exception {
        if (Misc.isNotNullOrEmpty(sensitiveData)) {
            byte[] rawKeyBytes = unwrapRawKey(key);
            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(key.getHsmDeviceId());
            return new String(hsmDevice.decryptData(rawKeyBytes, Base64.decode(sensitiveData.getBytes(US_ASCII))), utf8);
        }

        return sensitiveData;
    }

    public byte[] decrypt(byte[] sensitiveData, java.security.Key key) throws Exception {
        if (sensitiveData != null && sensitiveData.length > 0) {
            String chipherKey = "aesDecKey." + key.hashCode();
            Cipher cipher = this.poolCipher(chipherKey, null);
            if (cipher == null) {
                cipher = Cipher.getInstance(DATAENCALG);
                cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            }
            sensitiveData = removeSaltForCBC(cipher, cipher.doFinal(sensitiveData));
            // restore only good one
            this.poolCipher(chipherKey, cipher);
        }
        return sensitiveData;
    }

    public static String createSecurityToken() {
        long tkn = ((long) (Math.random() * 83246997846378463L)) + 5945761364L;

        return Long.toHexString(tkn);
    }


    public static String getJWEMode(String encAlg) {
        if (JWE.enc.A128CBC_HS256.equals(encAlg) || JWE.enc.A192CBC_HS384.equals(encAlg) ||
            JWE.enc.A256CBC_HS512.equals(encAlg)) {
            return "CBC";
        }
        if (JWE.enc.A128GCM.equals(encAlg) || JWE.enc.A192GCM.equals(encAlg) || JWE.enc.A256GCM.equals(encAlg)) {
            return "GCM";
        }
        return null;
    }

    public static String getJWEPadding(String encAlg) {
        if (JWE.enc.A128CBC_HS256.equals(encAlg) || JWE.enc.A192CBC_HS384.equals(encAlg) ||
            JWE.enc.A256CBC_HS512.equals(encAlg)) {
            return "PKCS5Padding";
        }
        if (JWE.enc.A128GCM.equals(encAlg) || JWE.enc.A192GCM.equals(encAlg) || JWE.enc.A256GCM.equals(encAlg)) {
            return "NoPadding ";
        }
        return null;
    }

    public static String getJWEAuthAlg(String encAlg) {
        if (JWE.enc.A128CBC_HS256.equals(encAlg)) {
            return CryptoService.HMACSHA256;
        }
        if (JWE.enc.A192CBC_HS384.equals(encAlg)) {
            return CryptoService.HMACSHA384;
        }
        if (JWE.enc.A256CBC_HS512.equals(encAlg)) {
            return CryptoService.HMACSHA512;
        }

        if (JWE.enc.A128GCM.equals(encAlg) || JWE.enc.A192GCM.equals(encAlg) || JWE.enc.A256GCM.equals(encAlg)) {
            return null;
        }
        return null;
    }

    @Deprecated
    private com.modirum.ds.tds21msgs.XJWE encryptJWE(PublicKey pubKey, String encAlg, String alg, byte[] data) throws Exception {
        // create aes key
        byte[] aeskey = null;
        byte[] hmackey = null;

        String enca = "AES";
        String aesMode = getJWEMode(encAlg);
        String aesPadding = getJWEPadding(encAlg);
        String autha = getJWEAuthAlg(encAlg);

        if (JWE.enc.A128CBC_HS256.equals(encAlg) || JWE.enc.A128GCM.equals(encAlg)) {
            aeskey = KeyService.genKey(16);
            hmackey = JWE.enc.A128CBC_HS256.equals(encAlg) ? KeyService.genKey(16) : null;
        } else if (JWE.enc.A192CBC_HS384.equals(encAlg) || JWE.enc.A192GCM.equals(encAlg)) {
            aeskey = KeyService.genKey(24);
            hmackey = JWE.enc.A192CBC_HS384.equals(encAlg) ? KeyService.genKey(24) : null;
        } else if (JWE.enc.A256GCM.equals(encAlg) || JWE.enc.A256CBC_HS512.equals(encAlg)) {
            aeskey = KeyService.genKey(32);
            hmackey = JWE.enc.A256CBC_HS512.equals(encAlg) ? KeyService.genKey(32) : null;
        } else {
            throw new IllegalArgumentException("Invalid/unsupported JWE enc alg " + encAlg);
        }

        byte[] iv = null;
        if ("GCM".equals(aesMode)) {
            iv = KeyService.genRnd(16);

        }

        byte[] enckey = null;
        JWE.alg algx = JWE.alg.fromJavaValue(alg);

        final String ck = "JWEenc." + alg + "." + pubKey.hashCode();
        Cipher cipher = poolCipher(ck, null);
        if (cipher == null) {
            KeyService.getBouncyCastleProvider();
            cipher = Cipher.getInstance(algx.javaAlg); //, "BC");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        }
        if (hmackey != null) {
            byte[] comp = new byte[hmackey.length + aeskey.length];
            System.arraycopy(hmackey, 0, comp, 0, hmackey.length);
            System.arraycopy(aeskey, 0, comp, hmackey.length, aeskey.length);
            enckey = cipher.doFinal(comp);
        } else {
            enckey = cipher.doFinal(aeskey);
        }

        poolCipher(ck, cipher);

        // ASCII(BASE64URL(UTF8(JWE Protected Header))).
        com.modirum.ds.tds21msgs.XJWE xjwe = new com.modirum.ds.tds21msgs.XJWE();
        com.modirum.ds.tds21msgs.XJWEprotected p = new com.modirum.ds.tds21msgs.XJWEprotected();
        p.setAlg(algx.algv);
        p.setEnc(encAlg);
        xjwe.setProtected(p);

        StringBuilder ph = new StringBuilder();
        ph.append("{");
        ph.append("\"" + "alg" + "\":\"" + p.getAlg() + "\",");
        ph.append("\"" + "enc" + "\":\"" + p.getEnc() + "\"");
        ph.append("}");

        xjwe.setAad(Base64.encodeURL(Misc.toUtf8Bytes(ph)));

        // aes part..
        SecretKeySpec keySpec = new SecretKeySpec(aeskey, "AES");


        byte[] encData = null;
        byte[] autTag = null;
        if ("GCM".equals(aesMode)) {
            byte[] aad = xjwe.getAad().getBytes(StandardCharsets.UTF_8);
            Cipher aescipher = Cipher.getInstance(enca + "/" + aesMode + "/" + aesPadding);
            aescipher.init(Cipher.ENCRYPT_MODE, keySpec,
                           new GCMParameterSpec(128, iv != null ? iv : KeyService.zeroblock16));
            aescipher.updateAAD(aad);
            byte[] encDataAndAAD = aescipher.doFinal(data);
            encData = new byte[encDataAndAAD.length - 16];
            autTag = new byte[16];
            System.arraycopy(encDataAndAAD, 0, encData, 0, encData.length);
            System.arraycopy(encDataAndAAD, encData.length, autTag, 0, autTag.length);
            //System.out.println("AAD1="+Hex.toString(aad));
            //System.out.println("ENCTAG1="+Hex.toString(encDataAndAAD));
			/* test...
			Cipher aescipher2 =Cipher.getInstance(enca+"/"+aesMode+"/"+aesPadding);
			aescipher2.init(Cipher.DECRYPT_MODE, keySpec, new GCMParameterSpec(128, iv!=null ? iv : KeyService.zeroblock16));
			aescipher2.updateAAD(aad);
			aescipher2.doFinal(encDataAndAAD);
			OK
			*/
        } else {
            Cipher aescipher = Cipher.getInstance(enca + "/" + aesMode + "/" + aesPadding);
            aescipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv != null ? iv : KeyService.zeroblock16));
            encData = aescipher.doFinal(data);
        }

        xjwe.setEncryptedKey(Base64.encodeURL(enckey));
        xjwe.setCiphertext(Base64.encodeURL(encData));
        if (iv != null) {
            xjwe.setIv(Base64.encodeURL(iv));
        }

        if (autha != null) {
            byte[] aad = xjwe.getAad().getBytes(StandardCharsets.ISO_8859_1);
            long aadBitLen = xjwe.getAad().length() * 8;
            byte[] al = Misc.longToBytes(aadBitLen);
            int inputLen = aad.length + (iv != null ? iv.length : 0) + encData.length + al.length;
            java.io.ByteArrayOutputStream hmb = new java.io.ByteArrayOutputStream(inputLen);
            hmb.write(aad);
            if (iv != null) {
                hmb.write(iv);
            }
            hmb.write(encData);

            //64-Bit Big-Endian Representation of AAD Length  [0, 0, 0, 0, 0, 0, 1, 152]
            hmb.write(al);
            Mac hmac = Mac.getInstance(autha);
            String[] hc = {"HMACSHA256", "HMACSHA384", "HMACSHA512"};
            SecretKeySpec hmKeySpec = new SecretKeySpec(hmackey, hc[hmackey.length / 8 - 2]);
            hmac.init(hmKeySpec);
            byte[] at = hmac.doFinal(hmb.toByteArray());
            byte[] at16 = new byte[hmackey.length];
            System.arraycopy(at, 0, at16, 0, at16.length);
            autTag = at16;
        }
        if (autTag != null) {
            xjwe.setTag(Base64.encodeURL(autTag, 0));
        }

        return xjwe;
    }

    public String encryptJWEComp(PublicKey pubKey, String encAlg, String alg, String apv, byte[] data, boolean alllib) throws Exception {
        JWE.alg algx = JWE.alg.fromJavaValue(alg);
        // ASCII(BASE64URL(UTF8(JWE Protected Header))).
        if (JWE.alg.ECDH_ES == algx || alllib) {
            com.nimbusds.jose.EncryptionMethod em = null;
            com.nimbusds.jose.JWEHeader.Builder jwhb = null;
            if (JWE.alg.ECDH_ES == algx) {
                if (JWE.enc.A128CBC_HS256.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A128CBC_HS256;
                } else if (JWE.enc.A128GCM.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A128GCM;
                } else {
                    throw new RuntimeException("Enc method " + encAlg + " not suppored for ECDH_ES in 3DS spec");
                }

                jwhb = new com.nimbusds.jose.JWEHeader.Builder(com.nimbusds.jose.JWEAlgorithm.ECDH_ES, em);
            } else {
                if (JWE.enc.A128CBC_HS256.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A128CBC_HS256;
                } else if (JWE.enc.A192CBC_HS384.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A192CBC_HS384;
                } else if (JWE.enc.A256CBC_HS512.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A256CBC_HS512;
                } else if (JWE.enc.A128GCM.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A128GCM;
                } else if (JWE.enc.A192GCM.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A192GCM;
                } else if (JWE.enc.A256GCM.equals(encAlg)) {
                    em = com.nimbusds.jose.EncryptionMethod.A256GCM;
                }
                jwhb = new com.nimbusds.jose.JWEHeader.Builder(com.nimbusds.jose.JWEAlgorithm.RSA_OAEP_256, em);
            }


            KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
            kpg.initialize(256);
            KeyPair kp = kpg.generateKeyPair();
            ECPublicKey ecpe = (ECPublicKey) kp.getPublic();

            if (JWE.alg.ECDH_ES == algx) {
                com.nimbusds.jose.jwk.ECKey eck = new com.nimbusds.jose.jwk.ECKey(com.nimbusds.jose.jwk.Curve.P_256,
                                                                                  ecpe, null, null, null, null, null,
                                                                                  null, null, null, null);

                jwhb.ephemeralPublicKey(eck);
                if (apv != null) {
                    jwhb.agreementPartyVInfo(com.nimbusds.jose.util.Base64URL.encode(apv));
                }
            }
            com.nimbusds.jose.JWEHeader jwh = jwhb.build();

            com.nimbusds.jose.Payload pl = new com.nimbusds.jose.Payload(new String(data, StandardCharsets.UTF_8));
            com.nimbusds.jose.JWEObject jweObject = new com.nimbusds.jose.JWEObject(jwh, pl);

            if (JWE.alg.ECDH_ES == algx) {
                ECPublicKey ecp = (ECPublicKey) pubKey;
                jweObject.encrypt(new com.nimbusds.jose.crypto.ECDHEncrypter(ecp));
            } else {
                jweObject.encrypt(new com.nimbusds.jose.crypto.RSAEncrypter((RSAPublicKey) pubKey));
            }

            return jweObject.serialize();
        } else {
            com.modirum.ds.tds21msgs.XJWE xjwe = encryptJWE(pubKey, encAlg, alg, data);
            return MessageService210.getInstance().toCompactJWE(xjwe);
        }
    }

    /**
     * Decrypt JWE content using RSA (hsm/software) or EC keys.
     *
     * @param rsaPrivateKey RSA private key
     * @param ecPrivateKey EC private key
     * @param jwe JWE structure
     * @return decrypted JWE payload
     * @throws Exception in case of any decryption errors.
     */
    @Deprecated
    private byte[] decryptJWE(KeyData rsaPrivateKey, KeyData ecPrivateKey, XJWE jwe) throws Exception {
        String alg = jwe.getProtected().getAlg();
        if (log.isDebugEnabled()) {
            log.debug("Protected ALG=" + alg);
        }

        boolean ec = false;
        String algj = JWE.alg.RSAES_OAEP.javaAlg;
        if (alg == null) {
            alg = JWE.alg.RSAES_OAEP.algv;
            algj = JWE.alg.RSAES_OAEP.javaAlg;
        } else if (JWE.alg.RSAES_OAEP_256.algv.equals(alg)) {
            algj = JWE.alg.RSAES_OAEP_256.javaAlg;
        } else if (JWE.alg.RSA_OAEP.algv.equals(alg)) {
            algj = JWE.alg.RSA_OAEP.javaAlg;
        } else if (JWE.alg.RSA_OAEP_256.algv.equals(alg)) {
            algj = JWE.alg.RSA_OAEP_256.javaAlg;
        } else if (JWE.alg.ECDH_ES.algv.equals(alg)) {
            algj = JWE.alg.ECDH_ES.javaAlg;
            ec = true;
        }

        String encAlg = jwe.getProtected().getEnc();
        String aesMode = getJWEMode(encAlg);
        String aesPadding = getJWEPadding(encAlg);
        String autha = getJWEAuthAlg(encAlg);

        String kty = null;
        String crv = null;
        if (jwe.getProtected().getEpk() != null) {
            kty = jwe.getProtected().getEpk().getKty();
            crv = jwe.getProtected().getEpk().getCrv();
        }

        byte[] jweEncryptedKey = jwe.getEncryptedKey() != null ? Base64.decodeURL(jwe.getEncryptedKey()) : null;
        if (log.isDebugEnabled()) {
            log.debug("RSA/EC spec " + algj + " kty=" + kty + " crv=" + crv +
                    (jweEncryptedKey != null ? " ek len=" + jweEncryptedKey.length : ""));
        }

        // Decrypt "encrypted_key" component of JWE structure using provided private key - EC or RSA.
        // todo refactor to look nice
        byte[] keydata;
        if (ec) {
            if (ecPrivateKey == null) {
                throw new RuntimeException("SDK EC Private key not installed unable to decrypt");
            }

            byte[] ecPrivateKeyBytes = unwrapPrivateKey(ecPrivateKey);
            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(ecPrivateKey.getHsmDeviceId());
            keydata = hsmDevice.ecPrivateDecrypt(ecPrivateKeyBytes, jweEncryptedKey, algj);

        } else {
            if (rsaPrivateKey == null) {
                throw new RuntimeException("SDK RSA Private key not installed unable to decrypt");
            }

            byte[] rsaPrivateKeyBytes = unwrapPrivateKey(rsaPrivateKey);
            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(rsaPrivateKey.getHsmDeviceId());
            // EMV spec: 2.1.0, 2.2.0 only "alg"=RSA-OAEP-256 is defined to be supported for RSA
            keydata = hsmDevice.rsaPrivateDecrypt(rsaPrivateKeyBytes, jweEncryptedKey, RSADecryptPadMode.OAEP_SHA256_MGF1);
        }

        Cipher aescipher = Cipher.getInstance("AES/" + aesMode + "/" + aesPadding);

        byte[] iv = Misc.isNotNullOrEmpty(jwe.getIv()) ? Base64.decodeURL(jwe.getIv()) : null;
        byte[] plainData = null;
        byte[] encData = Base64.decodeURL(jwe.getCiphertext());
        //System.out.println("encData.len="+encData.length+ " iv "+(iv!=null ? iv.length : -1));
        if ("GCM".equals(aesMode)) {
            SecretKeySpec aesSpec = new SecretKeySpec(keydata, "AES");
            aescipher.init(Cipher.DECRYPT_MODE, aesSpec,
                           new GCMParameterSpec(128, iv != null ? iv : KeyService.zeroblock16));
            //byte[] aad =Base64.decode(jwe.getAad());
            byte[] aad = jwe.getAad().getBytes(StandardCharsets.UTF_8);
            byte[] autTag = Base64.decodeURL(jwe.getTag());
            byte[] encDataAAD = new byte[encData.length + autTag.length];
            System.arraycopy(encData, 0, encDataAAD, 0, encData.length);
            System.arraycopy(autTag, 0, encDataAAD, encData.length, autTag.length);

            aescipher.updateAAD(aad);
            plainData = aescipher.doFinal(encDataAAD);
        } else {
            int klen = keydata.length;
            int hklen = keydata.length;
            if (encAlg.contains("A128") && keydata.length > 16) {
                hklen = klen = 16;
            } else if (encAlg.contains("A192") && keydata.length > 24) {
                hklen = klen = 24;
            }
            if (encAlg.contains("A256") && keydata.length > 32) {
                hklen = klen = 32;
            }

            SecretKeySpec aesSpec = new SecretKeySpec(keydata, keydata.length > klen ? hklen : 0, klen, "AES");
            aescipher.init(Cipher.DECRYPT_MODE, aesSpec, new IvParameterSpec(iv != null ? iv : KeyService.zeroblock16));

            plainData = aescipher.doFinal(encData, 0, encData.length);//.doFinal(encData);
            // validate hmac
            if (autha != null) {
                if (Misc.isNullOrEmpty(jwe.getTag())) {
                    throw new IllegalArgumentException("Invalid JWE auth tag missing or empty");
                }

                java.io.ByteArrayOutputStream hmb = new java.io.ByteArrayOutputStream();
                hmb.write(jwe.getAad().getBytes(StandardCharsets.ISO_8859_1));
                if (iv != null) {
                    hmb.write(iv);
                }
                hmb.write(encData);

                //64-Bit Big-Endian Representation of AAD Length  [0, 0, 0, 0, 0, 0, 1, 152]
                long aadBitLen = jwe.getAad().length() * 8;
                hmb.write(Misc.longToBytes(aadBitLen));
                Mac hmac = Mac.getInstance(autha);
                String[] hc = {"HMACSHA256", "HMACSHA384", "HMACSHA512"};
                //SecretKeySpec hmKeySpec = new SecretKeySpec(hmackey, hc[hmackey.length/8-2]);
                SecretKeySpec hsmacSpec = new SecretKeySpec(keydata, 0, hklen, hc[hklen / 8 - 2]);
                hmac.init(hsmacSpec);

                byte[] at = hmac.doFinal(hmb.toByteArray());
                byte[] at16 = new byte[hklen];
                System.arraycopy(at, 0, at16, 0, at16.length);
                byte[] atMsg = Base64.decodeURL(jwe.getTag());

                if (!java.util.Arrays.equals(at16, atMsg)) {
                    throw new RuntimeException(
                            "Invalid JWE auth tag mismatch " + Base64.encodeURL(at16) + " vs " + jwe.getTag());
                }
            }
        }
        return plainData;
    }

    /**
     * Decrypt AReq.sdkEncData element (JWE compact string) using software private keys.
     *
     * @param privateKeyRSA RSA private key to decrypt with.
     * @param privateKeyEC EC private key to decrypt with
     * @param sdkEncData JWE string to decrypt.
     * @param isNimbusDecryption Whether to use nimbus jose decryption library. False, if using DS original decryption.
     * @return Decrypted sdkEncData payload.
     * @throws Exception in case of any decryption errors.
     */
    public byte[] decryptSdkEncData(KeyData privateKeyRSA, KeyData privateKeyEC, String sdkEncData, boolean isNimbusDecryption)
            throws Exception {

        JWEObject jweObject = JWEObject.parse(sdkEncData);
        boolean ec = jweObject.getHeader().getAlgorithm().getName().startsWith("ECDH");
        if (!isNimbusDecryption && !ec) { //own compatibility
            XJWE jwe = MessageService210.getInstance().fromCompactJWE(sdkEncData.getBytes(StandardCharsets.UTF_8));
            return decryptJWE(privateKeyRSA, privateKeyEC, jwe);
        }

        if (ec && !JWEAlgorithm.ECDH_ES.equals(jweObject.getHeader().getAlgorithm())) {
            throw new RuntimeException(
                    "Not supporteded ECDH algorithm " + jweObject.getHeader().getAlgorithm().getName());
        }

        String curveName = (jweObject.getHeader().getEphemeralPublicKey() instanceof CurveBasedJWK) ?
                            ((CurveBasedJWK)jweObject.getHeader().getEphemeralPublicKey()).getCurve().getName() :
                            null;
        if (ec && !"P-256".equals(curveName)) {
            throw new RuntimeException(
                    "Not supporteded ECDH curve " + curveName);
        }

        // todo all-nimbus only supports software keys for now. Add MDjce later if required
        if (ec) {
            if (privateKeyEC == null) {
                throw new RuntimeException("EC private key is not set (null), unable to decrypt SDK data");
            }

            KeyFactory keyFactory = KeyFactory.getInstance("ECDSA");
            byte[] ecPrivateKeyBytes = unwrapPrivateKey(privateKeyEC);
            ECPrivateKey ecp = (ECPrivateKey) keyFactory.generatePrivate(new PKCS8EncodedKeySpec(ecPrivateKeyBytes));

            jweObject.decrypt(new ECDHDecrypter(ecp));
        } else {
            if (privateKeyRSA == null) {
                throw new RuntimeException("RSA private key is not set (null), unable to decrypt SDK data");
            }

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] rsaPrivateKeyBytes = unwrapPrivateKey(privateKeyRSA);
            PrivateKey rsaPK = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(rsaPrivateKeyBytes));

            jweObject.decrypt(new RSADecrypter(rsaPK));
        }
        return jweObject.getPayload().toBytes();
    }

    /**
     * Unwraps keydata and parses it into a Private Key bytes. If not wrapped, only parses the keydata.
     *
     * @param sourcePrivateKey Private key to unwrap.
     * @return unwrapped private key bytes.
     * @throws Exception in case of key retrieval or unwrapping errors.
     */
    public byte[] unwrapPrivateKey(KeyData sourcePrivateKey) throws Exception {
        if (sourcePrivateKey.getWrapkeyId() == null) {
            String privateKeyDataBase64 = KeyServiceBase.parsePemPrivateKey(sourcePrivateKey.getKeyData());
            return Base64.decode(privateKeyDataBase64);
        }

        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        KeyData wrapperKey = pse.getKeyDataById(sourcePrivateKey.getWrapkeyId());
        HSMDevice wrapperHsm = ServiceLocator.getInstance().getHSMDevice(wrapperKey.getHsmDeviceId());

        String sourceKeyBase64 = KeyServiceBase.parsePemEncryptedPrivateKey(sourcePrivateKey.getKeyData());
        byte[] sourceKeyBytes = Base64.decode(sourceKeyBase64);
        return wrapperHsm.decryptData(Base64.decode(wrapperKey.getKeyData()), sourceKeyBytes);
    }

    /**
     * Unwraps generic raw keydata. If not wrapped, returns original bytes.
     *
     * @param sourceRawKeyData Raw key to unwrap.
     * @return unwrapped raw key bytes.
     * @throws Exception in case of key retrieval or unwrapping errors.
     */
    public byte[] unwrapRawKey(KeyData sourceRawKeyData) throws Exception {
        if (sourceRawKeyData.getWrapkeyId() == null) {
            return Base64.decode(sourceRawKeyData.getKeyData());
        }

        PersistenceService pse = ServiceLocator.getInstance().getPersistenceService();
        KeyData wrapperKey = pse.getKeyDataById(sourceRawKeyData.getWrapkeyId());
        HSMDevice wrapperHsm = ServiceLocator.getInstance().getHSMDevice(wrapperKey.getHsmDeviceId());

        byte[] sourceKeyBytes = Base64.decode(sourceRawKeyData.getKeyData());
        return wrapperHsm.decryptData(Base64.decode(wrapperKey.getKeyData()), sourceKeyBytes);
    }

    public String calculateHMACKeyCheckValue(byte[] key) throws Exception {
        Key hmacKey = new javax.crypto.spec.SecretKeySpec(key, 0, key.length, CryptoService.HMACSHA256);
        byte[] kcvfull = createHMAC(CryptoUtil.ZERO_BLOCK_8, hmacKey);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public X509Certificate signX509certificate(X509v3CertificateBuilder certificateBuilder,
                                               HSMDevice hsmDevice,
                                               byte[] issuerPrivateKeyBytes,
                                               PublicKey issuerPublicKey,
                                               String signatureAlgorithm) throws Exception {

        ContentSigner customContentSigner = hsmDevice.getContentSigner(issuerPrivateKeyBytes, signatureAlgorithm);
        X509CertificateHolder holder = certificateBuilder.build(customContentSigner);

        X509Certificate cert = new JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME)
                .getCertificate(holder);

        if (!holder.isSignatureValid(new JcaContentVerifierProviderBuilder()
                .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(issuerPublicKey))) {

            throw new Exception("Certificate generation failed: invalid signature");
        }
        return cert;
    }

    public PKCS10CertificationRequest generateCSR(HSMDevice hsmDevice,
                                                  KeyPair rsaKeyPair,
                                                  String subjectDN,
                                                  Extensions extensions,
                                                  String signatureAlgorithm) throws Exception {


        X500Name x500Name = new X500Name(subjectDN);
        PKCS10CertificationRequestBuilder requestBuilder = new JcaPKCS10CertificationRequestBuilder(x500Name, rsaKeyPair.getPublic());

        //Extensions
        if (extensions != null) {
            requestBuilder.addAttribute(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest, extensions);
        }

        byte[] csrPrivateKeyBytes = rsaKeyPair.getPrivate().getEncoded();
        ContentSigner contentSigner = hsmDevice.getContentSigner(csrPrivateKeyBytes, signatureAlgorithm);
        PKCS10CertificationRequest csr = requestBuilder.build(contentSigner);

        if (!csr.isSignatureValid(new JcaContentVerifierProviderBuilder()
                .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(rsaKeyPair.getPublic()))) {

            log.error("Error verifying CSR signature with its own public key.");
            throw new Exception("CSR generation failed.");
        }
        return csr;
    }

}
