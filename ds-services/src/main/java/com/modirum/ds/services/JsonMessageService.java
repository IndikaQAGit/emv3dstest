package com.modirum.ds.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.modirum.ds.util.RegexUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * An API service that provides operations in JsonMessage such as parsing of json string
 * and value masking of specified field or object.
 */
public class JsonMessageService {

    public static final String DEFAULT_FULL_VALUE_MATCHER = ".*";
    public static final String DEFAULT_MASK_REPLACEMENT = "***";
    private static final Logger log = LoggerFactory.getLogger(JsonMessageService.class);
    /**
     * Internal implementation using 3rd party library. Must make sure these should be kept internal and prevent leakage
     * to decouple the business code from this internal dependency.
     */
    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * This parses and builds json string into object represented by JsonMessage.
     * This in turn provides easy way to manipulate the json tree.
     * @param json The json string
     * @return
     * @throws IOException Error happens while parsing the json string such as invalid structure.
     */
    public JsonMessage parse(String json) throws IOException {
        return new JsonMessage(objectMapper.readTree(json), objectMapper);
    }

    /**
     * Converts the json tree into Pojo Object represented by this class.
     * @param jsonMessage Contains the json tree
     * @param clazz The class of the Pojo Object which the json tree will be converted to
     * @param <T>
     * @return
     * @throws IOException
     */
    public <T> T convert(JsonMessage jsonMessage, Class<T> clazz) throws IOException {
        return objectMapper.convertValue(jsonMessage.getRootNode(), clazz);
    }

    /**
     * Converts the json string into Pojo Object represented by this class.
     * @param jsonContent The json string
     * @param clazz The class of the Pojo Object which the json tree will be converted to
     * @return
     * @throws IOException
     */
    public <T> T convert(String jsonContent, Class<T> clazz) throws IOException {
        return objectMapper.readValue(jsonContent, clazz);
    }

    /**
     * Converts the json string into Pojo Object represented by this class while ignoring unknown properties in json.
     * @param jsonContent The json string
     * @param clazz The class of the Pojo Object which the json tree will be converted to
     * @return
     * @throws IOException
     */
    public <T> T convertIgnoreUnknownProperties(String jsonContent, Class<T> clazz) throws IOException {
        ObjectMapper objectMapperCopy = objectMapper.copy();
        objectMapperCopy.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapperCopy.readValue(jsonContent, clazz);
    }

    /**
     * Apply masking on the value of the field specified by @param fieldName in the jsonMessage. The masking pattern
     * identifies the segment of the value string and replace that part with the @param maskReplacement.
     * @param jsonMessage The JsonMessage to be masked
     * @param fieldName The specific field in the JsonMessage to be masked
     * @param maskingPattern A regex pattern that identifies which segment of the string value is to be masked
     * @param maskReplacement The actual masked value as replacement on the identified segment of the string value
     */
    public void maskValue(JsonMessage jsonMessage, String fieldName, String maskingPattern, String maskReplacement) {
        if (!RegexUtil.isValidPattern(maskingPattern)) {
            log.warn("Encountered invalid masking pattern of {} for fieldName={}. Will ignore masking of this field for now.", maskingPattern, fieldName);
            return;
        }

        String[] fieldNamePath = fieldName.split("\\.");

        JsonNode parentNode = jsonMessage.getLeafParent(fieldNamePath);

        if (parentNode != null) {
            String leafNodeFieldName = fieldNamePath[fieldNamePath.length - 1];

            maskNode(parentNode, leafNodeFieldName, maskingPattern, maskReplacement, parentNode.get(leafNodeFieldName));
        }

    }

    /**
     * Checks if given fieldName is present or not in the json tree.
     * @param jsonMessage
     * @param fieldName Uses dot notation for nested json field names
     * @return
     */
    public boolean isFieldPresent(JsonMessage jsonMessage, String fieldName) {
        JsonNode rootNode = jsonMessage.getRootNode();
        String[] fieldNamePath = fieldName.split("\\.");
        String fieldPath = "/" + StringUtils.join(fieldNamePath, '/', 0, fieldNamePath.length);
        JsonNode fieldNode = rootNode.at(fieldPath);
        return !fieldNode.isMissingNode();
    }

    /**
     * Completely hide the full value of the field specified by @param fieldName in the jsonMessage behind the default
     * masking pattern {@link JsonMessageService#DEFAULT_MASK_REPLACEMENT}
     *
     * @param jsonMessage
     * @param fieldName
     */
    public void hideValue(JsonMessage jsonMessage, String fieldName) {
        maskValue(jsonMessage, fieldName, DEFAULT_FULL_VALUE_MATCHER, DEFAULT_MASK_REPLACEMENT);
    }

    /**
     * This is the core implementation logic that handles masking for a json node(field). Currently handles object and fields.
     * This uses recursion for masking fieldName that points to objects.
     *
     * @param parentNode
     * @param fieldName
     * @param maskingPattern
     * @param maskReplacement
     * @param currentFieldJsonNode
     */
    private void maskNode(JsonNode parentNode, String fieldName, String maskingPattern, String maskReplacement, JsonNode currentFieldJsonNode) {

        if (currentFieldJsonNode != null) {

            if (currentFieldJsonNode.isObject()) {

                currentFieldJsonNode
                        .fieldNames()
                        .forEachRemaining(childFieldName -> maskNode(currentFieldJsonNode, childFieldName, maskingPattern, maskReplacement, currentFieldJsonNode.get(childFieldName)));

            } else {
                ((ObjectNode) parentNode).put(fieldName, currentFieldJsonNode.asText().replaceFirst(maskingPattern, maskReplacement));
            }
        }

    }

    /**
     * Writing JsonMessage json content to the given OutputStream
     * @param outputStream
     * @param jsonMessage
     * @param <T>
     * @return
     * @throws Exception
     */
    public <T extends OutputStream> T writeOutputStream(T outputStream, JsonMessage jsonMessage) throws Exception {
        outputStream.write(jsonMessage.toString().getBytes(StandardCharsets.UTF_8));
        return outputStream;
    }

}
