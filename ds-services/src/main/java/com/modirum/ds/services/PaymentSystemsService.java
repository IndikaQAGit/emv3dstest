package com.modirum.ds.services;

import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.User;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * This service provides shared functionalities related to PaymentSystems needed on UI.
 */
public class PaymentSystemsService {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentSystemsService.class);

    private PersistenceService persistenceService;

    public PaymentSystemsService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    /**
     * Returns the list of all available Payment Systems
     * @return Payment Systems list.
     */
    public List<PaymentSystem> getPaymentSystems() {
        return Misc.nullSafe(persistenceService.getPersitableList(PaymentSystem.class, 0, -1));
    }

    /**
     * Looks up a payment system entry by ID
     */
    public PaymentSystem get(Integer id) {
        try {
            return persistenceService.getPersistableById(id, PaymentSystem.class);
        } catch (Exception e) {
            LOG.error("Error looking up payment system entry", e);
            return null;
        }
    }
    /**
     * Looks up payment systems which are accessible to the <code>user</code> based on the user's access rights.
     */
    public List<PaymentSystem> getPaymentSystems(User user) {
        if (hasAccessToAllPaymentSystems(user)) {
            return getPaymentSystems();
        }

        // user-specific payment system only
        try {
            return Collections.singletonList(
                    persistenceService.getPersistableById(user.getPaymentSystemId(), PaymentSystem.class));
        } catch (Exception e) {
            LOG.error("Could not retrieve Payment System entry from the database.", e);
            return Collections.emptyList();
        }
    }

    /**
     * Checks if a payment system exists given a payment system ID
     */
    public boolean isPaymentSystemExists(Integer paymentSystemId) {
        return get(paymentSystemId) != null;
    }

    /**
     * Checks if user has access to all payment systems
     * @param user
     * @return
     */
    public boolean hasAccessToAllPaymentSystems(User user) { return user.getPaymentSystemId() == null; }
}
