/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 22.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.services;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.model.AcsSearcher;
import com.modirum.ds.model.CacheObject;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.model.IssuerSearcher;
import com.modirum.ds.db.model.User;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class IssuerService {

    private final PersistenceService persistenceService;
    private final AuditLogService auditLogService;
    static Logger log = LoggerFactory.getLogger(IssuerService.class);
    protected Map<String, CacheObject<List<ACSProfile>>> acsListCache;
    public final static int cMaxBinLengthPossible = 19;
    public final static int cMaxBinLength = 8;
    public final static int cMinBinLength = 3;
    public final static int cMinRangeSize = 9999;
    public final static int cMinRangeLength = 13;
    public final static int cMaxRangeLength = 19;

    int maxBinLength = cMaxBinLength;
    int minRangeSize = cMinRangeSize;
    boolean endStartMode = false;

    public IssuerService(PersistenceService persistenceService, AuditLogService auditLogService) {
        this.persistenceService = persistenceService;
        this.auditLogService = auditLogService;
        acsListCache = new TreeMap<>();
        try {
            endStartMode = "true".equals(persistenceService.getStringSetting(DsSetting.ISSUER_RANGE_MODE.getKey()));
            if (endStartMode) {
                maxBinLength = cMaxBinLengthPossible;

                String minRangeSizeSett = persistenceService.getStringSetting(DsSetting.ISSUER_MIN_RANGE_SIZE.getKey());
                if (Misc.isInt(minRangeSizeSett) && Misc.parseInt(minRangeSizeSett) > -1) {
                    minRangeSize = Misc.parseInt(minRangeSizeSett);
                    if (minRangeSize < cMinRangeSize) {
                        log.warn("Min range size set to " + minRangeSize + " (may not be secure or non PCIDSS)");
                    }
                }
            } else {
                String maxBinLenSett = persistenceService.getStringSetting(DsSetting.ISSUER_MAX_BIN_LENGTH.getKey());
                if (Misc.isInRange(maxBinLenSett, 6, cMaxBinLengthPossible)) {
                    maxBinLength = Misc.parseInt(maxBinLenSett);
                    if (maxBinLength > 9) {
                        log.warn("Max BIN length set to " + maxBinLength + " (may not be secure or non PCIDSS)");
                    }
                }
            }

        } catch (Exception e) {
            log.error("Error in Issuer service initialization ", e);
        }
    }

    public int getMaxBINLength() {
        return maxBinLength;
    }

    public int getMinBINLength() {
        return cMinBinLength;
    }

    public int getMinRangeSize() {
        return minRangeSize;
    }

    public boolean isBINRangeMode() {
        return endStartMode;
    }

    public Issuer getIssuerById(Integer id) {
        Issuer found = null;
        if (id != null) {
            found = persistenceService.getPersistableByIdUnchecked(id, Issuer.class);
        }
        return found;
    }

    public void setIssuerActive(final String name, final Integer paymentSystemId) {
        persistenceService.setIssuerStatus(name, paymentSystemId, DSModel.Issuer.Status.ACTIVE);
    }

    public void setIssuerDisabled(final String name, final Integer paymentSystemId) {
        persistenceService.setIssuerStatus(name, paymentSystemId, DSModel.Issuer.Status.DISABLED);
    }

    public ACSProfile getACSProfileByUrlAndIssuerId(final String url, final Integer issuerId) {
        return persistenceService.getACSProfileByUrlAndIssuerId(url, issuerId);
    }

    public Issuer getIssuerByNameAndPaymentSystemID(final String name, final Integer paymentSystemId) {
        return persistenceService.getIssuerByNameAndPaymentSystem(name, paymentSystemId);
    }

    public boolean isExistingDuplicateIssuersBin(Integer issuerId, String issuerBin, Integer paymentSystemId) {
        return persistenceService.isExistingDuplicateIssuersBin(issuerId, issuerBin, paymentSystemId);
    }

    public Boolean isIssuerExists(String issuerName, Integer paymentSystemId, Integer issuerId) {
        return persistenceService.isIssuerExists(issuerName, paymentSystemId, issuerId);
    }

    public List<Issuer> getIssuersByQuery(IssuerSearcher searcher) throws Exception {
        List<Issuer> found = null;
        if (searcher != null) {
            Map<String, Object> query = new LinkedHashMap<>();
            if (Misc.isNotNullOrEmpty(searcher.getName())) {
                query.put("name", searcher.getName() + "%");
            }
            if (Misc.isNotNullOrEmpty(searcher.getStatus())) {
                query.put("status", searcher.getStatus());
            }
            if (searcher.getPaymentSystemId() != null) {
                query.put("paymentSystemId", searcher.getPaymentSystemId());
            }
            if (Misc.isNotNullOrEmpty(searcher.getCountry())) {
                query.put("country", searcher.getCountry());
            }
            if (Misc.isNotNullOrEmpty(searcher.getCity())) {
                query.put("city", searcher.getCity());
            }

            if (Misc.isNotNullOrEmpty(searcher.getBIN())) {
                query.put("BIN", searcher.getBIN());
            }

            if (searcher.getPan() != null && searcher.getPan().length() <= 19 && searcher.getPan().length() >= 13) {
                CardRange matchedRange;
                if (isBINRangeMode()) {
                    matchedRange = getBestMatchingIssuerCardBinByRange(searcher.getPan());
                } else {
                    String issuerBIN = Misc.trunc(searcher.getPan(), getMaxBINLength());
                    matchedRange = getBestMatchingIssuerCardBin(issuerBIN);
                }
                if (matchedRange != null) {
                    query.put("id:", new Object[]{matchedRange.getIssuerId()});
                }
            }

            if (Misc.isNotNullOrEmpty(searcher.getBinMatch()) && searcher.getBinMatch().length() > 3) {
                Map<String, Object> rquery = new LinkedHashMap<>();
                rquery.put("binSt", searcher.getBinMatch() + "%");
                List<CardRange> idsctr = this.persistenceService.getPersitableList(
                        CardRange.class, "binSt", null, true, null, null, rquery);

                List<Object> ids = new LinkedList<>();
                for (CardRange ctr : idsctr) {
                    ids.add(ctr.getIssuerId());
                }
                Map<String, Object> rquery2 = new LinkedHashMap<>();
                rquery2.put("end", searcher.getBinMatch() + "%");
                List<CardRange> idsctr2 = this.persistenceService.getPersitableList(
                        CardRange.class, "binSt", null, true, null, null, rquery2);
                for (CardRange ctr : idsctr2) {
                    ids.add(ctr.getIssuerId());
                }
                if (ids.size() == 0) {
                    ids.add(0); // for no results
                }
                query.put("id:", ids.toArray(new Object[ids.size()]));
            }

            found = this.persistenceService.getPersitableList(Issuer.class,
                                                                                           searcher.getOrder(),
                                                                                           searcher.getOrderDirection(),
                                                                                           true, searcher.getStart(),
                                                                                           searcher.getLimit(), query);
            if (query.get("total") != null) {
                searcher.setTotal(((Long) query.get("total")).intValue());
            }
        }
        return found;
    }

    public List<CardRange> getIssuerCardBins(Integer issuerId) {
        return persistenceService.getCardRangesByIssuerId(issuerId);
    }

    public List<CardRange> getExistingCardRangesPerPaymentSystem(Long cardRangeId, String bin, Integer paymentSystemId) {
        return persistenceService.getCardRangesPerPaymentSystem(cardRangeId, bin, paymentSystemId);
    }

    public boolean isCardRangeNameExistingUnderPaymentSystem(String name, Long cardRangeId, Integer paymentSystemId) {
        return persistenceService.isCardRangeNameExistingUnderPaymentSystem(name, cardRangeId, paymentSystemId);
    }

    public void getIssuerCardBins(List<String> check, Map<String, CardRange> results) throws Exception {
        if (check != null && check.size() > 0 && results != null) {
            persistenceService.getIssuerCardBins(check, results);
        }
    }

    public CardRange getBestMatchingIssuerCardBin(String maxBin, Integer paymentSystemId) {
        if (maxBin != null) {
            return persistenceService.getBestMatchingIssuerCardBin(maxBin, paymentSystemId, getMinBINLength(),
                                                                   getMaxBINLength());
        }

        return null;
    }

    public CardRange getBestMatchingIssuerCardBin(String maxBin) {
        if (maxBin != null) {
            return persistenceService.getBestMatchingIssuerCardBin(maxBin, getMinBINLength(), getMaxBINLength());
        }

        return null;
    }

    public CardRange getBestMatchingIssuerCardBinByRange(String pan) throws Exception {
        if (pan != null) {
            return persistenceService.getBestMatchingIssuerCardBinByRange(pan);
        }
        return null;
    }

    public CardRange getBestMatchingIssuerCardBinByRange(String pan, Integer paymentSystemId) throws Exception {
        if (pan != null) {
            return persistenceService.getBestMatchingIssuerCardBinByRange(pan, paymentSystemId);
        }
        return null;
    }

    public List<ACSProfile> getIssuerACSList(Integer issuerId, Short set, String status, boolean cacped) {
        List<ACSProfile> list = null;

        if (issuerId != null) {
            final String ck = "" + issuerId + "-" + set + "-" + status;
            CacheObject<List<ACSProfile>> co = acsListCache.get(ck);

            if (!cacped || co == null || co.loaded < System.currentTimeMillis() - 60000) {

                list = persistenceService.getAcsProfiles(issuerId, set, status);

                if (co == null) {
                    co = new CacheObject<>();
                    co.obj = list;
                    co.loaded = System.currentTimeMillis();
                    synchronized (acsListCache) {
                        acsListCache.put(ck, co);
                    }
                } else {
                    co.obj = list;
                    co.loaded = System.currentTimeMillis();
                }
            } else {
                list = co.obj;
            }
        }
        return list;
    }

    public List<ACSProfile> getACSListByCertId(Long certId, String status, boolean cached) {
        List<ACSProfile> list = null;

        if (certId != null) {
            final String ck = "ce" + certId + "-" + status;
            CacheObject<List<ACSProfile>> co = acsListCache.get(ck);

            if (!cached || co == null || co.loaded < System.currentTimeMillis() - 60000) {

                list = persistenceService.getAcsProfiles(certId, status);

                if (co == null) {
                    co = new CacheObject<>();
                    synchronized (acsListCache) {
                        acsListCache.put(ck, co);
                    }
                }
                co.obj = list;
                co.loaded = System.currentTimeMillis();
            } else {
                list = co.obj;
            }
        }
        return list;
    }

    public ACSProfile getACSById(Integer acsId, boolean cached) throws Exception {
        ACSProfile acp = null;

        if (acsId != null) {
            final String ck = "acs-" + acsId;
            CacheObject<List<ACSProfile>> co = acsListCache.get(ck);

            if (!cached || co == null || co.loaded < System.currentTimeMillis() - 60000) {
                acp = this.persistenceService.getPersistableById(acsId, ACSProfile.class);
                if (co == null) {
                    co = new CacheObject<>();
                    synchronized (acsListCache) {
                        acsListCache.put(ck, co);
                    }
                }
                List<ACSProfile> acpl = new java.util.ArrayList<>(1);
                if (acp != null) {
                    acpl.add(acp);
                }
                co.obj = acpl;
                co.loaded = System.currentTimeMillis();
            } else {
                List<ACSProfile> acpl = co.obj;
                acp = acpl != null && acpl.size() > 0 ? acpl.get(0) : null;
            }
        }
        return acp;
    }

    public List<ACSProfile> getACSProfilesByRefNoAndIssId(String refNo, Integer issid) {
        List<ACSProfile> result = null;
        if (Misc.isNotNullOrEmpty(refNo)) {
            result = persistenceService.getAcsProfiles(refNo, issid);
        }

        return result;
    }

    public List<ACSProfile> getAttemptsACSList(String status, Integer paymentSystemId) {
        return persistenceService.getAttemptsACSList(status, paymentSystemId);
    }

    public List<ACSProfile> getAcsProfileByNameAndPaymentSystem(String name, Integer paymentSystemId) {
        return persistenceService.getAcsProfileByNameAndPaymentSystem(name, paymentSystemId);
    }

    public List<ACSProfile> searchAttemptsACS(AcsSearcher acsSearcher) {
        Map<String, Object> query = new HashMap<>();
        query.put("issuerId", PersistenceService.IS_NULL_OPERATOR);
        if (StringUtils.isNotEmpty(acsSearcher.getName())) {
            query.put("name", acsSearcher.getName() + "%");
        }
        if (acsSearcher.getPaymentSystemId() != null) {
            query.put("paymentSystemId", acsSearcher.getPaymentSystemId());
        }
        if (StringUtils.isNotEmpty(acsSearcher.getStatus())) {
            query.put("status", acsSearcher.getStatus());
        }
        return this.persistenceService.searchList(ACSProfile.class, acsSearcher, query);
    }

    public ACSProfile getAttemptsACSById(Integer aacsId) {
        return persistenceService.getAttemptsACSById(aacsId);
    }

    public void saveOrUpdate(Issuer issuer, User changer) throws Exception {
        if (issuer.getId() == null) {
            insertNewIssuer(issuer, changer);
        } else {
            updateExistingIssuer(issuer, changer);
        }
    }

    private void insertNewIssuer(Issuer issuer, User changer) throws Exception {
        issuer.setCreatedDate(LocalDateTime.now());
        issuer.setLastModifiedDate(LocalDateTime.now());
        persistenceService.saveOrUpdate(issuer);
        auditLogService.logInsert(changer, issuer, null);
    }

    private void updateExistingIssuer(Issuer updatedIssuer, User changer) throws Exception {
        Issuer oldIssuer = getIssuerById(updatedIssuer.getId());
        updatedIssuer.setPaymentSystemId(oldIssuer.getPaymentSystemId());
        updatedIssuer.setCreatedDate(oldIssuer.getCreatedDate());
        updatedIssuer.setLastModifiedDate(LocalDateTime.now());
        persistenceService.saveOrUpdate(updatedIssuer);
        auditLogService.logUpdate(changer, oldIssuer, updatedIssuer, null);
    }

    public void prepareIssuer(final Issuer issuer) {
        issuer.setName(StringUtils.trim(issuer.getName()));
        issuer.setEmail(StringUtils.trim(issuer.getEmail()));
        issuer.setAddress(StringUtils.trim(issuer.getAddress()));
        issuer.setCity(StringUtils.trim(issuer.getCity()));
        issuer.setPhone(StringUtils.trim(issuer.getPhone()));
    }

    public Map<String, String> getIssuerDropdownMap(Integer paymentSystemId) {
        Map<String, String> issuers = new LinkedHashMap<>();
        issuers.put("", "");
        try {
            List<Issuer> accessibleIssuers = persistenceService.getPersitableList(Issuer.class, "name", null);

            if (paymentSystemId != null) {
                accessibleIssuers = accessibleIssuers.stream().filter(
                        issuer -> paymentSystemId.equals(issuer.getPaymentSystemId())).collect(Collectors.toList());
            }

            for (int i = 0; i < accessibleIssuers.size(); i++) {
                Issuer ix = accessibleIssuers.get(i);
                issuers.put(Integer.toString(ix.getId()), ix.getName());
            }

        } catch (Exception e) {
            log.error("Error getting issuer list", e);
        }

        return issuers;
    }
}