package com.modirum.ds.services;

import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.model.IssuerSearcher;
import com.modirum.ds.db.model.Merchant;
import com.modirum.ds.db.model.TDSServerProfile;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public class AcquirerService {
    private final PersistenceService persistenceService;
    private static Logger log = LoggerFactory.getLogger(AcquirerService.class);

    public AcquirerService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public Acquirer getAcquirerById(final Integer acquirerId) {
        return persistenceService.getPersistableByIdUnchecked(acquirerId, Acquirer.class);
    }

    public TDSServerProfile getTdsServerProfileById(final Long tdsServerProfileId) {
        return persistenceService.getPersistableByIdUnchecked(tdsServerProfileId, TDSServerProfile.class);
    }

    public List<Acquirer> getAcquirersByQuery(IssuerSearcher searcher) throws Exception {
        List<Acquirer> found = null;
        if (searcher != null) {
            Map<String, Object> query = new LinkedHashMap<>();
            if (Misc.isNotNullOrEmpty(searcher.getName())) {
                query.put("name", searcher.getName() + "%");
            }
            if (Misc.isNotNullOrEmpty(searcher.getStatus())) {
                query.put("status", searcher.getStatus());
            }
            if (Misc.isNotNullOrEmpty(searcher.getCountry())) {
                query.put("country", searcher.getCountry());
            }
            if (Misc.isNotNullOrEmpty(searcher.getCity())) {
                query.put("city", searcher.getCity());
            }
            if (Misc.isNotNullOrEmpty(searcher.getBIN())) {
                query.put("BIN", searcher.getBIN());
            }
            if (Misc.isNotNullOrEmpty(searcher.getPaymentSystemId())) {
                query.put("paymentSystemId", searcher.getPaymentSystemId());
            }

            found = persistenceService.getPersitableList(Acquirer.class,
                searcher.getOrder(),
                searcher.getOrderDirection(),
                true, searcher.getStart(),
                searcher.getLimit(), query);
            if (query.get("total") != null) {
                searcher.setTotal(((Long) query.get("total")).intValue());
            }
        }
        return found;
    }

    public Map<String, Merchant> getMerchantsByIdentifierAndAcquirerId(List<String> idCheck, List<Integer> aidCheck) {
        Map<String, Merchant> results = new HashMap<>();
        if (idCheck != null && idCheck.size() > 0 && aidCheck != null) {
            List<Merchant> foundMerchants = persistenceService.getMerchantsByIdentifierAndAcquirerId(idCheck, aidCheck);
            for (Merchant merchant : foundMerchants) {
                results.put(merchant.getIdentifier() + "-" + merchant.getAcquirerId(), merchant);
            }
        }
        return results;
    }

    public Boolean isExistingDuplicateAcquirerName(Integer acquirerId, String acquirerName, Integer paymentSystemId) {
        return persistenceService.isExistingDuplicateAcquirerName(acquirerId, acquirerName, paymentSystemId);
    }

    public Boolean isExistingDuplicateAcquirerBIN(Integer acquirerId, String acquirerBIN, Integer paymentSystemId) {
        return persistenceService.isExistingDuplicateAcquirerBIN(acquirerId, acquirerBIN, paymentSystemId);
    }

    public List<TDSServerProfile> getTdsServerProfilesByRequestorIdOrOperatorId(String requestorID, String operatorID) {
        List<TDSServerProfile> result = null;
        if (requestorID != null || operatorID != null) {
            result = persistenceService.getTdsServerProfilesByRequestorIdOrOperatorId(requestorID, operatorID
            );
        }

        return result;
    }

    public List<TDSServerProfile> getTdsServerProfilesByNameAndPaymentSystem(String name, Integer paymentSystemId) {
        return persistenceService.getTdsServerProfilesByNameAndPaymentSystem(name, paymentSystemId);
    }

    public TDSServerProfile getActiveTdsServerProfileByOperatorId(Integer paymentSystemId, String operatorID) {
        TDSServerProfile tdsServerProfile = null;
        if (operatorID != null) {
            tdsServerProfile = persistenceService.getActiveTdsServerProfileByOperatorId(paymentSystemId, operatorID);
        }

        return tdsServerProfile;
    }

    public Map<String, String> getAcquirerDropdownMap(Locale locale, Integer paymentSystemId) {
        List<Acquirer> acquirerList = persistenceService.getAcquirers(paymentSystemId);
        Map<String, String> acquirers = new LinkedHashMap<>();
        acquirers.put("", "");
        for (Acquirer acquirer : acquirerList) {
            acquirers.put("" + acquirer.getId(), acquirer.getName());
        }

        return acquirers;
    }

    public Map<String, String> getTDSServerDropdown(Integer paymentSystemId) throws Exception {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("", "");
        Map<String, Object> query = new HashMap<>();
        if (paymentSystemId != null) {
            query.put("paymentSystemId", paymentSystemId);
        }
        List<TDSServerProfile> tdsServerProfiles = Optional
                .ofNullable(persistenceService.getPersitableList(TDSServerProfile.class, "name", null, true, 0, null, query))
                .orElse(Collections.emptyList());
        for (TDSServerProfile tdsServer : tdsServerProfiles) {
            map.put(String.valueOf(tdsServer.getId()), "" + tdsServer.getId() + "-" + tdsServer.getName());
        }
        return map;
    }

    public Boolean isExistingDuplicateOperatorId(String operatorId, Integer paymentSystemId, Long tdsServerProfileId) {
        return persistenceService.isExistingDuplicateOperatorId(operatorId, paymentSystemId, tdsServerProfileId);
    }

    public Acquirer getAcquirer(String bin, Integer paymentSystemId) {
        try {
            return persistenceService.getAcquirerByBINAndPaymentSystem(bin , paymentSystemId);
        } catch (Exception e) {
            log.info("Unable to find  acquirer with bin {} and paymentSystemId {}", bin , paymentSystemId);
            return null;
        }
    }
}
