package com.modirum.ds.providers;

import com.modirum.ds.utils.ISO3166;

import java.util.ArrayList;
import java.util.List;

public class CountriesProvider {

    private static ISO3166[] isoCountries = ISO3166.values();

    private static List<String> a2CountriesCodes;
    private static List<String> a3CountriesCodes;

    public static List<String> getA2CountriesCodes() {
        if (a2CountriesCodes == null) {
            a2CountriesCodes = new ArrayList<>();
            for (ISO3166 isoCountry : getIsoCountries()) {
                a2CountriesCodes.add(isoCountry.getA2code());
            }
        }
        return a2CountriesCodes;
    }

    public static List<String> getA3CountriesCodes() {
        if (a3CountriesCodes == null) {
            a3CountriesCodes = new ArrayList<>();
            for (ISO3166 isoCountry : getIsoCountries()) {
                a3CountriesCodes.add(isoCountry.getA3code());
            }
        }
        return a3CountriesCodes;
    }

    public static ISO3166[] getIsoCountries() {
        return isoCountries;
    }
}
