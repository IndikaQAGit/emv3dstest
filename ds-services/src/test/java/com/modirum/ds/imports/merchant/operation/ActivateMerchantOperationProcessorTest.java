package com.modirum.ds.imports.merchant.operation;

import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import com.modirum.ds.services.MerchantService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ActivateMerchantOperationProcessorTest {

    private ImportMerchantModelProvider modelProvider = new ImportMerchantModelProvider();

    @Mock
    private MerchantService merchantService;

    @Test
    void testProcess() {
        BatchMerchantModel model = modelProvider.getChangeModel();
        ActivateMerchantOperationProcessor processor = new ActivateMerchantOperationProcessor(merchantService);
        processor.process(Collections.singletonList(model));
        verify(merchantService).setMerchantActive(model.getName(), Integer.parseInt(model.getPaymentSystemId()));
    }
}