package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.db.dao.PersistenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DeleteIssuerOperationProcessorTest {

    private ImportIssuerModelProvider modelProvider = new ImportIssuerModelProvider();

    @Mock
    private PersistenceService persistenceService;

    @Test
    void testProcess() {
        BatchIssuerModel model = modelProvider.getValidModel();
        DeleteIssuerOperationProcessor processor = new DeleteIssuerOperationProcessor(persistenceService);
        processor.process(Collections.singletonList(model));
        verify(persistenceService).deleteIssuerByNameAndPaymentSystemId(model.getName(), Integer.parseInt(model.getPaymentSystemId()));
    }
}