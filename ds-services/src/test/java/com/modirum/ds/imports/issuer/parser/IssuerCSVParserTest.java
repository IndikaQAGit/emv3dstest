package com.modirum.ds.imports.issuer.parser;

import com.modirum.ds.imports.CSVParserException;
import com.modirum.ds.imports.DataBatchBundle;
import com.modirum.ds.imports.DataChangeStateOperation;
import com.modirum.ds.imports.merchant.parser.BatchMerchantModel;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.modirum.ds.TestResourceFileReader.ISSUERS_ADD;
import static com.modirum.ds.TestResourceFileReader.ISSUERS_ADD_MISSING_ELEMENTS;
import static com.modirum.ds.TestResourceFileReader.ISSUERS_CHANGE_STATE;
import static com.modirum.ds.TestResourceFileReader.readFileAsString;
import static org.junit.jupiter.api.Assertions.*;

class IssuerCSVParserTest {

    private IssuerCSVParser parser = new IssuerCSVParser();

    @Test
    public void testParseThrowsError() throws Exception {
        String fileContent = readFileAsString(ISSUERS_ADD_MISSING_ELEMENTS);
        assertThrows(CSVParserException.class, () -> parser.parse(fileContent));
    }


    @Test
    public void testParseWithChangeStateHeader() throws Exception {
        String fileContent = readFileAsString(ISSUERS_CHANGE_STATE);
        DataBatchBundle<BatchIssuerModel> model = parser.parse(fileContent);

        List<BatchIssuerModel> batchMerchantModels = model.getImportModels();
        assertEquals(model.getImportModels().size(), 3);

        assertEquals("issuer1", batchMerchantModels.get(0).getName());
        assertEquals("issuer2", batchMerchantModels.get(1).getName());
        assertEquals("issuer3", batchMerchantModels.get(2).getName());

        assertEquals(DataChangeStateOperation.ACTIVATE, batchMerchantModels.get(0).getOperation());
        assertEquals(DataChangeStateOperation.DISABLE, batchMerchantModels.get(1).getOperation());
        assertEquals(DataChangeStateOperation.DELETE, batchMerchantModels.get(2).getOperation());

        assertEquals("1", batchMerchantModels.get(0).getPaymentSystemId());
        assertEquals("2", batchMerchantModels.get(1).getPaymentSystemId());
        assertEquals("3", batchMerchantModels.get(2).getPaymentSystemId());
    }

    @Test
    public void testParseWithAddStateHeader() throws Exception {
        String fileContent = readFileAsString(ISSUERS_ADD);
        DataBatchBundle<BatchIssuerModel> model = parser.parse(fileContent);
        List<BatchIssuerModel> batchIssuerModels = model.getImportModels();
        assertEquals(batchIssuerModels.size(), 2);

        assertEquals("testIssuer", batchIssuerModels.get(0).getName());
        assertEquals("testIssuer2", batchIssuerModels.get(1).getName());

        assertEquals("1200", batchIssuerModels.get(0).getBin());
        assertEquals("1300", batchIssuerModels.get(1).getBin());

        assertEquals("active", batchIssuerModels.get(0).getStatus());
        assertEquals("disabled", batchIssuerModels.get(1).getStatus());

        assertEquals("1", batchIssuerModels.get(0).getPaymentSystemId());
        assertEquals("2", batchIssuerModels.get(1).getPaymentSystemId());
    }
}