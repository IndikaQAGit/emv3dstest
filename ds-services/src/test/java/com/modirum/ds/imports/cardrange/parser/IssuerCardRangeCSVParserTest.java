package com.modirum.ds.imports.cardrange.parser;

import com.modirum.ds.imports.CSVParserException;
import com.modirum.ds.imports.DataBatchBundle;
import com.modirum.ds.imports.DataChangeStateOperation;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.modirum.ds.TestResourceFileReader.ISSUER_CARD_RANGE_ADD;
import static com.modirum.ds.TestResourceFileReader.ISSUER_CARD_RANGE_CHANGE_STATE;
import static com.modirum.ds.TestResourceFileReader.ISSUER_CARD_RANGE_MISSING_ELEMENTS;
import static com.modirum.ds.TestResourceFileReader.readFileAsString;
import static org.junit.jupiter.api.Assertions.*;

class IssuerCardRangeCSVParserTest {

    private final IssuerCardRangeCSVParser parser = new IssuerCardRangeCSVParser();

    @Test
    public void testParseThrowsError() throws Exception {
        String fileContent = readFileAsString(ISSUER_CARD_RANGE_MISSING_ELEMENTS);
        assertThrows(CSVParserException.class, () -> parser.parse(fileContent));
    }

    @Test
    public void testParseWithChangeStateHeader() throws Exception {
        String fileContent = readFileAsString(ISSUER_CARD_RANGE_CHANGE_STATE);
        DataBatchBundle<BatchCardRangeModel> model = parser.parse(fileContent);

        List<BatchCardRangeModel> batchMerchantModels = model.getImportModels();
        assertEquals(model.getImportModels().size(), 3);

        assertEquals("120000", batchMerchantModels.get(0).getBinStart());
        assertEquals("130000", batchMerchantModels.get(1).getBinStart());
        assertEquals("140000", batchMerchantModels.get(2).getBinStart());

        assertEquals("125000", batchMerchantModels.get(0).getBinEnd());
        assertEquals("135000", batchMerchantModels.get(1).getBinEnd());
        assertEquals("145000", batchMerchantModels.get(2).getBinEnd());

        assertEquals(DataChangeStateOperation.ACTIVATE, batchMerchantModels.get(0).getOperation());
        assertEquals(DataChangeStateOperation.DISABLE, batchMerchantModels.get(1).getOperation());
        assertEquals(DataChangeStateOperation.DELETE, batchMerchantModels.get(2).getOperation());

        assertEquals("1", batchMerchantModels.get(0).getPaymentSystemId());
        assertEquals("2", batchMerchantModels.get(1).getPaymentSystemId());
        assertEquals("3", batchMerchantModels.get(2).getPaymentSystemId());
    }

    @Test
    public void testParseWithAddStateHeader() throws Exception {
        String fileContent = readFileAsString(ISSUER_CARD_RANGE_ADD);
        DataBatchBundle<BatchCardRangeModel> model = parser.parse(fileContent);
        List<BatchCardRangeModel> importIssuerModels = model.getImportModels();
        assertEquals(importIssuerModels.size(), 2);

        assertEquals("testIssuer", importIssuerModels.get(0).getIssuerName());
        assertEquals("testIssuer2", importIssuerModels.get(1).getIssuerName());

        assertEquals("120000", importIssuerModels.get(0).getBinStart());
        assertEquals("130000", importIssuerModels.get(1).getBinStart());

        assertEquals("125000", importIssuerModels.get(0).getBinEnd());
        assertEquals("135000", importIssuerModels.get(1).getBinEnd());

        assertEquals("participating", importIssuerModels.get(0).getStatus());
        assertEquals("notparticipating", importIssuerModels.get(1).getStatus());

        assertEquals("1", importIssuerModels.get(0).getPaymentSystemId());
        assertEquals("2", importIssuerModels.get(1).getPaymentSystemId());

        assertEquals("1", importIssuerModels.get(0).getCardType());
        assertEquals("2", importIssuerModels.get(1).getCardType());

        assertEquals("1", importIssuerModels.get(0).getSet());
        assertEquals("2", importIssuerModels.get(1).getSet());

        assertEquals("2", importIssuerModels.get(0).getAcsSet());
        assertEquals("3", importIssuerModels.get(1).getAcsSet());
    }
}