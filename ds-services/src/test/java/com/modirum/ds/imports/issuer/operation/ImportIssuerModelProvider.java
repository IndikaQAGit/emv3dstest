package com.modirum.ds.imports.issuer.operation;

import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;

public class ImportIssuerModelProvider {

    public BatchIssuerModel getInalidModel() {
        return BatchIssuerModel.builder()
            .bin("10000")
            .name("Name")
            .phone("Phone")
            .email("Email")
            .country("Country")
            .status("Active")
            .paymentSystemId("1")
            .build();
    }

    public BatchIssuerModel getValidModel() {
        return BatchIssuerModel.builder()
            .bin("10000")
            .name("Name")
            .phone("832512351")
            .email("test@modirum.com")
            .country("AU")
            .status("active")
            .paymentSystemId("1")
            .build();
    }
}
