package com.modirum.ds.hsm;

import mockit.Expectations;
import mockit.Mocked;
import org.apache.commons.lang3.ArrayUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;

public class HSMR10KServiceTest {

    private static HSMR10KService r10k;

    @Mocked
    private DatagramSocket udpSocketMock;
    @Mocked
    private DatagramPacket udpPacketMock;

    @BeforeAll
    public static void beforeClass() {
        r10k = new HSMR10KService("127.0.0.1", 1500);
    }

    @Test
    public void generateHSMDataEncryptionKey_success() throws IOException {
        String expectedKey = "10128D0AB00N0000295AEE55056CF676533C336C9F91E36A3B2F8E29229D7F284D007F3116B824F6F36214C5CCA5AD0CD021F672DA12485E96A79800A093DA0A";
        String expectedKCV = "545DE5";

        byte[] emulatedHsmResponseBytes = getHsmResponseBytes(("A100S" + expectedKey + expectedKCV).getBytes());
        new Expectations() {{
            udpPacketMock.getData();
            result = emulatedHsmResponseBytes;
        }};

        HSMDevice.Key generatedDEK = null;
        try {
            generatedDEK = r10k.generateHSMDataEncryptionKey();
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during generateHSMDataEncryptionKey_success.");
        }

        Assertions.assertNotNull(generatedDEK);
        Assertions.assertEquals(expectedKey, new String(generatedDEK.getValue()));
        Assertions.assertEquals(expectedKCV, Hex.toHexString(generatedDEK.getCheckValue()).toUpperCase());
    }

    @Test
    public void encryptData_success() throws IOException {
        String encryptionKey = "10128D0AB00N0000295AEE55056CF676533C336C9F91E36A3B2F8E29229D7F284D007F3116B824F6F36214C5CCA5AD0CD021F672DA12485E96A79800A093DA0A";
        String cleartextData = "Non padded dætä cleartext";
        byte[] cleartextDataBytes = cleartextData.getBytes(StandardCharsets.UTF_8);

        String expectedEncryptedBytesHexed = "F9BA08E48F7775C9D598CB524C26F602C33800C49483B99A04F2646185676234";

        byte[] emulatedHsmResponseBytes = getHsmResponseBytes(Hex.decode("4D31303030303230" + expectedEncryptedBytesHexed));
        new Expectations() {{
            udpPacketMock.getData();
            result = emulatedHsmResponseBytes;
        }};

        byte[] encryptedBytes = null;
        try {
            encryptedBytes = r10k.encryptData(encryptionKey.getBytes(), cleartextDataBytes);
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during encryptData_success.");
        }

        Assertions.assertNotNull(encryptedBytes);
        String actualEncrypredBytesHexed = Hex.toHexString(encryptedBytes).toUpperCase();
        Assertions.assertEquals(expectedEncryptedBytesHexed, actualEncrypredBytesHexed);
    }

    @Test
    public void decryptData_success() throws IOException {
        String encryptionKey = "10128D0AB00N0000295AEE55056CF676533C336C9F91E36A3B2F8E29229D7F284D007F3116B824F6F36214C5CCA5AD0CD021F672DA12485E96A79800A093DA0A";
        String encryptedDataBytesHexed = "F9BA08E48F7775C9D598CB524C26F602C33800C49483B99A04F2646185676234";

        String expectedClearText = "Non padded dætä cleartext";

        byte[] emulatedPayloadPart = r10k.pkcs5pad(expectedClearText.getBytes(StandardCharsets.UTF_8), r10k.BLOCK_SIZE_AES);
        byte[] emulatedCmdAndPayloadPart = ArrayUtils.addAll(Hex.decode("4D33303030303230"), emulatedPayloadPart);
        byte[] emulatedHsmResponseBytes = getHsmResponseBytes(emulatedCmdAndPayloadPart);
        new Expectations() {{
            udpPacketMock.getData();
            result = emulatedHsmResponseBytes;
        }};

        byte[] decryptedBytes = null;
        try {
            decryptedBytes = r10k.decryptData(encryptionKey.getBytes(), Hex.decode(encryptedDataBytesHexed));
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during decryptData_success.");
        }

        Assertions.assertNotNull(decryptedBytes);
        String actualDecryptedStringData = new String(decryptedBytes, StandardCharsets.UTF_8);
        Assertions.assertEquals(expectedClearText, actualDecryptedStringData);
    }

    @Test
    public void getMasterKeyCheckValue_success() throws IOException {
        String expectedMasterKeyKCV = "9D04A0";

        byte[] emulatedHsmResponseBytes = getHsmResponseBytes(("ND00" + expectedMasterKeyKCV
                + "00000000001500-0022").getBytes());
        new Expectations() {{
            udpPacketMock.getData();
            result = emulatedHsmResponseBytes;
        }};

        String actualMasterKeyKCV = null;
        try {
            actualMasterKeyKCV = r10k.getMasterKeyCheckValue();
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during getMasterKeyCheckValue_success.");
        }

        Assertions.assertNotNull(actualMasterKeyKCV);
        Assertions.assertEquals(expectedMasterKeyKCV, actualMasterKeyKCV);
    }

    @Test
    public void getHardwareSerial_success() throws IOException {
        String expectedHardwareSerial = "S0246377943M";

        byte[] emulatedHsmResponseBytes = getHsmResponseBytes(("JL00" + expectedHardwareSerial
                + "200520103807121303101010100000K2TAES Test LMK\u0014\u001500").getBytes());
        new Expectations() {{
            udpPacketMock.getData();
            result = emulatedHsmResponseBytes;
        }};

        String actualHardwareSerial = null;
        try {
            actualHardwareSerial = r10k.getHardwareSerial();
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during getHardwareSerial_success.");
        }

        Assertions.assertNotNull(actualHardwareSerial);
        Assertions.assertEquals(expectedHardwareSerial, actualHardwareSerial);
    }

    @Test
    public void encryptDataInHSMWithMaster_success() {
        Exception actualException = null;
        try {
            r10k.encryptDataInHSMWithMaster(new byte[0], true);
        } catch (Exception e) {
            actualException = e;
        }

        Assertions.assertNotNull(actualException);
        Assertions.assertTrue(actualException instanceof UnsupportedOperationException, "Expected UnsupportedOperationException");
    }


    /////////////
    // HELPERS //
    /////////////

    private static byte[] getHsmResponseBytes(byte[] expectedResponse) {
        // hsm prepends HEADER to every command
        // hsm TCP/UDP response has 2 first bytes reserved for length.
        int cmdLength = HSMR10KService.HEADER.length + expectedResponse.length;
        byte[] cmdLengthAsBytes = new byte[] {
                (byte) (cmdLength >>> 8),
                (byte) cmdLength
        };

        byte[] responseBytes = ArrayUtils.addAll(cmdLengthAsBytes, HSMR10KService.HEADER);
        return ArrayUtils.addAll(responseBytes, expectedResponse);
    }
}
