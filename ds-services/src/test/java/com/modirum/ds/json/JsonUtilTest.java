package com.modirum.ds.json;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.json.JsonObject;


import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import java.io.Serializable;

import static org.junit.jupiter.api.Assertions.*;

class JsonUtilTest {

    private static JsonObject jsonObject;

    private static String validJson = "{\n" +
            "\"acctInfo\" : {\"chAccAgeInd\" : \"05\", \"chAccChange\" : \"20180808\", \"chAccChangeInd\" : \"04\", \"chAccDate\" : \"20180808\", \"chAccPwChange\" : \"20180808\", \"chAccPwChangeInd\" : \"05\", \"nbPurchaseAccount\" : \"24\", \"provisionAttemptsDay\" : \"1\", \"txnActivityDay\" : \"2\", \"txnActivityYear\" : \"100\", \"paymentAccAge\" : \"20180808\", \"paymentAccInd\" : \"05\", \"shipAddressUsage\" : \"20180808\", \"shipAddressUsageInd\" : \"04\", \"shipNameIndicator\" : \"01\", \"suspiciousAccActivity\" : \"01\"},\n" +
            "\"acctNumber\" : \"400000#######000\",\n" +
            "\"cardExpiryDate\" : \"2404\",\n" +
            "\"acctType\" : \"02\",\n" +
            "\"acquirerBIN\" : \"444444\",\n" +
            "\"acquirerMerchantID\" : \"000001\",\n" +
            "\"addrMatch\" : \"Y\",\n" +
            "\"browserAcceptHeader\" : \"text/html, application/xhtml+xml, */*\",\n" +
            "\"browserIP\" : \"216.58.197.110\",\n" +
            "\"browserJavaEnabled\" : true,\n" +
            "\"browserLanguage\" : \"en-GB\",\n" +
            "\"browserColorDepth\" : \"24\",\n" +
            "\"browserScreenHeight\" : \"1137\",\n" +
            "\"browserScreenWidth\" : \"2021\",\n" +
            "\"browserTZ\" : \"-180\",\n" +
            "\"browserUserAgent\" : \"Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko\",\n" +
            "\"deviceChannel\" : \"02\",\n" +
            "\"mcc\" : \"5499\",\n" +
            "\"merchantCountryCode\" : \"246\",\n" +
            "\"merchantName\" : \"Modirum DS-3DS-Emulator\",\n" +
            "\"merchantRiskIndicator\" : { \"shipIndicator\" : \"07\" },\n" +
            "\"messageCategory\" : \"01\",\n" +
            "\"messageType\" : \"AReq\",\n" +
            "\"messageVersion\" : \"2.1.0\",\n" +
            "\"notificationURL\" : \"https://3ds2-ds.ci.modirum.com/ds-3ds-emulator/randomUnusedAnywayPage.jsp\",\n" +
            "\"purchaseAmount\" : \"10600\",\n" +
            "\"purchaseCurrency\" : \"840\",\n" +
            "\"purchaseExponent\" : \"2\",\n" +
            "\"purchaseDate\" : \"20170127075424\",\n" +
            "\"purchaseInstalData\" : \"123\",\n" +
            "\"recurringExpiry\" : \"20170127\",\n" +
            "\"recurringFrequency\" : \"1234\",\n" +
            "\"threeDSCompInd\" : \"U\",\n" +
            "\"threeDSRequestorAuthenticationInd\" : \"03\",\n" +
            "\"threeDSRequestorChallengeInd\" : \"01\",\n" +
            "\"threeDSRequestorID\" : \"7979791\",\n" +
            "\"threeDSRequestorName\" : \"Modirum DS-3DS-Emulator\",\n" +
            "\"threeDSRequestorURL\" : \"http://localhost\",\n" +
            "\"threeDSServerRefNumber\" : \"DS-3DS-EMULATOR-REF-NUMBER\",\n" +
            "\"threeDSServerOperatorID\" : \"DS-3DS-EMULATOR-OPER-ID\",\n" +
            "\"threeDSServerTransID\" : \"4cdb7520-a404-4b08-bd5b-d9cf64cfd66a\",\n" +
            "\"threeDSServerURL\" : \"https://3ds2-ds.ci.modirum.com/ds-3ds-emulator/3dsServerRreqStub.jsp\",\n" +
            "\"transType\" : \"28\"\n" +
            "}";

    private static String invalidJson1 = "I am an invalid Json";
    private static String invalidJson2 ="No content, HTTP code: null";

    @BeforeAll
    public static void init() {
        JsonObjectBuilder builder = new JsonProviderImpl().createObjectBuilder();
        builder.add("emptyValue", "");
        builder.add("nullValue", new JsonValueImpl(JsonValue.ValueType.NULL));
        jsonObject = builder.build();
    }

    @Test
    void isJsonValid() {
        assertTrue(JsonUtil.isJsonValid(validJson));
        assertFalse(JsonUtil.isJsonValid(invalidJson1));
        assertFalse(JsonUtil.isJsonValid(invalidJson2));
    }

    private static class JsonValueImpl implements JsonValue, Serializable {
        private final ValueType valueType;

        JsonValueImpl(ValueType valueType) {
            this.valueType = valueType;
        }

        public ValueType getValueType() {
            return this.valueType;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else {
                return obj instanceof JsonValue ? this.getValueType().equals(((JsonValue) obj).getValueType()) : false;
            }
        }

        public int hashCode() {
            return this.valueType.hashCode();
        }
    }
}