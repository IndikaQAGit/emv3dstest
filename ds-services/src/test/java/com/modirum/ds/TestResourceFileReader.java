package com.modirum.ds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.stream.Collectors;

public class TestResourceFileReader {

    private static final String IMPORT_MERCHANTS_PATH = "data/imports/merchant/";
    private static final String IMPORT_ISSUERS_PATH = "data/imports/issuers/";
    private static final String IMPORT_ISSUERS_CARD_RANGE_PATH = "data/imports/cardranges/";

    public static final String MERCHANTS_ADD                  = IMPORT_MERCHANTS_PATH + "merchants_add.csv";
    public static final String MERCHANTS_ADD_MISSING_ELEMENTS = IMPORT_MERCHANTS_PATH + "merchants_add_missing_element.csv";
    public static final String MERCHANTS_CHANGE_STATE         = IMPORT_MERCHANTS_PATH + "merchants_change_state.csv";

    public static final String ISSUERS_CHANGE_STATE         = IMPORT_ISSUERS_PATH + "issuers_change_state.csv";
    public static final String ISSUERS_ADD                  = IMPORT_ISSUERS_PATH + "issuers_add.csv";
    public static final String ISSUERS_ADD_MISSING_ELEMENTS = IMPORT_ISSUERS_PATH + "issuers_add_missing_element.csv";

    public static final String ISSUER_CARD_RANGE_ADD              = IMPORT_ISSUERS_CARD_RANGE_PATH + "issuers_cardranges_add.csv";
    public static final String ISSUER_CARD_RANGE_MISSING_ELEMENTS = IMPORT_ISSUERS_CARD_RANGE_PATH + "issuers_cardranges_add_missing_element.csv";
    public static final String ISSUER_CARD_RANGE_CHANGE_STATE     = IMPORT_ISSUERS_CARD_RANGE_PATH + "issuers_cardranges_change_state.csv";

    /**
     * Reads file from project test resources folder.
     */
    public static String readFileAsString(final String filename) throws Exception {
        URL url = ClassLoader.getSystemResource(filename);
        return readUrl(url);
    }

    private static String readUrl(final URL url) throws IOException {
        try (InputStream in = url.openStream()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }
}
