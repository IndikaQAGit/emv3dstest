if [ -z "$DS_VERSION" ]
then
  DS_VERSION=0.0.7;
fi
echo 'DS_VERSION='$DS_VERSION;
DS_VERSION=$DS_VERSION docker-compose up -d --force-recreate --no-deps --build;