package com.modirum.ds.mngr.support;

import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Writer;

/**
 * User: Jevgeni Shumik Date: 07.01.14 10:15
 */
public class FreemarkerExceptionHandler implements TemplateExceptionHandler {

    static Logger log = LoggerFactory.getLogger(FreemarkerExceptionHandler.class);

    public void handleTemplateException(TemplateException template, Environment env, Writer out) throws TemplateException {
        //template.get
        String errorId = "" + System.currentTimeMillis();
        try {
            String tn = env.getCurrentTemplate() != null ? env.getCurrentTemplate().getName() : "unknown";
            log.error("Error In template " + tn + ", id " + errorId, template);
            out.write("<span style=\"color: red;\">Template error, id " + errorId + "</span>");
        } catch (Exception e) {
            log.error("Error print template error " + errorId, e);
        }

    }
}
