package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.User;
import com.modirum.ds.db.model.UserSearchFilter;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.UserService;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.ObjectDiff;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private UserService userService = ServiceLocator.getInstance().getUserService();

    @ModelAttribute("factor2Authentications")
    public Map<String, String> getFactor2Authentications() {
        final String key = "secondFactorMap." + locale() + ".cached";
        @SuppressWarnings("unchecked")
        Map<String, String> map = (Map<String, String>) this.getContextAttribute(key);
        if (map == null) {
            map = new LinkedHashMap<>();
            map.put("", "");
            try {
                String sfoSet = cnfs.getStringSetting(DsSetting.MNGR_FACTOR2AUTHENTICATIONS.getKey());
                String[] sfo = Misc.split(sfoSet, ',');
                if (sfo != null) {
                    for (String sfox : sfo) {
                        map.put(sfox, localizationService.getText("factor2method." + sfox, locale()));
                    }
                }

                this.setContextAttribute(key, map);
            } catch (Exception e) {
                log.error("2nd factor options read error", e);
            }
        }

        return map.size() > 1 ? map : null;
    }


    @ModelAttribute("certificateAuthentications")
    public Boolean certificateAuthentications() {
        if ("true".equals(cnfs.getStringSetting(DsSetting.MNGR_SSL_AUTH_ENABLED.getKey()))) {
            return Boolean.TRUE;
        }

        return null;
    }

    @ModelAttribute("allroles")
    public Map<String, String> allRoles() {
        final String key = "allRolesMap_cached." + locale();
        @SuppressWarnings("unchecked")
        Map<String, String> allRoles = (Map<String, String>) this.getContextAttribute(key);
        if (allRoles != null && allRoles.size() > 0) {
            return allRoles;
        }

        allRoles = new LinkedHashMap<>();
        Field[] roleFields = Roles.class.getFields();

        for (int i = 0; i < roleFields.length; i++) {
            try {
                String role = (String) roleFields[i].get(null);
                allRoles.put(role, localizationService.getText("role." + role, locale()));
            } catch (Exception e) {
                log.error("Roles read error", e);
            }
        }
        this.setContextAttribute(key, allRoles);
        return allRoles;
    }

    @ModelAttribute("userOderMap")
    public Map<String, String> geUserOderMap() {
        final String key = "userOderMap." + locale() + ".cached";
        Map<String, String> userOderMap = (Map<String, String>) this.getContextAttribute(key);
        if (userOderMap != null && userOderMap.size() > 0) {
            return userOderMap;
        }
        userOderMap = new HashMap<>();
        userOderMap.put("", "");
        for (String ox : User.VALID__SEARCH_ORDERS) {
            userOderMap.put(ox, localizationService.getText("user.order." + ox, locale()));
        }
        this.setContextAttribute(key, userOderMap);
        return userOderMap;
    }

    @RequestMapping("/usersList.html")
    public ModelAndView usersList(HttpServletRequest request,
                                  @ModelAttribute(value = "filter") UserSearchFilter filter) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.userView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();

        ModelAndView mav = new ModelAndView("users/usersList");

        // if one merchant user then he can only manage his merchant
        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {
            // validate search form
            if (!paymentSystemDropDown().containsKey(filter.getPaymentSystemId().toString())) {
                filter.setPaymentSystemId(currentUser().getPaymentSystemId());
            }

            if (filter.getStart() == null) {
                filter.setStart(0);
            }
            if (filter.getLimit() == null) {
                filter.setLimit(25);
            }

            List<User> userList = userService.listUsers(filter);
            if (CollectionUtils.isNotEmpty(userList)) {
                Map<Integer, PaymentSystem> paymentSystems = paymentSystemsService.getPaymentSystems().stream()
                        .collect(Collectors.toMap(PaymentSystem::getId, paymentSystem -> paymentSystem));
                for (User user : userList) {
                    if (user.getPaymentSystemId() != null && paymentSystems.get(user.getPaymentSystemId()) != null) {
                        user.setPaymentSystemName(paymentSystems.get(user.getPaymentSystemId()).getName());
                    }
                }
            }

            mav.addObject("filter", filter);
            mav.addObject("userList", userList);

            if (log.isDebugEnabled()) {
                log.debug("Userlist done in {} ms", (System.currentTimeMillis() - start));
            }

            if ("true".equals(cnfs.getStringSetting(DsSetting.AUDIT_LOG_SEARCHES.getKey()))) {
                Map<String, Object> chMap = ObjectDiff.properties(filter, true);
                // remove unnecessary
                chMap.remove("factor2Passed");
                chMap.remove("forcedPasswordChange");
                chMap.remove("loginAttempts");
                String detail = "" + chMap;
                auditLogService.logAction(AuthorityUtil.getUser(), new User(), DSModel.AuditLog.Action.SEARCH, detail);
            }
        } else {
            if (Misc.isNullOrEmpty(filter.getLimit())) {
                filter.setLimit(25);
            }
            if (Misc.isNullOrEmpty(filter.getOrder())) {
                filter.setOrder("loginname");
                filter.setOrderDirection("asc");
            }
        }

        return mav;
    }

    @RequestMapping("/userView.html")
    public ModelAndView viewUser(@RequestParam("id") Long id, HttpServletRequest request) throws Exception {
        User user = persistenceService.getPersistableById(id, User.class);
        if (!AuthorityUtil.isAccessAllowed(user, Roles.userView)) {
            return unauthorized(request);
        }

        if (user != null && !isUserAuthorizedForAction(user)) {
            return super.redirect("../users/usersList.html");
        }

        ModelAndView mav = new ModelAndView("users/userView");
        mav.addObject("user", user);

        Map<Integer, PaymentSystem> paymentSystems = paymentSystemsService.getPaymentSystems().stream()
                .collect(Collectors.toMap(PaymentSystem::getId, paymentSystem -> paymentSystem));
        if (user.getPaymentSystemId() != null && paymentSystems.get(user.getPaymentSystemId()) != null) {
            user.setPaymentSystemName(paymentSystems.get(user.getPaymentSystemId()).getName());
        }

        mav.addObject("lastModified", auditLogService.getLastModidified(user));
        auditLogService.logAction(AuthorityUtil.getUser(), user, DSModel.AuditLog.Action.VIEW, "");

        return mav;
    }

    @RequestMapping("/userEdit.html")
    public ModelAndView editUser(@RequestParam(value = "id", required = false) String id,
                                 HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.userEdit)) {
            return unauthorized(request);
        }

        User user = null;
        if (id != null && Misc.isLong(id)) {
            user = persistenceService.getPersistableById(Misc.parseLong(id), User.class);
            if (!AuthorityUtil.isAccessAllowed(user)) {
                return unauthorized(request);
            }
        }

        if (user == null) {
            user = new User();
        }

        // if authorized for the user or for adding new one
        if (!isUserAuthorizedForAction(user)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("users/userEdit");
        mav.addObject("user", user);
        boolean isSelfEdit = currentUser().getId().equals(user.getId());
        mav.addObject("isSelfEdit", isSelfEdit);
        if (user != null && user.getId() != null) {
            auditLogService.logAction(AuthorityUtil.getUser(), user, DSModel.AuditLog.Action.VIEW, "");
        }

        return mav;
    }

    @RequestMapping(value = "/userDelete.html", method = RequestMethod.POST)
    public ModelAndView deleteUser(@RequestParam(value = "id", required = false) String id,
                                   HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.userEdit)) {
            return unauthorized(request);
        }

        User user;
        if (id != null && Misc.isLong(id)) {
            user = persistenceService.getPersistableById(Misc.parseLong(id), User.class);
            if (!AuthorityUtil.isAccessAllowed(user)) {
                return unauthorized(request);
            }

            if (user != null) {
                if (!isUserAuthorizedForAction(user)) {
                    return super.redirect("../users/usersList.html");
                } else {
                    persistenceService.delete(user);
                    auditLogService.logDelete(AuthorityUtil.getUser(), user, user.getLoginname() + " delted");
                }
            }
        }
        return super.redirect("usersList.html");
    }

    @RequestMapping(value = "/userUpdate.html", method = RequestMethod.POST)
    public ModelAndView updateUser(@RequestParam(value = "newTemplate", defaultValue = "false") boolean newTemplate,
                                   @RequestParam(value = "newUser", defaultValue = "false") boolean newUser,
                                   @Valid User user,
                                   BindingResult result,
                                   HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.isAccessAllowed(user, Roles.userEdit) || !isUserAuthorizedForAction(user)) {
            return unauthorized(request);
        }

        boolean isSelfEdit = currentUser().getId().equals(user.getId());
        int ENC_PASS_LENGTH = 64;
        String newPassword = user.getNewPassword();

        User loginCheck;
        User userFromDB = null;
        String emailpattern = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-z0-9-]*[a-zA-Z0-9])?\\.)+[a-z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
        if (!newUser) {
            if (user.getId() != null) {
                userFromDB = persistenceService.getPersistableById(user.getId(), User.class);
            }

            // set new user true because id is null
            if (userFromDB == null) {
                newUser = true;
            }
        }

        trimUserFields(user);

        if (Misc.isNullOrEmpty(user.getLoginname())) {
            result.addError(new FieldError("user", "loginname", user.getLoginname(), false, null, null,
                                           localizationService.getText("err.required", localizationService.getText(
                                                   "text.user.loginname", locale()), locale())));
        }

        if (Misc.isNullOrEmpty(user.getFirstName())) {
            result.addError(new FieldError("user", "firstName", user.getFirstName(), false, null, null,
                                           localizationService.getText("err.required", localizationService.getText(
                                                   "text.user.firstName", locale()), locale())));
        }

        if (Misc.isNullOrEmpty(user.getLastName())) {
            result.addError(new FieldError("user", "lastName", user.getLastName(), false, null, null,
                                           localizationService.getText("err.required",
                                                                       localizationService.getText("text.user.lastName",
                                                                                                   locale()),
                                                                       locale())));
        }

        if (Misc.isNotNullOrEmpty(user.getLoginname())) {
            loginCheck = userService.getUserByLoginname(user.getLoginname());
            if ((newTemplate || newUser) && loginCheck != null ||
                loginCheck != null && !loginCheck.getId().equals(user.getId())) {
                result.addError(new FieldError("user", "loginname", user.getLoginname(), false, null, null,
                                               localizationService.getText("err.user.with.this.loginname.exists",
                                                                           locale())));
            }
        }

        Pattern mail = Pattern.compile(emailpattern);
        if (user.getEmail() == null || !mail.matcher(user.getEmail().toLowerCase()).matches()) {
            result.addError(new FieldError("user", "email", user.getEmail(), false, null, null,
                                           localizationService.getText("err.email.invalid", locale())));
        }

        if (DSModel.User.Status.ACTIVE.equals(user.getStatus())) {
            user.setLockedDate(null);
            user.setLoginAttempts(0);
        }

        if (Misc.isNotNullOrEmpty(newPassword)) {
            Pattern digit = Pattern.compile("[\\d]+");
            Pattern alpha = Pattern.compile("[a-zA-Z]+");
            if (newPassword.length() < 7) {
                result.addError(new FieldError("user", "newPassword",
                                               localizationService.getText("err.password.security", locale())));
            } else if (digit.matcher(newPassword).matches() || alpha.matcher(newPassword).matches()) {
                result.addError(new FieldError("user", "newPassword",
                                               localizationService.getText("err.password.security.num.let", locale())));
            }

            // current password check
            if (!newUser) {
                if (checkPassword(newPassword, userFromDB.getPassword())) {
                    result.addError(new FieldError("user", "newPassword",
                                                   localizationService.getText("err.password.security.history",
                                                                               locale())));
                }
            }
            // old passwords check
            if (!newUser) {
                String oldPass = userFromDB.getOldpasswords();
                if (oldPass != null && !(oldPass.length() == 0)) {
                    for (int i = 0; i < oldPass.length() / ENC_PASS_LENGTH; i++) {
                        String pass = oldPass.substring(ENC_PASS_LENGTH * i, ENC_PASS_LENGTH * (i + 1));
                        if (checkPassword(newPassword, pass)) {
                            result.addError(new FieldError("user", "newPassword",
                                                           localizationService.getText("err.password.security.history",
                                                                                       locale())));
                            break;
                        }
                    }
                }
            }
        } // password checks

        // Validate access permissions to the selected payment system
        Integer inputPaymentSystemId = user.getPaymentSystemId();
        String inputPaymentSystemIdStr = null;
        if (inputPaymentSystemId == null) {
            result.addError(new FieldError("user", "paymentSystemId", getLocalizedMessage("err.required", "general.paymentSystem")));
        } else {
            inputPaymentSystemIdStr = inputPaymentSystemId.toString(); // Int will be string(group) later
            if (!paymentSystemUserMap().containsKey(inputPaymentSystemIdStr)) {
                result.addError(new FieldError("user", "paymentSystemId", getLocalizedMessage("err.invalid.value")));
            }
        }

        if (isSelfEdit) {
            // disallow editing own roles, fallback to original
            user.setRoles(currentUser().getRoles());
        } else {
            // for any edited user only allow roles, which the currentUser has
            Set<String> currentUserRoles = currentUser().getRoles();
            user.getRoles().removeIf(role -> !currentUserRoles.contains(role));

            if (userFromDB != null) {
                // for existing users - only modify mutable roles (belonging to the currentUser)
                Set<String> immutableRoles = userFromDB.getRoles().stream()
                        .filter(role -> !currentUserRoles.contains(role)).collect(Collectors.toSet());
                // put back any existing roles, which are not under the authority of the currentUser.
                user.getRoles().addAll(immutableRoles);
            }
        }

        if (!result.hasErrors()) {
            // no user input errors, processing valid input data.
            if (PaymentSystemConstants.UNLIMITED.toString().equals(inputPaymentSystemIdStr)) {
                user.setPaymentSystemId(null); // superuser is defined by null psID
            }

            if (newTemplate || newUser) {
                user.setId(null);
                if (newTemplate) {
                    user.setStatus(DSModel.User.Status.TEMPLATE);
                } else {
                    user.setStatus(DSModel.User.Status.ACTIVE);
                }

                user.setPassword(encryptPassword(user.getNewPassword()));
                user.setLastPassChangeDate(null); // force password change on first logon
            } else if (Misc.isNotNullOrEmpty(newPassword) &&
                       userFromDB != null) { // if password was changed save it into old passwords
                String old = "";
                if (userFromDB.getOldpasswords() != null) {
                    old = userFromDB.getOldpasswords().substring(0, userFromDB.getOldpasswords().length() <
                                                                    ENC_PASS_LENGTH *
                                                                    4 ? userFromDB.getOldpasswords().length() :
                            ENC_PASS_LENGTH * 4);
                    user.setOldpasswords(userFromDB.getPassword() + old.substring(0, (old.length() < ENC_PASS_LENGTH *
                                                                                                     3 ? userFromDB.getOldpasswords().length() :
                            ENC_PASS_LENGTH * 3)));
                } else {
                    user.setOldpasswords(userFromDB.getPassword());
                }
                user.setPassword(encryptPassword(newPassword));
                user.setLastPassChangeDate(null); // force password user change after admin reset
                // cut old passwords to fit into table
            } else if (!(newTemplate || newUser) && userFromDB != null) {
                user.setLastPassChangeDate(userFromDB.getLastPassChangeDate());
                user.setOldpasswords(userFromDB.getOldpasswords());
                // if no new provided use the old one
                if (Misc.isNullOrEmpty(newPassword)) {
                    user.setPassword(userFromDB.getPassword());
                }
            }

            Long oldId = user.getId();
            persistenceService.saveOrUpdate(user);

            if (oldId == null) {
                auditLogService.logInsert(AuthorityUtil.getUser(), user,
                                          user.getLoginname() + " created, roles: " + user.getRoles());
            } else {
                List<ObjectDiff> odf = ObjectDiff.diff(userFromDB, user);
                ObjectDiff.removeDiffByProperty(odf, "oldpasswords");
                ObjectDiff.removeDiffByProperty(odf, "newPassword");
                ObjectDiff.removeDiffByProperty(odf, "newPasswordRepeat");
                Set<String> mask = new java.util.HashSet<String>();
                mask.add("password");

                String changes = ObjectDiff.createSimpleDiffString(odf, mask);
                auditLogService.logUpdate(AuthorityUtil.getUser(), user, user.getLoginname() + " " + changes);
            }

            return super.redirect("userView.html?id=" + user.getId());
        } // not errors

        ModelAndView mav = new ModelAndView("users/userEdit");
        mav.addObject("user", user);
        mav.addObject("isSelfEdit", isSelfEdit);
        return mav;
    }

    @RequestMapping(value = "/userActivate.html", method = RequestMethod.POST)
    public ModelAndView activateUser(@RequestParam String id, HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.userEdit)) {
            return unauthorized(request);
        }

        if (id != null && Misc.isLong(id)) {
            User user = persistenceService.getPersistableById(Misc.parseLong(id), User.class);
            if (!AuthorityUtil.isAccessAllowed(user) || !isUserAuthorizedForAction(user)) {
                return unauthorized(request);
            }

            if (user != null) {
                user.setStatus(DSModel.User.Status.ACTIVE);
                persistenceService.saveOrUpdate(user);
                auditLogService.logUpdate(AuthorityUtil.getUser(), user,
                                          user.getLoginname() + " status to " + DSModel.User.Status.ACTIVE);
            }
        }
        return super.redirect("userView.html?id=" + id);
    }

    @RequestMapping(value = "/userDeactivate.html", method = RequestMethod.POST)
    public ModelAndView deactivateUser(@RequestParam String id, HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole("userEdit")) {
            return unauthorized(request);
        }

        if (id != null && Misc.isLong(id)) {
            User user = persistenceService.getPersistableById(Misc.parseLong(id), User.class);
            if (!AuthorityUtil.isAccessAllowed(user) || !isUserAuthorizedForAction(user)) {
                return super.redirect("../users/usersList.html");
            }

            if (user != null) {
                user.setStatus(DSModel.User.Status.DISABLED);
                persistenceService.saveOrUpdate(user);
                auditLogService.logUpdate(AuthorityUtil.getUser(), user,
                                          user.getLoginname() + " status to " + user.getStatus());
            }
        }
        return super.redirect("userView.html?id=" + id);
    }

    private boolean isUserAuthorizedForAction(User user) {

        return true;
    }

    /**
     * Returns a set of user-accessible payment systems based on user access restrictions.
     * Is only used for creating/editing user profiles (not for search filters)
     */
    @ModelAttribute("paymentSystemUserMap")
    protected Map<String, String> paymentSystemUserMap() {
        if (currentUser() == null) { // no session
            return null;
        }

        Map<String, String> map = new LinkedHashMap<>();
        if (currentUser().getPaymentSystemId() == null) { // current user is a superuser
            map.put(PaymentSystemConstants.UNLIMITED.toString(), getLocalizedMessage("general.unlimited"));
        }

        List<PaymentSystem> paymentSystems = paymentSystemsService.getPaymentSystems(currentUser());
        Collections.sort(paymentSystems);
        for (PaymentSystem paymentSystem : paymentSystems) {
            map.put(String.valueOf(paymentSystem.getId()), paymentSystem.getName());
        }

        return map;
    }
    
    /**
     * Returns payment systems that can be used for filter adding All in the map for superUser
     */
    @ModelAttribute("paymentSystemDropDown")
    protected Map<String, String> paymentSystemDropDown() {
        Map<String, String> paymentSystemDropDownMap = new LinkedHashMap<>();
        if (currentUser().getPaymentSystemId() == null) {
            paymentSystemDropDownMap.put(PaymentSystemConstants.ALL.toString(), getLocalizedMessage(
                    "general.all"));
        }
        paymentSystemDropDownMap.putAll(paymentSystemUserMap());
        return paymentSystemDropDownMap;
    }

    private boolean checkPassword(String pplain, String phash) {
        return CryptoService.sha256WithUniquePredictable(pplain).equals(phash);
    }

    private String encryptPassword(String pplain) {
        return CryptoService.sha256WithUniquePredictable(pplain);
    }

    private void trimUserFields(User user) {
        user.setLoginname(StringUtils.trim(user.getLoginname()));
        user.setFirstName(StringUtils.trim(user.getFirstName()));
        user.setLastName(StringUtils.trim(user.getLastName()));
        user.setEmail(StringUtils.trim(user.getEmail()));
        user.setPhone(StringUtils.trim(user.getPhone()));
    }
}
