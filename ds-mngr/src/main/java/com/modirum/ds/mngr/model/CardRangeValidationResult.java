package com.modirum.ds.mngr.model;

import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

public class CardRangeValidationResult implements Serializable {

    private List<Error> errors = new ArrayList<>();
    private Integer index;

    public void addError(Integer index, String fieldName, String errorText) {
        this.index = index;
        Error error = new Error();
        error.setField(fieldName);
        error.setErrorText(errorText);
        errors.add(error);
    }

    public static class Error {
        private String field;
        private String errorText;

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getErrorText() {
            return errorText;
        }

        public void setErrorText(String errorText) {
            this.errorText = errorText;
        }
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
