package com.modirum.ds.mngr.model.ui.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DS-Admin Merchant profile search-list filter. Filled from search panel, used to lookup Merchants from DB.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MerchantSearchFilter {
    private String name;
    private String requestorID;
    private String acquirerMerchantID;
    private Integer acquirerId;
    private Integer paymentSystemId;
    private String country;
    private String status;
    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
}
