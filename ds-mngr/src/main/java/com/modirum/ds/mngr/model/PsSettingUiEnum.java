package com.modirum.ds.mngr.model;

import com.modirum.ds.enums.PsSetting;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * UI representation enum of the Payment System settings. From the UI perspective settings are scoped to
 * ADMIN - accessible to DS system administrators only, and USER - accessible to Payment System specific end users.
 */
public enum PsSettingUiEnum {
    ADMIN(PsSetting.DS_URL),
    USER(PsSetting.ACCEPTED_SDK_LIST),
    ELO_PAPI(PsSetting.ELO_PAPI_ENABLED,
             PsSetting.ELO_PAPI_AUTH_URL,
             PsSetting.ELO_PAPI_DETOKEN_URL,
             PsSetting.ELO_PAPI_DETOKEN_CLIENT_ID,
             PsSetting.ELO_PAPI_DETOKEN_CLIENT_SECRET,
             PsSetting.ELO_PAPI_READ_TIMEOUT_MILLIS,
             PsSetting.ELO_PAPI_CONNECT_TIMEOUT_MILLIS,
             PsSetting.ELO_PAPI_SKIP_TLS_HOSTNAME_VERIFY,
             PsSetting.ELO_PAPI_TLS_CLIENT_CERT_ID,
             PsSetting.ELO_PAPI_USERNAME,
             PsSetting.ELO_PAPI_PASSWORD);

    private List<PsSetting> psSettings;
    private Set<String> keys;

    PsSettingUiEnum(PsSetting... psSettings) {
        this.psSettings = new ArrayList<>();
        keys = new HashSet<>();

        for(PsSetting psSetting: psSettings){
            addPsSetting(psSetting);
        }
    }

    private void addPsSetting(PsSetting psSetting) {
        psSettings.add(psSetting);
        keys.add(psSetting.getKey());
    }

    public List<PsSetting> getPsSettings() {
        return psSettings;
    }

    public static PsSettingUiEnum getType(String key) {
        if (ADMIN.keys.contains(key)) {
            return ADMIN;
        } else if(USER.keys.contains(key)) {
            return USER;
        } else if(ELO_PAPI.keys.contains(key)) {
            return ELO_PAPI;
        }
        throw new IllegalArgumentException(String.format("%s is not a valid PsSetting key", key));
    }
}
