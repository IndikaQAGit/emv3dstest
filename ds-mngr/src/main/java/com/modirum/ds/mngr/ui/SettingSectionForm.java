package com.modirum.ds.mngr.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SettingSectionForm implements Serializable {
    private Set<String> shownSections = new HashSet<>();
    private final Map<String, String> settings = new HashMap<>();

    public void addSettingsMap(Map<String, String> formDataMap) {
        formDataMap.entrySet().forEach(entry -> {
            if (entry.getKey().startsWith("settings.")) {
                this.settings.put(entry.getKey().replaceFirst("settings\\.", ""), entry.getValue());
            }
        });
    }

    public Set<String> getShownSections() {
        return shownSections;
    }

    public SettingSectionForm setShownSections(Set<String> shownSections) {
        this.shownSections = shownSections;
        return this;
    }

    public Map<String, String> getSettings() {
        return settings;
    }
}
