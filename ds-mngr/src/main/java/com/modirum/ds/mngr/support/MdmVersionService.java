package com.modirum.ds.mngr.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.io.InputStream;
import java.util.jar.Manifest;

/**
 * This service reads the product information from manifest file.
 */
public class MdmVersionService {

    private static final Logger log = LoggerFactory.getLogger(MdmVersionService.class);

    private static final long lastCheck = 0;
    private static final long OneYear = 365L * 24L * 3600L * 1000L;
    private static final long ThreeYears = 3 * OneYear;
    private static final long OneHour = 3600L * 1000L;
    private static final long OneDay = 24L * OneHour;
    private static String buildVersion;
    private static long buildDate = 0;
    private static String product = ApplicationControl.getAppName();
    private static String vendor = "Modirum Ltd.";
    private static Manifest mf = null;

    public static final void initManifest(ServletContext servletContext) {
        try (InputStream manifestInputStream = servletContext.getResourceAsStream("/META-INF/MANIFEST.MF")){
            Manifest manifest = new Manifest();
            manifest.read(manifestInputStream);
            mf = manifest;
        } catch (Exception e) {
            log.error("Error reading manifest info ", e);
        }
    }

    public static final String getVendor() {
        return vendor;
    }

    public static final java.util.Date getBuildDate() {
        if (buildDate > 0) {
            return new java.util.Date(buildDate);
        }
        return null;
    }

    public static final String getBuildVersion() {
        if (buildDate > 0) {
            // if build is younger than one year dont go to print..
            if (buildDate > System.currentTimeMillis() - OneYear) {
                return buildVersion;
            }

            // go to print every hour in case build is between 1 and 3 years old..
            if (buildDate > System.currentTimeMillis() - ThreeYears &&
                lastCheck > System.currentTimeMillis() - OneHour) {
                return buildVersion;
            }
        }

        String bd = null;
        if (mf != null) {
            String pro = mf.getMainAttributes().getValue("Implementation-Title");
            if (pro != null && pro.length() > 0) {
                product = pro;
            }
            String ven = mf.getMainAttributes().getValue("Implementation-Vendor");
            if (ven != null && ven.length() > 0) {
                vendor = ven;
            }
            String bv = mf.getMainAttributes().getValue("Implementation-Version");
            if (bv != null && bv.length() > 0) {
                buildVersion = bv;
            }

            bd = mf.getMainAttributes().getValue("Built-Date");
        }

        try {
            buildDate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse(bd != null ? bd : "2016-12-20 12:00:00")
                    .getTime();
        } catch (Exception dc) {
            buildDate = 1355998647569L;
        }

        log.info("Product: {} build version {} build date: {}", product, buildVersion, new java.util.Date(buildDate));

        if (buildDate > System.currentTimeMillis() - OneYear && buildDate < System.currentTimeMillis() - 60 * OneDay) {
            log.info("Current {} build version less than one year old, so may be relatively safe to use, " +
                     "but allways check for updates from {}",
                     product, vendor);
        } else if (buildDate > System.currentTimeMillis() - ThreeYears &&
                   buildDate < System.currentTimeMillis() - OneYear) {
            log.warn("Current {} build {} from {} is older than one year, please contact, {} for updates!",
                     product, buildVersion, new java.util.Date(buildDate), vendor);
        } else if (buildDate <= System.currentTimeMillis() - ThreeYears) {
            log.warn("WARNING: Current {} build {} from {} is outdated" +
                     " and should not be used to process production transactions.",
                     product, buildVersion, new java.util.Date(buildDate));
            log.warn("Please contact {} obtain updates and install newer version" +
                     " to make this installation PCI DSS compliant!", vendor);
        }

        return buildVersion;
    }

}
