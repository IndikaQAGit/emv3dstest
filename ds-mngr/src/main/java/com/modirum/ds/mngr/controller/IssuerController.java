package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.imports.AbstractBatchHandlerFacade;
import com.modirum.ds.imports.DataBatchHandlerConfiguration;
import com.modirum.ds.imports.DataBatchResultStatus;
import com.modirum.ds.imports.cardrange.IssuerCardRangeBatchHandlerFacade;
import com.modirum.ds.imports.cardrange.parser.BatchCardRangeModel;
import com.modirum.ds.imports.issuer.IssuerBatchHandlerFacade;
import com.modirum.ds.imports.issuer.parser.BatchIssuerModel;
import com.modirum.ds.mngr.model.CardRangeValidationResult;
import com.modirum.ds.mngr.service.CardRangeUIHelper;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.support.Helpers;
import com.modirum.ds.db.model.ACSProfile;
import com.modirum.ds.db.model.CardRange;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.Issuer;
import com.modirum.ds.model.IssuerSearcher;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.ObjectDiff;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Controller
@RequestMapping("/issuers")
public class IssuerController extends BaseController {
    private final transient Logger log = LoggerFactory.getLogger(IssuerController.class);

    IssuerService issuerService = ServiceLocator.getInstance().getIssuerService();

    @Autowired
    private CardRangeUIHelper cardRangeUIHelper;

    public static class Lists {
        private List<CardRange> list1 = new AutoPopulatingList<CardRange>(CardRange.class);
        private List<ACSProfile> list2 = new AutoPopulatingList<ACSProfile>(ACSProfile.class);

        public Lists() {
        }

        public List<CardRange> getList1() {
            if (list1 == null) {
                list1 = new ArrayList<>();
            }

            return list1;
        }

        public void setList1(List<CardRange> list) {
            this.list1 = list;
        }

        public List<ACSProfile> getList2() {
            if (list2 == null) {
                list2 = new ArrayList<>();
            }

            return list2;
        }

        public void setList2(List<ACSProfile> list) {
            this.list2 = list;
        }
    }

    @ModelAttribute("countryMapA2")
    public Map<String, String> countryMap() {
        return super.countriesMapA2();
    }

    @ModelAttribute("issuerStatuses")
    public Map<String, String> issuerStatusesMap() {
        return statusMap(DSModel.Issuer.Status.class.getName());
    }

    @ModelAttribute("protoVersionList")
    public Map<String, String> protoVersionMap() {
        return protocolVersionsMap(false);
    }

    @ModelAttribute("maxBINLength")
    public Integer maxBINLength() {
        return issuerService.getMaxBINLength();
    }

    @ModelAttribute("rangeMode")
    public Boolean isBINRangeMode() {
        return issuerService.isBINRangeMode();
    }

    @RequestMapping("/issuersBatch.html")
    public ModelAndView issuersBatch(@ModelAttribute(value = "data") IssuerImportRequestData data, HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView("issuers/issuersBatch");
        mav.addObject("data", data);
        if (request.getParameter("actionButton") != null) {
            String actionButton = request.getParameter("actionButton");
            if (!data.getUploadFile().isEmpty() || data.getUploadData() != null) {
                if (!data.getUploadFile().isEmpty()) {
                    // Return selected file to a user so he can use it again without the need to open file
                    data.setUploadData(Base64.encode(data.getUploadFile().getBytes(), 0));
                    data.setUploadFileName(data.getUploadFile().getOriginalFilename());
                }
                String fileContent = new String(!data.getUploadFile().isEmpty()
                        ? data.getUploadFile().getBytes() : Base64.decode(data.getUploadData()));
                DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(currentUser());
                configuration.setContinueOnError(true);
                AbstractBatchHandlerFacade<BatchIssuerModel> importer = new IssuerBatchHandlerFacade(ServiceLocator.getInstance().getPersistenceService(), configuration);
                DataBatchResultStatus results = null;
                switch (actionButton) {
                    case "Upload":
                        if (!AuthorityUtil.hasRole(Roles.issuerEdit)) {
                            return unauthorized(request);
                        }
                        results = importer.validateAndProcess(fileContent);
                        break;
                    case "Validate":
                        results = importer.validate(fileContent);
                        break;
                    default:
                        mav.addObject("error", "Not supported action");
                }
                mav.addObject("results", results);
            }
        }

        return mav;
    }

    @RequestMapping("/issuerCardRangesBatch.html")
    public ModelAndView issuerCardRangesBatch(@ModelAttribute(value = "data") IssuerImportRequestData data, HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView("issuers/issuerCardRangesBatch");
        mav.addObject("data", data);
        if (request.getParameter("actionButton") != null) {
            String actionButton = request.getParameter("actionButton");
            if (!data.getUploadFile().isEmpty() || data.getUploadData() != null) {
                if (!data.getUploadFile().isEmpty()) {
                    // Return selected file to a user so he can use it again without the need to open file
                    data.setUploadData(Base64.encode(data.getUploadFile().getBytes(), 0));
                    data.setUploadFileName(data.getUploadFile().getOriginalFilename());
                }
                String fileContent = new String(!data.getUploadFile().isEmpty()
                        ? data.getUploadFile().getBytes() : Base64.decode(data.getUploadData()));
                DataBatchHandlerConfiguration configuration = new DataBatchHandlerConfiguration(currentUser());
                configuration.setContinueOnError(true);
                AbstractBatchHandlerFacade<BatchCardRangeModel> importer = new IssuerCardRangeBatchHandlerFacade(ServiceLocator.getInstance().getPersistenceService(), configuration);
                DataBatchResultStatus results = null;
                switch (actionButton) {
                    case "Upload":
                        if (!AuthorityUtil.hasRole(Roles.issuerEdit)) {
                            return unauthorized(request);
                        }
                        results = importer.validateAndProcess(fileContent);
                        break;
                    case "Validate":
                        results = importer.validate(fileContent);
                        break;
                    default:
                        mav.addObject("error", "Not supported action");
                }
                mav.addObject("results", results);
            }
        }

        return mav;
    }

    @RequestMapping("/issuerList.html")
    public ModelAndView issuersList(HttpServletRequest request,
                                    @ModelAttribute(value = "searcher") IssuerSearcher searcher) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.issuerView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();

        ModelAndView mav = new ModelAndView("issuers/issuerList");
        mav.addObject("binLength", getBinLength());
        if (Misc.isNotNullOrEmpty(request.getParameter("search"))) {
            // validate search form
            if (!paymentSystemSearchMap().containsKey(searcher.getPaymentSystemId().toString())) {
                searcher.setPaymentSystemId(currentUser().getPaymentSystemId());
            }

            if (searcher.getStart() == null) {
                searcher.setStart(0);
            }
            if (searcher.getLimit() == null) {
                searcher.setLimit(25);
            }
            if (PaymentSystemConstants.UNLIMITED.equals(searcher.getPaymentSystemId())) {
                searcher.setPaymentSystemId(null);
            }

            List<Issuer> found = issuerService.getIssuersByQuery(searcher);
            mav.addObject("foundIssuers", found);
            mav.addObject("searcher", searcher);

            if (log.isDebugEnabled()) {
                log.debug("Issuerlist done in " + (System.currentTimeMillis() - start) + " ms");
            }

            if ("true".equals(cnfs.getStringSetting(DsSetting.AUDIT_LOG_SEARCHES.getKey()))) {
                String detail = "" + ObjectDiff.properties(searcher, true);
                auditLogService.logAction(AuthorityUtil.getUser(), new Issuer(), DSModel.AuditLog.Action.SEARCH,
                                          detail);
            }
        } else {
            if (Misc.isNullOrEmpty(searcher.getLimit())) {
                searcher.setLimit(25);
            }
            if (Misc.isNullOrEmpty(searcher.getOrder())) {
                searcher.setOrder("id");
                searcher.setOrderDirection("desc");
            }
        }

        return mav;
    }

    protected List<Short> createSets(List<CardRange> crl, List<ACSProfile> apl) {
        java.util.List<Short> sets = new java.util.ArrayList<Short>();
        if (crl != null) {
            for (CardRange crr : crl) {
                if (issuerService.isBINRangeMode()) {
                    crr.setRangeMode();
                }

                if (crr.getSet() != null && !sets.contains(crr.getSet())) {
                    sets.add(crr.getSet());
                } else if (crr.getSet() == null) {
                    crr.setSet(DSModel.DEFAULT_SET);
                }
            }
        }
        if (apl != null) {
            for (ACSProfile acp : apl) {
                if (acp.getSet() != null && !sets.contains(acp.getSet())) {
                    sets.add(acp.getSet());
                } else if (acp.getSet() == null) {
                    acp.setSet(DSModel.DEFAULT_SET);
                }
            }
        }

        if (sets.size() < 1) {
            sets.add(DSModel.DEFAULT_SET);
        }

        return sets;
    }

    @RequestMapping("/issuerView.html")
    public ModelAndView viewIssuer(@RequestParam("id") Integer id, HttpServletRequest request) throws Exception {
        Issuer issuer = issuerService.getIssuerById(id);

        if (!AuthorityUtil.isAccessAllowed(issuer, Roles.issuerView)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("issuers/issuerView");
        modelAndView.addObject("issuer", issuer);
        if (issuer != null) {
            List<CardRange> cardRanges = issuerService.getIssuerCardBins(issuer.getId());
            List<ACSProfile> apl = issuerService.getIssuerACSList(issuer.getId(), null, null, false);
            modelAndView.addObject("cardbins", cardRanges);
            modelAndView.addObject("acsprofiles", apl);
            modelAndView.addObject("sets", createSets(cardRanges, apl));
            modelAndView.addObject("cardRangeStatuses", statusMap(DSModel.CardRange.Status.class.getName()));
            modelAndView.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
            modelAndView.addObject("inCertificateList", inCertificateMap());
            modelAndView.addObject("outCertificateList", outCertificateMap());
            modelAndView.addObject("attemptsACSMap", attemptsACSMap(issuer.getPaymentSystemId()));
            if (Misc.isNotNullOrEmpty(issuer.getPaymentSystemId())) {
                PaymentSystem paymentSystem = persistenceService.getPersistableById(issuer.getPaymentSystemId(), PaymentSystem.class);
                if (paymentSystem != null) {
                    modelAndView.addObject("paymentSystemName", paymentSystem.getName());
                }
            }
            this.auditLogService.logAction(AuthorityUtil.getUser(), issuer, DSModel.AuditLog.Action.VIEW,
                                           "View issuer " + issuer.getId());
        }

        return modelAndView;
    }

    @RequestMapping("/issuerEdit.html")
    public ModelAndView editIssuer(@RequestParam(value = "id", required = false) String id,
                                   @ModelAttribute("cardRangeErrors") CardRangeValidationResult errors, @ModelAttribute("newCardRange") CardRange newCardRange, HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerEdit)) {
            return unauthorized(request);
        }

        Issuer issuer = null;
        if (id != null && Misc.isLong(id)) {
            issuer = issuerService.getIssuerById(Misc.parseInt(id));

            if (!AuthorityUtil.isAccessAllowed(issuer)) {
                return unauthorized(request);
            }
        }

        if (issuer == null) {
            issuer = new Issuer();
        }

        ModelAndView mav = new ModelAndView("issuers/issuerEdit");
        mav.addObject("issuer", issuer);

        Lists lists = new Lists();
        List<CardRange> cardRangeList = null;
        List<ACSProfile> acsProfileList = null;
        if (issuer != null && issuer.getId() != null) {
            cardRangeList = issuerService.getIssuerCardBins(issuer.getId());
            if (newCardRange != null && newCardRange.getSet() != null) {
                newCardRange.setId(null);
                newCardRange.setIssuerId(Misc.parseInt(id));
                mav.addObject("newCardRangeSet", newCardRange.getSet());

                cardRangeList.add(newCardRange);
            }
            lists.setList1(cardRangeList);
            acsProfileList = issuerService.getIssuerACSList(issuer.getId(), null, null, false);
            lists.setList2(acsProfileList);
            this.auditLogService.logAction(AuthorityUtil.getUser(), issuer, DSModel.AuditLog.Action.VIEW,
                    "View issuer " + issuer.getId());
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(issuer.getPaymentSystemId())));
        }
        mav.addObject("sets", createSets(cardRangeList, acsProfileList));
        mav.addObject("cardRangeStatuses", statusMap(DSModel.CardRange.Status.class.getName()));
        mav.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        mav.addObject("inCertificateList", inCertificateMap());
        mav.addObject("outCertificateList", outCertificateMap());
        mav.addObject("attemptsACSMap", attemptsACSMap(issuer.getPaymentSystemId()));
        mav.addObject("cardRangeErrors", errors);
        mav.addObject("lists", lists);
        mav.addObject("deleteConfirmMsg", localizationService.getText("text.wrn.are.you.sure.delete", localizationService.getText("text.binrange", locale()), locale()));
        return mav;
    }

    @RequestMapping(value = "/cardRangeUpdate.html", method = RequestMethod.POST)
    public String updateCardRange(@ModelAttribute("lists") Lists lists, HttpServletRequest request, BindingResult result, final RedirectAttributes redirectAttributes) throws Exception {
        Integer index = Misc.parseInt(request.getParameter("index"));
        CardRange cardRangeFromUI = lists.list1.get(index);
        CardRange newCardRange = null;
        String issuerId = request.getParameter("id");

        if (request.getParameter("Save") != null) {
            cardRangeUIHelper.saveOrUpdateCardRange(cardRangeFromUI, issuerId, result, request, locale());
            if (result.hasErrors() && cardRangeFromUI.getId() == null) {
                newCardRange = cardRangeFromUI;
            }
        } else if (request.getParameter("Delete") != null) {
            cardRangeUIHelper.deleteCardRange(cardRangeFromUI, Misc.parseInt(request.getParameter("id")));
        } else if (request.getParameter("addNewBIN") != null) {
            newCardRange = new CardRange();
            if (issuerService.isBINRangeMode()) {
                newCardRange.setRangeMode();
            }
        }
        if (newCardRange != null) {
            newCardRange.setSet((short) Misc.parseInt(request.getParameter("setId")));
            redirectAttributes.addFlashAttribute("newCardRange", newCardRange);
        }
        if (result.hasErrors()) {
            CardRangeValidationResult cardRangeError = new CardRangeValidationResult();
            for (ObjectError entry : result.getAllErrors()) {
                //TODO should be refactored
                Field field = entry.getClass().getDeclaredField("field");
                field.setAccessible(true);
                String fieldName = (String) field.get(entry);
                String message = entry.getDefaultMessage();
                cardRangeError.addError(index, fieldName, message);
            }
            redirectAttributes.addFlashAttribute("cardRangeErrors", cardRangeError);
        }

        return "redirect:/issuers/issuerEdit.html?id=" + request.getParameter("id");
    }

    @RequestMapping(value = "/issuerUpdate.html", method = RequestMethod.POST)
    public ModelAndView updateIssuer(@RequestParam(value = "newIssuer", defaultValue = "false") boolean newIssuer,
                                     @ModelAttribute("issuer") Issuer issuer,
                                     BindingResult resultIss,
                                     @ModelAttribute("lists") Lists lists,
                                     BindingResult result,
                                     HttpServletRequest request) throws Exception {

        if (!AuthorityUtil.isAccessAllowed(issuer, Roles.issuerEdit)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("issuers/issuerEdit");
        mav.addObject("issuer", issuer);
        mav.addObject("lists", lists);
        mav.addObject("cardRangeStatuses", statusMap(DSModel.CardRange.Status.class.getName()));
        mav.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        mav.addObject("inCertificateList", inCertificateMap());
        mav.addObject("outCertificateList", outCertificateMap());
        mav.addObject("attemptsACSMap", attemptsACSMap(issuer.getPaymentSystemId()));
        mav.addObject("deleteConfirmMsg", localizationService.getText("text.wrn.are.you.sure.delete", localizationService.getText("text.binrange", locale()), locale()));
        if (issuer != null && issuer.getPaymentSystemId() != null) {
            mav.addObject("paymentSystemName", paymentSystemComponentMap().get(String.valueOf(issuer
                    .getPaymentSystemId())));
        }
        List<CardRange> cardRanges = issuerService.getIssuerCardBins(issuer.getId());
        if(cardRanges == null) {
            cardRanges = new ArrayList<>();
        }
        lists.getList1().addAll(cardRanges);

        boolean addCommand = false;

        String addNewACPSet = getParameterSetValueStartsWith("addNewACSP", request);
        if (Misc.isNotNullOrEmpty(addNewACPSet) && Misc.isInt(addNewACPSet)) {
            if (issuer != null && lists != null) {
                ACSProfile ap = new ACSProfile();
                ap.setSet((short) Misc.parseInt(addNewACPSet));
                lists.getList2().add(ap);
                addCommand = true;
            }
        }

        List<Short> setList = createSets(lists.getList1(), lists.getList2());
        mav.addObject("sets", setList);

        if (Misc.isNotNullOrEmpty(request.getParameter("addNewSet"))) {
            addCommand = true;
            if (issuer != null && setList != null) {
                for (int n = DSModel.DEFAULT_SET; n < 100; n++) {
                    if (!setList.contains((short) n)) {
                        setList.add((short) n);
                        break;
                    }
                }
            }
        }

        if (addCommand) {
            return mav;
        }

        issuerService.prepareIssuer(issuer);

        if (Misc.isNullOrEmpty(issuer.getName())) {
            resultIss.addError(new FieldError("issuer", "name", issuer.getName(), false, null, null,
                                              getLocalizedMessage("err.required", getLocalizedMessage("text.name"))));
        } else if (issuer.getPaymentSystemId() != null && issuerService.isIssuerExists(issuer.getName(),
                issuer.getPaymentSystemId(), issuer.getId())) {
            resultIss.addError(new FieldError("issuer", "name", issuer.getName(), false, null, null,
                    getLocalizedMessage("err.duplicate.value")));
        }  

        if (Misc.isNullOrEmpty(issuer.getStatus())) {
            resultIss.addError(new FieldError("issuer", "status", issuer.getStatus(), false, null, null,
                                              localizationService.getText("err.required",
                                                                          localizationService.getText("text.status",
                                                                                                      locale()),
                                                                          locale())));
        }

        if (Misc.isNullOrEmpty(issuer.getBIN())) {
            resultIss.addError(new FieldError("issuer", "BIN", issuer.getBIN(), false, null, null,
                                              localizationService.getText("err.required", "BIN", locale())));
        } else if (!Misc.isNumber(issuer.getBIN())) {
            resultIss.addError(new FieldError("issuer", "BIN", issuer.getBIN(), false, null, null,
                                              localizationService.getText("err.invalid", localizationService.getText(
                                                      "err.must.be.number", "BIN", locale()), locale())));
        } else if (issuer.getPaymentSystemId() != null &&
                   issuerService.isExistingDuplicateIssuersBin(issuer.getId(), issuer.getBIN(), issuer.getPaymentSystemId())) {
            resultIss.addError(new FieldError("issuer", "BIN", issuer.getBIN(), false, null, null,
                                              getLocalizedMessage("err.duplicate.value")));
        }

        // validate payment system
        String userInputPaymentSystemId = issuer.getPaymentSystemId().toString();
        if (!paymentSystemComponentMap().containsKey(userInputPaymentSystemId)) {
            result.addError(new FieldError("issuer", "paymentSystemId", getLocalizedMessage("err.invalid.value")));
        }

        // validate bins

        List<ACSProfile> okAcsProfiles = new ArrayList<>();
        List<ACSProfile> deleteAcsProfiles = new ArrayList<>();
        for (int i = 0; lists != null && i < lists.getList2().size(); i++) {
            ACSProfile acsProfile = lists.getList2().get(i);
            if (DSModel.ACSProfile.Status.ENDED.equals(acsProfile.getStatus())) {
                continue;
            }

            if ("true".equals(request.getParameter("deleteacp" + acsProfile.getId()))) {
                deleteAcsProfiles.add(acsProfile);
            } else {
                if (Misc.isNullOrEmpty(acsProfile.getName()) && Misc.isNullOrEmpty(acsProfile.getURL())) {
					/*	if (cbx.getId() != null)
						{
							delCardBins.add(cbx);
						}
					*/
                } else {
                    
                    int binerrs = 0;
                    
                    if (Misc.isNullOrEmpty(acsProfile.getName())) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "name", acsProfile.getName(), false, null, null,
                                               localizationService.getText("err.required", "Name", locale())));

                        binerrs++;
                    }

                    if (Misc.isNullOrEmpty(acsProfile.getURL())) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "URL", acsProfile.getURL(), false, null, null,
                                                       localizationService.getText("err.required",
                                                                                   localizationService.getText(
                                                                                           "text.acsprofile.url",
                                                                                           locale()), locale())));

                        binerrs++;
                    } else if (acsProfile.getURL().length() > 1024) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "URL", acsProfile.getURL(), false, null, null,
                                                       localizationService.getText("err.invalid.length",
                                                                                   localizationService.getText(
                                                                                           "err.must.be.length",
                                                                                           "<=1024", locale()),
                                                                                   locale())));
                        binerrs++;
                    } 

                    if (!sslCertsManagedExternally() && Misc.isNullOrEmpty(acsProfile.getInClientCert())) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "inClientCert", acsProfile.getOutClientCert(), false,
                                               null, null, localizationService.getText("err.required",
                                                                                       localizationService.getText(
                                                                                               "text.acsprofile.in.clientcertid",
                                                                                               locale()), locale())));

                        binerrs++;
                    }

                    if (Misc.isNotNullOrEmpty(acsProfile.getThreeDSMethodURL())) {
                        int maxLength = (TDSModel.MessageVersion.V2_3_0.value().equals(acsProfile.getStartProtocolVersion()) &&
                                TDSModel.MessageVersion.V2_3_0.value().equals(acsProfile.getEndProtocolVersion())) ? 2048 : 256;

                        if (acsProfile.getThreeDSMethodURL().length() > maxLength) {
                            result.addError(new FieldError("lists.list2[" + i + "]", "threeDSMethodURL",
                                    acsProfile.getThreeDSMethodURL(), false, null, null,
                                    localizationService.getText("err.invalid.length",
                                            localizationService.getText(
                                                    "err.must.be.length",
                                                    "<=" + maxLength, locale()),
                                            locale())));
                            binerrs++;
                        }
                    }

                    if (Misc.isNullOrEmpty(acsProfile.getRefNo())) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "refNo", acsProfile.getRefNo(), false, null, null,
                                               localizationService.getText("err.required", "ACSReferenceNumber",
                                                                           locale())));

                        binerrs++;
                    } else if (acsProfile.getRefNo().length() > 150) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "refNo", acsProfile.getRefNo(), false, null, null,
                                               localizationService.getText("err.invalid.length",
                                                                           localizationService.getText(
                                                                                   "err.must.be.length", "<=150",
                                                                                   locale()), locale())));

                        binerrs++;
                    }

                    if (Misc.isNotNullOrEmpty(acsProfile.getOperatorID()) && acsProfile.getOperatorID().length() > 32) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "operatorID", acsProfile.getOperatorID(), false, null,
                                               null, localizationService.getText("err.invalid.length",
                                                                                 localizationService.getText(
                                                                                         "err.must.be.length", "<=32",
                                                                                         locale()), locale())));
                        binerrs++;
                    }

                    if (Misc.isNullOrEmpty(acsProfile.getStatus())) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "status",
                                getLocalizedMessage("err.required", "text.issuer.status")));
                        binerrs++;
                    }

                    List<ACSProfile> acsProfilesList = issuerService.getAcsProfileByNameAndPaymentSystem(acsProfile
                            .getName(), issuer.getPaymentSystemId());
                    if (CollectionUtils.isNotEmpty(acsProfilesList)) {
                        for (ACSProfile acsProfile2 : acsProfilesList) {
                            if (!acsProfile2.getId().equals(acsProfile.getId())) {
                                result.addError(new FieldError("lists.list2[" + i + "]", "name", acsProfile.getName(),
                                        false, null, null, localizationService.getText("err.duplicate.value", "Name",
                                                locale())));
                                binerrs++;
                            }
                        }
                    }

                    if (StringUtils.isNotEmpty(acsProfile.getURL())) {
                        acsProfile.setURL(acsProfile.getURL().trim());
                        if (!Helpers.isValidUrl(acsProfile.getURL())) {
                            result.addError(new FieldError("lists.list2[" + i + "]", "URL", acsProfile.getURL(), false,
                                    null, null, getLocalizedMessage("err.invalid.url")));
                            binerrs++;
                        }
                    }

                    if (StringUtils.isNotEmpty(acsProfile.getThreeDSMethodURL())) {
                        acsProfile.setThreeDSMethodURL(acsProfile.getThreeDSMethodURL().trim());
                        if (!Helpers.isValidUrl(acsProfile.getThreeDSMethodURL())) {
                            result.addError(new FieldError("lists.list2[" + i + "]", "threeDSMethodURL", acsProfile
                                    .getThreeDSMethodURL(), false, null, null, getLocalizedMessage(
                                            "err.invalid.url")));
                            binerrs++;
                        }
                    }

                    if (binerrs == 0) {
                        okAcsProfiles.add(acsProfile);
                    }
                }
            }
        }

        if (!result.hasErrors() && !resultIss.hasErrors()) {
            int matter = 0;
            if (newIssuer) {
                issuer.setId(null);
                matter++;
            } else {

            }

            issuerService.saveOrUpdate(issuer, AuthorityUtil.getUser());

            Date now = new Date();
            if (okAcsProfiles.size() > 0) {
                for (int i = 0; i < okAcsProfiles.size(); i++) {
                    (okAcsProfiles.get(i)).setIssuerId(issuer.getId());
                    (okAcsProfiles.get(i)).setPaymentSystemId(issuer.getPaymentSystemId());
                    (okAcsProfiles.get(i)).setLastMod(now);
                }
                persistenceService.saveOrUpdate(okAcsProfiles);

            }
            if (deleteAcsProfiles.size() > 0) {
                persistenceService.delete(deleteAcsProfiles);
                for (int i = 0; i < deleteAcsProfiles.size(); i++) {
                    auditLogService.logDelete(AuthorityUtil.getUser(), deleteAcsProfiles.get(i));
                }
                matter++;
            }

            if (matter > 0) {
                Setting s = new Setting();
                s.setKey(DsSetting.DS_RELOAD_RANGES.getKey());
                s.setValue("100");
                s.setLastModified(new java.util.Date());
                s.setLastModifiedBy("ImportService");
                persistenceService.saveOrUpdate(s);
            }

            setContextAttribute("issuers_cached", null);
            return super.redirect("issuerView.html?id=" + issuer.getId());
        }
        return mav;
    }

    @RequestMapping(value = "/acsProfiles.html")
    public ModelAndView acsProfiles(@ModelAttribute("lists") Lists lists, BindingResult result, HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.issuerView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("issuers/acsProfiles");
        mav.addObject("lists", lists);
        mav.addObject("acpStatuses", statusMap(DSModel.ACSProfile.Status.class.getName()));
        mav.addObject("inCertificateList", inCertificateMap());
        mav.addObject("outCertificateList", outCertificateMap());

        if ("GET".equals(request.getMethod())) {
            if (lists != null) {
                lists.getList2().clear();
                lists.getList2().addAll(issuerService.getAttemptsACSList(null, null));
                return mav;
            }

            return mav;
        }

        // if post then save stuff
        if (!AuthorityUtil.hasRole(Roles.issuerEdit)) {
            return unauthorized(request);
        }

        if (Misc.isNotNullOrEmpty(request.getParameter("addNewACSP"))) {
            if (lists != null) {
                lists.getList2().add(new ACSProfile());
                return mav;
            }
        }

        List<ACSProfile> okACPs = new java.util.LinkedList<ACSProfile>();
        List<ACSProfile> delACPs = new java.util.LinkedList<ACSProfile>();
        for (int i = 0; lists != null && i < lists.getList2().size(); i++) {
            ACSProfile acsProfile = lists.getList2().get(i);
            if ("true".equals(request.getParameter("deleteacp" + acsProfile.getId()))) {
                delACPs.add(acsProfile);
            } else {
                if (Misc.isNullOrEmpty(acsProfile.getName()) && Misc.isNullOrEmpty(acsProfile.getURL())) {
					/*	if (cbx.getId() != null)
						{
							delCardBins.add(cbx);
						}
					*/
                } else {
                    int binerrs = 0;
                    if (Misc.isNullOrEmpty(acsProfile.getName())) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "name", acsProfile.getName(), false, null, null,
                                               localizationService.getText("err.required", "Name", locale())));

                        binerrs++;
                    }
                    if (Misc.isNullOrEmpty(acsProfile.getURL())) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "URL", acsProfile.getURL(), false, null, null,
                                                       localizationService.getText("err.required",
                                                                                   localizationService.getText(
                                                                                           "text.acsprofile.url",
                                                                                           locale()), locale())));

                        binerrs++;
                    } else if (acsProfile.getURL().length() > 1024) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "URL", acsProfile.getURL(), false, null, null,
                                                       localizationService.getText("err.invalid.length",
                                                                                   localizationService.getText(
                                                                                           "err.must.be.length", "6",
                                                                                           locale()), locale())));
                        binerrs++;
                    } else {
                        try {
                            if (acsProfile.getURL().startsWith("embed:")) {
                                // OK
                            } else {
                                new java.net.URL(acsProfile.getURL());
                            }

                        } catch (Exception e) {
                            result.addError(
                                    new FieldError("lists.list2[" + i + "]", "URL", acsProfile.getURL(), false, null, null,
                                                   localizationService.getText("err.invalid.value",
                                                                               localizationService.getText("valid.url",
                                                                                                           locale()),
                                                                               locale())));

                            binerrs++;
                        }
                    }

                    if (!sslCertsManagedExternally() && Misc.isNullOrEmpty(acsProfile.getInClientCert()) &&
                        !(acsProfile.getURL() != null && acsProfile.getURL().startsWith("embed:"))) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "inClientCert", acsProfile.getInClientCert(), false,
                                               null, null, localizationService.getText("err.required",
                                                                                       localizationService.getText(
                                                                                               "text.acsprofile.in.clientcertid",
                                                                                               locale()), locale())));
                        binerrs++;
                    }

                    if (Misc.isNotNullOrEmpty(acsProfile.getThreeDSMethodURL())) {
                        try {
                            new java.net.URL(acsProfile.getThreeDSMethodURL());

                        } catch (Exception e) {
                            result.addError(new FieldError("lists.list2[" + i + "]", "threeDSMethodURL",
                                                           acsProfile.getThreeDSMethodURL(), false, null, null,
                                                           localizationService.getText("err.invalid.value",
                                                                                       localizationService.getText(
                                                                                               "valid.url", locale()),
                                                                                       locale())));
                            binerrs++;
                        }
                    } else if (acsProfile.getThreeDSMethodURL().length() > 1024) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "threeDSMethodURL",
                                                       acsProfile.getThreeDSMethodURL(), false, null, null,
                                                       localizationService.getText("err.invalid.length",
                                                                                   localizationService.getText(
                                                                                           "err.must.be.length", "6",
                                                                                           locale()), locale())));
                        binerrs++;
                    }

                    if (Misc.isNullOrEmpty(acsProfile.getRefNo())) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "refNo", acsProfile.getRefNo(), false, null, null,
                                               localizationService.getText("err.required", "ACSReferenceNumber",
                                                                           locale())));

                        binerrs++;
                    } else if (acsProfile.getRefNo().length() > 150) {
                        result.addError(
                                new FieldError("lists.list2[" + i + "]", "refNo", acsProfile.getRefNo(), false, null, null,
                                               localizationService.getText("err.invalid.length",
                                                                           localizationService.getText(
                                                                                   "err.must.be.length", "<=150",
                                                                                   locale()), locale())));

                        binerrs++;
                    }

                    if (Misc.isNullOrEmpty(acsProfile.getStatus())) {
                        result.addError(new FieldError("lists.list2[" + i + "]", "status",
                                getLocalizedMessage("err.required", "text.issuer.status")));
                        binerrs++;
                    }

                    if (binerrs == 0) {
                        okACPs.add(acsProfile);
                    }
                }
            }
        }

        if (!result.hasErrors()) {
            if (okACPs.size() > 0) {
                for (ACSProfile acp : okACPs) {
                    acp.setIssuerId(null);
                    if (acp.getId() == null) {
                        auditLogService.logInsert(AuthorityUtil.getUser(), acp, null);
                    } else {
                        ACSProfile acpold = persistenceService.getPersistableById(acp.getId(), ACSProfile.class);
                        auditLogService.logUpdate(AuthorityUtil.getUser(), acpold, acp, null);
                    }
                }
                persistenceService.saveOrUpdate(okACPs);

            }
            if (delACPs.size() > 0) {
                persistenceService.delete(delACPs);
                for (ACSProfile acp : delACPs) {
                    auditLogService.logDelete(AuthorityUtil.getUser(), acp);
                }
            }

            mav.addObject("msg", this.localizationService.getText("text.data.saved.ok", locale()));
        }

        return mav;
    }

    @SuppressWarnings("unchecked")
    protected Map<String, String> attemptsACSMap(Integer paymentSystemId) {
        if (!"true".equals(this.getWebContext().getStringSetting(DsSetting.ATTEMPTS_ACS_ENABLED.getKey()))) {
            return null;
        }
        Map<String, String> acsMap = new java.util.LinkedHashMap<>();
        acsMap.put("", "");
        List<ACSProfile> acpList =
                issuerService.getAttemptsACSList(null, paymentSystemId);
        for (ACSProfile acp : acpList) {
            acsMap.put(String.valueOf(acp.getId()), acp.getName() + " (" + acp.getURL() + ")");
        }
        return acsMap;
    }

    public static class IssuerImportRequestData {

        private String uploadData;
        private String uploadFileName;
        private MultipartFile uploadFile;

        public MultipartFile getUploadFile() {
            return uploadFile;
        }

        public void setUploadFile(MultipartFile uploadFile) {
            this.uploadFile = uploadFile;
        }

        public String getUploadData() {
            return uploadData;
        }

        public void setUploadData(String uploadData) {
            this.uploadData = uploadData;
        }

        public String getUploadFileName() {
            return uploadFileName;
        }

        public void setUploadFileName(String uploadFileName) {
            this.uploadFileName = uploadFileName;
        }
    }


    String getParameterSetValueStartsWith(String s, HttpServletRequest request) {
        @SuppressWarnings("rawtypes") Enumeration ex = request.getParameterNames();
        while (ex.hasMoreElements()) {
            String pnx = (String) ex.nextElement();
            if (pnx.startsWith(s + "[")) {
                String value = pnx.substring(pnx.indexOf('[') + 1, pnx.length() - 1);
                return value;
            }
        }

        return null;
    }

}
