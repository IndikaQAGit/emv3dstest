package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.DsSettingSection;
import com.modirum.ds.enums.DsSettingType;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.ui.SettingItem;
import com.modirum.ds.mngr.ui.SettingSectionEntry;
import com.modirum.ds.mngr.ui.SettingSectionForm;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.SettingSearcher;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.util.BooleanUtils;
import com.modirum.ds.util.DateFormatUtils;
import com.modirum.ds.util.RegexUtil;
import com.modirum.ds.web.blocks.WebForm;
import com.modirum.ds.web.blocks.WebFormField;
import com.modirum.ds.web.context.WebContext;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Andri Kruus
 * @version 1.0, 10.02.2014
 */
@Controller
@RequestMapping("/settings")
public class SettingsController extends BaseController {

    @Autowired
    private KeyService keyService;

    public static final String SETTING_SECTIONS_MODEL_ATTRIBUTE = "settingSections";
    private final transient Logger log = LoggerFactory.getLogger(SettingsController.class);

    @ModelAttribute("settController")
    protected SettingsController getController() {
        return this;
    }

    public boolean isInSettingsAccessList(String ip) throws Exception {
        if (persistenceService.isSetting(DsSetting.DISABLE_DS_MNGR_IP_WHITELISTING.getKey())) {
            return true;
        }
        String settingsIpList = persistenceService.getStringSetting(DsSetting.SETTINGS_ACCESS_LIST.getKey());
        return ip != null && settingsIpList != null && settingsIpList.indexOf(ip) > -1;
    }

    public boolean isSettingEdtiable(String skey) throws Exception {
        return !DsSetting.SETTINGS_ACCESS_LIST.getKey().equals(skey) && !DsSetting.DS_BUILD_VERSION.getKey().equals(skey) && !"secretAttrs".equals(skey) &&
               (skey == null ||
                (!skey.startsWith("DS.instanceinfo") && !skey.startsWith("serviceShield.activeShields") &&
                 !skey.startsWith("stbo-")));
    }

    @RequestMapping("/settingEdit.html")
    public ModelAndView settingEdit(HttpServletRequest request, @RequestParam(value = "sid", required = false) String sid) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }
        // keep this only to authorized ips even with 2nd factor
        if (!isInSettingsAccessList(request.getRemoteAddr())) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }
        if (!isSettingEdtiable(sid)) {
            request.setAttribute("error", "This setting is not editable from web app");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/settingEdit");
        try {
            if (sid != null) {
                Setting s = persistenceService.getSettingById(sid);
                if (s == null) {
                    modelAndView.addObject("error", "Setting by id not found");
                    modelAndView.addObject("setting", new Setting());
                } else {
                    modelAndView.addObject("setting", s);
                }
            } else {
                modelAndView.addObject("setting", new Setting());
            }
        } catch (Exception e) {
            log.error("Error edit setting", e);
            throw e;
        }

        return modelAndView;
    }

    @RequestMapping(value = "/settingSave.html", method = RequestMethod.POST)
    public ModelAndView settingSave(HttpServletRequest request, @RequestParam(value = "cmd", required = false) String cmd, @Valid Setting setting, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }
        // keep this only to authorized ips even with 2nd factor
        if (!isInSettingsAccessList(request.getRemoteAddr())) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }

        if (!isSettingEdtiable(setting.getKey())) {
            request.setAttribute("error", "This setting is not editable from web app");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/settingEdit");
        try {

            String oldKey = request.getParameter("oldKey");

            if ("save".equals(cmd) || "saveAsNew".equals(cmd)) {

                // validation
                if (Misc.isNullOrEmpty(setting.getKey())) {
                    result.addError(new FieldError("setting", "key", setting.getKey(), false, null, null,
                                                   "Setting key can not be empty."));
                }

                if (setting.getProcessorId() == null) {
                    setting.setProcessorId((short) 0);
                }

                if (setting.getValue() != null && setting.getValue().length() > 20000) {
                    result.addError(new FieldError("setting", "value", setting.getValue(), false, null, null,
                                                   "Setting maximum length allowed is 20000 chars."));
                }

                if (setting.getComment() != null && setting.getComment().length() > 255) {
                    result.addError(new FieldError("setting", "comment", setting.getComment(), false, null, null,
                                                   "Setting comment max length allowed is 255 chars."));
                }

                if (!result.hasErrors()) {
                    if ("saveAsNew".equals(cmd)) {
                        Setting existing = persistenceService.getSettingById(setting.getKey());
                        if (existing != null) {
                            result.addError(new FieldError("setting", "key", setting.getKey(), false, null, null,
                                                           "Duplicate setting, setting with same key already exists."));
                        }
                    }

                }

                if (!result.hasErrors()) {

                    if (setting.getKey() != null &&
                        (setting.getKey().startsWith("scheme.") || setting.getKey().equals(DsSetting.MNGR_CSS.getKey()))) {
                        processor = null;
                    }

                    if ("save".equals(cmd)) {
                        if (Misc.isNotNullOrEmpty(oldKey) && !oldKey.equals(setting.getKey())) {
                            Setting old = new Setting();
                            old.setKey(oldKey);
                            persistenceService.delete(old);
                        }
                    }

                    setting.setLastModified(new java.util.Date());
                    setting.setLastModifiedBy(AuthorityUtil.getLastModifiedUser());

                    // encrypted setting support
                    if (setting.getValue() != null && setting.getValue().startsWith("enc:")) {
                        String valueToEnc = setting.getValue().substring(4);
                        log.error("Settings encryption is not supported.");
                        // TODO add support for settings encryption.
//                        KeyData keyData = ServiceLocator.getInstance().getKeyService().getSecretKey(
//                                KeyService.KeyAlias.dataKey,
//                                KeyData.KeyStatus.working,
//                                CryptoService.DATAENCALG);
//
//                        String valueEnc = "sec:" + crs.encrypt(valueToEnc,  keyData);
//                        setting.setValue(valueEnc);
                    }
                    persistenceService.saveOrUpdate(setting);

                    if (Misc.isNullOrEmpty(oldKey) || "saveAsNew".equals(cmd)) {
                        auditLogService.logInsert(AuthorityUtil.getUser(), setting,
                                                  "key " + setting.getKey() + ", value '" +
                                                  Misc.maskIntelligent(setting.getValue(), 1) + "'");
                    } else {
                        auditLogService.logUpdate(AuthorityUtil.getUser(), setting,
                                                  (Misc.isNotNullOrEmpty(oldKey) && !oldKey.equals(setting.getKey()) ?
                                                          oldKey + " -> " + setting.getKey() : setting.getKey()) +
                                                  ", value '" + Misc.maskIntelligent(setting.getValue(), 1) + "'");
                    }


                    getWebContext().resetSettingCache();
                    resetMaps();
                    processor = null; // rest processor (that is derived from settings)
                    return super.redirect("settingsList.html?ok=true");
                }
            } else if ("delete".equals(cmd)) {
                Setting existing = persistenceService.getSettingById(setting.getKey());
                if (existing != null) {
                    persistenceService.delete(existing);
                    auditLogService.logDelete(AuthorityUtil.getUser(), existing);

                    getWebContext().resetSettingCache();
                    resetMaps();
                    processor = null; // rest processor (that is derived from settings)
                    return super.redirect("settingsList.html?ok=true");
                } else {
                    result.addError(new FieldError("setting", "key", setting.getKey(), false, null, null,
                                                   "Such setting do not exist and could not be deleted."));
                }
            }
        } catch (Exception e) {
            log.error("Error getting/saving setting", e);
            throw e;
        }

        return modelAndView;
    }

    @RequestMapping("/settingsList.html")
    public ModelAndView settingsList(HttpServletRequest request, @RequestParam(value = "cmd", required = false) String cmd, @ModelAttribute(value = "s") SettingSearcher searcher, BindingResult result) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }
        if (!(AuthorityUtil.isFactor2Passed() || isInSettingsAccessList(request.getRemoteAddr()))) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/settingsList");
        if (persistenceService != null) {
            try {
                if (searcher != null && "search".equals(cmd)) {

                    modelAndView.addObject("searcher", searcher);
                    if ("key".equals(searcher.getOrder()) || "value".equals(searcher.getOrder()) ||
                        "lastModified".equals(searcher.getOrder())) {
                        /** allow valid orders only, prevent orm... */
                    } else {
                        searcher.setOrder("key");
                    }

                    if (searcher.getStart() == null) {
                        searcher.setStart(0);
                    }
                    if (searcher.getLimit() == null) {
                        searcher.setLimit(25);
                    }

                    searcher.setExclude("DS.timings.");
                    modelAndView.addObject("settingsList", persistenceService.findSettings(searcher));
                    modelAndView.addObject("s", searcher);
                }
            } catch (Exception e) {
                log.error("Error getting settings list", e);
                throw e;
            }
        }

        return modelAndView;
    }

    @RequestMapping("/shields.html")
    public ModelAndView shields(HttpServletRequest request, @RequestParam(value = "cmd", required = false) String cmd, @ModelAttribute(value = "s") SettingSearcher searcher, BindingResult result) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }
        if (!(AuthorityUtil.isFactor2Passed() || isInSettingsAccessList(request.getRemoteAddr()))) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/shields");
        if (persistenceService != null) {
            try {
                if ("purge".equals(cmd)) {
                    if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
                        return unauthorized(request);
                    }

                    WebContext ctx = new WebContext();
                    ctx.setRequest(request);
                    ctx.setServletContext(super.servletContext !=
                                          null ? super.servletContext : request.getSession().getServletContext());
                    ctx.getCtxHolder().setApplicationAttribute(PersistenceService.class.getName(), persistenceService);

                    ModelAndView mavpost = new ModelAndView("common/postform");

                    WebForm shieldsForm = new WebForm();
                    shieldsForm.setMethod("POST");
                    Setting set = persistenceService.getSettingById(DsSetting.SERVICE_URL.getKey());
                    if (set != null) {
                        shieldsForm.setAction(set.getValue());
                    } else {
                        shieldsForm.setAction("/ds/DServer");
                    }

                    WebFormField cmdf = new WebFormField();
                    cmdf.setType("hidden");
                    cmdf.setName("cmd");
                    cmdf.setValue("shields");
                    shieldsForm.addField(cmdf);

                    WebFormField ruf = new WebFormField();
                    ruf.setType("hidden");
                    ruf.setName("returnUrl");
                    ruf.setValue(request.getRequestURL().toString());
                    shieldsForm.addField(ruf);

                    WebFormField stbof = new WebFormField();
                    stbof.setType("hidden");
                    stbof.setName("stbo");
                    stbof.setValue(CryptoService.createSecurityToken());
                    shieldsForm.addField(stbof);

                    WebFormField dtof = new WebFormField();
                    dtof.setType("hidden");
                    dtof.setName("dt");
                    dtof.setValue(DateUtil.formatDate(new java.util.Date(), "yyyyMMddHHmmss", true));
                    shieldsForm.addField(dtof);


                    WebFormField loginf = new WebFormField();
                    loginf.setType("hidden");
                    loginf.setName("login");
                    loginf.setValue(AuthorityUtil.getUser().getLoginname());
                    shieldsForm.addField(loginf);

                    KeyData sharedSecretKeyData = keyService
                            .loadRawKeyData(KeyService.KeyAlias.sharedSecret, KeyData.KeyStatus.working);

                    byte[] sharedSecretBo = cryptoService.unwrapRawKey(sharedSecretKeyData);

                    String digestBase = Misc.defaultString(cmdf.getValue()) + "|" + Misc.defaultString(ruf.getValue()) + "|" +
                                        Misc.defaultString(dtof.getValue()) + "|" + Misc.defaultString(stbof.getValue()) + "|" +
                                        Misc.defaultString(loginf.getValue()) + "|" +
                                        new String(sharedSecretBo, StandardCharsets.UTF_8);
                    String digest = Misc.calculateSHA256AndEncodeBASE64(digestBase);

                    WebFormField tgf = new WebFormField();
                    tgf.setType("hidden");
                    tgf.setName("digest");
                    tgf.setValue(digest);
                    shieldsForm.addField(tgf);
                    shieldsForm.setAccept_charset("UTF-8");

                    auditLogService.logAction(AuthorityUtil.getUser(), new Setting(), DSModel.AuditLog.Action.DELETE,
                                              "User " + AuthorityUtil.getLastModifiedUser() +
                                              " performing clean shields");
                    mavpost.addObject("form", shieldsForm);
                    return mavpost;
                }

                if (request.getParameter("res") != null) {
                    modelAndView.addObject("res", request.getParameter("res"));
                }

                java.util.List<Setting> shields = persistenceService.getSettingsByIdStart(
                        "serviceShield.activeShields%", null, null);
                modelAndView.addObject("shields", shields);

            } catch (Exception e) {
                log.error("Error purge shields", e);
                //throw e;
            }
        }

        return modelAndView;
    }

    @RequestMapping("/keyInfo.html")
    public ModelAndView keyInfo(HttpServletRequest request, @RequestParam(value = "cmd", required = false) String cmd, @ModelAttribute(value = "searcher") SettingSearcher searcher, BindingResult result) throws Exception {

        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }
        if (!(AuthorityUtil.isFactor2Passed() || isInSettingsAccessList(request.getRemoteAddr()))) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/keyInfo");
        if (searcher.getStart() == null) {
            searcher.setStart(0);
        }
        if (searcher.getLimit() == null) {
            searcher.setLimit(25);
        }

        try {
            if (searcher != null && "search".equals(cmd)) {
                int[] total = new int[1];
                searcher.setTotal(total[0]);
                //modelAndView.addObject("searcher", searcher);
                List<KeyData> kdl = persistenceService.getKeyDatasByAlias(searcher.getKeyword(), searcher.getValue(),
                                                                          searcher.getOrder(),
                                                                          searcher.getOrderDirection(),
                                                                          searcher.getStart(), searcher.getLimit(),
                                                                          total);
                java.util.Map<String, String> x509Info = new java.util.LinkedHashMap<>();
                fillX509Info(kdl, x509Info);
                modelAndView.addObject("keyList", kdl);
                modelAndView.addObject("x509Info", x509Info);
                searcher.setTotal(total[0]);
            }
        } catch (Exception e) {
            log.error("Error getting keyinfo list", e);
            throw e;
        }
        return modelAndView;
    }

    void fillX509Info(List<KeyData> kdl, java.util.Map<String, String> x509Info) {
        for (KeyData kdx : kdl) {
            X509Certificate x509 = null;
            String cert = KeyService.parsePEMCert(kdx.getKeyData(), true);
            if (Misc.isNotNullOrEmpty(cert)) {
                try {
                    X509Certificate[] x509s = KeyService.parseX509PemHeadersSafeCertificates(
                            cert.getBytes(StandardCharsets.ISO_8859_1), "X509");
                    if (x509s.length > 0) {
                        x509 = x509s[0];
                    }

                } catch (Exception dc) {
                    log.warn("Failed to parse X509 " + dc);
                }
            }
            if (x509 != null) {
                x509Info.put("X509Subj." + kdx.getId(), "X509 subject: \t " + x509.getSubjectDN().getName());
                x509Info.put("X509Iss." + kdx.getId(), "X509 issuer: \t" + x509.getIssuerDN().getName());
                x509Info.put("X509Validity." + kdx.getId(), "X509 validity: \t " +
                                                            DateUtil.formatDate(x509.getNotBefore(),
                                                                                KeyService.signDateFormat, true) +
                                                            " - " + DateUtil.formatDate(x509.getNotAfter(),
                                                                                        KeyService.signDateFormat,
                                                                                        true) + " GMT");
                int bits = KeyService.getPublicKeyBitLength(x509.getPublicKey());
                x509Info.put("X509KeyBits." + kdx.getId(),
                             "X509 " + x509.getPublicKey().getAlgorithm() + " public key" +
                             (bits > -1 ? " bits: " + bits : ""));
            }
        }
    }

    public List<SettingSectionEntry> settingSections() throws Exception {

        Map<String, Setting> settingsMap = Optional.ofNullable(persistenceService.getAllSettings()).orElse(Collections.emptyList())
                                                   .stream()
                                                   .collect(Collectors.toMap(Setting::getKey, setting -> setting));

        Map<String, List<SettingItem>> map = new HashMap<>();

        for (DsSetting dsSetting : DsSetting.values()) {
            List<SettingItem> settingItems = map.get(dsSetting.getSection().name());
            if (Misc.isNullOrEmpty(settingItems)) {
                settingItems = new ArrayList<>();
                map.put(dsSetting.getSection().name(), settingItems);
            }

            Setting storedSetting = settingsMap.get(dsSetting.getKey());
            String settingValue = storedSetting != null ?
                                  storedSetting.getValue() :
                                  null;

            settingItems.add(new SettingItem()
                                     .setKey(dsSetting.getKey())
                                     .setType(dsSetting.getType().name())
                                     .setValue(settingValue)
                                     .setPlaceholder(dsSetting.getType().getPlaceholder())
                                     .setIsEditable(isSettingEdtiable(dsSetting.getKey()))
                                     .setLastModified(storedSetting != null ? storedSetting.getLastModified() : null)
                                     .setLastModifiedBy(storedSetting != null ? storedSetting.getLastModifiedBy() : null));
        }

        List<SettingSectionEntry> settingSections = Arrays.stream(DsSettingSection.values())
                                                          .map(dsSettingSection ->
                                                                       new SettingSectionEntry()
                                                                               .setName(dsSettingSection.name())
                                                                               .setDescription(dsSettingSection.getDescription())
                                                                               .setSettingItems(map.get(dsSettingSection.name())))
                                                          .collect(Collectors.toList());

        return settingSections;
    }

    @RequestMapping(value = "/settingsEnumList.html", method = RequestMethod.GET)
    public ModelAndView getSettingsEnumList(HttpServletRequest request) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }

        if (!(AuthorityUtil.isFactor2Passed() || isInSettingsAccessList(request.getRemoteAddr()))) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/settingsEnumList");
        modelAndView.addObject(SETTING_SECTIONS_MODEL_ATTRIBUTE, settingSections());
        modelAndView.getModelMap().put("activeSections", new HashSet<>());

        return modelAndView;
    }

    @RequestMapping(value = "/settingsEnumList.html", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ModelAndView saveEnumSetting(HttpServletRequest request, @RequestParam Map<String, String> formMap, @ModelAttribute("settingsForm") SettingSectionForm settingSectionForm, BindingResult result) throws Exception {
        settingSectionForm.addSettingsMap(formMap);

        if (!AuthorityUtil.hasRole(Roles.adminSetup)) {
            return unauthorized(request);
        }
        // keep this only to authorized ips even with 2nd factor
        if (!isInSettingsAccessList(request.getRemoteAddr())) {
            request.setAttribute("error", "Your ip address " + request.getRemoteAddr() +
                                          " is not in authorized ip list for settings");
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("settings/settingsEnumList");
        List<Setting> settingsUpdated = new ArrayList<>();
        for (Map.Entry<String, String> keyValueEntry : settingSectionForm.getSettings().entrySet()) {

            Setting setting = new Setting();
            setting.setKey(keyValueEntry.getKey());
            setting.setValue(keyValueEntry.getValue());

            com.modirum.ds.utils.Setting oldSetting = persistenceService.getSettingByKey(setting.getKey());

            //Detect change to include this setting in validation and database update
            //1. old setting is non existing and new setting value has value - requires insertion
            //2. old setting is existing and new setting has empty value - requires deletion
            //3. old setting is existing and new setting has different value - requires update
            if ((oldSetting == null && Misc.isNotNullOrEmpty(setting.getValue()))
                || (oldSetting != null && Misc.isNullOrEmpty(setting.getValue()))
                || (oldSetting != null && !setting.getValue().equals(oldSetting.getValue()))) {

                settingsUpdated.add(setting);

                Errors fieldErrors = validateSetting(settingSectionForm, setting);
                if (fieldErrors.hasErrors()) {
                    result.addAllErrors(fieldErrors);
                }
            }

        }

        if (!result.hasErrors()) {

            for (Setting setting : settingsUpdated) {
                saveSetting(setting);
            }

            if (settingsUpdated.size() > 0) {
                getWebContext().resetSettingCache();
                resetMaps();
                 processor = null; // rest processor (that is derived from settings)
                 modelAndView.addObject("processor", processor(request));
            }

            modelAndView.addObject(SETTING_SECTIONS_MODEL_ATTRIBUTE, settingSections());
            modelAndView.getModelMap().put("successMessage", "Successfully saved settings.");

        } else {

            List<SettingSectionEntry> settingSectionEntries = settingSections();

            for (SettingSectionEntry settingSectionEntry : settingSectionEntries) {

                settingSectionEntry.getSettingItems()
                                   .forEach(settingItem -> settingItem
                                           .setValue(settingSectionForm.getSettings().get(settingItem.getKey())));

            }

            modelAndView.getModelMap().put("errorMessage", "Encountered error(s) in form.");
            modelAndView.addObject(SETTING_SECTIONS_MODEL_ATTRIBUTE, settingSectionEntries);
        }

        modelAndView.getModelMap().put("activeSections", settingSectionForm.getShownSections());

        return modelAndView;

    }

    private Errors validateSetting(SettingSectionForm sectionForm, Setting setting) throws Exception {
        BeanPropertyBindingResult fieldErrors = new BeanPropertyBindingResult(sectionForm, "settingsForm");
        String formFieldName = "settings['" + setting.getKey() + "']";

        if (!isSettingEdtiable(setting.getKey())) {
            fieldErrors.rejectValue(formFieldName, null, "This setting is not editable from web app");
            return fieldErrors;
        }

        setting.setProcessorId((short) 0);

        if (Misc.isNotNullOrEmpty(setting.getValue())) {

            DsSetting dsSetting = DsSetting.keyOf(setting.getKey());

            if (DsSettingType.INT.equals(dsSetting.getType())) {

                if (Misc.parseIntBoxed(setting.getValue()) == null) {
                    fieldErrors.rejectValue(formFieldName, null, "Setting value should be an integer");
                }

            } else if (DsSettingType.LONG.equals(dsSetting.getType())) {

                if (Misc.parseLongBoxed(setting.getValue()) == null) {
                    fieldErrors.rejectValue(formFieldName, null, "Setting value should be a number");
                }

            } else if (DsSettingType.BOOLEAN.equals(dsSetting.getType())) {

                setting.setValue(setting.getValue().toLowerCase());

                if (!BooleanUtils.isBoolean(setting.getValue())) {
                    fieldErrors.rejectValue(formFieldName, null, "Setting value should be either true or false");
                }

            } else if (DsSettingType.DATE_FORMAT.equals(dsSetting.getType())) {

                if (!DateFormatUtils.isValidDateFormat(setting.getValue())) {
                    fieldErrors.rejectValue(formFieldName, null, "Setting value should be a valid date format");
                }

            } else if (DsSettingType.REGEX_PATTERN.equals(dsSetting.getType())) {

                if (!RegexUtil.isValidPattern(setting.getValue())) {
                    fieldErrors.rejectValue(formFieldName, null, "Setting value should be a valid regex pattern");
                }

            }

        }

        if (setting.getValue() != null && setting.getValue().length() > 20000) {
            fieldErrors.rejectValue(formFieldName, null, "Setting maximum length allowed is 20000 chars.");
        }

        return fieldErrors;

    }

    private void saveSetting(Setting setting) throws Exception {
        setting.setLastModified(new java.util.Date());
        setting.setLastModifiedBy(AuthorityUtil.getLastModifiedUser());

        // encrypted setting support
        if (setting.getValue() != null && setting.getValue().startsWith("enc:")) {
            String valueToEnc = setting.getValue().substring(4);
            log.error("Settings encryption is not supported.");
            // TODO add support for settings encryption.
//            String valueEnc = "sec:" + crs.encrypt(valueToEnc,
//                                                   ServiceLocator.getInstance().getKeyService().getSecretKey(
//                                                           KeyService.KeyAlias.dataKey,
//                                                           KeyData.KeyStatus.working,
//                                                           CryptoService.DATAENCALG));
//            setting.setValue(valueEnc);
        }

        boolean isSettingExisting = persistenceService.getSettingByKey(setting.getKey()) != null;

        if (Misc.isNullOrEmpty(setting.getValue()) && isSettingExisting) {

            persistenceService.delete(setting);
            auditLogService.logDelete(AuthorityUtil.getUser(), setting,
                                      "key " + setting.getKey() + ", value '" +
                                      Misc.maskIntelligent(setting.getValue(), 1) + "'");

        } else {

            persistenceService.saveOrUpdate(setting);

            if (isSettingExisting) {
                auditLogService.logUpdate(AuthorityUtil.getUser(), setting,
                                          setting.getKey() + ", value '" +
                                          Misc.maskIntelligent(setting.getValue(), 1) + "'");
            } else {
                auditLogService.logInsert(AuthorityUtil.getUser(), setting,
                                          "key " + setting.getKey() + ", value '" +
                                          Misc.maskIntelligent(setting.getValue(), 1) + "'");
            }

        }

    }

}


