package com.modirum.ds.mngr.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.enums.DSId;
import com.modirum.ds.enums.PaymentSystemSettingInputType;
import com.modirum.ds.enums.PaymentSystemType;
import com.modirum.ds.enums.PsSetting;
import com.modirum.ds.mngr.model.PsSettingUiEnum;
import com.modirum.ds.services.PaymentSystemConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.ui.PaymentSystemConfigUIForm;
import com.modirum.ds.jdbc.core.model.PaymentSystemConfig;

@Controller
@RequestMapping("/paymentSystemSettings")
public class PaymentSystemSettingController extends BaseController {

    @Autowired
    private PaymentSystemConfigService paymentSystemConfigService;

    @RequestMapping("/paymentSystemSettingsList.html")
    public ModelAndView settingsList(HttpServletRequest request,
            @RequestParam(value = "paymentSystemId", required = true) Integer paymentSystemId) throws Exception {

        PaymentSystem paymentSystem = paymentSystemsService.get(paymentSystemId);
        if (!AuthorityUtil.isAccessAllowed(paymentSystem, Roles.paymentsystemView) &&
            !AuthorityUtil.isAccessAllowed(paymentSystem, Roles.adminSetup)) {
            return unauthorized(request);
        }

        ModelAndView modelAndView = new ModelAndView("paymentSystemSettings/paymentSystemSettingsList");
        Map<String, PaymentSystemConfig> paymentSystemConfigMap = paymentSystemConfigService.getPaymentSystemConfigs(
                DSId.DEFAULT, paymentSystemId);

        if (isELO(paymentSystem)) {
            modelAndView.addObject("eloPAPIPaymentSystemSettings",
                                   convertToUIForm(PsSettingUiEnum.ELO_PAPI.getPsSettings(), paymentSystemConfigMap,
                                                   paymentSystemId));
        }

        modelAndView.addObject("adminPaymentSystemSettings",
                               convertToUIForm(PsSettingUiEnum.ADMIN.getPsSettings(), paymentSystemConfigMap,
                                               paymentSystemId));
        modelAndView.addObject("userPaymentSystemSettings",
                               convertToUIForm(PsSettingUiEnum.USER.getPsSettings(), paymentSystemConfigMap,
                                               paymentSystemId));
        modelAndView.addObject("paymentSystem", paymentSystem);
        return modelAndView;
    }

    @RequestMapping("/paymentSystemSettingsEdit.html")
    public ModelAndView settingsEdit(HttpServletRequest request,
            @RequestParam(value = "dsId", required = true) Integer dsId,
            @RequestParam(value = "paymentSystemId", required = true) Integer paymentSystemId,
            @RequestParam(value = "key", required = true) String key,
            @RequestParam(value = "value", required = false) String value,
            @RequestParam(value = "comment", required = true) String comment,
            @RequestParam(value = "inputType", required = true) String inputType) throws Exception {

        if (isUnauthorizedToEdit(paymentSystemId, key)) {
            return unauthorized(request);
        }

        if (isUncheckedCheckbox(inputType, value)) {
            // Unchecked checkboxes will have a default value of null.
            // To make it more consistent and readable in DB that's why we set the value to false
            value = "false";
        }

        PaymentSystemConfig updatedPaymentSystemConfig = PaymentSystemConfig.builder().dsId(dsId).paymentSystemId(
                paymentSystemId).key(key).comment(comment).value(value).build();
        paymentSystemConfigService.saveOrUpdatePaymentSystemConfig(updatedPaymentSystemConfig, AuthorityUtil.getUser());

        return redirect("paymentSystemSettingsList.html?paymentSystemId=" + paymentSystemId);
    }

    private boolean isUnauthorizedToEdit(Integer paymentSystemId, String psSettingKey) {
        PaymentSystem paymentSystem = paymentSystemsService.get(paymentSystemId);
        PsSetting psSetting = PsSetting.fromKey(psSettingKey);
        boolean isSettingEditable = psSetting != null && psSetting.isEditable();
        boolean unauthorizedUserEdit = PsSettingUiEnum.USER.equals(PsSettingUiEnum.getType(psSettingKey)) &&
                                       !AuthorityUtil.isAccessAllowed(paymentSystem, Roles.paymentsystemEdit);
        boolean unauthorizedAdminEdit = PsSettingUiEnum.ADMIN.equals(PsSettingUiEnum.getType(psSettingKey)) &&
                                        !AuthorityUtil.isAccessAllowed(paymentSystem, Roles.adminSetup);
        boolean unauthorizedELOPAPIEdit = PsSettingUiEnum.ELO_PAPI.equals(PsSettingUiEnum.getType(psSettingKey)) &&
                                       !AuthorityUtil.isAccessAllowed(paymentSystem, Roles.paymentsystemEdit);

        return !isSettingEditable ||  unauthorizedUserEdit || unauthorizedAdminEdit || unauthorizedELOPAPIEdit;
    }

    private List<PaymentSystemConfigUIForm> convertToUIForm(List<PsSetting> psSettings, Map<String, PaymentSystemConfig> paymentSystemConfigMap, Integer paymentSystemId) {
        List<PaymentSystemConfigUIForm> paymentSystemConfigUIForms = new ArrayList<PaymentSystemConfigUIForm>();
        for (PsSetting psSetting : psSettings) {
            PaymentSystemConfig paymentSystemConfig = paymentSystemConfigMap.getOrDefault(psSetting.getKey(), new PaymentSystemConfig());
            PaymentSystemConfigUIForm paymentSystemConfigUIForm = PaymentSystemConfigUIForm.builder()
                    .dsId(paymentSystemConfig.getDsId())
                    .paymentSystemId(paymentSystemId)
                    .key(psSetting.getKey())
                    .editable(AuthorityUtil.hasAnyRole(Roles.paymentsystemEdit))
                    .inputType(psSetting.getInputType())
                    .comment(paymentSystemConfig.getComment())
                    .value(paymentSystemConfig.getValue())
                    .lastModifiedDate(paymentSystemConfig.getLastModifiedDate())
                    .lastModifiedBy(paymentSystemConfig.getLastModifiedBy())
                    .build();
            paymentSystemConfigUIForms.add(paymentSystemConfigUIForm);
        }
        return paymentSystemConfigUIForms;
    }

    private boolean isELO(PaymentSystem paymentSystem) {
        return PaymentSystemType.elo.isEqual(paymentSystem.getType());
    }

    private boolean isUncheckedCheckbox(String inputType, String value) {
        return PaymentSystemSettingInputType.CHECKBOX.equalsIgnoreCase(inputType) && StringUtils.isEmpty(value);
    }
}
