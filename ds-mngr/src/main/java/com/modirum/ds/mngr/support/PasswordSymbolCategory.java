package com.modirum.ds.mngr.support;

/**
 * User: Jevgeni Shumik
 * Date: 06/05/2016 10:39
 */
public class PasswordSymbolCategory {

    final public static String SymbolsDefault = "!@#$%^&*()_+";

    private int totalOccurrences = 0;
    private int sequence = 0;

    String categoryName;
    int minimumRequired = 0;
    int maximumSequence = 0;
    String customSymbols = null;

    public PasswordSymbolCategory(String categoryName, int minimumRequired, int maximumSequence) {
        this.categoryName = categoryName;
        this.minimumRequired = minimumRequired;
        this.maximumSequence = maximumSequence;
    }

    public void increment() {
        totalOccurrences++;
        sequence++;
    }

    public void restartSequence() {
        sequence = 0;
    }

    public String checkMinimumOccurrences() {
        if (minimumRequired > 0 && totalOccurrences < minimumRequired) {
            return "Must have at least " + minimumRequired + " of " + categoryName;
        }
        return null;
    }

    public String checkMaximumSequence() {
        if (maximumSequence > 0 && maximumSequence < sequence) {
            return ("Maximum " + categoryName + " consecutive sequence of " + maximumSequence + " exceeded");
        }

        return null;
    }

    public boolean isSymbol(char c) {
        if (customSymbols != null && customSymbols.length() > 0) {
            return customSymbols.indexOf(c) > -1;
        }

        return SymbolsDefault.indexOf(c) > -1;
    }

    public boolean minimumRequirementsFulfilled() {
        return minimumRequired > 0 && totalOccurrences >= minimumRequired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PasswordSymbolCategory)) {
            return false;
        }

        PasswordSymbolCategory that = (PasswordSymbolCategory) o;

        return categoryName.equals(that.categoryName);
    }

    @Override
    public int hashCode() {
        return categoryName.hashCode();
    }

    public int getMinimumRequired() {
        return minimumRequired;
    }

    public void setMinimumRequired(int minimumRequired) {
        this.minimumRequired = minimumRequired;
    }

    public int getMaximumSequence() {
        return maximumSequence;
    }

    public void setMaximumSequence(int maximumSequence) {
        this.maximumSequence = maximumSequence;
    }

    public String getCustomSymbols() {
        return customSymbols;
    }

    public void setCustomSymbols(String customSymbols) {
        this.customSymbols = customSymbols;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
