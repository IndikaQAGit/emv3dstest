package com.modirum.ds.mngr.controller;

import com.modirum.ds.services.ProductInfoService;
import com.modirum.ds.services.db.jdbc.audit.AuditLogJdbcService;
import com.modirum.ds.services.db.jdbc.core.DSInitService;
import com.modirum.ds.utils.db.IdGen;
import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.services.PaymentSystemsService;
import com.modirum.ds.mngr.support.ApplicationControl;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.mngr.support.MdmVersionService;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.db.model.CertificateData;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.Processor;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.AuditLogService;
import com.modirum.ds.services.ConfigService;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.LocalizationService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.ISO3166;
import com.modirum.ds.utils.ISO4217;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.web.context.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.Field;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.modirum.ds.enums.DsSetting.MNGR_BIN_LENGTH;

@Controller
public class BaseController implements ServletContextAware {

    private static final Logger log = LoggerFactory.getLogger(BaseController.class);

    private final static String dateFormat = ("yyyy-MM-dd HH:mm");

    @Autowired
    protected AuditLogJdbcService auditLogJdbcService;
    @Autowired
    protected DSInitService dsInitService;
    @Autowired
    protected CryptoService cryptoService;

    protected PersistenceService persistenceService       = ServiceLocator.getInstance().getPersistenceService();
    protected LocalizationService localizationService     = ServiceLocator.getInstance().getLocalizationService();
    protected AuditLogService auditLogService             = ServiceLocator.getInstance().getAuditLogService();
    protected ConfigService cnfs                          = ServiceLocator.getInstance().getConfigService();
    protected PaymentSystemsService paymentSystemsService = ServiceLocator.getInstance().getPaymentSystemService();
    protected ProductInfoService productInfoService       = ServiceLocator.getInstance().getProductInfoService();

    protected static Processor processor;

    @Autowired
    protected ApplicationControl application;

    @Autowired(required = false)
    @Qualifier("saveSearchToSession")
    protected Boolean saveSearchToSession;

    public static BaseController instance = null;

    public BaseController() {
        instance = this;
    }

    @ModelAttribute("resetLists")
    public String resetAllCachedListIfImporterRunDetected() {
        if (servletContext != null) {
            String prevIsId = (String) servletContext.getAttribute("importSessionId");
            Long lastCheck = (Long) servletContext.getAttribute("importSessionIdLastCheck");
            if (lastCheck == null || lastCheck < System.currentTimeMillis() - 60000) {
                servletContext.setAttribute("importSessionIdLastCheck", System.currentTimeMillis());
                Setting sise = this.persistenceService.getSettingById(DsSetting.IMPORT_SESSION_ID.getKey());
                if (sise != null && sise.getValue() != null && !sise.getValue().equals(prevIsId)) {
                    servletContext.setAttribute("importSessionId", sise.getValue());

                    Enumeration<String> e = servletContext.getAttributeNames();
                    while (e.hasMoreElements()) {
                        String an = e.nextElement();
                        if (an != null && an.contains("_cached")) {
                            servletContext.setAttribute(an, null);
                        }
                    }
                    return "true";
                }
            }
        }
        return "false";
    }

    @ModelAttribute("supportedLocales")
    public Map<String, String> getSupportedLocales() {
        Map<String, String> am = (Map<String, String>) servletContext.getAttribute("supportedLocales_" + locale() + "_cached");
        if (am == null) {
            am = new LinkedHashMap<>();
            am.put("", "");
            try {
                List<Locale> sl = this.getWebContext().getSupportedLocaleList();
                for (int i = 0; i < sl.size(); i++) {
                    Locale ct = sl.get(i);
                    am.put(ct.toString(), localizationService.getText("locale." + ct.toString(), locale()));
                }
            } catch (Exception e) {
                log.error("Error getting locales list", e);
            }

            servletContext.setAttribute("supportedLocales_" + locale() + "_cached", am);
        }
        return am;
    }

    @ModelAttribute("certificateExpWarning")
    public String certificateWarning(HttpServletRequest request) {
        if (AuthorityUtil.hasRole(Roles.userEdit) || AuthorityUtil.hasRole(Roles.issuerEdit) ||
            AuthorityUtil.hasRole(Roles.acquirerEdit) || AuthorityUtil.hasRole(Roles.adminSetup) ||
            AuthorityUtil.hasRole(Roles.caEdit)) {
            String wrn = (String) getSessionAttribute(request, "ds.expCertificates");

            if (wrn != null && wrn.length() > 0) {
                return wrn;
            }

            StringBuilder result = new StringBuilder();
            Date now = new Date();
            if (!getWebContext().isTrueSetting(DsSetting.SSL_CERTS_MANAGED_EXTERNALLY.getKey())) {
                Date upperLimit = new Date(System.currentTimeMillis() + 20 * 24L * 3600L * 1000L);
                Date lowerLimit = new Date(System.currentTimeMillis() - 10 * 24L * 3600L * 1000L);

                List<CertificateData> ds = persistenceService.getCertificatesByExpiryDateRange(lowerLimit, upperLimit);
                for (CertificateData cdata : ds) {
                    appendWarning(result, now, cdata.getExpires(), cdata.getSubjectDN());
                }
                String warnings = result.toString();
                setSessionAttribute(request, "ds.expCertificates", warnings);
                return warnings;
            } else {
                try {
                    List<Setting> expCerts = persistenceService.getSettingsByIdStart("DS.expCerts.", null, null);
                    for (Setting sx : expCerts) {
                        String settingContent = sx.getValue();
                        if (Misc.isNotNullOrEmpty(settingContent)) {
                            List<String> elements = Misc.splitToList(settingContent, ";");
                            for (String pair : elements) {
                                String[] sndDate = Misc.splitTwo(pair, "|");
                                if (sndDate != null && sndDate.length == 2) {
                                    if (Misc.isNotNullOrEmpty(sndDate[0]) && result.indexOf(sndDate[0]) > -1) {
                                        continue;
                                    }
                                    Date expDate = DateUtil.parseDate(sndDate[1], dateFormat);
                                    appendWarning(result, now, expDate, sndDate[0]);
                                }
                            }
                        } else {
                            break;
                        }
                    }
                } catch (Exception e) {
                    log.warn("Error during warning creation", e);
                    return "Unable to check expiring certificates errid= " + e;
                }

                String warnings = result.toString();
                setSessionAttribute(request, "ds.expCertificates", warnings);
                return warnings;
            }
        }
        return null;
    }

    @ModelAttribute("cryptoperiodWarning")
    public String getCryptoperiodWarning(HttpServletRequest request) {
        if (persistenceService != null &&
            (AuthorityUtil.hasRole(Roles.userEdit) || AuthorityUtil.hasRole(Roles.adminSetup))) {
            String wrn = (String) getSessionAttribute(request, "ds.cryptoperiodwarning");
            if (wrn != null && wrn.length() == 0) {
                return null;
            } else if (wrn != null && wrn.length() > 0) {
                return wrn;
            }

            try {
                wrn = KeyService.createCryptoPeriodWarnings(persistenceService);
                setSessionAttribute(request, "ds.cryptoperiodwarning", wrn);
                if (wrn.length() == 0) {
                    return null;
                } else if (wrn.length() > 0) {
                    return wrn;
                }
            } catch (Exception e) {
                String errId = "" + System.currentTimeMillis();
                wrn = "Unable to check crypto periods errid= " + e;
                log.error("Unable to check crypto periods errid=" + errId, e);
                return wrn;
            }
        }

        return null;
    }

    protected void appendWarning(StringBuilder result, Date now, Date expDate, String sdn) {
        long days = DateUtil.calculateDaysDiff(now, expDate);
        result.append("SDN: ").append(sdn).append(
                days > -1 ? " expires in " + days + " days" : " expired " + (-days) + " ago").append('\n');
    }

    @ModelAttribute("attemptsACSEnabled")
    public Boolean attemptsACSEnabled() {
        return "true".equals(this.getWebContext().getStringSetting(DsSetting.ATTEMPTS_ACS_ENABLED.getKey()));
    }

    @ModelAttribute("sslCertsManagedExternally")
    public Boolean sslCertsManagedExternally() {
        return getWebContext().isTrueSetting(DsSetting.SSL_CERTS_MANAGED_EXTERNALLY.getKey());
    }

    @ModelAttribute("dateFormat")
    public String getDateFormat() {
        return ((SimpleDateFormat) this.getWebContext().getDateFormat(java.text.DateFormat.SHORT, 1)).toPattern();
    }

    @ModelAttribute("serverTime")
    public Date getServerTime() {
        return new Date();
    }

    @ModelAttribute("timeZone")
    public TimeZone getTimeZone() {
        TimeZone ustz = (TimeZone) this.getWebContext().getSessionAttribute("usTimeZone");
        if (ustz == null) {
            User user = AuthorityUtil.getUser();
            if (user != null && user.getFactor2Data3() != null && getTimeZones().get(user.getFactor2Data3()) != null) {
                ustz = TimeZone.getTimeZone(user.getFactor2Data3());
            }
            if (ustz == null) {
                ustz = TimeZone.getDefault();
            }
            this.getWebContext().setSessionAttribute("usTimeZone", ustz);
        }

        return ustz;
    }

    public void setTimeZone(String tzId) {

        if (getTimeZones().get(tzId) != null) {
            this.getWebContext().setSessionAttribute("usTimeZone", TimeZone.getTimeZone(tzId));

            User user = AuthorityUtil.getUser();
            if (user != null) {
                try {
                    User dbuser = persistenceService.getPersistableById(user.getId(), User.class);
                    if (dbuser != null) {
                        dbuser.setFactor2Data3(tzId);
                        persistenceService.saveOrUpdate(dbuser);
                        auditLogService.logUpdate(user, dbuser, dbuser.getLoginname() + " selg set new timezone" + tzId);
                    }
                } catch (Exception e) {
                    log.error("Unexpected: failed to store user preferred timezone", e);
                }
            }
        }
    }

    @ModelAttribute("timeZones")
    public Map<String, String> getTimeZones() {
        // by hour cached short time because this is not stable and may change over time zone daylighsavings swich on/off
        String currentHour = DateUtil.formatDate(new Date(), "HH", true);
        Map<String, String> tzMap = (Map<String, String>) this.getWebContext().getApplicationAttribute("tzMap_cached." + currentHour);
        if (tzMap == null) {
            String mngrTimeZones = this.getWebContext().getStringSetting(DsSetting.MNGR_TIMEZONES.getKey());
            Date now = new Date();
            tzMap = new LinkedHashMap<>();
            for (String tzid : TimeZone.getAvailableIDs()) {
                TimeZone tzx = TimeZone.getTimeZone(tzid);
                if (tzx != null && (Misc.isNullOrEmpty(mngrTimeZones) || mngrTimeZones.contains(tzid))) {
                    String offsStr = DateUtil.formatDate(now, "Z", tzx);
                    tzMap.put(tzx.getID(), offsStr + " " + tzid);
                }
            }
            this.clearContextAttributeStartsWith(new String[]{"tzMap_cached."});
            this.getWebContext().setApplicationAttribute("tzMap_cached." + currentHour, tzMap);
        }
        return tzMap;
    }

    @ModelAttribute("licenseWarning")
    public String getLicenseWarning(HttpServletRequest request) {
        return null;
    }

    // date type binder
    @InitBinder
    public void binder(WebDataBinder binder) {
        // as shown above
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                try {
                    if (Misc.isNotNullOrEmpty(value)) {
                        if (value.length() == getDateFormat().length() + 9) {
                            Date d = DateUtil.parseDate(value, getDateFormat() + " HH:mm:ss", getTimeZone());
                            if (d == null) {
                                throw new IllegalArgumentException(
                                        "Invalid date format, must be " + getDateFormat() + " HH:mm:ss");
                            }
                            setValue(d);
                        } else {
                            Date d = DateUtil.parseDate(value, getDateFormat(), getTimeZone());
                            if (d == null) {
                                throw new IllegalArgumentException("Invalid date format, must be " + getDateFormat());
                            }
                            setValue(d);
                        }
                    } else {
                        setValue(null);
                    }
                } catch (Exception e) {
                    setValue(null);
                    throw new IllegalArgumentException("Invalid date format, must be " + getDateFormat());
                }
            }

            public String getAsText() {
                return DateUtil.formatDate((Date) getValue(), getDateFormat(), getTimeZone());
            }
        });

        binder.registerCustomEditor(Short.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                if (Misc.isNotNullOrEmpty(value) && Misc.isInt(value)) {
                    setValue(Short.valueOf((short) Misc.parseInt(value)));
                } else {
                    setValue(null);
                }
            }

            public String getAsText() {
                return "" + getValue();
            }
        });

        binder.registerCustomEditor(short.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                if (Misc.isNotNullOrEmpty(value) && Misc.isInt(value)) {
                    setValue((short) Misc.parseInt(value));
                } else {
                    setValue((short) 0);
                }
            }

            public String getAsText() {
                return "" + getValue();
            }
        });

        binder.registerCustomEditor(int.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                if (Misc.isNotNullOrEmpty(value) && Misc.isInt(value)) {
                    setValue(Misc.parseInt(value));
                } else {
                    setValue(0);
                }
            }

            public String getAsText() {
                return "" + getValue();
            }
        });

        binder.registerCustomEditor(Integer.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                if (Misc.isNotNullOrEmpty(value) && Misc.isInt(value)) {
                    setValue(Misc.parseInt(value));
                } else {
                    setValue(null);
                }
            }

            public String getAsText() {
                return getValue() != null ? "" + getValue() : null;
            }
        });


        binder.registerCustomEditor(Long.class, new PropertyEditorSupport() {
            public void setAsText(String value) {
                if (Misc.isNotNullOrEmpty(value) && Misc.isLong(value)) {
                    setValue(Misc.parseLongBoxed(value));
                } else {
                    setValue(null);
                }

            }

            public String getAsText() {
                return getValue() != null ? "" + getValue() : null;
            }
        });


    }

    @ModelAttribute("loca")
    public LocalizationService loca() {
        return localizationService;
    }

    protected ServletContext servletContext;

    @ModelAttribute("appVersion")
    public String appVersion() {
        String appVersion = servletContext != null ? (String) servletContext.getAttribute("appVersion") : null;
        if (appVersion == null && servletContext != null) {

            appVersion = MdmVersionService.getBuildVersion();
            if (appVersion != null && servletContext != null) {
                servletContext.setAttribute("appVersion", appVersion);
            }
        }
        return appVersion;
    }

    @ModelAttribute("appName")
    public String appName() {
        return ApplicationControl.getAppName();
    }

    @ModelAttribute("instanceId")
    public long getInstanceId() {
        return IdGen.getDbId();
    }

    public WebContext getWebContext() {
        WebContext wctx = (WebContext) getSessionAttribute("WebContext");
        if (wctx == null) {
            wctx = new WebContext();
            wctx.setSession(session());
            setSessionAttribute("WebContext", wctx);
        }
        wctx.setSettingService(cnfs);
        wctx.setServletContext(this.servletContext);


        return wctx;
    }

    @ModelAttribute("locale")
    public Locale locale() {
        return getWebContext().getLocale();
    }

    public void fillModelAttrs(ModelAndView mv) {
        mv.addObject("loca", loca());
        mv.addObject("locale", locale());
        mv.addObject("dateFormat", getDateFormat());
        mv.addObject("supportedLocales", getSupportedLocales());
        mv.addObject("appVersion", appVersion());
        mv.addObject("appName", appName());
        mv.addObject("userName", getUserName());
    }

    @ModelAttribute("userName")
    public String getUserName() {
        User user = AuthorityUtil.getUser();
        return user != null ? user.getLoginname() : null;
    }

    @ModelAttribute("isSuperuser")
    public Boolean isSuperuser() {
        User user = AuthorityUtil.getUser();
        return user != null && user.getPaymentSystemId() == null;
    }

    @ModelAttribute("lastGet")
    public String lastGet(HttpServletRequest request) {
        String prevGet = (String) getSessionAttribute(request, "lastGetSess");
        if ("GET".equals(request.getMethod())) {
            String qs = request.getQueryString();
            setSessionAttribute(request, "lastGetSess",
                                request.getRequestURI() + (qs != null && qs.length() > 0 ? "?" + qs : ""));
        }
        return prevGet;
    }

    /**
     * Logs unauthorized access attempt and redirects to unauthorized page.
     */
    public ModelAndView unauthorized(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("errors/unauthorized");
        String error = (String) request.getAttribute("error");
        if (error == null) {
            error = getLocalizedMessage("err.privilege.noaccess");
        }
        User user = AuthorityUtil.getUser();
        String orig = (String) request.getAttribute("javax.servlet.forward.request_uri");
        if (orig == null) {
            orig = request.getRequestURI();
        }
        auditLogService.logAction(user, null, DSModel.AuditLog.Action.UNAUTH,
                                  "URI=" + orig + "; IP=" + request.getRemoteAddr() +
                                  (user != null ? " user: " + user.getLoginname() + "/" + user.getId() : "") + " " +
                                  error);

        mav.addObject("errorm", error);
        return mav;
    }

    /**
     * redirects without exposing model attributes.
     */
    public ModelAndView redirect(String to) {
        return new ModelAndView(new RedirectView(to, true, true, false));
    }

    public ModelAndView invalidRequest() {
        return redirect("/errors/invalid-request.html");
    }

    protected Map<String, String> countriesMap() {
        Map<String, String> countriesMap = (Map<String, String>) getContextAttribute("countriesMap_cached" + locale());

        if (countriesMap == null) {
            countriesMap = new LinkedHashMap<>();
            countriesMap.put("", "");
            ISO3166[] countries = ISO3166.values();
            for (ISO3166 country : countries) {
                String key = "country." + country.getA2code();
                String cname = localizationService.getText(key, locale());
                if (cname.contains(key)) {
                    cname = country.getCountryName();
                }

                countriesMap.put(String.valueOf(country.getNumeric()), cname);
            }
            setContextAttribute("countriesMap_cached" + locale(), countriesMap);
        }

        return countriesMap;
    }

    protected Map<String, String> countriesMapA2() {
        Map<String, String> countriesMap = (Map<String, String>) getContextAttribute("countriesMapA2_cached" + locale());

        if (countriesMap == null) {
            ISO3166[] countries = ISO3166.values();
            countriesMap = new LinkedHashMap<>();
            countriesMap.put("", "");
            for (ISO3166 country : countries) {
                String key = "country." + country.getA2code();
                String cname = localizationService.getText(key, locale());
                if (cname.contains(key)) {
                    cname = country.getCountryName();
                }
                cname = cname + " (" + country.getA2code() + ")";

                countriesMap.put(country.getA2code(), cname);
            }
            setContextAttribute("countriesMapA2_cached" + locale(), countriesMap);
        }

        return countriesMap;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public static Object getSessionAttribute(HttpServletRequest request, String key) {
        HttpSession s = request.getSession(false);
        if (s != null) {
            return s.getAttribute(key);
        }
        return null;
    }

    public static void setSessionAttribute(HttpServletRequest request, String key, Object attr) {
        HttpSession s = request.getSession(false);
        if (s != null) {
            s.setAttribute(key, attr);
        }
    }

    public static void setSessionAttribute(String key, Object attr) {
        HttpSession s = session();
        if (s != null) {
            s.setAttribute(key, attr);
        }
    }

    public static Object getSessionAttribute(String key) {
        HttpSession s = session();
        if (s != null) {
            return s.getAttribute(key);
        }
        return null;
    }

    public static Object removeSessionAttribute(String key) {
        HttpSession s = session();
        if (s != null) {
            Object o = s.getAttribute(key);
            s.removeAttribute(key);
            return o;
        }

        return null;
    }

    public Object getContextAttribute(String key) {
        if (servletContext != null) {
            return servletContext.getAttribute(key);
        }

        return null;
    }

    public void setContextAttribute(String key, Object attr) {
        if (servletContext != null) {
            servletContext.setAttribute(key, attr);
        }
    }

    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(false);
    }

    @ModelAttribute("cardTypes")
    public Map<String, String> cardTypes() {
        Map<String, String> brands = (Map<String, String>) getContextAttribute("cardTypes_cached" + locale());
        if (brands == null) {
            brands = new LinkedHashMap<>();
            brands.put("", "");

            try {
                List<Setting> sl = ServiceLocator.getInstance().getConfigService().getSettingsByIdStart("cardType");
                for (int i = 0; i < sl.size(); i++) {
                    Setting ct = sl.get(i);
                    brands.put(ct.getValue(), localizationService.getText("cardType." + ct.getValue(), locale()));
                }
                setContextAttribute("cardTypes_cached" + locale(), brands);

            } catch (Exception e) {
                log.error("Error getting brands settings", e);
            }
        }
        return brands;
    }

    @ModelAttribute("processor")
    public Processor processor(HttpServletRequest request) {
        if (processor != null) {
            return processor;
        }

        try {
            Processor processor1 = new Processor();
            processor1.setName(persistenceService.getStringSetting(DsSetting.SCHEME_NAME.getKey()));
            processor1.setContactInfo(persistenceService.getStringSetting(DsSetting.SCHEME_CONTACT.getKey()));
            processor1.setStyleSheet(persistenceService.getStringSetting(DsSetting.MNGR_CSS.getKey()));
            processor1.setLogoImage(persistenceService.getStringSetting(DsSetting.SCHEME_LOGO.getKey()));
            processor1.setEmail("scheme.contact.email");
            processor1.setWebSite(persistenceService.getStringSetting(DsSetting.SCHEME_WEBSITE.getKey()));
            processor1.setMaster(true);
            processor = processor1;

        } catch (Throwable pe) {
            log.error("Error populating processor", pe);
        }

        return processor;
    }

    @ModelAttribute("settingsEnumEnabled")
    public Boolean settingsEnumEnabled() {
        return "true".equals(this.getWebContext().getStringSetting(DsSetting.SETTINGS_ENUM_ENABLED.getKey()));
    }

    /**
     * Returns a list of user-accessible payment systems based on user access restriction.
     * Is used for creating new 3DS Components per Payment System.
     */
    @ModelAttribute("paymentSystemComponentMap")
    public Map<String, String> paymentSystemComponentMap() {
        if (currentUser() == null) { // no session
            return null;
        }
        List<PaymentSystem> paymentSystems = paymentSystemsService.getPaymentSystems(currentUser());
        Map<String, String> paymentSystemsMap = new LinkedHashMap<>();
        paymentSystems.forEach(paymentSystem -> paymentSystemsMap.put(String.valueOf(paymentSystem.getId()), paymentSystem.getName()));
        return paymentSystemsMap;
    }

    /**
     * Returns a list of user-accessible payment systems based on user access restriction.
     * Is used in search filters to look up related 3DS Components.
     */
    @ModelAttribute("paymentSystemSearchMap")
    public Map<String, String> paymentSystemSearchMap() {
        if (currentUser() == null) { // no session
            return null;
        }

        Map<String, String> map = new LinkedHashMap<>();
        if (currentUser().getPaymentSystemId() == null) { // current user is a superuser
            map.put(PaymentSystemConstants.UNLIMITED.toString(), getLocalizedMessage("general.all"));
        }

        List<PaymentSystem> paymentSystems = paymentSystemsService.getPaymentSystems(currentUser());
        // For ps-group users this should be filtered by the PS group list of ids.
        Collections.sort(paymentSystems);
        for (PaymentSystem paymentSystem : paymentSystems) {
            map.put(String.valueOf(paymentSystem.getId()), paymentSystem.getName());
        }

        return map;
    }

    @SuppressWarnings("rawtypes")
    public void clearContextAttributeStartsWith(String[] key) {
        if (servletContext != null && key != null && key.length > 0) {
            Enumeration e = servletContext.getAttributeNames();
            while (e != null && e.hasMoreElements()) {
                String an = (String) e.nextElement();
                for (String kx : key) {
                    if (an != null && an.startsWith(kx)) {
                        servletContext.setAttribute(an, null);
                    }
                }
            }
        }
    }

    @ModelAttribute("currentUser")
    protected User currentUser() {
        return AuthorityUtil.getUser();
    }

    protected String getLocalizedMessage(String key) {
        return getLocalizedMessage(key, null);
    }

    protected String getLocalizedMessage(String key, String... params) {
        Locale locale = locale();
        LinkedList<String> localizedParams = new LinkedList<>();
        if (params != null) {
            for (String param : params) {
                localizedParams.add(localizationService.getText(param, locale));
            }
        }
        return localizationService.getText(key, localizedParams.toArray(), locale);
    }

    protected Map<String, String> currencyCodeMap() {
        Map<String, String> currencyMap = (Map<String, String>) getContextAttribute("currencyCodeMap_cached");

        if (currencyMap == null) {
            ISO4217[] codes = ISO4217.values();
            currencyMap = new LinkedHashMap<>();
            currencyMap.put("", "");

            Comparator<ISO4217> cmpc = new Comparator<ISO4217>() {
                private final Collator collator = Collator.getInstance();

                @Override
                public int compare(ISO4217 o1, ISO4217 o2) {
                    return collator.compare(o1.getA3code(), o2.getA3code());
                }
            };

            Arrays.sort(codes, cmpc);

            for (ISO4217 code : codes) {
                currencyMap.put(String.valueOf(code.getNumeric()),
                               code.getA3code() + " (" + Misc.padCutStringLeft("" + code.getNumeric(), '0', 3) + ")");
            }
            setContextAttribute("currencyCodeMap_cached", currencyMap);
        }

        return currencyMap;
    }


    protected Map<String, String> statusMap(String clazz) {
        Map<String, String> statusMap = (Map<String, String>) getContextAttribute("statusMap." + clazz + "." + locale());

        if (statusMap == null) {
            statusMap = new LinkedHashMap<>();
            statusMap.put("", "");

            String tkp = clazz.toLowerCase();
            if (!clazz.startsWith("com.modirum.ds.db.model")) {
                clazz = "com.modirum.ds.db.model.DSModel$" + clazz;
            } else {
                tkp = Misc.replace(clazz, "com.modirum.ds.db.model.DSModel$", "");

            }
            tkp = Misc.replace(tkp, "$", ".").toLowerCase();

            try {
                @SuppressWarnings("rawtypes") Class c = Class.forName(clazz);

                Field[] statusFields = c.getFields();
                for (int i = 0; i < statusFields.length; i++) {
                    try {
                        Object value = statusFields[i].get(null);
                        statusMap.put(String.valueOf(value), localizationService.getText(tkp + "." + value, locale()));
                    } catch (Exception e) {
                        log.error("Roles read error", e);
                    }
                }

            } catch (Exception e) {
                log.error("Error loading statuses for " + clazz, e);
            }

            setContextAttribute("statusMap." + clazz + "." + locale(), statusMap);
        }

        return statusMap;
    }

    protected Map<String, String> inCertificateMap() {
        Map<String, String> certMap = (Map<String, String>) getContextAttribute("inCertificateList_cached." + locale());

        if (certMap == null) {
            certMap = new LinkedHashMap<>();
            certMap.put("", "");
            try {
                List<CertificateData> cerList = this.persistenceService.getPersitableList(CertificateData.class, "name",
                                                                                          null);
                for (CertificateData cx : cerList) {
                    certMap.put(String.valueOf(cx.getId()),
                                cx.getName() + ":" + this.getWebContext().formatDateOnly(cx.getExpires()) + ":" + " " +
                                localizationService.getText("sertificatedata.status" + "." + cx.getStatus() + "",
                                                            locale()));
                }
            } catch (Exception e) {
                log.error("Error loading in certificate list", e);
            }
            setContextAttribute("inCertificateList_cached." + locale(), certMap);
        }

        return certMap;
    }

    protected Map<String, String> outCertificateMap() {
        Map<String, String> certMap = (Map<String, String>) getContextAttribute("outCertificateList_cached." + locale());

        if (certMap == null) {
            certMap = new LinkedHashMap<>();
            certMap.put("", "");
            try {
                List<KeyData> cerList = this.persistenceService.getKeyDatasByAlias(KeyService.KeyAlias.clientAuthCert,
                                                                                   KeyData.KeyStatus.working,
                                                                                   "keyAlias", null, null, null, null);
                for (KeyData cx : cerList) {
                    certMap.put(String.valueOf(cx.getId()),
                                cx.getKeyAlias() + " " + this.getWebContext().formatDateOnly(cx.getKeyDate()) + "-" +
                                this.getWebContext().formatDateOnly(new java.util.Date(
                                        cx.getKeyDate().getTime() + cx.getCryptoPeriodDays() * 24L * 3600L * 1000L)));
                }
            } catch (Exception e) {
                log.error("Error loading out certificate list", e);
            }
            setContextAttribute("outCertificateList_cached." + locale(), certMap);
        }
        return certMap;
    }

    protected Map<String, String> protocolVersionsMap(boolean withempty) {
        Map<String, String> versionsMap = (Map<String, String>) getContextAttribute("protocolVersionsMap.we" + withempty);

        if (versionsMap == null) {
            versionsMap = new HashMap<>();
            if (withempty) {
                versionsMap.put("", "");
            }

            String pse = cnfs.getStringSetting(DsSetting.DS_VERSIONS_ENABLED.getKey());

            for (TDSModel.MessageVersion mvx : TDSModel.MessageVersion.values()) {
                if (pse != null && (pse.contains("UIALL") || pse.contains(mvx.value()))) {
                    versionsMap.put(mvx.value(), mvx.value());
                }
            }

            // set default if none configured
            if (withempty && versionsMap.size() < 2 || !withempty && versionsMap.size() < 1) {
                for (TDSModel.MessageVersion mvx : TDSModel.MessageVersion.values()) {
                    versionsMap.put(mvx.value(), mvx.value());
                }
            }
            setContextAttribute("protocolVersionsMap.we" + withempty, versionsMap);
        }

        return versionsMap;
    }

    protected void resetMaps() {
        clearContextAttributeStartsWith(
                new String[]{"secondFactorMap", "allRolesMap", "cardTypes_cached", "statusMap", "protocolVersionsMap"});
    }

    protected int getBinLength() throws Exception {
        String binLength = persistenceService.getStringSetting(MNGR_BIN_LENGTH.getKey());
        if (Misc.isNotNullOrEmpty(binLength) && Misc.isInt(binLength) && Misc.parseInt(binLength) > 0) {
            return Misc.parseInt(binLength);
        } else {
            return 6;
        }
    }
}


