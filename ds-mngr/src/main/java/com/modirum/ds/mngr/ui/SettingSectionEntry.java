package com.modirum.ds.mngr.ui;

import java.util.List;

public class SettingSectionEntry {
    private String description;
    private String name;
    private List<SettingItem> settingItems;

    public String getDescription() {
        return description;
    }

    public SettingSectionEntry setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getName() {
        return name;
    }

    public SettingSectionEntry setName(String name) {
        this.name = name;
        return this;
    }

    public List<SettingItem> getSettingItems() {
        return settingItems;
    }

    public SettingSectionEntry setSettingItems(List<SettingItem> settingItems) {
        this.settingItems = settingItems;
        return this;
    }
}
