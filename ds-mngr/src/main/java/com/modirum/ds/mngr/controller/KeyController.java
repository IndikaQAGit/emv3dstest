package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.Roles;
import com.modirum.ds.mngr.support.AuthorityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping({"/keys"})
public class KeyController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(KeyController.class);

    @RequestMapping("/keySetupList")
    public ModelAndView keySetupList(HttpServletRequest request) {
        if (!AuthorityUtil.hasRole(Roles.keyManage)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("keys/keySetupList");
        return mav;
    }
}