/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on Mar 6, 2017
 *
 */
package com.modirum.ds.mngr.support;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.User;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.utils.http.SSLSocketHelper;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.ModirumIDAuthFunctions;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.ModirumIDAuthInterface;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.CheckMessageRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.CheckMessageResponse;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.GenerateMdidUserIdRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.GenerateMdidUserIdResponse;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.IsEnrolledRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.IsEnrolledResponse;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.Message;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.NewMessageRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.NewMessageResponse;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.NewRegistrationCodeRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.NewRegistrationCodeResponse;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.RejectMessageRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.RejectMessageResponse;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.VerifyRequest;
import com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.VerifyResponse;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.Misc;
import com.modirum.ds.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.xml.ws.BindingProvider;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.UUID;

public class ModirumIDASClientService {
    static Logger log = LoggerFactory.getLogger(ModirumIDASClientService.class);
    final static int WSTODEFAULT = 15000;
    ModirumIDAuthFunctions aifws;

    private final CryptoService cryptoService = ServiceLocator.getInstance().getCryptoService();

    public interface MessageStatus {

        String cApproved = "approved";
        String cRejected = "rejected";
        String cPending = "pending";

    }

    public ModirumIDAuthFunctions createWS(Context ctx, int wstimeout) throws Exception {
        String wsUrl = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_AS_WS_URL.getKey());

        if (aifws != null && wsUrl.equals(
                ((BindingProvider) aifws).getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY))) {
            return aifws;
        }

        String sslContext = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_SSL_CLIENT_PROTO.getKey());
        if (Misc.isNullOrEmpty(sslContext)) {
            sslContext = SSLSocketHelper.SSLProto.DEFAULT;
        }

        String keyId = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_SSL_CLIENT_KEY_CERTID.getKey());
        KeyData keyAndChain = null;
        if (Misc.isInt(keyId)) {
            keyAndChain = ServiceLocator.getInstance().getKeyService().getPrivateKeyAndCertChainById(
                    Misc.parseLongBoxed(keyId));
        }

        PrivateKey sslAuthKey = null;
        X509Certificate[] clientCertChain = null;
        if (keyAndChain != null) {
            byte[] privateKeyBytes  = cryptoService.unwrapPrivateKey(keyAndChain);
            HSMDevice hsmDevice = ServiceLocator.getInstance().getHSMDevice(keyAndChain.getHsmDeviceId());
            sslAuthKey = hsmDevice.toRSAprivateKey(privateKeyBytes);
            String certChainPem = KeyService.parsePEMChain(keyAndChain.getKeyData());
            clientCertChain = KeyService.parseX509Certificates(certChainPem.getBytes(StandardCharsets.ISO_8859_1));
        }

        KeyStore sslTrustStore = getSSLTruststore(ctx);
        String keyManagerFactoryType = getKeyManagerFactoryType(ctx);

        ModirumIDAuthInterface rs = new ModirumIDAuthInterface(
                ModirumIDAuthInterface.class.getClassLoader().getResource("modirumid-auth-interface.wsdl"));
        ModirumIDAuthFunctions aifwsl = rs.getModirumIDAuthFunctions();

        Map<String, Object> requestContext = ((BindingProvider) aifwsl).getRequestContext();

        boolean trustAny = "true".equals(ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_SSL_TRUSTANY.getKey()));
        log.info("Set service endpoint URL to: " + wsUrl);
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsUrl);
        requestContext.put("com.sun.xml.internal.ws.request.timeout", wstimeout); // Timeout in millis
        requestContext.put("com.sun.xml.internal.ws.connect.timeout", wstimeout); // Timeout in millis
        requestContext.put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory",
                           SSLSocketHelper.initFactory(sslContext, keyManagerFactoryType, sslAuthKey, clientCertChain,
                                                       sslTrustStore, trustAny, null, null));

        String sslHostVerify = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_SSL_HOST_VERIFY.getKey());
        if ("false".equals(sslHostVerify)) {
            requestContext.put("com.sun.xml.internal.ws.transport.https.client.hostname.verifier",
                               new SSLSocketHelper.OkHostnameVerifier());
        }

        aifws = aifwsl;

        return aifws;
    }

    public String getNewMDIDUserId(User user, String hostname, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx));

        GenerateMdidUserIdRequest wsreq = new GenerateMdidUserIdRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + Misc.merge(hostname, user.getLoginname()));

        long s = System.currentTimeMillis();
        GenerateMdidUserIdResponse wsres = aifws.generateMdidUserId(wsreq);
        long e = System.currentTimeMillis();
        log.info(user.getLoginname() + " generateMdidUserId in " + (e - s) + " ms");
        String m = isOk(wsres);
        if (m == null) {
            return wsres.getMdidUserId();
        }

        throw new RuntimeException(m);
    }

    public String getNewRegistrationCode(User user, String hostname, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx));
        String midus = getNewMDIDUserId(user, hostname, ctx);
        user.setFactor2Data2(midus);
        //ServiceLocator.getInstance().getPersistenceService().saveOrUpdate(user);

        NewRegistrationCodeRequest wsreq = new NewRegistrationCodeRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + Misc.merge(hostname, user.getLoginname()));

        wsreq.setMdidUserId(midus);
        String isid = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_ISSUER_ID.getKey());
        wsreq.setIssuerId(Misc.isNullOrEmpty(isid) ? "-1" : isid);
        //wsreq.setUrl(callbackUrl);
        long s = System.currentTimeMillis();
        NewRegistrationCodeResponse wsres = aifws.newRegistrationCode(wsreq);
        long e = System.currentTimeMillis();
        log.info(user.getLoginname() + " newRegistrationCode in " + (e - s) + " ms");
        String m = isOk(wsres);
        if (m == null) {
            return wsres.getRegistrationCode();
        }

        throw new RuntimeException(m);
    }

    public boolean getIsEnrolledStatus(User user, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx));
        String midus = user.getFactor2Data2();

        IsEnrolledRequest wsreq = new IsEnrolledRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + user.getLoginname());
        wsreq.setMdidUserId(midus);
        long s = System.currentTimeMillis();
        IsEnrolledResponse wsres = aifws.isEnrolled(wsreq);
        long e = System.currentTimeMillis();
        String m = isOk(wsres);
        if (m == null) {
            log.info(user.getLoginname() + " IsEnrolledStatus=" + wsres.isEnrolled() + " in " + (e - s) + " ms");
            return wsres.isEnrolled();
        }

        throw new RuntimeException(m);
    }


    public boolean verifyOtp(User user, String otp, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx) + 1000);
        VerifyRequest wsreq = new VerifyRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + user.getLoginname());
        wsreq.setMdidUserId(user.getFactor2Data2());
        wsreq.setMethod("PIN");
        wsreq.setOtp(otp);
        long s = System.currentTimeMillis();
        VerifyResponse wsres = aifws.verify(wsreq);
        long e = System.currentTimeMillis();
        String m = isOk(wsres);
        if (m == null) {
            log.info(user.getLoginname() + " otp verified=" + wsres.isVerified() + " in " + (e - s) + " ms");
            return wsres.isVerified();
        }

        throw new RuntimeException(m);
    }


    public Message createMessage(User user, String msg, String hostname, Context ctx) {
        Message m = new Message();
        m.setSignData("%%SIGNDATA_EN%%=" + msg);
        m.setMdidUserId(user.getFactor2Data2());
        m.setMessageId(UUID.randomUUID().toString());
        m.setTransactionId(UUID.randomUUID().toString());
        m.setSessionId("");
        //m.setChallenge("");

        return m;
    }

    public Message sendMessage(User user, String msg, String hostname, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx));
        NewMessageRequest wsreq = new NewMessageRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + Misc.merge(hostname, user.getLoginname()));
        wsreq.setMdidUserId(user.getFactor2Data2());
        Message m = createMessage(user, msg, hostname, ctx);
        wsreq.setTransactionId(m.getTransactionId());
        wsreq.setSignData(m.getSignData());
        wsreq.setSessionId(m.getSessionId());
        //wsreq.setUrl("");

        String isid = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_ISSUER_ID.getKey());
        wsreq.setIssuerId(Misc.isNullOrEmpty(isid) ? "-1" : isid);
        //wsreq.setUrl(callbackUrl);
        long s = System.currentTimeMillis();
        NewMessageResponse wsres = aifws.newMessage(wsreq);
        long e = System.currentTimeMillis();
        log.info(user.getLoginname() + " sendMessage " + m.getMessageId() + " in " + (e - s) + " ms");
        String rm = isOk(wsres);
        if (rm == null) {
            return wsres.getMessage();
        }

        throw new RuntimeException(rm);
    }

    public String checkMessage(User user, Message msg, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx));
        CheckMessageRequest wsreq = new CheckMessageRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + user.getLoginname());
        wsreq.setMessage(msg);

        //String isid=ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_ISSUER_ID.getKey());
        //wsreq.setIssuerId(Misc.isne(isid) ? "-1" : isid);
        //wsreq.setUrl(callbackUrl);
        long s = System.currentTimeMillis();
        CheckMessageResponse wsres = aifws.checkMessage(wsreq);
        long e = System.currentTimeMillis();
        log.info(user.getLoginname() + " checkMessage " + msg.getMessageId() + " status:" + wsres.getMessageStatus() +
                 " in " + (e - s) + " ms");
        String rm = isOk(wsres);
        if (rm == null) {
            return wsres.getMessageStatus();
        }

        throw new RuntimeException(rm);
    }

    public boolean rejectMessage(User user, Message msg, Context ctx) throws Exception {
        createWS(ctx, getWSTimeout(ctx));
        RejectMessageRequest wsreq = new RejectMessageRequest();
        fillBaseRequestImpl(wsreq);
        wsreq.setClientDetail(ApplicationControl.getAppName() + " " + user.getLoginname());
        wsreq.setMessage(msg);

        long s = System.currentTimeMillis();
        RejectMessageResponse wsres = aifws.rejectMessage(wsreq);
        long e = System.currentTimeMillis();
        log.info(user.getLoginname() + " rejectMessage " + msg.getMessageId() + " in " + (e - s) + " ms");
        String rm = isOk(wsres);
        if (rm == null) {
            return true;
        }

        throw new RuntimeException(rm);
    }


    int getWSTimeout(Context ctx) {
        int to = ctx.getIntSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_WS_TIMEOUT.getKey());
        if (to >= 3000 && to <= 30000) {
            return to;
        }

        return WSTODEFAULT;

    }

    KeyStore sslTrustStore = null;
    String sslTrustCertsLd;

    private KeyStore getSSLTruststore(Context ctx) throws Exception {
        String sslTrustCertsSe = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_SSL_TRUSTED_CERTS.getKey());
        if (Misc.isNotNullOrEmpty(sslTrustCertsSe) && !sslTrustCertsSe.equals(sslTrustCertsLd)) {
            log.info("Loading ModirumIDASClient SSL trusted certs from setting ModirumIDASClient.sslTrustedCerts");
            sslTrustCertsLd = sslTrustCertsSe;
            KeyStore keystore = KeyStore.getInstance("JKS");
            keystore.load(null, null);
            X509Certificate[] roots = KeyService.parseX509Certificates(
                    sslTrustCertsSe.getBytes(StandardCharsets.ISO_8859_1), "X509");
            for (X509Certificate xc : roots) {
                keystore.setCertificateEntry(xc.getSubjectDN().getName(), xc);
            }
            log.info("Loading ModirumIDASClientService SSL trusted certs " + roots.length + " done..");
            sslTrustStore = keystore;
        }

        return sslTrustStore;
    }

    private String getKeyManagerFactoryType(Context ctx) {
        String keyManagerFactoryType = ctx.getStringSetting(DsSetting.MODIRUM_ID_AS_CLIENT_SERVICE_SSL_KEYMANAGERFACTORY_TYPE.getKey());
        if (Misc.isNullOrEmpty(keyManagerFactoryType)) {
            keyManagerFactoryType = KeyManagerFactory.getDefaultAlgorithm();
        }
        return keyManagerFactoryType;
    }

    protected void fillBaseRequestImpl(com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.BaseRequest wsBaseRequest) {
        wsBaseRequest.setClientName(ApplicationControl.getAppName());
        wsBaseRequest.setClientVersion(MdmVersionService.getBuildVersion());
        wsBaseRequest.setClientDetail(ApplicationControl.getAppName());
        wsBaseRequest.setClientTxId(UUID.randomUUID().toString());
        wsBaseRequest.setDebugMsg("Modirum DS Manager " + Utils.getLocalNonLoIp());
    }


    protected String isOk(com.modirum.mdpay.modirumid.as.auth.modirumidauthinterface.types.BaseResponse response) throws Exception {
        if (response != null) {
            if (response.getECode().intValue() != 0) {
                return ("Failed ec=" + response.getECode() + " " + response.getEDescription() + " esc=" +
                        response.getSubCode());
            }
        } else {
            return ("Invalid response (null)");
        }

        return null;
    }
}
