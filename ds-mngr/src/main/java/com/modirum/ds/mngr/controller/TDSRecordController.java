package com.modirum.ds.mngr.controller;

import com.modirum.ds.enums.DsSetting;
import com.modirum.ds.enums.PaymentSystemConstants;
import com.modirum.ds.enums.Roles;
import com.modirum.ds.enums.TransactionAttribute;
import com.modirum.ds.jdbc.core.model.TdsRecordAttribute;
import com.modirum.ds.json.JsonUtil;
import com.modirum.ds.mngr.service.TdsRecordExportService;
import com.modirum.ds.mngr.support.AuthorityUtil;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.db.model.KeyData;
import com.modirum.ds.db.model.PaymentSystem;
import com.modirum.ds.db.model.Setting;
import com.modirum.ds.db.model.TDSMessageData;
import com.modirum.ds.model.TDSModel;
import com.modirum.ds.db.model.TDSRecord;
import com.modirum.ds.db.model.TDSRecordSearcher;
import com.modirum.ds.services.AcquirerService;
import com.modirum.ds.services.CryptoService;
import com.modirum.ds.services.KeyService;
import com.modirum.ds.services.IssuerService;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.services.MessageService;
import com.modirum.ds.services.ServiceLocator;
import com.modirum.ds.services.TdsRecordAttributeService;
import com.modirum.ds.utils.DateUtil;
import com.modirum.ds.utils.Misc;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Controller
@RequestMapping("/tdsrecords")
public class TDSRecordController extends BaseController {
    private final transient Logger log = LoggerFactory.getLogger(TDSRecordController.class);

    @Autowired
    private TdsRecordExportService tdsRecordExportService;
    private JsonMessageService jsonMessageService = ServiceLocator.getInstance().getJsonMessageService();
    private AcquirerService acquirerService = ServiceLocator.getInstance().getAcquirerService();
    private IssuerService issuerService = ServiceLocator.getInstance().getIssuerService();
    @Autowired
    private KeyService keyService;
    @Autowired
    private TdsRecordAttributeService tdsRecordAttributeService;

    public TDSRecordController() {
    }

    @ModelAttribute("protoVersionList")
    public Map<String, String> protoVersionMap() {
        return protocolVersionsMap(true);
    }


    @ModelAttribute("tdsstatuses")
    public Map<String, String> tdsstatuses() {
        java.util.Map<String, String> arstatuses = new java.util.LinkedHashMap<String, String>();
        Field[] sFields = DSModel.TSDRecord.Status.class.getFields();
        arstatuses.put("", "");
        for (int i = 0; i < sFields.length; i++) {
            try {
                if (Modifier.isPublic(sFields[i].getModifiers())) {
                    Object st = sFields[i].get(null);
                    arstatuses.put(String.valueOf(st), localizationService.getText("ar.localstatus." + st, locale()));
                }
            } catch (Exception e) {
                log.error("arstatuses read error", e);
            }

        }

        return arstatuses;
    }

    @ModelAttribute("tdsauthstatuses")
    public Map<String, String> tdsauthstatuses() {
        java.util.Map<String, String> arstatuses = new java.util.LinkedHashMap<String, String>();
        TDSModel.XtransStatus[] xtt = TDSModel.XtransStatus.values();
        arstatuses.put("", "");
        for (int i = 0; i < xtt.length; i++) {
            String st = xtt[i].value();
            arstatuses.put(st, localizationService.getText("ar.authstatus." + st, locale()));
        }

        return arstatuses;
    }

    @ModelAttribute("deviceChannels")
    public Map<String, String> deviceChannels() {
        java.util.Map<String, String> arstatuses = new java.util.LinkedHashMap<String, String>();
        TDSModel.DeviceChannel[] xtt = TDSModel.DeviceChannel.values();
        arstatuses.put("", "");
        for (int i = 0; i < xtt.length; i++) {
            String st = xtt[i].value();
            arstatuses.put(st, localizationService.getText("ar.deviceChannel." + st, locale()));
        }

        return arstatuses;
    }


    @ModelAttribute("merchantCountries")
    public Map<String, String> merchantCountries() {
        return super.countriesMap();
    }

    @ModelAttribute("issuersMap")
    public Map<String, String> issuersMap() {
        return issuerService.getIssuerDropdownMap(currentUser().getPaymentSystemId());
    }

    @ModelAttribute("acquirersMap")
    public Map<String, String> acquirersMap() {
        return acquirerService.getAcquirerDropdownMap(locale(), currentUser().getPaymentSystemId());
    }

    @ModelAttribute("currencyCodeMap")
    public Map<String, String> currencyCodeMap() {
        return super.currencyCodeMap();
    }

    @RequestMapping("/tdsrecords.html")
    public ModelAndView tdsrecords(HttpServletRequest request, @ModelAttribute(value = "s") TDSRecordSearcher searcher, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.recordsView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();
        searcher.setPaymentSystemId(resolvePaymentSystemId(searcher.getPaymentSystemId()));

        ModelAndView mav = new ModelAndView("tdsrecords/tdsrecords");
        mav.addObject("binLength", getBinLength());
        mav.addObject("recordRowLimitForCsvExport", getTdsRecordRowLimitForCsvExport());
        mav.addObject("acquirersDropdown", acquirerService.getAcquirerDropdownMap(locale(), searcher.getPaymentSystemId()));
        mav.addObject("issuersDropdown", issuerService.getIssuerDropdownMap(searcher.getPaymentSystemId()));
        if ("refreshPaymentSystem".equals(request.getParameter("cmd"))) {
            searcher.setLocalIssuerId(null);
            searcher.setLocalAcquirerId(null);
            buildSearchResult(request, searcher, mav);
            return mav;
        }

        String action = request.getParameter("action");
        if ("search".equalsIgnoreCase(action)) {

            buildSearchResult(request, searcher, mav);

            if (Misc.isNotNullOrEmpty(searcher.getMerchantName()) && searcher.getMerchantName().trim().length() < 2) {
                result.addError(new FieldError("s", "merchantName", searcher.getMerchantName(), false, null, null,
                                               localizationService.getText("err.min.length", "" + 2, locale())));
            }

            if (!result.hasErrors()) {
                List<TDSRecord> found = persistenceService.getTDSRecordsByQuery(searcher, true);
                mav.addObject("found", found);
            }

        } else {
            if (Misc.isNullOrEmpty(searcher.getLimit())) {
                searcher.setLimit(25);
            }
            if (Misc.isNullOrEmpty(searcher.getOrder())) {
                searcher.setOrder("localDateStart");
                searcher.setOrderDirection("desc");
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Authrecords list done in {} ms", System.currentTimeMillis() - start);
        }

        return mav;
    }

    @RequestMapping("/export.html")
    public Object exportTdsRecords(HttpServletRequest request, HttpServletResponse response, @ModelAttribute(value = "s") TDSRecordSearcher searcher, BindingResult result) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.recordsView)) {
            return unauthorized(request);
        }
        long start = System.currentTimeMillis();
        searcher.setPaymentSystemId(resolvePaymentSystemId(searcher.getPaymentSystemId()));

        ModelAndView mav = new ModelAndView();
        buildSearchResult(request, searcher, mav);
        searcher.setStart(0);
        searcher.setLimit(getTdsRecordRowLimitForCsvExport());

        try {
            response.addHeader(HttpHeaders.CONTENT_TYPE, "text/csv;charset=UTF-8");
            String csvFilename = "ds-tx-export-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".csv";
            response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + csvFilename+ "\"");
            response.addHeader(HttpHeaders.EXPIRES, "0");
            this.tdsRecordExportService.writeCsv(response.getWriter(),
                                                 persistenceService.getTDSRecordsIdForExport(searcher),
                                                 getTimeZone(),
                                                 locale());
        } catch (Exception e) {
            log.error("Error encounter when exporting auth records", e);
        }

        if (log.isDebugEnabled()) {
            log.debug("Exporting Authrecords list done in {} ms", System.currentTimeMillis() - start);
        }

        return ResponseEntity.ok()
                             .build();
    }

    private int getTdsRecordRowLimitForCsvExport() {
        int rowLimit = getWebContext().getIntSetting(DsSetting.TDS_RECORD_CSV_EXPORT_LIMIT.getKey());
        return rowLimit > 0 ? rowLimit : 5000;
    }


    private void buildSearchResult(HttpServletRequest request, TDSRecordSearcher searcher, ModelAndView mav) {
        if (searcher.getTrDateFrom() != null) {
            final String fromHourParamName = "trDateFromHH";
            final Integer fromHour = Misc.parseIntBoxed(request.getParameter(fromHourParamName));
            if (DateUtil.isValidHour(fromHour)) {
                searcher.setTrDateFrom(DateUtils.addHours(searcher.getTrDateFrom(), fromHour));
                mav.addObject(fromHourParamName, fromHour);
            }

            final String fromMinuteParamName = "trDateFrommm";
            final Integer fromMinute = Misc.parseIntBoxed(request.getParameter(fromMinuteParamName));
            if (DateUtil.isValidMinute(fromMinute)) {
                searcher.setTrDateFrom(DateUtils.addMinutes(searcher.getTrDateFrom(), fromMinute));
                mav.addObject(fromMinuteParamName, fromMinute);
            }
        }

        if (searcher.getTrDateTo() != null) {
            java.util.Calendar dateCal = DateUtils.toCalendar(searcher.getTrDateTo(), getTimeZone());
            Date toDate = DateUtil.normalizeDate(dateCal).getTime();
            final String toHourParamName = "trDateToHH";
            final Integer toHour = Misc.parseIntBoxed(request.getParameter(toHourParamName));
            if (DateUtil.isValidHour(toHour)) {
                toDate = DateUtils.addHours(toDate, toHour);
                mav.addObject(toHourParamName, toHour);

            }

            final String toMinuteParamName = "trDateTomm";
            final Integer toMinute = Misc.parseIntBoxed(request.getParameter(toMinuteParamName));
            if (DateUtil.isValidMinute(toMinute)) {
                toDate = DateUtils.addMinutes(toDate, toMinute);
                mav.addObject(toMinuteParamName, toMinute);
            }

            if (DateUtil.isValidHour(toHour) || DateUtil.isValidMinute(toMinute)) {
                searcher.setTrDateTo(toDate);
            } else {
                // Default value is end of day.
                searcher.setTrDateTo(DateUtil.normalizeDateDayEnd(dateCal).getTime());
            }
        }

        if (searcher.getStart() == null) {
            searcher.setStart(0);
        }
        if (searcher.getLimit() == null) {
            searcher.setLimit(25);
        }

        mav.addObject("searcher", searcher);
    }

    @RequestMapping("/tdsrecord.html")
    public ModelAndView viewRecord(@RequestParam("id") Long id, HttpServletRequest request) throws Exception {
        TDSRecord authrecord = persistenceService.getPersistableById(id, TDSRecord.class);
        tdsRecordAttributeService.setTDSRecordAttributes(authrecord);
        if (!AuthorityUtil.isAccessAllowed(authrecord, Roles.recordsView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("tdsrecords/tdsrecord");
        mav.addObject("authrecord", authrecord);
        if (authrecord != null) {
            if (authrecord.getPaymentSystemId() != null) {
                PaymentSystem paymentSystem = persistenceService.getPersistableById(authrecord.getPaymentSystemId(),
                        PaymentSystem.class);
                if (paymentSystem != null) {
                    mav.addObject("paymentSystemName", paymentSystem.getName());
                }
            }
            boolean parsed = false; //whether deviceInfo could be parsed (and was longer than 10)
            List<TDSMessageData> tdsMessageDatas = persistenceService.getTDSMessagesByRecordId(authrecord.getId());
            List<TdsRecordAttribute> tdsRecordAttributeList = tdsRecordAttributeService.getRecordAttributesByTDSRecordId(
                    authrecord.getId());
            decryptAuthValuesToDisplay(tdsRecordAttributeList);
            for (TDSMessageData md : tdsMessageDatas) {
                String decryptedContent = cryptoService.decryptData(md.getContents());
                md.setContents(decryptedContent);

                String contentsFormatted = JsonUtil.isJsonValid(decryptedContent) ?
                                           jsonMessageService.parse(decryptedContent).toPrettyString() :
                                           decryptedContent;
                if (hasDeviceInfo(md)) {
                    int startIndex = contentsFormatted.indexOf("deviceInfo");
                    startIndex += 11;
                    startIndex = contentsFormatted.indexOf("\"", startIndex) + 1;
                    int endIndex = contentsFormatted.indexOf("\"", startIndex);
                    if (startIndex > -1 && (endIndex - startIndex >=
                                            11)) //if deviceInfo was shorter than 11 its assumed to be problematic. it wont be masked and no button will be displayed
                    {
                        md.setContents(Misc.mask(contentsFormatted, startIndex + 5, contentsFormatted.length() - endIndex + 5));
                        parsed = true;
                    }

                } else {
                    md.setContents(contentsFormatted);
                }
            }
            mav.addObject("tdsrecordattributes", tdsRecordAttributeList);
            mav.addObject("tdsmessages", tdsMessageDatas);

            tdsRecordAttributeList.stream()
                                  .filter(txAttr -> TransactionAttribute.ERROR_DESCRIPTION.getKey().equals(txAttr.getKey()))
                                  .findFirst()
                                  .ifPresent(txAttr -> authrecord.setErrorDescription(txAttr.getAsciiValue()));

            tdsRecordAttributeList.stream()
                                  .filter(txAttr -> TransactionAttribute.ERROR_RESOLUTION.getKey().equals(txAttr.getKey()))
                                  .findFirst()
                                  .ifPresent(txAttr -> authrecord.setErrorResolution(txAttr.getAsciiValue()));

            if (AuthorityUtil.hasRole(Roles.panView) && AuthorityUtil.getUser().isFactor2Passed()) {
                mav.addObject("viewpan", Boolean.TRUE);
            }
            if (AuthorityUtil.hasRole(Roles.recordsView) && AuthorityUtil.getUser().isFactor2Passed() && parsed) {
                mav.addObject("viewDevInfo", Boolean.TRUE);
            }

        }

        return mav;
    }

    @RequestMapping("/viewpan.html")
    public ModelAndView viewPan(@RequestParam("id") Long id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html");
        if (!AuthorityUtil.hasRole(Roles.panView)) {
            unauthorized(request);
            response.getWriter().write("<html><head></head><body>ERROR No privilege</body></html>");
            return null;
        }
        if (!AuthorityUtil.getUser().isFactor2Passed()) {
            request.setAttribute("error", localizationService.getText("err.2ndfactor.required.for.panview", locale()));
            unauthorized(request);
            response.getWriter().write("<html><head></head><body>ERROR No 2nd factor</body></html>");
            return null;
        }

        TDSRecord authrecord = persistenceService.getPersistableById(id, TDSRecord.class);
        ModelAndView mav = new ModelAndView("tdsrecords/pantdsrecord");
        mav.addObject("authrecord", authrecord);
        if (authrecord != null && Misc.isNotNullOrEmpty(authrecord.getAcctNumberEnc())) {
            try {
                KeyData keyData = keyService.getSecretKey(authrecord.getAcctNumberKid(), CryptoService.DATAENCALG);
                if (keyData != null) {
                    String pan = cryptoService.decrypt(authrecord.getAcctNumberEnc(), keyData);
                    auditLogService.logAction(AuthorityUtil.getUser(), authrecord, DSModel.AuditLog.Action.VIEW,
                                              "View full pan of record " + authrecord.getId());
                    response.getWriter().write("<html><head></head><body>VALUE " + pan + "</body></html>");
                } else {
                    response.getWriter().write("<html><head></head><body>ERROR No key</body></html>");
                }
            } catch (Exception e) {
                log.error("PAN decryption failure, recorde id " + authrecord.getId(), e);
                response.getWriter().write("<html><head></head><body>ERROR Decrypt failed");
            }
        } else {
            response.getWriter().write("<html><head></head><body>ERROR No data</body></html>");
        }

        return null;
    }

    @RequestMapping("/viewDeviceInfo.html")
    public ModelAndView viewDeviceInfo(@RequestParam("id") Long id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        if (!AuthorityUtil.hasRole(Roles.recordsView)) {
            unauthorized(request);
            response.getWriter().write("<html><head></head><body>ERROR No privilege</body></html>");
            return null;
        }

        if (!AuthorityUtil.getUser().isFactor2Passed()) {
            request.setAttribute("error",
                                 localizationService.getText("err.2ndfactor.required.for.devinfoview", locale()));
            unauthorized(request);
            response.getWriter().write("<html><head></head><body>ERROR No 2nd factor</body></html>");
            return null;
        }

        List<TDSMessageData> tdsMessages = persistenceService.getTDSMessagesByRecordId(id);

        for (TDSMessageData md : tdsMessages) {
            String decryptedContent = cryptoService.decryptData(md.getContents());
            md.setContents(decryptedContent);

            if (hasDeviceInfo(md)) {
                String devInfo;
                String contents = md.getContents();
                int startIndex = contents.indexOf("deviceInfo");
                startIndex += 11;
                startIndex = contents.indexOf("\"", startIndex) + 1;
                int endIndex = contents.indexOf("\"", startIndex);
                if (startIndex > -1 && (endIndex > startIndex)) {
                    devInfo = contents.substring(startIndex, endIndex);
                    auditLogService.logAction(AuthorityUtil.getUser(), md, DSModel.AuditLog.Action.VIEW,
                                              "Viewed full deviceInfo of message belonging to auth record " +
                                              md.getTdsrId());
                } else {
                    devInfo = "Invalid deviceInfo";//this wont happen normally
                }
                response.getWriter().write("<html><head></head><body>" + devInfo + "</body></htm>");

            }
        }

        return null;
    }

    private boolean hasDeviceInfo(TDSMessageData md) {
        return TDSModel.XmessageType.A_REQ.value().equals(md.getMessageType()) && md.getDestIP() != null &&
               md.getSourceIP() != null && md.getContents() != null && md.getDestIP().startsWith("ACS") &&
               md.getSourceIP().startsWith("DS") && md.getContents().contains("\"deviceInfo\"");
    }

    @RequestMapping("/metrics.html")
    public ModelAndView metrics(HttpServletRequest request) {
        if (!isSuperuser() || !AuthorityUtil.hasRole(Roles.recordsView)) {
            return unauthorized(request);
        }

        ModelAndView mav = new ModelAndView("tdsrecords/metrics");

        @SuppressWarnings("unchecked") Map<String, List<String>> timings = (Map<String, List<String>>) this.getContextAttribute("timings");
        Long timingsTx = (Long) this.getContextAttribute("timingsts");

        if (timings == null || timingsTx == null || timingsTx < System.currentTimeMillis() - 15000) {
            timings = loadTimings(false);
            this.setContextAttribute("timings", timings);
            this.setContextAttribute("timingsts", System.currentTimeMillis());
        }


        mav.addObject("xtimings", timings);

        long unit = 60000;
        long start = System.currentTimeMillis() - 15 * unit;
        List<Long> dates = new java.util.ArrayList<>(15);
        for (int i = 0; i < 6; i++) {
            dates.add(new Long(start + i * unit));
        }

        mav.addObject("start", dates.get(0));
        mav.addObject("end", dates.get(dates.size() - 1));

        return mav;
    }

    @RequestMapping("/metricsLastData.html")
    public ModelAndView metricsLastData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (!AuthorityUtil.hasRole(Roles.recordsView)) {
            return unauthorized(request);
        }

        Map<String, List<String>> timings = loadTimings(true);
        response.setContentType(MessageService.CT_JSON_UTF8);

        Writer w = response.getWriter();
        w.write("{");
        for (Map.Entry<String, List<String>> me : timings.entrySet()) {
            w.write("\"" + me.getKey() + "\" :");
            w.write(me.getValue().get(0));
        }
        w.write("}");
        return null;
    }

    String[] xtimingsnames = {"aReqTimings", "pReqTimings", "rReqTimings"};

    public Map<String, List<String>> loadTimings(boolean lastOnly) {
        Map<String, List<String>> dataMap = new java.util.LinkedHashMap<String, List<String>>();
        //"DS.timings." + getDSID()+"."+
        try {
            List<Setting> intancXTs = this.persistenceService.getSettingsByIdStart("DS.timings.%",
                                                                                   lastOnly ? "lastModifieddesc" : "lastModifiedasc",
                                                                                   lastOnly ? 10 : null);

            for (Setting s : intancXTs) {
                String instance = s.getKey().substring(("DS.timings.").length());
                if (instance.indexOf('/') > 0) {
                    instance = instance.substring(0, instance.indexOf('/'));
                }

                if (lastOnly && dataMap.containsKey(instance)) {
                    break;
                }
                if (Misc.isNotNullOrEmpty(s.getValue())) {
                    if (lastOnly) {
                        dataMap.put(instance, Arrays.asList(s.getValue()));
                    } else {
                        List<String> vl = dataMap.get(instance);
                        if (vl == null) {
                            vl = new ArrayList<String>(90);
                            dataMap.put(instance, vl);
                        }
                        vl.add(s.getValue());
                    }
                }
            }
        } catch (Exception e) {
            log.warn("Unable to read timings", e);
        }

        return dataMap;
    }
    
    private void decryptAuthValuesToDisplay(List<TdsRecordAttribute> tdsRecordAttributeList) {
        if (tdsRecordAttributeList.size() > 0) {
            for (TdsRecordAttribute tdsRecordAttribute : new ArrayList<TdsRecordAttribute>(tdsRecordAttributeList)) {
                if (StringUtils.equalsIgnoreCase(tdsRecordAttribute.getKey(), TransactionAttribute.AUTHENTICATION_VALUE.getKey())) {
                    try {
                        tdsRecordAttribute.setAsciiValue(cryptoService.decryptData(tdsRecordAttribute
                                .getAsciiValue()));
                    } catch (Exception e) {
                        tdsRecordAttributeList.remove(tdsRecordAttribute);
                        log.error("Unable to decrypt authentication value.", e);
                    }
                }
            }
        }
    }

    private Integer resolvePaymentSystemId(Integer paymentSystemId) {
        if (PaymentSystemConstants.UNLIMITED.equals(paymentSystemId)) {
            return null;
        } else if (!paymentSystemSearchMap().containsKey(String.valueOf(paymentSystemId))) {
            return currentUser().getPaymentSystemId();
        } else {
            return paymentSystemId;
        }

    }
}
