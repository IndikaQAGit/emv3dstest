package com.modirum.ds.mngr.support.csv;

import com.modirum.ds.utils.Misc;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CsvService {
    private final Logger log = LoggerFactory.getLogger(CsvService.class);

    public <T> void writeCsv(CsvParams<T> csvParams) throws Exception {
        try {
            log.debug("CsvService writing csv starting ...");
            CSVPrinter csvPrinter = new CSVPrinter(csvParams.getWriter(),
                                                   CSVFormat.DEFAULT
                                                           .withHeader(csvParams.getHeaders())
                                                           .withDelimiter(csvParams.getDelimiter()));

            if (Misc.isNotNullOrEmpty(csvParams.getRecords())) {
                for (T item : csvParams.getRecords()) {
                    csvPrinter.printRecord(csvParams.getCsvMapper().buildRecord(item));
                }
            }

            csvPrinter.flush();

            log.debug("CsvService writing csv ended!");
        } catch (Exception e) {
            log.error("CsvService encountered an error while writing csv.", e);
            throw e;
        }
    }
}
