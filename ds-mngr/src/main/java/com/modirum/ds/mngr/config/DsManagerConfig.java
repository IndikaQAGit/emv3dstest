package com.modirum.ds.mngr.config;

import com.modirum.ds.mngr.support.MdmVersionService;
import com.modirum.ds.db.dao.PersistenceService;
import com.modirum.ds.services.ServiceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

@Configuration
public class DsManagerConfig implements ServletContextAware {
    /**
     * This initializes the Product Build information once servletContext
     * has been created.
     * @param servletContext
     */
    @Override
    public void setServletContext(ServletContext servletContext) {
        MdmVersionService.initManifest(servletContext);
    }

    @Bean
    public PersistenceService persistenceService() {
        return ServiceLocator.getInstance().getPersistenceService();
    }
}
