package com.modirum.ds.mngr.support.csv;

@FunctionalInterface
public interface CsvMapper<T> {
    Object[] buildRecord(T item) throws Exception;
}
