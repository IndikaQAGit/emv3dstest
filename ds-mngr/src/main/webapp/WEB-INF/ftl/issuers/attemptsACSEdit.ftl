<#-- @ftlvariable name="attemptsACS" type="com.modirum.ds.db.model.ACSProfile" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <a href="attemptsACSList.html?x=${(.now?time)?html}"><@global.text key='text.attempts.acs.profiles'/></a> &gt;
        <#if attemptsACS?? && attemptsACS.id??>
            <a href="attemptsACSView.html?id=${(attemptsACS.getId()!"")?html}"><@global.text key='text.details'/>: ${(attemptsACS.name!"")?html}</a>
            &gt;
        </#if>
        <span class="selected">
        <#if attemptsACS?? && attemptsACS.id??>
            <@global.text key='text.edit'/>
        <#else>
            <@global.text key='text.add.new'/>
        </#if>
        </span>
    </h1>
    <h2><@global.text key='text.attempts.acs.profile'/></h2>
    <#if attemptsACS??>
        <form id="uForm" action="attemptsACSEdit.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <@global.spring.formHiddenInput path='attemptsACS.id'/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="150" class="tdHeaderVertical"><@global.text key='text.name'/>:*</td>
                    <td width="405">
                        <@global.spring.formInput path="attemptsACS.name" attributes='size="50" maxlength="100"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:*</td>
                    <td>
                        <#if attemptsACS?? && attemptsACS.id??>
                            ${(paymentSystemName!"")?html}
                            <@global.spring.formHiddenInput path='attemptsACS.paymentSystemId'/>
                        <#else>
                            <@global.spring.formSingleSelect path="attemptsACS.paymentSystemId" options=paymentSystemComponentMap/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </#if>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.acsprofile.refNo'/>*:</td>
                    <td>
                        <@global.spring.formTextarea path="attemptsACS.refNo" attributes='rows="3" style="margin: 0px; width: 350px; height: 50px;" maxlength="150"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.acsprofile.url'/>*:</td>
                    <td>
                        <@global.spring.formInput path="attemptsACS.URL" attributes='size="40" maxlength="1024"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.attempts.acs.3dsmethodurl'/>:</td>
                    <td>
                        <@global.spring.formInput path="attemptsACS.threeDSMethodURL" attributes='size="40" maxlength="1024"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical">
                        <@global.text key='text.acsprofile.in.clientcertid'/>
                        :<#if sslCertsManagedExternally?? && sslCertsManagedExternally><#else>*</#if>
                    </td>
                    <td>
                        <@global.spring.formSingleSelect path="attemptsACS.inClientCert" options=inCertificateList attributes='style="width: 220px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>Incoming SSL client auth certificate (required if app managed certificates)</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.acsprofile.out.clientcertid'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="attemptsACS.outClientCert" options=inCertificateList attributes='style="width: 220px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>OutgoingSSL client auth certificate (if empty default is used)</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.protoStart'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="attemptsACS.startProtocolVersion" options=protoVersionList attributes=' style="width: 160px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.protoEnd'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="attemptsACS.endProtocolVersion" options=protoVersionList attributes=' style="width: 160px;"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.status'/>:*</td>
                    <td>
                        <@global.spring.formSingleSelect path="attemptsACS.status" options=acpStatuses/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                <tr>
                    <td colspan="3">
                        <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                        <#if attemptsACS.id??>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'attemptsACSView.html?id=${attemptsACS.id}'; return false;"/>
                        <#else>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'attemptsACSList.html'; return false;"/>
                        </#if>
                    </td>
                </tr>
            </table>
        </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
