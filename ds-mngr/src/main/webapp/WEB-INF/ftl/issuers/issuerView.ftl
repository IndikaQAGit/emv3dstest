<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a>
        &gt;
        <a href="issuerList.html?x=${(.now?time)?html}"><@global.text key='text.issuers'/></a>
        &gt;
        <#if issuer?exists>
            <span class="selected"><@global.text key='text.details'/>: ${(issuer.getName()!"")?html}</span>
        </#if>
    </h1>
    <h2><@global.text key='text.issuer'/></h2>
    <#if issuer?exists>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <@global.security.authorize access="hasAnyRole('issuerEdit')">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <form action="issuerEdit.html" method="post">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="id" value="${(issuer.getId()!"")?html}"/>
                                        <input type="submit" name="button" value="Edit"/>
                                    </form>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </@global.security.authorize>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">Id:</td>
                <td>${(issuer.id!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.name'/>:</td>
                <td>${(issuer.name!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.bin'/>:</td>
                <td>${(issuer.BIN!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                <td>${(paymentSystemName!"")?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.email'/>:</td>
                <td>${(issuer.email!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.address'/>:</td>
                <td>${(issuer.address!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.city'/>:</td>
                <td>${(issuer.city!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeader"><@global.text key='text.issuer.country'/></td>
                <td><#if issuer.country?? && countryMapA2??>${(countryMapA2[issuer.country?string]!issuer.country)?html}</#if></td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.phone'/>:</td>
                <td>${(issuer.phone!'')?html}</td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.status'/>:</td>
                <td><@global.objstatus issuerStatuses issuer.status!""/></td>
            </tr>
            <#if issuer?exists && issuer.modifiedDate?exists>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.modified'/>:</td>
                    <td>
                        <#if issuer?exists && issuer.modifiedDate?exists>
                            ${issuer.modifiedDate?string(dateFormat+" HH:mm:ss")}
                        </#if>
                        <#if issuer?exists &&issuer.modifiedissuer?exists>
                            &nbsp; ${(issuer.modifiedissuer!"")?html}
                        </#if>
                    </td>
                </tr>
            </#if>
            <#list sets as set>
            <#if set &gt; 1>
                <tr>
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr></#if>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.bins'/> <@global.text key='text.set'/> ${(set!"")?html}
                    :
                </td>
                <td>
                    <#if cardbins?exists>
                        <table class="stripe">
                            <tr>
                            <tr>
                                <th <#if rangeMode?? && rangeMode>colspan="2"</#if>
                                    width="<#if rangeMode?? && rangeMode>350<#else>120</#if>">
                                    <#if rangeMode?? && rangeMode><@global.text key='text.binrange'/><#else><@global.text key='text.bin'/></#if>
                                    *
                                </th>
                                <th width="110"><@global.text key='text.cardtype'/></th>
                                <th width="<#if rangeMode?? && rangeMode>150<#else>230</#if>"><@global.text key='text.cardname'/></th>
                                <th width="110"><@global.text key='text.bin.status'/></th>
                                <th width="65">
                                <@global.text key='text.ext.options'/></td>
                                <th width="65">
                                <@global.text key='text.ext.acsInfoInd.2_2_0'/></td>
                                <th width="125">
                                <@global.text key='text.lastmod'/></td>
                            </tr>
                            <#list cardbins as cb>
                                <#if cb.set?? && cb.set==set>
                                    <tr>
                                        <#if rangeMode?? && rangeMode>
                                            <td class="textAlignCenter">${(cb.start!"")?html}</td>
                                            <td class="textAlignCenter"> - ${(cb.end!"")?html}</td><#else>
                                            <td class="textAlignCenter">${(cb.bin!"")?html}</td></#if>
                                        <td class="textAlignCenter"><#if cb.type??>${(cardTypes[cb.type?string]!"")?html}</#if></td>
                                        <td class="textAlignCenter">${(cb.name!"")?html}</td>
                                        <td class="textAlignCenter"><@global.objstatus cardRangeStatuses cb.status!""/></td>
                                        <td class="textAlignCenter">${(cb.options!"")?html}</td>
                                        <td class="textAlignCenter">${(cb.acsInfoInd!"")?html}</td>
                                        <td class="textAlignCenter"><#if cb.lastMod??>${cb.lastMod?string(dateFormat+" HH:mm:ss")}</#if></td>
                                    </tr>
                                </#if>
                            </#list>
                        </table>
                    </#if>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical"><@global.text key='text.issuer.acsprofiles'/>:</td>
                <td>
                    <#if acsprofiles?exists>
                        <table class="stripe">
                            <tr>
                            <tr>
                                <th width="210"><@global.text key='text.acsprofile.name'/></th>
                                <th width="330"><@global.text key='text.acsprofile.url'/></th>
                                <th width="160"><@global.text key='text.acsprofile.in.clientcertid'/></th>
                                <th width="160"><@global.text key='text.acsprofile.out.clientcertid'/></th>
                                <th><@global.text key='text.acsprofile.status'/></th>
                            </tr>
                            <#list acsprofiles as cb>
                                <#if cb.set?? && cb.set==set>
                                    <tr>
                                        <td class="top textAlignCenter">${(cb.name!"")?html}<br/>
                                            <b><@global.text key='text.acsprofile.refno'/></b> ${(cb.refNo!"")?html}
                                            <br/>
                                            <b><@global.text key='text.acsprofile.operatorid'/></b> ${(cb.operatorID!"")?html}
                                            <br/>
                                        </td>
                                        <td class="top textAlignCenter">${(cb.URL!"")?html}<br/>${(cb.threeDSMethodURL!"")?html}<br/>
                                            <#if cb.acsInfoInd??><b><@global.text key='text.ext.acsInfoInd.2_3_0'/></b> ${(cb.acsInfoInd!"")?html}</#if></td>
                                        <td class="top textAlignCenter">
                                            <#if inCertificateList?? && cb.inClientCert??>${(inCertificateList[cb.inClientCert?string]!"")?html}</#if>
                                            <#if protoVersionList?? && cb.startProtocolVersion??>
                                                <br/><@global.text key='text.protoStart'/> ${(protoVersionList[cb.startProtocolVersion?string]!"")?html}</#if>
                                        </td>
                                        <td class="top textAlignCenter">
                                            <#if outCertificateList?? && cb.outClientCert??>${(outCertificateList[cb.outClientCert?string]!"")?html}</#if>
                                            <#if protoVersionList?? && cb.endProtocolVersion??>
                                                <br/><@global.text key='text.protoEnd'/> ${(protoVersionList[cb.endProtocolVersion?string]!"")?html}</#if>
                                        </td>
                                        <td class="top textAlignCenter">
                                            <@global.objstatus acpStatuses cb.status!""/>
                                            <#if cb.lastMod??><br/><@global.text key='text.lastmod'/>
                                                <br/>${cb.lastMod?string(dateFormat+" HH:mm:ss")}</#if>
                                        </td>
                                    </tr>
                                </#if>
                            </#list>
                        </table>
                    </#if>
                </td>
            </tr>
            </#list><!-- end set -->
            <#if attemptsACSMap??>
                <#if sets?? && sets?size &gt; 1>
                    <tr>
                        <td colspan="3">
                            <hr/>
                        </td>
                    </tr></#if>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.attemptsacs'/>:</td>
                    <td>
                        <#if issuer.attemptsACSProId?? && attemptsACSMap??>${(attemptsACSMap[issuer.attemptsACSProId?string]!issuer.attemptsACSProId)?html}</#if>
                    </td>
                </tr>
            </#if>
        </table>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>
