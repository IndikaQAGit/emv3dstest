<#-- @ftlvariable name="issuer" type="com.modirum.ds.db.model.Issuer" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a>
        &gt;
        <a href="issuerList.html?x=${(.now?time)?html}"><@global.text key='text.issuers'/></a>
        &gt;
        <#if issuer?? && issuer.id??>
            <a href="issuerView.html?id=${(issuer.getId()!"")?html}"><@global.text key='text.details'/>: ${(issuer.name!"")?html}</a>
            &gt;
        </#if>
        <span class="selected"><#if issuer?? && issuer.id??><@global.text key='text.edit'/><#else><@global.text key='text.add.new'/></#if> </span>
    </h1>
    <h2><@global.text key='text.issuer'/></h2>
    <#if issuer??>
        <form id="uForm" action="issuerUpdate.html" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <@global.spring.formHiddenInput path='issuer.id'/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="100" class="tdHeaderVertical"><@global.text key='text.issuer.name'/>:*</td>
                    <td width="505">
                        <@global.spring.formInput path="issuer.name" attributes='size="50"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>Known or business name</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.bin'/>*:</td>
                    <td>
                        <@global.spring.formInput path="issuer.BIN" attributes='size="11" maxlength="11"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td>BIN Issuer is registered with Scheme</td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:</td>
                    <td>
                        <#if issuer?? && issuer.id??>
                            ${(paymentSystemName!"")?html}
                            <@global.spring.formHiddenInput path='issuer.paymentSystemId'/>
                        <#else>
                            <@global.spring.formSingleSelect path="issuer.paymentSystemId" options=paymentSystemComponentMap/>
                            <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                        </#if>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.email'/>:</td>
                    <td>
                        <@global.spring.formInput path="issuer.email" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.address'/>:</td>
                    <td>
                        <@global.spring.formInput path="issuer.address" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.city'/>:</td>
                    <td>
                        <@global.spring.formInput path="issuer.city" attributes='size="50" maxlength="128"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.country'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="issuer.country" options=countryMapA2 />
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.phone'/>:</td>
                    <td>
                        <@global.spring.formInput path="issuer.phone" attributes='size="16" maxlength="16"'/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.status'/>:*</td>
                    <td>
                        <@global.spring.formSingleSelect path="issuer.status" options=issuerStatuses/>
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                    <td></td>
                </tr>
         </form>
              <#if issuer?? && issuer.id??>
             <#list sets as set>
                <#if set &gt; 1>
                <tr>
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr></#if>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.bins'/> <@global.text key='text.set'/> ${(set!"")?html}
                        :
                    </td>
                    <td colspan="2">
                        <@global.text key='text.issuer.bins.list'/>
                        <table class="stripe">
                        <tr>
                                <th width="<#if rangeMode?? && rangeMode>150<#else>120</#if>">
                                    <#if rangeMode?? && rangeMode><@global.text key='text.binrange'/><#else><@global.text key='text.bin'/></#if>
                                    *
                                </th>
                                <th width="110"><@global.text key='text.cardtype'/>*</th>
                                <th width="<#if rangeMode?? && rangeMode>180<#else>280</#if>"><@global.text key='text.cardname'/>
                                    *
                                </th>
                                <th width="110"><@global.text key='text.bin.status'/>*</th>
                                <th width="60">
                                <@global.text key='text.ext.options'/></td>
                                <th width="60">
                                <@global.text key='text.ext.acsInfoInd.2_2_0'/></td>
                                <th width="70">
                                <@global.text key='text.lastmod'/></td>
                            </tr>
                            <#if maxBINLength?? >
                                <#assign inputLen = maxBINLength +1 />
                                <#assign inputSize = maxBINLength -1 />
                            <#else>
                                <#assign inputLen = 11 />
                                <#assign inputSize = 10 />
                            </#if>

                            <#list lists.list1 as cb>
                                <#if cb.set?? && cb.set==set>
                                    <form id="cardRangeForm[${cb_index}]" action="cardRangeUpdate.html" method="post">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <input type="hidden" name="index" value="${cb_index}"/>
                                    <input type="hidden" name="setId" value="${(set!"")?html}"/>

                                    <@global.spring.formHiddenInput path='issuer.id'/>

                                  <tr <#if cardRangeErrors?? && cardRangeErrors.errors?? && cardRangeErrors.index?? && cb_index == cardRangeErrors.index > style="outline: thin solid red"</#if>>
                                        <td class="top">
                                            <@global.spring.formHiddenInput path='lists.list1[${cb_index}].id'/>
                                            <@global.spring.formHiddenInput path='lists.list1[${cb_index}].set'/>
                                            <#if rangeMode?? && rangeMode>
                                                <@global.spring.formInput path="lists.list1[${cb_index}].start" attributes='size="${inputSize}" maxlength="${inputLen}"'/>
                                                <@listerror "lists","lists.list1[${cb_index}]","start"/>
                                                <@global.spring.formInput path="lists.list1[${cb_index}].end" attributes='size="${inputSize}" maxlength="${inputLen}"'/>
                                                <@listerror "lists","lists.list1[${cb_index}]","end"/>
                                            <#else>
                                                <@global.spring.formInput path="lists.list1[${cb_index}].bin" attributes='size="${inputSize}" maxlength="${inputLen}"'/>
                                                <@listerror "lists","lists.list1[${cb_index}]","bin"/>
                                            </#if>
                                        </td>
                                        <td class="top">
                                            <@global.spring.formSingleSelect path="lists.list1[${cb_index}].type" options=cardTypes attributes=' style="width: 110px;"'/>
                                            <@listerror "lists","lists.list1[${cb_index}]","type"/>
                                        </td>
                                        <td class="top">
                                            <#if rangeMode?? && rangeMode><#assign nameInputLen = 25 /><#else><#assign nameInputLen = 32 /></#if>
                                            <@global.spring.formInput path="lists.list1[${cb_index}].name" attributes='size="${nameInputLen}" maxlength="32""'/>
                                        </td>
                                        <td class="top">
                                            <@global.spring.formSingleSelect path="lists.list1[${cb_index}].status" options=cardRangeStatuses attributes=' style="width: 110px;"'/>
                                            <@listerror "lists","lists.list1[${cb_index}]","status"/>
                                        </td>
                                        <td class="top">
                                            <@global.spring.formInput path="lists.list1[${cb_index}].options" attributes='size="8" maxlength="32"'/>
                                        </td>
                                        <td class="top">
                                            <@global.spring.formInput path="lists.list1[${cb_index}].acsInfoInd" attributes='size="8" maxlength="20"'/>
                                        </td>
                                        <td class="top"><#if cb.lastMod??>${cb.lastMod?string(dateFormat+" HH:mm:ss")}</#if></td>

                                        <td class="top">
                                        <td>
                                            <input type="submit" name="Save"  value="Save"/>
                                            <#if cb.id??>
                                            <input type="submit" name="Delete" value="Delete" onclick="confirmWarning(event, '${deleteConfirmMsg}')" />
                                            </#if>
                                        </td>
                                    </tr>
                                       <#if cardRangeErrors?? && cardRangeErrors.errors?? && cardRangeErrors.index?? && cb_index == cardRangeErrors.index >
                                             <#list cardRangeErrors.errors as entry>
                                                  <tr>
                                                      <td><b style="color:red">${entry.errorText}</b></td>
                                                  </tr>
                                             </#list>
                                        </#if>
                                  </form>
                                </#if>
                            </#list>
                        </table>
                          <form id="addNewCardRange" action="cardRangeUpdate.html" method="post">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <input type="hidden" name="setId" value="${(set!"")?html}"/>

                                    <@global.spring.formHiddenInput path='issuer.id'/>
                                    <input type="submit" name="addNewBIN" value="<@global.text key='button.addNewBin'/>"/>
                           </form>
                    </td>
                </tr>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.acsprofiles'/>:</td>
                    <td colspan="2">
                        <table class="stripe">
                            <tr>
                                <th width="235"><@global.text key='text.acsprofile.name'/>*</th>
                                <th width="320"><@global.text key='text.acsprofile.url'/>*</th>
                                <th width="160"><@global.text key='text.acsprofile.in.clientcertid'/>
                                    <#if sslCertsManagedExternally?? && sslCertsManagedExternally><#else>*</#if>
                                </th>
                                <th width="160"><@global.text key='text.acsprofile.out.clientcertid'/></th>
                                <th><@global.text key='text.acsprofile.status'/>*</th>
                            </tr>
                            <#list lists.list2 as acp>
                                <#if acp.set?? && acp.set==set>
                                    <tr>
                                        <#if acp.status?? && acp.status="E">
                                            <td class="top">${(acp.name!"")?html} <br/>
                                                <b><@global.text key='text.acsprofile.refno'/></b> ${(acp.refNo!"")?html}
                                                <br/>
                                                <b><@global.text key='text.acsprofile.operatorid'/></b> ${(acp.opertorID!"")?html}
                                                <@global.spring.formHiddenInput path='lists.list2[${acp_index}].id'/>
                                                <@global.spring.formHiddenInput path='lists.list2[${acp_index}].set'/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].name"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].refNo"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].opertorID"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].URL"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].threeDSMethodURL"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].inClientCert"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].outClientCert"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].status"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].endProtocolVersion"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].startProtocolVersion"/>
                                                <@global.spring.formHiddenInput path="lists.list2[${acp_index}].lastMod"/>
                                            </td>
                                            <td class="top">${(acp.URL!"")?html}
                                                <br/>${(acp.threeDSMethodURL!"")?html}</td>
                                            <td class="top">
                                                <#if inCertificateList?? && acp.inClientCert??>${(inCertificateList[acp.inClientCert?string]!"")?html}</#if>
                                                <#if protoVersionList?? && cb.startProtocolVersion??>
                                                    <br/><@global.text key='text.protoStart'/> ${(protoVersionList[cb.startProtocolVersion?string]!"")?html}</#if>
                                            </td>
                                            <td class="top">
                                                <#if outCertificateList?? && acp.outClientCert??>${(inCertificateList[acp.outClientCert?string]!"")?html}</#if>
                                                <#if protoVersionList?? && cb.endProtocolVersion??>
                                                    <br/><@global.text key='text.protoEnd'/> ${(protoVersionList[cb.endProtocolVersion?string]!"")?html}</#if>
                                            </td>
                                            <td class="top"><@global.objstatus acpStatuses acp.status!""/>
                                                <#if acp.lastMod??>
                                                    <br/><@global.text key='text.lastmod'/> ${acp.lastMod?string(dateFormat+" HH:mm:ss")}</#if>
                                            </td>
                                        <#else>
                                            <td class="top">
                                                <@global.spring.formHiddenInput path='lists.list2[${acp_index}].id'/>
                                                <@global.spring.formHiddenInput path='lists.list2[${acp_index}].set'/>
                                                N:<@global.spring.formInput path="lists.list2[${acp_index}].name" attributes='placeholder="ACS name" size="25" maxlength="100"'/>
                                                <@listerror "lists","lists.list2[${acp_index}]","name"/>
                                                <br/>
                                                R:<@global.spring.formTextarea path="lists.list2[${acp_index}].refNo" attributes='style="width: 191px; height: 45px;resize:vertical" placeholder="ACS Reference Numbers"'/>
                                                <@listerror "lists","lists.list2[${acp_index}]","refNo"/>
                                                <br/>
                                                O:<@global.spring.formInput path="lists.list2[${acp_index}].operatorID" attributes='placeholder="ACS operator ID" size="25" maxlength="32"'/>
                                                <@listerror "lists","lists.list2[${acp_index}]","operatorID"/>
                                            </td>
                                            <td class="top" align="right">
                                                <@global.spring.formInput path="lists.list2[${acp_index}].URL" attributes='placeholder="ACS Service URL required" size="40" maxlength="255"'/>
                                                <@listerror "lists","lists.list2[${acp_index}]","URL"/><br/>
                                                <span style="margin-left: -40px;font-size: 11px;font-weight: 700;">
                                                     <@global.text key='text.attempts.acs.3dsmethodurl'/>
                                                </span>
                                                <@global.spring.formInput path="lists.list2[${acp_index}].threeDSMethodURL" attributes='placeholder="3DS Method URL optional" size="30" maxlength="2048"'/>
                                                <@listerror "lists","lists.list2[${acp_index}]","threeDSMethodURL"/><br/>
                                                <span style="margin-left: -40px;font-size: 11px;font-weight: 700;">
                                                     <@global.text key='text.ext.acsInfoInd.2_3_0'/>
                                                </span>
                                                <@global.spring.formInput path="lists.list2[${acp_index}].acsInfoInd" attributes='placeholder="2.3.0 ACS InfoInd optional" size="32" maxlength="20"'/>
                                                <@listerror "lists","lists.list2[${acp_index}]","acsInfoInd"/>
                                            </td>
                                            <td class="top">
                                                <@global.spring.formSingleSelect path="lists.list2[${acp_index}].inClientCert" options=inCertificateList attributes=' style="width: 160px;"' />
                                                <@listerror "lists","lists.list2[${acp_index}]","inClientCert"/>
                                                <#if protoVersionList??><br/>
                                                    <@global.text key='text.protoStart'/>
                                                    <@global.spring.formSingleSelect path="lists.list2[${acp_index}].startProtocolVersion" options=protoVersionList attributes=' style="width: 70px;"' />
                                                    <@listerror "lists","lists.list2[${acp_index}]","startProtocolVersion"/>
                                                </#if>
                                            </td>
                                            <td class="top">
                                                <@global.spring.formSingleSelect path="lists.list2[${acp_index}].outClientCert" options=outCertificateList attributes=' style="width: 160px;"' />
                                                <@listerror "lists","lists.list2[${acp_index}]","outClientCert"/>
                                                <#if protoVersionList??><br/>
                                                    <@global.text key='text.protoEnd'/>
                                                    <@global.spring.formSingleSelect path="lists.list2[${acp_index}].endProtocolVersion" options=protoVersionList attributes=' style="width: 70px;"' />
                                                    <@listerror "lists","lists.list2[${acp_index}]","endProtocolVersion"/>
                                                </#if>
                                            </td>
                                            <td class="top"><@global.spring.formSingleSelect path="lists.list2[${acp_index}].status" options=acpStatuses attributes=' style="width: 80px;"' />
                                                <@listerror "lists","lists.list2[${acp_index}]","status"/>
                                                <#if acp.lastMod??><br/><@global.text key='text.lastmod'/>
                                                    <br/>${acp.lastMod?string(dateFormat+" HH:mm:ss")}</#if>
                                            </td>
                                        </#if>
                                    </tr>
                                </#if>
                            </#list>
                        </table>
                        <input type="submit" name="addNewACSP[${(set!"")?html}]"
                               value="<@global.text key='button.addNewACSP'/>"/>
                    </td>
                </tr
                    </#list><!-- end set-->
                    <#if attemptsACSMap??>
                <#if sets?? && sets?size &gt; 1>
                <tr>
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr></#if>
                <tr>
                    <td class="tdHeaderVertical"><@global.text key='text.issuer.attemptsacs'/>:</td>
                    <td>
                        <@global.spring.formSingleSelect path="issuer.attemptsACSProId" options=attemptsACSMap attributes=' style="width: 265px;"' />
                        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                    </td>
                </tr>
                </#if>
                <tr>
                    <td colspan="3">

                        <input type="submit" name="addNewSet" value="<@global.text key='button.addNewSet'/>"/>
                    </td>
                </tr>
                </#if>
                  <tr>
                    <td colspan="3">
                        <input type="submit" form="uForm" name="save" value="<@global.text key='button.save'/>"/>
                        <#if issuer.id??>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'issuerView.html?id=${issuer.id}'; return false;"/>
                        <#else>
                            <input type="submit" value="<@global.text key='button.cancel'/>"
                                   onclick="document.location.href = 'issuerList.html'; return false;"/>
                        </#if>
                    </td>
                </tr>
            </table>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
    <#macro listerror lists, path, field>
        <#if springMacroRequestContext.getErrors(lists)??>
            <#assign perrs = springMacroRequestContext.getErrors(lists).getFieldErrors(field) />
            <#list perrs as ferror>
                <#if ferror.objectName==path && ferror.field==field>
                    <span class="error">${(ferror.getDefaultMessage()!"")}</span>
                </#if>
            </#list>
        </#if>
    </#macro>
</@global.page>
