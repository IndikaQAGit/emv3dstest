<#import "../common/global.ftl" as global/>
<#global loginPage = "true">
<@global.page hideLogout=true>
    <div id="main">
        <div id="content">
            <div id="content-header">
                <img src="../img/modirum-logo.svg"/>
            </div>
            <div id="content-login" style="width: 550px;">
                <h2><@global.text key='text.secondFactor.login'/></h2>
                <#if msg?exists>
                    <span class="msg">${(msg!"")?html}</span>
                </#if>
                <#if err?exists>
                    <br/><span class="error">${(err!"")?html}</span>
                </#if>
                <form id="sf" action="secondFactor.html" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="hidden" name="cmd" value="verify"/>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="30%" class="tdHeaderVertical"><@global.text key='text.secondFactorPassword'/>:
                            </td>
                            <td>
                                <@global.spring.formPasswordInput path="user.password" attributes="size='20' maxlength='20'"/>
                                <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="tdHeaderVertical">&nbsp;</td>
                            <td>
                                <input type="submit" id="loginBtn" name="loginBtn" value="Login"/>
                                <input type="submit" id="cancelBtn" name="cancelBtn" value="Cancel"/>
                            </td>
                        </tr>
                    </table>
                    <#if authmethod?exists && authmethod='SMSOTP'>
                        <#if smsotpCount?exists && smsotpCount &gt; 3>
                        <#else>
                            <#if newsmsotp?exists && newsmsotp='true'>
                                <@global.text key='text.smsotp.wait'/>&nbsp;<b><span
                                        id="countdowntimer">&nbsp;</span></b>..<br/>
                            </#if>
                            <@global.text key='text.did.not.received.otp'/> <input id="newotpbtn" type="button"
                                                                                   name="newotp"
                                                                                   value="<@global.text key='button.newsmsotp'/>"
                                                                                   onclick="javascript:window.location='secondFactor.html?newotp=true'"/>
                        </#if>
                    </#if>
                </form>
                <form id="sf2" action="secondFactor.html" method="post" target="resultsIf">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input type="hidden" name="cmd" value="chackapproval"/>
                </form>
                <script type="text/javascript">
                    document.getElementById("password").focus();

                    var waitSecs = 30;

                    function countdown() {
                        if (waitSecs > 0) {
                            waitSecs = waitSecs - 1;

                            var tbox = document.getElementById("countdowntimer");
                            var resbtn = document.getElementById("newotpbtn");
                            if (tbox != null && resbtn != null) {
                                if (waitSecs > 5)
                                    resbtn.disabled = true;
                                else
                                    resbtn.disabled = false;

                                tbox.innerHTML = "" + waitSecs;
                                window.setTimeout("countdown();", 1000);
                            }
                        }

                    }

                    if (document.getElementById("newotpbtn") != null) {
                        countdown();
                    }

                    var tim = null;

                    function refresh(millis) {
                        tim = setTimeout("refreshNow();", millis)
                        document.getElementById("password").disabled = true;
                        document.getElementById("loginBtn").disabled = true;
                    }

                    function refreshNow() {
                        document.location = "secondFactor.html?x=${.now?time?js_string}";
                        if (tim != null) {
                            clearTimeout(tim);
                            tim = null;
                        }
                    }


                    var inte = null;

                    function checkapproval(millis) {
                        inte = setTimeout("checkapprovalNow();", millis);
                    }

                    var apResCheckCount = 0;

                    function checkapprovalNow() {
                        apResCheckCount = 0;
                        submitFormWithCmd("loginBtn", "sf2", "checkapproval");

                        if (inte != null) {
                            clearTimeout(inte);
                            inte = null;
                        }

                        inte = setTimeout("checkapprovalResultNow();", 1000);
                    }

                    function checkapprovalResultNow() {
                        apResCheckCount++;
                        var ifr = document.getElementById("resultsIf");
                        if (ifr != null) {
                            var resp = ifr.contentDocument.body.innerHTML;
                            console.log("resp innerhtml=" + resp)
                            if (resp == null && apResCheckCount < 10) {
                                inte = setTimeout("checkapprovalResultNow();", 1000);
                            } else if (resp.indexOf("approved") > -1) {
                                if (inte != null) {
                                    clearTimeout(inte);
                                    inte = null;
                                }

                                submitFormWithCmd("loginBtn", "sf", "verify");
                            } else if (resp.indexOf("pending") > -1) {
                                checkapproval(4000);
                            }
                        }
                    }

                    var toc = null;

                    function cancel(millis) {
                        if (millis == null || millis < 1) {
                            millis = 10;
                        }

                        toc = setTimeout("cancelNow();", millis);
                    }

                    function cancelNow() {
                        submitFormWithCmd("cancelBtn", "sf", "cancel");
                        if (toc != null) {
                            clearTimeout(toc);
                            toc = null;
                        }
                    }

                    <#if refresh??>
                    refresh(${refresh?js_string});
                    </#if>
                    <#if checkapproval??>
                    checkapproval(${checkapproval?js_string});

                    </#if>
                    <#if sfLimit??>
                    cancel(${sfLimit?js_string});

                    </#if>


                </script>
                <iframe id="resultsIf" name="resultsIf" style="display:none; visibility: hidden "></iframe>
            </div>
            <div id="content-footer">
                <img src="../img/powered-by-modirum.svg"/>
                <div id="footer-right">
                </div>
            </div>
        </div>
    </div>
</@global.page>