<#import "../common/global.ftl" as global/>
<@global.page>
    <script type="text/javascript" src="../js/calendar.js">
        // need for browsers...
    </script>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span
                class="selected"><@global.text key='text.auditlogs'/></span></h1>

    <@global.security.authorize access="hasRole('auditLogView')">
        <h2><@global.text key='text.search.auditlogs'/></h2>
        <form id="uSearchForm" action="auditlogs.html" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <span style="width: 120px; display: inline-block;"><@global.text key='text.al.date'/>:</span>
        <#if searcher?exists && searcher.logDateFrom?exists >
            <#assign dateFrom = searcher.logDateFrom?string(dateFormat)/>
        <#else>
            <#assign dateFrom = ""/>
        </#if>
        <input id="dateFrom" type="text" size="10" name="logDateFrom" value="${dateFrom}"/>
        <a href="javascript:showCalPopup('dfoico', 'dateFrom', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dfoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
        HH:mm<input style="width: 18px;" id="dateFromHH" type="text" size="2" maxlength="2" name="logDateFromHH"
                    value="${(logDateFromHH!"")?html}"/>:
        <input style="width: 18px;" id="dateFrommm" type="text" size="2" maxlength="2" name="logDateFrommm"
               value="${(logDateFrommm!"")?html}"/>
        to
        <#if searcher?exists && searcher.logDateTo?exists>
            <#assign dateTo= searcher.logDateTo?string(dateFormat)/>
        <#else>
            <#assign dateTo = ""/>
        </#if>
        <input id="dateTo" type="text" size="10" name="logDateTo" value="${dateTo}"/>
        <a href="javascript:showCalPopup('dtoico', 'dateTo', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dtoico" align="baseline" border="0" src="../img/calicon.gif"/></a>

        <input type="button" name="today" value="<@global.text key='text.today'/>"
               onClick="fillDatesToday('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="yesterDay" value="<@global.text key='text.yesterday'/>"
               onClick="fillDatesYesterday('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="thisMonth" value="<@global.text key='text.thismonth'/>"
               onClick="fillDatesThisMonth('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="prevMonth" value="<@global.text key='text.prevmonth'/>"
               onClick="fillDatesPrevMonth('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <br/>
        <span style="width: 120px; display: inline-block;"><@global.text key='text.al.action'/>:</span>
        <@global.spring.formSingleSelect path="s.action" options=events />
        <br/>
        <span style="width: 120px; display: inline-block;"><@global.text key='text.al.objectClass'/>:</span>
        <@global.spring.formSingleSelect path="s.objectClass" options=objectClasses />
        <span style="width: 70px; display: inline-block;"><@global.text key='text.al.objectId'/>:</span>
        <@global.spring.formInput path="s.objectId" attributes='size="18" maxlength="18"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: 120px; display: inline-block;"><@global.text key='text.al.by'/>:</span>
        <@global.spring.formInput path="s.by" attributes='size="20" maxlength="30"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: 120px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
        <@global.spring.formSingleSelect path="s.order" options={"when":"Date","action":"Action", "objectClass":"Object type", "by":"User"} />
        <@global.spring.formSingleSelect path="s.orderDirection" options={"desc":"Descending","asc":"Ascending"} />
        <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
        <@global.spring.formSingleSelect path="s.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
        <br/>
        <span style="width: 120px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
        <@global.spring.formSingleSelect path="s.paymentSystemId" options=paymentSystemSearchMap/>
        <br/>
        <input type="hidden" name="search" value="Search"/>
        <input type="submit" name="submitbtn" value="Search" onclick="showWaitBox();"/>
        <br/>
        <#if found?exists>
            <#if searcher?exists && searcher.total &gt; 0 >
                <h2><@global.text key='text.found.total'/> ${searcher.total!""?html} <@global.text key='text.auditlogs'/>
                    , <@global.text key='text.showing'/> ${(searcher.start+1)!""?html} <@global.text key='text.to'/>
                    <#if searcher.total &gt; (searcher.start+searcher.limit)>
                        ${(searcher.start+searcher.limit)!""?html}
                    <#else>
                        ${(searcher.total)!""?html}
                    </#if>
                </h2>
                <#assign colspan = "3"/>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="165" class="tdHeader"><@global.text key='text.al.when'/></td>
                        <td width="80" class="tdHeader"><@global.text key='text.al.action'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.al.by'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.al.objectClass'/></td>
                        <td width="80" class="tdHeader"><@global.text key='text.al.objectId'/></td>
                        <td class="tdHeader"><@global.text key='text.al.details'/></td>
                    </tr>
                    <#list found as al>
                        <tr <#if al_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                            <td><#if al.when?exists>${al.when?string(dateFormat+" HH:mm:ssZ")}</#if></td>
                            <td>
                                <#if al.action?exists && events?exists && events[al.action]?exists>${(events[al.action]!"")?html}<#else>${(al.action!"")?html}</#if>
                            </td>
                            <td>${(al.by!"")?html}</td>
                            <td>${(al.objectClass!"")?html}</td>
                            <td>${(al.objectId!"")?html}</td>
                            <td>${(al.details!"")?html}</td>
                        </tr>
                    </#list>
                    <@global.pagingButtons searcher=searcher colspan=6/>
                </table>
                </form>
            <#else>
                <br/>
                <@global.text key='text.no.matches.found'/>
            </#if>
        </#if>
        <div id="waitbox" style="display: none;" class="waitoverlay">
            <br/><br/>
            <b><@global.text key='text.please.wait'/></b>
        </div>
    </@global.security.authorize>
</@global.page>