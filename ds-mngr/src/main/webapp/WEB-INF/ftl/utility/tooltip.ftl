<#macro tooltipDiv addlClasses divContent revealContent>
	<span>
		<div class="cont ${addlClasses}">${divContent}
		    <span class="contToolTip">${revealContent}</span>
		</div>
	</span>
 </#macro>