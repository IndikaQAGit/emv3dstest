<#import "../common/global.ftl" as global/>
<@global.page>
    <STYLE type="text/css">
        .waitoverlay {
            position: fixed;
            left: 50%;
            top: 20px;
            height: 200px;
            margin-left: -175px;
            width: 350px;

            background-color: rgb(110, 100, 100);
            border: solid rgb(70, 50, 50) 2px;
            z-index: 3;
            filter: alpha(opacity=75);
            /* CSS3 standard */
            opacity: 0.75;
            text-align: center;
        }

        #waitoverlayMsg {
            font-size: 16px;
            color: rgb(250, 50, 50);
        }
    </STYLE>

    <h2>Please wait while operation processing..</h2>
    <div class="waitoverlay">
        <div id="waitoverlayMsg"
             style="margin-top: 80px; font-size: 15pt; font-weight: 600; filter:alpha(opacity=100); opacity:1;"
        >Please wait while operation processing..
        </div>
    </div>
    <form id="postForm" action="${form.action}" method="${form.method}"
            <#if form.accept_charset?exists>
                accept_charset="${form.accept_charset?html}"
            </#if>
    />

    <#list form.fields as field >
        <#if field?exists>

            <#if field.type=="submit" || field.type="button">
                <input class="btn" type="${field.type?html}" name="${field.name?html}" value="${field.value?html}"/>
            <#elseif field.type=="image">
                <input class="btn" src="${field.src?html}" type="${field.type?html}" name="${field.name?html}"
                       value="${field.value?html}"/>
            <#else>
                <input type="${field.type?html}" name="${field.name?html}" value="${field.value?html}"/>
            </#if>

        </#if>
    </#list>
    <input type="submit" name="submitBtn" value="Click here to continue"/>
    </form>
    <script type="text/javascript">
        function submitTimed() {
            var timer = setTimeout("submitForm(null, 'postForm');", 5);
        }

        function submitForm(button, formid) {
            var cform = document.getElementById(formid);
            if (cform != null) {
                if (button != null) button.disabled = true;
                cform.submit();
            }
        }

        submitTimed();
    </script>

</@global.page>
