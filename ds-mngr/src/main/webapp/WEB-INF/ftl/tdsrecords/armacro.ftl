<#macro formatAmount amount exp cur>
    <#if amount?? && amount &gt; -1 && amount?string?length &gt;0 && cur?? && exp??>
        <#if exp==0 >${amount?string("0")}<#elseif exp==1>${(amount/10)?string("0.0")}<#elseif  exp==2>${(amount/100)?string("0.00")}<#else>${(amount/100)?string("0.00")}</#if>
    </#if>
    <#if cur?? && currencyCodeMap?? && currencyCodeMap[cur?string]??>${(currencyCodeMap[cur?string])?html}</#if>
</#macro>

<#macro arStatus status>
    <#if tdsstatuses?? && status?? && tdsstatuses[status?string]??>${tdsstatuses[status?string]?html}</#if>
</#macro>

<#macro arAuthStatus astatus>
    <#if tdsauthstatuses?exists && tdsauthstatuses[astatus]?exists>    ${tdsauthstatuses[astatus]?html}<#else
    >${(tdsauthstatuses!"")?html}</#if>
</#macro>

<#macro maskPan maskedPan maskMode>
    <#if maskedPan?exists && maskedPan?length &gt;=6>
        <#if maskMode=="4+4">
            ${maskedPan?substring(0,4)?html}##${maskedPan?substring(6)?html}
        <#elseif maskMode=="6+2">
            ${maskedPan?substring(0,6)?html}${maskedPan?substring(6, maskedPan?length-4)?html}##${maskedPan?substring(maskedPan?length-2)?html}
        <#else>
            ${maskedPan?substring(0,6)?html}#${maskedPan?substring(7, maskedPan?length-5)?html}####${maskedPan?substring(maskedPan?length-4)?html}
        </#if>
    <#else>${(maskedPan!"")?html}</#if>
</#macro>