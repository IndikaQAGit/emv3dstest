<#import "../common/global.ftl" as global/>
<#include "armacro.ftl"/>
<@global.page>
    <script type="text/javascript" src="../js/calendar.js">
        // need for browsers...
    </script>
    <script type="text/javascript" src="../js/tdsrecords.js"></script>

    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <span
                class="selected"><@global.text key='text.authrecords'/></span></h1>

    <@global.security.authorize access="hasRole('recordsView')">
        <h2><@global.text key='text.search.authrecords'/></h2>
        <#assign col1=110/>
        <form id="uSearchForm" action="tdsrecords.html" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <input type="hidden" name="cmd" value=""/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.ar.date'/>:</span>
        <#if searcher?exists && searcher.trDateFrom?exists >
            <#assign dateFrom = searcher.trDateFrom?string(dateFormat)/>
        <#else>
            <#assign dateFrom = ""/>
        </#if>
        <input id="dateFrom" type="text" size="10" name="trDateFrom" value="${dateFrom}"/>
        <a href="javascript:showCalPopup('dfoico', 'dateFrom', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dfoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
        HH:mm<input style="width: 18px;" id="dateFromHH" type="text" size="2" maxlength="2" name="trDateFromHH"
                    value="${(trDateFromHH!"")?html}"/>:
        <input style="width: 18px;" id="dateFrommm" type="text" size="2" maxlength="2" name="trDateFrommm"
               value="${(trDateFrommm!"")?html}"/>
        to
        <#if searcher?exists && searcher.trDateTo?exists>
            <#assign dateTo= searcher.trDateTo?string(dateFormat)/>
        <#else>
            <#assign dateTo = ""/>
        </#if>
        <input id="dateTo" type="text" size="10" name="trDateTo" value="${dateTo}"/>
        <a href="javascript:showCalPopup('dtoico', 'dateTo', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dtoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
        HH:mm<input style="width: 18px;"  type="text" size="2" maxlength="2" name="trDateToHH"
                    value="${(trDateToHH!"")?html}"/>:
        <input style="width: 18px;"  type="text" size="2" maxlength="2" name="trDateTomm"
               value="${(trDateTomm!"")?html}"/>
        <input type="button" name="today" value="<@global.text key='text.today'/>"
               onClick="fillDatesToday('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="yesterDay" value="<@global.text key='text.yesterday'/>"
               onClick="fillDatesYesterday('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="thisMonth" value="<@global.text key='text.thismonth'/>"
               onClick="fillDatesThisMonth('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <input type="button" name="prevMonth" value="<@global.text key='text.prevmonth'/>"
               onClick="fillDatesPrevMonth('${(dateFormat!"yyyy-mm-dd")?lower_case}');"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
        <@global.spring.formSingleSelect path="s.paymentSystemId" options=paymentSystemSearchMap attributes='onchange="submitFormWithCmd(null,\'uSearchForm\' , \'refreshPaymentSystem\');"'/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.by.requestorid'/>:</span>
        <@global.spring.formInput path="s.requestorID" attributes='size="30" maxlength="35"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.merchant.name'/>:</span>
        <@global.spring.formInput path="s.merchantName" attributes='size="30" maxlength="40"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        (min 2 chars)
        <span style="width: 120px; display: inline-block; text-align: right;"><@global.text key='text.merchant.acqid'/>:</span>
        <@global.spring.formInput path="s.acquirerMerchantID" attributes='size="25" maxlength="24"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.merchant.country'/>:</span>
        <@global.spring.formSingleSelect path="s.merchantCountry" options=merchantCountries />
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.card.bin.and.last4'/>:</span>
        <@global.spring.formInput path="s.first6" attributes='size="8" maxlength="6"'/> ...
        <@global.spring.formInput path="s.last4" attributes='size="5" maxlength="4"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.purchaseamount.range'/>:</span>
        <@global.spring.formInput path="s.purchaseAmountLow" attributes='size="12" maxlength="15"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/> - <@global.spring.formInput
    path="s.purchaseAmountHigh" attributes='size="12" maxlength="15"'/>(in minor units eg cents)
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <@global.spring.formSingleSelect path="s.purchaseCurrency" options=currencyCodeMap attributes='style="width: 90px;"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.ar.deviceChannel'/>:</span>
        <@global.spring.formSingleSelect path="s.deviceChannel" options=deviceChannels attributes='style="width: 120px;"'/>
        <span style="width: 88px; display: inline-block; text-align: right;"><@global.text key='text.ar.protocol'/>:</span>
        <@global.spring.formSingleSelect path="s.protocol" options=protoVersionList attributes='style="width: 80px;"'/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.ar.status'/>:</span>
        <@global.spring.formSingleSelect path="s.localStatus" options=tdsstatuses attributes='style="width: 120px;"'/>
        <span style="width: 88px; display: inline-block; text-align: right;"><@global.text key='text.ar.authstatus'/>:</span>
        <@global.spring.formSingleSelect path="s.transStatus" options=tdsauthstatuses attributes='style="width: 120px;"'/>
        &nbsp;<span style="display: inline-block; text-align: right;"><@global.text key='text.ar.statusreason'/>:</span>
        <@global.spring.formInput path="s.transStatusReason" attributes='size="3" maxlength="2"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.issuer'/>:</span>
        <@global.spring.formSingleSelect path="s.localIssuerId" options = issuersDropdown attributes='style="width: 120px;"'/>
        <span style="width: 88px; display: inline-block;  text-align: right;"><@global.text key='text.issuerbin'/>:</span>
        <@global.spring.formInput path="s.issuerBIN" attributes='size="${binLength}" maxlength="${binLength}"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.acquirer'/>:</span>
        <@global.spring.formSingleSelect path="s.localAcquirerId" options = acquirersDropdown attributes='style="width: 120px;"'/>
        <span style="width: 88px; display: inline-block;  text-align: right;"><@global.text key='text.acquirerbin'/>:</span>
        <@global.spring.formInput path="s.acquirerBIN" attributes='size="${binLength}" maxlength="${binLength}"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.sdktransid'/>:</span>
        <@global.spring.formInput path="s.SDKTransID" attributes='size="36" maxlength="36"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <span style="width: 130px; display: inline-block; text-align: right;"><@global.text key='text.tdsservertransid'/>:</span>
        <@global.spring.formInput path="s.tdsTransID" attributes='size="36" maxlength="36"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.dstransid'/>:</span>
        <@global.spring.formInput path="s.DSTransID" attributes='size="36" maxlength="36"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <span style="width: 130px; display: inline-block; text-align: right;"><@global.text key='text.acstransid'/>:</span>
        <@global.spring.formInput path="s.ACSTransID" attributes='size="36" maxlength="36"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.errorcode'/>:</span>
        <@global.spring.formInput path="s.errorCode" attributes='size="3" maxlength="3"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <span style="width: 90px; display: inline-block; text-align: right;"><@global.text key='text.errordetail'/>:</span>
        <@global.spring.formInput path="s.errorDetail" attributes='size="25" maxlength="30"'/>
        <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
        <br/>
        <span style="width: ${col1}px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
        <@global.spring.formSingleSelect path="s.order" options={"localDateStart":"Start Date", "localStatus":"Status", "id":"Id"} />
        <@global.spring.formSingleSelect path="s.orderDirection" options={"desc":"Descending","asc":"Ascending"} />
        <span style="width: 110px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
        <@global.spring.formSingleSelect path="s.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
        <br/>
        <input type="hidden" name="action" id="action" value="search"/>
        <input type="submit" name="submitbtn" value="<@global.text key='text.searchbtn'/>"
               onclick="submitSearchForm('search', true);"/>
         
        <input type="button" name="exportbtn" value="<@global.text key='text.exportbtn'/>"
               onclick="exportCSV('export', false);"/>
        <input type="button" name="resetButton" value="<@global.text key='text.resetbtn'/>"
               onClick="blankCriterias('uSearchForm');"/>
        <br/>
        <#if found?exists>
            <#if searcher?exists && searcher.total &gt; 0 >
                <h2><@global.text key='text.found.total'/> ${searcher.total!""?html} <@global.text key='text.authrecords'/>
                    , <@global.text key='text.showing'/> ${(searcher.start+1)!""?html} <@global.text key='text.to'/>
                    <#if searcher.total &gt; (searcher.start+searcher.limit)>
                        ${(searcher.start+searcher.limit)!""?html}
                    <#else>
                        ${(searcher.total)!""?html}
                    </#if>
                </h2>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="100" class="tdHeader"><@global.text key='text.ar.id'/></td>
                        <td width="95" class="tdHeader"><@global.text key='text.ar.datetime'/></td>
                        <td width="95" class="tdHeader"><@global.text key='text.ar.completed'/></td>
                        <td width="170" class="tdHeader"><@global.text key='text.ar.maskedpan'/></td>
                        <td width="85" class="tdHeader"><@global.text key='text.ar.requestorid'/>
                            /<@global.text key='text.ar.merchantid'/></td>
                        <td width="120" class="tdHeader"><@global.text key='text.ar.merchantName'/>
                            /<@global.text key='text.requestor.name'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.ar.issuer'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.ar.acquirer'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.ar.total'/></td>
                        <td width="90" class="tdHeader"><@global.text key='text.ar.status'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.ar.authstatus'/></td>
                        <td width="40" class="tdHeader"><@global.text key='text.ar.eci'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.sdktransid'/></td>
                        <td width="100" class="tdHeader"><@global.text key='text.tdsservertransid'/></td>
                    </tr>
                    <#list found as ar>
                        <tr <#if ar_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                            <td>
                                <a href="tdsrecord.html?id=${(ar.id!"")?html}">${(ar.id!"")?html}</a><br/>${(ar.DSTransID!"")?html}
                            </td>
                            <td>
                                <#if ar.localDateStart?exists>${ar.localDateStart?string(dateFormat+" HH:mm:ss")}</#if>
                            </td>
                            <td>
                                <#if ar.localDateEnd?exists>${ar.localDateEnd?string(dateFormat+" HH:mm:ss")}</#if>
                            </td>
                            <td><@maskPan ar.acctNumber!"" "6+4"/></td>
                            <td>${(ar.requestorID!"")?html}/ ${(ar.acquirerMerchantID!"")?html}</td>
                            <td>${(ar.merchantName!"")?html}/ ${(ar.requestorName!"")?html}</td>
                            <td><#if ar.localIssuerId?exists>${(issuersMap[ar.localIssuerId?string]!"")?html}</#if></td>
                            <td><#if ar.localAcquirerId?exists>${(acquirersMap[ar.localAcquirerId?string]!"")?html}</#if></td>
                            <td><@formatAmount ar.purchaseAmount!-1 ar.purchaseExponent!2 ar.purchaseCurrency!0/></td>
                            <td><@arStatus ar.localStatus!""/></td>
                            <td><@arAuthStatus ar.transStatus!""/></td>
                            <td>${(ar.ECI!"")?html}</td>
                            <td>${(ar.SDKTransID!"")?html}</td>
                            <td>${(ar.tdsTransID!"")?html}</td>
                        </tr>
                    </#list>
                    <@global.pagingButtons searcher=searcher colspan=15/>
                </table>
                </form>
            <#else>
                <br/>
                <@global.text key='text.no.matches.found'/>
            </#if>
        </#if>
        <div id="exportModal" style="border: solid;border-width: thin;width: 500px;height: 100px;top: 30%;left: 20%;background-color: white;color: black;position: fixed;text-align: center;line-height: 100px; display:none;">
        Transaction export is limited to ${recordRowLimitForCsvExport} entries.
        </div>
        <div id="waitbox" style="display: none;" class="waitoverlay">
            <br/><br/>
            <b><@global.text key='text.please.wait'/></b>
        </div>
    </@global.security.authorize>

</@global.page>


