<#-- @ftlvariable name="filter" type="com.modirum.ds.mngr.model.ui.filter.AcquirerSearchFilter" -->
<#-- @ftlvariable name="acquirerList" type="java.util.List<com.modirum.ds.db.model.Acquirer>" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <span class="selected"><@global.text key='text.acquirers'/></span>
    </h1>

    <@global.security.authorize access="hasAnyRole('acquirerEdit')">
        <a href="acquirerEdit.html"><@global.text key='text.addNewAcquirer'/></a>
    </@global.security.authorize>

    <h2><@global.text key='text.search.acquirers'/></h2>
    <style> input[type="text"], input[type="password"], select {
            margin-bottom: 2pt;
        } </style>
    <form id="uSearchForm" action="acquirerList.html" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <span style="width: 110px; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
    <@global.spring.formSingleSelect path="filter.paymentSystemId" options=paymentSystemSearchMap/>
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.name'/>:</span>
    <@global.spring.formInput path="filter.name" attributes='size="25" maxlength="50"'/>
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.bin'/>:</span>
    <@global.spring.formInput path="filter.BIN" attributes='size="${binLength}" maxlength="${binLength}"'/>
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.status'/>:</span>
    <@global.spring.formSingleSelect path="filter.status" options=acquirerStatuses />
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.country'/>:</span>
    <@global.spring.formSingleSelect path="filter.country" options=countryMapA2 />
    <br/>
    <span style="width: 110px; display: inline-block;"><@global.text key='text.orderby'/>:</span>
    <@global.spring.formSingleSelect path="filter.order" options={"":"","id":"Id", "status":"Status", "name":"Name"} />
    <@global.spring.formSingleSelect path="filter.orderDirection" options={"asc":"Ascending","desc":"Descending"} />
    <span style="width: 100px; display: inline-block;"><@global.text key='text.results.per.page'/>:</span>
    <@global.spring.formSingleSelect path="filter.limit" options={"10":"10","25":"25", "50":"50", "100":"100", "250":"250" } />
    <br/>
    <input type="hidden" name="search" value="Search"/>
    <input type="submit" name="submitbtn" value="<@global.text key='button.search'/>"/>
    <br/>
    <#if acquirerList?exists>
        <#if filter?exists && filter.total &gt; 0 >
            <h2><@global.text key='text.found.total'/> ${filter.total!""?html} <@global.text key='text.acquirers'/>
                , <@global.text key='text.showing'/> ${(filter.start+1)!""?html} <@global.text key='text.to'/>
                <#if filter.total &gt; (filter.start+filter.limit)>
                    ${(filter.start+filter.limit)!""?html}
                <#else>
                    ${(filter.total)!""?html}
                </#if>
            </h2>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" class="tdHeader">Id</td>
                    <td width="15%" class="tdHeader"><@global.text key='text.name'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.bin'/></td>
                    <td width="15%" class="tdHeader"><@global.text key='text.email'/></td>
                    <td width="10%" class="tdHeader"><@global.text key='text.country'/></td>
                    <td width="10%" class="tdHeader"><@global.text key='general.paymentSystem'/></td>
                    <td align="center" class="tdHeader"><@global.text key='text.status'/></td>
                </tr>
                <#list acquirerList as acqx>
                    <tr <#if acqx_index%2==0>class="tr_even" <#else>class="tr_odd"</#if>>
                        <@acquirerDetails acq=acqx />
                    </tr>
                </#list>
                <@global.pagingButtons searcher=filter colspan=6/>
            </table>
            </form>
        <#else>
            <@global.text key='text.no.matches.found'/>
        </#if>
    </#if>
</@global.page>

<#macro acquirerDetails acq>
    <td width="5%"><a href="acquirerView.html?id=${(acq.id!"")?html}">${(acq.id!"")?html}</a></td>
    <td width="15%"><a href="acquirerView.html?id=${(acq.id!"")?html}">${(acq.name!"")?html}</a></td>
    <td width="15%">${(acq.BIN!"")?html}</td>
    <td width="15%">${(acq.email!"")?html}</td>
    <td><#if acq.country??>${(countryMapA2[acq.country?string]!acq.country)?html}</#if></td>
    <td>
        <#if acq.paymentSystemId??>
            ${((paymentSystemComponentMap[acq.paymentSystemId?string])!acq.paymentSystemId)?html}
        </#if>
    </td>
    <td width="15%" align="center"><@global.objstatus acquirerStatuses acq.status!""/></td>
</#macro>
