<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt; <a
                href="../settings/settingsList.html"><@global.text key='text.settings'/></a>
        &gt; <@global.text key='text.setting.edit'/>:</h1>
    <h2><@global.text key='text.setting.edit'/></h2>
    <#if error?exists>
        <span class="error">${error?html}</span>
    </#if>
    <form action="settingSave.html" id="tForm" accept-charset="UTF-8" method="POST">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <input id="cmd" type="hidden" name="cmd" value="save"/>
        <#if setting?exists>
            <input id="oldKey" type="hidden" name="oldKey" value="${(setting.key!"")?html}"/>
            <input id="oldProcessorId" type="hidden" name="oldProcessorId" value="${(setting.processorId!"")?html}"/>
        </#if>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeaderVertical">Key:</td>
                <td>
                    <@global.spring.formInput path="setting.key" attributes='id="key" size="64" maxlength="100"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <div class="helptext">Setting key</div>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">Setting value:*</td>
                <td class="tdHeaderVertical">
                    <@global.spring.formTextarea path="setting.value" attributes='rows="10" cols="65"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <div class="helptext">Enter the setting value, if adding a setting that should be secret like
                        password
                        use prefix "enc:" before value, then such setting will automatically encrypted,
                        and will be displayed later as sec:encryptedvalue
                    </div>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">Setting comment:</td>
                <td class="tdHeaderVertical">
                    <@global.spring.formTextarea path="setting.comment" attributes='rows="2" cols="65"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <div class="helptext">Enter the setting comment</div>
                </td>
            </tr>
            <tr>
                <td class="tdHeaderVertical">Last modified:</td>
                <td class="tdHeaderVertical">
                    <#if setting.lastModified?exists>${setting.lastModified?string(dateFormat+" HH:mm:ss")}</#if>
                    <#if setting.lastModifiedBy?exists> &nbsp; ${(setting.lastModifiedBy!"")?html}</#if>
                </td>
                <td></td>
            </tr>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="tdHeaderVertical">&nbsp;</td>
                <td colspan="2">
                    <input type="submit" name="submitbutton" value="Save/Update"/>
                    <#if setting?exists && setting.key?exists>
                        <input type="submit" name="savenew" value="Save as New" onClick="saveNewSetting();"/>
                        <input type="submit" name="delbutton" value="Delete" onClick="delSetting();"/>
                    </#if>
                    <input type="button" name="cancelbutton" value="Cancel"
                           onclick="document.location.href = 'settingsList.html'"/>
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">
        function delSetting() {
            var cmd = document.getElementById("cmd");
            if (cmd != null)
                cmd.value = "delete";
        }

        function saveNewSetting() {
            var cmd = document.getElementById("cmd");
            if (cmd != null)
                cmd.value = "saveAsNew";
        }


    </script>
</@global.page>
