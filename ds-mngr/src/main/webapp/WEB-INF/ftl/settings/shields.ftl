<#import "../common/global.ftl" as global/>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.home'/></a> &gt;
        <span class="selected"><a href="settingsList.html?x=${(.now?time)?html}"><@global.text key='text.settings'/></a> - <@global.text key='text.service.shields'/></span>
    </h1>
    <h2><@global.text key='text.service.shields'/></h2>
    <@global.security.authorize access="hasAnyRole('adminSetup')">
        <a href="shields.html?cmd=purge">Purge all shields</a>
    </@global.security.authorize>
    <#if res?exists && res=="ok">
        <span style="color: blue; font-weight: bold;">Shields removed successfully</span>
        <br/>
    </#if>
    <h2>Current active shields</h2>
    <#list shields as shield>
        <span style="white-space: pre;">${(shield.value!"")?html}</span>
        </br>
    </#list>
</@global.page>
