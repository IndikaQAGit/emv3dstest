<#-- @ftlvariable name="filter" type="com.modirum.ds.mngr.model.ui.filter.ReportsFilter" -->
<#import "../common/global.ftl" as global/>
<@global.page>
    <script type="text/javascript" src="../js/calendar.js">
    </script>

    <h1>
        <a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt;
        <a href="reportsList.html?x=${(.now?time)?html}"><@global.text key='text.reports'/></a> &gt;
        <span class="selected"><@global.text key='${typeHeader}'/></span>
    </h1>

    <#assign colwidth="130px"/>

    <h2><@global.text key='${typeHeader}'/></h2>

    <br/>

    <#if error??>
        <span class="error">${error?html}</span>
        <br/>
        <br/>
    </#if>

    <form id="rForm" action="reportsSave.html" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <input type="hidden" id='type' name='type' value="${filter.type}" />

    <#if reportType == 'TransactionReport'>
        <span style="width: ${colwidth}; display: inline-block;"><@global.text key='text.ar.date'/>:</span>
        <input id="dateFrom" type="text" size="10" name="startDate" value="${(filter.startDate?string(dateFormat))!""}"/>
        <a href="javascript:showCalPopup('dfoico', 'dateFrom', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dfoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
        HH:mm<input style="width: 18px;" id="dateFromHH" type="text" size="2" maxlength="2" name="startDateHour"
                    value="${(filter.startDateHour)!""}"/>:
        <input style="width: 18px;" id="dateFrommm" type="text" size="2" maxlength="2" name="startDateMinute"
               value="${(filter.startDateMinute)!""}"/>
        to
        <input id="dateTo" type="text" size="10" name="endDate" value="${(filter.endDate?string(dateFormat))!""}"/>
        <a href="javascript:showCalPopup('dtoico', 'dateTo', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
            <img id="dtoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
        <br/>

        <span style="width: ${colwidth}; display: inline-block;"><@global.text key='text.report.locale'/>:</span>
        <@global.spring.formSingleSelect path="filter.locale" options=supportedLocales />
        <br/>

    </#if>

        <span style="width: ${colwidth}; display: inline-block;"><@global.text key='text.report.timezone'/>:</span>
        <@global.spring.formSingleSelect path="filter.timezone" options=timezones />
        <br/>

        <span style="width: ${colwidth}; display: inline-block;"><@global.text key='general.paymentSystem'/>:</span>
        <@global.spring.formSingleSelect path="filter.paymentSystemId" options=paymentSystemSearchMap />
        <br/>
        <br/>

        <input type="submit" name="submit-run-report" id="submit-run-report" value="<@global.text "button.run.report"/>"/>

    </form>
</@global.page>

