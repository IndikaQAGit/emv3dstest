<#import "../common/global.ftl" as global/>
<#import "../utility/tooltip.ftl" as tooltip/>
<@global.page>
    <#-- Page Headers -->
    <h1>
        <a href="../main/main.html"><@global.text key='text.home'/></a>
        &gt;
        <a href="hsmDeviceList.html"><@global.text key='text.HSMDevices'/></a>
        &gt;
        <#if hsmDevice?? && hsmDevice.id??>
            <a href="hsmDeviceView.html?id=${(hsmDevice.getId()!"")?html}">
                <@global.text key='text.details'/>: ${(hsmDevice.name!"")?html}</a>
            &gt;
        </#if>
        <span class="selected">
            <#if hsmDevice?? && hsmDevice.id??>
                <@global.text key='text.edit'/>
            <#else>
                <@global.text key='text.add.new'/>
            </#if>
        </span>
    </h1>
    <h2><@global.text key='text.hsmDevice'/></h2>

    <#-- HSM Device Details -->
    <#if hsmDevice??>
    <form id="uForm" action="hsmDeviceUpdate.html" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <@global.spring.formHiddenInput path='hsmDevice.id'/>
        <@global.spring.formHiddenInput path='hsmDeviceConf.dsId'/>
        <@global.spring.formHiddenInput path='hsmDeviceConf.hsmDeviceId'/>
        <@global.spring.formHiddenInput path='hsmDevice.status'/>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.HSMDevice.name'/>:*</td>
                <td width="400">
                    <@global.spring.formInput path="hsmDevice.name" attributes='size="40"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
            </tr>
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.HSMDevice.className'/>:*</td>
                <td width="400">
                    <@global.spring.formInput path="hsmDevice.className" attributes='size="40"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
            </tr>
            <tr>
                <td width="115" class="tdHeaderVertical"><@global.text key='text.HSMDeviceConf.config'/>:</td>
                <td width="400">
                    <@global.spring.formInput path="hsmDeviceConf.config" attributes='size="40"'/>
                    <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
                </td>
                <td>
                    <@tooltip.tooltipDiv addlClasses="helpText" divContent="${helpText}" revealContent="${configTooltip}" />
                </td>
            </tr>
            <#if hsmDevice.id??>
            <tr>
                <td width="115"  class="tdHeaderVertical"><@global.text key='text.status'/>:</td>
                <td width="425"><@global.displayStatus hsmDevice.status!""/></td>
            </tr>
            </#if>
            <tr>
                <td colspan="2">
                    <input type="submit" name="save" value="<@global.text key='button.save'/>"/>
                    <#if hsmDevice.id??>
                        <input type="submit" name="delete" value="<@global.text key='button.delete'/>" onclick="deleteRecord();"/>
                        <input type="submit" value="<@global.text key='button.cancel'/>"
                               onclick="document.location.href = 'hsmDeviceView.html?id=${hsmDevice.id}'; return false;"/>
                    <#else>
                        <input type="submit" value="<@global.text key='button.cancel'/>"
                               onclick="document.location.href = 'hsmDeviceList.html'; return false;"/>
                    </#if>
                </td>
            </tr>
        </table>
    </form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
    <script type="text/javascript">
        function deleteRecord() {
            if (confirmWarning(event, '${deleteConfirmMsg}')) {
                document.getElementById('uForm').action = 'hsmDeviceDelete.html';
                document.getElementById('uForm').submit();
            }
        }
    </script>
</@global.page>