<#import "../common/global.ftl" as global/>
<script type="text/javascript" src="../js/signCSR.js"></script>
<script type="text/javascript" src="../js/calendar.js"></script>
<@global.page>
    <h1><a href="../main/main.html"><@global.text key='text.mainpage'/></a> &gt; <a
                href="certificateList.html?x=${.now?time}"><@global.text key='text.certificates'/></a> &gt;
        <@global.text key='text.text.sign.csr'/></h1>

    <#if msg?exists>
        <span class="msg">${msg?html}</span><br>
    </#if>
    <#if duplicate??>
        <span class="warn"><@global.text key='text.wrn.certificate.sdn.exists'/></span><br>
    </#if>

    <#if certificate??>
        <script type="text/javascript">

            function saveFile(filename, type, text) {
                var link = document.createElement("a");
                link.setAttribute("target", "_blank");
                if (Blob !== undefined) {
                    var blob = new Blob([text], {type: type});
                    link.setAttribute("href", URL.createObjectURL(blob));
                } else {
                    link.setAttribute("href", "data:text/plain," + encodeURIComponent(text));
                }
                link.setAttribute("download", filename);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        </script>
        <h2><@global.text key='text.text.cert.signed'/></h2>
        <form id="dummy" action="" method="post">
            <span style="width: 140px; display: inline-block;"><@global.text key='text.name'/>:</span>${certificate.name?html}
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.expires'/>:</span><#if certificate.expires??>${certificate.expires?string(dateFormat+" HH:mm:ss")}</#if>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.subjAltNames'/>:</span>${subjAltNames?html}
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.subjectDN'/>:</span>${certificate.subjectDN?html}
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.issuerDN'/>:</span>${certificate.issuerDN?html}
            <br>
            <pre id="cert">${certificate.x509data?html}</pre>
            <#-- <input type="button" value="<@global.text key='button.saveCert'/>" onclick='saveFile("Certificate.cer", "application/pkix-cert", document.getElementById("cert").innerHTML);'/> -->
            <input type="button" value="<@global.text key='button.downloadCert'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}'; return false;"/>
            <h2><@global.text key='text.text.cert.chain'/></h2>
            <pre id="chain">${chain?html}</pre>
            <#-- <input type="button" value="<@global.text key='button.saveChain'/>" onclick='saveFile("Chain.p7c", "application/pkcs7-mime", document.getElementById("chain").innerHTML);'/> -->
            <input type="button" value="<@global.text key='button.downloadchain'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}&chain=true'; return false;"/>
            <input type="button" value="<@global.text key='button.downloadchainp7'/>"
                   onclick="document.location.href = 'certificateDownload.html?id=${certificate.id?html}&chain=true&p7=true'; return false;"/>
        </form>

    <#elseif csr?? && csr.chain?? && csr.chain?length &gt; 0>

        <h2><@global.text key='text.text.cert.signed'/></h2>
        <form id="uForm" name="uForm" action="signCSR2.html" method="post">

            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.name'/>:</span>${csr.name?html}
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.expires'/>:</span><#if csr.expires??>${csr.expires?string(dateFormat+" HH:mm:ss")}</#if>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.subjAltNames'/>:</span>${(csr.subjAltNames!"")?html}
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.subjectDN'/>:</span>${csr.subjectDN?html}
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.issuerDN'/>:</span>${csr.issuerDN?html}
            <br>
            <span style="width: 140px; display: inline-block;">CSR (PEM Format):*</span>
            <@global.spring.formTextarea path="csr.pemData" attributes='style="margin: 0px; width: 700px; height: 280px;" readonly'/>
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.text.cert.chain'/>:</span>
            <textarea style="margin: 0px; width: 700px; height: 500px;" readonly>${csr.chain?xhtml}</textarea>
            <br>
            <#if duplicate??>
                <span style="width: 140px; display: inline-block;"><@global.text key='text.confirm.replace.cert'/>:</span>
                <input type="checkbox" name="replace" value="true"
                       onchange="document.getElementById('uploadBtn').disabled = !this.checked;" />
                <br>
            </#if>
            <input type="submit" name="uploadBtn" id="uploadBtn" value="<@global.text key='button.upload'/>" <#if duplicate??>disabled="true"</#if>/>
            <input type="button" value="<@global.text key='button.cancel'/>" onclick="document.location.href='signCSR.html'; return false;"/>

    <#elseif csr?? && caKeyMap?? && caKeyMap?size &gt; 0>
        <h2><@global.text key='text.text.sign.csr'/></h2>
        <form id="uForm" name="uForm" action="signCSR2.html" method="post" enctype="multipart/form-data">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.name'/>:*</span>
            <@global.spring.formInput path="csr.name" attributes='size="50"'/>
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.expires'/>:*</span>
            <@global.spring.formInput path="csr.expires" attributes='size="10" maxlength="10"'/>
            <a href="javascript:showCalPopup('deoico', 'expires', '${(dateFormat!"yyyy-mm-dd")?lower_case}', 'en');">
                <img id="deoico" align="baseline" border="0" src="../img/calicon.gif"/></a>
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.sig.cert'/>:*</span>
            <@global.spring.formSingleSelect path="csr.caKeyId" options=caKeyMap />
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.sig.alg'/>:*</span>
            <@global.spring.formSingleSelect path="csr.sigalg" options=sigAlgMap />
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.cert.extUse'/>:*</span>
            <@global.spring.formSingleSelect path="csr.extUse" options=extUsesMap />
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;">CSR (PEM Format):*</span>
            <@global.spring.formTextarea path="csr.pemData" attributes='style="margin: 0px; width: 700px; height: 280px;"'/>
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>
            <span style="width: 140px; display: inline-block;"><@global.text key='text.select.csr.file'/></span>

            <input type="hidden" id="fileSelect" name="fileSelect" value=""/>
            <input type="file" name="uploadFile" value="<@global.text key='button.select.csr.file'/>" onchange="populateOnFileSelect()"/>
            <@global.spring.bind path="csr.uploadFile"/>
            <@global.spring.showErrors separator="<br>" classOrStyle="error"/>
            <br>

            <input type="submit" name="signBtn" id="signBtn" value="<@global.text key='button.sign'/>" />
            <input type="button" value="<@global.text key='button.cancel'/>" onclick="document.location.href='signCSR.html'; return false;"/>
        </form>
    <#elseif caKeyMap?? && caKeyMap?size &gt; 0>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
</@global.page>