<#import "../common/global.ftl" as global/>
<@global.page>
    <div class="bottom">
        <h2><@global.text key="text.data.management"/></h2>
        <ul>
            <@global.security.authorize access="hasAnyRole('recordsView')">
                <li>
                    <a href="../tdsrecords/tdsrecords.html?x=${(.now?time)?html}"><@global.text key='text.authrecords'/></a>
                </li>
            </@global.security.authorize>
            <@global.security.authorize access="hasAnyRole('auditLogView')">
                <li><a href="../auditlogs/auditlogs.html?x=${(.now?time)?html}"><@global.text key='text.auditlogs'/></a>
                </li>
            </@global.security.authorize>
        </ul>

        <h2><@global.text key='text.system.setup'/></h2>
        <ul>
            <@global.security.authorize access="hasRole('userView')">
                <li><a href="../users/usersList.html?x=${(.now?time)?html}"><@global.text key='text.manage.users'/></a>
                </li>
            </@global.security.authorize>

            <@global.security.authorize access="hasAnyRole('issuerView')">
                <li>
                    <a href="../issuers/issuerList.html?x=${(.now?time)?html}"><@global.text key='text.manage.issuers'/></a>
                    (Issuers, BINs and ACS profiles)
                </li>
            </@global.security.authorize>
            <@global.security.authorize access="hasAnyRole('acquirerView')">
                <li>
                    <a href="../acquirers/acquirerList.html?x=${(.now?time)?html}"><@global.text key='text.manage.acquirers'/></a>
                    (Acquirers, MI profiles, Merchants)
                    <ul>
                        <li>
                            <a href="../acquirers/tdsServerList.html?x=${(.now?time)?html}"><@global.text key="text.tdsserverprofiles"/></a>
                        </li>
                        <li>
                            <a href="../acquirers/merchantList.html?x=${(.now?time)?html}"><@global.text key="text.merchants"/></a>
                        </li>
                    </ul>
                </li>
            </@global.security.authorize>
            <@global.security.authorize access="hasAnyRole('caView', 'caEdit')">
                <li>
                    <a href="../ca/certificateList.html?x=${(.now?time)?html}"><@global.text key='text.manage.certificates'/></a>
                    (Inbound Client SSL Certificates)
                </li>
                <@global.security.authorize access="hasAnyRole('caEdit')">
                    <li><a href="../ca/signCSR.html?x=${(.now?time)?html}"><@global.text key="text.sign.csr"/></a></li>
                </@global.security.authorize>
            </@global.security.authorize>

            <@global.security.authorize access="hasAnyRole('textView')">
                <li>
                    <a href="../localization/textsList.html?x=${(.now?time)?html}"><@global.text key='text.localization'/></a>
                    (Add or change texts of this app)
                </li>
            </@global.security.authorize>
            <@global.security.authorize access="hasAnyRole('adminSetup')">
                <li>
                    <a href="../settings/settingsList.html?x=${(.now?time)?html}"><@global.text key='text.settings'/></a>
                    (General app settings)
                </li>
            </@global.security.authorize>

        </ul>
        <div id="waitbox" style="display: none;" class="waitoverlay">
            <br/><br/>
            <b><@global.text key='text.please.wait'/></b>
        </div>
    </div>
</@global.page>
