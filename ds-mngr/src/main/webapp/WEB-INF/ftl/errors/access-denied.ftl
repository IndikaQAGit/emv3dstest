<#-- @ftlvariable name="ex" type="Exception" -->
<#import "../common/global.ftl" as global/>
<@global.page hideLogout=true>
    <#assign ctxPath = rc.getContextPath() >
    <span class="error" style="text-align:center;">
<#if error??>${error?html}<#else>You do not have sufficient rights to access this page or object</#if>
<br/>
<input type="submit" onclick="document.location.href = '${ctxPath?html}/main/main.html';" value="Continue main page">
</span>
    <p>

    </p>
</@global.page>
