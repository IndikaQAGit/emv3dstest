<#import "../common/global.ftl" as global/>
<#import "../utility/tooltip.ftl" as tooltip/>
<@global.page>
	<h1>
		<a href="../main/main.html">
			<@global.text key='text.mainpage'/>
		</a> &gt;
		<a href="usersList.html?x=${(.now?time)?html}">
			<@global.text key='text.users'/>
		</a> &gt;
		<#if user?? && user.id??>
			<a href="userView.html?id=${(user.getId()!"")?html}"><@global.text key='text.al.details'/>: ${(user.loginname!"")?html} </a>&gt;
		</#if>
		<span class="selected">
		<#if user?? && user.id??>
			<@global.text key='text.edit'/>
		<#else>
			<@global.text key='text.add.new'/>
		</#if>
		</span>
	</h1>
	<h2><@global.text key='text.user.details'/></h2>

	<#if user??>
		<form id="uForm" action="../users/userUpdate.html" method="post">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<@global.spring.formHiddenInput path='user.id'/>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.loginname'/>:*</td>
					<td>
					<@global.spring.formInput path="user.loginname" attributes='size="50" maxlength="32"'/>
					<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.firstName'/>:*</td>
					<td>
					<@global.spring.formInput path="user.firstName" attributes='size="50" maxlength="60"'/>
					<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.lastName'/>:*</td>
					<td>
					<@global.spring.formInput path="user.lastName" attributes='size="50" maxlength="60"'/>
					<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.email'/>*:</td>
					<td>
					<@global.spring.formInput path="user.email" attributes='size="50" maxlength="128"'/>
					<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='general.paymentSystem'/>:*</td>
					<td>
						<@global.spring.formSingleSelect path="user.paymentSystemId" options=paymentSystemUserMap attributes='style="width: 200px;"'/>
						<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.phone'/>:</td>
					<td>
					<@global.spring.formInput path="user.phone" attributes='size="30" maxlength="30"'/>
					<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.locale'/>:</td>
					<td><@global.spring.formSingleSelect path="user.locale" options=supportedLocales attributes='style=" width: 200px;"'/>
						<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.status'/>:*</td>
					<td>
						<#if (user.getStatus()?? && user.getStatus() == "T")>
						<@global.spring.formHiddenInput path="user.status"/>
							Template
							<#else>
							<@global.spring.formSingleSelect path="user.status" options={"A":"Active","B":"Blocked","L":"Temp. locked"} attributes='style=" width: 200px;"'/>
						</#if>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.roles'/>:</td>
					<td>
						<@global.spring.bind path="user.roles"/>
						<#list allroles?keys as value>
							<#assign id="${value}">
							<#assign isSelected = user.roles?? && user.roles?seq_contains(value)>
							<input type="checkbox" name="roles" value="${value?html}" id="${id}"
								<#if isSelected>checked="checked"</#if>
								<#-- only allow currentUser to set roles which it has itself. -->
								<#if !isSelfEdit && currentUser.roles?seq_contains(value)>
									data-hasaccess
								<#else>
									disabled
								</#if>
							>
							<label class="roleLabel" for="${id}" title="<@global.text key='text.tooltip.user.roles.${value}'/>">${allroles[value]?html}</label><br/>
						</#list>
						<input type="hidden" name="_roles" value="on"/>
						<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.setNewPass'/>:</td>
					<td>
						<@global.spring.formPasswordInput path="user.newPassword" attributes="oninput='getPasswordStrengthFeedback(this)' style=' background-color: #dddddd;' readonly='readonly' size='20' maxlength='40'"/>
						<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
						<input type="button" id="autobutton1" value="<@global.text key='password.auto'/>" onclick="javascript:showGenSecretPopUp('newPassword', 12,'${(sha1pnrg!"ABCDEFGHIJKLMNOPQRSTUVXYZ1234567890abcdefghijklmnopqrstuvxyz")?js_string}');"/>
						<input type="button" id="cancel1" value="<@global.text key='password.reset'/>" onclick="javascript:setValue('newPassword',''); unDisplay('passwordStrengthFeedback');"/>
						<span id="rndpopupnewPassword" class="natpopup"></span>
						<br/>
						<span id="passwordStrengthFeedback" style="display:none;"></span>
						<input id="currentPass" type="hidden" name="currentPass" value="auto"/>
					</td>
					<td>PCIDSS 3.2 8.2.6</td>
				</tr>
	 			<#if factor2Authentications??>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.factor2Authmethod'/>:</td>
					<td>
					<@global.spring.formSingleSelect path="user.factor2Authmethod" options=factor2Authentications />
					<@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td></td>
				</tr>
				</#if>
				<#if factor2Authentications?? || certificateAuthentications??>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.factor2AuthDeviceId'/>:</td>
					<td>
					 <@global.spring.formInput path="user.factor2AuthDeviceId" attributes='size="50" maxlength="100"'/>
					 <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td>
					For SMS OTP enter mobile phone number with country code<br/>
					For RSA secure id enter device number<br/>
					For PIN calculator enter device number<br/>
					</td>
				</tr>
				</#if>
				<#if certificateAuthentications??>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.factor2CertCN'/>:</td>
					<td>
					 <@global.spring.formInput path="user.factor2Data1" attributes='size="50" maxlength="250"'/>
					 <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td>
					For Certificate auth enter common name CN or ID code for ID card based<br/>
					</td>
				</tr>
				</#if>
				<#if factor2Authentications?? && user.factor2Data2??>
				<tr>
					<td class="tdHeaderVertical"><@global.text key='text.user.factor2MDIDUserId'/>:</td>
					<td>
					 <@global.spring.formInput path="user.factor2Data2" attributes='size="50" maxlength="100" readonly="readonly"'/>
					 <@global.spring.showErrors separator="<br/>" classOrStyle="error"/>
					</td>
					<td>Retrieved from Modirum ID AS Service on activation<br/></td>
				</tr>
				</#if>
				<tr>
					<td class="tdHeaderVertical">&nbsp;</td>
					<td colspan="2">
						<input type="submit" value="<@global.text key='button.save'/>" onclick="insertCheckedDisabledCheckBoxesAsHidden(document.getElementById('uForm'));"/>
						<#if user.id??>
							<input type="submit" value="<@global.text key='button.saveAsNew'/>" onclick="saveAsNewUser()"/>
							<input type="submit" value="<@global.text key='button.cancel'/>" onclick="document.location.href = 'userView.html?id=${user.id}'; return false;"/>
						<#else>
							<input type="submit" value="<@global.text key='button.cancel'/>" onclick="document.location.href = 'usersList.html'; return false;"/>
						</#if>
						<input type="submit" value="<@global.text key='button.saveAsNewTemplate'/>" onclick="saveAsNewTemplate()"/>
					</td>
				</tr>
			</table>
		</form>
    <#else>
        <span style=" color: red;"><@global.text key='text.object.not.found'/></span>
    </#if>
	<script type="text/javascript" src="../js/userEdit.js"></script>
</@global.page>
