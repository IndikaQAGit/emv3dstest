<#global loginPage = "true">
<#import "../common/global.ftl" as global/>
<@global.page hideLogout=true>
    <script type="text/javascript">
        // <!--
        if (top.location != self.location) {
            top.location = self.location.href
        }

        //-->

        function toggleUrl(url, mode) {
            var f = document.getElementById("lf");
            if (f != null) {
                f.action = url;
            }

            var pa = document.getElementById("password");
            if (pa != null) {
                if ("cert" == mode) {
                    pa.disabled = true;
                } else {
                    pa.disabled = false;
                }
            }
        }

    </script>
    <div id="main">
        <div id="content">
            <div id="content-header">
                <img src="../img/modirum-logo.svg"/>
            </div>
            <div id="content-login">
                <h2>Login</h2>
                <#if errorMessage??>
                    <div class="error">${(errorMessage!"")?html}</div>
                </#if>
                <#if initAdminDone??>
                    <div>${(initAdminDone!"")?html}</div>
                </#if>
                <form id="lf" name="f" action="../login" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <table>
                        <tr>
                            <td class="tdLogin"><label for="username">Username</label></td>
                            <td class="tdLogin"><input type="text" size="16" maxlength="32" name="username"
                                                       id="username" style="width: 165px;" autofocus tabindex="1"/></td>
                        </tr>
                        <tr>
                            <td class="tdLogin"><label for="password">Password</label></td>
                            <td class="tdLogin"><input type="password" size="16" maxlength="32" name="password"
                                                       id="password" style="width: 165px;"/></td>
                        </tr>
                        <#if certAuthUri??>
                            <tr>
                                <td class="tdLogin"><label for="password">Mode</label></td>
                                <td class="tdLogin">
                                    <input type="radio" name="mode" value="password" checked="checked"
                                           onclick="toggleUrl('../login', 'pass')">Password
                                    <input type="radio" name="mode" value="cerificate"
                                           onclick="toggleUrl('${certAuthUri?js_string?html}', 'cert')"> ID
                                    smartcard/certificate
                                </td>
                            </tr>
                        </#if>
                        <tr>
                            <td class="tdLogin">&nbsp;</td>
                            <td class="tdLogin"><input type="submit" name="button2" id="button" value="Login"/></td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="content-footer">
                <img src="../img/powered-by-modirum.svg"/>
                <div id="footer-right">
                </div>
            </div>
        </div>
    </div>
</@global.page>