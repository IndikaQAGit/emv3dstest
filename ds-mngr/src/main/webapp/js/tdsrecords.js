function submitSearchForm(formAction, isShowWait) {
    if (isShowWait) {
        showWaitBox();
    }
    document.getElementById("action").value = formAction;
    var searchForm = document.getElementById("uSearchForm");
    if (formAction == 'export') {
    	searchForm.action = 'export.html';  
    }
    searchForm.submit();
    searchForm.action = 'tdsrecords.html';
}

function exportCSV(formAction, isShowWait) {
	 var divModal = document.getElementById('exportModal')
	 divModal.style.display = "block";
	 setTimeout(function() {
		 divModal.style.display = "none";
		 submitSearchForm(formAction, isShowWait);
	 }, "3000");
}