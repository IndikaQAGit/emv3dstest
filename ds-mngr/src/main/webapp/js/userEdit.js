var roleMap = new Map();

if (window.addEventListener) {
    window.addEventListener('load', loadPage());
} else {
    // IE versions <9 support
    window.attachEvent('onload', loadPage());
}

function loadPage() {
    cacheRoles();
    setupEventListeners();
    for (let role of roleMap.keys()) {
        if (isEditRole(role)) {
            toggleEditRole(role);
        }
    }
}

function setupEventListeners() {
    for (let role of roleMap.keys()) {
        if (isEditRole(role)) {
            document.getElementById(role).onchange = function() {
                toggleEditRole(role);
            }
        }
    }
}

function cacheRoles() {
    var roles = document.getElementsByName('roles');
    roles.forEach(function(role) {
        let roleName = role.value;
        if (!roleMap.has(roleName)) {
            let coreRoleName = roleName.replace(/edit|view/i, '');
            roleMap.set(roleName, coreRoleName);
        }
    });
}

function insertHiddenInputToForm(form, fieldName, fieldValue) {
    if (fieldName && fieldValue) {
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = fieldName;
        input.value = fieldValue;
        form.appendChild(input);
    }
}

function insertCheckedDisabledCheckBoxesAsHidden(form) {
    for (let i = 0; i < form.elements.length; i++ ) {
        if (form.elements[i].type == 'checkbox') {
            if (form.elements[i].disabled == true && form.elements[i].checked == true) {
                insertHiddenInputToForm(form, 'roles', form.elements[i].value);
            }
        }
    }
}

function saveAsNewUser() {
    var form = document.getElementById('uForm');
    insertCheckedDisabledCheckBoxesAsHidden(form);
    insertHiddenInputToForm(form, 'newUser', 'true');
    form.submit();
};

function saveAsNewTemplate() {
    var form = document.getElementById('uForm');
    insertCheckedDisabledCheckBoxesAsHidden(form);
    insertHiddenInputToForm(form, 'newTemplate', 'true');
    form.submit();
};

function toggleEditRole(editRole) {
    var roleName = roleMap.get(editRole);
    toggleCheckbox(editRole, roleName + 'View');
}

function toggleCheckbox(parent, child, toggleChecked = true, toggleReadonly = true) {
    var parentElem = document.getElementById(parent);
    var parentHiddenElem = document.getElementById(parent + "_hidden");
    var childElem = document.getElementById(child);
    var childHiddenElem = document.getElementById(child + "_hidden");

    // save parent checkbox in a hidden input field if it can be disabled
    if (parentElem && parentHiddenElem) {
        parentHiddenElem.value = parentElem.checked ? parentElem.value : '';
    }

    // assign child checkbox' checked state based on parent checkbox
    if (toggleChecked && parentElem && childElem) {
        if (parentElem.checked) {
            childElem.checked = true;
        }
    }

    // assign child checkbox' readonly state based on parent checkbox
    if (toggleReadonly && parentElem && childElem) {
        if (parentElem.checked) {
            childElem.setAttribute('disabled', 'disabled');
        } else if (childElem.hasAttribute('data-hasaccess')) {
            childElem.removeAttribute('disabled');
        }
        if (childHiddenElem) {
            childHiddenElem.value = childElem.checked ? childElem.value : '';
        }
    }
}

function isEditRole(role) {
    return role.match(/edit/i);
}