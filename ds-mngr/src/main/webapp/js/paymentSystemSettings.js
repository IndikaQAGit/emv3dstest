function edit(editButton){
    let parentTr = editButton.closest('tr');
    let displayOnlyValue = parentTr.querySelector('.settings-value');
    let inputValue = parentTr.querySelector('.settings-value-input');
    hideElement(displayOnlyValue);
    showElement(inputValue);
    prepareInputValue(inputValue, displayOnlyValue);

    let spanComment = parentTr.querySelector('span.settings-comment');
    let textAreaComment = parentTr.querySelector('textarea.settings-comment');
    hideElement(spanComment);
    showElement(textAreaComment);
    textAreaComment.value = spanComment.innerText;
    textAreaComment.innerText = spanComment.innerText;

    let buttonParent = editButton.closest('td');
    let cancelButton = buttonParent.querySelector('.cancelButton');
    let saveButton = buttonParent.querySelector('.saveButton');
    hideElement(editButton);
    showElement(cancelButton);
    showElement(saveButton);
}

function cancel(cancelButton){
    let buttonParent = cancelButton.closest('td');
    let saveButton = buttonParent.querySelector('.saveButton');
    let editButton = buttonParent.querySelector('.editButton');
    hideElement(cancelButton);
    hideElement(saveButton);
    showElement(editButton);

    let parentTr = cancelButton.closest('tr');
    let displayOnlyValue = parentTr.querySelector('.settings-value');
    let inputValue = parentTr.querySelector('.settings-value-input');
    showElement(displayOnlyValue);
    hideElement(inputValue);

    let spanComment = parentTr.querySelector('span.settings-comment');
    let textAreaComment = parentTr.querySelector('textarea.settings-comment');
    showElement(spanComment);
    hideElement(textAreaComment);
}

function hideElement(elem){
    elem.style.display = "none";
}

function showElement(elem){
    elem.style.display = "";
}

function prepareInputValue(inputValue, displayOnlyValue){
    let type = inputValue.type;
    if (type == 'textarea' || type == 'text') {
        inputValue.value = displayOnlyValue.innerText;
        inputValue.innerText = displayOnlyValue.innerText;
    } else if (type == 'checkbox') {
        inputValue.value = displayOnlyValue.value;
        inputValue.checked = displayOnlyValue.checked;
    }
}

function onCheckboxChange(checkbox) {
    if (checkbox.checked) {
        checkbox.value = true;
    } else {
        checkbox.value = false;
    }
}
