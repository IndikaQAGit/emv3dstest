## DS Reporting CLI

### How to Run DS Report

1. Build the DS maven project
2. Copy and unzip the ds-reporting.zip under target folder of ds-reporting module
3. Within the extracted folder, ds-reporting/conf/reportConfig.template.properties, you may configure the database settings for reporting source database, audit logs database and csv settings.
4. Within the extracted folder, execute the command line below against ds-reporting.jar
```bash
java -jar ds-reporting.jar \
--config conf/reportConfig.template.properties 
```

### Main Class CLI Parameters

#### --config
The sample `conf/reportConfig.template.properties` is expected to contain
the reporting configurations that includes:
- reporting source database settings (reporting.*)
- audit logs database settings which can be the 
  same as the reporting source database (audit.*)
- reporting output base directory (reporting.dir)
- csv settings (csv.*)

