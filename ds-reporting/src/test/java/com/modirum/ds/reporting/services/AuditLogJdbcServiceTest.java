package com.modirum.ds.reporting.services;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.RunWithDB;
import com.modirum.ds.reporting.model.AuditLog;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.model.rowmapper.AuditLogResultSetMapper;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import com.modirum.ds.reporting.util.JdbcUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

@RunWithDB
public class AuditLogJdbcServiceTest {
    private static DataSource dataSource= TestEmbeddedDbUtil.dataSource();
    private static AuditLogJdbcService auditLogJdbcService = ServiceLocator.initInstance(dataSource).getAuditLogJdbcService();

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeScriptsBefore = {"/services/auditlog-service.sql"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_auditlog;", "TRUNCATE TABLE ds_ids;"})
    public void logAction() throws Exception {
        User user = new User(0L, "system");
        auditLogJdbcService.logAction(user, null, "Test logAction", "AuditLogJdbcServiceTest.logAction() is tested.");

        AuditLog auditLog = JdbcUtils.findFirst(dataSource, "SELECT * from ds_auditlog", null, new AuditLogResultSetMapper());
        Assertions.assertNotNull(auditLog);
        Assertions.assertEquals("Test logAction", auditLog.getAction());
        Assertions.assertEquals("AuditLogJdbcServiceTest.logAction() is tested.", auditLog.getDetails());
        Assertions.assertEquals(Long.valueOf(2L), auditLog.getId());
        Assertions.assertEquals(user.getId(), auditLog.getById());
        Assertions.assertEquals(user.getLoginName(), auditLog.getBy());
    }
}
