package com.modirum.ds.reporting;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.reporting.enums.AuditLogAction;
import com.modirum.ds.reporting.enums.ReportConfigKeys;
import com.modirum.ds.reporting.model.AuditLog;
import com.modirum.ds.reporting.model.rowmapper.AuditLogResultSetMapper;
import com.modirum.ds.reporting.testutil.TestEmbeddedDbUtil;
import com.modirum.ds.reporting.testutil.TestReportPropertiesUtil;
import com.modirum.ds.reporting.testutil.TestResourceUtil;
import com.modirum.ds.reporting.reports.TransactionReport;
import com.modirum.ds.reporting.util.JdbcUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

@RunWithDB
public class DsReportDaemonTest {
    private static DataSource dataSource = TestEmbeddedDbUtil.dataSource();
    private static Properties reportConfigProperties;
    private DsReportDaemon dsReportDaemon = new DsReportDaemon();

    private void interrupt(long milliseconds) {
        Thread thread = Thread.currentThread();

        new Timer().schedule(new TimerTask() {
            public void run() {
                thread.interrupt();
            }
        }, milliseconds);
    }

    @BeforeAll
    public static void beforeAll() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/mngr-texts.sql");

        reportConfigProperties = TestReportPropertiesUtil.loadResourceProperties("reportConfigTemplate.properties");
        String reportingDirRelativePath = reportConfigProperties.getProperty(ReportConfigKeys.REPORTING_DIR);
        //also stores relative test directory for unit test's file cleanup operations
        reportConfigProperties.setProperty("reporting.test.dir", reportingDirRelativePath);
        TestResourceUtil.createTempDirectory(reportingDirRelativePath);
        //absolute url is used by the Report Daemon and decoupled from unit test resources
        TestReportPropertiesUtil.convertPropertyToAbsolutePath(reportConfigProperties, ReportConfigKeys.REPORTING_DIR);

        //need to store the report property file to different path so as not to overwritten the original
        //overwriting the original file will cause issues when multiple attempts are done
        //instead always start from fresh copy from a different file
        TestReportPropertiesUtil.saveProperties(reportConfigProperties, "reportConfig.properties");
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @BeforeEach
    public void beforeEach() throws Exception {
        dsReportDaemon = new DsReportDaemon();
    }

    @AfterEach
    public void afterEach() throws Exception {
        TestResourceUtil.cleanDirectory(reportConfigProperties.getProperty("reporting.test.dir"));
    }

    @Test
    @DataSet(executeScriptsBefore = {"transaction-reports/transaction-reports.sql"},
            executeStatementsBefore = {"INSERT INTO ds_reports (id, paymentSystemId, status, name, dateFrom, dateTo, " +
                    " createdDate, createdUser, lastModifiedDate, lastModifiedBy, criteria)" +
                    " VALUES(1, 1, 'Q',  'TransactionReport', '2020-07-23 07:00:00', '2020-07-23 08:00:00', " +
                    "'2021-03-08 14:14:03.897000', 'john', '2021-03-08 14:14:03.897000', 'john', '{}');"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;",
                                      "TRUNCATE TABLE ds_tdsrecords;",
                                      "TRUNCATE TABLE ds_ids;",
                                      "TRUNCATE TABLE ds_auditlog;",
                                      "TRUNCATE TABLE ds_tdsrecord_attributes;",
                                      "TRUNCATE TABLE ds_paymentsystems;",
                                      "TRUNCATE TABLE ds_reports;",
                                      "TRUNCATE TABLE ds_acquirers;",
                                      "TRUNCATE TABLE ds_issuers;"})
    public void generateReportWithDefaultTimezoneAndLocale() throws Exception {

        interrupt(1500);

        dsReportDaemon.start(new String[]{
                "--config", TestResourceUtil.getPath("reportConfig.properties"),
        });
        AuditLog auditLog = JdbcUtils.findFirst(dataSource, "SELECT * from ds_auditlog", null, new AuditLogResultSetMapper());
        Assertions.assertNotNull(auditLog);
        Assertions.assertEquals(AuditLogAction.CREATE_REPORT, auditLog.getAction());
        Assertions.assertEquals("ReportClassName[TransactionReport] has been generated with the reportFile=" + TestResourceUtil.getFirstReportFile("report-output-folder").toURI().getPath(), auditLog.getDetails());

        String expectedCsv = String.join(",", TransactionReport.HEADERS) + System.lineSeparator() +
                             "Default PS,2020-07-23,07:44:25,470020,2.2.0,02,01,28,400000,0000,2404,216.58.197.110,John Smith,Oklahoma City,840,54 E Pear St,,,73102,OK,Oklahoma City,840,54 E Pear St,,,73102,OK,+1123456789,02,10600,840,2,106.00 USD (840),2017-01-27 07:54:24,20170127,1234,05,Test Acquirer Name,444444,Modirum DS-3DS-Emulator,000001,246,Finland,5499,\"{\"\"shipIndicator\"\":\"\"07\"\"}\",,,DS-3DS-EMULATOR-REF-NUMBER,b0f61cfe-d04f-446a-b648-8431b395ca0b,https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp,03,DS-3DS-EMULATOR-OPER-ID,01,http://localhost,7979791,Modirum DS-3DS-Emulator,Test DS-3DS-EMULATOR issuer,400000,20b01757-0eab-5931-8000-000000072c04,EMVCo1234567,https://dsmngr.local.com/ds/DServer,,,,ACS-EMULATOR-REF-NO,14960eb4-b1f6-42c4-8b06-bd0877cfd064,http://localhost/ds-3ds-emulator/acsAreqStub.jsp,ACS-EMULATOR-OPER-ID,01,,,,,,true,01,,,,2020-07-23 07:44:25,2020-07-23 07:44:25,2020-07-23 07:44:29,2020-07-23 07:44:38,,,,,Y,Authentication Successful,,,Y,01,Completed,Yes,,,,,," + System.lineSeparator();
        String actualCsv = TestResourceUtil.readFirstReportFileAsString("report-output-folder");
        Assertions.assertEquals(expectedCsv, actualCsv);

    }

    @Test
    @DataSet(executeScriptsBefore = {"transaction-reports/transaction-reports.sql"},
            executeStatementsBefore = {"INSERT INTO ds_reports (id, paymentSystemId, status, name, dateFrom, dateTo, " +
                    " createdDate, createdUser, lastModifiedDate, lastModifiedBy, criteria)" +
                    " VALUES(1, -1, 'Q',  'TransactionReport', '2020-07-23 15:00:00', '2020-07-23 16:00:00', " +
                    "'2021-03-08 14:14:03.897000', 'john', '2021-03-08 14:14:03.897000', 'john', '{\"timezone\":\"Asia/Singapore\"}');"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;",
                                      "TRUNCATE TABLE ds_tdsrecords;",
                                      "TRUNCATE TABLE ds_ids;",
                                      "TRUNCATE TABLE ds_auditlog;",
                                      "TRUNCATE TABLE ds_tdsrecord_attributes;",
                                      "TRUNCATE TABLE ds_paymentsystems;",
                                      "TRUNCATE TABLE ds_reports;",
                                      "TRUNCATE TABLE ds_acquirers;",
                                      "TRUNCATE TABLE ds_issuers;"})
    public void generateReportWithOtherTimeZone() throws Exception {

        interrupt(1500);

        dsReportDaemon.start(new String[]{
                "--config", TestResourceUtil.getPath("reportConfig.properties"),
        });
        AuditLog auditLog = JdbcUtils.findFirst(dataSource, "SELECT * from ds_auditlog", null, new AuditLogResultSetMapper());
        Assertions.assertNotNull(auditLog);
        Assertions.assertEquals(AuditLogAction.CREATE_REPORT, auditLog.getAction());
        Assertions.assertEquals("ReportClassName[TransactionReport] has been generated with the reportFile=" + TestResourceUtil.getFirstReportFile("report-output-folder").toURI().getPath(), auditLog.getDetails());

        String expectedCsv = String.join(",", TransactionReport.HEADERS) + System.lineSeparator() +
                             "Default PS,2020-07-23,15:44:25,470020,2.2.0,02,01,28,400000,0000,2404,216.58.197.110,John Smith,Oklahoma City,840,54 E Pear St,,,73102,OK,Oklahoma City,840,54 E Pear St,,,73102,OK,+1123456789,02,10600,840,2,106.00 USD (840),2017-01-27 15:54:24,20170127,1234,05,Test Acquirer Name,444444,Modirum DS-3DS-Emulator,000001,246,Finland,5499,\"{\"\"shipIndicator\"\":\"\"07\"\"}\",,,DS-3DS-EMULATOR-REF-NUMBER,b0f61cfe-d04f-446a-b648-8431b395ca0b,https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp,03,DS-3DS-EMULATOR-OPER-ID,01,http://localhost,7979791,Modirum DS-3DS-Emulator,Test DS-3DS-EMULATOR issuer,400000,20b01757-0eab-5931-8000-000000072c04,EMVCo1234567,https://dsmngr.local.com/ds/DServer,,,,ACS-EMULATOR-REF-NO,14960eb4-b1f6-42c4-8b06-bd0877cfd064,http://localhost/ds-3ds-emulator/acsAreqStub.jsp,ACS-EMULATOR-OPER-ID,01,,,,,,true,01,,,,2020-07-23 15:44:25,2020-07-23 15:44:25,2020-07-23 15:44:29,2020-07-23 15:44:38,,,,,Y,Authentication Successful,,,Y,01,Completed,Yes,,,,,," + System.lineSeparator();
        String actualCsv = TestResourceUtil.readFirstReportFileAsString("report-output-folder");
        Assertions.assertEquals(expectedCsv, actualCsv);

    }

    @Test
    @DataSet(executeScriptsBefore = {"transaction-reports/transaction-reports.sql"},
            executeStatementsBefore = {"INSERT INTO ds_reports (id, paymentSystemId, status, name, dateFrom, dateTo, " +
                    " createdDate, createdUser, lastModifiedDate, lastModifiedBy, criteria)" +
                    " VALUES(1, -1, 'Q',  'TransactionReport', '2020-07-23 07:00:00', '2020-07-23 08:00:00', " +
                    "'2021-03-08 14:14:03.897000', 'john', '2021-03-08 14:14:03.897000', 'john', '{}');"},
            executeStatementsAfter = {"TRUNCATE TABLE ds_tdsmessages;",
                                      "TRUNCATE TABLE ds_tdsrecords;",
                                      "TRUNCATE TABLE ds_ids;",
                                      "TRUNCATE TABLE ds_auditlog;",
                                      "TRUNCATE TABLE ds_tdsrecord_attributes;",
                                      "TRUNCATE TABLE ds_paymentsystems;",
                                      "TRUNCATE TABLE ds_reports;",
                                      "TRUNCATE TABLE ds_acquirers;",
                                      "TRUNCATE TABLE ds_issuers;"})
    public void generateTransactionReportWithoutPaymentSystemId() throws Exception {

        interrupt(1500);

        dsReportDaemon.start(new String[]{
                "--config", TestResourceUtil.getPath("reportConfig.properties"),
        });
        AuditLog auditLog = JdbcUtils.findFirst(dataSource, "SELECT * from ds_auditlog", null, new AuditLogResultSetMapper());
        Assertions.assertNotNull(auditLog);
        Assertions.assertEquals(AuditLogAction.CREATE_REPORT, auditLog.getAction());
        Assertions.assertEquals("ReportClassName[TransactionReport] has been generated with the reportFile=" + TestResourceUtil.getFirstReportFile("report-output-folder").toURI().getPath(), auditLog.getDetails());

        String expectedCsv = String.join(",", TransactionReport.HEADERS) + System.lineSeparator() +
                             "Default PS,2020-07-23,07:44:25,470020,2.2.0,02,01,28,400000,0000,2404,216.58.197.110,John Smith,Oklahoma City,840,54 E Pear St,,,73102,OK,Oklahoma City,840,54 E Pear St,,,73102,OK,+1123456789,02,10600,840,2,106.00 USD (840),2017-01-27 07:54:24,20170127,1234,05,Test Acquirer Name,444444,Modirum DS-3DS-Emulator,000001,246,Finland,5499,\"{\"\"shipIndicator\"\":\"\"07\"\"}\",,,DS-3DS-EMULATOR-REF-NUMBER,b0f61cfe-d04f-446a-b648-8431b395ca0b,https://dsmngr.local.com/ds-3ds-emulator/3dsServerRreqStub.jsp,03,DS-3DS-EMULATOR-OPER-ID,01,http://localhost,7979791,Modirum DS-3DS-Emulator,Test DS-3DS-EMULATOR issuer,400000,20b01757-0eab-5931-8000-000000072c04,EMVCo1234567,https://dsmngr.local.com/ds/DServer,,,,ACS-EMULATOR-REF-NO,14960eb4-b1f6-42c4-8b06-bd0877cfd064,http://localhost/ds-3ds-emulator/acsAreqStub.jsp,ACS-EMULATOR-OPER-ID,01,,,,,,true,01,,,,2020-07-23 07:44:25,2020-07-23 07:44:25,2020-07-23 07:44:29,2020-07-23 07:44:38,,,,,Y,Authentication Successful,,,Y,01,Completed,Yes,,,,,," + System.lineSeparator();
        String actualCsv = TestResourceUtil.readFirstReportFileAsString("report-output-folder");
        Assertions.assertEquals(expectedCsv, actualCsv);

    }

}