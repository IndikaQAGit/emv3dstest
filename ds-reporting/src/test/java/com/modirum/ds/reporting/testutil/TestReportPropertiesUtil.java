package com.modirum.ds.reporting.testutil;

import com.modirum.ds.reporting.enums.ReportConfigKeys;
import com.modirum.ds.reporting.util.PropertiesUtil;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Helper methods for Properties used in DS Reports.
 */
public class TestReportPropertiesUtil {
    /**
     * Common report Properties
     *
     * @return
     */
    public static Properties reportProperties() {
        Properties properties = new Properties();
        properties.setProperty(ReportConfigKeys.CSV_FILTERING_ENABLED, "true");
        properties.setProperty(ReportConfigKeys.CSV_PROHIBITED_CHARACTERS_REGEX, "(\\\\t|\\\\n|\\\\r)");
        properties.setProperty(ReportConfigKeys.CSV_SANITIZING_ENABLED, "true");
        properties.setProperty(ReportConfigKeys.CSV_UNSAFE_STARTING_CHARACTERS, "=+-@");
        properties.setProperty(ReportConfigKeys.CSV_INJECTION_SANITIZER, "'");
        return properties;
    }

    /**
     * Updates properties file referred by the resourceRelativePath with the given Properties.
     *
     * @param properties
     * @param resourceRelativePath
     * @throws IOException
     */
    public static void saveProperties(Properties properties, String resourceRelativePath) throws IOException, URISyntaxException {
        String resourceAbsolutePath = TestResourceUtil.getBasePath() + "/" + resourceRelativePath;
        try (FileOutputStream fos = new FileOutputStream(resourceAbsolutePath)) {
            properties.store(fos, null);
        }
    }

    /**
     * Converts the property value from relative path to absolute path under test resource.
     *
     * @param properties  Properties object containing the property to be modified.
     * @param propertyKey the key of the property to be converted to absolute path.
     */
    public static void convertPropertyToAbsolutePath(Properties properties, String propertyKey) throws URISyntaxException {
        properties.put(propertyKey, TestResourceUtil.getPath(properties.getProperty(propertyKey)));
    }

    /**
     * This loads a properties file defined by the relative resource path into Properties object.
     *
     * @param resourceRelativePath
     * @return
     * @throws IOException
     */
    public static Properties loadResourceProperties(String resourceRelativePath) throws IOException, URISyntaxException {
        return PropertiesUtil.loadFromFile(TestResourceUtil.getPath(resourceRelativePath));
    }
}
