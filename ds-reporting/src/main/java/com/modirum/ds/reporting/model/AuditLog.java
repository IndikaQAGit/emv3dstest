package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuditLog extends PersistableClass implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long objectId;
    private String objectClass;
    private Date when;
    private String by;
    private Long byId;
    private String action;
    private String details;

}
