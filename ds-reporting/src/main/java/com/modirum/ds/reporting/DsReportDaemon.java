package com.modirum.ds.reporting;

import com.modirum.ds.reporting.enums.AuditLogAction;
import com.modirum.ds.reporting.enums.ReportConfigKeys;
import com.modirum.ds.reporting.model.User;
import com.modirum.ds.reporting.reports.DsReport;
import com.modirum.ds.reporting.reports.ReportParameters;
import com.modirum.ds.reporting.services.AuditLogJdbcService;
import com.modirum.ds.reporting.services.ServiceLocator;
import com.modirum.ds.reporting.util.JdbcUtils;
import com.modirum.ds.reporting.util.PropertiesUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Entrypoint main class for generating report in DS.
 */
public class DsReportDaemon {
    private final Properties reportConfigProps = new Properties();
    static {
        //set the default logback.xml location if not specified
        if (StringUtils.isEmpty(System.getProperty("logback.configurationFile"))) {
            System.setProperty("logback.configurationFile", "conf/logback.xml");
        }
    }
    private static final Logger log = LoggerFactory.getLogger(DsReportDaemon.class);
    ExecutorService executor = null;

    private boolean singleThreadMode = false;
    private static final int DEFAULT_POOL_SIZE = 10;
    private Integer deleteReportsDaysOld = null;
    private int sleepTime = 4000;

    public static void main(String[] args) {
        try {
            new DsReportDaemon().start(args);
        } catch (Exception e) {
            log.error("Error encountered ", e);
            System.exit(1);
        }
        System.exit(0);
    }

    /**
     * Main API for the report daemon that can be called even in non-cli way.
     * @param args
     * @throws Exception
     */
    public void start(String[] args) throws Exception {
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("--config")) {
                String configFile = args[i + 1];
                reportConfigProps.putAll(PropertiesUtil.loadFromFile(configFile));
                i++;
            }
        }

        DataSource reportingDS = JdbcUtils.createDataSource(reportConfigProps.getProperty(ReportConfigKeys.REPORTING_DB_DRIVER),
                                                            reportConfigProps.getProperty(ReportConfigKeys.REPORTING_DB_URL),
                                                            reportConfigProps.getProperty(ReportConfigKeys.REPORTING_DB_USER),
                                                            reportConfigProps.getProperty(ReportConfigKeys.REPORTING_DB_PASS),
                                                            reportConfigProps.getProperty(ReportConfigKeys.REPORTING_VALIDATION_QUERY, "SELECT 1"));
        ServiceLocator.initInstance(reportingDS);

        ServiceLocator.getInstance()
                      .getCsvFilteringService().initializeFromProperties(reportConfigProps);



        if (reportConfigProps.containsKey(ReportConfigKeys.DELETE_REPORTS_DAYS_OLD)) {
            try {
                deleteReportsDaysOld = Integer.parseInt(reportConfigProps.getProperty(ReportConfigKeys.DELETE_REPORTS_DAYS_OLD));
            } catch (Exception e) {
                log.error("Could not parse " + ReportConfigKeys.DELETE_REPORTS_DAYS_OLD, e);
            }
        }

        if (reportConfigProps.containsKey(ReportConfigKeys.SLEEP_TIME_SECONDS)) {
            try {
                sleepTime = Integer.parseInt(reportConfigProps.getProperty(ReportConfigKeys.SLEEP_TIME_SECONDS)) * 1000;
            } catch (Exception e) {
                log.error("Could not parse " + ReportConfigKeys.SLEEP_TIME_SECONDS, e);
            }
        }

        if (reportConfigProps.containsKey(ReportConfigKeys.SINGLE_THREADED)) {
            singleThreadMode = Boolean.parseBoolean(reportConfigProps.getProperty(ReportConfigKeys.SINGLE_THREADED));
        }

        String reportBaseDir = reportConfigProps.getProperty(ReportConfigKeys.REPORTING_DIR);

        DataSource auditDS = JdbcUtils.createDataSource(reportConfigProps.getProperty(ReportConfigKeys.AUDIT_DB_DRIVER),
                                                        reportConfigProps.getProperty(ReportConfigKeys.AUDIT_DB_URL),
                                                        reportConfigProps.getProperty(ReportConfigKeys.AUDIT_DB_USER),
                                                        reportConfigProps.getProperty(ReportConfigKeys.AUDIT_DB_PASS),
                                                        reportConfigProps.getProperty(ReportConfigKeys.AUDIT_VALIDATION_QUERY, "SELECT 1"));
        AuditLogJdbcService auditLogJdbcService = ServiceLocator.getInstance().getAuditLogJdbcService();
        auditLogJdbcService.setDataSource(auditDS);

        if (singleThreadMode) {
            executor = Executors.newSingleThreadExecutor();
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    log.debug("Processing single threaded...");
                    executor.submit(new ReportFactoryImpl(log, reportingDS, auditLogJdbcService, deleteReportsDaysOld, reportBaseDir));
                    Thread.sleep(sleepTime);
                } catch (InterruptedException ex) {
                    log.debug("Interrupt received, shutting down...");
                    Thread.currentThread().interrupt();
                }
            }
        } else {

            int poolSize = DEFAULT_POOL_SIZE;
            if (reportConfigProps.containsKey(ReportConfigKeys.REPORTS_POOL_SIZE)) {
                try {
                    poolSize = Integer.parseInt(reportConfigProps.getProperty(ReportConfigKeys.REPORTS_POOL_SIZE));
                } catch (Exception e) {
                    log.error("Could not parse " + ReportConfigKeys.REPORTS_POOL_SIZE, e);
                }
            }

            executor = Executors.newFixedThreadPool(poolSize);
            ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor)executor;

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    log.debug("Processing...");
                    executor.submit(new ReportFactoryImpl(log, reportingDS, auditLogJdbcService, deleteReportsDaysOld, reportBaseDir));

                    log.debug("ReportDaemon: active threads=" + threadPoolExecutor.getActiveCount()
                            + " out of " + threadPoolExecutor.getMaximumPoolSize()
                            + ", awaiting execution=" + threadPoolExecutor.getQueue().size()
                            + ", total completed=" + threadPoolExecutor.getCompletedTaskCount());

                    Thread.sleep(sleepTime);
                } catch (InterruptedException ex) {
                    log.debug("Interrupt received, shutting down...");
                    Thread.currentThread().interrupt();
                }
            }
        }

        executor.shutdown();

    }
}
