package com.modirum.ds.reporting.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enumerated values for User Status. This contains both the actual value
 * stored in database and also the counterpart human-readable text.
 */
public enum UserStatus {
    ACTIVE("A", "Active"),
    DISABLED("D", "Blocked"),
    TEMP_LOCKED("L", "Temp. Locked");

    private String value;
    private String text;
    UserStatus(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public static UserStatus fromValue(String value) {
        Optional<UserStatus> optional = Arrays.stream(UserStatus.values())
              .filter(userStatus -> userStatus.value.equals(value))
              .findFirst();

        return optional.isPresent() ? optional.get() : null;
    }

    public String getValue() {
        return this.value;
    }
    public String getText() {
        return this.text;
    }
}
