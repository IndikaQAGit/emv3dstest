package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentSystem extends PersistableClass implements Comparable<PaymentSystem> {

    private Integer id;
    private String name;
    private String type;
    private Short port;

    @Override
    public int compareTo(PaymentSystem o) {
        if (o == null || o.getName() == null) return -1;
        if (this.getName() == null) return 1;
        return this.getName().compareToIgnoreCase(o.getName());
    }

}
