package com.modirum.ds.reporting.model.rowmapper;

import com.modirum.ds.db.model.Acquirer;
import com.modirum.ds.reporting.util.JdbcUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AcquirerResultSetMapper implements ResultSetMapper<Acquirer> {

    @Override
    public Acquirer map(ResultSet resultSet) throws SQLException {
        Acquirer acquirer = new Acquirer();
        acquirer.setId(JdbcUtils.getColumnValue(resultSet, "id", Integer.class));
        acquirer.setPaymentSystemId(JdbcUtils.getColumnValue(resultSet, "paymentSystemId", Integer.class));
        acquirer.setBIN(JdbcUtils.getColumnValue(resultSet, "BIN", String.class));
        acquirer.setName(JdbcUtils.getColumnValue(resultSet, "name", String.class));
        acquirer.setPhone(JdbcUtils.getColumnValue(resultSet, "phone", String.class));
        acquirer.setEmail(JdbcUtils.getColumnValue(resultSet, "email", String.class));
        acquirer.setAddress(JdbcUtils.getColumnValue(resultSet, "address", String.class));
        acquirer.setCity(JdbcUtils.getColumnValue(resultSet, "city", String.class));
        acquirer.setCountry(JdbcUtils.getColumnValue(resultSet, "country", String.class));
        acquirer.setStatus(JdbcUtils.getColumnValue(resultSet, "status", String.class));
        acquirer.setIsId(JdbcUtils.getColumnValue(resultSet, "isid", Byte.class));
        return acquirer;
    }
}