package com.modirum.ds.reporting.services;

import com.modirum.ds.reporting.model.TDSMessageData;
import com.modirum.ds.reporting.model.rowmapper.TDSMessageDataResultSetMapper;
import com.modirum.ds.reporting.util.JdbcUtils;

import javax.sql.DataSource;
import java.util.List;

/**
 * Jdbc Service for TDSMessageData entity
 */
public class TdsMessagesDataJdbcService {
    private final DataSource dataSource;

    TdsMessagesDataJdbcService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Retrieves all TDSMessageData associated with a TdsRecord via the given id.
     *
     * @param tdsrId
     * @return
     * @throws Exception
     */
    public List<TDSMessageData> getByTdsRecordId(Long tdsrId) throws Exception {
        String sql = "SELECT id,tdsrId,messageType,messageVersion,sourceIP,destIP,messageDate,contents FROM ds_tdsmessages WHERE tdsrId=?";
        return JdbcUtils.query(dataSource, sql, new Object[]{tdsrId}, new TDSMessageDataResultSetMapper());
    }
}
