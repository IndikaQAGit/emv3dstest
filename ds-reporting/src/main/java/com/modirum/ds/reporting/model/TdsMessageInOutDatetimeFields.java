package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Wraps the csv columns for the in and out datetime of different 3ds messages types
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public final class TdsMessageInOutDatetimeFields {
    private String preqMessageDatetime;
    private String presMessageDatetime;
    private String areqInDatetime;
    private String areqOutDatetime;
    private String aresInDatetime;
    private String aresOutDatetime;
    private String rreqInDatetime;
    private String rreqOutDatetime;
    private String rresInDatetime;
    private String rresOutDatetime;
}
