package com.modirum.ds.reporting.model.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.modirum.ds.reporting.model.PaymentSystemConfig;
import com.modirum.ds.reporting.util.JdbcUtils;

public class PaymentSystemConfigResultSetMapper implements ResultSetMapper<PaymentSystemConfig> {

    @Override
    public PaymentSystemConfig map(ResultSet resultSet) throws SQLException {
        return PaymentSystemConfig.builder()
                .paymentSystemId(JdbcUtils.getColumnValue(resultSet, "paymentSystemId", Long.class))
                .dsId(JdbcUtils.getColumnValue(resultSet, "dsId", Integer.class))
                .key(JdbcUtils.getColumnValue(resultSet, "skey", String.class))
                .value(JdbcUtils.getColumnValue(resultSet, "value", String.class))
                .comment(JdbcUtils.getColumnValue(resultSet, "comment", String.class))
                .createdDate(JdbcUtils.getColumnValue(resultSet, "createdDate", Timestamp.class))
                .createdUser(JdbcUtils.getColumnValue(resultSet, "createdUser", String.class))
                .lastModifiedDate(JdbcUtils.getColumnValue(resultSet, "lastModifiedDate", Timestamp.class))
                .lastModifiedBy(JdbcUtils.getColumnValue(resultSet, "lastModifiedBy", String.class))
                .build();  
    }
}
