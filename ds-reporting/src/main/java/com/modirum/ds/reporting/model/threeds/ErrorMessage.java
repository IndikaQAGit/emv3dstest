package com.modirum.ds.reporting.model.threeds;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EMVCo 3DS 2.0.1 draft 5
 * <p/>
 * All the fields defined in the class are mandatory in all error messages.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
    private String threeDSServerTransID;
    private String acsTransID;
    private String dsTransID;
    private String errorCode;
    private String errorDescription;
    private String errorMessageType;
    private String errorDetail;
    private String errorComponent;
    private final String messageType = "Erro";
    private String messageVersion;
    private String sdkTransID;
}