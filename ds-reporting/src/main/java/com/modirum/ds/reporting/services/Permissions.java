package com.modirum.ds.reporting.services;

import java.security.SecurityPermission;

public interface Permissions {

    SecurityPermission DS_SecurityPermission = new SecurityPermission("DS_SecurityPermission");
}
