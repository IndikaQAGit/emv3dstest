package com.modirum.ds.reporting.reports;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.sql.DataSource;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

/**
 * Contains the parameters needed by a DsReport to generate a report.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportParameters {
    private DataSource reportingDataSource;
    private TimeZone timeZone;
    private Locale locale;
    private Properties criteriaProperties;
}
