package com.modirum.ds.reporting.services;

import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.reporting.model.KeyData;
import com.modirum.ds.reporting.util.CryptoUtil;
import com.modirum.ds.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;


public class CryptoService {

    private static final String MDE04_ = "MDE04_"; // Encrypted string prefix with "encryption version"
    java.security.GuardedObject cipherPoolsGuarded = null;
    java.security.GuardedObject macPoolsGuarded = null;
    java.security.SecureRandom sr;

    public CryptoService() {
        java.security.GuardedObject x = new java.security.GuardedObject(this, Permissions.DS_SecurityPermission);
        x.getObject();

        cipherPoolsGuarded = new java.security.GuardedObject(
                new java.util.HashMap<String, java.util.LinkedList<Cipher>>(), Permissions.DS_SecurityPermission);
        macPoolsGuarded = new java.security.GuardedObject(new java.util.HashMap<String, java.util.LinkedList<Mac>>(),
                                                          Permissions.DS_SecurityPermission);

        try {
            sr = java.security.SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(("sasQQP<--er4h" + new java.util.Date() + "s99WWQSGHWM" + Runtime.getRuntime().totalMemory() +
                        "ZCPWCBsl" + System.currentTimeMillis() + Math.random()).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * DS generic (sensitive) data decryption method. Decrypts provided input data with symmetric 'dataKey'.
     *
     * @param input Input to decrypt.
     * @return Decrypted input.
     * @throws Exception in case of decryption errors.
     */
    public String decryptData(final String input) throws Exception {
        if (input == null || !input.startsWith(MDE04_)) {
            return input;
        }

        StringTokenizer st = new StringTokenizer(input, "_");
        st.nextToken(); // remove MDExx_ prefix
        String keyId = st.nextToken();
        String encodedData = st.nextToken();

        KeyService keyService = ServiceLocator.getInstance().getKeyService();
        KeyData dataKey = keyService.getKey(Long.valueOf(keyId));
        Long hsmDeviceId = dataKey.getHsmDeviceId();
        HSMDevice hsm = ServiceLocator.getInstance().getHSMDevice(hsmDeviceId);

        byte[] dataKeyBytes = unwrapRawKey(dataKey);
        byte[] encryptedData = Base64.decode(encodedData);
        byte[] decrypted = hsm.decryptData(dataKeyBytes, encryptedData);

        byte[] decompressed = CryptoUtil.gunzip(decrypted);
        return new String(decompressed, StandardCharsets.UTF_8);
    }

    /**
     * Unwraps generic raw keydata. If not wrapped, returns original bytes.
     *
     * @param sourceRawKeyData Raw key to unwrap.
     * @return unwrapped raw key bytes.
     * @throws Exception in case of key retrieval or unwrapping errors.
     */
    public byte[] unwrapRawKey(KeyData sourceRawKeyData) throws Exception {
        if (sourceRawKeyData.getWrapkeyId() == null) {
            return Base64.decode(sourceRawKeyData.getKeyData());
        }

        KeyService keyService = ServiceLocator.getInstance().getKeyService();
        KeyData wrapperKey = keyService.getKeyDataById(sourceRawKeyData.getWrapkeyId());
        HSMDevice wrapperHsm = ServiceLocator.getInstance().getHSMDevice(wrapperKey.getHsmDeviceId());

        byte[] sourceKeyBytes = Base64.decode(sourceRawKeyData.getKeyData());
        return wrapperHsm.decryptData(Base64.decode(wrapperKey.getKeyData()), sourceKeyBytes);
    }

}
