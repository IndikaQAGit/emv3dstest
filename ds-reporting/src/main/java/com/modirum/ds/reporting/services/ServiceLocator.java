package com.modirum.ds.reporting.services;

import com.modirum.ds.hsm.HSMDevice;
import com.modirum.ds.reporting.model.HsmDevice;
import com.modirum.ds.reporting.model.HsmDeviceConf;
import com.modirum.ds.services.JsonMessageService;
import com.modirum.ds.utils.Context;
import com.modirum.ds.utils.Misc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceLocator {

    private static final Logger log = LoggerFactory.getLogger(ServiceLocator.class);
    static ServiceLocator instance;
    private KeyService keyService;
    private CryptoService cryptoService;
    private TdsMessagesDataJdbcService tdsMessagesDataJdbcService;
    private LocalizationService localizationService;
    private HashMap<Long, HSMDevice> hsmDevices; // ID to HSM device map
    private JsonMessageService jsonMessageService;
    private HsmDeviceReportService hsmDeviceReportService;
    private AuditLogJdbcService auditLogJdbcService;
    private IdService idService;
    private UserJdbcService userJdbcService;
    private CsvFilteringService csvFilteringService;
    private PaymentSystemConfigService paymentSystemConfigService;
    private AcquirerService acquirerService;

    private ServiceLocator(DataSource dataSource) {
        keyService = new KeyService(dataSource);
        cryptoService = new CryptoService();
        tdsMessagesDataJdbcService = new TdsMessagesDataJdbcService(dataSource);
        localizationService = new LocalizationService(dataSource);
        jsonMessageService = new JsonMessageService();
        hsmDeviceReportService = new HsmDeviceReportService(dataSource);
        auditLogJdbcService = new AuditLogJdbcService(dataSource);
        idService = new IdService(dataSource);
        this.userJdbcService = new UserJdbcService(dataSource);
        csvFilteringService = new CsvFilteringService();
        paymentSystemConfigService = new PaymentSystemConfigService(dataSource);
        acquirerService = new AcquirerService(dataSource);
    }

    public static ServiceLocator initInstance(DataSource dataSource) {
        if (instance != null) {
            return instance;
        }
        return instance = new ServiceLocator(dataSource);
    }

    public static ServiceLocator getInstance() {
        if (instance == null) {
           throw new RuntimeException("ServiceLocator is not instantiated yet.");
        }
        return instance;
    }

    public KeyService getKeyService() {
        return keyService;
    }

    /**
     * Looks up, initializes and returns all HSM devices registered in DS. Service lookup from DB is performed once at
     * first call to this method. Subsequent calls return cached instances.
     *
     * @return Map of hsmID - HSM device instances.
     */
    public Map<Long, HSMDevice> getHSMDevices() {
        if (hsmDevices != null) {
            return hsmDevices;
        }

        hsmDevices = new HashMap<>();
        try {
            List<HsmDevice> dbHsmDevices = getHsmDeviceReportService().listActiveHsmDevices();
            if (dbHsmDevices == null || dbHsmDevices.isEmpty()) {
                log.error("No HSM Devices registered in database.");
                return null;
            }

            String instanceId = new Context().getServerIntanceId();
            for (HsmDevice dbHsm : dbHsmDevices) {
                HSMDevice hsmDevice = (HSMDevice) Class.forName(dbHsm.getClassName()).newInstance();

                String cnfStr = null;
                if (Misc.isNullOrEmpty(cnfStr)) {
                    HsmDeviceConf hsmConf = getHsmDeviceReportService().getHsmDeviceConfig(dbHsm.getId(), 0);
                    cnfStr = hsmConf != null ? hsmConf.getConfig() : null;
                }
                hsmDevice.initializeHSM(cnfStr);
                hsmDevices.put(dbHsm.getId(), hsmDevice);
            }
        } catch (Exception e) {
            log.error("Error initializing HSM devices", e);
            throw new RuntimeException("Error initializing HSM devices", e);
        }
        return hsmDevices;
    }

    public HSMDevice getHSMDevice(Long hsmDeviceId) {
        return getHSMDevices().get(hsmDeviceId);
    }

    public CryptoService getCryptoService() {
        return cryptoService;
    }

    public TdsMessagesDataJdbcService getTdsMessagesDataJdbcService() {
        return tdsMessagesDataJdbcService;
    }

    public LocalizationService getLocalizationService() {
        return localizationService;
    }

    public JsonMessageService getJsonMessageService() {
        return jsonMessageService;
    }

    public HsmDeviceReportService getHsmDeviceReportService() {
        return hsmDeviceReportService;
    }

    public AuditLogJdbcService getAuditLogJdbcService() {
        return auditLogJdbcService;
    }

    public IdService getIdService() {
        return idService;
    }

    public UserJdbcService getUserJdbcService() {
        return this.userJdbcService;
    }

    public CsvFilteringService getCsvFilteringService() {
        return csvFilteringService;
    }  

    public PaymentSystemConfigService getPaymentSystemConfigService() {
        return paymentSystemConfigService;
    }

    public AcquirerService getAcquirerService() { return acquirerService; }
}
