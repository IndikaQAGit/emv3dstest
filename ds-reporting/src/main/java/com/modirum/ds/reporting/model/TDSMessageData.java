package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TDSMessageData extends PersistableClass implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private Long tdsrId;
    private String messageType;
    private String messageVersion;
    private String sourceIP;
    private String destIP;
    private Date messageDate;
    private String contents;

}
