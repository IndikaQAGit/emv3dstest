package com.modirum.ds.reporting.util;

import com.modirum.ds.reporting.model.rowmapper.ResultSetMapper;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Helper methods for jdbc layer.
 */
public final class JdbcUtils {
    private final static Logger log = LoggerFactory.getLogger(JdbcUtils.class);

    private JdbcUtils() {
    }

    /**
     * Creates the PreparedStatement with the sql query and binds the parameter values.
     *
     * @param connection
     * @param query
     * @param parameterValues
     * @return
     * @throws SQLException
     */
    public static PreparedStatement getPreparedStatement(Connection connection, String query, Object[] parameterValues) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        for (int i = 0; i < ObjectUtils.safeNull(parameterValues).length; i++) {
            preparedStatement.setObject(i + 1, parameterValues[i]);
        }
        return preparedStatement;
    }

    /**
     * Convenient querying of a simple list and build into Object List by the given ResultSetMapper.
     *
     * @param dataSource
     * @param sql
     * @param parameterValues
     * @param resultSetMapper
     * @param <T>
     * @return
     * @throws SQLException
     */
    public static <T> List<T> query(DataSource dataSource, String sql, Object[] parameterValues, ResultSetMapper<T> resultSetMapper) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            return query(connection, sql, parameterValues, resultSetMapper);
        }
    }

    /**
     * Convenient querying of a simple list and build into Object List by the given ResultSetMapper.
     * This uses a given Connection instead of a DataSource to reuse the same connection useful when
     * on the same transaction.
     *
     * @param connection
     * @param sql
     * @param parameterValues
     * @param resultSetMapper
     * @param <T>
     * @return
     * @throws SQLException
     */
    public static <T> List<T> query(Connection connection, String sql, Object[] parameterValues, ResultSetMapper<T> resultSetMapper) throws SQLException {
        log.debug("Executing SQL = \"{}\"", sql);
        try (PreparedStatement preparedStatement = JdbcUtils.getPreparedStatement(connection, sql, parameterValues);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            int resultSize = getRowSize(resultSet);
            log.debug("SQL query found {} results.", resultSize);
            if (resultSize == 0) {
                return Collections.emptyList();
            }
            List<T> resultList = new ArrayList<>(resultSize);
            while (resultSet.next()) {
                resultList.add(resultSetMapper.map(resultSet));
            }
            return resultList;
        }
    }

    /**
     * Convenient querying of a single result and build into Object by the given ResultSetMapper.
     *
     * @param dataSource
     * @param sql
     * @param parameterValues
     * @param resultSetMapper
     * @param <T>
     * @return
     * @throws SQLException
     */
    public static <T> T findFirst(DataSource dataSource, String sql, Object[] parameterValues, ResultSetMapper<T> resultSetMapper) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            return  findFirst(connection, sql, parameterValues, resultSetMapper);
        }
    }

    /**
     * Convenient querying of a single result and build into Object by the given ResultSetMapper.
     * This uses a given Connection instead of a DataSource to reuse the same connection useful when
     * on the same transaction.
     *
     * @param connection
     * @param sql
     * @param parameterValues
     * @param resultSetMapper
     * @param <T>
     * @return
     * @throws SQLException
     */
    public static <T> T findFirst(Connection connection, String sql, Object[] parameterValues, ResultSetMapper<T> resultSetMapper) throws SQLException {
        try (PreparedStatement preparedStatement = JdbcUtils.getPreparedStatement(connection, addLimitToQuery(sql), ArrayUtils.add(parameterValues, 1L));
             ResultSet resultSet = preparedStatement.executeQuery()) {
            if (getRowSize(resultSet) == 0) {
                return null;
            }
            resultSet.next();
            return resultSetMapper.map(resultSet);
        }
    }

    /**
     * Gets the size of the ResultSet.
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    public static int getRowSize(ResultSet resultSet) throws SQLException {
        resultSet.last();
        int rowSize = resultSet.getRow();
        resultSet.beforeFirst();
        return rowSize;
    }

    /**
     * A convenient helper method in extracting column value from ResultSet.
     *
     * @param resultSet
     * @param columnLabel
     * @param valueClazz
     * @param <T>
     * @return
     */
    public static <T> T getColumnValue(ResultSet resultSet, String columnLabel, Class<T> valueClazz) throws SQLException {
        if (isColumnExisting(resultSet, columnLabel)) {
            //This is specially handled since getObject for Short is not internally supported by the jdbc driver
            if (Short.class.equals(valueClazz)) {
                String value = resultSet.getString(columnLabel);
                return resultSet.wasNull() ? null : (T) Short.valueOf(value);
            }
            T value = resultSet.getObject(columnLabel, valueClazz);
            return resultSet.wasNull() ? null : value;
        }
        return null;
    }

    /**
     * Checks if columnLabel is existing in the ResultSet.
     *
     * @param resultSet
     * @param columnLabel
     * @return
     */
    public static boolean isColumnExisting(ResultSet resultSet, String columnLabel) {
        try {
            //Need to separate this check to silently ignore the SQLException when columnLabel is not existing in the query
            //but we don't want to ignore other causes of SQLException.
            return resultSet.findColumn(columnLabel) > 0;
        } catch (SQLException sqlException) {
            //Silently ignore, this means columnLabel is not included in the sql select columns
            return false;
        }
    }

    /**
     * Adds a limit assuming that the database is MYSQL.
     *
     * @param query
     * @return
     */
    public static String addLimitToQuery(String query) {
        return query + " LIMIT ?";
    }

    /**
     * Creates Datasource to be used for jdbc layer.
     *
     * @param dbDriver
     * @param dbUrl
     * @param dbUser
     * @param dbPassword
     * @param validationQuery
     * @return
     */
    public static DataSource createDataSource(String dbDriver, String dbUrl, String dbUser, String dbPassword, String validationQuery) {
        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName(dbDriver);
        ds.setUsername(dbUser);
        ds.setPassword(dbPassword);
        ds.setUrl(dbUrl);

        ds.setMaxTotal(25);
        ds.setMaxIdle(5);
        ds.setInitialSize(10);
        ds.setValidationQuery(validationQuery);

        return ds;
    }

    /**
     * Execute a sql command with the given DataSource and parameters.
     *
     * @param dataSource
     * @param sql
     * @param params
     * @return
     * @throws SQLException
     */
    public static boolean execute(DataSource dataSource, String sql, Object[] params) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            return execute(connection, sql, params);
        }
    }

    /**
     * Execute a sql command with the given DataSource and parameters.
     * This uses a given Connection instead of a DataSource to reuse
     * the same connection useful when on the same transaction.
     *
     * @param connection
     * @param sql
     * @param params
     * @return
     * @throws SQLException
     */
    public static boolean execute(Connection connection, String sql, Object[] params) throws SQLException {
        try (PreparedStatement preparedStatement = getPreparedStatement(connection, sql, params)) {
            return preparedStatement.execute();
        }
    }

    /**
     * Execute a sql command with the given DataSource and parameters.
     * This uses a given Connection instead of a DataSource to reuse
     * the same connection useful when on the same transaction.
     *
     * @param connection
     * @param sql
     * @param params
     * @return
     * @throws SQLException
     */
    public static int executeUpdate(Connection connection, String sql, Object[] params) throws SQLException {
        try (PreparedStatement preparedStatement = getPreparedStatement(connection, sql, params)) {
            return preparedStatement.executeUpdate();
        }
    }

    /**
     * Starts a transaction with auto commit to false.
     *
     * @param dataSource
     * @throws SQLException
     */
    public static Connection startTransaction(DataSource dataSource) throws SQLException {
        Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    /**
     * Commit and close the transaction connection. If commit fails, silently rollback
     * and close the connection.
     */
    public static void txCommitAndClose(Connection connection) {
        try {
            DbUtils.commitAndClose(connection);
        } catch (SQLException e) {
            DbUtils.rollbackAndCloseQuietly(connection);
        }
    }
}
