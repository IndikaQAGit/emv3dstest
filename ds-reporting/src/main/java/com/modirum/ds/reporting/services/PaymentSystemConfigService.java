package com.modirum.ds.reporting.services;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.modirum.ds.reporting.model.PaymentSystemConfig;
import com.modirum.ds.reporting.model.rowmapper.PaymentSystemConfigResultSetMapper;
import com.modirum.ds.reporting.util.JdbcUtils;

public class PaymentSystemConfigService {
    
    private DataSource dataSource;
    private static final Logger log = LoggerFactory.getLogger(PaymentSystemConfigService.class);
    
    PaymentSystemConfigService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource paymentSystemConfigDataSource) {
        dataSource = paymentSystemConfigDataSource;
    }
    
    public void addPaymentSystemConfig(PaymentSystemConfig paymentSystemConfig) {
        
        String insertPscSQL = "INSERT INTO ds_paymentsystem_conf (paymentSystemId, dsId, `skey`, `value`, comment, createdDate, createdUser, lastModifiedDate, lastModifiedBy) "
        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try {
            JdbcUtils.execute(dataSource, insertPscSQL,
                    new Object[]{
                        paymentSystemConfig.getPaymentSystemId(), 
                        paymentSystemConfig.getDsId(),
                        paymentSystemConfig.getKey(),
                        paymentSystemConfig.getValue(),
                        paymentSystemConfig.getComment(),
                        paymentSystemConfig.getCreatedDate(),
                        paymentSystemConfig.getCreatedUser(),
                        paymentSystemConfig.getLastModifiedDate(),
                        paymentSystemConfig.getLastModifiedBy()
                        });
        } catch (Exception e) {
            log.error("Unable to add payment system config in memory db", e);
        }
    }
    
    public PaymentSystemConfig getPaymentSystemConfig(Long paymentSystemId, String key, Integer dsId) {
        PaymentSystemConfig queriedPaymentSystemConfig = null;
        String selectPscQuery = "SELECT * FROM ds_paymentsystem_conf where paymentSystemId=? and `skey`=? and dsId=? ";
        Object[] selectPscQueryParams = {paymentSystemId, key, dsId};
        try {
            queriedPaymentSystemConfig = JdbcUtils.findFirst(dataSource, selectPscQuery, selectPscQueryParams, new PaymentSystemConfigResultSetMapper());
        } catch (SQLException e) {
            log.error("Unable to get payment system config in memory db", e);
        }
        return queriedPaymentSystemConfig;
    }
    
    public void editPaymentSystemConfig (PaymentSystemConfig updatedPsc) {
        String updatePscSQL = "UPDATE ds_paymentsystem_conf set `value`=?, comment=?, createdDate=?, createdUser=?, lastModifiedDate=?, lastModifiedBy=? "
            + "WHERE  paymentSystemId=? and `skey`=? and dsId=? ";
        Object[] updatePscQueryParams = {
                updatedPsc.getValue(),
                updatedPsc.getComment(),
                updatedPsc.getCreatedDate(),
                updatedPsc.getCreatedUser(),
                updatedPsc.getLastModifiedDate(),
                updatedPsc.getLastModifiedBy(),
                updatedPsc.getPaymentSystemId(),
                updatedPsc.getKey(),
                updatedPsc.getDsId()
            };
        try {
            JdbcUtils.execute(dataSource, updatePscSQL,updatePscQueryParams);
        } catch (Exception e) {
            log.error("Unable to modify payment system config in memory db", e);
        }
    }
    
    public void deletePaymentSystemConfig (Long paymentSystemId, String key, Integer dsId) {
        String deletePscSQL = "DELETE FROM ds_paymentsystem_conf WHERE  paymentSystemId=? and `skey`=? and dsId=? ";
        Object[] deletePscQueryParams = {paymentSystemId, key, dsId};
        try {
            JdbcUtils.execute(dataSource, deletePscSQL, deletePscQueryParams);
        } catch (Exception e) {
            log.error("Unable to delete payment system config in memory db", e);
        }
    }
}
