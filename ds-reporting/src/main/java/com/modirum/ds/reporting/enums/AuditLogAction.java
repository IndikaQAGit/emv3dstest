package com.modirum.ds.reporting.enums;

/**
 * Enumerated constants of action column in AuditLog entity.
 */
public final class AuditLogAction {
    private AuditLogAction() {
    }
    public static final String INSERT = "ins";
    public static final String UPDATE = "upd";
    public static final String DELETE = "del";
    public static final String VIEW = "view";
    public static final String LOGIN = "login";
    public static final String LOGINFAIL = "loginfail";
    public static final String LOGOFF = "logoff";
    public static final String UNAUTH = "unauthorized";
    public static final String SEARCH = "search";
    public static final String APPSTART = "appStart";
    public static final String APPSTOP = "appStop";
    public static final String CREATE_REPORT = "Create Report";
}
