package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TDSRecord extends PersistableClass implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String acquirerBIN;
    private String acquirerMerchantID;
    private String ACSOperatorID;
    private String ACSRefNo;
    private String ACSTransID;
    private String ACSURL;
    private String acctNumber;
    private String acctNumberEnc;
    private Long acctNumberKid;
    private String acctNumberHMAC;
    private Long acctNumberHMKid;
    private String authenticationMethod;
    private String authenticationType;
    private String authenticationValue;

    private String browserIP;
    private String browserJavaEnabled;
    private String browserLanguage;
    private String browserUserAgent;
    private String cardExpiryDate;
    private String deviceChannel;
    private String dsRef;
    private String DSTransID;
    private String errorCode;
    private String errorDetail;
    private String ECI;
    private String issuerName;
    private String issuerBIN;
    private Date localDateStart;
    private Date localDateEnd;
    private Short localStatus;
    private Integer localIssuerId;
    private Integer localAcquirerId;
    private String requestorName;
    private String requestorID;
    private String requestorURL;
    private String MCC;
    private Short merchantCountry;
    private String merchantName;
    private String merchantRiskIndicator;

    private String messageCategory;
    private String tdsTransID;
    private String MIReferenceNumber;
    private String tdsServerURL;
    private Long MIProfileId;

    private String protocol;

    private Long purchaseAmount;
    private Short purchaseCurrency;
    private Short purchaseExponent;
    private Date purchaseDate;
    private Short purchaseInstalData;

    private String recurringExpiry;
    private Short recurringFrequency;
    private String resultsStatus;
    private String SDKAppID;
    private String SDKReferenceNumber;
    private String SDKTransID;
    private String transType;
    private String transStatus;
    private String transStatusReason;
    private String issuerOptions;
    private String acquirerOptions;
    private List<TDSMessageData> tdsMessageDataList;
    private Integer paymentSystemId;
}
