package com.modirum.ds.reporting.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Encapsulates error columns needed in CSV Export of transactions.
 */
@Getter
@AllArgsConstructor
public class ErrorMessageExportFields {
    private final String component;
    private final String messageType;
    private final String code;
    private final String description;
    private final String details;
}
