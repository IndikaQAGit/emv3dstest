Examples, how to add/update internal repository libs. Generates directories, pom, xml, hash files.
If there any problems, use full path for files and dirs.

mvn deploy:deploy-file -DgroupId=com.modirum.ds -DartifactId=generics-utils -Dversion=1.0.4 -Durl=file:/Users/alex/Modirum/DS/lib/ -DrepositoryId=local-maven-repo -DupdateReleaseInfo=true -Dfile=/Users/alex/Modirum/Generic/generics-utils/target/jar/generics-utils-1.0.4.jar
