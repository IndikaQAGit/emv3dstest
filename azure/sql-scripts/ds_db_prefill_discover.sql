
-- Add an administrator user
-- username: admin
-- password: pass1234
INSERT INTO ds_users (id, loginname, password, oldpasswords, lastPassChangeDate, loginAttempts, lockedDate, firstName, lastName, paymentSystemId, status, phone, email, locale, factor2Authmethod, factor2AuthDeviceId, factor2Data1, factor2Data2, factor2Data3, factor2Data4) VALUES (10000, 'admin', '+t+dUx5cl1rGyIWLlzEoD7CGJzos25Cp+uFr9mBKbiA=', 'rbSmdWVjAJbfqyMltpLCDwz+2T3pgAHJLl0HcF0pjl0=+t+dUx5cl1rGyIWLlzEoD7CGJzos25Cp+uFr9mBKbiA=', '2025-05-11 06:56:05.827000', 0, NULL, 'Admin', 'Admin', NULL, 'A', '', 'test@modirum.com', '', NULL, NULL, NULL, NULL, NULL, NULL);

-- Add all roles to admin user
INSERT INTO ds_userroles (user_id, role)
VALUES (10000, 'acquirerEdit'),
  (10000, 'acquirerView'),
  (10000, 'auditLogView'),
  (10000, 'caEdit'),
  (10000, 'caView'),
  (10000, 'issuerEdit'),
  (10000, 'issuerView'),
  (10000, 'merchantsEdit'),
  (10000, 'merchantsView'),
  (10000, 'panView'),
  (10000, 'recordsView'),
  (10000, 'adminSetup'),
  (10000, 'textEdit'),
  (10000, 'textView'),
  (10000, 'userEdit'),
  (10000, 'userView'),
  (10000, 'paymentsystemView'),
  (10000, 'paymentsystemEdit'),
  (10000, 'keyManage'),
  (10000, 'reports');

INSERT INTO ds_keydata (id, keyAlias, status, keyData, hsmdevice_id, keyCheckValue, keyDate, cryptoPeriodDays)
VALUES (1, 'dataKey', 'working', 'k/bL4vTLJjT+MNnT2zquP/r7w23T5Ts1Qy6kg1KMyIU=', 1, '6E8C5C', '2020-07-10 12:30:21.996000', 1460),
  (2, 'sdkRSAKey', 'working', '-----BEGIN PRIVATE KEY-----\r\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCVC/sA04zSGy2yn9xvYZ/X5QFE\nU/i99k/ArqLOkW/tvtJJo93F4lJ207C3N9iO6h753WSa5+gqgW0zIpNbfDkaSNmfWEBZuvkO7/Z9\ny5DhtzzQVSFu5WdVK9tWPuqwniiyN9APYwChAJSfRifZ7BZ3CMyIy2r79ZWY0Yf2ppf6cH5/TBpR\n9lQWvLbyDcULhhOBKLyRIY95GkCruduKsDapnyFX+ZDcVHtMttNDjCr3VViGWfQQshqqHNLYHIvA\nKJvYzJeRCrc/O7icWUKK+nQEw7/o22Vpiyom33Aatm3jaWoH/bYE6lfWcyRcY8fX4WfdSUwz2SW0\ninEgAOnBGn9TAgMBAAECggEAHqb2Gfdn0JXyUff6d5WWL+pS1mCGmVqckD3aEa1rXXOO/8IJi/a9\nit6CS0va55lSgYR8ejcTUhqIAKotr/TeMX+l8dVOdqWzLtU+1iQVSbZLdXTsiUf4dlWsqBIMZgJA\n0FYWHfP4XyQlbJQbAgBBpn1SwxdCi0q4V1NQKPOd8cz7E3fApCuixRN340YrxYJ4BZ7y5nJ5D2ZB\nwu8ay/VelJzG6EUqtEWtE0lXurugieq0EZCAWNGIMEU5GeqKI0+QsXyYgivby7CokpU2T7B2Qo0y\n8DdX3DlAMXXJ8x4MxVRJI1OEf1K6pC0+qlT4LyQ5u+BIpwcVM1fPDK4mBBH6kQKBgQDrp/Hx3ceJ\ncdRBxKXotYTa32GAtyuLaI6loqcnexT43SvHZf8yO+1XI2CR/IxjDia5cFoC7x/a0i/0onTptQtI\nL/G8Vt8otqpLQzNP/5Pt8ClXJ0jZ1M//iLQGTeVT1/+mDNZ8xh8KKPZvT8TXaVrwFFhh8HFkDV4t\ntZ6m+wzB5QKBgQCh6fNSqOeVoztno7yXeFdZOUsUZL4RtGZbANnqqtJQQtU3lZcWw6X8S0CLbZtk\nlNNWQhfDEJfQgT+KnV5QwJaLv2UrjpJ3aE7Kn24IB01WunfjUOUq1Y4kmi5jNgFPd57TX55bV6cj\nwojY6vh3efOqkp4+V+x7Sp2JHSt4GrqI1wKBgFvu3e1shu5w0MyDifH59oVoheIwEzXEqXmmta0q\nUCkyJ2UXvnH7fQD0lTT+oEn4eFT5prPkU7oLK5g1VITkEy2rPpQqE88PS+omojsAStaVTZtpgPsy\nEZ7v1sNq2h9Wca527aoppGxTRKhyJymeOVLVrREwOxw8NtSAOQ+NexGdAoGAGYuz3ELR5wYOBVgM\nFUnjXixv8BPE+T4hOBJ2T1QMQqWIxnBPmkPcGGY3FMiCT+g7P8zFuv0PNtJtmA0kKFF6byoPaEPi\nT/yhEc79qT6dsSVSbQcCYTO4bYX186k9o7AX2vmnHnpB4J58wNyxvwf6rdrcJLHymbE4Eb4McbKl\n45kCgYEA5AHliC8pES3WsHbk/3/Nfj+Evr7OalJhHMrkUxsoY//iXcmtb/VJIxKzpMy6eE5EFK6u\n2F0/6z/FbgmbwQFdWCk1rJUiYY8mPkFY6UKgLMFS8Uzwe19AVcOqT1qDPy9IgGeJ6q5BGWLNgKM6\n/JyLpW7YvGOwjbPSkJjIOcJgKXI=\r\n-----END PRIVATE KEY-----\r\n-----BEGIN CERTIFICATE-----\r\nMIIDxzCCAq+gAwIBAgIUScvjNry4oroW4HE4b2yxYR68srkwDQYJKoZIhvcNAQEL\r\nBQAwczELMAkGA1UEBhMCRUUxEDAOBgNVBAcMB1RhbGxpbm4xFjAUBgNVBAoMDU1v\r\nZGlydW0gTURQYXkxHzAdBgNVBAsMFk1vZGlydW0gQ2xvdWQgU2VydmljZXMxGTAX\r\nBgNVBAMMEERpc2NvdmVyIDNEUyBTREswHhcNMjAxMjA5MTMzNTE0WhcNMjMxMjA5\r\nMTMzNTE0WjBzMQswCQYDVQQGEwJFRTEQMA4GA1UEBwwHVGFsbGlubjEWMBQGA1UE\r\nCgwNTW9kaXJ1bSBNRFBheTEfMB0GA1UECwwWTW9kaXJ1bSBDbG91ZCBTZXJ2aWNl\r\nczEZMBcGA1UEAwwQRGlzY292ZXIgM0RTIFNESzCCASIwDQYJKoZIhvcNAQEBBQAD\r\nggEPADCCAQoCggEBAJUL+wDTjNIbLbKf3G9hn9flAURT+L32T8Cuos6Rb+2+0kmj\r\n3cXiUnbTsLc32I7qHvndZJrn6CqBbTMik1t8ORpI2Z9YQFm6+Q7v9n3LkOG3PNBV\r\nIW7lZ1Ur21Y+6rCeKLI30A9jAKEAlJ9GJ9nsFncIzIjLavv1lZjRh/aml/pwfn9M\r\nGlH2VBa8tvINxQuGE4EovJEhj3kaQKu524qwNqmfIVf5kNxUe0y200OMKvdVWIZZ\r\n9BCyGqoc0tgci8Aom9jMl5EKtz87uJxZQor6dATDv+jbZWmLKibfcBq2beNpagf9\r\ntgTqV9ZzJFxjx9fhZ91JTDPZJbSKcSAA6cEaf1MCAwEAAaNTMFEwHQYDVR0OBBYE\r\nFEL6Jys9+MOpvCwiqgTr2oInoyLbMB8GA1UdIwQYMBaAFEL6Jys9+MOpvCwiqgTr\r\n2oInoyLbMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBACIj2eG4\r\niRWAvCdBDmFCjbrqQ3YBy5CDl1EerAWTqhhl1FnT9IafVcymw/KtLYpyvHefScoB\r\nYnpx12hIOTduTkxYiUagQhjf23NLjH8Y8tGwXzWaL8mptYMJw8ZnMADaajVmT7y6\r\nYQ/CbbnerA1YmVIA4FrEo0j7uH2mNPfOGBD0tQ6WtGE9uSqIQJQ+h2OyAWxv2zaA\r\nSjm3OZWBB9G6RrxkgoewygDpPmB2UZ9JnSLe6DWfLwDswaiZ6X8BhD9WDyucLXdJ\r\nCnbi+0Q+6lv5xoDrfQI0odIv+5dOviAeVLjmkJln6YH2jxyuxm7894ZnC8laJUPq\r\nzkN9sTWslgEYGrM=\r\n-----END CERTIFICATE-----', 1, NULL, '2020-07-10 14:46:30.757000', 3650),
  (4, 'clientAuthCert', 'working', '-----BEGIN PRIVATE KEY-----\r\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDMhlniqFv3538E\r\n3Qt3VQ/lpvZ9CdFxKdklDS4ixGDBrIF848r/P00NsNgY4nKMxwFCtitzdOjZBxsl\r\nl2dArpXtbBV6IvZi+cRrbO7EJoNsrRiESEG7erVEVP7eU9LoQJl5I38lScpw10hi\r\ngcCjBHzgNMlT13iH4hrusJpyOFgLYpvhAtftJyrz9kgwLjue+DxOwbrZ3/RqN8zU\r\n/C7U44HCPuDRC+0rA6kZnhWNKn1EiDIfaaRtChFQJQ4yrN+LqXrkiaIBNGYvbbeW\r\nYSPILPvM9tktvk2QccHvCykzhf4WnxGSr7o6X06fBC1iTukpuvHgGIfJ0hA7xdnt\r\n05AA3y+FAgMBAAECggEALlvZrfWpewS/FtQf0Dm4s75nqqn6DmOE89AMABdxMxUt\r\n+KgNkFPB3HOUV/ekLWLCV2PBb5c1XFQvBBNQdSndlAZmy9t267JfolX7vPxZ191Y\r\nbEuQkrVatxVo++4WJUzXPM0d+c7DE1bI8oMkrg7LW2q7o8R4bpyCrE3dhDxfQJz+\r\nf3+XphnzdSzzPYLzJwFkLHTB1u+wHB94FloC5QOpLwy1lSndD2Xf7CAbtK/Eh1t5\r\ni7Q0Rt4aEXQJJ3XBqxXEKtUuVWahCgmikAqz2SXPClX9FSqZkLwUpdYgJnibX2qj\r\nDrR3RR3+VbbLuUsCj/5nBvPIQdm0UgCfgRmjkInIwQKBgQDyPr7tu4rCyuvOxX89\r\nt/nuMvZwUmSSd31RQQwZbwhk+i8fhqZBZGnWS4xNB7W/MaVeP9M1C/UbOSJ63jOO\r\npyFvPS3xL2STYlXAms+awSo6SfN3AVWOBeqlAAS4Z1FQGFs5psPhZ3O7aRG7b2E3\r\nkdTP055JxCPzZdDExmHWGvCrkQKBgQDYI05ssHRhshcHuJMY0klwY11w9vRtnkry\r\nCth4Q73hpFa+wm+yZ/T7WtSFoBcphV3Sss1+406bjdThMtGiW2YgSfKrGve1+cSQ\r\nDdTIBHu/UhZm27YYflDFHUzj8KQc0eOjpa/Oho4cfvHi16uJqfFZ+1VtrbATBJME\r\nRS+2GpLCtQKBgCEX00t02GL+B7LKpjRPQtR4n9P+XYDo0/TatwVudq4cw31CDspW\r\nJmNBFNydFgYViSrT+01+4bAjYQN6AX8Wc+nXaMRPHiVIRMC4JW5VvygRXDJAu2hY\r\nhJKf6wUqECCqAyRgFNhhIp1SoDqD37sQsAroNPKM2gBxQ1DubQUrzv8RAoGBAIj/\r\nOAJkhiA2hap7L6GYCLyX/u5PWfbxUofAoBz6syyGwgT/sVTf18RKelKIeqXyxtl6\r\nRA6dfYHTkI99aqsCS+VDefFCkqhhvFmiqrBtRxKpsVFZCj7UMdX/EHGaYityTlu3\r\n9Ytudj1RqXgjQ7NqaHSkfi2a3fPv/k/Cms9OYJ/RAoGAMngQ5K1o9hsJ2MlplGkn\r\n2MKRCmaGEAhItyjOxeD/G39mb/+StECsMD+66Pt/OqxPt/c42uJhhXE8Xlyrzbc3\r\nt9m7qQuokPaQWQaS2/9j8ghtSERulK+SIpbxgCPDh8oxjs2azSAJMOtb0iEJOpXy\r\nWtlkEkiNbpefIKUZCTQ3/1U=\r\n-----END PRIVATE KEY-----\r\n-----BEGIN CERTIFICATE-----\r\nMIIERjCCAy6gAwIBAgICBnowDQYJKoZIhvcNAQELBQAwgaQxCzAJBgNVBAYTAlVT\r\nMREwDwYDVQQIEwhJbGxpbm9pczETMBEGA1UEBxMKUml2ZXJ3b29kczEkMCIGA1UE\r\nChMbRGlzY292ZXIgRmluYW5jaWFsIFNlcnZpY2VzMRkwFwYDVQQLExBQYXltZW50\r\nIFNlcnZpY2VzMSwwKgYDVQQDEyNleHRlbmRlZGludGVybWVkaWF0ZS5wcm90ZWN0\r\nYnV5LmNvbTAeFw0yMDEwMTIwMDAwMDBaFw0yMjEwMTIwMDAwMDBaMG8xCzAJBgNV\r\nBAYTAkVFMRAwDgYDVQQHEwdUYWxsaW5uMRYwFAYDVQQKEw1Nb2RpcnVtIE1EUGF5\r\nMR8wHQYDVQQLExZNb2RpcnVtIENsb3VkIFNlcnZpY2VzMRUwEwYDVQQDEwxEaXNj\r\nb3ZlciAzRFMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDMhlniqFv3\r\n538E3Qt3VQ/lpvZ9CdFxKdklDS4ixGDBrIF848r/P00NsNgY4nKMxwFCtitzdOjZ\r\nBxsll2dArpXtbBV6IvZi+cRrbO7EJoNsrRiESEG7erVEVP7eU9LoQJl5I38lScpw\r\n10higcCjBHzgNMlT13iH4hrusJpyOFgLYpvhAtftJyrz9kgwLjue+DxOwbrZ3/Rq\r\nN8zU/C7U44HCPuDRC+0rA6kZnhWNKn1EiDIfaaRtChFQJQ4yrN+LqXrkiaIBNGYv\r\nbbeWYSPILPvM9tktvk2QccHvCykzhf4WnxGSr7o6X06fBC1iTukpuvHgGIfJ0hA7\r\nxdnt05AA3y+FAgMBAAGjgbUwgbIwHwYDVR0jBBgwFoAU4T5ZrR/41KsRn2RnOQ9A\r\ndhlcuVkwHQYDVR0OBBYEFJv+NmLDT0P+a70jTxl+kPyn8WPlMAwGA1UdDwQFAwMA\r\noAAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwTQYDVR0fBEYwRDBCokCGPmh0dHA6Ly93\r\nd3cuZGlzY292ZXJmaW5hbmNpYWxjZXJ0LmNvbS9jcmxzL1Jldm9jYXRpb25MaXN0\r\nXzMuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQA/kRGA2UM5qlHfCBCfCZyW+YOhTzTC\r\nnpf20HzF0S8Yrt94W2Wl7Q5I33+zsax6SuYn8HHujA0JCzwkwRDrHY4LWyFqc+ey\r\nysxeTJjNYNMB+B8bkDsePKd5KqECr3zqlm5DhDc20WXld8/iBmWV/XeJkSxPHNVT\r\nnGWh39/SlHvftu5WDD6XG38tj3U0cdBiQCsHNhtN9qLOhl/DlWDnYUkN6jrIYV91\r\n173hFO4KOg01IX5UBdnfsn57B0RQ8QbWZl1+RDq0+0+1OALm/GZhbdRRMO/5bzOc\r\nS1KY7A/hbQcJTY5jg+IuIHrfalC7uJj9EfR6hnCWKUMfx3lx5d/EsQnL\r\n-----END CERTIFICATE-----\r\n-----BEGIN CERTIFICATE-----\r\nMIIEkzCCA3ugAwIBAgICAV8wDQYJKoZIhvcNAQELBQAwgZwxCzAJBgNVBAYTAlVT\r\nMREwDwYDVQQIEwhJbGxpbm9pczETMBEGA1UEBxMKUml2ZXJ3b29kczEkMCIGA1UE\r\nChMbRGlzY292ZXIgRmluYW5jaWFsIFNlcnZpY2VzMRkwFwYDVQQLExBQYXltZW50\r\nIFNlcnZpY2VzMSQwIgYDVQQDExtleHRlbmRlZHJvb3QucHJvdGVjdGJ1eS5jb20w\r\nHhcNMTcwMjAzMDAwMDAwWhcNMjcwMjAzMDAwMDAwWjCBpDELMAkGA1UEBhMCVVMx\r\nETAPBgNVBAgTCElsbGlub2lzMRMwEQYDVQQHEwpSaXZlcndvb2RzMSQwIgYDVQQK\r\nExtEaXNjb3ZlciBGaW5hbmNpYWwgU2VydmljZXMxGTAXBgNVBAsTEFBheW1lbnQg\r\nU2VydmljZXMxLDAqBgNVBAMTI2V4dGVuZGVkaW50ZXJtZWRpYXRlLnByb3RlY3Ri\r\ndXkuY29tMIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEA3Hb4PGfHF5qc\r\n1cX7EXsEdZyBeT8CXS879LlSFFsdxpTY3mbKBZXQKuZRwx4RAbwG4SROkgznhofp\r\nta5AC4px/btVl0Q48sgs8+5HCRShohBOvW0c8bmSh37MwRq0B1FM+MyC7am0ijVO\r\n/PYn/+K9W64jS3KB0o5RffXEXxUv4SEF5hp5APLtpa5/KV7DL9kXj6znoZdXFTd2\r\namjp77rG+jRG4pSKM9CLPiSur1oiJd5lBtimiwSqqbY7c/oRFXjPpigurrpfw3gy\r\nUcLhyFTzjRHjsDVhYpml6VKlUbvEuFhskkDFilx4Ss5ja4G2aFua/bFcX2FoM8TR\r\nZJWKHz+fXwIBA6OB1jCB0zAfBgNVHSMEGDAWgBQo9O54ohDA+iqJRT8lpzFNgNwe\r\n0DAdBgNVHQ4EFgQU4T5ZrR/41KsRn2RnOQ9AdhlcuVkwEgYDVR0TAQH/BAgwBgEB\r\n/wIBADAPBgNVHQ8BAf8EBQMDAIQAMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEF\r\nBQcDAjBNBgNVHR8ERjBEMEKiQIY+aHR0cDovL3d3dy5kaXNjb3ZlcmZpbmFuY2lh\r\nbGNlcnQuY29tL2NybHMvUmV2b2NhdGlvbkxpc3RfMy5jcmwwDQYJKoZIhvcNAQEL\r\nBQADggEBAJMrE5bT8qMHVXsVhB9W2tZGlqUBrH2NHy+Y1jxlVQRvDbaXqBV///Rs\r\n64ufL0yY9Nb24RjzdYwF3pylDqQR0ltZPu/o69OEUMVjM5PEs1fN3WZXSLp4ltw6\r\nNsnUvJSGyGQOQIESKFCdhYALvqzyuGACtpvSzGYSSk+Z45J9lxvXa8mf+vhNDHdg\r\nPfpSDyqSHqpl07YS/3dpNO1w7ft6PGmLeCk0049dO0oV1OJ23tcx0vIayD96T78M\r\nQL03lFPkjnusdJ7UxS7QfNbNKKzYS+P7+sExTHWrybJKzrlWz3DGxHRnY37QZ3/X\r\nnwxbgDe7y+r2l5x2n8fKhregYJThYew=\r\n-----END CERTIFICATE-----\r\n-----BEGIN CERTIFICATE-----\r\nMIIElTCCA32gAwIBAgICAVwwDQYJKoZIhvcNAQELBQAwgZwxCzAJBgNVBAYTAlVT\r\nMREwDwYDVQQIEwhJbGxpbm9pczETMBEGA1UEBxMKUml2ZXJ3b29kczEkMCIGA1UE\r\nChMbRGlzY292ZXIgRmluYW5jaWFsIFNlcnZpY2VzMRkwFwYDVQQLExBQYXltZW50\r\nIFNlcnZpY2VzMSQwIgYDVQQDExtleHRlbmRlZHJvb3QucHJvdGVjdGJ1eS5jb20w\r\nHhcNMTcwMjAzMDAwMDAwWhcNMjcwMjAzMDAwMDAwWjCBnDELMAkGA1UEBhMCVVMx\r\nETAPBgNVBAgTCElsbGlub2lzMRMwEQYDVQQHEwpSaXZlcndvb2RzMSQwIgYDVQQK\r\nExtEaXNjb3ZlciBGaW5hbmNpYWwgU2VydmljZXMxGTAXBgNVBAsTEFBheW1lbnQg\r\nU2VydmljZXMxJDAiBgNVBAMTG2V4dGVuZGVkcm9vdC5wcm90ZWN0YnV5LmNvbTCC\r\nASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBAO1otxObcaAkdZF3ldFTVNKc\r\ncpGk+OB+VyaA4tBmSdgefj1ykeycHRli9yZnECRySctMZ8T13HFRGvGLehBxbtex\r\nfVP6HP7SvwJUNlw7l2Bxdn6BNhEatmrRvz1rSURwWtWkWcJKz6sK0zsZwepXucyg\r\n77EeWcRkmaCs8pbre/NcvkzWuhXzoFAD4r56y04OzpW1Epu01hQd7fIB6Ck/juYw\r\nMoNQXYTWWVhGidsRoVxSdyYTlhAurTSXSpRwsiWeNroS11paejH1Mh8uU1frlrfj\r\nRubVMFkdKMnPzkgeE/IvCP76fpxba4hIvT8D+NKOhuV1ZKCKn+hsqPcTyfznjo8C\r\nAQOjgeAwgd0wHwYDVR0jBBgwFoAUKPTueKIQwPoqiUU/JacxTYDcHtAwHQYDVR0O\r\nBBYEFCj07niiEMD6KolFPyWnMU2A3B7QMBIGA1UdEwEB/wQIMAYBAf8CAQEwDwYD\r\nVR0PAQH/BAUDAwCGADAnBgNVHSUEIDAeBggrBgEFBQcDAQYIKwYBBQUHAwIGCCsG\r\nAQUFBwMJME0GA1UdHwRGMEQwQqJAhj5odHRwOi8vd3d3LmRpc2NvdmVyZmluYW5j\r\naWFsY2VydC5jb20vY3Jscy9SZXZvY2F0aW9uTGlzdF8zLmNybDANBgkqhkiG9w0B\r\nAQsFAAOCAQEAX+U6rteEJU7XF+Ze6E9wVpkP59PUZHWE+ffe+qCBpBPrKv9/BZHI\r\nA17WLiNSIFpM/HwcCaLRymUzt8KR9LiriZ493xNB8ZF6MAs1zVUGWCT7frX/Y+Te\r\nasQq0l0DRCIZSQaCO5R7BuV8q95O0jP5EjuC5BPonJDY5ME3eYQR6MYrRSZ8DeoD\r\nfA8mVa5BjBIg1HRZZn6Ay+4hLdcfhBuUmDrjsnaCr/HJn8/tN4De2FYv7txOzg+t\r\nJsw8Vo8oFcztrH4rmi8CSiZ8qhV1dIkDo7H+wo75/AkAF/FL6HhZHsS9VdMtbqIz\r\nm6C3zea/yyGQ78n2SY6sNbTOPh3lpKPrOg==\r\n-----END CERTIFICATE-----', 1, 'PRI: 4D72E6', '2020-07-10 14:02:06.412000', 365),
  (5, 'trustedRootCert', 'working', '-----BEGIN CERTIFICATE-----\r\nMIIElTCCA32gAwIBAgICAVwwDQYJKoZIhvcNAQELBQAwgZwxCzAJBgNVBAYTAlVT\r\nMREwDwYDVQQIEwhJbGxpbm9pczETMBEGA1UEBxMKUml2ZXJ3b29kczEkMCIGA1UE\r\nChMbRGlzY292ZXIgRmluYW5jaWFsIFNlcnZpY2VzMRkwFwYDVQQLExBQYXltZW50\r\nIFNlcnZpY2VzMSQwIgYDVQQDExtleHRlbmRlZHJvb3QucHJvdGVjdGJ1eS5jb20w\r\nHhcNMTcwMjAzMDAwMDAwWhcNMjcwMjAzMDAwMDAwWjCBnDELMAkGA1UEBhMCVVMx\r\nETAPBgNVBAgTCElsbGlub2lzMRMwEQYDVQQHEwpSaXZlcndvb2RzMSQwIgYDVQQK\r\nExtEaXNjb3ZlciBGaW5hbmNpYWwgU2VydmljZXMxGTAXBgNVBAsTEFBheW1lbnQg\r\nU2VydmljZXMxJDAiBgNVBAMTG2V4dGVuZGVkcm9vdC5wcm90ZWN0YnV5LmNvbTCC\r\nASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBAO1otxObcaAkdZF3ldFTVNKc\r\ncpGk+OB+VyaA4tBmSdgefj1ykeycHRli9yZnECRySctMZ8T13HFRGvGLehBxbtex\r\nfVP6HP7SvwJUNlw7l2Bxdn6BNhEatmrRvz1rSURwWtWkWcJKz6sK0zsZwepXucyg\r\n77EeWcRkmaCs8pbre/NcvkzWuhXzoFAD4r56y04OzpW1Epu01hQd7fIB6Ck/juYw\r\nMoNQXYTWWVhGidsRoVxSdyYTlhAurTSXSpRwsiWeNroS11paejH1Mh8uU1frlrfj\r\nRubVMFkdKMnPzkgeE/IvCP76fpxba4hIvT8D+NKOhuV1ZKCKn+hsqPcTyfznjo8C\r\nAQOjgeAwgd0wHwYDVR0jBBgwFoAUKPTueKIQwPoqiUU/JacxTYDcHtAwHQYDVR0O\r\nBBYEFCj07niiEMD6KolFPyWnMU2A3B7QMBIGA1UdEwEB/wQIMAYBAf8CAQEwDwYD\r\nVR0PAQH/BAUDAwCGADAnBgNVHSUEIDAeBggrBgEFBQcDAQYIKwYBBQUHAwIGCCsG\r\nAQUFBwMJME0GA1UdHwRGMEQwQqJAhj5odHRwOi8vd3d3LmRpc2NvdmVyZmluYW5j\r\naWFsY2VydC5jb20vY3Jscy9SZXZvY2F0aW9uTGlzdF8zLmNybDANBgkqhkiG9w0B\r\nAQsFAAOCAQEAX+U6rteEJU7XF+Ze6E9wVpkP59PUZHWE+ffe+qCBpBPrKv9/BZHI\r\nA17WLiNSIFpM/HwcCaLRymUzt8KR9LiriZ493xNB8ZF6MAs1zVUGWCT7frX/Y+Te\r\nasQq0l0DRCIZSQaCO5R7BuV8q95O0jP5EjuC5BPonJDY5ME3eYQR6MYrRSZ8DeoD\r\nfA8mVa5BjBIg1HRZZn6Ay+4hLdcfhBuUmDrjsnaCr/HJn8/tN4De2FYv7txOzg+t\r\nJsw8Vo8oFcztrH4rmi8CSiZ8qhV1dIkDo7H+wo75/AkAF/FL6HhZHsS9VdMtbqIz\r\nm6C3zea/yyGQ78n2SY6sNbTOPh3lpKPrOg==\r\n-----END CERTIFICATE-----', 1, NULL, '2020-07-10 14:02:06.412000', 365);

INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy)
VALUES ('product.extensions', 'fss, tdsmrelay', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licensee', 'DS Demo', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licenseexp', 'Never', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licenseissued', '2020-10-19', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.licensekey', 'tjnzTfoodu1/y3xlVvDYorW52oQm4B4ChP8eCidez3Vk5SR3nIOhPhc/FENRrpJg+XzxClMnMvS3\r\nx5QjZe8486Zqx/qH7YsYvoQiCXhprPXRKFmCpV3zwa8cLYJ9o4fEpfaXqDk9VMMfJD69KjpGE1G2\r\n9GgySznVAC7aqrFbYg3/BTDlmsWzXhl8i3oOopSiX8h1KOYgdP8TIICU0mUVT9INt+4TIMmy5y1d\r\nA0qtOeoMp8LejZkfIliuiW2N17Y3UFKRqrKZSxiN5Fn0Ts09bNIeDnVVeVBn0Tge0TLEQ6f0H9Jt\r\nAeEUY6SOoPzq57+byLFEyrxRSpv8GHtrR1bRlQ==', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.maxmerchants', 'Unlimited', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.name', 'Modirum DS', NULL, '2020-07-24 00:31:55.000000', 'admin'),
  ('product.version', '1.0', NULL, '2020-07-24 00:31:55.000000', 'admin');

INSERT INTO ds_settings (skey, svalue, comments, lastModified, lastModifiedBy)
VALUES ('enableTrustAllMerchants', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('disableDsMngrIpWhitelisting', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('viaProxyEnabled', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('sslCertsManagedExternally', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('proxyType', 'nginx', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('acs.ssl.client.certId', '4', 'Default ACS SSL auth keypair/certificate id', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('acs.ssl.client.trustany', 'true', NULL, '2020-08-18 14:48:11.301000', 'admin (10000)'),
  ('acs.ssl.client.verifyhostname', 'false', NULL, '2020-11-13 08:17:13.757000', 'admin (10000)'),
  ('mi.ssl.client.certId', '4', 'Default MI SSL auth keypair/certificate id', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('mi.ssl.client.trustany', 'true', NULL, '2020-08-18 14:48:31.898000', 'admin (10000)'),
  ('mi.ssl.client.verifyhostname', 'true', NULL, '2020-08-18 14:48:31.342000', 'admin (10000)'),
  ('settingsEnumEnabled', 'false', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('acsTimeout', '12000', '', '2020-10-29 13:40:24.268000', 'admin (10000)'),
  ('attempts.acs.enabled', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('issuer.rangeMode', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('DS.versionDefault', '2.1.0', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('DS.versionsEnabled', '2.1.0 2.2.0', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('acceptedSDKList', '3DSSDK74332823FF', '', '2020-07-10 12:16:37.537000', 'admin (10000)'),
  ('acceptedMIList', '3DS_LOA_SER_MOMD_020100_00068,3DS_LOA_SER_LYNE_020100_00098,3DS_LOA_SER_CACC_020100_00050,3DS_LOA_SDK_CACC_020100_00063,3DS_LOA_SDK_NTGN_020100_00165,3DS_LOA_SER_NTGN_020200_00261,3DS_LOA_SER_NTGN_020100_00047,3DS_LOA_SDK_NTGN_020100_00173,3DS_LOA_ACS_NTGN_020100_00040', '', '2020-09-29 21:09:50.083000', 'admin (10000)'),
  ('scheme.logo', 'https://modirum.blob.core.windows.net/discover/images/DN_ProtectBuy_Horiz.jpg', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('com.modirum.ds.services.HSMDevice', 'com.modirum.ds.hsm.SoftwareJCEService', '', '2020-09-28 17:42:25.000000', 'admin (10000)'),
  ('com.modirum.ds.services.HSMDevice.config', '', '', '2020-09-28 17:42:25.000000', 'admin (10000)'),
  ('extraRequestorChallengeInds', '\"01\",\"02\",\"03\",\"04\",\"05\",\"07\",\"08\",\"09\"', 'Adding in 05 and 07 to support TRA and SCA merchant challenge ind.', '2020-11-17 12:56:14.819000', 'admin (10000)'),
  ('cardType.01.visa', '1', '', '2016-03-16 12:49:46.000000', 'admin (10000)'),
  ('cardType.02.mastercard', '2', '', '2016-03-16 12:49:57.000000', 'admin (10000)'),
  ('cardType.03.amex', '3', '', '2017-05-11 11:35:11.000000', 'admin (10000)'),
  ('cardType.04.jcb', '4', '', '2017-05-11 11:34:54.000000', 'admin (10000)'),
  ('cardType.05.discovery', '5', '', '2017-05-11 11:35:56.000000', 'admin (10000)'),
  ('cardType.06.diners', '6', '', '2017-05-11 11:34:40.000000', 'admin (10000)'),
  ('cardType.07.mistercash', '7', '', '2017-05-11 11:36:18.000000', 'admin (10000)'),
  ('cardType.08.mir', '8', '', '2017-05-11 11:36:35.000000', 'admin (10000)'),
  ('storeAuthenticationValue', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('storeDeviceInfo', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('storeSdkEncData', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('storeEphemPubKeys', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('enableLoggingFullPres', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)');

INSERT INTO ds_texts (id, tkey, locale, message)
VALUES (default, 'cardType.1', 'en', 'VISA'),
  (default, 'cardType.2', 'en', 'MasterCard'),
  (default, 'cardType.3', 'en', 'American Express'),
  (default, 'cardType.4', 'en', 'JCB'),
  (default, 'cardType.5', 'en', 'Discover'),
  (default, 'cardType.6', 'en', 'Diners'),
  (default, 'cardType.7', 'en', 'Bancontact Mister Cash'),
  (default, 'cardType.8', 'en', 'NSPK MIR'),
  (default, 'cardType.amex', 'en', 'American Express'),
  (default, 'cardType.diners', 'en', 'Diners Club'),
  (default, 'cardType.jcb', 'en', 'JCB'),
  (default, 'cardType.mastercard', 'en', 'MasterCard'),
  (default, 'cardType.visa', 'en', 'VISA');

INSERT INTO ds_issuers (id, BIN, name, phone, email, address, city, country, status, attemptsACSPro, isid, createdDate, lastModifiedDate, paymentSystemId)
VALUES (10000, '601194', 'Azure Issuer', '', 'admin@modirum.com', '', '', '', 'A', NULL, NULL, '2016-03-16 12:49:57.000000', '2016-03-16 12:49:57.000000', 0),
(11000, '400000', 'DS-3DS-EMULATOR issuer', '', 'emulator@modirum.com', '', '', '', 'A', NULL, NULL, '2016-03-16 12:49:57.000000', '2016-03-16 12:49:57.000000', 0),
(12110, '651000', 'Test Case Issuer', '12345678', 'aidan@explicitselection.com', 'Eichenweg 45', 'Altenholz', 'US', 'A', NULL, NULL, '2016-03-16 12:49:57.000000', '2016-03-16 12:49:57.000000', 0);

INSERT INTO ds_card_ranges (id, bin, end, name, cardtype, subtype, setv, status, lastMod, issuerId, isid, acsInfoInd, includeThreeDSMethodURL, options)
VALUES (10010, '36816499999958', '36816500009957', 'Discover 36816499999958 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (10020, '36839099999962', '36839100009961', 'Discover 36839099999962 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (10030, '36849800000000', '36849800012000', 'Discover 36849800000059 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (12010, '36849900020056', '36849900030055', 'Discover 36849900020056 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (12020, '6011947300599855', '6011947300609953', 'Discover 6011947300599855 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (12040, '6011947499999957', '6011947500009956', 'Discover 6011947499999957 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (12050, '6011947599999956', '6011947600009955', 'Discover 6011947599999956 PAN', 5, NULL, 1, 'P', '2020-12-17 13:02:49.861000', 10000, NULL, '', 1, ''),
  (11000, '4000000000000000', '4000000999999999', 'DS-3DS-EMULATOR 400000', 2, NULL, 1, 'P', '2020-09-08 15:00:49.104000', 11000, NULL, '', 1, ''),
  (12160, '6510000000000000', '6510000000999999', 'Certification Test Cards', 5, NULL, 1, 'P', '2020-11-05 15:37:57.856000', 12110, NULL, '', 1, ''),
  (14030, '36070500000000', '36070500009999', 'Certification Test Cards', 6, NULL, 1, 'P', '2020-11-05 15:37:57.856000', 12110, NULL, '', 1, '');

INSERT INTO ds_acquirers (id, BIN, name, phone, email, address, city, country, status, isid, paymentSystemId)
VALUES (10000, '601194', 'Azure Acquirer', '', '', '', '', '', 'A', NULL, 0),
  (11000, '444444', 'EMULATOR Acquirer', '', '', '', '', '', 'A', NULL, 0);

INSERT INTO ds_tdsserver_profiles (id, name, URL, outClientCert, inClientCert, status, SSLProto, requestorID, operatorID, isid, tdsReferenceNumberList, paymentSystemId)
VALUES (10000, 'Azure Modirum 3DSS', '', 4, NULL, 'A', NULL, NULL, '$TDS_SERVER_OPERATOR_ID', NULL, 'AZ-MD-3DSS-REF-NUMBER', 0),
  (11000, 'Modirum DS-3DS-Emulator', '', NULL, NULL, 'A', NULL, NULL, 'DS-3DS-EMULATOR-OPER-ID', NULL, 'DS-3DS-EMULATOR-REF-NUMBER', 0);

INSERT INTO ds_merchants (id, tdsServerProfileId, acquirerId, identifier, name, phone, email, country, URL, requestorID, status, isid, options, paymentSystemId)
VALUES (10000, 10000, 10000, '1001', 'Azure Merchant', '', '', '', '', '$TDS_REQUESTOR_ID', 'A', NULL, '', 0),
  (11000, 11000, 11000, '000001', 'Modirum DS-3DS-Emulator', '', '', '', '', '7979791', 'A', NULL, '', 0);

INSERT INTO ds_hsmdevice (id, name, className) VALUES ('1', 'Software engine', 'com.modirum.ds.hsm.SoftwareJCEService');