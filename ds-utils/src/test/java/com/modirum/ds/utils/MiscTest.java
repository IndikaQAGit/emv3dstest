package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MiscTest {

    @Test
    public void isNullOrEmptyObject() {
        Assertions.assertTrue(Misc.isNullOrEmpty((String) null));
        Assertions.assertTrue(Misc.isNullOrEmpty(""));
        Assertions.assertFalse(Misc.isNullOrEmpty(" "));

        String[] strArray = new String[0];
        Assertions.assertSame(Misc.isNullOrEmpty((Object) strArray), Misc.isNullOrEmpty(strArray));
        Assertions.assertTrue(Misc.isNullOrEmpty((Object) new char[0]));
        Assertions.assertTrue(Misc.isNullOrEmpty((Object) new byte[0]));

        strArray = new String[1];
        Assertions.assertSame(Misc.isNullOrEmpty((Object) strArray), Misc.isNullOrEmpty(strArray));
        Assertions.assertFalse(Misc.isNullOrEmpty((Object) new char[1]));
        Assertions.assertFalse(Misc.isNullOrEmpty((Object) new byte[1]));

        Assertions.assertFalse(Misc.isNullOrEmpty(new long[0]));

        List<Object> list = new ArrayList<Object>();
        Assertions.assertSame(Misc.isNullOrEmpty((Object) list), Misc.isNullOrEmpty(list));
    }

    @Test
    public void isNullOrEmptyCollections() {
        List<Integer> list = new ArrayList<Integer>();
        Set<Integer> set = new HashSet<Integer>();

        Assertions.assertTrue(Misc.isNullOrEmpty(list));
        Assertions.assertTrue(Misc.isNullOrEmpty(set));

        list.add(1);
        set.add(1);

        Assertions.assertFalse(Misc.isNullOrEmpty(list));
        Assertions.assertFalse(Misc.isNullOrEmpty(set));
    }

    @Test
    public void mergeStrings() {
        String empty = "";
        String space = " ";
        String hello = "Hello";
        String world = "World";
        String helloWorld = "Hello World";

        Assertions.assertNull(Misc.merge(null, null));
        Assertions.assertEquals(hello, Misc.merge(hello, null));
        Assertions.assertEquals(world, Misc.merge(null, world));
        Assertions.assertEquals(hello, Misc.merge(hello, empty));
        Assertions.assertEquals(world, Misc.merge(empty, world));
        Assertions.assertEquals(hello + "  ", Misc.merge(hello, space));
        Assertions.assertEquals("  " + world, Misc.merge(space, world));
        Assertions.assertEquals(helloWorld, Misc.merge(hello, world));
    }

    @Test
    public void lastBytes() {
        byte[] hello = "hello".getBytes(StandardCharsets.UTF_8);
        byte[] ello = "ello".getBytes(StandardCharsets.UTF_8);
        Assertions.assertTrue(Arrays.equals(ello, Misc.lastBytes(hello, 1)));
        Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () -> Misc.lastBytes(hello, -1));
    }

    @Test
    public void isNullOrEmptyStringArray() {
        String[] emptyArray = new String[0];
        String[] arrayWithEmptyString = new String[]{""};
        String[] arrayWithSpaceString = new String[]{" "};
        String[] arrayWithEmptyStringAndNonEmptyString = new String[]{"", "a"};
        String[] arrayWithNonEmptyStringAndEmptyString = new String[]{"a", ""};

        Assertions.assertTrue(Misc.isNullOrEmpty(emptyArray));
        Assertions.assertTrue(Misc.isNullOrEmpty(arrayWithEmptyString));
        Assertions.assertFalse(Misc.isNullOrEmpty(arrayWithSpaceString));
        Assertions.assertTrue(Misc.isNullOrEmpty(arrayWithEmptyStringAndNonEmptyString));
        Assertions.assertFalse(Misc.isNullOrEmpty(arrayWithNonEmptyStringAndEmptyString));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   "})
    public void isNullOrEmptyObjectWithTrim(String string) {
        Assertions.assertTrue(Misc.isNullOrEmptyTrimmed(string));
    }

    @Test
    public void notIsNullOrEmptyObjectWithTrim() {
        Assertions.assertFalse(Misc.isNullOrEmptyTrimmed(" a "));
        Assertions.assertFalse(Misc.isNullOrEmptyTrimmed(0));
    }

    @Test
    public void cutOrTruncateString() {
        String hello = "hello";

        Assertions.assertNull(Misc.cut(null, 4));
        Assertions.assertEquals("", Misc.cut(hello, 0));
        Assertions.assertEquals("hell", Misc.cut(hello, 4));
        Assertions.assertEquals("hello", Misc.cut(hello, 6));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.cut(hello, -1));

        // Misc.trunc() is equivalent to Misc.cut()
        Assertions.assertNull(Misc.trunc(null, 4));
        Assertions.assertEquals("", Misc.trunc("", 0));
        Assertions.assertEquals("hell", Misc.trunc(hello, 4));
        Assertions.assertEquals("hello", Misc.trunc(hello, 6));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.trunc(hello, -1));

        Assertions.assertNull(Misc.cutLeft(null, 4));
        Assertions.assertEquals("", Misc.cutLeft("", 0));
        Assertions.assertEquals("ello", Misc.cutLeft(hello, 4));
        Assertions.assertEquals("hello", Misc.cutLeft(hello, 6));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.cutLeft(hello, -1));
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void emptyOrNullStringToNull(String string) {
        Assertions.assertNull(Misc.defaultToNull(string));
    }

    public void notEmptyOrNullStringToNull() {
        Assertions.assertNotNull(Misc.defaultToNull(" "));
    }

    @Test
    public void emptyOrNullObjectToEmptyString() {
        Assertions.assertEquals("", Misc.defaultString(null));
        Assertions.assertEquals("", Misc.defaultString(""));
        Assertions.assertEquals("1", Misc.defaultString(1));
    }

    @Test
    public void nullSafeList() {
        Assertions.assertNotNull(Misc.nullSafe(null));
        Assertions.assertNotNull(Misc.nullSafe(Collections.emptyList()));
    }

    @Test
    public void isIntAndParseInt() {
        Assertions.assertTrue(Misc.isInt(String.valueOf(Integer.MAX_VALUE)));
        Assertions.assertTrue(Misc.isInt(String.valueOf(Integer.MIN_VALUE)));
        Assertions.assertFalse(Misc.isInt(String.valueOf(1L + Integer.MAX_VALUE)));
        Assertions.assertFalse(Misc.isInt(String.valueOf(Integer.MIN_VALUE - 1L)));
        Assertions.assertFalse(Misc.isInt(null));
        Assertions.assertFalse(Misc.isInt(""));
        Assertions.assertFalse(Misc.isInt("a"));

        Assertions.assertEquals(1, Misc.parseInt("1"));
        Assertions.assertEquals(Integer.MAX_VALUE, Misc.parseInt(String.valueOf(Integer.MAX_VALUE)));
        Assertions.assertEquals(Integer.MIN_VALUE, Misc.parseInt(String.valueOf(Integer.MIN_VALUE)));
        Assertions.assertEquals(0, Misc.parseInt(String.valueOf(1L + Integer.MAX_VALUE)));
        Assertions.assertEquals(0, Misc.parseInt(String.valueOf(Integer.MIN_VALUE - 1L)));
        Assertions.assertEquals(0, Misc.parseInt(null));
        Assertions.assertEquals(0, Misc.parseInt(""));
        Assertions.assertEquals(0, Misc.parseInt("a"));

        Assertions.assertEquals(1, Misc.parseIntBoxed("1"));
        Assertions.assertEquals(Integer.MAX_VALUE, Misc.parseIntBoxed(String.valueOf(Integer.MAX_VALUE)));
        Assertions.assertEquals(Integer.MIN_VALUE, Misc.parseIntBoxed(String.valueOf(Integer.MIN_VALUE)));
        Assertions.assertNull(Misc.parseIntBoxed(String.valueOf(1L + Integer.MAX_VALUE)));
        Assertions.assertNull(Misc.parseIntBoxed(String.valueOf(Integer.MIN_VALUE - 1L)));
        Assertions.assertNull(Misc.parseIntBoxed(null));
        Assertions.assertNull(Misc.parseIntBoxed(""));
        Assertions.assertNull(Misc.parseIntBoxed("a"));
    }

    @Test
    public void isLongAndParseLong() {
        BigInteger maxLongPlusOne = BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.ONE);
        BigInteger minLongMinusOne = BigInteger.valueOf(Long.MIN_VALUE).subtract(BigInteger.valueOf(1));

        Assertions.assertTrue(Misc.isLong(String.valueOf(Long.MAX_VALUE)));
        Assertions.assertTrue(Misc.isLong(String.valueOf(Long.MIN_VALUE)));
        Assertions.assertFalse(Misc.isLong(maxLongPlusOne.toString()));
        Assertions.assertFalse(Misc.isLong(minLongMinusOne.toString()));
        Assertions.assertFalse(Misc.isLong(null));
        Assertions.assertFalse(Misc.isLong(""));
        Assertions.assertFalse(Misc.isLong("a"));

        Assertions.assertEquals(1, Misc.parseLong("1"));
        Assertions.assertEquals(Long.MAX_VALUE, Misc.parseLong(String.valueOf(Long.MAX_VALUE)));
        Assertions.assertEquals(Long.MIN_VALUE, Misc.parseLong(String.valueOf(Long.MIN_VALUE)));
        Assertions.assertEquals(0, Misc.parseLong(maxLongPlusOne.toString()));
        Assertions.assertEquals(0, Misc.parseLong(minLongMinusOne.toString()));
        Assertions.assertEquals(0, Misc.parseLong(null));
        Assertions.assertEquals(0, Misc.parseLong(""));
        Assertions.assertEquals(0, Misc.parseLong("a"));

        Assertions.assertEquals(1, Misc.parseLongBoxed("1"));
        Assertions.assertEquals(Long.MAX_VALUE, Misc.parseLongBoxed(String.valueOf(Long.MAX_VALUE)));
        Assertions.assertEquals(Long.MIN_VALUE, Misc.parseLongBoxed(String.valueOf(Long.MIN_VALUE)));
        Assertions.assertNull(Misc.parseLongBoxed(maxLongPlusOne.toString()));
        Assertions.assertNull(Misc.parseLongBoxed(minLongMinusOne.toString()));
        Assertions.assertNull(Misc.parseLongBoxed(null));
        Assertions.assertNull(Misc.parseLongBoxed(""));
        Assertions.assertNull(Misc.parseLongBoxed("a"));
    }

    @Test
    public void parseDouble() {
        Double d = Misc.parseDouble(String.valueOf(Double.MAX_VALUE) + 1);
        Assertions.assertEquals(1.0, Misc.parseDouble("1"));
        Assertions.assertEquals(Double.MAX_VALUE, Misc.parseDouble(String.valueOf(Double.MAX_VALUE)));
        Assertions.assertEquals(Double.MIN_VALUE, Misc.parseDouble(String.valueOf(Double.MIN_VALUE)));
        Assertions.assertEquals(0.0, Misc.parseDouble(null));
        Assertions.assertEquals(0.0, Misc.parseDouble(""));
        Assertions.assertEquals(0.0, Misc.parseDouble("a"));
    }

    @Test
    public void replaceString() {
        String hello = "hello";
        String world = "world";
        String helloString = "hello hello hello hello";
        String allReplaced = "world world world world";
        String oneReplaced = "world hello hello hello";

        Assertions.assertEquals(allReplaced, Misc.replace(helloString, "hello", "world"));

        StringBuilder helloStringBuilder = new StringBuilder(helloString);
        Misc.replace(helloStringBuilder, hello, world, 0);
        Assertions.assertEquals(oneReplaced, helloStringBuilder.toString());

        helloStringBuilder = new StringBuilder(helloString);
        Misc.replace(helloStringBuilder, hello, world, 1);
        Assertions.assertEquals(oneReplaced, helloStringBuilder.toString());

        helloStringBuilder = new StringBuilder(helloString);
        Misc.replace(helloStringBuilder, hello, world, Integer.MAX_VALUE);
        Assertions.assertEquals(allReplaced, helloStringBuilder.toString());

        StringBuffer helloStringBuffer = new StringBuffer(helloString);
        Misc.replace(helloStringBuffer, hello, world, 0);
        Assertions.assertEquals(oneReplaced, helloStringBuffer.toString());

        helloStringBuffer = new StringBuffer(helloString);
        Misc.replace(helloStringBuffer, hello, world, 1);
        Assertions.assertEquals(oneReplaced, helloStringBuffer.toString());

        helloStringBuffer = new StringBuffer(helloString);
        Misc.replace(helloStringBuffer, hello, world, Integer.MAX_VALUE);
        Assertions.assertEquals(allReplaced, helloStringBuffer.toString());
    }

    @Test
    public void splitString() {
        String stringToSplit = "abc12def12ghi12jkl1212";
        String delimiter = "12";
        String[] splitStrings = new String[]{"abc", "def", "ghi", "jkl", "", ""};

        Assertions.assertTrue(Arrays.equals(splitStrings, Misc.split(stringToSplit, delimiter)));
        Assertions.assertTrue(Arrays.equals(splitStrings, Misc.split(new StringBuilder(stringToSplit), delimiter)));
        Assertions.assertTrue(Arrays.equals(splitStrings, Misc.splitToList(stringToSplit, delimiter).toArray()));
    }

    @Test
    public void splitStringIntoTwo() {
        String stringToSplit = "abc12def12ghi12jkl1212";

        String delimiter = "1";
        String[] expectedArray = new String[]{"abc", "2def12ghi12jkl1212"};
        Assertions.assertTrue(Arrays.equals(expectedArray, Misc.splitTwo(stringToSplit, delimiter)));

        delimiter = "12";
        expectedArray = new String[]{"abc", "2def12ghi12jkl1212"};
        Assertions.assertTrue(Arrays.equals(expectedArray, Misc.splitTwo(stringToSplit, delimiter)));

        delimiter = "123";
        expectedArray = new String[]{"abc12def12ghi12jkl1212"};
        Assertions.assertTrue(Arrays.equals(expectedArray, Misc.splitTwo(stringToSplit, delimiter)));

        delimiter = "1212";
        expectedArray = new String[]{"abc12def12ghi12jkl", "212"};
        Assertions.assertTrue(Arrays.equals(expectedArray, Misc.splitTwo(stringToSplit, delimiter)));

        stringToSplit = "abc12def12ghi12jkl3";
        delimiter = "3";
        expectedArray = new String[]{"abc12def12ghi12jkl", ""};
        Assertions.assertTrue(Arrays.equals(expectedArray, Misc.splitTwo(stringToSplit, delimiter)));
    }

    @Test
    public void mask() {
        String number = "12345678901";

        Assertions.assertEquals("###########", Misc.mask(number, 0));
        Assertions.assertEquals("1#########1", Misc.mask(number, 1));
        Assertions.assertEquals("12#######01", Misc.mask(number, 2));
        Assertions.assertEquals("12345678901", Misc.mask(number, 6));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.mask(number, -1));

        Assertions.assertEquals("###########", Misc.mask(number, 0, 0));
        Assertions.assertEquals("1234###8901", Misc.mask(number, 4, 4));
        Assertions.assertEquals("1234##78901", Misc.mask(number, 4, 5));
        Assertions.assertEquals("1234#678901", Misc.mask(number, 4, 6));
        Assertions.assertEquals("12345678901", Misc.mask(number, 4, 8));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.mask(number, -1, 8));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.mask(number, 8, -1));

        Assertions.assertEquals("###########", Misc.maskEnd(number, 0));
        Assertions.assertEquals("1##########", Misc.maskEnd(number, 1));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.maskEnd(number, -1));

        Assertions.assertEquals("##..7..##", Misc.maskIntelligent(number, 0));
        Assertions.assertEquals("1##..5..##1", Misc.maskIntelligent(number, 1));
        Assertions.assertEquals("12##..3..##01", Misc.maskIntelligent(number, 2));
        Assertions.assertEquals("123##..1..##901", Misc.maskIntelligent(number, 3));
        Assertions.assertEquals("1234###8901", Misc.maskIntelligent(number, 4));
        Assertions.assertEquals("12345#78901", Misc.maskIntelligent(number, 5));
        Assertions.assertEquals("12345678901", Misc.maskIntelligent(number, 6));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.maskIntelligent(number, -1));

        String alphanumeric = "1A3C5E7G9I1";
        Assertions.assertEquals("1A#C#E#G#I1", Misc.maskNumsOnly(alphanumeric, 2, 2));
        Assertions.assertEquals("1A3C#E#G9I1", Misc.maskNumsOnly(alphanumeric, 3, 3));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.maskNumsOnly(number, -1, 1));
        Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> Misc.maskNumsOnly(number, 1, -1));
    }

    @Test
    public void byteArraytoStringBuilder() {
        String value = "Hello";
        StringBuilder stringBuilder = Misc.toStringBuilder(value.getBytes(StandardCharsets.ISO_8859_1));
        Assertions.assertEquals(value, stringBuilder.toString());

        // UTF8 not handled at the moment
        value = "您好";
        stringBuilder = Misc.toStringBuilder(value.getBytes(StandardCharsets.UTF_8));
        Assertions.assertNotEquals(value, stringBuilder.toString());
    }

    @Test
    public void stringToByteArray() {
        String value = "Hello";
        byte[] byteArray = Misc.toBytes(value);
        Assertions.assertEquals(value, new String(byteArray));

        byteArray = Misc.toBytes(StandardCharsets.ISO_8859_1, value);
        Assertions.assertEquals(value, new String(byteArray));

        value = "您好";
        byteArray = Misc.toUtf8Bytes(value);
        Assertions.assertEquals(value, new String(byteArray, StandardCharsets.UTF_8));

        byteArray = Misc.toBytes(StandardCharsets.UTF_8, value);
        Assertions.assertEquals(value, new String(byteArray));
    }

    @Test
    public void encodeHtmlForForm() {
        String data = "<>&\"";
        String htmlEncodedData = "&lt;&gt;&amp;&quot;";
        Assertions.assertEquals(htmlEncodedData, Misc.encodeHTMLForForm(data));
    }

    @Test
    public void indexOfBytes() {
        String data = "halohalo";
        String search = "lo";
        Assertions.assertEquals(2, Misc.indexOf(data.getBytes(), search.getBytes()));
        Assertions.assertEquals(6, Misc.indexOf(data.getBytes(), search.getBytes(), 4));
        Assertions.assertEquals(6, Misc.indexOf(data, search, 4));
    }

    @Test
    public void longToBytesAndBack() {
        long max = Long.MAX_VALUE;
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(max);
        byte[] maxByteArray = Misc.longToBytes(max);
        Assertions.assertTrue(Arrays.equals(buffer.array(), maxByteArray));
        Assertions.assertEquals(max, Misc.bytesToLong(maxByteArray));
    }

    @Test
    public void ipFunctions() {
        String ip = "10.0.0.1";
        Assertions.assertTrue(Misc.isIpV4Address(ip));
        Assertions.assertFalse(Misc.isIpV4Address(ip + "A"));
        Assertions.assertFalse(Misc.isIpV4Address(null));

        Assertions.assertNull(Misc.parseIpV4AddressRange(null));
        Assertions.assertNull(Misc.parseIpV4AddressRange(""));
        Assertions.assertNull(Misc.parseIpV4AddressRange("192.168.0.0-192.168.168.168-192.168.255.255"));

        int[] range = Misc.parseIpV4AddressRange("192.168.0.0-192.168.255.255");
        Assertions.assertNull(Misc.parseIpV4AddressRange("192.168.0.0-192.168.255.A"));
        Assertions.assertEquals(Misc.parseIpV4Value("192.168.0.0"), range[0]);
        Assertions.assertEquals(Misc.parseIpV4Value("192.168.255.255"), range[1]);

        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            int result = 0;
            for (byte b : ipAddress.getAddress()) {
                result = result << 8 | (b & 0xFF);
            }
            Assertions.assertEquals(result, Misc.parseIpV4Value(ip));
        } catch (Throwable t) {
            Assertions.fail("Failed validating IP operations");
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {"10", "10.0000001"})
    public void isNumericStringInRange(String string) {
        Assertions.assertTrue(Misc.isInRange(string, 10.0, 11.0));
    }

    @ParameterizedTest
    @ValueSource(strings = {"11.0000001", "a"})
    public void notIsNumericStringInRange(String string) {
        Assertions.assertFalse(Misc.isInRange(string, 10.0, 11.0));
    }

    @ParameterizedTest
    @CsvSource(value = {"0000000123,0,10", "'       123',' ',10", "XXXXXXX123,X,10", "23,X,2"})
    public void padStringLeft(String expected, Character mask, int length) {
        Assertions.assertEquals(expected, Misc.padCutStringLeft("123", mask, length));
        Assertions.assertThrows(IllegalArgumentException.class, () -> Misc.padCutStringLeft(null, '0', 10));
        Assertions.assertThrows(IllegalArgumentException.class, () -> Misc.padCutStringLeft("123", '0', -1));
    }

    @ParameterizedTest
    @CsvSource(value = {",10", "123,-1"})
    public void padStringLeftThrowsException(String expected, int length) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Misc.padCutStringLeft(expected, '0', length));
    }


    @ParameterizedTest
    @CsvSource(value = {"1230000000,0,10", "'123       ',' ',10", "123XXXXXXX,X,10", "12,X,2"})
    public void padStringRight(String expected, Character mask, int length) {
        Assertions.assertEquals(expected, Misc.padCutStringRight("123", mask, length));
    }

    @ParameterizedTest
    @CsvSource(value = {",10", "123,-1"})
    public void padStringRightThrowsException(String expected, int length) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Misc.padCutStringRight(expected, '0', length));
    }

    @Test
    public void isBase64() {
        String text = "Unencoded string.";
        String base64EncodedText = Base64.encode(text.getBytes(StandardCharsets.UTF_8));
        Assertions.assertFalse(Misc.isBase64(text.getBytes(StandardCharsets.UTF_8)));
        Assertions.assertTrue(Misc.isBase64(base64EncodedText.getBytes(StandardCharsets.UTF_8)));
        Assertions.assertFalse(Misc.isBase64(null));
        Assertions.assertFalse(Misc.isBase64(new byte[0]));

        String url = "http://google.com?param1=testing&param2=¾a";
        String base64EncodedUrl = Base64.encode(url.getBytes(StandardCharsets.UTF_8));
        String base64UrlEncodedUrl = Base64.encodeURL(url.getBytes(StandardCharsets.UTF_8));
        Assertions.assertFalse(Misc.isBase64(base64EncodedUrl.getBytes(StandardCharsets.UTF_8), Base64.Mode.cURL));
        Assertions.assertTrue(Misc.isBase64(base64UrlEncodedUrl.getBytes(StandardCharsets.UTF_8), Base64.Mode.cURL));
        Assertions.assertFalse(Misc.isBase64(null, Base64.Mode.cURL));
        Assertions.assertFalse(Misc.isBase64(new byte[0], Base64.Mode.cURL));
    }

    @Test
    public void isNumber() {
        Assertions.assertFalse(Misc.isNumber(""));
        Assertions.assertFalse(Misc.isNumber(null));
        Assertions.assertFalse(Misc.isNumber("a"));
        Assertions.assertTrue(Misc.isNumber("1"));
        Assertions.assertTrue(Misc.isNumber("1.1"));
        Assertions.assertTrue(Misc.isNumber("1.1."));
        Assertions.assertTrue(Misc.isNumber("1.1.1"));
    }

    @Test
    public void calculateSHA256AndEncodeBASE64() {
        Assertions.assertEquals("47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=",
                Misc.calculateSHA256AndEncodeBASE64(""));
        Assertions.assertEquals("fRpUEnsiJQL1t5tfsIAwYRUqRPkrN+I8ZSe69mXU2po=",
                Misc.calculateSHA256AndEncodeBASE64("abcdefg"));
        Assertions.assertThrows(RuntimeException.class, () -> Misc.calculateSHA256AndEncodeBASE64(null));
    }

    @Test
    public void arrayToCsv() {
        Assertions.assertNull(Misc.arrayToCSV(null));
        Assertions.assertEquals("", Misc.arrayToCSV(new Object[0]));
        Assertions.assertEquals("a,b,c", Misc.arrayToCSV(new Object[]{"a", "b", "c"}));
    }

    @Test
    public void listToCsv() {
        Assertions.assertNull(Misc.listToCSV(null));
        Assertions.assertEquals("", Misc.listToCSV(Collections.emptyList()));
        Assertions.assertEquals("a,b,c", Misc.listToCSV(Arrays.asList("a", "b", "c")));
    }

    @Test
    public void objectInArray() {
        Assertions.assertFalse(Misc.in(null, null));
        Assertions.assertTrue(Misc.in(null, new Object[]{null}));
        Assertions.assertTrue(Misc.in(1L, new Object[]{new Long(1)}));
        Assertions.assertFalse(Misc.in(1L, new Object[]{1}));
        Assertions.assertFalse(Misc.in("A", new Object[]{"B"}));
    }

    @Test
    public void isBCP47LanguageTag() {
        Assertions.assertFalse(Misc.isBCP47LanguageTag(null));
        Assertions.assertFalse(Misc.isBCP47LanguageTag(""));
        Assertions.assertTrue(Misc.isBCP47LanguageTag("en"));
        Assertions.assertTrue(Misc.isBCP47LanguageTag("en-US"));
        Assertions.assertFalse(Misc.isBCP47LanguageTag("en1"));
        Assertions.assertFalse(Misc.isBCP47LanguageTag("العربية"));
    }

    @Test
    public void ipv6() {
        String ip = "2001:0db8:85a3:0000:0000:8a2e:0370:7334";
        Assertions.assertTrue(Misc.isIpV6Address(ip));
        Assertions.assertFalse(Misc.isIpV6Address(ip + "A"));
        Assertions.assertFalse(Misc.isIpV6Address(null));
    }
}
