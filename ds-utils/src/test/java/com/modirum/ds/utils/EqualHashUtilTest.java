package com.modirum.ds.utils;

import com.modirum.ds.utils.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EqualHashUtilTest {

    @Test
    public void testHash() {
        Person john = Person.builder().name("John").build();
        Person john2 = Person.builder().name("John").build();
        Assertions.assertEquals(EqualHashUtil.hashCode(john), EqualHashUtil.hashCode(john2));

        john.setAge(25);
        john2.setAge(23);
        Assertions.assertNotEquals(EqualHashUtil.hashCode(john), EqualHashUtil.hashCode(john2));
    }

    @Test
    public void testEquals() {
        Person john = Person.builder().name("John").build();
        Person john2 = Person.builder().name("John").build();
        Assertions.assertTrue(EqualHashUtil.equals(john, john2));

        john.setAge(25);
        john2.setAge(23);
        Assertions.assertFalse(EqualHashUtil.equals(john, john2));
    }

}
