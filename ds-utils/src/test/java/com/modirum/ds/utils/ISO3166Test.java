package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ISO3166Test {

    @Test
    public void checkISO3166() {
        for (ISO3166 entry: ISO3166.values()) {
            Assertions.assertEquals(entry, ISO3166.findFromNumeric(entry.getNumeric()));
            Assertions.assertEquals(2, entry.getA2code().length());
            Assertions.assertEquals(3, entry.getA3code().length());
            Assertions.assertFalse(Misc.isNullOrEmpty(entry.getCountryName()));
        }

        ISO3166 usEntry = ISO3166.findFromNumeric((short)840);
        Assertions.assertNotNull(usEntry);
        Assertions.assertFalse(usEntry.subDivisions.isEmpty());

        Assertions.assertNull(ISO3166.findFromNumeric((short)0));
    }

}
