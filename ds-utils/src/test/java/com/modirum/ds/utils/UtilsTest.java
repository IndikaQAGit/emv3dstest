package com.modirum.ds.utils;

import com.modirum.ds.utils.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Properties;

public class UtilsTest {

    @Test
    public void loadLog4jConfigFromProperties() {
        String output = null;
        try {
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                // need to read string from system.out to know if log4j configuration succeeded
                System.setOut(new java.io.PrintStream(out));

                ClassLoader classLoader = getClass().getClassLoader();
                URL resource = classLoader.getResource("log4j2.xml");
                File f = new File(resource.toURI());
                Utils.configureLog4j(f.getAbsolutePath());

                Properties settings = new Properties();
                settings.put("log4j.rootLogger", "CONSOLE");
                Utils.configureLog4j(settings);

                output = out.toString();
                Assertions.assertFalse(output.contains("log4j2 property configuration failed") ||
                        output.contains("log4j2 configuration failed"));
            } finally {
                // redirect to console output
                System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
            }
        } catch (Throwable t) {
            Assertions.fail("Error testing loading of log4j config", t);
        }
    }

    @Test
    public void testUrlEncode() {
        try {
            String value = "$&+,/:;=?@";
            String urlEncodedValue = Utils.encode(value, StandardCharsets.UTF_8.toString());
            Assertions.assertEquals(value, URLDecoder.decode(urlEncodedValue, StandardCharsets.UTF_8.toString()));
        } catch (Throwable t) {
            Assertions.fail("Problem detected with encoding param value", t);
        }
    }

    @Test
    public void saveFile() {
        String filename = "testing-ds-utils-save-file.text";
        File f = new File(filename);
        try {
            String data = "Entwickeln Sie mit Vergnügen";
            byte[] dataByteArray = data.getBytes(StandardCharsets.UTF_8);
            Utils.saveFile(filename, dataByteArray);

            byte[] fileContents = Utils.readFile(filename);
            Assertions.assertTrue(Arrays.equals(dataByteArray, fileContents));

            // replace file
            String newData = "Develop with pleasure";
            byte[] newDataByteArray = newData.getBytes(StandardCharsets.UTF_8);
            Utils.saveFile(filename, newDataByteArray);

            fileContents = Utils.readFile(f);
            Assertions.assertTrue(Arrays.equals(newDataByteArray, fileContents));
        } catch (Throwable t) {
            Assertions.fail("Failed file operations", t);
        } finally {
            if (f.exists()) {
                f.delete();
            }
        }
    }

    @Test
    public void getLocalNonLoIp() {
        Assertions.assertNotNull(Utils.getLocalNonLoIp());
    }

    @Test
    public void getClassOrigin() {
        String origin = Utils.classOrigin(new Person());
        Assertions.assertNotNull(origin);
        Assertions.assertNotEquals("Unknown", origin);
    }
}
