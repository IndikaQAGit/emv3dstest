package com.modirum.ds.utils.db;

import org.hibernate.engine.jdbc.dialect.spi.DialectResolutionInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CustomDialectResolverTest {

    @Mock
    DialectResolutionInfo info;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void resolveDialect() {
        CustomDialectResolver resolver = new CustomDialectResolver();

        Mockito.when(info.getDatabaseName()).thenReturn("MySQL");
        Assertions.assertEquals("org.hibernate.dialect.MySQLDialect", resolver.resolveDialect(info).toString());
    }

}
