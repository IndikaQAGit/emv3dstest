package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class StringBuilderReaderTest {

    @Test
    public void test() {
        StringBuilder builder = new StringBuilder();
        builder.append("test");

        try (StringBuilderReader reader = new StringBuilderReader(builder)) {
            try {
                Assertions.assertTrue(reader.ready());
                Assertions.assertTrue(reader.markSupported());

                reader.mark(0);

                int i = (char) -1;
                do {
                    i = reader.read();
                } while (i != -1);

                reader.reset();
                reader.skip(1);
                char[] cbuf = new char[4];
                cbuf[0] = 't';
                reader.read(cbuf, 1, 3);

                Assertions.assertTrue(Arrays.equals(builder.toString().toCharArray(), cbuf));

            } catch (Throwable t) {
                Assertions.fail("Something went wrong", t);
            }
        }
    }
}
