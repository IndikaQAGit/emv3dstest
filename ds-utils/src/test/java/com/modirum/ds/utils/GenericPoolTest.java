package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GenericPoolTest {

    @Test
    public void takeAndReturn() {
        GenericPool<StringBuilder> pool = new GenericPool<StringBuilder>();
        Assertions.assertNull(pool.reuse());

        StringBuilder stringBuilder1 = new StringBuilder();
        StringBuilder stringBuilder2 = new StringBuilder();
        pool.recycle(stringBuilder1);
        pool.recycle(stringBuilder2);

        Assertions.assertEquals(stringBuilder2, pool.reuse());
        Assertions.assertEquals(stringBuilder1, pool.reuse());

        Assertions.assertNull(pool.reuse());
        pool.recycle(stringBuilder1);
        pool.recycle(stringBuilder1);

        Assertions.assertEquals(stringBuilder1, pool.reuse());

        StringBuilder duplicateReference = pool.reuse();
        Assertions.assertNull(duplicateReference);
        Assertions.assertNotEquals(stringBuilder1, duplicateReference);
    }

}
