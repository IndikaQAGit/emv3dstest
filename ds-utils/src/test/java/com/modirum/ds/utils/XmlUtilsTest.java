package com.modirum.ds.utils;

import com.modirum.ds.utils.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class XmlUtilsTest {

    @Test
    public void checkXESafeDocumentBuilderFactory() {
        try {
            DocumentBuilderFactory dbf = XmlUtils.getXESafeDocumentBuilderFactory();
            DocumentBuilder db = dbf.newDocumentBuilder();

            // find a suitable xml to test against
            String xml = "<?xml version=\"1.0\" standalone=\"no\"?>\n" +
                    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n" +
                    "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\" [\n" +
                    "<!ENTITY current.version    \"3.0-RELEASE\">\n" +
                    "<!ENTITY last.version       \"2.2.7-RELEASE\">\n" +
                    "]>";
            try (InputStream targetStream = new ByteArrayInputStream(xml.getBytes())) {
                SAXParseException saxpe = Assertions.assertThrows(SAXParseException.class, () -> db.parse(targetStream), "");
                if (!(saxpe.getMessage() != null && saxpe.getMessage().contains("DOCTYPE is disallowed"))) {
                    Assertions.fail("Failed to parse xml", saxpe);
                }
            }
        } catch (Exception e) {
            Assertions.fail("Failed to parse xml", e);
        }
    }

    @Test
    public void marshalAndUnmarshalObject() {
        Assertions.assertDoesNotThrow( () -> {
            Person person = Person.builder().name("John").age(25).build();
            XmlUtils.JaxbHelp<Person> jaxbHelp = new XmlUtils.JaxbHelp<Person>(Person.class);
            jaxbHelp.setFormatted(true);
            byte[] marshalledPerson = jaxbHelp.marshal(person);
            Person unmarshalledPerson = jaxbHelp.unmarshal(new ByteArrayInputStream(marshalledPerson));
            Assertions.assertEquals(person.name, unmarshalledPerson.name);
            Assertions.assertEquals(person.age, unmarshalledPerson.age);
        });
    }

}
