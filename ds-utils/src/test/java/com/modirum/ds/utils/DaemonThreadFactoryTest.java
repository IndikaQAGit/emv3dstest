package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DaemonThreadFactoryTest {

    @Test
    public void checkThreadName() {
        String daemonName = "test";
        DaemonThreadFactory threadFactory = new DaemonThreadFactory(daemonName);
        Thread thread = threadFactory.newThread(() -> {});
        Assertions.assertTrue(thread.getName().startsWith(daemonName + " "));
        Assertions.assertTrue(thread.isDaemon());
    }

}
