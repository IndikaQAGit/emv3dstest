package com.modirum.ds.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

public class ContextTest {

    Context context = null;

    @BeforeEach
    public void beforeAll() {
        context = new Context();
    }

    @Test
    public void getBestLocaleForLang() {
        Assertions.assertEquals(Locale.ENGLISH, context.getBestLocaleForLang("en"));
        Assertions.assertEquals(Locale.US, context.getBestLocaleForLang("en_US"));
        Assertions.assertNull(context.getBestLocaleForLang("en_XX"));
    }

    @Test
    public void getSupportedLocalesWithNoLocaleList() {
        try {
            List<Locale> supportedLocales = context.getSupportedLocaleList();
            Assertions.assertEquals(1, supportedLocales.size());
            Assertions.assertEquals(context.getDefaultLocale(), supportedLocales.get(0));
        } catch (Throwable t) {
            Assertions.fail("Failed to get supported locales", t);
        }
    }

    @Test
    public void getSupportedLocalesWithProvidedLocaleList() {
        try {
            String localeList = String.join(",", Locale.FRANCE.toString(), Locale.ITALY.toString());
            Properties properties = new Properties();
            properties.put("supportedLocales", localeList);
            context.setApplicationAttribute("properties", properties);

            List<Locale> supportedLocales = context.getSupportedLocaleList();
            Assertions.assertEquals(2, supportedLocales.size());
            Assertions.assertEquals(Locale.FRANCE, supportedLocales.get(0));
            Assertions.assertEquals(Locale.ITALY, supportedLocales.get(1));
        } catch (Throwable t) {
            Assertions.fail("Failed to get supported locales", t);
        }
    }

    @Test
    public void getLocaleStringSetting() {
        String greeting = "greeting";
        String frenchGreeting = "bon matin";
        String italianGreeting = "buongiorno";
        String englishGreeting = "good morning";
        Properties properties = new Properties();
        properties.setProperty(greeting + "." + Locale.FRANCE, frenchGreeting);
        properties.setProperty(greeting + "." + Locale.ITALY, italianGreeting);
        properties.setProperty(greeting + "." + Locale.US, englishGreeting);
        properties.setProperty("defaultlocale", Locale.ITALY.toString());

        context.getCtxHolder().setApplicationAttribute("properties", properties);

        context.setLang(Locale.FRANCE.toString());
        Assertions.assertEquals(frenchGreeting, context.getLocaleStringSetting(greeting));
        context.setLang(Locale.ITALY.toString());
        Assertions.assertEquals(italianGreeting, context.getLocaleStringSetting(greeting));

        // override default locale via settings (refer to defaultlocale property set above)
        context.setLocale(context.getDefaultLocale());
        Assertions.assertEquals(italianGreeting, context.getLocaleStringSetting(greeting));

        // override default locale via session attribute
        context.setSessionAttribute("dafaultlocale", Locale.FRANCE);
        context.setLocale(context.getDefaultLocale());
        Assertions.assertEquals(frenchGreeting, context.getLocaleStringSetting(greeting));

        // when settings is removed, will default to US
        properties.remove("defaultlocale");
        // need to clear application cache
        context.resetSettingCache();
        // need to clear session attribute so default locale will be reevaluated
        ((Context.DefaultContextHolder)context.getCtxHolder()).removeSessionAttribute("dafaultlocale");
        context.setLocale(context.getDefaultLocale()); // defaultLocale is US unless overridden
        Assertions.assertEquals(englishGreeting, context.getLocaleStringSetting(greeting));
    }

    @Test
    public void callingGetDefaultLocaleSetsSessionLocale() {
        Assertions.assertNull(context.getSessionAttribute("dafaultlocale"));
        context.getDefaultLocale();
        Assertions.assertEquals(Locale.US, context.getSessionAttribute("dafaultlocale"));
    }

    @Test
    public void setLangSetsLocaleAsWell() {
        context.setLang(Locale.ITALY.toString());
        Assertions.assertEquals(Locale.ITALY, context.getLocale());
        Assertions.assertEquals(Locale.ITALY, context.getRequestAttribute(Context.locakeKey));
        Assertions.assertEquals(Locale.ITALY, context.getRequestAttribute(Context.locakeKey));
    }

    @Test
    public void getLocale() {
        Context.DefaultContextHolder contextHolder = (Context.DefaultContextHolder) context.getCtxHolder();
        context.setRequestAttribute(Context.locakeKey, Locale.FRANCE);
        context.setSessionAttribute(Context.locakeKey, Locale.CANADA);
        context.setRequestAttribute("locale.last", Locale.ITALY);

        // use request locale if set
        Assertions.assertEquals(Locale.FRANCE, context.getLocale());

        // else use session locale if set
        contextHolder.removeRequestAttribute(Context.locakeKey);
        Assertions.assertEquals(Locale.CANADA, context.getLocale());

        // else use request locale.last if set
        contextHolder.removeSessionAttribute(Context.locakeKey);
        Assertions.assertEquals(Locale.ITALY, context.getLocale());

        // else use default locale
        contextHolder.removeRequestAttribute("locale.last");
        // need to clear locakeKey again because using locale.last will set them
        contextHolder.removeRequestAttribute(Context.locakeKey);
        contextHolder.removeSessionAttribute(Context.locakeKey);
        Assertions.assertEquals(context.getDefaultLocale(), context.getLocale());
    }

    @Test
    public void settingLocaleSetsRequestAndSessionLocales() {
        context.setLocale(Locale.ITALY);
        Assertions.assertEquals(Locale.ITALY, context.getRequestAttribute(Context.locakeKey));
        Assertions.assertEquals(Locale.ITALY, context.getRequestAttribute(Context.locakeKey));
    }

    @Test
    public void settingLocaleClearsSessionAttributes() {
        context.setLocale(context.getDefaultLocale());
        Assertions.assertNull(context.getSessionAttribute("priceformat"));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.FULL));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.LONG));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.MEDIUM));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.SHORT));

        context.setSessionAttribute("priceformat", "x");
        context.setSessionAttribute("dateformat." + java.text.DateFormat.FULL, "x");
        context.setSessionAttribute("dateformat." + java.text.DateFormat.LONG, "x");
        context.setSessionAttribute("dateformat." + java.text.DateFormat.MEDIUM, "x");
        context.setSessionAttribute("dateformat." + java.text.DateFormat.SHORT, "x");

        Assertions.assertNotNull(context.getSessionAttribute("priceformat"));
        Assertions.assertNotNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.FULL));
        Assertions.assertNotNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.LONG));
        Assertions.assertNotNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.MEDIUM));
        Assertions.assertNotNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.SHORT));

        // setting locale again should remove the session attributes above
        context.setLocale(context.getDefaultLocale());
        Assertions.assertNull(context.getSessionAttribute("priceformat"));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.FULL));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.LONG));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.MEDIUM));
        Assertions.assertNull(context.getSessionAttribute("dateformat." + java.text.DateFormat.SHORT));
    }


    class TestSettingService implements SettingService {

        List<Setting> settings = new ArrayList<Setting>();

        @Override
        public Setting getSettingByKey(String key) {
            for (Setting setting: settings) {
                if (setting.getKey().equals(key)) {
                    return setting;
                }
            }
            return null;
        }

        @Override
        public Setting getSettingByKey(String key, Short procId) {
            for (Setting setting: settings) {
                if (setting.getKey() != null && setting.getKey().equals(key) && setting.getProcessorId() != null &&
                        setting.getProcessorId().equals(procId)) {
                    return setting;
                }
            }
            return getSettingByKey(key);
        }

        public void addSetting(String key, String value) {
            addSetting(key, null, value);
        }

        public void addSetting(String key, Short procId, String value) {
            Setting setting = new Setting();
            setting.setKey(key);
            setting.setProcessorId(procId);
            setting.setValue(value);
            settings.add(setting);
        }

    }

    @Test
    public void getSetting() {
        String settingName = "test.setting";
        Properties properties = new Properties();
        properties.setProperty(settingName, "1");
        context.getCtxHolder().setApplicationAttribute("properties", properties);

        Assertions.assertEquals("1", context.getSetting(settingName).getValue());
        Assertions.assertEquals("1", context.getSetting(settingName, (short) 1).getValue());
        Assertions.assertEquals("1", context.getStringSetting(settingName));
        Assertions.assertEquals(1, context.getIntSetting(settingName));

        Assertions.assertNull(context.getSetting(null));
        Assertions.assertNull(context.getSetting(null, (short) 1));
        Assertions.assertNull(context.getStringSetting(null));
        Assertions.assertEquals(0, context.getIntSetting(null));

        // setting will be created but with null value
        String nonexistentSettingName = "null.setting";
        Assertions.assertNotNull(context.getSetting(nonexistentSettingName));
        Assertions.assertNull(context.getSetting(nonexistentSettingName).getValue());
        Assertions.assertNotNull(context.getSetting(nonexistentSettingName, (short) 1));
        Assertions.assertNull(context.getSetting(nonexistentSettingName, (short) 1).getValue());
        Assertions.assertNull(context.getStringSetting(nonexistentSettingName));
        Assertions.assertEquals(0, context.getIntSetting(nonexistentSettingName));

        // processorId
        String processorIdSettingName = "setting.name";
        properties.setProperty(processorIdSettingName + "[10]", "10");
        properties.setProperty(processorIdSettingName + "[20]", "20");
        Assertions.assertNotNull(context.getSetting(processorIdSettingName, (short)10));
        Assertions.assertNotNull(context.getSetting(processorIdSettingName, (short)20));
        Assertions.assertEquals("10", context.getSetting(processorIdSettingName, (short)10).getValue());
        Assertions.assertEquals("20", context.getSetting(processorIdSettingName, (short)20).getValue());

        TestSettingService settingService = new TestSettingService();
        context.setSettingService(settingService);
        settingService.addSetting("greeting", "hello");
        Assertions.assertEquals("hello", context.getSetting("greeting").getValue());
    }

    @Test
    public void formatDate() {
        // also covers testing for getDateFormat() because t is used by formatDate()
        Date date = DateUtil.parseDate("24/02/2018", "dd/MM/yyyy");
        String dateFormatterDateShortStyleSessionAttribute = "dateformat.en_US.3.1";
        ContextHolder ctxHolder = context.getCtxHolder();
        Assertions.assertNull(ctxHolder.getSessionAttribute(dateFormatterDateShortStyleSessionAttribute));
        Assertions.assertEquals("2/24/2018", context.formatDateOnly(date));
        Assertions.assertNotNull(ctxHolder.getSessionAttribute(dateFormatterDateShortStyleSessionAttribute));

        // if dateformat for locale only uses 2 digits for year, code will make it 4 digits
        Properties properties = new Properties();
        properties.setProperty("dateformat" + "." + Locale.CANADA, "yy-MM-dd");
        context.getCtxHolder().setApplicationAttribute("properties", properties);
        context.setLocale(Locale.CANADA);
        Assertions.assertEquals("2018-02-24", context.formatDateOnly(date));
    }

    @Test
    public void formatNumber() {
        ContextHolder ctxHolder = context.getCtxHolder();
        context.setLocale(Locale.US);
        String numberFormatterTwoFractionalDigitsSessionAttribute = "numberformat.en_US.2";
        Assertions.assertNull(ctxHolder.getSessionAttribute(numberFormatterTwoFractionalDigitsSessionAttribute));
        Assertions.assertEquals("123.56", context.format(123.555, 2));
        Assertions.assertNotNull(ctxHolder.getSessionAttribute(numberFormatterTwoFractionalDigitsSessionAttribute));
    }

    @Test
    public void settingsCache() {
        Properties properties = new Properties();
        properties.setProperty("testSetting", "true");
        context.getCtxHolder().setApplicationAttribute("properties", properties);

        Setting oldSetting = context.getSetting("testSetting");
        System.out.println(oldSetting);
        Assertions.assertNotNull(oldSetting);

        properties.setProperty("testSetting", "false");
        Setting newSetting = context.getSetting("testSetting");
        Assertions.assertSame(oldSetting, newSetting); // the same object

        context.resetSettingCache();
        newSetting = context.getSetting("testSetting");

        // cannot use equals() because of EqualsHashUtil's implementation of hashCode() and equals()
        Assertions.assertNotSame(oldSetting, newSetting); // a new object
    }

    @Test
    public void getUnique() {
        Assertions.assertNotEquals(Context.getUnique(), Context.getUnique());
    }


}
