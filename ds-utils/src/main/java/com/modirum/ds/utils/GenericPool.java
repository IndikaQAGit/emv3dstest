/*
 * Copyright (C) 2017 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 21. jaan 2017
 *
 */
package com.modirum.ds.utils;

public class GenericPool<T> {

    public static final int DEFAULT_SIZE = 10;

    private final T[] pool;
    private int pointer;

    public GenericPool() {
        this(DEFAULT_SIZE);
    }

    public GenericPool(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("Size < 1");
        }
        pool = (T[]) new Object[size];
    }

    public final synchronized T reuse() {
        if (pointer > 0) {
            T t = pool[--pointer];
            pool[pointer] = null;
            return t;
        }
        return null;
    }

    public final synchronized void recycle(T pooled) {
        if (pointer < pool.length) {
            boolean alreadyPooled = false;
            for (int i = 0; i <= pointer; i++) {
                if (pool[i] == pooled) {
                    alreadyPooled = true;
                    break;
                }
            }
            if (!alreadyPooled && pointer < pool.length) {
                pool[pointer++] = pooled;
            }
        }
    }
}
