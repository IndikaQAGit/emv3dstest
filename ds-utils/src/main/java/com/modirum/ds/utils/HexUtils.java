/*
 * Copyright (C) 2010 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 30.11.2010
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

public class HexUtils {

    final static char[][] hextableUC = new char[256][2];
    final static char[][] hextableLC = new char[256][2];
    final static char[] hexcharsLC = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    final static char[] hexcharsUC = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    static {
        for (int i = 0; i < 256; i++) {
            hextableLC[i][0] = hexcharsLC[i / 16];
            hextableLC[i][1] = hexcharsLC[i % 16];

            hextableUC[i][0] = hexcharsUC[i / 16];
            hextableUC[i][1] = hexcharsUC[i % 16];
        }
    }

    private HexUtils() {
    }

    public static byte[] fromString(String inHex) {
        int len = inHex.length();
        int pos = 0;
        byte[] buffer = new byte[((len + 1) / 2)];
        if ((len % 2) == 1) {
            buffer[0] = (byte) asciiToHex(inHex.charAt(0));
            pos = 1;
            len--;
        }
        for (int ptr = pos; len > 0; len -= 2) {
            buffer[pos++] = (byte) ((asciiToHex(inHex.charAt(ptr++)) << 4) | (asciiToHex(inHex.charAt(ptr++))));
        }
        return buffer;
    }

    public static String toString(byte[] buffer) {
        char[] returnBuffer = new char[buffer.length * 2];
        for (int pos = 0; pos < buffer.length; pos++) {
            int px = buffer[pos] & 0xFF;
            returnBuffer[pos * 2] = hextableUC[px][0];
            returnBuffer[pos * 2 + 1] = hextableUC[px][1];
        }
        return new String(returnBuffer);
    }

    public static String toStringLC(byte[] buffer) {
        char[] returnBuffer = new char[buffer.length * 2];
        for (int pos = 0; pos < buffer.length; pos++) {
            int px = buffer[pos] & 0xFF;
            returnBuffer[pos * 2] = hextableLC[px][0];
            returnBuffer[pos * 2 + 1] = hextableLC[px][1];
        }
        return new String(returnBuffer);
    }

    public static int asciiToHex(char c) {
        if ((c >= 'a') && (c <= 'f')) {
            return (c - 'a' + 10);
        }
        if ((c >= 'A') && (c <= 'F')) {
            return (c - 'A' + 10);
        }
        if ((c >= '0') && (c <= '9')) {
            return (c - '0');
        }
        //throw new Error("ascii to hex failed");
        throw new IllegalArgumentException("ascii to hex failed " + c);
    }

    public static void main(String[] args) {
        byte[] block = new byte[]{0x00, 0, 0x01, 0x02, 0x03, 0x04, 0x05, 0x09, 0x0A, (byte) 0x9B, (byte) 0xAA, (byte) 0xCD, (byte) 0xDE, (byte) 0xF0, (byte) 0xFF};

        String rs1 = null;
        String rs2 = null;
        String rs3 = null;

        // warm up
        for (int i = 0; i < 1000000; i++) {
            rs1 = toString(block);
        }

        int todo = 5000000;
        long s1 = System.currentTimeMillis();
        for (int i = 0; i < todo; i++) {
            rs1 = toString(block);
        }

        long s2 = System.currentTimeMillis();
        for (int i = 0; i < todo; i++) {
            rs3 = toStringLC(block);
        }

        long s3 = System.currentTimeMillis();

        System.out.println("NewUC=" + rs1 + " in " + (s2 - s1) + " ms");
        System.out.println("NewLC=" + rs3 + " in " + (s3 - s2) + " ms");

    }

}