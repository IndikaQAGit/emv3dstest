/*
 * Copyright (C) 2011 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 24.01.2011
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.utils;

import java.io.Serializable;

import com.modirum.ds.utils.db.PersistableClass;

public class Setting extends PersistableClass implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    protected String key;
    protected Short processorId;
    protected String value;
    protected String comment;
    protected java.util.Date lastMod;
    protected String lastModBy;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Short getProcessorId() {
        return processorId;
    }

    public void setProcessorId(Short processorId) {
        this.processorId = processorId;
    }

    public java.util.Date getLastMod() {
        return lastMod;
    }

    public void setLastMod(java.util.Date lastMod) {
        this.lastMod = lastMod;
    }

    public String getLastModBy() {
        return lastModBy;
    }

    public void setLastModBy(String lastModBy) {
        this.lastModBy = lastModBy;
    }

    @Override
    public Serializable getId() {
        return new SettingPk(key, processorId);
    }

    public static class SettingPk implements Serializable {
        private static final long serialVersionUID = 1L;
        private String key;
        private Short processorId;

        public SettingPk() {
        }

        public SettingPk(String key, Short processorId) {
            this.key = key;
            this.processorId = processorId;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Short getProcessorId() {
            return processorId;
        }

        public void setProcessorId(Short processorId) {
            this.processorId = processorId;
        }
    }
}
