
/**
 * ISO4217 - Class for ISO 4217 Currency code conversions
 *
 * @author Jari Heikkinen
 * @version 1.0 13.01.2003
 */
package com.modirum.ds.utils;

import java.io.IOException;
import java.text.Collator;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Andri Kruus Date: 14.03.2016 Time: 14:06:59
 */
public class ISO4217 {

    private static Logger log = LoggerFactory.getLogger(ISO4217.class);

    public static final int ALPHA_3_CODE_INDEX = 0;
    public static final int EXPONENT_INDEX = 1;
    public static final int NAME_INDEX = 2;
    public static final int SYMB_INDEX = 3;
    public static final String ALPHA_CODES_DELIMITER = "\\|";
    public static final String ISO4217_FILE_PATH = "/ISO4217.xml";

    private static ISO4217[] iso4217WithoutEmptyValues = new ISO4217[0];
    private static ISO4217[] iso4217WithEmptyValues = new ISO4217[1000];


    private static Map<Short, java.util.List<ChangedExponent>> changedExponents = new HashMap<>();

    private static class ChangedExponent {
        private Date dateOfChange;
        private short newValue;

        ChangedExponent(Date changeDate, short newValue) {
            this.dateOfChange = changeDate;
            this.newValue = newValue;
        }
    }

    static {
        Properties properties = new Properties();
        TreeSet<ISO4217> iso4217s = new TreeSet<ISO4217>(new Comparator<ISO4217>() {
            private Collator collator = Collator.getInstance();

            @Override
            public int compare(ISO4217 o1, ISO4217 o2) {
                return collator.compare(o1.getName(), o2.getName());
            }
        });

        java.io.InputStream is = null;
        try {
            is = ISO4217.class.getResourceAsStream(ISO4217_FILE_PATH);
            properties.loadFromXML(is);
            for (Map.Entry<Object, Object> iso3166Entry : properties.entrySet()) {
                //<entry key="800:2017-10-15">UGX|0|Ugandan shilling</entry>
                String k = (String) iso3166Entry.getKey();
                int di = -1;
                if ((di = k.indexOf(":")) > -1) {
                    Short numericCode = Short.parseShort(k.substring(0, di));
                    Date changeDate = DateUtil.parseDate(k.substring(di + 1), "yyyy-MM-dd");
                    String[] alphaCodes = ((String) iso3166Entry.getValue()).split(ALPHA_CODES_DELIMITER);
                    String exponentStr = alphaCodes[EXPONENT_INDEX];
                    java.util.List<ChangedExponent> cl = changedExponents.get(numericCode);
                    if (cl == null) {
                        cl = new java.util.ArrayList<>();
                        changedExponents.put(numericCode, cl);
                    }
                    cl.add(new ChangedExponent(changeDate, (short) Misc.parseInt(exponentStr)));
                    continue;
                }

                Short numericCode = Short.parseShort(k);
                String[] alphaCodes = ((String) iso3166Entry.getValue()).split(ALPHA_CODES_DELIMITER);
                String name = alphaCodes[NAME_INDEX];
                String exponentStr = alphaCodes[EXPONENT_INDEX];
                String alpha3Code = alphaCodes[ALPHA_3_CODE_INDEX];
                String symb = alphaCodes.length > SYMB_INDEX ? alphaCodes[SYMB_INDEX] : alpha3Code;
                ISO4217 iso4217 = new ISO4217(name, alpha3Code, (short) Misc.parseInt(exponentStr), numericCode, symb);
                iso4217s.add(iso4217);
                iso4217WithEmptyValues[numericCode] = iso4217;
            }
        } catch (IOException e) {
            log.warn("Failed to load currency data " + e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                    is = null;
                } catch (Exception dc) {
                }
            }
        }
        iso4217WithoutEmptyValues = iso4217s.toArray(iso4217WithoutEmptyValues);
    }

    public final String name;
    public final String a3code;
    public final String symbol;
    public Short numeric;
    public Short exponent;

    protected ISO4217(String name, String a3code, Short exponenent, Short numCode, String symbol) {
        this.name = name;
        this.exponent = exponenent;
        this.numeric = numCode;
        this.a3code = a3code;
        this.symbol = symbol;
    }

    public static ISO4217 findFromNumeric(Short numericCode) {
        if (numericCode >= 0 && numericCode < iso4217WithEmptyValues.length) {
            return iso4217WithEmptyValues[numericCode];
        }

        return null;
    }

    public static ISO4217[] values() {
        return iso4217WithoutEmptyValues;
    }

    /**
     * Helper method that can contain custom logic for currency exponent resolving. For example if some currency changes exponent on certain
     * date and current date is later than that, then new exponent is returned.
     *
     * @return default currency exponent or new exponent if certain time has come
     */
    public short getCurrencyExponent() {
        return getCurrencyExponent(new Date());
    }

    /**
     * Helper method that can contain custom logic for currency exponent resolving. For example if some currency changes exponent on certain
     * date and current date is later than that, then new exponent is returned.
     *
     * @param whenDate date at which exponent should be read
     * @return default currency exponent or new exponent if certain time has come
     */
    public short getCurrencyExponent(Date whenDate) {
        short expx = exponent;
        java.util.List<ChangedExponent> cexl = changedExponents.get(numeric);
        if (cexl != null) {
            if (whenDate == null) {
                whenDate = new java.util.Date();
            }

            java.util.Date latest = null;
            for (ChangedExponent cex : cexl) {
                if ((latest == null || cex.dateOfChange.after(latest)) && !whenDate.before(cex.dateOfChange)) {
                    expx = cex.newValue;
                    latest = cex.dateOfChange;
                }
            }
        }

        return expx;
    }

    public String name() {
        return name;
    }


    public String getA3code() {
        return a3code;
    }

    public Short getNumeric() {
        return numeric;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

}