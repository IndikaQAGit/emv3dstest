package com.modirum.models;

public enum DeviceChannel {
    APP("APP", "01"),
    BRW("BRW", "02"),
    THREERI("3RI", "03"),
    NA("NA", "");

    private final String alias;
    private final String value;

    DeviceChannel(final String alias, final String value){
        this.alias = alias;
        this.value = value;
    }

    public final String alias(){
        return alias;
    }

    public final String value(){
        return value;
    }

    public static DeviceChannel getDeviceChannelFromAlias(String alias){
        if(alias.equals(APP.alias)) return APP;
        if(alias.equals(BRW.alias)) return BRW;
        if(alias.equals(THREERI.alias)) return THREERI;

        return BRW; // default
    }

    public static DeviceChannel getDeviceChannelFromValue(String value){
        if(value.equals(APP.value)) return APP;
        if(value.equals(BRW.value)) return BRW;
        if(value.equals(THREERI.value)) return THREERI;

        return BRW; // default
    }

    @Override
    public final String toString() {
        return alias;
    }
}
