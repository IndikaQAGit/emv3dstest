package com.modirum.models;

public enum MessageCategory {
    PA("PA", "01"),
    NPA("NPA", "02"),
    NA("NA", "");

    private final String alias;
    private final String value;

    MessageCategory(final String alias, final String value){
        this.alias = alias;
        this.value = value;
    }

    public final String alias(){
        return alias;
    }

    public final String value(){
        return value;
    }

    public static MessageCategory getMessageCategoryFromAlias(String alias){
        if(alias.equals(PA.alias)) return PA;
        if(alias.equals(NPA.alias)) return NPA;
        if(alias.equals(NA.alias)) return NA;

        return PA; // default
    }

    public static MessageCategory getMessageCategoryFromValue(String value){
        if(value.equals(PA.value)) return PA;
        if(value.equals(NPA.value)) return NPA;
        if(value.equals(NA.value)) return NA;

        return PA; // default
    }

    @Override
    public final String toString() {
        return alias;
    }
}
