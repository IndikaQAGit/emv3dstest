package com.modirum.testcases.transactionFlow;

import com.modirum.models.*;
import com.modirum.pages.dsemulator.areqrreq.AreqRreqEmulator;
import com.modirum.pages.dsemulator.areqrreq.AreqRreqEmulatorFactory;
import com.modirum.reports.ITestReporter;
import com.modirum.testcases.AbstractProtocolTest;
import com.modirum.testcases.testdata.TestCommon_DataProviders;
import com.modirum.utilities.JsonData;
import com.modirum.utilities.MessageBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class Transactions_FieldInclusionTests_Required extends AbstractProtocolTest {
    private static Logger LOGGER;
    private String m_testClassName;
    private DsEmulatorType emulatorType;

    MessageBuilder defaultAreq;
    MessageBuilder defaultAres;
    MessageBuilder defaultRreq;
    MessageBuilder defaultRres;

    private String emulatorUrl;

    @Parameters({"deviceChannel", "messageCategory", "targetDS", "testSet", "emulatorType"})
    @BeforeClass(alwaysRun = true)
    public void setUp(ITestContext context, String deviceChannel, String messageCategory, String targetDS, String testSet, String emulatorType){
        String testContextName = context.getName();
        String[] p = context.getAllTestMethods()[0].getInstance().getClass().getSimpleName().split("_");
        m_testClassName = testContextName + "_" + p[p.length-1];

        LOGGER = LogManager.getLogger(m_testClassName);

        switch(emulatorType){
            case "HTTPCLIENT":
                this.emulatorType = DsEmulatorType.HTTPCLIENT;
                break;
            case "WEBPAGE" :
            default:
                this.emulatorType = DsEmulatorType.WEBPAGE;
                break;
        }

        this.deviceChannel = DeviceChannel.getDeviceChannelFromValue(deviceChannel);
        this.messageCategory = MessageCategory.getMessageCategoryFromValue(messageCategory);
        context.setAttribute("targetDS", targetDS);
        context.setAttribute("testSet", testSet);

        initDefaultMessages();
        setTestContext(context);
        reporter = (ITestReporter) context.getAttribute("testReporter");

        // Get base URLs
        emulatorUrl = testEnv;
    }

    protected void initDefaultMessages(){
        defaultAreq = generateMessage("AReq");
        defaultAres = generateMessage("ARes");
        defaultRreq = generateMessage("RReq");
        defaultRres = generateMessage("RRes");
    }

    protected void setTestContext(ITestContext context){
        context.setAttribute("deviceChannel",this.deviceChannel);
        context.setAttribute("messageCategory", this.messageCategory);
        context.setAttribute("requiredAreqFields", defaultAreq.getRequiredFields());
        context.setAttribute("requiredAresFields", defaultAres.getRequiredFields());
        context.setAttribute("requiredRreqFields", defaultRreq.getRequiredFields());
        context.setAttribute("requiredRresFields", defaultRres.getRequiredFields());
    }

    /*****************************************
     * Test AREQ FIELD INCLUSIONS (REQUIRED)
     *****************************************/
    @Test(enabled = true,
            invocationCount = 1,
            description = "Tests Missing Required fields in AReq",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_AREQ_REQUIRED_FIELDS"
    )
    public void testAREQ_MissingRequiredField(String removeField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);
        areq.addOrUpdateField("threeDSServerTransID", areq.generateAutoValue("UUID"));

        // TEST DATA : remove field
        areq.removeField(removeField);
        String areqString = areq.toString();
        reporter.logInfo("areq: " + areqString);
        areqEmulator.set3dssAreq(areqString);

        areqEmulator.generateAcsAresFrictionless();
        areqEmulator.cacheAcsAres();
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        } else if(expectedError == ErrorCode.ACCESS_DENIED_INVALID_ENDPOINT){
            assertThatResponseIsErro303(responseString);
        }  else {
            assertThatResponseIsErro201(responseString, removeField);
        }
    }

    @Test(enabled = true,
            invocationCount = 1,
            description = "Tests Empty Required fields in AReq",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_AREQ_REQUIRED_FIELDS"
    )
    public void testAREQ_EmptyRequiredField(String removeField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);
        areq.addOrUpdateField("threeDSServerTransID", areq.generateAutoValue("UUID"));

        // TEST DATA : remove field
        areq.addOrUpdateField(removeField, "");
        String areqString = areq.toString();
        reporter.logInfo("areq: " + areqString);
        areqEmulator.set3dssAreq(areqString);

        areqEmulator.generateAcsAresFrictionless();
        areqEmulator.cacheAcsAres();
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101(responseString);
        } else if(expectedError == ErrorCode.ACCESS_DENIED_INVALID_ENDPOINT){
            assertThatResponseIsErro303(responseString);
        }  else {
            assertThatResponseIsErro201(responseString, removeField);
        }
    }

    /*****************************************
     * Test ARES FIELD INCLUSIONS (REQUIRED)
     *****************************************/
    @Test(enabled = true,
            invocationCount = 1,
            description = "Tests Missing Required fields in ARes",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_ARES_REQUIRED_FIELDS"
    )
    public void testARES_MissingRequiredField(String removeField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);
        areq.addOrUpdateField("threeDSServerTransID", areq.generateAutoValue("UUID"));
        reporter.logInfo("areq: " + areq.toString());
        areqEmulator.set3dssAreq(areq.toString());

        MessageBuilder ares = new MessageBuilder(defaultAres);
        ares.addOrUpdateField("threeDSServerTransID", areq.getField("threeDSServerTransID"));

        // TEST DATA : remove field
        ares.removeField(removeField);

        reporter.logInfo("ares: " + ares.toString());
        areqEmulator.setAcsAres(ares.toString());
        areqEmulator.cacheAcsAres();
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        }  else {
            assertThatResponseIsErro201(responseString, removeField);
        }
    }

    @Test(enabled = true,
            invocationCount = 1,
            description = "Tests Empty Required fields in ARes",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_ARES_REQUIRED_FIELDS"
    )
    public void testARES_EmptyRequiredField(String removeField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        MessageBuilder areq = new MessageBuilder(defaultAreq);
        areq.addOrUpdateField("threeDSServerTransID", areq.generateAutoValue("UUID"));
        reporter.logInfo("areq: " + areq.toString());
        areqEmulator.set3dssAreq(areq.toString());

        MessageBuilder ares = new MessageBuilder(defaultAres);
        ares.addOrUpdateField("threeDSServerTransID", areq.getField("threeDSServerTransID"));

        // TEST DATA : remove field
        ares.removeField(removeField);

        reporter.logInfo("ares: " + ares.toString());
        areqEmulator.setAcsAres(ares.toString());
        areqEmulator.cacheAcsAres();
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("responseString: " + responseString);

        // ASSERTIONS - check all Erro fields
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        } else if(expectedError == ErrorCode.ACCESS_DENIED_INVALID_ENDPOINT){
            assertThatResponseIsErro303(responseString);
        }  else {
            assertThatResponseIsErro201(responseString, removeField);
        }
    }

    /*****************************************
     * Test RREQ FIELD INCLUSIONS (REQUIRED)
     *****************************************/
    @Test(enabled = true,
            description = "Tests Missing Required fields in RReq",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_RREQ_REQUIRED_FIELDS"
    )
    public void testRREQ_MissingRequiredField(String removeField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        // send areq
        if(this.deviceChannel == DeviceChannel.BRW && this.messageCategory == MessageCategory.PA) {
            areqEmulator.generate3dssAreq(this.messageVersion, this.messageCategory, this.deviceChannel);
            reporter.logInfo("areq: auto-generated");
        } else {
            MessageBuilder areq = new MessageBuilder(defaultAreq);
            String areqString = areq.toString();
            reporter.logInfo("areq: " + areqString);
            areqEmulator.set3dssAreq(areqString);
        }
        areqEmulator.generateAcsAres(TransactionFlow.CHALLENGE);
        areqEmulator.cacheAcsAres();
        reporter.logInfo("ACS ares: auto-generated");
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("DS ARES responseString: " + responseString);

        // ASSERTIONS
        assertThatResponseIsARes(responseString, "C", ""); // transStatus should be "C"

        // if OK, cache rres
        areqEmulator.generate3dssRres();
        areqEmulator.cache3dssRres();
        reporter.logInfo("3DSS rres: auto-generated");

        // send rreq
        areqEmulator.generateAcsRreq();
        MessageBuilder rreq = new MessageBuilder();
        rreq.build(areqEmulator.getAcsRreqJson());

        //---> TEST DATA : remove required RReq field
        rreq.removeField(removeField);
        String rreqString = rreq.toString();
        reporter.logInfo("rreq: " + rreqString);
        areqEmulator.setAcsRreqJson(rreqString);
        areqEmulator.sendAcsRreq();

        responseString = areqEmulator.waitForDsRres();
        LOGGER.debug("DS RRES/ERRO: " + responseString);
        reporter.logInfo("RRES/ERRO responseString: " + responseString);

        // check erro
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        }  else {
            assertThatResponseIsErro201(responseString, removeField);
        }
    }

    @Test(enabled = true,
            description = "Tests Empty Required fields in RReq",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_RREQ_REQUIRED_FIELDS"
    )
    public void testRREQ_EmptyRequiredField(String emptyField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        // send areq
        if(this.deviceChannel == DeviceChannel.BRW && this.messageCategory == MessageCategory.PA) {
            areqEmulator.generate3dssAreq(this.messageVersion, this.messageCategory, this.deviceChannel);
            reporter.logInfo("areq: auto-generated");
        } else {
            MessageBuilder areq = new MessageBuilder(defaultAreq);
            String areqString = areq.toString();
            reporter.logInfo("areq: " + areqString);
            areqEmulator.set3dssAreq(areqString);
        }
        areqEmulator.generateAcsAres(TransactionFlow.CHALLENGE);
        areqEmulator.cacheAcsAres();
        reporter.logInfo("ACS ares: auto-generated");
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("responseString: " + responseString);
        reporter.logInfo("DS ARES responseString: " + responseString);

        // ASSERTIONS
        assertThatResponseIsARes(responseString, "C", ""); // transStatus should be "C"

        // if OK, cache rres
        areqEmulator.generate3dssRres();
        areqEmulator.cache3dssRres();
        reporter.logInfo("3DSS rres: auto-generated");

        // send rreq
        areqEmulator.generateAcsRreq();
        MessageBuilder rreq = new MessageBuilder();
        rreq.build(areqEmulator.getAcsRreqJson());

        //---> TEST DATA : remove required RReq field
        rreq.addOrUpdateField(emptyField, "");
        String rreqString = rreq.toString();
        reporter.logInfo("rreq: " + rreqString);
        areqEmulator.setAcsRreqJson(rreqString);
        areqEmulator.sendAcsRreq();

        responseString = areqEmulator.waitForDsRres();
        LOGGER.debug("DS RRES/ERRO: " + responseString);
        reporter.logInfo("RRES/ERRO responseString: " + responseString);

        // check erro
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        }  else {
            assertThatResponseIsErro201(responseString, emptyField);
        }
    }

    /*****************************************
     * Test RRES FIELD INCLUSIONS (REQUIRED)
     *****************************************/
    @Test(enabled = true,
            description = "Tests Missing Required fields in RRes",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_RRES_REQUIRED_FIELDS"
    )
    public void testRRES_MissingRequiredField(String removeField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        // send areq
        if(this.deviceChannel == DeviceChannel.BRW && this.messageCategory == MessageCategory.PA) {
            areqEmulator.generate3dssAreq(this.messageVersion, this.messageCategory, this.deviceChannel);
            reporter.logInfo("areq: auto-generated");
        }else{
            MessageBuilder areq = new MessageBuilder(defaultAreq);
            String areqString = areq.toString();
            reporter.logInfo("areq: " + areqString);
            areqEmulator.set3dssAreq(areqString);
        }
        areqEmulator.generateAcsAres(TransactionFlow.CHALLENGE);
        areqEmulator.cacheAcsAres();
        reporter.logInfo("ACS ares: auto-generated");
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("DS ARES responseString: " + responseString);
        reporter.logInfo("DS ARES responseString: " + responseString);

        // ASSERTIONS
        assertThatResponseIsARes(responseString, "C", "");// transStatus should be "C"

        // if OK, cache rres
        areqEmulator.generate3dssRres();
        MessageBuilder rres = new MessageBuilder();
        rres.build(areqEmulator.get3dssRresJson());

        //---> TEST DATA : remove required RRes field
        rres.removeField(removeField);
        String rresString = rres.toString();
        reporter.logInfo("3DSS rres: " + rresString);
        areqEmulator.setRresJson(rres.toString());

        areqEmulator.cache3dssRres();

        // send rreq
        areqEmulator.generateAcsRreq();
        areqEmulator.sendAcsRreq();
        LOGGER.debug("ACS rreq: auto-generated");
        reporter.logInfo("ACS rreq: auto-generated");

        responseString = areqEmulator.waitForDsRres();
        LOGGER.debug("DS RRES/ERRO responseString: " + responseString);
        reporter.logInfo("DS RRES/ERRO  responseString: " + responseString);

        // check erro
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        } else {
            assertThatResponseIsErro201(responseString, removeField);
        }
    }

    @Test(enabled = true,
            description = "Tests Empty Required fields in RRes",
            dataProviderClass = TestCommon_DataProviders.class,
            dataProvider = "DP_RRES_REQUIRED_FIELDS"
    )
    public void testRRES_EmptyRequiredField(String emptyField, ErrorCode expectedError) {
        AreqRreqEmulator areqEmulator = AreqRreqEmulatorFactory.getAreqRreqEmulator(emulatorType, selDriver, emulatorUrl);

        // send areq
        if(this.deviceChannel == DeviceChannel.BRW && this.messageCategory == MessageCategory.PA) {
            areqEmulator.generate3dssAreq(this.messageVersion, this.messageCategory, this.deviceChannel);
            reporter.logInfo("areq: auto-generated");
        }else{
            MessageBuilder areq = new MessageBuilder(defaultAreq);
            String areqString = areq.toString();
            reporter.logInfo("areq: " + areqString);
            areqEmulator.set3dssAreq(areqString);
        }
        areqEmulator.generateAcsAres(TransactionFlow.CHALLENGE);
        areqEmulator.cacheAcsAres();
        reporter.logInfo("ACS ares: auto-generated");
        areqEmulator.send3dssAreq();

        String responseString = areqEmulator.waitForDsAres();
        LOGGER.debug("DS ARES responseString: " + responseString);
        reporter.logInfo("DS ARES responseString: " + responseString);

        // ASSERTIONS
        assertThatResponseIsARes(responseString, "C", "");// transStatus should be "C"

        // if OK, cache rres
        areqEmulator.generate3dssRres();
        MessageBuilder rres = new MessageBuilder();
        rres.build(areqEmulator.get3dssRresJson());

        //---> TEST DATA : remove required RRes field
        rres.addOrUpdateField(emptyField, "");
        String rresString = rres.toString();
        reporter.logInfo("3DSS rres: " + rresString);
        areqEmulator.setRresJson(rres.toString());

        areqEmulator.cache3dssRres();

        // send rreq
        areqEmulator.generateAcsRreq();
        areqEmulator.sendAcsRreq();
        LOGGER.debug("ACS rreq: auto-generated");
        reporter.logInfo("ACS rreq: auto-generated");

        responseString = areqEmulator.waitForDsRres();
        LOGGER.debug("DS RRES/ERRO responseString: " + responseString);
        reporter.logInfo("DS RRES/ERRO  responseString: " + responseString);

        // check erro
        if(expectedError == ErrorCode.INVALID_MESSAGE){
            assertThatResponseIsErro101Or201(responseString);
        } else {
            assertThatResponseIsErro201(responseString, emptyField);
        }
    }
}
