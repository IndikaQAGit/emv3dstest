package com.modirum.testcases.testdata;

import com.modirum.models.DataType;
import com.modirum.models.ErrorCode;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestFormats_DataProviders {
    /* **************************************************
     * TRANSACTION FLOWS
     ************************************************** */

    @DataProvider(name = "DP_AREQ_CONDITIONAL_FIELDS_threeDSReqAuthMethodInd")
    private Iterator<Object[]> AReq_threeDSReqAuthMethodInd(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("conditionalAreqFields");
        String ds = (String)context.getAttribute("targetDS");
        List<Object[]> data = new ArrayList<>();
        if(fields.contains("threeDSReqAuthMethodInd")) {
            Object[] tests = FieldFormatsTestData.getFormats(ds, "threeDSReqAuthMethodInd");
            if(tests[1] == DataType.STRING){
                for(String s : (String[])tests[2]){
                    data.add(new Object[] {s, null});
                }
                for(String s : (String[])tests[3]){
                    data.add(new Object[]{s, ErrorCode.INVALID_FORMAT});
                }
            }
        }
        return data.iterator();
    }

    //@DataProvider(name = "DP_AREQ_CONDITIONAL_FIELDS_threeDSReqAuthMethodInd")
    private Iterator<Object[]> AReq_Conditional_threeDSReqAuthMethodInd(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("conditionalAreqFields");
        String ds = (String)context.getAttribute("targetDS");

        List<Object[]> data = new ArrayList<>();
        if(fields.contains("threeDSReqAuthMethodInd")){
            switch(ds.toLowerCase()){
                default:
                case "modirum":
                    // EMVco spec: DS-specific rules
                    data.add(new Object[]{ "01", null });
                    data.add(new Object[]{ "02", null });
                    data.add(new Object[]{ "03", null });
                    break;
                case "eftpos":
                    // eftpos spec: Reserved for future use
                    data.add(new Object[]{ "01", ErrorCode.INVALID_FORMAT });
                    data.add(new Object[]{ "02", ErrorCode.INVALID_FORMAT });
                    data.add(new Object[]{ "03", ErrorCode.INVALID_FORMAT });
                    break;
            }

            // COMMON FORMAT CHECKS
            // 04–79 = Reserved for EMVCo future use (values invalid until defined by EMVCo)
            data.add(new Object[]{ "04", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "79", ErrorCode.INVALID_FORMAT});
            // 80–99 = Reserved for DS use
            data.add(new Object[]{ "80", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "99", ErrorCode.INVALID_FORMAT});
            // length checks
            data.add(new Object[]{ "1", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "100", ErrorCode.INVALID_FORMAT});
        }

        return data.iterator();
    }

    // acctType
    @DataProvider(name = "DP_AREQ_CONDITIONAL_FIELDS_acctType")
    private Iterator<Object[]> AReq_Conditional_acctType(ITestContext context){
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>)context.getAttribute("conditionalAreqFields");
        String ds = (String)context.getAttribute("targetDS");

        List<Object[]> data = new ArrayList<>();
        if(fields.contains("acctType")){
            switch(ds.toLowerCase()){
                default:
                case "modirum":
                    // EMVco spec: DS-specific rules
                    data.add(new Object[]{ "01", null });
                    data.add(new Object[]{ "02", null });
                    data.add(new Object[]{ "03", null });
                    break;
                case "eftpos":
                    // eftpos spec: Reserved for future use
                    data.add(new Object[]{ "01", ErrorCode.INVALID_FORMAT });
                    data.add(new Object[]{ "02", ErrorCode.INVALID_FORMAT });
                    data.add(new Object[]{ "03", ErrorCode.INVALID_FORMAT });
                    break;
            }

            // COMMON FORMAT CHECKS
            // 04–79 = Reserved for EMVCo future use (values invalid until defined by EMVCo)
            data.add(new Object[]{ "04", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "79", ErrorCode.INVALID_FORMAT});
            // 80–99 = Reserved for DS use
            data.add(new Object[]{ "80", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "99", ErrorCode.INVALID_FORMAT});
            // length checks
            data.add(new Object[]{ "1", ErrorCode.INVALID_FORMAT});
            data.add(new Object[]{ "100", ErrorCode.INVALID_FORMAT});
        }

        return data.iterator();
    }

    /* **************************************************
     * PREPARATION FLOW (PREQ)
     ************************************************** */
}
