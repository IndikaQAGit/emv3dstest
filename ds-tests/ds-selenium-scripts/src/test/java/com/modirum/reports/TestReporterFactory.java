package com.modirum.reports;

public class TestReporterFactory {
    public static ITestReporter getTestReporter(TestReportType type, String fullPath, Boolean bOverwrite){
        switch(type) {
            case DEFAULT:
            default:
                return new ExtentTestReporter(fullPath, bOverwrite);
            case HTML:
                return new AltExtentTestReporter(fullPath);
            case CONSOLE:
                return new ConsoleTestReporter();
        }
    }
}
