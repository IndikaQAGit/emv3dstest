package com.modirum.utilities;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpClient {
    private static final Logger LOGGER = LogManager.getLogger(com.modirum.utilities.HttpClient.class.getName());
    public static Map<String, Object> send(Map<String, Object> headerMap, String method, String urlString, String payload){
        Map<String, Object> responseObj = new HashMap<>();

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(method);
            urlConnection.setDoOutput(true);

            for (String key : headerMap.keySet()) {
                urlConnection.setRequestProperty(key, (String) headerMap.get(key));
            }

            try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                wr.writeBytes(payload);
            }

            int responseCode = urlConnection.getResponseCode();
            responseObj.put("responseCode", responseCode);

            StringBuilder responsePayload = new StringBuilder();
            if (responseCode == 200) {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))) {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        responsePayload.append(inputLine);
                    }
                }
                responseObj.put("response", responsePayload.toString());
            } else {
                // log error
                LOGGER.error("[ERROR] Response: " + responseCode);
            }
        } catch (Exception e) {
            LOGGER.error("[ERROR] HTTP Client caught Exception " + e);
        }
        return responseObj;
    }

    public static String formatPayload(List<NameValuePair> paramsList){
        return URLEncodedUtils.format(paramsList, "UTF-8");
    }

    public static List<NameValuePair> parsePayload(String payload){
        return URLEncodedUtils.parse(payload, StandardCharsets.UTF_8);
    }

    public static NameValuePair createParam(String name, String value){
        return new BasicNameValuePair(name, value);
    }
}
