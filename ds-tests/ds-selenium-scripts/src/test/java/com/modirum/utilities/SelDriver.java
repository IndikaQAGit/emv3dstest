package com.modirum.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SelDriver {
    private static final Logger LOGGER = LogManager.getLogger(SelDriver.class.getName());

    private WebDriver driver = null;    // cannot be static, each instance must be its own
    private static String browser = null;
    private static boolean isPageElemsTestedCritically = false;
    private long defaultWaitTime = 10L;


    public SelDriver(){
        Properties config = new Properties();
        FileInputStream fis = null;

        try{
            // Read Config.properties
            fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator+ "test" + File.separator +
                    "resources" + File.separator + "properties" + File.separator + "Config.properties");
            config.load(fis);

            this.browser = config.getProperty("browser");
            this.isPageElemsTestedCritically = Boolean.parseBoolean(config.getProperty("isPageElemsTestedCritically")); // Flag for testing Page Elements (TODO: is this needed for DS tests?)
            this.defaultWaitTime = Long.parseLong(config.getProperty("implicit.wait"));

            String node = String.format("http://%s:%s/wd/hub", config.get("ip.address"), config.get("ip.port"));


            if( !this.browser.equalsIgnoreCase("firefox") /* && !this.browser.equalsIgnoreCase("chrome") TODO: add actual support for chrome? */ )
                throw new IllegalArgumentException("The Browser type is not supported!");

            /* Support for geckodriver (selenium/firefox upgrade) */
            String geckoDriverPath = System.getProperty("user.dir") +
                    (FileUtil.isWindowsOS() ? config.getProperty("firefoxWebdriverWin") : config.getProperty("firefoxWebdriverMac"));
            System.setProperty("webdriver.gecko.driver", geckoDriverPath);

            LOGGER.info("\n********************");
            LOGGER.info("Executing on FireFox");
            LOGGER.info(" - node: " + node);
            LOGGER.info(" - geckodriver path: " + geckoDriverPath);
            LOGGER.info("********************");
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setHeadless(true); // TODO: Set Headless Mode false if debugging

            DesiredCapabilities cap = new DesiredCapabilities(browser, "", Platform.ANY);
            cap.setAcceptInsecureCerts(true);
            firefoxOptions.merge(cap);
            driver = new RemoteWebDriver(new URL(node), firefoxOptions.toCapabilities());

            // SETTINGS: Window size 1024x600 800x600
            int desiredWidth = 1024;
            int desiredHeight = 600;


            // SETTINGS: Positioning browser
            driver.manage().window().maximize();
            Dimension windowSize = driver.manage().window().getSize();
            // driver.manage().window().setPosition(new Point(windowSize.width-desiredWidth-20, windowSize.height-desiredHeight-20)); // Right of screen
            driver.manage().window().setPosition(new Point(0, windowSize.height-desiredHeight-20)); // Left of screen
            driver.manage().window().setSize(new Dimension(desiredWidth,desiredHeight));


            // SETTINGS: 10 sec wait before Exception
            driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);

        }catch(Exception e){
            LOGGER.error("Something went wrong... - " + e.getMessage());
        }finally{
            try {
                // make sure filestream is closed
                if (fis != null) {
                    fis.close();
                }
            }catch(IOException f){
                // do nothing
            }
        }
    }

    /**
     * Get the native webdriver instance
     * This method is to get the native WebDriver instance
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Open the given URL
     * This method is to open the given URL in the browser
     * Pass the URL path as Arguments to this method
     *
     * @param url
     */
    public void openUrl(String url) {
        driver.get(url);
    }

    /**
     * Close the browser
     * This method is to close the browser
     */
    public void closeBrowser() {
        if(driver != null) {
            driver.get(driver.getCurrentUrl());
            driver.close();
        }
    }

    public void waitUntilElementVisible(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 5L);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public WebElement findElement(By by, long waitTimeInSeconds) throws NoSuchElementException {
        driver.manage().timeouts().implicitlyWait(waitTimeInSeconds, TimeUnit.SECONDS);
        try{
            return driver.findElement(by);
        } finally {
            driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        }
    }

    public WebElement findElement(By by) throws NoSuchElementException {
        return findElement(by, 1);
    }

    public List<WebElement> findElements(By by) throws NoSuchElementException {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        try{
            return driver.findElements(by);
        } finally {
            driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        }
    }

    /***************************************************
     * Getters
     ***************************************************/
    public String getValue(By by){
        return getValue(findElement(by));
    }

    public String getValue(WebElement elem){
        try {
            return getValue(elem, 1);
        } catch (NoSuchElementException | NullPointerException ex) {
            return "";
        }
    }

    public String getValue(WebElement elem, int waitTimeInSeconds){
        try {
            driver.manage().timeouts().implicitlyWait(waitTimeInSeconds, TimeUnit.SECONDS);
            String value = trimAll(elem.getText());
            if ( StringUtils.isNotEmpty(value) )
                return value;

            value = trimAll(elem.getAttribute("innerText"));
            if ( StringUtils.isNotEmpty(value) )
                return value;

            value = trimAll(elem.getAttribute("value"));
            if ( StringUtils.isNotEmpty(value) )
                return value;
        } catch(NoSuchElementException ex){
        } finally {
            driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        }
        return null;
    }


    /***************************************************
     * Setters / Clickers
     ***************************************************/
    public void setInputField(WebElement el, String value){
//        if ( notNullNorEmpty(value) ) { if editing a previously populated field to blank, this wont work
        waitUntilElementVisible(el);
        if ( value!=null ) {
            el.clear();
            el.sendKeys(value);
        }
    }

    public void click(By by){
        click(findElement(by), true, defaultWaitTime);
    }
    public void click(By by, long waitTimeInSeconds){
        click(by, true, waitTimeInSeconds);
    }
    public void click(By by, boolean isClickOnce, long waitTimeInSeconds){
        click(findElement(by, waitTimeInSeconds), isClickOnce, waitTimeInSeconds);
    }
    public void click(WebElement el){
        click(el, true, defaultWaitTime);
    }
    public void click(WebElement el, long waitTimeInSeconds){
        click(el, true, waitTimeInSeconds);
    }
    public void click(WebElement el, boolean isClickOnce){
        click(el, isClickOnce, defaultWaitTime);
    }
    public void click(WebElement el, boolean isClickOnce, long waitTimeInSeconds){
        driver.manage().timeouts().implicitlyWait(waitTimeInSeconds, TimeUnit.SECONDS);
        try {
            //waitUntilElementClickable(el, waitTimeInSeconds);
            el.click(); // does not execute the click when only one call is made
            if (!isClickOnce) {
                try {
                    el.click(); // enclosed in try-catch just in case first click works
                } catch (Exception e) {
                }
            }
        } finally {
            driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        }
    }

    /***************************************************
     * Common Methods -- TODO: following methods are not really Webdriver-related. Check if these should be moved to another class
     ***************************************************/
    public static Object[][] append(Object[][] a, Object[][] b) {
        Object[][] result = new Object[a.length + b.length][];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public static String trimAll(String value){
        if(StringUtils.isNotEmpty(value)) {
            value = value.trim().replaceAll("\u00A0", "");
        }
        return value;
    }
}