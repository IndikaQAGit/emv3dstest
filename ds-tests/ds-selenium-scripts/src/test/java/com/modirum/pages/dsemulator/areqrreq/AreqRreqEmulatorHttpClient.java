package com.modirum.pages.dsemulator.areqrreq;

import com.modirum.models.DeviceChannel;
import com.modirum.models.MessageCategory;
import com.modirum.models.TransactionFlow;
import com.modirum.utilities.MessageBuilder;
import com.modirum.utilities.FileUtil;
import com.modirum.utilities.HttpClient;
import com.modirum.utilities.JsonData;
import com.modirum.utilities.SelDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AreqRreqEmulatorHttpClient implements AreqRreqEmulator{
    private static final Logger LOGGER = LogManager.getLogger(AreqRreqEmulatorHttpClient.class.getName());

    private String baseURL;
    private static final String DSERVER_URL = "/ds/DServer";
    private static final String CACHE_ARES_URL = "/ds-3ds-emulator/cacheAcsAres.jsp?threeDSServerTransID=";
    private static final String CACHE_RRES_URL = "/ds-3ds-emulator/cache3dsServerRres.jsp?acsTransId=";

    private String dsEntrypointUrl;

    private String threeDSServerTransID;
    private String acsTransID;
    private String dsTransID;
    private String dsReferenceNumber;

    // 3DS MESSAGES
    private String threedssAreq;
    private String dsAres;
    private String acsAres;
    private String acsRreq;
    private String threedssRres;
    private String dsRres;

    // TRANSACTION FLOW FIELDS
    private String messageVersion;
    private MessageCategory messageCategory;
    private DeviceChannel deviceChannel;

    public AreqRreqEmulatorHttpClient(SelDriver selDriver /* unused */, String baseURL){
        this.baseURL = baseURL;
        this.dsEntrypointUrl = baseURL + DSERVER_URL;
        threeDSServerTransID = "";
        acsTransID = "";
        threedssAreq = "";
        acsAres = "";
        dsAres = "";
        acsRreq = "";
        threedssRres = "";
        dsRres = "";

        dsTransID = "";
        dsReferenceNumber = "";

        messageVersion = "2.2.0";
        messageCategory = MessageCategory.PA;
        deviceChannel = DeviceChannel.BRW;
    }

    /***************************************************
     * Getters
     ***************************************************/
    public String getDsEntrypointUrl(){ return dsEntrypointUrl; }
    public String get3dssAreq(){ return threedssAreq; }
    public String getAcsAres(){ return acsAres; }
    public String getDsAres(){ return dsAres; }
    public String getAcsTransIdForRres(){ return acsTransID; }
    public String get3dssRresJson(){ return threedssRres; }
    public String getAcsRreqJson(){ return acsRreq; }
    public String getDsRres(){ return dsRres; }

    /***************************************************
     * Setters / Clickers
     ***************************************************/

    public AreqRreqEmulatorHttpClient setDsEntrypointUrl(String dsEntrypointUrl){
        this.dsEntrypointUrl = dsEntrypointUrl;
        return this;
    }
    public AreqRreqEmulatorHttpClient set3dssAreq(String areq){
        this.threedssAreq = areq;
        MessageBuilder areqBuilder = new MessageBuilder();
        areqBuilder.build(areq);

        if(areqBuilder.getField("messageVersion") != null && areqBuilder.getField("messageVersion") != "") {
            messageVersion = areqBuilder.getField("messageVersion").toString();
        }
        if(areqBuilder.getField("deviceChannel") != null) {
            deviceChannel = DeviceChannel.getDeviceChannelFromValue(areqBuilder.getField("deviceChannel").toString());
        }
        if(areqBuilder.getField("messageCategory") != null) {
            messageCategory = MessageCategory.getMessageCategoryFromValue(areqBuilder.getField("messageCategory").toString());
        }
        if(areqBuilder.getField("threeDSServerTransID") != null) {
            threeDSServerTransID = areqBuilder.getField("threeDSServerTransID").toString();
        }
        return this;
    }
    public void send3dssAreq(){
        dsAres = sendMessage(dsEntrypointUrl, threedssAreq);
        if(dsAres != null){
            MessageBuilder aresBuilder = new MessageBuilder();
            aresBuilder.build(dsAres);

            if(aresBuilder.getField("messageType").equals("ARes")) {
                dsReferenceNumber = aresBuilder.getField("dsReferenceNumber").toString();
                dsTransID = aresBuilder.getField("dsTransID").toString();
            }
        }
    }
    public AreqRreqEmulatorHttpClient setAcsAres(String acsAres){ this.acsAres = acsAres; return this; }
    public AreqRreqEmulatorHttpClient setDsAres(String dsAres){ this.dsAres = dsAres; return this; }
    public void generateAreqPaBrw210(){
        messageVersion = "2.1.0";
        deviceChannel = DeviceChannel.BRW;
        messageCategory = MessageCategory.PA;

        Map<String, Object> areq =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "3dss-areq.json"));

        JsonData.addOrUpdateField(areq, "threeDSServerTransID", UUID.randomUUID().toString());
        threeDSServerTransID = (String)areq.get("threeDSServerTransID");
        threedssAreq = JsonData.getAsString(areq);
    }

    public void generateAreqPaBrw220(){
        messageVersion = "2.2.0";
        deviceChannel = DeviceChannel.BRW;
        messageCategory = MessageCategory.PA;

        Map<String, Object> areq =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "3dss-areq.json"));

        JsonData.addOrUpdateField(areq, "threeDSServerTransID", UUID.randomUUID().toString());
        threeDSServerTransID = (String)areq.get("threeDSServerTransID");
        threedssAreq = JsonData.getAsString(areq);
    }
    public void generateAcsAresFrictionless(){
        Map<String, Object> ares =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "acs-ares-frictionless.json"));

        JsonData.addOrUpdateField(ares, "acsTransID", UUID.randomUUID().toString());
        JsonData.addOrUpdateField(ares, "threeDSServerTransID", threeDSServerTransID);

        acsTransID = (String)ares.get("acsTransID");
        acsAres = JsonData.getAsString(ares);
    }
    public void generateAcsAresChallenge(){
        Map<String, Object> ares =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "acs-ares-challenge.json"));

        JsonData.addOrUpdateField(ares, "acsTransID", UUID.randomUUID().toString());
        JsonData.addOrUpdateField(ares, "threeDSServerTransID", threeDSServerTransID);
        JsonData.addOrUpdateField(ares, "transStatus", "C");

        acsTransID = (String)ares.get("acsTransID");
        acsAres = JsonData.getAsString(ares);
    }
    public void cacheAcsAres(){
        cacheMessage(baseURL + CACHE_ARES_URL + threeDSServerTransID, acsAres);
    }
    public AreqRreqEmulatorHttpClient setAcsTransIdForRres(String acsTransId){ this.acsTransID = acsTransId; return this; }
    public AreqRreqEmulatorHttpClient setRresJson(String rresJson){
        this.threedssRres = rresJson;
        return this;
    }
    public void generate3dssRres(){
        Map<String, Object> rres =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "3dss-rres.json"));

        JsonData.addOrUpdateField(rres, "acsTransID", acsTransID);
        JsonData.addOrUpdateField(rres, "threeDSServerTransID", threeDSServerTransID);
        JsonData.addOrUpdateField(rres, "dsTransID", dsTransID);

        threedssRres = JsonData.getAsString(rres);
    }
    public void cache3dssRres(){
        cacheMessage(baseURL + CACHE_RRES_URL + acsTransID, threedssRres);
    }
    public void generateAcsRreq(){
        Map<String, Object> rreq =
                JsonData.readFromFile(FileUtil.getTestResourceFilePath("dsemulator" + File.separator + messageVersion
                                + File.separator,
                        "acs-rreq.json"));

        JsonData.addOrUpdateField(rreq, "acsTransID", acsTransID);
        JsonData.addOrUpdateField(rreq, "threeDSServerTransID", threeDSServerTransID);
        JsonData.addOrUpdateField(rreq, "dsTransID", dsTransID);
        JsonData.addOrUpdateField(rreq, "messageCategory", messageCategory.value());

        acsRreq = JsonData.getAsString(rreq);
    }
    public AreqRreqEmulatorHttpClient setAcsRreqJson(String rreqJson){ this.acsRreq = rreqJson; return this; }
    public void sendAcsRreq(){
        dsRres = sendMessage(dsEntrypointUrl, acsRreq);
    }


    /***************************************************
     * Common Methods
     ***************************************************/
    private boolean cacheMessage(String url, String payload){
        Map<String, Object> httpRespObj;

        // Create request header
        Map<String, Object> header = new HashMap<>();
        header.put("Content-Type", "text/plain");
        header.put("Connection", "keep-alive");
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        header.put("Accept-Encoding", "gzip, deflate, br");
        header.put("Accept-Language", "en-US,en;q=0.5");

        httpRespObj = HttpClient.send(header, "POST", url, payload);
        if(Integer.parseInt(httpRespObj.get("responseCode").toString()) == 200) {
            LOGGER.info(httpRespObj.toString());
            return true;
        } else {
            LOGGER.error("RESPONSE CODE: " + httpRespObj.get("responseCode").toString());
            LOGGER.error(httpRespObj.toString());
            return false;
        }
    }

    private String sendMessage(String url, String payload){
        Map<String, Object> httpRespObj;

        // Create request header
        Map<String, Object> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Connection", "keep-alive");
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        header.put("Accept-Encoding", "gzip, deflate, br");
        header.put("Accept-Language", "en-US,en;q=0.5");

        httpRespObj = HttpClient.send(header, "POST", url, payload);
        if(Integer.parseInt(httpRespObj.get("responseCode").toString()) == 200) {
            LOGGER.info(httpRespObj.toString());
            return httpRespObj.get("response").toString();
        } else {
            LOGGER.error("RESPONSE CODE: " + httpRespObj.get("responseCode").toString());
            LOGGER.error(httpRespObj.toString());
            return null;
        }
    }

    public String waitForDsAres(){
        return getDsAres();
    }

    public String waitForDsRres(){
        return getDsRres();
    }

    public void generate3dssAreq(String version, MessageCategory category, DeviceChannel channel){
        // TODO : Log warnings/errors for unsupported paths
        switch(version){
            case "2.1.0":
                switch(category){
                    case PA:
                        switch(channel){
                            case BRW:
                                generateAreqPaBrw210();
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            case "2.2.0":
                switch(category){
                    case PA:
                        switch(channel){
                            case BRW:
                                generateAreqPaBrw220();
                                return;
                            default:
                                return;
                        }
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public void generateAcsAres(TransactionFlow transactionFlow){
        switch(transactionFlow){
            case FRICTIONLESS:
                generateAcsAresFrictionless();
                return;
            case CHALLENGE:
                generateAcsAresChallenge();
                return;
            default:
                return;
        }
    }
}
