package com.modirum.pages.dsemulator.areqrreq;

import com.modirum.models.DsEmulatorType;
import com.modirum.utilities.SelDriver;

public class AreqRreqEmulatorFactory {
    public static AreqRreqEmulator getAreqRreqEmulator(DsEmulatorType emulatorType, SelDriver selDriver, String emulatorUrl){
        switch(emulatorType){
            case HTTPCLIENT: return new AreqRreqEmulatorHttpClient(selDriver, emulatorUrl);
            case WEBPAGE:
            default:
                return new AreqRreqEmulatorPage(selDriver, emulatorUrl);
        }
    }
}
