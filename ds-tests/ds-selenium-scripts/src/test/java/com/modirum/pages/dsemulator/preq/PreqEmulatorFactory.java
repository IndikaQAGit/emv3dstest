package com.modirum.pages.dsemulator.preq;

import com.modirum.models.DsEmulatorType;
import com.modirum.utilities.SelDriver;

public class PreqEmulatorFactory {
    public static PreqEmulator getPreqEmulator(DsEmulatorType emulatorType, SelDriver selDriver, String emulatorUrl){
        switch(emulatorType){
            case HTTPCLIENT: return new PreqEmulatorHttpClient(selDriver, emulatorUrl);
            case WEBPAGE:
            default:
                return new PreqEmulatorPage(selDriver, emulatorUrl);
        }
    }
}
