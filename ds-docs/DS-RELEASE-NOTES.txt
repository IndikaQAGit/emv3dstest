2018-12-19 revision 1.0.8
	#192 DS 3DS Method relay for FSS data collection, extensions licensing
	#191, #197 DS<-->FSS JSON WS Integration
	#190 CR21 issuer bin store length configurable
	#190 merchant and range service options attributes support
	#183 dependency updates and migration to log4j2
2018-10-24 revision 1.0.7 (ul passed)
	#187 not related delete rows before add/update avoid conflict
	#183 related deployment descriptor web.xml extended with attributes metadata-complete="true"
	that prevents a tomcat7 on java8 error when modules start up. 
2018-09-26 revision 1.0.7 build 1
	#184 DS importer MIProfile with opertorID fix.
	#183 Added required dependencies to make function on Java11 environment, minor dstools fixes.
		Note: if to run on java8 then add jar xml-apis-1.4.01.jar to tomcat/lib
	#182 Add dsRefferenceNumber to Erro to acs in case RRes errors improvement
	#181 Support either acschallengeMandated and acsChallengeMandated for 2.0.1 depending on setting protocol.2.0.1.subversion
	#180 Do not restrict Urls to https or http as also http or HTTP, Http shall be fine as FQU
	#179 operatorID, startProtocolVersion, endProtocolVersion support for importer
	#179 operatorID, startProtocolVersion, endProtocolVersion support for importer
	#176 ACS and 3DSM url can't begin with whitespaces validation. Validation error for missing card range status. 
		Fix missing validation error for missing card type
		Card range rejected value for card type from bin to type
	#177 browserJavaEnabled manual boolean type validation needed as xsd layers dont ..

2018-07-11 revision 1.0.6 final
	UL UAT tests 1649 of 1654  passed, remaining are known ul, spec issues.
	Check ds-docs\sql-patch-cumul.sql for updating mysql schema is necessary
	#173 fixed 2.1 deviceInfo to base64url encoding
	#141 support for http.userAgent setting, default enabled protocol is 2.1
	#172 fix validate RRes ciritcal extensions critical flag and reject if not "known"
	#171 test tools improvements
	#170 Manager: showAcsContentBox() must open on correct location
	#166 extension data length validation update, xsd normalization
	#166 DS ARes acsTransId,acsRefNum=dsTransId,dsRefNum (emwco faq)
	#166 do not return PRes with empty array, acceptContainerAuth fix
	#140 Manager: Support for more than one message with acs signed content
	#141 PRes messagetype empty in records
	#141 Changed XcardRangeData.actionInd type to XactionInd
	#166 challengeCangel confusion after bulletin
	#166 in error to ares use record values when applicable
	#167 caseTC_DS_10172_001 empty acttInfo handl + general err hand improv	and more case fixes..
	#164 Keysintaller convenience new automated installation flow (save an hour of so in new isntallation)
	#98 requestorIDwithMerchant manager improvements, requestorIDwithMerchant - default to true
	If previously requestor id was defult with 3DS Profile /MI then now its default with merchant profile
	behaviour is configurable by setting check manual.
	#141 Made challengeCancel accept only 01,04,05,06,07,08 as specified  in specification bulletin 204:
	 This Draft Specification Bulletin No. 204 provides updates, clarifications and errata incorporated into the EMV 3-D Secure Protocol and Core Functions 
	 Specification since the October 2017 v2.1.0 publication.
	 #127 Testtools: support delayed RReq (after CRes) test case, and other

Merged notes:
	#163 Log also positive CN/DNS auth cases
	Dont log warn purchaseDate for PReq
	Cleanup and chd null out HQL query fixes
	#163 CR DS has to check of matching host ip address with FQDN from CN + range checks 
	settings 
	stage2authentication.checkSdnCnDNSMatchAcquirer 
	stage2authentication.checkSdnCnDNSMatchIssuer
	
	updated dependencies moxy to 2.7.0, modirum generics to 1.0.2
	SDK key download support
	3RI cases and other validations improvmenets
	manager: login checks improvements
	keysinstaller: SDKEC key install support
	

2018-02-05 revision 1.0.6b1
	#151 RRes destination logged wrong as 3DS in db
	#150 NPA ARes/RReq conditional AV validation settings
	#149 2.1 npa validation fixes for mcc, merchantCountryCode, merchantName
	#148 'sdkEphemPubKey is missing' in 2.1 fix (format changed)

	#147 DS Tomcat extensions moved from DS to generics
	So if You configured realm or secured properties extensions with tomcat please
	update the jar and class names in configuration as in new documentation.
	Change in ds.xml
	<Realm className="com.modirum.ds.tomcat.SSLRealm" validate="true" />
	to
	<Realm className="com.modirum.tomcat.SSLRealm" validate="true" />
	
	Change in catalina properties class name
	com.modirum.tomcat.ds.SecuredPropertySource
	to
	com.modirum.tomcat.SecuredPropertySource

	#146 Manager: Fix for delete ranges.
	#145  Support for CardRange partial updates by serial (optional functionality) enhancement

	#143 JDK Scheduler thread pool does not grow beyond core size bug/malfunction 
		fix: core size set to max size
	#141 support for EMV 3-D Secure 2.1 (M6) 
	New DS features summary:
	Configurable support for protocol either only 2.0.1 or 2.1.0 or both
	Protocol version 2.1.0 support implementation (new message structures and new processing service)
	ACS Profiles configurable with lowest and highest protocol supported.
	Configurable by setting support to distribute card ranges (PRes) with serial number and diffs only between serial numbers.
	(serial numbered card ranges data snapshots are stored to temp files so if a serial number based diff request is received the diff is calculated using current valid ranges
	compared with previously stored temp snapshot, its up to platform administrator to decide how long snapshot temp files are preserved)
	Implemented patched version of MOXY to support paring empty JSON structures (not causing nullpointerexception)


2017-04-04 revision 1.0.5b6 build2
	#163 Log also positive CN/DNS auth cases
	Dont log warn purchaseDate for PReq
	Cleanup and chd null out HQL query fixes
	Various improvments and validation fixes during eap testing.
	#153 not specified fields out filtering on forward


2017-08-31 revision 1.0.5b5
	Several logging improvements and settings reload detection improvments in cluser mode
	Spec change, error message no longer requires errorMessageType if not known (unkn removed)
	#125 Support for 2017 03 spec - treat empty as missing with setting  treatEmptyValueAsMissing=true
	#120 support multiple spec purchaseDateFormat and apply outgoing patch is setting set (patchPurchaseDateFormat value java date patterns)
	#130 Modirum JSON provider instead path to avoid illegalaccessexception under tomcat8
	RReq handlinh imrovements and default interaction counter fix 1 to 01


2017-08-07 revision 1.0.5b4
	Support for ACS Operator ID validation and certificate SDN checks
	Auto detection of settings change on every instance if multiple instances installed

2017-07-27 revision 1.0.5b4
	Importer duplicate acs profiles detection fix
	resultsstatus format fix from 1 to 2
	include dsTransID in PRes
	fix issuer acs client certificate family check
	Eliminate Array rewrites, savings with PublicBOS
	Implemented statusACSError and statusReasonACSError check for RReq also. Was only for ARes
	statusBINNotFound and statusReasonBINNotFound were not in effect when bin == null (not found at all)
	Added setting attemptACSOnAResValidationError true/false If true, then any ARes validation error including
		AReq/ARes mismatch error should result in transaction being forwarded to attempt ACS.
		Does not interfere with existing acceptedAResStatuses logic.
		If acceptedAResStatuses related error occurs then its logic priority is greater.


2017-06-09 revision 1.0.5b2
	#98 M5 updates related to 2.0.1 protocol, not compatible with 2.0.0
	#110 Importer - problems with loading entities referred to non-existent bug/malfunction
	#109 DS Manager user selectable UI timezone enhancement
	#108 Password change downgraded login strength bug/malfunction
	#105  Cookies disabled context shift bug/malfunction
	#107 DS Manager UI bug all ACP under any issuer bug/malfunction
	#104 Importer - option to import ended objects CR change reqest enhancement
	#103 Add easier support for external CA signed SDK key (certficate installation) enhancement
	#102 DS importer log issues minor issue/bug
	#96  Add CRLURL support for DS CA functionality improvement security issue 1 low
	#101 Hibernate cfg and mappings improvement

2017-03-17 revision 1.0.4b4
	M4 Advance to 2017/1 specification and other improivements
	Manager: SMS OTP, Modirum ID, ID card/cert logins
	Several minor manager app issue corrections

2016-12-21 revision 1.0.3
	M3 Advance to 2016/10 specification and other improivements