# DS
Directory Server (2.0)
ds-mngr 	manager application for ds
ds-server 	directory server application
ds-services services and models used by server and manager 


### Local Deployment via Docker Compose

**Prerequisite**:<br>
 Docker and docker-compose has been installed. Default docker-machine is already setup and running.

**Note**: This uses single tomcat to prevent CORS issue in the DS server and 3DS emulator.<br>
This intends to test functionality and not network connections.<br>
Since this uses docker-compose, implicitly a docker build is executed. <br>
No need to manually maintain a separate docker build for local deployment.<br>
1. Deploy DS Database, DS Manager, and DS Server using docker-compose. You can run shell script below: <br>
      `sh docker-compose-start.sh` <br>

2. You can now try to access the ds admin and server in the browser. <br>
      **DS Manager** <br>
      a. If you are using Docker Toolbox, you have to get first the ip address of docker machine. <br>
        `http://<docker-machine-ip>:9090/dsmngr/` <br>
      b. If you are using Docker for Mac or Docker Desktop, <br>
        `http://localhost:9090/dsmngr/`
        <br>
      **DS Server** <br>
      a. If you are using Docker Toolbox, you have to get first the ip address of docker machine. <br>
        `http://<docker-machine-ip>:9090/ds/DServer` <br>
      b. If you are using Docker for Mac or Docker Desktop, <br>
        `http://localhost:9090/ds/DServer`
        
### Build Azure Docker Images

**Prerequisite**:<br>
- An already running docker daemon
- In the terminal, must login to Container Registry in Azure CLI with the Modirum account

1. To build the docker images with the required version of code must provide:<br>
- the git hash that is already merged in master branch
- the ssh username as needed credential to fetch artifacts from jenkins build server. The script will automatically add the default ssh keys.
- the new image version for the DS<br>

```bash
git_hash=<git-hash> username=<ssh-username> DS_VERSION=<image-version> sh azure-build.sh
```

<br>
2. To push to azure registry the docker images built locally, execute the script below:<br>

```bash
DS_VERSION=<image-version> sh azure-push.sh
```