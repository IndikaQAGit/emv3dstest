package com.modirum.ds.jdbc.audit.dao;

import com.modirum.ds.jdbc.BaseDAO;
import com.modirum.ds.jdbc.SqlUtil;
import com.modirum.ds.jdbc.audit.model.AuditLog;
import com.modirum.ds.jdbc.audit.rowmapper.AuditLogRowMapper;
import com.modirum.ds.utils.Misc;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AuditLogDAO extends BaseDAO {

    private final static int DETAIL_MAX_LENGTH = 512;

    private final AuditLogRowMapper auditLogRowMapper = new AuditLogRowMapper();

    public void insert(AuditLog auditLog) {
        long nextAuditLogId = SqlUtil.getIdGenNextValue("auditId", getJdbcTemplate());

        auditLog.setId(nextAuditLogId);
        auditLog.setDetails(Misc.trunc(auditLog.getDetails(), DETAIL_MAX_LENGTH));

        String sql = "INSERT INTO ds_auditlog (id, whendate, byuser, byuserId, action, objectId, objectClass, details, paymentSystemId)" +
                " VALUES (:id, :whendate, :byuser, :byuserId, :action, :objectId, :objectClass, :details, :paymentSystemId)";

        Map<String, Object> params = new HashMap<>();
        params.put("id", auditLog.getId());
        params.put("whendate", auditLog.getWhendate());
        params.put("byuser", auditLog.getByuser());
        params.put("byuserId", auditLog.getByuserId());
        params.put("action", auditLog.getAction());
        params.put("objectId", auditLog.getObjectId());
        params.put("objectClass", auditLog.getObjectClass());
        params.put("details", auditLog.getDetails());
        params.put("paymentSystemId", auditLog.getPaymentSystemId());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    public AuditLog get(Long id) {
        String sql = "SELECT id, whendate, byuser, byuserId, action, objectId, objectClass, details, paymentSystemId" +
                " FROM ds_auditlog WHERE id=?";

        List<AuditLog> result  = getJdbcTemplate().query(sql, auditLogRowMapper, id);
        if (CollectionUtils.isEmpty(result)) {
            return null;
        }
        return result.get(0);
    }
}
