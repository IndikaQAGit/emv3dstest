package com.modirum.ds.jdbc.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Represents ds_init table entry.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DSInit {

    private String initKey;
    private String initValue;
    private String lastModifiedBy;
    private Date lastModifiedDate;

}
