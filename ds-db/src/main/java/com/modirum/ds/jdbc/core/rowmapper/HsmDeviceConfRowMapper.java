package com.modirum.ds.jdbc.core.rowmapper;

import com.modirum.ds.jdbc.core.model.HsmDeviceConf;
import com.modirum.ds.utils.JdbcUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HsmDeviceConfRowMapper implements RowMapper<HsmDeviceConf> {

    @Override
    public HsmDeviceConf mapRow(ResultSet rs, int i) throws SQLException {
        return HsmDeviceConf.builder()
            .dsId(JdbcUtils.getColumnValue(rs, "dsId", Integer.class))
            .hsmDeviceId(JdbcUtils.getColumnValue(rs, "hsmdevice_id", Long.class))
            .config(JdbcUtils.getColumnValue(rs, "config", String.class))
            .build();
    }

}
