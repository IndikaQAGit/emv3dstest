package com.modirum.ds.jdbc.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HsmDeviceConf {

    private Long hsmDeviceId;
    private int dsId;
    private String config;

}
