package com.modirum.ds.jdbc.audit.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Represents ds_auditlog table entry.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuditLog {

    private Long id;
    private Date whendate;
    private String byuser;
    private Long byuserId;
    private String action;
    private Long objectId;
    private String objectClass;
    private String details;
    private Integer paymentSystemId;
}
