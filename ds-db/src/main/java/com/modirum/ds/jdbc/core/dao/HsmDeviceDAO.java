package com.modirum.ds.jdbc.core.dao;

import com.modirum.ds.jdbc.BaseDAO;
import com.modirum.ds.jdbc.SqlUtil;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.core.rowmapper.HsmDeviceRowMapper;
import com.modirum.ds.utils.Misc;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class HsmDeviceDAO extends BaseDAO {

    private final HsmDeviceRowMapper hsmDeviceRowMapper = new HsmDeviceRowMapper();

    /**
     * Checks if an HSM Device exists given its ID
     * @param id
     * @return
     */
    public boolean exists(Long id) {
        String sql = "SELECT 1 FROM ds_hsmdevice WHERE id = ? LIMIT 1";
        return getJdbcTemplate().queryForList(sql, Long.class, id).stream().findFirst().isPresent();
    }

    /**
     * Retrieves an HSM Device given its ID
     * @param id
     * @return
     */
    public HsmDevice get(Long id) {
        String sql = "SELECT id, name, className, status FROM ds_hsmdevice WHERE id = ?";
        return getJdbcTemplate().query(sql, hsmDeviceRowMapper, id).stream().findFirst().orElse(null);
    }

    /**
     * Retrieves all HSM Devices that have the given status
     * @param status
     * @return
     */
    public List<HsmDevice> getByStatus(String status) {
        String sql = "SELECT id, name, className, status FROM ds_hsmdevice WHERE status = ?";
        List<HsmDevice> hsmDevices = getJdbcTemplate().query(sql, hsmDeviceRowMapper, status);
        if (CollectionUtils.isEmpty(hsmDevices)) {
            return null;
        }
        return hsmDevices;
    }

    /**
     * Retrieves all HSM Devices that have the given name
     * @param name
     * @return
     */
    public List<HsmDevice> getByName(String name) {
        String sql = "SELECT id, name, className, status FROM ds_hsmdevice WHERE name = ?";
        List<HsmDevice> hsmDevices = getJdbcTemplate().query(sql, hsmDeviceRowMapper, name);
        if (CollectionUtils.isEmpty(hsmDevices)) {
            return null;
        }
        return hsmDevices;
    }

    /**
     * Retrieves all HSM Devices
     * @return
     */
    public List<HsmDevice> getAll() {
        String sql = "SELECT id, name, className, status FROM ds_hsmdevice";
        List<HsmDevice> hsmDevices = getJdbcTemplate().query(sql, hsmDeviceRowMapper);
        if (CollectionUtils.isEmpty(hsmDevices)) {
            return null;
        }
        return hsmDevices;
    }

    /**
     * Inserts an HSM Device record
     * @param hsmDevice
     */
    public void insert(HsmDevice hsmDevice) {
        String sql = "INSERT INTO ds_hsmdevice (id, name, className, status)" +
                " VALUES (:id, :name, :className, :status)";

        Map<String, Object> params = new HashMap<>();
        if (Misc.isNullOrEmpty(hsmDevice.getId())) {
            long nextId = SqlUtil.getIdGenNextValue("hsmDeviceId", getJdbcTemplate());
            hsmDevice.setId(nextId);
        }
        params.put("id", hsmDevice.getId());
        params.put("name", hsmDevice.getName());
        params.put("className", hsmDevice.getClassName());
        params.put("status", hsmDevice.getStatus());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    /**
     * Updates an HSM Device record
     * @param hsmDevice
     */
    public void update(HsmDevice hsmDevice) {
        String sql = "UPDATE ds_hsmdevice SET name = :name, className = :className, status = :status " +
                "WHERE id = :id";

        Map<String, Object> params = new HashMap<>();
        params.put("id", hsmDevice.getId());
        params.put("name", hsmDevice.getName());
        params.put("className", hsmDevice.getClassName());
        params.put("status", hsmDevice.getStatus());

        getNamedParameterJdbcTemplate().update(sql, params);
    }
}
