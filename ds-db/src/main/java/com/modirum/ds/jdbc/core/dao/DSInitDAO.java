package com.modirum.ds.jdbc.core.dao;

import com.modirum.ds.jdbc.BaseDAO;
import com.modirum.ds.jdbc.SqlUtil;
import com.modirum.ds.jdbc.core.rowmapper.DSInitRowMapper;
import com.modirum.ds.jdbc.core.model.DSInit;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DSInitDAO extends BaseDAO {

    private final DSInitRowMapper dsInitRowMapper = new DSInitRowMapper();

    public DSInit getByInitKey(String initKey) {
        String sql = "SELECT initKey, initValue, lastModifiedBy, lastModifiedDate FROM ds_init WHERE initKey=?";
        List<DSInit> result  = getJdbcTemplate().query(sql, dsInitRowMapper, initKey);
        if (CollectionUtils.isEmpty(result)) {
            return null;
        }
        return result.get(0);
    }

    /**
     * Checks whether a ds_init entry exists in the database for the given <code>initKey</code>.
     */
    public boolean exists(String initKey) {
        String sql = "SELECT 1 FROM ds_init WHERE initKey=? LIMIT 1";
        List<Long> result  = getJdbcTemplate().queryForList(sql, Long.class, initKey);
        if (CollectionUtils.isEmpty(result)) {
            return false;
        }
        return true;
    }

    public void insert(DSInit dsInit) {
        String sql = "INSERT INTO ds_init (initKey, initValue, lastModifiedBy, lastModifiedDate)" +
                " VALUES (:initKey, :initValue, :lastModifiedBy, :lastModifiedDate)";

        Map<String, Object> params = new HashMap<>();
        params.put("initKey", dsInit.getInitKey());
        params.put("initValue", dsInit.getInitValue());
        params.put("lastModifiedBy", dsInit.getLastModifiedBy());
        params.put("lastModifiedDate", dsInit.getLastModifiedDate());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

    public void update(DSInit dsInit) {
        String sql = "UPDATE ds_init SET initValue=:initValue, lastModifiedBy=:lastModifiedBy," +
                " lastModifiedDate=:lastModifiedDate WHERE initKey=:initKey";

        Map<String, Object> params = new HashMap<>();
        params.put("initValue", dsInit.getInitValue());
        params.put("lastModifiedBy", dsInit.getLastModifiedBy());
        params.put("lastModifiedDate", dsInit.getLastModifiedDate());
        params.put("initKey", dsInit.getInitKey());

        getNamedParameterJdbcTemplate().update(sql, params);
    }

}
