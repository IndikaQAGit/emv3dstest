package com.modirum.ds.jdbc.core.rowmapper;

import com.modirum.ds.jdbc.core.model.TdsRecordAttribute;
import com.modirum.ds.utils.JdbcUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class TdsRecordAttributeRowMapper implements RowMapper<TdsRecordAttribute> {

    @Override
    public TdsRecordAttribute mapRow(ResultSet rs, int i) throws SQLException {
        return TdsRecordAttribute.builder()
                .id(JdbcUtils.getColumnValue(rs, "id", String.class))
                .tdsRecordId(JdbcUtils.getColumnValue(rs, "tdsRecordId", Long.class))
                .key(JdbcUtils.getColumnValue(rs, "attr", String.class))
                .value(JdbcUtils.getColumnValue(rs, "value", String.class))
                .asciiValue(JdbcUtils.getColumnValue(rs, "asciiValue", String.class))
                .createdDate(JdbcUtils.getColumnValue(rs, "createdDate", Timestamp.class))
                .createdUser(JdbcUtils.getColumnValue(rs, "createdUser", String.class))
                .build();
    }

}
