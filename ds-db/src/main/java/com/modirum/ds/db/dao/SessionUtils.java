package com.modirum.ds.db.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Helper methods for Hibernate Session API
 */
final class SessionUtils {
    private SessionUtils() {}

    /**
     * Safely begin a transaction if not currently active anymore.
     * @param session
     */
    public static Transaction txBegin(Session session) {
        if (!session.getTransaction().isActive()) {
            return session.beginTransaction();
        }
        return session.getTransaction();
    }

    /**
     * Quietly rollback the active transaction.
     * @param session
     */
    public static void txRollback(Session session) {
        try {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
        } catch (Exception e) {
        }
    }
}
