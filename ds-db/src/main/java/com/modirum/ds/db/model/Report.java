package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Generated
@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class Report extends PersistableClass implements PaymentSystemResource, Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer paymentSystemId;
    private String status;
    private String secret;
    private String filename;
    private String name;
    private String dateFrom;
    private String dateTo;
    private String criteria;
    private String createdUser;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;

}
