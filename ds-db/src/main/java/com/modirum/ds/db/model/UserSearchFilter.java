package com.modirum.ds.db.model;

import java.io.Serializable;

/**
 * DS-Admin User profile search-list filter. Filled from search panel, used to lookup Users from DB.
 */
public class UserSearchFilter extends User implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer start;
    private Integer limit;
    private Integer total;
    private String order;
    private String orderDirection;
    private String keyword;

    private String issuerRole;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(String orderDirection) {
        this.orderDirection = orderDirection;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getIssuerRole() {
        return issuerRole;
    }

    public void setIssuerRole(String issuerRole) {
        this.issuerRole = issuerRole;
    }
}
