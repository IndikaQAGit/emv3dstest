package com.modirum.ds.db.model;

import java.io.Serializable;

public class HsmDeviceConf implements Persistable, Serializable {

    private Long hsmDeviceId;
    private int dsId;
    private String config;

    public Long getHsmDeviceId() {
        return hsmDeviceId;
    }

    public void setHsmDeviceId(Long hsmDevice) {
        this.hsmDeviceId = hsmDeviceId;
    }

    public int getDsId() {
        return dsId;
    }

    public void setDsId(int dsId) {
        this.dsId = dsId;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    @Override
    public Serializable getId() {
        return new HsmDeviceConfCompositeKey(hsmDeviceId, dsId);
    }

    public static final class HsmDeviceConfCompositeKey implements Serializable {
        private Long hsmDeviceId;
        private int dsId;

        public HsmDeviceConfCompositeKey(Long hsmDeviceId, int dsId) {
            this.hsmDeviceId = hsmDeviceId;
            this.dsId = dsId;
        }
    }
}
