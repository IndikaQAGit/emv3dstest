package com.modirum.ds.db.model;

import com.modirum.ds.db.model.ui.PaymentSystemResource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
@XmlType(name = "ACSProfile", propOrder = {"issuerId", "name", "URL", "threeDSMethodURL", "status", "refNo", "outClientCert", "inClientCert", "set", "SSLProto", "operatorID", "startProtocolVersion", "endProtocolVersion", "acsInfoInd"})
public class ACSProfile extends PersistableClass implements PaymentSystemResource {

    protected Integer id;
    protected Integer issuerId;
    private Integer paymentSystemId;
    protected String name;
    protected String URL;
    protected String threeDSMethodURL;
    protected Long outClientCert;
    protected Long inClientCert;
    protected String status;
    protected String SSLProto;
    protected String refNo;
    protected String operatorID;
    protected Short set;
    protected String startProtocolVersion;
    protected String endProtocolVersion;
    protected String acsInfoInd;
    private Date lastMod;
    private transient String paymentSystemName; // for viewModel

    @XmlAttribute
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(Integer issuerId) {
        this.issuerId = issuerId;
    }

    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String uRL) {
        URL = uRL;
    }

    public Long getOutClientCert() {
        return outClientCert;
    }

    public void setOutClientCert(Long outClientCert) {
        this.outClientCert = outClientCert;
    }

    public Long getInClientCert() {
        return inClientCert;
    }

    public void setInClientCert(Long inClientCerId) {
        this.inClientCert = inClientCerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSSLProto() {
        return SSLProto;
    }

    public void setSSLProto(String sSLProto) {
        SSLProto = sSLProto;
    }

    public String getThreeDSMethodURL() {
        return threeDSMethodURL;
    }

    public void setThreeDSMethodURL(String threeDSMethodURL) {
        this.threeDSMethodURL = threeDSMethodURL;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Short getSet() {
        return set;
    }

    public void setSet(Short set) {
        this.set = set;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    @XmlAttribute
    public Date getLastMod() {
        return lastMod;
    }

    public void setLastMod(Date lastMod) {
        this.lastMod = lastMod;
    }

    public String getEndProtocolVersion() {
        return endProtocolVersion;
    }

    public void setEndProtocolVersion(String endProtocolVersion) {
        this.endProtocolVersion = endProtocolVersion;
    }

    public String getStartProtocolVersion() {
        return startProtocolVersion;
    }

    public void setStartProtocolVersion(String startProtocolVersion) {
        this.startProtocolVersion = startProtocolVersion;
    }

    public String getPaymentSystemName() {
        return paymentSystemName;
    }

    public void setPaymentSystemName(String paymentSystemName) {
        this.paymentSystemName = paymentSystemName;
    }

    public String getAcsInfoInd() {
        return acsInfoInd;
    }

    public void setAcsInfoInd(String acsInfoInd) {
        this.acsInfoInd = acsInfoInd;
    }
}
