/*
 * Copyright (C) 2013 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 24.01.2013
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public abstract class PersistableClass implements Importable, Persistable, Cloneable {

    transient Byte isid; // ignore in equals

    @XmlTransient
    public Byte getIsId() {
        return isid;
    }

    public void setIsId(Byte isid) {
        this.isid = isid;
    }

    @Override
    public int hashCode() {
        return com.modirum.ds.utils.EqualHashUtil.hashCode(this);
    }

    @Override
    public boolean equals(Object o) {
        return com.modirum.ds.utils.EqualHashUtil.equals(this, o);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
