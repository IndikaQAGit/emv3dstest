/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.01.2016
 *
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "SchemeSubject", propOrder = {"BIN", "address", "city", "country", "email", "name", "phone", "status"})
public class SchemeSubject extends PersistableClass {

    protected Integer id;
    protected String BIN;
    protected String name;
    protected String phone;
    protected String email;
    protected String address;
    protected String city;
    protected String country;
    protected String status;

    @XmlAttribute()
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBIN() {
        return BIN;
    }

    public void setBIN(String bIN) {
        BIN = bIN;
    }
}
