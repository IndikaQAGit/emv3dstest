package com.modirum.ds.db.model;

/**
 * Base interface for Searcher DTO used in database query.
 */
public interface Searcher {
    Integer getStart();

    Integer getLimit();

    Integer getTotal();

    void setTotal(Integer total);

    String getOrder();

    String getOrderDirection();
}
