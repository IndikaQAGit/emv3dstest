/*
 * Copyright (C) 2016 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.01.2016
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.db.model;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType()
@XmlType(name = "CardRange", propOrder = {"bin", "start", "end", "issuerId", "name", "status", "subType", "type", "set"})
public class CardRange extends PersistableClass implements Serializable, Persistable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Integer issuerId;
    private String name;
    private String bin; // bin or start prefix
    private String end; // end
    private Short type;
    private Short subType;
    private Short set;
    private String status;
    private String methodURL; // joined
    private String startProtocolVersion; // joined
    private String endProtocolVersion; // joined
    private Date lastMod;
    boolean binmode = true;
    boolean includeThreeDSMethodURL;
    private String acsInfoInd; // comma separated values
    private String options; // rba/fss or other options
    private transient Integer paymentSystemId;
    private String acsInfoInd2_3_0;

    @XmlAttribute()
    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public void setRangeMode() {
        binmode = false;
    }

    public boolean isRangeMode() {
        return !binmode;
    }

    // option wrap to supported xsd
    public String getStart() {
        return binmode ? null : bin;
    }

    public void setStart(String start) {
        this.bin = start;
    }

    public String getBin() {
        return binmode ? bin : null;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    // these methods are for hibenrate layer only
    @XmlTransient
    public void setBinSt(String binStart) {
        this.bin = binStart;
    }

    public String getBinSt() {
        return bin;
    }

    public String getEnd() {
        return binmode ? null : end;
    }

    public void setEnd(String end) {
        binmode = (end == null);
        this.end = end;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public boolean isIncludeThreeDSMethodURL() {
        return includeThreeDSMethodURL;
    }

    public void setIncludeThreeDSMethodURL(boolean includeThreeDSMethodURL) {
        this.includeThreeDSMethodURL = includeThreeDSMethodURL;
    }

    @XmlAttribute()
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String toString() {
        return "CardRange {" + id + ":" + type + ":" + bin + "}";
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(Integer issuerId) {
        this.issuerId = issuerId;
    }

    public Short getSubType() {
        return subType;
    }

    public void setSubType(Short subType) {
        this.subType = subType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getSet() {
        return set;
    }

    public void setSet(Short set) {
        this.set = set;
    }

    // from acs profile and used in ranges pres creation
    @XmlTransient
    public String getMethodURL() {
        return methodURL;
    }

    public void setMethodURL(CharSequence methodURL) {
        this.methodURL = methodURL != null ? methodURL.toString() : null;
    }

    @XmlAttribute
    public Date getLastMod() {
        return lastMod;
    }

    public void setLastMod(Date lastMod) {
        this.lastMod = lastMod;
    }

    // from acs profile and used in ranges pres creation
    @XmlTransient
    public String getEndProtocolVersion() {
        return endProtocolVersion;
    }

    public void setEndProtocolVersion(String endProtocolVersion) {
        this.endProtocolVersion = endProtocolVersion;
    }

    // from acs profile and used in ranges pres creation
    @XmlTransient
    public String getStartProtocolVersion() {
        return startProtocolVersion;
    }

    public void setStartProtocolVersion(String startProtocolVersion) {
        this.startProtocolVersion = startProtocolVersion;
    }

    @XmlTransient
    public String getAcsInfoInd2_3_0() {
        return acsInfoInd2_3_0;
    }

    public void setAcsInfoInd2_3_0(String acsInfoInd2_3_0) {
        this.acsInfoInd2_3_0 = acsInfoInd2_3_0;
    }

    public String getAcsInfoInd() {
        return acsInfoInd;
    }

    public void setAcsInfoInd(String acsInfoInd) {
        this.acsInfoInd = acsInfoInd;
    }

    public Integer getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Integer paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }
}
