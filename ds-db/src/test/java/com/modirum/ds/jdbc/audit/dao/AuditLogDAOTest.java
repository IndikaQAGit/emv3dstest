package com.modirum.ds.jdbc.audit.dao;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.jdbc.audit.model.AuditLog;
import com.modirum.ds.jdbc.test.support.RunWithDB;
import com.modirum.ds.jdbc.test.support.TestEmbeddedDbUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;

@RunWithDB
public class AuditLogDAOTest {

    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    private static AuditLogDAO auditLogDAO;

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        auditLogDAO = new AuditLogDAO();
        auditLogDAO.setDataSource(dataSource);
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_auditlog;"})
    public void insert_success() {
        AuditLog auditLog = AuditLog.builder()
                .whendate(new Date())
                .byuser("thisuser")
                .byuserId(1L)
                .action("someAction")
                .objectId(123L)
                .objectClass("class.string")
                .details("details text")
                .paymentSystemId(null).build();

        auditLogDAO.insert(auditLog);

        Assertions.assertNotNull(auditLog.getId());

        AuditLog retrieved = auditLogDAO.get(auditLog.getId());
        Assertions.assertNotNull(retrieved);

        Assertions.assertEquals("thisuser", retrieved.getByuser());
        Assertions.assertEquals(1L, retrieved.getByuserId());
        Assertions.assertNull(retrieved.getPaymentSystemId());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_auditlog;"})
    public void insert_two_success() {
        AuditLog auditLog = AuditLog.builder()
                .whendate(new Date())
                .byuser("thisuser")
                .byuserId(1L)
                .action("someAction")
                .objectId(123L)
                .objectClass("class.string")
                .details("details text")
                .paymentSystemId(null).build();

        auditLogDAO.insert(auditLog);
        Long insertIdOne = auditLog.getId();

        auditLogDAO.insert(auditLog);
        Long insertIdTwo = auditLog.getId();

        Assertions.assertNotNull(insertIdOne);
        Assertions.assertNotNull(insertIdTwo);
        Assertions.assertTrue(!insertIdOne.equals(insertIdTwo));

        AuditLog retrieved1 = auditLogDAO.get(insertIdOne);
        AuditLog retrieved2 = auditLogDAO.get(insertIdTwo);

        Assertions.assertNotNull(retrieved1);
        Assertions.assertNotNull(retrieved2);

    }
}
