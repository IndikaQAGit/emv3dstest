package com.modirum.ds.jdbc.core.dao;

import com.github.database.rider.core.api.dataset.DataSet;
import com.modirum.ds.db.model.DSModel;
import com.modirum.ds.jdbc.core.model.HsmDevice;
import com.modirum.ds.jdbc.test.support.RunWithDB;
import com.modirum.ds.jdbc.test.support.TestEmbeddedDbUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

@RunWithDB
public class HsmDeviceDAOTest {

    private static final DataSource dataSource = TestEmbeddedDbUtil.dataSource();

    private static HsmDeviceDAO hsmDeviceDAO;

    @BeforeAll
    public static void beforeAll() throws Exception {
        TestEmbeddedDbUtil.runProjectScript(dataSource, "/../db-scripts/ds-schema-mysql-a.sql");
        hsmDeviceDAO = new HsmDeviceDAO();
        hsmDeviceDAO.setDataSource(dataSource);
    }

    @AfterAll
    public static void afterAll() throws SQLException {
        TestEmbeddedDbUtil.shutdownDB(dataSource);
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice;"})
    public void insert_success() {
        HsmDevice expectedHsmDevice = new HsmDevice();
        expectedHsmDevice.setId(1L);
        expectedHsmDevice.setName("insert Test Name");
        expectedHsmDevice.setClassName("com.modirum.ds.hsm.ACOS5Service");
        expectedHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
        hsmDeviceDAO.insert(expectedHsmDevice);

        HsmDevice actualHsmDevice = hsmDeviceDAO.get(1L);
        Assertions.assertNotNull(actualHsmDevice);
        Assertions.assertEquals("insert Test Name", actualHsmDevice.getName());
        Assertions.assertEquals("com.modirum.ds.hsm.ACOS5Service", actualHsmDevice.getClassName());
        Assertions.assertEquals(DSModel.HsmDevice.Status.ACTIVE, actualHsmDevice.getStatus());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice;"})
    public void get_success() {
        HsmDevice expectedHsmDevice = new HsmDevice();
        expectedHsmDevice.setId(1L);
        expectedHsmDevice.setName("get Test Name");
        expectedHsmDevice.setClassName("com.modirum.ds.hsm.ACOS5Service");
        expectedHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
        hsmDeviceDAO.insert(expectedHsmDevice);

        HsmDevice actualHsmDevice = hsmDeviceDAO.get(1L);
        Assertions.assertNotNull(actualHsmDevice);
        Assertions.assertEquals("get Test Name", actualHsmDevice.getName());
        Assertions.assertEquals("com.modirum.ds.hsm.ACOS5Service", actualHsmDevice.getClassName());
        Assertions.assertEquals(DSModel.HsmDevice.Status.ACTIVE, actualHsmDevice.getStatus());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice;"})
    public void update_success() {
        HsmDevice expectedHsmDevice = new HsmDevice();
        expectedHsmDevice.setId(1L);
        expectedHsmDevice.setName("update Test Name");
        expectedHsmDevice.setClassName("com.modirum.ds.hsm.ACOS5Service");
        expectedHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
        hsmDeviceDAO.insert(expectedHsmDevice);

        expectedHsmDevice.setName("new update Test Name");
        expectedHsmDevice.setStatus(DSModel.HsmDevice.Status.DISABLED);
        hsmDeviceDAO.update(expectedHsmDevice);

        HsmDevice actualHsmDevice = hsmDeviceDAO.get(1L);
        Assertions.assertNotNull(actualHsmDevice);
        Assertions.assertEquals("new update Test Name", actualHsmDevice.getName());
        Assertions.assertEquals("com.modirum.ds.hsm.ACOS5Service", actualHsmDevice.getClassName());
        Assertions.assertEquals(DSModel.HsmDevice.Status.DISABLED, actualHsmDevice.getStatus());
    }

    @Test
    @DataSet(executeStatementsAfter = {"TRUNCATE TABLE ds_hsmdevice;"})
    public void exists_success() {
        HsmDevice expectedHsmDevice = new HsmDevice();
        expectedHsmDevice.setId(1L);
        expectedHsmDevice.setName("update Test Name");
        expectedHsmDevice.setClassName("com.modirum.ds.hsm.ACOS5Service");
        expectedHsmDevice.setStatus(DSModel.HsmDevice.Status.ACTIVE);
        hsmDeviceDAO.insert(expectedHsmDevice);

        boolean isExists = hsmDeviceDAO.exists(1L);
        Assertions.assertTrue(isExists);
    }
}
