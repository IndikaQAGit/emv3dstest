INSERT INTO ds.ds_settings (skey,svalue,comments,lastModified,lastModifiedBy)
VALUES ('disableDsMngrIpWhitelisting','true','','2020-09-28 17:42:25.000','admin (1000)'),
  ('viaProxyEnabled','true','','2020-09-28 17:42:25.000','admin (1000)'),
  ('sslCertsManagedExternally','true','','2020-09-28 17:42:25.000','admin (1000)'),
  ('proxyType','nginx','','2020-09-28 17:42:25.000','admin (1000)'),
  ('acs.ssl.client.certId','4','Default ACS SSL auth keypair/certificate id', '2020-09-28 17:42:25.000', 'admin (1000)'),
  ('mi.ssl.client.certId','4','Default MI SSL auth keypair/certificate id', '2020-09-28 17:42:25.000', 'admin (1000)'),
  ('settingsEnumEnabled','true','','2020-09-28 17:42:25.000','admin (1000)'),
  ('DS.versionDefault', '2.1.0', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('DS.versionsEnabled', '2.1.0 2.2.0', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('enableLoggingFullPres', 'true', '', '2020-09-28 17:42:25.000', 'admin (10000)'),
  ('scheme.logo', 'https://modirum.blob.core.windows.net/discover/images/DN_ProtectBuy_Horiz.jpg','','2020-09-28 17:42:25.000','admin (1000)');
