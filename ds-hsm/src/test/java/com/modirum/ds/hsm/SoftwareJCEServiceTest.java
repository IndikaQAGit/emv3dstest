package com.modirum.ds.hsm;

import com.modirum.ds.hsm.support.HSMUtil;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import com.modirum.ds.utils.HexUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class SoftwareJCEServiceTest {

    private static SoftwareJCEService softwareJCEService;

    @BeforeAll
    public static void beforeClass() throws Exception {
        softwareJCEService = new SoftwareJCEService();
        softwareJCEService.initializeHSM(null);
    }

    @Test
    public void generateHSMDataEncryptionKey_success() {
        try {
            // AES keys are just sequence of random bytes, no special checks.
            HSMDevice.Key aesKey = softwareJCEService.generateHSMDataEncryptionKey();

            Assertions.assertNotNull(aesKey);
            // using 256bit (32 bytes) aes key
            Assertions.assertEquals(32, aesKey.getValue().length, "Generated AES key is not 256 bit long.");
            Assertions.assertEquals(3, aesKey.getCheckValue().length, "AES kcv is not 3 bytes long");

            byte[] encryptedZeroBlock = softwareJCEService.encryptData(aesKey.getValue(), SoftwareJCEService.ZERO_BYTE_16_BLOCK);
            byte[] calculatedKVC = HSMUtil.head(3, encryptedZeroBlock);

            Assertions.assertEquals(HexUtils.toString(aesKey.getCheckValue()), HexUtils.toString(calculatedKVC), "Expected KCV is not equal to recalculated.");
        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during generateHSMDataEncryptionKey_success.");
        }
    }

    @Test
    public void encryptData_success() {
        try {
            HSMDevice.Key aesKey = softwareJCEService.generateHSMDataEncryptionKey();
            String clearTextData = "clear text 19 bytes";

            byte[] encryptedData = softwareJCEService.encryptData(aesKey.getValue(), clearTextData.getBytes());

            Assertions.assertNotNull(encryptedData);

            // Manual decryption to verify explicitly. AES in ECB mode with pkcs5
            Cipher cipher = Cipher.getInstance(SoftwareJCEService.AES_ECB_PKCS5_PADDING, SoftwareJCEService.SUN_JCE);
            SecretKeySpec secretKey = new SecretKeySpec(aesKey.getValue(), SoftwareJCEService.AES);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decrypted = cipher.doFinal(encryptedData);
            String decrypredText = new String(decrypted);

            Assertions.assertEquals(clearTextData, decrypredText, "Data decrypted is not equal to original");

        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during encryptData_success.");
        }
    }

    @Test
    public void decryptData_success() {
        try {
            String clearTextData = "clear text 19 bytes";
            HSMDevice.Key aesKey = softwareJCEService.generateHSMDataEncryptionKey();

            // Manual encryption to verify explicitly. AES in ECB mode with pkcs5
            Cipher cipher = Cipher.getInstance(SoftwareJCEService.AES_ECB_PKCS5_PADDING, SoftwareJCEService.SUN_JCE);
            SecretKeySpec secretKey = new SecretKeySpec(aesKey.getValue(), SoftwareJCEService.AES);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encrypted = cipher.doFinal(clearTextData.getBytes());

            // test
            byte[] decrypted = softwareJCEService.decryptData(aesKey.getValue(), encrypted);
            Assertions.assertNotNull(decrypted);
            Assertions.assertEquals(clearTextData, new String(decrypted), "Data decrypted is not equal to original");

        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during decryptData_success.");
        }
    }

    @Test
    public void generateRSAdecryptAndSign_success() {
        try {
            KeyPair rsaKeyResult = softwareJCEService.generateRSAdecryptAndSign(4096);
            byte[] bytesToSignForTest = "Bunch of random bytes to sign".getBytes();

            // Manual test signature.
            KeyFactory keyFactory = KeyFactory.getInstance(SoftwareJCEService.RSA, SoftwareJCEService.BC);
            PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(rsaKeyResult.getPublic().getEncoded()));
            PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(rsaKeyResult.getPrivate().getEncoded()));

            Signature sign = Signature.getInstance("SHA256withRSA");
            sign.initSign(privateKey);
            sign.update(bytesToSignForTest);
            byte[] signature = sign.sign();

            sign.initVerify(publicKey);
            sign.update(bytesToSignForTest);

            boolean isValidSignature = sign.verify(signature);
            Assertions.assertTrue(isValidSignature, "Unable to verify RSA keypair.");

        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during generateRSA_success.");
        }
    }

    @Test
    public void rsaPrivateDecrypt_success() {
        try {
            KeyPair rsaKeyResult = softwareJCEService.generateRSAdecryptAndSign(4096);
            String clearTextData = "Some test data for rsa decryption test";

            KeyFactory keyFactory = KeyFactory.getInstance(SoftwareJCEService.RSA, SoftwareJCEService.BC);
            PublicKey rsaKeyResultPublic = keyFactory.generatePublic(new X509EncodedKeySpec(rsaKeyResult.getPublic().getEncoded()));
            Cipher rsaEncryptCipher = Cipher.getInstance(SoftwareJCEService.RSA_NONE_OAEP_SHA256_MGF1);
            rsaEncryptCipher.init(Cipher.ENCRYPT_MODE, rsaKeyResultPublic);
            byte[] rsaEncryptedData = rsaEncryptCipher.doFinal(clearTextData.getBytes());

            byte[] decrypted = softwareJCEService.rsaPrivateDecrypt(rsaKeyResult.getPrivate().getEncoded(),
                    rsaEncryptedData, RSADecryptPadMode.OAEP_SHA256_MGF1);

            Assertions.assertEquals(clearTextData, new String(decrypted), "RSA decrypted data ");

        } catch (Exception e) {
            e.printStackTrace();
            Assertions.fail("Unexpected exception during rsaPrivateDecrypt_success.");
        }
    }
}
