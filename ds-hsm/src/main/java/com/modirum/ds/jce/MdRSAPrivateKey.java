package com.modirum.ds.jce;

import com.modirum.ds.utils.Base64;

import java.math.BigInteger;
import java.security.interfaces.RSAPrivateKey;
import java.util.Random;

/**
 * Modirum RSA private key implementation for custom JCE support.
 */
public final class MdRSAPrivateKey implements RSAPrivateKey {

    private final byte[] key;

    public MdRSAPrivateKey(final byte[] key) {
        this.key = key;
    }

    /**
     * Creates RSA private key from base64-encoded bytes.
     *
     * @param base64key base64 string
     * @return HSM RSA private key.
     */
    public static MdRSAPrivateKey fromString(String base64key) {
        return new MdRSAPrivateKey(Base64.decode(base64key));
    }

    @Override
    public BigInteger getModulus() {
        // kludge to get around TLS 1.2 hash selection in JSSE
        // RSA operations happen in-HSM
        return new BigInteger(4096, new Random());
    }

    @Override
    public BigInteger getPrivateExponent() {
        // RSA operations happen in-HSM
        return null;
    }

    @Override
    public String getAlgorithm() {
        return "RSA";
    }

    @Override
    public byte[] getEncoded() {
        return key;
    }

    @Override
    public String getFormat() {
        // RSA operations happen in-HSM
        return null;
    }
}
