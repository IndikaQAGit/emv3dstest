/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 11.02.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.hsm;

import java.io.ByteArrayOutputStream;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
import javax.smartcardio.TerminalFactory;

import com.modirum.ds.hsm.support.HsmException;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;

/**
 * Not compliant with PCI-3DS requirement to have in-HSM RSA private key to decrypt areq.sdkEncData
 */
@Deprecated
public class ACOS5Service implements HSMDevice {

    protected static Logger log = LoggerFactory.getLogger(ACOS5Service.class);
    protected static boolean debug = false;
    protected String aesKeyFolder = "4100";
    protected String aesKeyRecord = "4102";

    public static class ACOSDataKey implements HSMDevice.Key {
        byte[] value;
        byte[] checkValue;

        public ACOSDataKey(byte[] value, byte[] checkValue) {
            this.value = value;
            this.checkValue = checkValue;
        }

        public byte[] getValue() {
            return this.value;
        }

        public byte[] getCheckValue() {
            return this.checkValue;
        }
    }

    public interface ResultCodes {
        public int COK = Integer.valueOf("9000", 16); // 9000
        public int COKSw1 = Integer.valueOf("90", 16); // 9000
        public int COKgetData = Integer.valueOf("6100", 16); // 61nn ***
        public int COKgetDataSW1 = Integer.valueOf("61", 16);
        public int CInvalidCLA = Integer.valueOf("6E00", 16);
        public int CInvalidINS = Integer.valueOf("6D00", 16);
        public int CWrongP3 = Integer.valueOf("6700", 16);
        public int CSecCondFail = Integer.valueOf("6982", 16);
        public int CWrongMac = Integer.valueOf("6884", 16);
        public int CMFExists = Integer.valueOf("6984", 16);
        public int CMacError = Integer.valueOf("6985", 16);
        public int CNoMFFound = Integer.valueOf("6986", 16);
        public int CNoData = Integer.valueOf("6A88", 16);

    }

    public interface BaudRate {
        public byte r9600 = (byte) 0x11;
        public byte r19200 = (byte) 0x12;
        public byte r38400 = (byte) 0x13;
        public byte r115200 = (byte) 0x18;
        public byte r111600 = (byte) 0x95;
        public byte r223200 = (byte) 0x96;

    }

    public interface Commands {
        public byte[] CGetCardInfo = {(byte) 0x80, (byte) 0x14};
        public byte[] CCreateFile = {(byte) 0x00, (byte) 0xE0};
        public byte[] CDelFile = {(byte) 0x00, (byte) 0xE4};
        public byte[] CSelFile = {(byte) 0x00, (byte) 0xA4};
        public byte[] CGetResponse = {(byte) 0x00, (byte) 0xC0};

        public byte[] CPutKey = {(byte) 0x80, (byte) 0xDA};
        public byte[] CUpdateRecord = {(byte) 0x00, (byte) 0xDC};
        public byte[] CReadRecord = {(byte) 0x00, (byte) 0xB2};
        public byte[] CEncryptPlain = {(byte) 0x00, (byte) 0x2A};
        public byte[] CDecryptPlain = {(byte) 0x00, (byte) 0x2A};
        public byte[] CEncryptChainPlain = {(byte) 0x10, (byte) 0x2A};
        public byte[] CDecryptChainPlain = {(byte) 0x10, (byte) 0x2A};

        public byte[] CMNGSE = {(byte) 0x00, (byte) 0x22};
        public byte[] CClearCard = {(byte) 0x80, (byte) 0x30};
        public byte[] CAppendRecord = {(byte) 0x00, (byte) 0xE2};
        public byte[] CVerify = {(byte) 0x00, (byte) 0x20};
        public byte[] CChangePin = {(byte) 0x00, (byte) 0x24};
        public byte[] CResetPin = {(byte) 0x00, (byte) 0x2C};
        public byte[] CLogout = {(byte) 0x80, (byte) 0x2E};
        public byte[] CSetBaudRate = {(byte) 0x80, (byte) 0x32};
    }

    public interface Files {
        public byte[] MF = {(byte) 0x3F, (byte) 0x00};
        public byte[] DF1 = {(byte) 0x40, (byte) 0x00};
        public byte[] DF2 = {(byte) 0x41, (byte) 0x00};
        public byte EF1 = (byte) 0x01;
        public byte EF2 = (byte) 0x02;
        public byte SE = (byte) 0x03;
    }

    public interface FileType {
        public byte[] MF_3F = {(byte) 0x3F};
        public byte[] DF_38 = {(byte) 0x38};
        public byte[] EFTB_01 = {(byte) 0x01};
        public byte[] EFLF_02 = {(byte) 0x02};
        public byte[] EFLV_04 = {(byte) 0x04};
        public byte[] EFCY_06 = {(byte) 0x06};
        public byte[] EFKEY_0C = {(byte) 0x0C};
        public byte[] EFASYMKEY_09 = {(byte) 0x09};
    }

    public interface FileLifeCycle {
        public byte[] CREATION_01 = {(byte) 0x01};
        public byte[] ACTIVE_05 = {(byte) 0x05};
        public byte[] DEACTIVE_04 = {(byte) 0x04};
        public byte[] TERM_0C = {(byte) 0x0C};
    }

    public interface FileTags {
        public byte[] FILEID83 = {(byte) 0x83};
        public byte[] FILEDESC82 = {(byte) 0x82};
        public byte[] SIZE80 = {(byte) 0x80};
        public byte[] DFLONGNAME84 = {(byte) 0x84};
        public byte[] SHORTFILEID88 = {(byte) 0x88};
        public byte[] LIFECYLE8A = {(byte) 0x8A};
        public byte[] SAC8C = {(byte) 0x8C};
        public byte[] SAEAB = {(byte) 0xAB};
        public byte[] SEFILEID8D = {(byte) 0x8D};
    }

    public interface SAC {
        public byte ALLOW_ALWAYS = 0;
        public byte NEVER = (byte) 0xFF;
        public byte AT_LEAST_ONE_SEID_1 = (byte) 1;
        public byte AT_LEAST_ONE_SEID_2 = (byte) 2;
        public byte AT_LEAST_ONE_SEID_3 = (byte) 3;
        public byte AT_LEAST_ONE_SEID_4 = (byte) 4;
        public byte AT_LEAST_ONE_SEID_5 = (byte) 5;
        public byte AT_LEAST_ONE_SEID_6 = (byte) 6;
        public byte AT_LEAST_ONE_SEID_7 = (byte) 7;
        public byte AT_LEAST_ONE_SEID_8 = (byte) 8;
        public byte AT_LEAST_ONE_SEID_9 = (byte) 9;
        public byte AT_LEAST_ONE_SEID_10 = (byte) 10;
        public byte AT_LEAST_ONE_SEID_11 = (byte) 11;
        public byte AT_LEAST_ONE_SEID_12 = (byte) 12;
        public byte AT_LEAST_ONE_SEID_13 = (byte) 13;
        public byte AT_LEAST_ONE_SEID_14 = (byte) 14;

        public byte ALL_SEID_1 = (byte) 129;
        public byte ALL_SEID_2 = (byte) 130;
        public byte ALL_SEID_3 = (byte) 131;
        public byte ALL_SEID_4 = (byte) 132;
        public byte ALL_SEID_5 = (byte) 133;
        public byte ALL_SEID_6 = (byte) 134;
        public byte ALL_SEID_7 = (byte) 135;
        public byte ALL_SEID_8 = (byte) 136;
        public byte ALL_SEID_9 = (byte) 137;
        public byte ALL_SEID_10 = (byte) 138;
        public byte ALL_SEID_11 = (byte) 139;
        public byte ALL_SEID_12 = (byte) 140;
        public byte ALL_SEID_13 = (byte) 141;
        public byte ALL_SEID_14 = (byte) 142;
        public byte SM_ALL_SEID_1 = (byte) 193;
        public byte SM_ALL_SEID_2 = (byte) 194;
        public byte SM_ALL_SEID_3 = (byte) 195;
        public byte SM_ALL_SEID_4 = (byte) 196;
        public byte SM_ALL_SEID_5 = (byte) 197;
        public byte SM_ALL_SEID_6 = (byte) 198;
        public byte SM_ALL_SEID_7 = (byte) 199;
        public byte SM_ALL_SEID_8 = (byte) 200;
        public byte SM_ALL_SEID_9 = (byte) 201;
        public byte SM_ALL_SEID_10 = (byte) 202;
        public byte SM_ALL_SEID_11 = (byte) 203;
        public byte SM_ALL_SEID_12 = (byte) 204;
        public byte SM_ALL_SEID_13 = (byte) 205;
        public byte SM_ALL_SEID_14 = (byte) 206;

        public byte SM_AT_LEAST_ONE_SEID_1 = (byte) 65;
        public byte SM_AT_LEAST_ONE_SEID_2 = (byte) 66;
        public byte SM_AT_LEAST_ONE_SEID_3 = (byte) 67;
        public byte SM_AT_LEAST_ONE_SEID_4 = (byte) 68;
        public byte SM_AT_LEAST_ONE_SEID_5 = (byte) 69;
        public byte SM_AT_LEAST_ONE_SEID_6 = (byte) 70;
        public byte SM_AT_LEAST_ONE_SEID_7 = (byte) 71;
        public byte SM_AT_LEAST_ONE_SEID_8 = (byte) 72;
        public byte SM_AT_LEAST_ONE_SEID_9 = (byte) 73;
        public byte SM_AT_LEAST_ONE_SEID_10 = (byte) 74;
        public byte SM_AT_LEAST_ONE_SEID_11 = (byte) 75;
        public byte SM_AT_LEAST_ONE_SEID_12 = (byte) 76;
        public byte SM_AT_LEAST_ONE_SEID_13 = (byte) 77;
        public byte SM_AT_LEAST_ONE_SEID_14 = (byte) 78;
    }

    public static byte[] createSacForDF_MF(byte sc6, byte sc5, byte sc4, byte sc3, byte sc1, byte sc0) {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte amByte = createAMbyte(sc6, sc5, sc4, sc3, sc1, sc1, sc0);
        bos.write(amByte);
        if (sc6 != SAC.ALLOW_ALWAYS) {
            bos.write(sc6);
        }
        if (sc5 != SAC.ALLOW_ALWAYS) {
            bos.write(sc5);
        }
        if (sc4 != SAC.ALLOW_ALWAYS) {
            bos.write(sc4);
        }
        if (sc3 != SAC.ALLOW_ALWAYS) {
            bos.write(sc3);
        }
        if (sc1 != SAC.ALLOW_ALWAYS) {
            bos.write(sc1);
        }
        if (sc1 != SAC.ALLOW_ALWAYS) {
            bos.write(sc1);
        }
        if (sc0 == SAC.ALLOW_ALWAYS) {
        } else {
            bos.write(sc0);
        }

        return bos.toByteArray();
    }

    public static byte[] createSacForEF(byte sc6, byte sc5, byte sc4, byte sc3, byte sc1, byte sc0) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte amByte = createAMbyte(sc6, sc5, sc4, sc3, SAC.ALLOW_ALWAYS, sc1, sc0);
        bos.write(amByte);
        if (sc6 != SAC.ALLOW_ALWAYS) {
            bos.write(sc6);
        }
        if (sc5 != SAC.ALLOW_ALWAYS) {
            bos.write(sc5);
        }
        if (sc4 != SAC.ALLOW_ALWAYS) {
            bos.write(sc4);
        }
        if (sc3 != SAC.ALLOW_ALWAYS) {
            bos.write(sc3);
        }
        if (sc1 != SAC.ALLOW_ALWAYS) {
            bos.write(sc1);
        }
        if (sc0 == SAC.ALLOW_ALWAYS) {
        } else {
            bos.write(sc0);
        }

        return bos.toByteArray();
    }

    public static byte[] createSacForKey(byte sc6, byte sc5, byte sc4, byte sc3, byte sc2, byte sc1, byte sc0) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte amByte = createAMbyte(sc6, sc5, sc4, sc3, sc2, sc1, sc0);
        bos.write(amByte);
        if (sc6 != SAC.ALLOW_ALWAYS) {
            bos.write(sc6);
        }
        if (sc5 != SAC.ALLOW_ALWAYS) {
            bos.write(sc5);
        }
        if (sc4 != SAC.ALLOW_ALWAYS) {
            bos.write(sc4);
        }
        if (sc3 != SAC.ALLOW_ALWAYS) {
            bos.write(sc3);
        }
        if (sc2 != SAC.ALLOW_ALWAYS) {
            bos.write(sc2);
        }
        if (sc1 != SAC.ALLOW_ALWAYS) {
            bos.write(sc1);
        }
        if (sc0 == SAC.ALLOW_ALWAYS) {
        } else {
            bos.write(sc0);
        }

        return bos.toByteArray();
    }

    public static byte[] createSacForSE(byte sc6, byte sc5, byte sc4, byte sc3, byte sc2, byte sc1, byte sc0) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte amByte = createAMbyte(sc6, sc5, sc4, sc3, sc2, sc1, sc0);
        bos.write(amByte);

        if (sc6 != SAC.ALLOW_ALWAYS) {
            bos.write(sc6);
        }
        if (sc5 != SAC.ALLOW_ALWAYS) {
            bos.write(sc5);
        }
        if (sc4 != SAC.ALLOW_ALWAYS) {
            bos.write(sc4);
        }
        if (sc3 != SAC.ALLOW_ALWAYS) {
            bos.write(sc3);
        }
        if (sc2 != SAC.ALLOW_ALWAYS) {
            bos.write(sc2);
        }
        if (sc1 != SAC.ALLOW_ALWAYS) {
            bos.write(sc1);
        }
        if (sc0 == SAC.ALLOW_ALWAYS) {
        } else {
            bos.write(sc0);
        }
        return bos.toByteArray();
    }

    private static byte createAMbyte(byte sc6, byte sc5, byte sc4, byte sc3, byte sc2, byte sc1, byte sc0) {
        byte amByte = (byte) 127;
        if (sc6 == SAC.ALLOW_ALWAYS) {
            amByte = ((byte) (amByte - 64));
        }
        if (sc5 == SAC.ALLOW_ALWAYS) {
            amByte = ((byte) (amByte - 32));
        }
        if (sc4 == SAC.ALLOW_ALWAYS) {
            amByte = ((byte) (amByte - 16));
        }
        if (sc3 == SAC.ALLOW_ALWAYS) {
            amByte = ((byte) (amByte - 8));
        }
        if (sc2 == SAC.ALLOW_ALWAYS) {
            amByte = ((byte) (amByte - 4));
        }
        if (sc1 == SAC.ALLOW_ALWAYS) {
            amByte = ((byte) (amByte - 2));
        }
        if (sc0 != SAC.ALLOW_ALWAYS) {
        } else {
            amByte = ((byte) (amByte - 1));
        }

        return amByte;
    }

    TerminalFactory tfactory = null;
    // SCM Microsystems Inc. SCR33x USB Smart Card Reader 0
    String terminalName;
    String terminalProto;
    char[] pin;
    long waitForCardMs = 10000;
    CardTerminal ct;

    // Card card;

    public ACOS5Service() {
        tfactory = TerminalFactory.getDefault();
    }

    public synchronized Card initCardConnection() throws Exception {

        Card card = null;

        if (ct == null) {
            if (Misc.isNullOrEmpty(terminalName)) {
                CardTerminals terminalList = tfactory.terminals();
                if (terminalList.list().size() > 0) {
                    ct = terminalList.list().get(0);
                } else {
                    throw new RuntimeException("Card terminal not found");
                }
            } else {
                ct = tfactory.terminals().getTerminal(terminalName);
            }

            boolean cardInPlace = ct.isCardPresent() || ct.waitForCardPresent(waitForCardMs);
            if (!cardInPlace) {
                throw new RuntimeException("Card not in place in " + ct.getName());
            }
        }

        card = ct.connect(Misc.isNullOrEmpty(terminalProto) ? "*" : terminalProto);

        return card;
    }

    public synchronized void disposeCardConnection(Card card) throws Exception {
        // seems this is not good to do
        // and can confuse other parallele connections..
        // and may be causing this Caused by: sun.security.smartcardio.PCSCException: SCARD_E_PROTO_MISMATCH
        // when a exclusivity had been just ended and other got another exclusivity
        // but the other now calls this..
        /*
         * if (card != null) { card.disconnect(false); card = null; }
         */

    }

    public synchronized void beginExclusive(Card card) throws Exception {

        int failed = 0;
        while (true) {
            try {
                card.beginExclusive();
                return;

            } catch (Exception e) {
                failed++;
                if (failed > 2) {
                    log.error("beginExclusive failed after " + failed + " attempts", e);
                    throw e;
                }

                Thread.sleep(500);
            }
        }

    }

    // seems this way all get their exclusive finally
    // as it obtains the card connection and exclusive same time
    // if one of them failing its forth retry some time later
    // again once other process had released its exclusivity
    // exclusivity is needed that processes or threads do not screw up the
    // other thread/process command sequence
    public synchronized Card beginExclusive() throws Exception {
        int failed = 0;
        while (true) {
            try {
                Card card = initCardConnection();
                card.beginExclusive();
                if (debug) {
                    log.info("beginExclusive:" + card.toString());
                }

                return card;

            } catch (Exception e) {
                failed++;
                if (failed > 19) {
                    log.error("beginExclusive failed after " + failed + " attempts", e);
                    throw e;
                }
                Thread.sleep(200 + failed * 10); // shorten wait by failed
            }
        }
    }

    public byte[] getCardSerial() throws Exception {
        Card card = null;
        try {
            card = this.beginExclusive();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(Commands.CGetCardInfo);
            bos.write((byte) 0x00);
            bos.write((byte) 0x00);
            bos.write((byte) 0x06);
            CommandAPDU gs = new CommandAPDU(bos.toByteArray());
            CardChannel channel = card.getBasicChannel();
            ResponseAPDU rp = channel.transmit(gs);
            if (ResultCodes.COK == rp.getSW()) {
                return rp.getData();
            }

            throw new RuntimeException("Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (card != null) {
                    card.endExclusive();
                }
            } catch (Exception ex) {
            }
            this.disposeCardConnection(card);
        }

    }

    public int clearCard() throws Exception {
        Card card = initCardConnection();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CClearCard);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);
        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);

        disposeCardConnection(card);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    // only for old acos5-32 does not work with acos5-64
    public int setBaudRate(byte newRate) throws Exception {
        Card card = initCardConnection();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CSetBaudRate);
        bos.write((byte) 0x00);
        bos.write(newRate);
        bos.write((byte) 0x00);
        if (debug) {
            log.info("setBaudRate: " + HexUtils.toString(bos.toByteArray()));
        }
        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);

        disposeCardConnection(card);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int logout(byte pinid, Card card) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CLogout);
        bos.write((byte) 0x00);
        bos.write(pinid);
        bos.write((byte) 0x00);
        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int getCardFilesCount(Card card) throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CGetCardInfo);
        bos.write((byte) 0x01);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);
        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        disposeCardConnection(card);
        if (ResultCodes.COKSw1 == rp.getSW1()) {
            return rp.getSW2();
        }

        throw new RuntimeException("Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public void installAESKey(byte[] aesKey) throws Exception {
        Card card = null;
        boolean exclusive = false;
        try {
            card = this.beginExclusive();
            exclusive = true;
            selectMF(card);
            selectFile(HexUtils.fromString(this.aesKeyFolder), card);
            selectFile(HexUtils.fromString(this.aesKeyRecord), card);
            if (pin != null) {
                verify((byte) 0x81, new String(pin).getBytes(), card);
            }

            updateRecord(HexUtils.fromString("8202FFFF22" + HexUtils.toString(aesKey)), true, card);
            if (pin != null) {
                logout((byte) 0x81, card);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (card != null && exclusive) {
                    card.endExclusive();
                }
            } catch (Exception dc) {
            }
            disposeCardConnection(card);
        }

    }

    public void wipeoutAESKey() throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        Card card = null;
        boolean exclusive = false;
        try {
            card = beginExclusive();
            exclusive = true;
            selectMF(card);
            selectFile(HexUtils.fromString(this.aesKeyFolder), card);
            selectFile(HexUtils.fromString(this.aesKeyRecord), card);
            if (pin != null) {
                verify((byte) 0x81, new String(pin).getBytes(), card);
            }

            updateRecord(new byte[5 + 32], true, card);
            if (pin != null) {
                logout((byte) 0x81, card);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                if (card != null && exclusive) {
                    card.endExclusive();
                }
            } catch (Exception dc) {
            }

            disposeCardConnection(card);
        }

    }

    public int craeteMF(Card card) throws Exception {
        return craeteMF(false, card);
    }

    public int craeteMF(boolean withsesac, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CCreateFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();

        bosd.write((byte) 0x82); // tag
        bosd.write((byte) 0x02); // len

        bosd.write((byte) 0x3F); // FDB MF
        bosd.write((byte) 0xFF); // DCB Data code Byte (arcos does not use it set to FF)

        bosd.write(FileTags.FILEID83); // File id tag
        bosd.write((byte) 0x02); // len
        bosd.write(Files.MF); // Master file
        if (withsesac) {
            bosd.write(FileTags.SEFILEID8D); // SE TAG
            bosd.write((byte) 0x02); // len
            bosd.write(new byte[]{(byte) 0x40, (byte) 0x03}); // len

            bosd.write(FileTags.SAC8C); // SAC TAG
            byte[] sacBytes = createSacForDF_MF(SAC.NEVER, SAC.NEVER, SAC.ALLOW_ALWAYS, SAC.NEVER, SAC.SM_ALL_SEID_1, SAC.SM_ALL_SEID_1);
            bosd.write((byte) sacBytes.length); // len
            bosd.write(sacBytes);

        }

        bos.write((byte) bosd.size() + 2);
        bos.write((byte) 0x62);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("Create MF. Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int verify(byte pinId, byte[] pin, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CVerify);
        bos.write((byte) 0x00);
        bos.write(pinId);
        bos.write((byte) pin.length);
        bos.write(pin);

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        if (debug) {
            log.info("verify: " + HexUtils.toString(bos.toByteArray()));
        }
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        if (rp.getSW() >= 0x63C0 && rp.getSW() <= 0x63C9) {
            String left = Integer.toHexString(rp.getSW()).toUpperCase();
            left = left.substring(left.length() - 1);
            throw new RuntimeException("Verify failed, Invalid pin, retries left " + left + " (" + rp.getSW() + " hex "
                    + Integer.toHexString(rp.getSW()).toUpperCase());
        }

        throw new RuntimeException("Verify failed, Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int changePin(byte pinId, byte[] pin, Card card) throws Exception {

        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CChangePin);
        bos.write((byte) 0x01);
        bos.write(pinId);
        bos.write((byte) pin.length);
        bos.write(pin);

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        if (debug) {
            log.info("changepin: " + HexUtils.toString(bos.toByteArray()));
        }
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("Change PIN failed, Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int resetPin(byte pinId, byte[] rsCode, byte[] pin, Card card) throws Exception {
        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CResetPin);
        bos.write((byte) 0x00);
        bos.write(pinId);
        bos.write((byte) (rsCode.length + pin.length));
        bos.write(rsCode);
        bos.write(pin);

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        if (debug) {
            log.info("changepin: " + HexUtils.toString(bos.toByteArray()));
        }
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("Reset PIN failed, Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int setSecurityEnv(byte seNr, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CMNGSE);
        bos.write((byte) 0xF3);
        bos.write(seNr);
        bos.write((byte) 0x00);

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("setSecurityEnv:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int setSecurityEnv(byte crtTag, byte[] crt, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CMNGSE);
        bos.write((byte) 0x01);
        bos.write(crtTag);
        bos.write((byte) crt.length);
        bos.write(crt);

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        if (debug) {
            log.info("SecurityEnv: " + HexUtils.toString(bos.toByteArray()));
        }
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("setSecurityEnv: Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int craeteDF(byte[] df, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CCreateFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();

        bosd.write(FileTags.FILEDESC82); // tag
        bosd.write((byte) 0x01); // len
        bosd.write(FileType.DF_38); //

        bosd.write(FileTags.FILEID83); // File id tag
        bosd.write((byte) 0x02); // len
        bosd.write(df); // 4000

        bosd.write(FileTags.DFLONGNAME84); // File id tag
        bosd.write((byte) 0x10); // len
        byte[] ln = new byte[16];
        ln[0] = df[0];
        ln[1] = df[1];
        bosd.write(ln);

        bosd.write(FileTags.LIFECYLE8A);
        bosd.write((byte) 0x01); // len
        bosd.write(FileLifeCycle.CREATION_01);

        bosd.write(FileTags.SAC8C);
        // bosd.write((byte)0x08); // len
        // bosd.write(new byte[] { (byte)0x7F,(byte)0x03,(byte)0x03,(byte)0x03,(byte)0x03,(byte)0xFF,(byte)0xFF,(byte)0x03 } );
        bosd.write(HexUtils.fromString("077D020202FFFF02"));

        bosd.write(FileTags.SAEAB);
        // bosd.write((byte)0x06); // len
        // bosd.write(new byte[] { (byte)0x86,(byte)0x02,(byte)0x22,(byte)0x2A,(byte)0x97,(byte)0x00 } );
        bosd.write(HexUtils.fromString("16860222F29700840122A00B9E0101A405830181950108"));

        bosd.write(FileTags.SEFILEID8D);
        bosd.write((byte) 0x02); // len
        bosd.write(new byte[]{df[0], Files.SE});

        bos.write((byte) bosd.size() + 2);
        bos.write((byte) 0x62);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("craeteDF: " + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("craeteDF:Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int craeteSE(byte[] df, byte[] sac, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CCreateFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();

        bosd.write(FileTags.FILEDESC82); // tag
        bosd.write((byte) 0x05); // len
        bosd.write(FileType.EFKEY_0C); // FD
        bosd.write((byte) 0x01); // DCB
        bosd.write((byte) 0x00); // 00
        bosd.write((byte) 0x27); // MRL
        bosd.write((byte) 0x05); // NOR

        bosd.write(FileTags.FILEID83); // File id tag
        bosd.write((byte) 0x02); // len
        bosd.write(new byte[]{df[0], Files.SE}); // 4003

        bosd.write(FileTags.SHORTFILEID88); // SFI File id tag
        bosd.write((byte) 0x01); // len
        bosd.write(Files.SE); // 03

        bosd.write(FileTags.LIFECYLE8A);
        bosd.write((byte) 0x01); // len
        bosd.write(FileLifeCycle.CREATION_01);

        bosd.write(FileTags.SAC8C);
        // bosd.write((byte)0x06); // len
        // bosd.write(new byte[] { (byte)0x6B,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF } );
        bosd.write((byte) sac.length); // len
        bosd.write(sac);

        bos.write((byte) bosd.size() + 2);
        bos.write((byte) 0x62);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("craeteSEFile: " + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("craeteSE: Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int craeteKeyEF(byte[] df, byte mrl, byte[] sac, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CCreateFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();

        bosd.write(FileTags.FILEDESC82); // tag
        bosd.write((byte) 0x05); // len
        bosd.write(FileType.EFKEY_0C); // FD
        bosd.write((byte) 0x01); // DCB
        bosd.write((byte) 0x00); // 00
        // bosd.write((byte)0x15); // MRL 16 byte key
        bosd.write(mrl); // MRL 32 byte key
        bosd.write((byte) 0x04); // NOR

        bosd.write(FileTags.FILEID83); // File id tag
        bosd.write((byte) 0x02); // len
        // bosd.write( new byte[] {df[0], Files.EF2}); //4002
        bosd.write(df); // 4002

        bosd.write(FileTags.SHORTFILEID88); // SFI short File id tag
        bosd.write((byte) 0x01); // len
        // bosd.write( Files.EF2);
        bosd.write(df[1]);

        bosd.write(FileTags.LIFECYLE8A);
        bosd.write((byte) 0x01); // len
        bosd.write(FileLifeCycle.CREATION_01);

        bosd.write(FileTags.SAC8C);
        bosd.write((byte) sac.length); // len
        bosd.write(sac);

        bos.write((byte) bosd.size() + 2);
        bos.write((byte) 0x62);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("craeteEF2: " + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("craeteKeyEF:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int craetePinEF(byte[] df, byte[] sac, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CCreateFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();

        bosd.write(FileTags.FILEDESC82); // tag
        bosd.write((byte) 0x05); // len
        bosd.write(FileType.EFKEY_0C); // FD
        bosd.write((byte) 0x01); // DCB
        bosd.write((byte) 0x00); // 00
        bosd.write((byte) 0x12); // MRL 32 byte key
        bosd.write((byte) 0x01); // NOR

        bosd.write(FileTags.FILEID83); // File id tag
        bosd.write((byte) 0x02); // len
        bosd.write(df); // 4001

        bosd.write(FileTags.SHORTFILEID88); // short File id tag
        bosd.write((byte) 0x01); // len
        bosd.write(df[1]); // Files.EF1); // SFI

        bosd.write(FileTags.LIFECYLE8A);
        bosd.write((byte) 0x01); // len
        bosd.write(FileLifeCycle.CREATION_01);

        bosd.write(FileTags.SAC8C);
        bosd.write((byte) sac.length); // len
        bosd.write(sac);

        bos.write((byte) bosd.size() + 2);
        bos.write((byte) 0x62);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("craeteEF1: " + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("craetePinEF:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int selectMF(Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CSelFile);
        bos.write((byte) 0x00); // for fileid
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        if (debug) {
            log.info("selectMF:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COKSw1 == rp.getSW1() || ResultCodes.COKgetDataSW1 == rp.getSW1()) {
            return rp.getSW2();
        }

        throw new RuntimeException("selectMF:Card returned error code " + rp.getSW() + " hex " + Integer.toHexString(rp.getSW()).toUpperCase());

    }

    public int selectFile(byte[] fileId, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        initCardConnection();
        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CSelFile);
        bos.write((byte) 0x00); // for fileid
        bos.write((byte) 0x00);
        bos.write((byte) 0x02);
        bos.write(fileId);

        if (debug) {
            log.info("selectFile:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COKSw1 == rp.getSW1() || ResultCodes.COKgetDataSW1 == rp.getSW1()) {
            return rp.getSW2();
        }

        throw new RuntimeException("selectFile:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());

    }

    public byte[] getResponse(byte numOfBytes, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CGetResponse);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);
        bos.write(numOfBytes);

        if (debug) {
            log.info("getResponse:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COKSw1 == rp.getSW1() || ResultCodes.COKgetDataSW1 == rp.getSW1()) {
            return rp.getData();
        }
        if (ResultCodes.CNoData == rp.getSW()) {
            return null;
        }

        throw new RuntimeException("getResponse:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());

    }

    public int putKeyToFile(byte[] keyData, boolean sym, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CPutKey);
        bos.write(sym ? (byte) 0x01 : (byte) 0x00); // sym
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();
        bosd.write(keyData);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("putKeyToFile:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("putKeyToFile:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int updateRecord(byte[] recData, boolean first, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        // create MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CUpdateRecord);
        bos.write((byte) 0x00);
        bos.write(first ? (byte) 0x00 : (byte) 0x02);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();
        bosd.write(recData);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("updateRecord:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("updateRecord:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public byte[] readRecord(boolean first, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }
        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CReadRecord);
        bos.write((byte) 0x00);
        bos.write(first ? (byte) 0x00 : (byte) 0x02);

        if (debug) {
            log.info("readRecord:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getBytes();
        }

        throw new RuntimeException("readRecord: Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public int appendRecord(byte[] recData, Card card) throws Exception {

        // crete MF CLA INS P1 P2 P3 len FCP =0x62
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CAppendRecord);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        ByteArrayOutputStream bosd = new ByteArrayOutputStream();
        bosd.write(recData);
        bos.write((byte) (bosd.size()));
        bos.write(bosd.toByteArray());
        if (debug) {
            log.info("appendRecord:" + HexUtils.toString(bos.toByteArray()));
        }

        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("appendRecord:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());
    }

    public byte[] encryptPlain(byte[] recData, Card card) throws Exception {
        return encryptPlain(recData, true, card);
    }

    public byte[] decryptPlain(byte[] recData, Card card) throws Exception {
        return encryptPlain(recData, false, card);
    }

    public byte[] encryptPlain(byte[] recData, boolean encypt, Card card) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        int maxLen = 240; // 240; //15*16; // multiple of 16
        if (recData.length > maxLen) {
            // cleanup possible previous failed or incomplted chaining
            encryptPlain(new byte[16], encypt, card);

            // this.getResponse((byte)240);
            ByteArrayOutputStream bosres = new ByteArrayOutputStream();
            for (int i = 0; i < recData.length; i += maxLen) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                if (i + maxLen < recData.length) {
                    bos.write(Commands.CEncryptChainPlain);
                } else {
                    bos.write(Commands.CEncryptPlain);
                }

                if (encypt) {
                    bos.write((byte) 0x84);
                    bos.write((byte) 0x80);
                } else {
                    bos.write((byte) 0x80);
                    bos.write((byte) 0x84);
                }

                ByteArrayOutputStream bosd = new ByteArrayOutputStream();
                bosd.write(recData, i, i + maxLen > recData.length ? recData.length - i : maxLen); // .write(recData);
                bos.write((byte) bosd.size());
                bos.write(bosd.toByteArray());
                if (debug) {
                    log.info("encryptPlain:" + HexUtils.toString(bos.toByteArray()));
                }

                CommandAPDU gs = new CommandAPDU(bos.toByteArray());
                CardChannel channel = card.getBasicChannel();
                ResponseAPDU rp = channel.transmit(gs);

                if (ResultCodes.COKgetDataSW1 == rp.getSW1()) {
                    // int numOfBytes=rp.getSW2();
                    // byte[] data=this.getResponse((byte)numOfBytes);
                    bosres.write(rp.getData());
                } else if (ResultCodes.COK == rp.getSW()) {
                    bosres.write(rp.getData());
                } else {
                    throw new RuntimeException("encryptPlain chained, Card returned error code " + rp.getSW() + " hex "
                            + Integer.toHexString(rp.getSW()).toUpperCase());
                }
            } // for
            return bosres.toByteArray();
        } else {
            // create MF CLA INS P1 P2 P3 len FCP =0x62
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(Commands.CEncryptPlain);

            if (encypt) {
                bos.write((byte) 0x84);
                bos.write((byte) 0x80);
            } else {
                bos.write((byte) 0x80);
                bos.write((byte) 0x84);
            }

            ByteArrayOutputStream bosd = new ByteArrayOutputStream();
            bosd.write(recData);
            bos.write((byte) (bosd.size()));
            bos.write(bosd.toByteArray());
            if (debug) {
                log.info("encryptPlain:" + HexUtils.toString(bos.toByteArray()));
            }

            CommandAPDU gs = new CommandAPDU(bos.toByteArray());
            CardChannel channel = card.getBasicChannel();
            ResponseAPDU rp = channel.transmit(gs);
            if (ResultCodes.COK == rp.getSW()) {
                return rp.getData();
            }

            throw new RuntimeException("encryptPlain, Card returned error code " + rp.getSW() + " hex "
                    + Integer.toHexString(rp.getSW()).toUpperCase());
        }

    }

    /*
     * public byte[] decryptPlain(byte[] recData) throws Exception { initCardConnection();
     *
     * // crete MF CLA INS P1 P2 P3 len FCP =0x62 ByteArrayOutputStream bos=new ByteArrayOutputStream(); bos.write(Commands.CDecryptPlain);
     * bos.write((byte)0x80); bos.write((byte)0x84);
     *
     * ByteArrayOutputStream bosd=new ByteArrayOutputStream(); bosd.write(recData); bos.write((byte)(bosd.size()));
     * bos.write(bosd.toByteArray()); if (debug) log.info("decryptPlain:"+Hex.toString(bos.toByteArray()));
     *
     * CommandAPDU gs=new CommandAPDU(bos.toByteArray()); CardChannel channel = card.getBasicChannel(); ResponseAPDU rp=channel.transmit(gs); if
     * (ResultCodes.COK==rp.getSW()) return rp.getData();
     *
     * throw new RuntimeException("Card returned error code "+rp.getSW()+" hex "+Integer.toHexString(rp.getSW()).toUpperCase()); }
     */

    public int deleteFile(byte[] fileId, Card card) throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CDelFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);

        bos.write((byte) 0x02);
        bos.write(fileId); // Master file
        if (debug) {
            log.info("deleteFile:" + HexUtils.toString(bos.toByteArray()));
        }
        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("deleteFile:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());

    }

    public int deleteCurrentFile(Card card) throws Exception {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(Commands.CDelFile);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);
        bos.write((byte) 0x00);
        if (debug) {
            log.info("deleteFile:" + HexUtils.toString(bos.toByteArray()));
        }
        CommandAPDU gs = new CommandAPDU(bos.toByteArray());
        CardChannel channel = card.getBasicChannel();
        ResponseAPDU rp = channel.transmit(gs);
        if (ResultCodes.COK == rp.getSW()) {
            return rp.getSW();
        }

        throw new RuntimeException("deleteCurrentFile:Card returned error code " + rp.getSW() + " hex "
                + Integer.toHexString(rp.getSW()).toUpperCase());

    }

    @Override
    public synchronized Key generateHSMDataEncryptionKey() throws Exception {
        byte[] randomKey = KeyServiceBase.genKey(32);
        byte[] encKey = this.encryptDataInHSMWithMaster(randomKey, true);
        String kcv = KeyServiceBase.calculateAESKeyCheckValue(randomKey);
        Key key = new ACOSDataKey(encKey, HexUtils.fromString(kcv));
        return key;
    }

    @Override
    public synchronized byte[] encryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception {
        try {
            byte[] plainKey = this.encryptDataInHSMWithMaster(hsmDataEncryptionKey, false);
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            SecretKeySpec keySpec = new SecretKeySpec(plainKey, "AES");
            encryptCipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
            byte[] result = encryptCipher.doFinal(data);

            return result;
        } catch (Exception e) {
            throw e;
        }
    }

    public synchronized byte[] encryptDataInHSMWithMaster(byte[] data, boolean encrypt) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        boolean exclusive = false;
        Card card = null;
        try {
            card = this.beginExclusive();
            exclusive = true;
            this.selectMF(card);
            this.selectFile(HexUtils.fromString(this.aesKeyFolder), card);
            this.selectFile(HexUtils.fromString(this.aesKeyRecord), card);
            if (this.pin != null) {
                this.verify((byte) 0x81, new String(pin).getBytes(), card);
            }

            this.setSecurityEnv((byte) 0xB8, HexUtils.fromString("830182" + "9501C0" + "800106"), card); // key, use, alg 06,07
            log.info("encryptDataInHSMWithMaster: operation " + (encrypt ? " encrypt" : "decrypt") + " of " + data.length
                    + " bytes by system user: " + System.getProperty("user.name"));
            byte[] result = this.encryptPlain(data, encrypt, card);

            if (this.pin != null) {
                this.logout((byte) 0x81, card);
            }

            return result;
        } catch (Exception e) {
            throw e;
        } finally {

            try {
                if (card != null && exclusive) {
                    card.endExclusive();
                }
            } catch (Exception ex) {
            }
            this.disposeCardConnection(card);
        }
    }

    @Override
    public synchronized byte[] decryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception {
        try {
            byte[] plainKey = this.encryptDataInHSMWithMaster(hsmDataEncryptionKey, false);
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            SecretKeySpec keySpec = new SecretKeySpec(plainKey, "AES");
            encryptCipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
            byte[] result = encryptCipher.doFinal(data);

            return result;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void initializeHSM(String config) {
        if (config != null && config.length() > 0) {
            String[] settings = Misc.split(config, ";");
            for (int i = 0; settings != null && i < settings.length; i++) {
                String[] spair = Misc.splitTwo(settings[i], "=");
                if (spair != null && spair.length == 2) {
                    if ("terminalName".equals(spair[0])) {
                        this.terminalName = spair[1];
                    }
                    if ("terminalProto".equals(spair[0])) {
                        this.terminalProto = spair[1];
                    }
                    if ("pin".equals(spair[0]) && spair[1] != null) {
                        this.pin = spair[1].toCharArray();
                    }

                }
            }
        }

    }

    public void setPin(String pin) {
        if (pin != null) {
            this.pin = pin.toCharArray();
        } else {
            this.pin = null;
        }
    }

    public String getMasterKeyCheckValue() throws Exception {
        byte[] zeroblock = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        byte[] kcvfull = this.encryptDataInHSMWithMaster(zeroblock, true);
        String kcvffullHex = HexUtils.toString(kcvfull);

        return kcvffullHex.substring(kcvffullHex.length() - 6);
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public String getTerminalProto() {
        return terminalProto;
    }

    public void setTerminalProto(String terminalProto) {
        this.terminalProto = terminalProto;
    }

    public void setPin(char[] pin) {
        this.pin = pin;
    }

    @Override
    public String getHardwareSerial() throws Exception {
        return HexUtils.toString(getCardSerial());
    }

    @Override
    public KeyPair generateRSAdecryptAndSign(int keyLength) throws HsmException {
        throw new HsmException("Hardware RSA is unsupported/unimplemented.");
    }

    @Override
    public byte[] rsaPrivateDecrypt(byte rsaPrivateKey[], byte encryptedData[], RSADecryptPadMode padMode) throws HsmException {
        throw new HsmException("Hardware RSA is unsupported/unimplemented.");
    }

    @Override
    public PrivateKey toRSAprivateKey(byte[] privateKeyBytes) throws Exception {
        throw new HsmException("Hardware RSA is unsupported/unimplemented.");
    }

    @Override
    public ContentSigner getContentSigner(final byte[] rsaPrivateKeyBytes, final String signatureAlgorithm) throws HsmException{
        throw new HsmException("Hardware RSA is unsupported/unimplemented.");
    }

    @Override
    public byte[] ecPrivateDecrypt(byte ecPrivateKey[], byte encryptedData[], String cipherTransformation)
            throws HsmException {

        throw new HsmException("Hardware Elliptic Curve is unsupported/unimplemented.");
    }

}
