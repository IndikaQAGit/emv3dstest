/*
 * Copyright (C) 2014 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 13.03.2014
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.hsm;

import java.security.SecurityPermission;

public interface Permissions {

    SecurityPermission MDMSecurityPermission = new SecurityPermission("MDMSecurityPermission");
}