package com.modirum.ds.hsm.support;

/**
 * A data holder class for LMKs present in an HSM.
 */
public class LMKData {

    private String lmkID; // lmk id in an HSM
    private char scheme; // variant 'V' or key block 'K'
    private String kcv; // key check value

    public LMKData(String lmkID, char scheme, String kcv) {
        this.lmkID = lmkID;
        this.scheme = scheme;
        this.kcv = kcv;
    }

    public String getLmkID() {
        return lmkID;
    }

    public char getScheme() {
        return scheme;
    }

    public String getKcv() {
        return kcv;
    }
}
