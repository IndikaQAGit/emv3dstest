package com.modirum.ds.hsm;

import com.modirum.ds.hsm.support.HSMDataKey;
import com.modirum.ds.hsm.support.HSMUtil;
import com.modirum.ds.hsm.support.HsmException;
import com.modirum.ds.hsm.support.LMKData;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import com.modirum.ds.hsm.support.SignAlgIdentifier;
import com.modirum.ds.jce.MdRSAPrivateKey;
import com.modirum.ds.jce.MdRSAPublicKey;
import com.modirum.ds.utils.Base64;
import com.modirum.ds.utils.HexUtils;
import com.modirum.ds.utils.Misc;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implements parts of Thales Payshield 8000 HSM interface for DS.
 * - Allows HSM usage with 'Variant LMK' only
 * - Uses 3DES key for data encryption.
 *
 * @deprecated HSM 8000 should not be used in production because it doesn't support AES data keys with AES LMK Keyblock.
 * Use HSM 9000 ok 10k instead. This implementation can be used for backwards compatibility in test environments.
 */
@Deprecated
public class HSMR8000Service implements HSMDevice {

    private transient static final Logger log = LoggerFactory.getLogger(HSMR8000Service.class);

    // const
    public static String ETX = "";
    public static String MESSAGE_TRAILER = "19";
    public static byte[] HEADER = new byte[]{0x30, 0x31, 0x32, 0x33};

    public static final byte[] EMPTY = new byte[0];
    public static final String IV_HEX_STR = "0000000000000000";
    public static final byte[] IV_HEX = IV_HEX_STR.getBytes(StandardCharsets.ISO_8859_1);
    public static final String KEY_TYPE_DEK = "00B";
    public static final int THALES_DATA_BLOCK_SIZE = 8; // thales data block size
    public static final int PKCS5_BLOCK_SIZE = 8; // pkcs5 block size
    final static byte[] ok = "00".getBytes(StandardCharsets.ISO_8859_1);

    private int MAX_RESEND = 5;
    public int timeout = 30 * 1000;
    InetAddress address = null;
    // start of the message char, not used w/ tcp/ip
    public static String STX = "";
    // end of the message string, not used w/ tcp/ip
    public String host;
    public int port;
    public boolean tcp;
    protected String lmkKB = null;
    protected String lmkVA = null;
    private String lmkKCV = null; // Setup and used once per instance lifetime (to determine lmkVA/lmkKB);
    protected int socketMaxInactivity = 0;
    protected long socketLastActivity = 0;
    protected AtomicInteger reqNo = new AtomicInteger();
    Socket socket;
    InputStream is = null;
    OutputStream os = null;
    boolean usePad = true;

    public String getLMKKB() {
        return lmkKB;
    }

    public String getLMKVA() {
        return lmkVA;
    }

    /**
     * Used to return key creation values with their checksums
     */
    public static class KeyResult {

        private String key;
        private String checkValue;

        public KeyResult(String key, String cvc) {
            this.key = key;
            this.checkValue = cvc;
        }

        public String getKey() {
            return key;
        }

        public String checkValue() {
            return checkValue;
        }
    }

    public Socket newSocket(String host, int port) throws Exception {
        Socket s = new Socket(host, port);
        s.setTrafficClass(0x10);
        s.setTcpNoDelay(true);
        return s;
    }

    public HSMR8000Service(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public HSMR8000Service() {

    }

    /* ==================================== */
    /* = Data reading from tcp connection = */
    /* ==================================== */
    public int readInt(InputStream in, int size) throws Exception {
        // int[] bytes = new int[size];
        byte[] b = new byte[size];
        // int ret =
        in.read(b);
        return (b[0] & 0xFF) * 256 + (b[1] & 0xFF);
    }

    public int readInt(byte[] b) {
        return (b[0] & 0xFF) * 256 + (b[1] & 0xFF);
    }

    private static byte[] readBytes(InputStream in, int len) throws Exception {
        byte[] bytes = new byte[len];
        int ret = in.read(bytes);
        if (ret != len) {
            throw new Exception("Couldn't read integer, got -1 from read");
        }
        return bytes;
    }

    /**
     * Converts decimal integer to hexadecimal. Result is represented as a byte array (2 bytes).
     * @param commandLength HSM command length
     * @return Hexadecimal number as byte array.
     * @throws IllegalArgumentException if HSM command length value cannot fit 2 bytes, or is negative.
     */
    public static byte[] getHsmCommandLength(int commandLength) throws IllegalArgumentException {
        if (commandLength < 0 || commandLength > 65535) {
            String msg = String.format("Generated HSM command length is invalid (%d). Length value must be a positive integer and fit into 2 bytes.", commandLength);
            throw new IllegalArgumentException(msg);
        }

        return new byte[] {
                (byte) (commandLength >>> 8),
                (byte) commandLength
        };
    }

    public static byte[] concat(byte[] a, byte[] b, byte[] c, byte[] d, byte[] e) {
        int len = a.length + b.length + c.length + d.length + e.length;
        byte[] res = new byte[len];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        System.arraycopy(c, 0, res, a.length + b.length, c.length);
        System.arraycopy(d, 0, res, a.length + b.length + c.length, d.length);
        System.arraycopy(e, 0, res, a.length + b.length + c.length + d.length, e.length);
        return res;
    }


    public static byte[] concat(byte[] a, byte[] b, byte[] c, byte[] d) {
        int len = a.length + b.length + c.length + d.length;
        byte[] res = new byte[len];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        System.arraycopy(c, 0, res, a.length + b.length, c.length);
        System.arraycopy(d, 0, res, a.length + b.length + c.length, d.length);
        return res;
    }

    public static byte[] concat(byte[] a, byte[] b, byte[] c) {
        int len = a.length + b.length + c.length;
        byte[] res = new byte[len];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        System.arraycopy(c, 0, res, a.length + b.length, c.length);
        return res;
    }

    public static byte[] concat(byte[] a, byte[] b) {
        int len = a.length + b.length;
        byte[] res = new byte[len];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        return res;
    }

    public byte[] removeHeader(byte[] res) {
        int len = res.length - HEADER.length;
        byte[] bs = new byte[len];
        System.arraycopy(res, HEADER.length, bs, 0, len);
        return bs;
    }

    /* ================= */
    /* = Sending stuff = */
    /* ================= */
    /*
     * public byte[] send(byte[] bytes) throws Exception { Socket socket = new Socket(host, port); if (socket == null) { throw new
     * Exception("Couldn't create socket."); } OutputStream out = socket.getOutputStream();; out.write(bytes); InputStream is =
     * socket.getInputStream(); int len = readInt(is, 2); debug("length of response: " + len); byte[] data = readBytes(is, len); debugHex(data);
     * is.close(); socket.close(); return data; }
     */

    public byte[] send(byte[] bytes) throws Exception {
        if (tcp) {
            return sendTCP(bytes);
        }

        return sendUDP(bytes, 0);
    }

    public byte[] sendUDP(byte[] bytes, int resendCounter) throws Exception {

        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        try {
            int xrn = reqNo.incrementAndGet();
            if (log.isDebugEnabled()) {
                log.debug("Sending request[" + xrn + "] (udp) packet: " + HexUtils.toString(bytes) + "\nplain: '" + new String(bytes) + "'");
            } else {
                log.info("Sending request[" + xrn + "] (udp) packet: " + bytes.length + " bytes");
            }

            if (address == null) {
                address = InetAddress.getByName(host);
            }
            long s = System.currentTimeMillis();
            DatagramSocket socket = new DatagramSocket();
            socket.setSoTimeout(timeout);
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, port);

            socket.send(packet);
            int bufsize = bytes.length + 4 * 1024;
            DatagramPacket rec = new DatagramPacket(new byte[bufsize], bufsize);
            socket.receive(rec);
            long e = System.currentTimeMillis();
            byte[] response = rec.getData();
            if (log.isDebugEnabled()) {
                log.debug("Response [" + xrn + "] Received(udp): " + response.length + " bytes in " + (e - s) + "ms");
            } else {
                log.info("Response [" + xrn + "] Received(udp): " + response.length + " bytes in " + (e - s) + "ms");
            }

            int len = readInt(response);
            byte data[] = new byte[len];
            System.arraycopy(response, 2, data, 0, len);
            socket.close();
            return data;
        } catch (SocketTimeoutException ste) {
            log.error("UDP send error with HSM @" + host + ":" + port + "  (counter=" + resendCounter + ")", ste);
            if (resendCounter < MAX_RESEND) {
                log.info("UDP send retry..");
                return sendUDP(bytes, ++resendCounter);
            } else {
                throw ste;
            }
        }
    }

    public byte[] sendTCP(byte[] bytes) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        Exception lastException = null;
        int tries = 0;
        do {
            try {
                return sendTCPData(bytes);
            } catch (Exception e) {
                lastException = e;
                tries++;
                try {
                    int sleeptime = 1 * tries + 1;
                    if (log.isDebugEnabled()) {
                        log.debug("sleeping for " + sleeptime);
                    }

                    Thread.sleep(sleeptime);
                } catch (InterruptedException e2) {
                    // intertruped exception may not be wise to silently ignore
                    throw e2;
                }

                /*
                 * this here is really bug and should not be here if you open the socket here and no streams the send tcpSendData will naturally
                 * fail
                 *
                 * try { socket = newSocket(host, port); } catch (Exception e3) { lastException = e3; }
                 */

            }
        } while (tries < 3);

        throw lastException;
    }

    private synchronized byte[] sendTCPData(byte[] bytes) throws Exception {
        try {
            if (socket == null || socketMaxInactivity > 0 && socketLastActivity < System.currentTimeMillis() - socketMaxInactivity * 1000L) {
                if (socket != null) {
                    log.info("Closing old long inactive socket (last activity " + new java.util.Date(socketLastActivity) + ")");
                    closeSocketAndStreams();
                }

                long s = System.currentTimeMillis();
                socket = new Socket(host, port);
                os = socket.getOutputStream();
                is = socket.getInputStream();
                long e = System.currentTimeMillis();
                socketLastActivity = e;
                log.info("Socket connection to " + host + ":" + port + " in " + (e - s) + " ms");
            }

            int xrn = reqNo.incrementAndGet();
            if (log.isDebugEnabled()) {
                log.debug("Sending request[" + xrn + "] (tcp) packet: " + HexUtils.toString(bytes) + "\nplain: '" + new String(bytes) + "'");
            } else {
                log.info("Sending request[" + xrn + "] (tcp) packet: " + bytes.length + " bytes");
            }

            long s = System.currentTimeMillis();
            socketLastActivity = s;
            os.write(bytes);

            os.flush();
            int len = readInt(is, 2);
            long e = System.currentTimeMillis();
            socketLastActivity = e;
            if (log.isDebugEnabled()) {
                log.debug("Response[" + xrn + "] (tcp) length of response: " + len + " in " + (e - s) + "ms");
            } else {
                log.info("Response[" + xrn + "] (tcp) length of response: " + len + " in " + (e - s) + "ms");
            }
            s = System.currentTimeMillis();
            byte[] data = readBytes(is, len);
            e = System.currentTimeMillis();
            socketLastActivity = e;
            if (log.isDebugEnabled()) {
                log.debug("Response[" + xrn + "] (tcp) Read " + data.length + " bytes in " + (e - s) + "ms");
            } else {
                log.info("Response[" + xrn + "] (tcp) Read " + data.length + " bytes in " + (e - s) + "ms");
            }

            if (socketMaxInactivity < 0) {
                closeSocketAndStreams();
            }


            return data;
        } catch (IOException ioe) { // close only on exception leave open if well..
            log.error("TCP failure with HSM @" + host + ":" + port);
            closeSocketAndStreams();
            throw ioe;
        }
    }

    void closeSocketAndStreams() {
        if (is != null) {
            try {
                is.close();
            } catch (Exception dc) {
            }
            is = null;
        }
        if (os != null) {
            try {
                os.close();
            } catch (Exception dc) {
            }
            os = null;
        }
        if (socket != null) {
            try {
                socket.close();
            } catch (Exception dc) {
            }
            socket = null;
        }
    }

    /*
     * public byte[] send(byte[] bytes) throws Exception { DatagramSocket socket = new DatagramSocket(); if (socket == null) { throw new
     * Exception("Couldn't create socket."); } InetAddress address = InetAddress.getByName(host); DatagramPacket packet = new
     * DatagramPacket(bytes, bytes.length, address, port); socket.send(packet); byte[] response = new byte[2];
     * Debug.debug("waiting for response"); packet = new DatagramPacket(response, response.length); socket.receive(packet);
     * Debug.debug("reading..."); ByteArrayInputStream is = new ByteArrayInputStream(response); int len = readInt(is, 2);
     * debug("length of response: " + len); byte[] data = readBytes(is, len); debugHex(data); is.close(); socket.close(); return data; }
     */
    public byte[] sendCommand(byte[] command, byte[] params) throws Exception {
        int len = HEADER.length + command.length + params.length;
        byte[] hlen = getHsmCommandLength(len);

        byte[] com = concat(hlen, HEADER, command, params);
        byte[] res = send(com);
        return removeHeader(res);
    }

    public byte[] sendCommand(String command) throws Exception {
        return sendCommand(command.toUpperCase().getBytes(StandardCharsets.ISO_8859_1), new byte[0]);
    }

    public byte[] sendCommand(String command, byte[] params) throws Exception {
        return sendCommand(command.toUpperCase().getBytes(StandardCharsets.ISO_8859_1), params);
    }

    /* ================ */
    /* = Hex and back = */
    /* ================ */
    public static String hex(byte b) {
        String i = Integer.toHexString(0xFF & b);
        return (i.length() < 2) ? "0" + i : i;
    }

    public static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String s = Integer.toHexString(0xFF & bytes[i]).toUpperCase();
            hexString.append(s.length() < 2 ? "0" + s : s);
            hexString.append(" ");
        }
        return hexString.toString();
    }

    public static String toHexStringWithoutSpaces(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String s = Integer.toHexString(0xFF & bytes[i]);
            hexString.append(s.length() < 2 ? "0" + s : s);
        }
        return hexString.toString();
    }

    public static String hex2ascii(byte[] hex) {
        return new String(hex);
    }

    public static byte[] hexToBytes(String s) throws Exception {
        if (s == null) {
            return null;
        }
        if (s.length() == 0) {
            return new byte[0];
        }
        byte[] result = new byte[s.length() / 2];
        for (int i = 0; i < s.length(); i += 2) {
            result[i / 2] = (byte) Integer.parseInt(s.substring(i, i + 2), 16);
        }
        return result;
    }

    public static byte[] hexToRaw(byte[] s) throws Exception {
        if (s == null) {
            return null;
        }
        if (s.length == 0) {
            return new byte[0];
        }
        byte[] result = new byte[s.length / 2];
        for (int i = 0; i < s.length; i += 2) {
            result[i / 2] = (byte) Integer.parseInt(new String(s, i, 2), 16);
        }
        return result;
    }

    public static byte[] rawToHex(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String s = Integer.toHexString(0xFF & bytes[i]).toUpperCase();
            hexString.append(s.length() < 2 ? "0" + s : s);
        }
        return hexString.toString().getBytes(StandardCharsets.ISO_8859_1);
    }

    public static String removeSpaces(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c != ' ') {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    public void checkResponseCode(byte[] res, String code) throws Exception {
        String codeReceived = new String(res, 0, 2, StandardCharsets.ISO_8859_1);
        if (!codeReceived.equals(code)) {
            String errorCd = null;
            if (res.length > 3) {
                errorCd = new String(res, 2, 2, StandardCharsets.ISO_8859_1);
            }
            throw new RuntimeException("Received response code '" + codeReceived + "' not as expected '" + code + "'" + (errorCd != null ? ", error code '" + errorCd + "'" : ""));
        }
    }

    public void checkErrorCode(byte[] res) throws Exception {
        // 00 is ok code, others are errors
        if (res[2] != ok[0] || res[3] != ok[1]) {
            String msg = "Received error code " + new String(res, 2, 2) + " from " + host + ":" + port;
            log.error(msg);
            if (log.isDebugEnabled()) {
                log.debug(toHexString(res));
            }

            throw new RuntimeException(msg);
        }
    }

    /* ================ */
    /* = Key creation = */
    /* ================ */
    // the format is following:
    // 4 bytes of return code and status
    // -> ... 2 1 0 1 private key
    // four bytes of the length of the pubkey
    // -> pubkey
    public static byte[][] cutKeys(byte[] buf, int len) {
        for (int i = 4; i < buf.length; i++) {
            // System.out.println("[" + i + "]: " + hex(buf[i]));
            if (buf[i] == 0x02) {
                if (buf[i + 1] == 0x03 && buf[i + 2] == 0x1 && buf[i + 3] == 0x0 && buf[i + 4] == 0x1) {
                    int cut = i + 5;
                    byte[] begin = new byte[cut - 4];
                    byte[] length = new byte[4];
                    byte[] end = new byte[buf.length - cut - 4];
                    System.arraycopy(buf, 4, begin, 0, cut - 4);
                    System.arraycopy(buf, cut, length, 0, 4);
                    System.arraycopy(buf, cut + 4, end, 0, buf.length - cut - 4);
                    return new byte[][]{begin, length, end};
                }
            }
        }
        return null;
    }

    public static byte[][] cutKeysNew(byte[] buf, int len) {
        // System.out.println("cutkeys: "+ hex(buf));

        // bin -> bytes
        len = len / 8 + 11;
        byte[] pub = new byte[len];

        // 4 bytes are return values...
        int privLen = buf.length - len - 4 - 4;
        byte[] priv = new byte[privLen];
        byte[] length = new byte[4];
        System.arraycopy(buf, 4, pub, 0, len);
        System.arraycopy(buf, 4 + len, length, 0, 4);
        System.arraycopy(buf, 4 + len + 4, priv, 0, buf.length - len - 8);
        // System.out.println("priv: " + hex(priv));
        // System.out.println("pub: " + hex(pub));
        // System.out.println("length: " + hex(length));
        return new byte[][]{pub, length, priv};
    }

    @Override
    public KeyPair generateRSAdecryptAndSign(int keysize) throws HsmException {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        String command = "EI";
        int keyTypeIndicator = 4; // Data encryption/decryption (e.g. TLS/SSL premaster secret)

        try {
            if (log.isDebugEnabled()) {
                log.debug("generateRSAdecryptAndSign, type {}, keysize {}", keyTypeIndicator, keysize);
            }
            // 1: 0 = signature only
            // 2-6: modulus length in bits = 1024
            // 7-8: public key format: 01 = der, integer, 02 = der, twos
            // complement
            byte[] params = (keyTypeIndicator + zeroPad(4, keysize) + "01").getBytes();

            byte[] res = sendCommand(command, params);
            if (log.isDebugEnabled()) {
                log.debug(toHexString(res));
            }

            checkResponseCode(res, "EJ");
            checkErrorCode(res);
            byte[][] keys = cutKeys(res, keysize);

            PrivateKey privateKey = new MdRSAPrivateKey(keys[2]);
            PublicKey publicKey = new MdRSAPublicKey(keys[0]);
            return new KeyPair(publicKey, privateKey);
        } catch (Exception e) {
            throw new HsmException("Error generating RSA keypair.", e);
        }
    }

    @Override
    public ContentSigner getContentSigner(final byte[] rsaPrivateKeyBytes, final String signatureAlgorithm)  {
        return new ContentSigner() {
            private ByteArrayOutputStream dataToSign = new ByteArrayOutputStream();

            @Override
            public AlgorithmIdentifier getAlgorithmIdentifier() {
                return new DefaultSignatureAlgorithmIdentifierFinder().find(signatureAlgorithm);
            }

            @Override
            public OutputStream getOutputStream() {
                return dataToSign;
            }

            @Override
            public byte[] getSignature() {
                try {
                    return sign(rsaPrivateKeyBytes, dataToSign.toByteArray(), signatureAlgorithm);
                } catch (Exception e) {
                    log.error("Error generating RSA signature.", e);
                    return null;
                }
            }
        };
    }

    public KeyPair generateSSLRSA(int size) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        int type = 4;

        byte abyte2[][];
        if (log.isDebugEnabled()) {
            log.debug("generateSSLRSA, type " + type + ", keysize " + size);
        }
        String s = "EI";
        byte abyte0[] = (type + zeroPad(4, size) + "01").getBytes();
        byte abyte1[] = sendCommand(s, abyte0);

        if (log.isDebugEnabled()) {
            log.debug(toHexString(abyte1));
        }

        checkResponseCode(abyte1, "EJ");
        checkErrorCode(abyte1);
        abyte2 = cutKeys(abyte1, size);

        PrivateKey privateKey = new MdRSAPrivateKey(abyte2[2]);
        PublicKey publicKey = new MdRSAPublicKey(abyte2[0]);
        return new KeyPair(publicKey, privateKey);
    }

    public KeyResult generateCVK() throws Exception {

        // Debug.debug("generateCVK");
        String command = "A0";
        // 1: mode (1: zmk encrypt, 0: no)
        // 2-4: keytype
        // 5: key scheme LKM : U = encrypt double length key
        byte[] res = sendCommand(command, "0402U".getBytes());
        checkResponseCode(res, "A1");
        checkErrorCode(res);
        return new KeyResult(new String(res, 4, res.length - 10), new String(res, 37, 6));
    }

    /* ======== */
    /* = CVV = */
    /* ======== */
    public String calculateCVV(String key, String pan, String expiry, String serviceCode) throws Exception {
        String command = "CW";
        /*
         * System.out.println("key: " + key); System.out.println("pan: " + pan); System.out.println("expiry: " + expiry);
         * System.out.println("sc: " + serviceCode);
         */
        String params = key + pan + ";" + expiry + serviceCode;
        // System.out.println("params: " + params);

        byte[] res = sendCommand(command, params.getBytes());
        checkResponseCode(res, "CX");
        checkErrorCode(res);
        return new String(res).substring(4);
    }

    public byte[] hmacFromZMKtoLMK(String zmk, String hmac) throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("hmacFromZMKtoLMK");
        }
        String command = "LU";
        // System.out.println("mac len: " + hmac.length());
        // System.out.println("len2hex: " + len2hex(hmac.length()));
        System.out.println("zmk len: " + zmk.length());
        byte[] zmkb = zmk.getBytes();
        byte[] p2 = concat(zmkb, "0032".getBytes());
        byte[] p3 = concat(p2, hmac.getBytes());
        // 01 = pkcs#11 ecb, 00 = hmac key format
        byte[] p4 = concat(p3, ";0300".getBytes());
        // 01 = sha1, 01 = hmac generation
        byte[] p5 = concat(p4, "01010032".getBytes());
        System.out.println("params: " + new String(p5));
        byte[] res = sendCommand(command, p5);
        checkResponseCode(res, "LV");
        checkErrorCode(res);
        System.out.println("RAW OUT: " + toHexString(res));
        byte[] out = dropFirst(8, res);
        return out;
    }

    /*
     * public byte[] hmacFromZMKtoLMK(String zmk, String hmac) throws Exception { try { Debug.debug("hmacFromZMKtoLMK"); String command = "LU";
     * byte[] zmkb = zmk.getBytes(); byte[] p2 = concat(zmkb, len2hex(hmac.length()).getBytes()); byte[] p3 = concat(p2, hmac.getBytes());
     * byte[] p4 = concat(p3, "0000".getBytes()); byte[] res = sendCommand(command, p4); checkResponseCode(res, "LV"); checkErrorCode(res);
     * byte[] out = head(hmac.length(), drop(8, res)); return out; } catch (Exception e) { e.printStackTrace(); throw e; } }
     */

    /* ======= */
    /* = RSA = */
    /* ======= */
    public static String zeroPad(int len, int value) {
        return zeroPad(len, "" + value);
    }

    public static String zeroPad(int len, String number) {
        if (number.length() == len) {
            return number;
        }
        StringBuilder buf = new StringBuilder();
        while (buf.length() + number.length() < len) {
            buf.append('0');
        }
        buf.append(number);
        return buf.toString();
    }

    public static byte[] sub(int offs, int count, byte[] d) {
        byte[] out = new byte[count];
        System.arraycopy(d, offs, out, 0, count);
        return out;
    }


    public static byte[] dropFirst(int count, byte[] d) {
        byte[] out = new byte[d.length - count];
        System.arraycopy(d, count, out, 0, d.length - count);
        return out;
    }

    public static byte[] tail(int count, byte[] d) {
        byte[] out = new byte[count];
        System.arraycopy(d, d.length - count, out, 0, count);
        return out;
    }

    static final byte[] MD5_PREFIX = {(byte) 0x30, (byte) 0x20, (byte) 0x30, (byte) 0x0c, (byte) 0x06, (byte) 0x08, (byte) 0x2a, (byte) 0x86,
            (byte) 0x48, (byte) 0x86, (byte) 0xf7, (byte) 0x0d, (byte) 0x02, (byte) 0x05, (byte) 0x05, (byte) 0x00, (byte) 0x04, (byte) 0x10};
    static final byte[] MD5_PREFIX2 = {(byte) 0x30, (byte) 0x21, (byte) 0x30, (byte) 0x09, (byte) 0x06, (byte) 0x05, (byte) 0x2A, (byte) 0x86,
            (byte) 0x48, (byte) 0x86, (byte) 0xF7, (byte) 0x0D, (byte) 0x02, (byte) 0x05, (byte) 0x05, (byte) 0x00, (byte) 0x04, (byte) 0x14};
    public static byte[] TEST = {(byte) 0x30, (byte) 0x21, (byte) 0x30, (byte) 0x09, (byte) 0x06, (byte) 0x05, (byte) 0x2B, (byte) 0x0E,
            (byte) 0x03, (byte) 0x02, (byte) 0x1A, (byte) 0x05, (byte) 0x00, (byte) 0x04, (byte) 0x14, (byte) 0x01, (byte) 0x23, (byte) 0x45,
            (byte) 0x67, (byte) 0x89, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF, (byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67, (byte) 0x89,
            (byte) 0xAB, (byte) 0xCD, (byte) 0xEF, (byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67};
    public static byte[] SHA1PREFIX = {(byte) 0x30, (byte) 0x21, (byte) 0x30, (byte) 0x09, (byte) 0x06, (byte) 0x05, (byte) 0x2B, (byte) 0x0E,
            (byte) 0x03, (byte) 0x02, (byte) 0x1A, (byte) 0x05, (byte) 0x00, (byte) 0x04, (byte) 0x14};

    /*
     * public byte[] digestInfo(byte[] data) { try { // System.out.println("digest: data length: " + data.length); DigestInfo di = null; byte[]
     * out = null; if (data.length == 20) { out = concat(SHA1PREFIX, data); } else if (data.length == 16) { di = new DigestInfo(new
     * AlgorithmIdentifier( PKCSObjectIdentifiers.md5), data); out = di.getEncoded(); } // DigestInfo di = new DigestInfo(new //
     * AlgorithmIdentifier(PKCSObjectIdentifiers.md5WithRSAEncryption), // data);
     *
     * // byte[] out = concat(MD5_PREFIX, data); // System.out.println("di: " + hex(out));
     *
     * // writeFile("/tmp/di.der", out); return out; // return TEST; } catch (Exception e) { e.printStackTrace(); return data; } }
     */

    public byte[] pkcs1pad(byte[] data) {
        /*
         * int inLen = data.length; int inOff = 0; byte[] block = new byte[128-20]; block[0] = 0x00; block[0] = 0x01; for (int i = 2; i !=
         * block.length - inLen - 1; i++) { block[i] = (byte)0xFF; } block[block.length - inLen - 1] = 0x00; // mark the end of the padding
         * System.arraycopy(data, inOff, block, block.length - inLen, inLen);
         *
         * return block;
         */
        return data;
    }

    public byte[][] generateDes3Key(byte[] keyType) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        if (log.isDebugEnabled()) {
            log.debug("generateDes3Key: " + new String(keyType));
        }

        String command = "A0";
        // 00B is DEK
        byte[] params = "0".getBytes();
        byte[] p2 = concat(params, keyType);
        boolean keyblock = "FFF".equals(new String(keyType));

        byte[] p3 = null;
        if (keyblock) {
            p3 = concat(p2, "S".getBytes()); // /Key Scheme (LMK)
            if (lmkKB != null && lmkKB.length() == 2) {
                p3 = concat(p3, ("%" + lmkKB).getBytes());
            }
            p3 = concat(p3, "#".getBytes()); // delimiter
            p3 = concat(p3, "D0".getBytes()); // usage D0 Data encryption (generic) 21 Data encryption using a DEK
            p3 = concat(p3, "T3".getBytes()); // alg triple length des
            p3 = concat(p3, "B".getBytes()); // mode of use encrypt and decrypt
            p3 = concat(p3, "00".getBytes()); // key version number
            p3 = concat(p3, "N".getBytes()); // exportablility No
            p3 = concat(p3, "00".getBytes()); // optional blocks
        } else {
            p3 = concat(p2, "T".getBytes());
            if (lmkVA != null && lmkVA.length() == 2) {
                p3 = concat(p3, ("%" + lmkVA).getBytes());
            }

        }

        byte[] res = sendCommand(command, p3);
        checkResponseCode(res, "A1");
        checkErrorCode(res);
        if (log.isDebugEnabled()) {
            log.debug("res length: " + res.length);
        }

        byte[][] keyAndCv = new byte[2][];
        // System.out.println("1A="+new String(head(1, dropFirst(4, res)))+" Hex="+toHexString(head(1, dropFirst(4, res))));
        //System.out.println("Res="+new String(res));
        if (keyblock) {
            byte[] keylen = sub(6, 4, res);
            int keyln = Misc.parseInt(new String(keylen, StandardCharsets.ISO_8859_1));
            keyAndCv[0] = sub(5, keyln, res);
            keyAndCv[1] = sub(5 + keyln, 6, res);
        } else {
            keyAndCv[0] = HSMUtil.head(48, dropFirst(5, res));
            keyAndCv[1] = HSMUtil.head(6, dropFirst(5 + 48, res));
        }

        return keyAndCv;
    }

    public byte[] des3Encrypt(byte[] keyRaw, byte[] data) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("des3Encrypt k d");
        }

        return des3Encrypt(KEY_TYPE_DEK.getBytes(), keyRaw, data, false);
    }

    public byte[] des3Encrypt(byte[] keyType, byte[] keyRaw, byte[] data, boolean cbc) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        if (log.isDebugEnabled()) {
            log.debug("des3Encrypt kt k d");
        }

        String command = "M0";
        boolean keyblockkey = "FFF".equals(new String(keyType, StandardCharsets.ISO_8859_1));

        byte[] key = null;
        if (keyblockkey) {
            //key=keyRaw;
            key = concat("S".getBytes(), keyRaw);
        } else {
            key = concat("T".getBytes(), keyRaw);
        }

        if (log.isDebugEnabled()) {
            log.debug("data length: " + data.length + " key length: " + key.length);
        }

        byte[] paramsAndLenAndData = concat(
                concat((cbc ? "0100" : "0000").getBytes(StandardCharsets.ISO_8859_1), keyType),
                key,
                cbc ? IV_HEX : EMPTY,
                len2hex4(data.length).getBytes(StandardCharsets.ISO_8859_1),
                data);

        if (lmkKB != null && keyblockkey) {
            paramsAndLenAndData = concat(paramsAndLenAndData, ("%" + lmkKB).getBytes(StandardCharsets.ISO_8859_1));
        } else if (lmkVA != null && !keyblockkey) {
            paramsAndLenAndData = concat(paramsAndLenAndData, ("%" + lmkVA).getBytes(StandardCharsets.ISO_8859_1));
        }


        byte[] res = sendCommand(command, paramsAndLenAndData);
        checkResponseCode(res, "M1");
        checkErrorCode(res);
        byte[] out = dropFirst(cbc ? 8 + 16 : 8, res);
        if (log.isDebugEnabled()) {
            log.debug("hexout: " + toHexString(out));
        }

        return out;
    }


    public byte[] des3Decrypt(byte[] rawKey, byte[] data) throws Exception {
        return this.des3Decrypt(KEY_TYPE_DEK.getBytes(), rawKey, data, false);
    }

    public byte[] des3Decrypt(byte[] keyType, byte[] rawKey, byte[] data, boolean cbc) throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        String command = "M2";

        byte[] params = concat((cbc ? "0100" : "0000").getBytes(StandardCharsets.ISO_8859_1), keyType);
        boolean keyblockkey = "FFF".equals(new String(keyType, StandardCharsets.ISO_8859_1));

        byte[] key = null;
        if (keyblockkey) {
            key = concat("S".getBytes(), rawKey);
        } else {
            key = concat("T".getBytes(), rawKey);
        }

        byte[] paramsAndKey = concat(params, key);

        if (log.isDebugEnabled()) {
            log.debug("data length: " + data.length + " key length: " + key.length);
        }

        byte[] paramsAndKeyAndDLenAndData = concat(paramsAndKey, cbc ? IV_HEX : EMPTY, len2hex4(data.length).getBytes(), data);

        if (lmkKB != null && keyblockkey) {
            paramsAndKeyAndDLenAndData = concat(paramsAndKeyAndDLenAndData, ("%" + lmkKB).getBytes(StandardCharsets.ISO_8859_1));
        } else if (lmkVA != null && !keyblockkey) {
            paramsAndKeyAndDLenAndData = concat(paramsAndKeyAndDLenAndData, ("%" + lmkVA).getBytes(StandardCharsets.ISO_8859_1));
        }


        byte[] res = sendCommand(command, paramsAndKeyAndDLenAndData);

        checkResponseCode(res, "M3");
        checkErrorCode(res);
        byte[] out = dropFirst(cbc ? 8 + 16 : 8, res);

        if (log.isDebugEnabled()) {
            log.debug("out: " + toHexString(out));
        }

        return out;
    }

    public String len2hex4(int n) {
        return zeroPad(4, Integer.toHexString(n).toUpperCase());
    }

    public String len2hex5(int n) {
        return zeroPad(5, Integer.toHexString(n));
    }

    public String len2hex7(int n) {
        return zeroPad(7, Integer.toHexString(n));
    }


    public byte[] rsaEncrypt(byte[] key, byte[] data) throws Exception {
        String command = "EW";
        byte[] padded = data;
        byte[] p1 = concat(("040101" + zeroPad(4, padded.length)).getBytes(), padded);
        byte[] p2 = concat((";99" + zeroPad(4, key.length)).getBytes(), key);

        byte[] params = concat(p1, p2);
        if (log.isDebugEnabled()) {
            log.debug("length: " + params.length + " sending: " + toHexString(params));
        }

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "EX");
        checkErrorCode(res);

        byte[] out = dropFirst(8, res);
        // writeFile("/tmp/result.txt", out);
        return out;
    }

    /**
     * Generate an RSA Signature
     *
     * @param key key
     * @param data message data
     * @param signatureAlgorithm algorithm
     * @return byte array
     * @throws Exception
     */
    private byte[] sign(byte[] key, byte[] data, String signatureAlgorithm) throws Exception {
        String command = "EW";
        // hash, alg, pad ; data len, data
        SignAlgIdentifier sigAlgIdentifiers = SignAlgIdentifier.getBySignatureAlgorithm(signatureAlgorithm);
        String hashIdentifier = sigAlgIdentifiers.getHashIdentifier();
        String padModeIdentifier = sigAlgIdentifiers.getPadModeIdentifier();

        byte[] p1 = concat((hashIdentifier + "01" + padModeIdentifier + zeroPad(4, data.length)).getBytes(), data);
        // '00' ... '20' : index of stored private key '99' : use private key provided with command
        String kLen = zeroPad(4, key.length);
        byte[] p2 = concat((";99" + kLen).getBytes(), key);
        byte[] params = concat(p1, p2);

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "EX");
        checkErrorCode(res);
        return dropFirst(8, res);
    }

    public byte[] rsaPrivateEncrypt(byte key[], byte data[]) throws Exception {
        String s = "GI";
        if (log.isDebugEnabled()) {
            log.debug("key length: " + key.length + " rsa encrypt data length: " + data.length);
        }

        String opts = "01" + // RSA
                "01" + // pkcs#1
                "33" + // mask
                "00" + // MGF hash
                ""; // param len

        byte packet[] = opts.getBytes();
        byte[] datablock = concat((zeroPad(4, data.length)).getBytes(), data);
        datablock = concat(datablock, ";99".getBytes());
        datablock = concat(datablock, (zeroPad(4, key.length)).getBytes());
        datablock = concat(datablock, key);

        packet = concat(packet, datablock);

        byte abyte5[] = sendCommand(s, packet);
        checkResponseCode(abyte5, "GJ");
        checkErrorCode(abyte5);
        byte abyte6[] = dropFirst(8, abyte5);
        return abyte6;
    }

    @Override
    public byte[] rsaPrivateDecrypt(byte rsaPrivateKey[], byte encryptedData[], RSADecryptPadMode padMode)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("key length: {}; rsa decrypt data length: {}", rsaPrivateKey.length, encryptedData.length);
        }

        String commandCode = "GI";
        String commandPrefix;
        switch (padMode) {
            case PKCS1:
                commandPrefix = "01013400"; // encryption identifier + pad mode identifier + key type
                break;
            case OAEP_SHA1_MGF1:
                // encryption identifier + pad mode identifier + mask generation function + MGF Hash function
                // + OAEP params length + OAEP params delimiter + key type
                commandPrefix = "0102010100;3400";
                break;
            case OAEP_SHA256_MGF1:
                // encryption identifier + pad mode identifier + mask generation function + MGF Hash function
                // + OAEP params length + OAEP params delimiter + key type
                commandPrefix = "0102010600;3400";
                break;
            default:
                throw new HsmException("Unknown padding mode for RSA decrypt: " + padMode.name());
        }

        byte abyte2[] = concat((commandPrefix + zeroPad(4, encryptedData.length)).getBytes(), encryptedData);
        byte abyte3[] = concat((";99" + zeroPad(4, rsaPrivateKey.length)).getBytes(), rsaPrivateKey);
        byte abyte4[] = concat(abyte2, abyte3);
        byte abyte5[] = sendCommand(commandCode, abyte4);
        checkResponseCode(abyte5, "GJ");
        checkErrorCode(abyte5);
        byte abyte6[] = dropFirst(8, abyte5);
        return abyte6;
    }

    @Override
    public PrivateKey toRSAprivateKey(byte[] privateKeyBytes) {
        return new MdRSAPrivateKey(privateKeyBytes);
    }

    /*
     * public byte[] checksum(byte[] key) throws Exception { Debug.debug("checksumming " + hex(key)); String command = "M0"; byte[] params =
     * concat("000000B".getBytes(), key); byte[] iv = byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; byte[] input = byte[] { 0x00,
     * 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; pn = concat(params, iv, input); byte[] res =
     * sendCommand(command, pn); checkResponseCode("M1"); checkErrorCode(res); Debug.debug("CHECKSUM: " + hex(res)); return null; }
     */
    public byte[] checksum(byte[] key) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("checksumming " + toHexString(key));
        }
        String command = "BU";
        byte[] params = concat("421".getBytes(), key);
        // byte[] p2 = concat(params, ";402".getBytes());
        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "BV");
        checkErrorCode(res);
        if (log.isDebugEnabled()) {
            log.debug("CHECKSUM: " + toHexString(res));
        }

        return null;
    }

    public byte[] macPublicKey(byte[] key) throws Exception {
        // System.out.println("key: " + key);
        byte[] res = sendCommand("EO", concat("02".getBytes(), key));
        checkResponseCode(res, "EP");
        checkErrorCode(res);
        return res;
    }

    public void verifyMac(byte[] key, byte[] mac) throws Exception {
        String command = "EQ";
        byte[] params = concat(mac, key);
        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "ER");
        checkErrorCode(res);
    }

    public boolean verify(byte[] key, byte[] signature, byte[] data) throws Exception {
        String command = "EY";
        byte[] p1 = concat(("010101" + zeroPad(4, signature.length)).getBytes(), signature);
        byte[] p2 = concat((";" + zeroPad(4, data.length)).getBytes(), data);
        // byte[] pubMac = tail(4, head(8, macPublicKey(key)));
        byte[] pubMac = dropFirst(4, macPublicKey(key));
        // System.out.println("PUBMAC: " + hex(pubMac));
        // verifyMac(key, pubMac);
        byte[] p3 = concat(";".getBytes(), pubMac);
        byte[] params = concat(p1, p2, p3);
        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "EZ");
        checkErrorCode(res);
        return true;
    }

    public byte[] hmac(String key, byte[] data) throws Exception {
        String command = "LQ";

        // 01 = sha1, 20 output lenght, 00 thales hmac format
        byte[] p1 = concat(("01" + "0020" + "00" + zeroPad(4, key.length() / 2)).getBytes(), hexToBytes(key));
        byte[] p2 = concat(p1, (";" + zeroPad(5, data.length)).getBytes());
        byte[] params = concat(p2, data);

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "LR");
        checkErrorCode(res);
        byte[] out = dropFirst(8, res);

        return out;
    }

    public static String pad(int value, int len) {
        String v = "" + value;
        while (v.length() < len) {
            v = "0" + v;
        }
        return v;
    }

    public boolean alive() throws Exception {
        try {
            String ping = "0004ping";
            if (log.isDebugEnabled()) {
                log.debug("pinging..");
            }
            byte[] res = sendCommand("B2", ping.getBytes());
            if (log.isDebugEnabled()) {
                log.debug("RES: " + toHexString(res));
            }
            checkResponseCode(res, "B3");
            checkErrorCode(res);
            return true;
        } catch (Exception e) {
            log.error("error", e);
            return false;
        }
    }

    @Override
    public String toString() {
        return "R8000(" + host + ", " + port + ")";
    }

    /* ======== */
    /* = MAIN = */
    /* ======== */
    public static byte[] unhex(String s) {
        if (s.length() % 2 != 0) {
            return null;
        }

        byte[] result = new byte[s.length() / 2];
        for (int i = 0; i < s.length(); i += 2) {
            try {
                result[i / 2] = (byte) Integer.parseInt(s.substring(i, i + 2), 16);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /*
     * public static void main(String[] args) { try { R8000 r = new R8000(args[0], Integer.parseInt(args[1])); //byte[] response =
     * r.sendCommand("NO", new byte[] {0x30, 0x30}); //r.sendCommand(args[0]);
     *
     * KeyResult key = r.generateCVK(); System.out.println("got key: " + key.getKey()); System.out.println(""+ r.calculateCVV(key.getKey(),
     * "401600000000000", "0909", "000"));
     *
     *
     * RSAKeyResult rsa = r.generateRSA(1024); System.out.println("priv key: " + hex(rsa.getPrivateKey())); System.out.println("pub  key: " +
     * hex(rsa.getPublicKey())); //System.out.println("mac: " + hex(r.macPublicKey(rsa.getPublicKey()))); byte[] signature = r.sign(new
     * RSAPrivateKey(rsa.getPrivateKey()), "foobar"); System.out.println("signed: " + hex(signature)); boolean b = r.verify(rsa.getPublicKey(),
     * signature, "foobar".getBytes()); System.out.println("verified: " + b);
     *
     * r.alive(); r.checksum(args[2].getBytes()); } catch (Exception e) { e.printStackTrace(); } }
     */
    public static byte[] getRandomBytes(int i) {
        byte abyte0[] = new byte[i];
        Random random = new Random();
        random.nextBytes(abyte0);
        return abyte0;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.modirum.ds.hsm.HSMDevice#generateHSMDataEncryptionKey()
     */
    public Key generateHSMDataEncryptionKey() throws Exception {
        byte[][] keyAncCv = generateDes3Key("00B".getBytes());
        Key dk = new HSMDataKey(keyAncCv[0], HexUtils.fromString(new String(keyAncCv[1], StandardCharsets.ISO_8859_1)));
        return dk;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.modirum.ds.hsm.HSMDevice#encryptData(byte[], byte[])
     */
    public byte[] encryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception {
        // OLD compabily when key block from HSM as IS was converted to Binary.
        if (hsmDataEncryptionKey.length < 25) {
            hsmDataEncryptionKey = HexUtils.toString(hsmDataEncryptionKey).getBytes(StandardCharsets.ISO_8859_1);
        }
        return des3Encrypt(hsmDataEncryptionKey, usePad ? pkcs5pad(data, PKCS5_BLOCK_SIZE) : data);
    }

    /**
     * Looks up all LMKs available in an HSM.
     *
     * @return List of LMK data items.
     * @throws Exception in case of any HSM issues.
     */
    protected List<LMKData> getLmkList() throws Exception {
        byte[] res = sendCommand("JK");
        checkResponseCode(res, "JL");
        checkErrorCode(res);

        int indexofNumberLMKsLoaded = 35; // position of "Number LMKs loaded" field in command response.

        char tamperState = (char) res[34];
        if (tamperState == '2') {
            log.warn("Retrieving list of LMKs from Thales HSM. HSM is in tamper state = 2.");
            //remove extra 14 (tamper cause, tamper date, tamper time).
            indexofNumberLMKsLoaded += 14;
        }

        // Extract "Number LMKs loaded" field from response. 2 bytes
        byte[] numberOfLMKsLoaded = HSMUtil.head(2, dropFirst(indexofNumberLMKsLoaded, res));
        int numberOfLMKsLoadedAsInt = Integer.parseInt(new String(numberOfLMKsLoaded));

        // position of first appearance of "LMK id" field in command response.
        int indexofFirstLmkId = indexofNumberLMKsLoaded + 6;
        byte[] lmkListBlock = dropFirst(indexofFirstLmkId, res); // almost everything after that is list of LMKs

        // LMK list items are separated by X'14 byte. Last block is trailing information.
        byte[][] listOfLmks = HSMUtil.split(lmkListBlock, 0x14);
        int lmkBlocksNumber = listOfLmks.length - 1;

        if (numberOfLMKsLoadedAsInt != lmkBlocksNumber) {
            log.warn("JK command: 'Number LMKs loaded' field not equal to extracted LMK list length.");
        }

        List<LMKData> lmkList = new ArrayList<>();
        for (int i = 0; i < lmkBlocksNumber; i++) {
            byte[] lmkInformationBlock = listOfLmks[i];
            String lmkInformation = new String(lmkInformationBlock);

            String lmkID = lmkInformation.substring(0, 2);
            char lmkScheme = lmkInformation.charAt(5);
            String lmkKCV = getLMK_KCV(lmkID);

            lmkList.add(new LMKData(lmkID, lmkScheme, lmkKCV));
        }
        return lmkList;
    }

    /**
     * Retrieve LMK Key Check Value. LMK is determined by provided LMK ID.
     *
     * @param lmkID ID of the LMK to calculate KCV for.
     * @return 6 first digits of 16-digit LMK KCV.
     * @throws Exception in case of any HSM related errors.
     */
    protected String getLMK_KCV(String lmkID) throws Exception {
        String command = "NC";

        byte[] params = ("%" + lmkID).getBytes();

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "ND");
        checkErrorCode(res);
        byte[] out = dropFirst(4, res);
        byte[] lmkCheckValue = HSMUtil.head(16, out);
        String fullKCV = new String(lmkCheckValue); // Actual values is 6 digits right-padded with 0
        return fullKCV.substring(0, 6);
    }

    public byte[] pkcs5pad(byte[] data, final int bz) {
        int pad = bz - data.length % bz;
        byte[] padded = new byte[data.length + pad];
        System.arraycopy(data, 0, padded, 0, data.length);
        byte[] padbuf = new byte[pad];
        for (int i = 0; i < padbuf.length; i++) {
            padbuf[i] = (byte) pad;
        }
        System.arraycopy(padbuf, 0, padded, data.length, padbuf.length);

        return padded;
    }

    public byte[] pkcs5unpad(byte[] data, final int bz) {
        int pad = data[data.length - 1];
        if (pad < 1 || pad > bz || data.length < pad) {
            throw new RuntimeException("Invalid padding");
        }

        byte[] unpadded = new byte[data.length - pad];
        System.arraycopy(data, 0, unpadded, 0, unpadded.length);

        return unpadded;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.modirum.ds.hsm.HSMDevice#decryptData(byte[], byte[])
     */
    public byte[] decryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception {
        // OLD compabily when key block from HSM as IS was converted to Binary.
        if (hsmDataEncryptionKey.length < 25) {
            hsmDataEncryptionKey = HexUtils.toString(hsmDataEncryptionKey).getBytes(StandardCharsets.ISO_8859_1);
        }

        return usePad ? pkcs5unpad(des3Decrypt(hsmDataEncryptionKey, data), PKCS5_BLOCK_SIZE) : des3Decrypt(hsmDataEncryptionKey, data);
    }

    /**
     * HSM initialization method. Should be run once prior to HSM usage. Configures HSM initial data and run first
     * commands to determine LMK.
     * @param config Configuration string, e.g. "host=127.0.0.1;port=1500;tcp=true;lmkkcv=2A56FA"
     * @throws HsmException in case of HSM initialization errors.
     */
    @Override
    public void initializeHSM(String config) throws Exception {
        if (config != null && config.length() > 0) {
            String[] settings = Misc.split(config, ";");
            setConfig(settings);
        }

        if (lmkKCV != null) {
            log.info("Looking up HSM LMK ID and type using LMK KCV: {}", lmkKCV);
            setHsmLmk(lmkKCV);
        }
    }

    /**
     * Looks up and sets LMK ID and type by LMK KCV.
     * @param lmkKCV LMK KCV to use.
     */
    private void setHsmLmk(String lmkKCV) throws Exception {
        List<LMKData> lmkDataList = getLmkList();
        if (lmkDataList == null || lmkDataList.isEmpty()) {
            throw new HsmException("Unable to retrieve LMK list from HSM");
        }

        log.info("Found {} LMK keys", lmkDataList.size());

        LMKData matchingLMK = null;
        for (LMKData lmk : lmkDataList) {
            log.info("Found LMK ID = {}; LMK Scheme = {}; LMK KCV = {}",
                    lmk.getLmkID(), lmk.getScheme(), lmk.getKcv());

            if (lmkKCV.equalsIgnoreCase(lmk.getKcv())) {
                matchingLMK = lmk;
            }
        }

        if (matchingLMK == null) {
            throw new HsmException("No matching LMK found in HSM for KCV: " + lmkKCV);
        }

        if ('K' == matchingLMK.getScheme()) { // Key Block LMK
            lmkKB = matchingLMK.getLmkID();
            log.info("Initializing HSM with Key Block LMK ID: {}", lmkKB);
        } else { // default to Variant LMK
            lmkVA = matchingLMK.getLmkID();
            log.info("Initializing HSM with Variant LMK ID: {}", lmkVA);
        }
    }

    private void setConfig(String[] settings) {
        for (int i = 0; settings != null && i < settings.length; i++) {
            String[] spair = Misc.split(settings[i], "=");
            if (spair != null && spair.length == 2) {
                if ("host".equals(spair[0])) {
                    this.host = spair[1];
                } else if ("port".equals(spair[0])) {
                    this.port = Misc.parseInt(spair[1]);
                } else if ("tcp".equals(spair[0])) {
                    this.tcp = "true".equals(spair[1]);
                } else if ("udp".equals(spair[0])) {
                    this.tcp = !("true".equals(spair[1]));
                } else if ("lmkva".equals(spair[0]) && spair[1].length() == 2) {
                    this.lmkVA = spair[1];
                } else if ("lmkkb".equals(spair[0]) && spair[1].length() == 2) {
                    this.lmkKB = spair[1];
                } else if ("lmkkcv".equals(spair[0]) && spair[1].length() == 6) {
                    this.lmkKCV = spair[1];
                } else if ("smit".equals(spair[0]) && spair[1].length() == 2) {
                    socketMaxInactivity = Misc.parseInt(spair[1]);
                } else if ("to".equals(spair[0]) && spair[1].length() == 2) {
                    timeout = Misc.parseInt(spair[1]) * 1000;
                }
            }
        }
    }


    public static void main(String args[]) {
        try {
            // Security.insertProviderAt(new MdJCE(), 1);
            String host = args != null && args.length > 0 ? args[0] : "185.35.202.86";
            String port = args != null && args.length > 1 ? args[1] : "1500";
            HSMR8000Service r8000 = new HSMR8000Service(host, Integer.parseInt(port));
            r8000.alive();

            byte[] lmkEncryptedKey = r8000.generateDes3Key("00B".getBytes())[0];

            String data = "Some nice secret text to be encrypted!";
            while (data.length() % 8 > 0) {
                data = data + " ";
            }

            System.out.println("LMKencrypted key=" + HexUtils.toString(lmkEncryptedKey));
            byte[] encData = r8000.des3Encrypt(lmkEncryptedKey, data.getBytes());
            byte[] plainData = r8000.des3Decrypt(lmkEncryptedKey, encData);

            System.out.println("Dec data hex=" + HexUtils.toString(plainData));
            System.out.println("Dec data='" + new String(plainData) + "'");

            String k = "120f0BhMxh7MuVDCUnWWUHFp+xY+4Ovh";
            byte[] kd = Base64.decode(k);
            System.out.println("KD len=" + kd.length);
            byte[] encData2 = r8000.des3Encrypt(HexUtils.toString(kd).getBytes(), data.getBytes());
            System.out.println("Enc data2 hex=" + HexUtils.toString(encData2));

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public byte[] encryptDataInHSMWithMaster(byte[] data, boolean encrypt) throws Exception, UnsupportedOperationException {
        throw new UnsupportedOperationException("HSM does not support this..");
    }

    @Override
    public String getMasterKeyCheckValue() throws Exception {
        if (System.getSecurityManager() != null) {
            System.getSecurityManager().checkPermission(Permissions.MDMSecurityPermission);
        }

        if (log.isDebugEnabled()) {
            log.debug("getMasterKeyCheckValue");
        }

        String command = "NC";

        byte[] params = ("%00").getBytes();

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "ND");
        checkErrorCode(res);
        byte[] out = dropFirst(4, res);
        byte[] lmkcheckvalue = HSMUtil.head(16, out);
        String fullhexkcv = new String(lmkcheckvalue);

        // return fullhexkcv.substring(fullhexkcv.length() - 6);
        // seems thales returns only 6 digits in start and remaining is 0000..
        return fullhexkcv.substring(0, 6);
    }

    @Override
    public String getHardwareSerial() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("getHardwareSerial");
        }

        String command = "JK";

        byte[] params = new byte[0];

        byte[] res = sendCommand(command, params);
        checkResponseCode(res, "JL");
        checkErrorCode(res);
        byte[] out = dropFirst(4, res);
        byte[] serial = HSMUtil.head(12, out);
        String serialStr = new String(serial, "UTF-8");

        return serialStr;
    }

    @Override
    public byte[] ecPrivateDecrypt(byte ecPrivateKey[], byte encryptedData[], String cipherTransformation)
            throws HsmException {

        throw new HsmException("Hardware Elliptic Curve is unsupported/unimplemented.");
    }

    public boolean isUsePad() {
        return usePad;
    }

    public void setUsePad(boolean usePad) {
        this.usePad = usePad;
    }

}
