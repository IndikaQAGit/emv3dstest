package com.modirum.ds.hsm.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Any HSM-related operations exception. HSM exceptions are critical to DS functionality, every exception is logged
 * regardless of how it is handled up the stack.
 */
public class HsmException extends Exception {

    private static final Logger LOG = LoggerFactory.getLogger(HsmException.class);

    public HsmException(String s) {
        super(s);
        LOG.error("HsmException thrown, message: " + s, this);
    }

    public HsmException(String s, Throwable t) {
        super(s, t);
        LOG.error("HsmException thrown: Original causing problem was: ", t);
    }
}
