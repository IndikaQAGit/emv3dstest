/*
 * Copyright (C) 2012 Modirum
 * All rights reserved.
 *
 * @author Andri Kruus,
 * Estonia Tallinn, http://www.modirum.com
 *
 * Created on 05.12.2012
 * $Id: Exp $
 * $Header$
 * $Date$
 * @version $Revision$
 *
 */
package com.modirum.ds.hsm;

import com.modirum.ds.hsm.support.HsmException;
import com.modirum.ds.hsm.support.RSADecryptPadMode;
import org.bouncycastle.operator.ContentSigner;

import java.security.KeyPair;
import java.security.PrivateKey;

/**
 * The general interface for HSM security devices needed for VPOS.
 */
public interface HSMDevice {

    interface Key {

        byte[] getValue();

        byte[] getCheckValue();
    }

    /**
     * This method should implement HSM specific logic
     * to generate a data encryption key for the specific HSM in that HSM.
     */
    Key generateHSMDataEncryptionKey() throws Exception;


    /**
     * Encrypt the data with HSM data encryption key (under HSM LMK).
     */
    byte[] encryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception;

    /**
     * Decrypt data with HSM data encryption key (under HSM LMK)
     */
    byte[] decryptData(byte[] hsmDataEncryptionKey, byte[] data) throws Exception;

    /**
     * should return a device masterkey check value last 3 bytes as formatted hex string upper case.
     */
    String getMasterKeyCheckValue() throws Exception, UnsupportedOperationException;


    /**
     * Set the configuration string for this HSM device implementations
     * the sting contents may wary depending on what particular HSM needs
     * for example "host=127.0.0.1;port=1500". Initializes HSM for further use.
     */
    void initializeHSM(String config) throws Exception;

    String getHardwareSerial() throws Exception;

    byte[] encryptDataInHSMWithMaster(byte[] data, boolean encrypt) throws Exception, UnsupportedOperationException;


    /**
     * Decrypts data using provided RSA private key.
     *
     * @param rsaPrivateKey HSM-specific RSA private key.
     * @param encryptedData Data encrypted with corresponding public key component.
     * @param padMode RSA decrypt padding mode to use.
     * @return Decrypted data.
     * @throws Exception in case of any HSM-related errors.
     */
    byte[] rsaPrivateDecrypt(byte rsaPrivateKey[], byte encryptedData[], RSADecryptPadMode padMode) throws Exception;

    /**
     * Decrypts data using provided EC private key.
     *
     * @param ecPrivateKey HSM-specific EC private key.
     * @param encryptedData Data encrypted with corresponding public key component.
     * @param cipherTransformation EC decrypt cipher transformation (e.g. ECIES)
     * @return Decrypted data.
     * @throws Exception in case of any HSM-related errors.
     */
    byte[] ecPrivateDecrypt(byte ecPrivateKey[], byte encryptedData[], String cipherTransformation) throws Exception;

    /**
     * Instantiates {@link ContentSigner} for use with BouncyCastle interfaces when implicit JCA security provider
     * implementation cannot be used.
     *
     * @param rsaPrivateKeyBytes HSM device specific rsa private key bytes.
     * @param signatureAlgorithm signature algorithm.
     * @return Content signer instance with setup private key and signature algorithm.
     */
    ContentSigner getContentSigner(final byte[] rsaPrivateKeyBytes, final String signatureAlgorithm) throws Exception;

    /**
     * Converts HSM-specific RSA key bytes to Private Key instance.
     * @param privateKeyBytes HSM specific RSA key bytes
     * @return RSA private key.
     */
    PrivateKey toRSAprivateKey(byte[] privateKeyBytes) throws Exception;

    KeyPair generateRSAdecryptAndSign(int keyLength)
        throws HsmException;
}
