parent_docker_version="0.0.1";
echo "Building Azure Images with DS_VERSION=${DS_VERSION}";
echo "parent_docker_version=${parent_docker_version}";
echo "ssh username=${username}";
echo "git_hash=${git_hash}";

docker build -f azure/docker/tomcat-curl-parent/Dockerfile . --tag modirum.azurecr.io/tomcat-curl-parent:${parent_docker_version};
docker build -f azure/docker/tomcat-curl-parent/Dockerfile . --tag modirum.azurecr.io/tomcat-curl-parent:latest;

ssh -D localhost:8084 -f -C -N ${username}@jump.ci.modirum.com;
docker build --build-arg parent_docker_version=${parent_docker_version} --build-arg username=${username} --build-arg git_hash=${git_hash} -f azure/docker/ds-admin/Dockerfile . --tag modirum.azurecr.io/mdpayds-admin:${DS_VERSION};
docker build --build-arg parent_docker_version=${parent_docker_version} --build-arg username=${username} --build-arg git_hash=${git_hash} -f azure/docker/ds/Dockerfile . --tag modirum.azurecr.io/mdpayds:${DS_VERSION};